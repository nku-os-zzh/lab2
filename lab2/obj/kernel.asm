
bin/kernel:     file format elf32-i386


Disassembly of section .text:

c0100000 <kern_entry>:

.text
.globl kern_entry
kern_entry:
    # load pa of boot pgdir
    movl $REALLOC(__boot_pgdir), %eax
c0100000:	b8 00 a0 11 00       	mov    $0x11a000,%eax
    movl %eax, %cr3
c0100005:	0f 22 d8             	mov    %eax,%cr3

    # enable paging
    movl %cr0, %eax
c0100008:	0f 20 c0             	mov    %cr0,%eax
    orl $(CR0_PE | CR0_PG | CR0_AM | CR0_WP | CR0_NE | CR0_TS | CR0_EM | CR0_MP), %eax
c010000b:	0d 2f 00 05 80       	or     $0x8005002f,%eax
    andl $~(CR0_TS | CR0_EM), %eax
c0100010:	83 e0 f3             	and    $0xfffffff3,%eax
    movl %eax, %cr0
c0100013:	0f 22 c0             	mov    %eax,%cr0

    # update eip
    # now, eip = 0x1.....
    leal next, %eax
c0100016:	8d 05 1e 00 10 c0    	lea    0xc010001e,%eax
    # set eip = KERNBASE + 0x1.....
    jmp *%eax
c010001c:	ff e0                	jmp    *%eax

c010001e <next>:
next:

    # unmap va 0 ~ 4M, it's temporary mapping
    xorl %eax, %eax
c010001e:	31 c0                	xor    %eax,%eax
    movl %eax, __boot_pgdir
c0100020:	a3 00 a0 11 c0       	mov    %eax,0xc011a000

    # set ebp, esp
    movl $0x0, %ebp
c0100025:	bd 00 00 00 00       	mov    $0x0,%ebp
    # the kernel stack region is from bootstack -- bootstacktop,
    # the kernel stack size is KSTACKSIZE (8KB)defined in memlayout.h
    movl $bootstacktop, %esp
c010002a:	bc 00 90 11 c0       	mov    $0xc0119000,%esp
    # now kernel stack is ready , call the first C function
    call kern_init
c010002f:	e8 02 00 00 00       	call   c0100036 <kern_init>

c0100034 <spin>:

# should never get here
spin:
    jmp spin
c0100034:	eb fe                	jmp    c0100034 <spin>

c0100036 <kern_init>:
int kern_init(void) __attribute__((noreturn));
void grade_backtrace(void);
static void lab1_switch_test(void);

int
kern_init(void) {
c0100036:	f3 0f 1e fb          	endbr32 
c010003a:	55                   	push   %ebp
c010003b:	89 e5                	mov    %esp,%ebp
c010003d:	83 ec 28             	sub    $0x28,%esp
    extern char edata[], end[];
    memset(edata, 0, end - edata);
c0100040:	b8 28 cf 11 c0       	mov    $0xc011cf28,%eax
c0100045:	2d 00 c0 11 c0       	sub    $0xc011c000,%eax
c010004a:	89 44 24 08          	mov    %eax,0x8(%esp)
c010004e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c0100055:	00 
c0100056:	c7 04 24 00 c0 11 c0 	movl   $0xc011c000,(%esp)
c010005d:	e8 95 59 00 00       	call   c01059f7 <memset>

    cons_init();                // init the console
c0100062:	e8 4f 16 00 00       	call   c01016b6 <cons_init>

    const char *message = "(THU.CST) os is loading ...";
c0100067:	c7 45 f4 20 62 10 c0 	movl   $0xc0106220,-0xc(%ebp)
    cprintf("%s\n\n", message);
c010006e:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100071:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100075:	c7 04 24 3c 62 10 c0 	movl   $0xc010623c,(%esp)
c010007c:	e8 39 02 00 00       	call   c01002ba <cprintf>

    print_kerninfo();
c0100081:	e8 f7 08 00 00       	call   c010097d <print_kerninfo>

    grade_backtrace();
c0100086:	e8 95 00 00 00       	call   c0100120 <grade_backtrace>

    pmm_init();                 // init physical memory management
c010008b:	e8 66 32 00 00       	call   c01032f6 <pmm_init>

    pic_init();                 // init interrupt controller
c0100090:	e8 9c 17 00 00       	call   c0101831 <pic_init>
    idt_init();                 // init interrupt descriptor table
c0100095:	e8 41 19 00 00       	call   c01019db <idt_init>

    clock_init();               // init clock interrupt
c010009a:	e8 5e 0d 00 00       	call   c0100dfd <clock_init>
    intr_enable();              // enable irq interrupt
c010009f:	e8 d9 18 00 00       	call   c010197d <intr_enable>
    //LAB1: CAHLLENGE 1 If you try to do it, uncomment lab1_switch_test()
    // user/kernel mode switch test
    //lab1_switch_test();

    /* do nothing */
    while (1);
c01000a4:	eb fe                	jmp    c01000a4 <kern_init+0x6e>

c01000a6 <grade_backtrace2>:
}

void __attribute__((noinline))
grade_backtrace2(int arg0, int arg1, int arg2, int arg3) {
c01000a6:	f3 0f 1e fb          	endbr32 
c01000aa:	55                   	push   %ebp
c01000ab:	89 e5                	mov    %esp,%ebp
c01000ad:	83 ec 18             	sub    $0x18,%esp
    mon_backtrace(0, NULL, NULL);
c01000b0:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c01000b7:	00 
c01000b8:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c01000bf:	00 
c01000c0:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
c01000c7:	e8 1b 0d 00 00       	call   c0100de7 <mon_backtrace>
}
c01000cc:	90                   	nop
c01000cd:	c9                   	leave  
c01000ce:	c3                   	ret    

c01000cf <grade_backtrace1>:

void __attribute__((noinline))
grade_backtrace1(int arg0, int arg1) {
c01000cf:	f3 0f 1e fb          	endbr32 
c01000d3:	55                   	push   %ebp
c01000d4:	89 e5                	mov    %esp,%ebp
c01000d6:	53                   	push   %ebx
c01000d7:	83 ec 14             	sub    $0x14,%esp
    grade_backtrace2(arg0, (int)&arg0, arg1, (int)&arg1);
c01000da:	8d 4d 0c             	lea    0xc(%ebp),%ecx
c01000dd:	8b 55 0c             	mov    0xc(%ebp),%edx
c01000e0:	8d 5d 08             	lea    0x8(%ebp),%ebx
c01000e3:	8b 45 08             	mov    0x8(%ebp),%eax
c01000e6:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
c01000ea:	89 54 24 08          	mov    %edx,0x8(%esp)
c01000ee:	89 5c 24 04          	mov    %ebx,0x4(%esp)
c01000f2:	89 04 24             	mov    %eax,(%esp)
c01000f5:	e8 ac ff ff ff       	call   c01000a6 <grade_backtrace2>
}
c01000fa:	90                   	nop
c01000fb:	83 c4 14             	add    $0x14,%esp
c01000fe:	5b                   	pop    %ebx
c01000ff:	5d                   	pop    %ebp
c0100100:	c3                   	ret    

c0100101 <grade_backtrace0>:

void __attribute__((noinline))
grade_backtrace0(int arg0, int arg1, int arg2) {
c0100101:	f3 0f 1e fb          	endbr32 
c0100105:	55                   	push   %ebp
c0100106:	89 e5                	mov    %esp,%ebp
c0100108:	83 ec 18             	sub    $0x18,%esp
    grade_backtrace1(arg0, arg2);
c010010b:	8b 45 10             	mov    0x10(%ebp),%eax
c010010e:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100112:	8b 45 08             	mov    0x8(%ebp),%eax
c0100115:	89 04 24             	mov    %eax,(%esp)
c0100118:	e8 b2 ff ff ff       	call   c01000cf <grade_backtrace1>
}
c010011d:	90                   	nop
c010011e:	c9                   	leave  
c010011f:	c3                   	ret    

c0100120 <grade_backtrace>:

void
grade_backtrace(void) {
c0100120:	f3 0f 1e fb          	endbr32 
c0100124:	55                   	push   %ebp
c0100125:	89 e5                	mov    %esp,%ebp
c0100127:	83 ec 18             	sub    $0x18,%esp
    grade_backtrace0(0, (int)kern_init, 0xffff0000);
c010012a:	b8 36 00 10 c0       	mov    $0xc0100036,%eax
c010012f:	c7 44 24 08 00 00 ff 	movl   $0xffff0000,0x8(%esp)
c0100136:	ff 
c0100137:	89 44 24 04          	mov    %eax,0x4(%esp)
c010013b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
c0100142:	e8 ba ff ff ff       	call   c0100101 <grade_backtrace0>
}
c0100147:	90                   	nop
c0100148:	c9                   	leave  
c0100149:	c3                   	ret    

c010014a <lab1_print_cur_status>:

static void
lab1_print_cur_status(void) {
c010014a:	f3 0f 1e fb          	endbr32 
c010014e:	55                   	push   %ebp
c010014f:	89 e5                	mov    %esp,%ebp
c0100151:	83 ec 28             	sub    $0x28,%esp
    static int round = 0;
    uint16_t reg1, reg2, reg3, reg4;
    asm volatile (
c0100154:	8c 4d f6             	mov    %cs,-0xa(%ebp)
c0100157:	8c 5d f4             	mov    %ds,-0xc(%ebp)
c010015a:	8c 45 f2             	mov    %es,-0xe(%ebp)
c010015d:	8c 55 f0             	mov    %ss,-0x10(%ebp)
            "mov %%cs, %0;"
            "mov %%ds, %1;"
            "mov %%es, %2;"
            "mov %%ss, %3;"
            : "=m"(reg1), "=m"(reg2), "=m"(reg3), "=m"(reg4));
    cprintf("%d: @ring %d\n", round, reg1 & 3);
c0100160:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
c0100164:	83 e0 03             	and    $0x3,%eax
c0100167:	89 c2                	mov    %eax,%edx
c0100169:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c010016e:	89 54 24 08          	mov    %edx,0x8(%esp)
c0100172:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100176:	c7 04 24 41 62 10 c0 	movl   $0xc0106241,(%esp)
c010017d:	e8 38 01 00 00       	call   c01002ba <cprintf>
    cprintf("%d:  cs = %x\n", round, reg1);
c0100182:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
c0100186:	89 c2                	mov    %eax,%edx
c0100188:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c010018d:	89 54 24 08          	mov    %edx,0x8(%esp)
c0100191:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100195:	c7 04 24 4f 62 10 c0 	movl   $0xc010624f,(%esp)
c010019c:	e8 19 01 00 00       	call   c01002ba <cprintf>
    cprintf("%d:  ds = %x\n", round, reg2);
c01001a1:	0f b7 45 f4          	movzwl -0xc(%ebp),%eax
c01001a5:	89 c2                	mov    %eax,%edx
c01001a7:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c01001ac:	89 54 24 08          	mov    %edx,0x8(%esp)
c01001b0:	89 44 24 04          	mov    %eax,0x4(%esp)
c01001b4:	c7 04 24 5d 62 10 c0 	movl   $0xc010625d,(%esp)
c01001bb:	e8 fa 00 00 00       	call   c01002ba <cprintf>
    cprintf("%d:  es = %x\n", round, reg3);
c01001c0:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
c01001c4:	89 c2                	mov    %eax,%edx
c01001c6:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c01001cb:	89 54 24 08          	mov    %edx,0x8(%esp)
c01001cf:	89 44 24 04          	mov    %eax,0x4(%esp)
c01001d3:	c7 04 24 6b 62 10 c0 	movl   $0xc010626b,(%esp)
c01001da:	e8 db 00 00 00       	call   c01002ba <cprintf>
    cprintf("%d:  ss = %x\n", round, reg4);
c01001df:	0f b7 45 f0          	movzwl -0x10(%ebp),%eax
c01001e3:	89 c2                	mov    %eax,%edx
c01001e5:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c01001ea:	89 54 24 08          	mov    %edx,0x8(%esp)
c01001ee:	89 44 24 04          	mov    %eax,0x4(%esp)
c01001f2:	c7 04 24 79 62 10 c0 	movl   $0xc0106279,(%esp)
c01001f9:	e8 bc 00 00 00       	call   c01002ba <cprintf>
    round ++;
c01001fe:	a1 00 c0 11 c0       	mov    0xc011c000,%eax
c0100203:	40                   	inc    %eax
c0100204:	a3 00 c0 11 c0       	mov    %eax,0xc011c000
}
c0100209:	90                   	nop
c010020a:	c9                   	leave  
c010020b:	c3                   	ret    

c010020c <lab1_switch_to_user>:

static void
lab1_switch_to_user(void) {
c010020c:	f3 0f 1e fb          	endbr32 
c0100210:	55                   	push   %ebp
c0100211:	89 e5                	mov    %esp,%ebp
    //LAB1 CHALLENGE 1 : TODO
}
c0100213:	90                   	nop
c0100214:	5d                   	pop    %ebp
c0100215:	c3                   	ret    

c0100216 <lab1_switch_to_kernel>:

static void
lab1_switch_to_kernel(void) {
c0100216:	f3 0f 1e fb          	endbr32 
c010021a:	55                   	push   %ebp
c010021b:	89 e5                	mov    %esp,%ebp
    //LAB1 CHALLENGE 1 :  TODO
}
c010021d:	90                   	nop
c010021e:	5d                   	pop    %ebp
c010021f:	c3                   	ret    

c0100220 <lab1_switch_test>:

static void
lab1_switch_test(void) {
c0100220:	f3 0f 1e fb          	endbr32 
c0100224:	55                   	push   %ebp
c0100225:	89 e5                	mov    %esp,%ebp
c0100227:	83 ec 18             	sub    $0x18,%esp
    lab1_print_cur_status();
c010022a:	e8 1b ff ff ff       	call   c010014a <lab1_print_cur_status>
    cprintf("+++ switch to  user  mode +++\n");
c010022f:	c7 04 24 88 62 10 c0 	movl   $0xc0106288,(%esp)
c0100236:	e8 7f 00 00 00       	call   c01002ba <cprintf>
    lab1_switch_to_user();
c010023b:	e8 cc ff ff ff       	call   c010020c <lab1_switch_to_user>
    lab1_print_cur_status();
c0100240:	e8 05 ff ff ff       	call   c010014a <lab1_print_cur_status>
    cprintf("+++ switch to kernel mode +++\n");
c0100245:	c7 04 24 a8 62 10 c0 	movl   $0xc01062a8,(%esp)
c010024c:	e8 69 00 00 00       	call   c01002ba <cprintf>
    lab1_switch_to_kernel();
c0100251:	e8 c0 ff ff ff       	call   c0100216 <lab1_switch_to_kernel>
    lab1_print_cur_status();
c0100256:	e8 ef fe ff ff       	call   c010014a <lab1_print_cur_status>
}
c010025b:	90                   	nop
c010025c:	c9                   	leave  
c010025d:	c3                   	ret    

c010025e <cputch>:
/* *
 * cputch - writes a single character @c to stdout, and it will
 * increace the value of counter pointed by @cnt.
 * */
static void
cputch(int c, int *cnt) {
c010025e:	f3 0f 1e fb          	endbr32 
c0100262:	55                   	push   %ebp
c0100263:	89 e5                	mov    %esp,%ebp
c0100265:	83 ec 18             	sub    $0x18,%esp
    cons_putc(c);
c0100268:	8b 45 08             	mov    0x8(%ebp),%eax
c010026b:	89 04 24             	mov    %eax,(%esp)
c010026e:	e8 74 14 00 00       	call   c01016e7 <cons_putc>
    (*cnt) ++;
c0100273:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100276:	8b 00                	mov    (%eax),%eax
c0100278:	8d 50 01             	lea    0x1(%eax),%edx
c010027b:	8b 45 0c             	mov    0xc(%ebp),%eax
c010027e:	89 10                	mov    %edx,(%eax)
}
c0100280:	90                   	nop
c0100281:	c9                   	leave  
c0100282:	c3                   	ret    

c0100283 <vcprintf>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want cprintf() instead.
 * */
int
vcprintf(const char *fmt, va_list ap) {
c0100283:	f3 0f 1e fb          	endbr32 
c0100287:	55                   	push   %ebp
c0100288:	89 e5                	mov    %esp,%ebp
c010028a:	83 ec 28             	sub    $0x28,%esp
    int cnt = 0;
c010028d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    vprintfmt((void*)cputch, &cnt, fmt, ap);
c0100294:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100297:	89 44 24 0c          	mov    %eax,0xc(%esp)
c010029b:	8b 45 08             	mov    0x8(%ebp),%eax
c010029e:	89 44 24 08          	mov    %eax,0x8(%esp)
c01002a2:	8d 45 f4             	lea    -0xc(%ebp),%eax
c01002a5:	89 44 24 04          	mov    %eax,0x4(%esp)
c01002a9:	c7 04 24 5e 02 10 c0 	movl   $0xc010025e,(%esp)
c01002b0:	e8 ae 5a 00 00       	call   c0105d63 <vprintfmt>
    return cnt;
c01002b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c01002b8:	c9                   	leave  
c01002b9:	c3                   	ret    

c01002ba <cprintf>:
 *
 * The return value is the number of characters which would be
 * written to stdout.
 * */
int
cprintf(const char *fmt, ...) {
c01002ba:	f3 0f 1e fb          	endbr32 
c01002be:	55                   	push   %ebp
c01002bf:	89 e5                	mov    %esp,%ebp
c01002c1:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    int cnt;
    va_start(ap, fmt);
c01002c4:	8d 45 0c             	lea    0xc(%ebp),%eax
c01002c7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    cnt = vcprintf(fmt, ap);
c01002ca:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01002cd:	89 44 24 04          	mov    %eax,0x4(%esp)
c01002d1:	8b 45 08             	mov    0x8(%ebp),%eax
c01002d4:	89 04 24             	mov    %eax,(%esp)
c01002d7:	e8 a7 ff ff ff       	call   c0100283 <vcprintf>
c01002dc:	89 45 f4             	mov    %eax,-0xc(%ebp)
    va_end(ap);
    return cnt;
c01002df:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c01002e2:	c9                   	leave  
c01002e3:	c3                   	ret    

c01002e4 <cputchar>:

/* cputchar - writes a single character to stdout */
void
cputchar(int c) {
c01002e4:	f3 0f 1e fb          	endbr32 
c01002e8:	55                   	push   %ebp
c01002e9:	89 e5                	mov    %esp,%ebp
c01002eb:	83 ec 18             	sub    $0x18,%esp
    cons_putc(c);
c01002ee:	8b 45 08             	mov    0x8(%ebp),%eax
c01002f1:	89 04 24             	mov    %eax,(%esp)
c01002f4:	e8 ee 13 00 00       	call   c01016e7 <cons_putc>
}
c01002f9:	90                   	nop
c01002fa:	c9                   	leave  
c01002fb:	c3                   	ret    

c01002fc <cputs>:
/* *
 * cputs- writes the string pointed by @str to stdout and
 * appends a newline character.
 * */
int
cputs(const char *str) {
c01002fc:	f3 0f 1e fb          	endbr32 
c0100300:	55                   	push   %ebp
c0100301:	89 e5                	mov    %esp,%ebp
c0100303:	83 ec 28             	sub    $0x28,%esp
    int cnt = 0;
c0100306:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    char c;
    while ((c = *str ++) != '\0') {
c010030d:	eb 13                	jmp    c0100322 <cputs+0x26>
        cputch(c, &cnt);
c010030f:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
c0100313:	8d 55 f0             	lea    -0x10(%ebp),%edx
c0100316:	89 54 24 04          	mov    %edx,0x4(%esp)
c010031a:	89 04 24             	mov    %eax,(%esp)
c010031d:	e8 3c ff ff ff       	call   c010025e <cputch>
    while ((c = *str ++) != '\0') {
c0100322:	8b 45 08             	mov    0x8(%ebp),%eax
c0100325:	8d 50 01             	lea    0x1(%eax),%edx
c0100328:	89 55 08             	mov    %edx,0x8(%ebp)
c010032b:	0f b6 00             	movzbl (%eax),%eax
c010032e:	88 45 f7             	mov    %al,-0x9(%ebp)
c0100331:	80 7d f7 00          	cmpb   $0x0,-0x9(%ebp)
c0100335:	75 d8                	jne    c010030f <cputs+0x13>
    }
    cputch('\n', &cnt);
c0100337:	8d 45 f0             	lea    -0x10(%ebp),%eax
c010033a:	89 44 24 04          	mov    %eax,0x4(%esp)
c010033e:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
c0100345:	e8 14 ff ff ff       	call   c010025e <cputch>
    return cnt;
c010034a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
c010034d:	c9                   	leave  
c010034e:	c3                   	ret    

c010034f <getchar>:

/* getchar - reads a single non-zero character from stdin */
int
getchar(void) {
c010034f:	f3 0f 1e fb          	endbr32 
c0100353:	55                   	push   %ebp
c0100354:	89 e5                	mov    %esp,%ebp
c0100356:	83 ec 18             	sub    $0x18,%esp
    int c;
    while ((c = cons_getc()) == 0)
c0100359:	90                   	nop
c010035a:	e8 c9 13 00 00       	call   c0101728 <cons_getc>
c010035f:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0100362:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0100366:	74 f2                	je     c010035a <getchar+0xb>
        /* do nothing */;
    return c;
c0100368:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c010036b:	c9                   	leave  
c010036c:	c3                   	ret    

c010036d <readline>:
 * The readline() function returns the text of the line read. If some errors
 * are happened, NULL is returned. The return value is a global variable,
 * thus it should be copied before it is used.
 * */
char *
readline(const char *prompt) {
c010036d:	f3 0f 1e fb          	endbr32 
c0100371:	55                   	push   %ebp
c0100372:	89 e5                	mov    %esp,%ebp
c0100374:	83 ec 28             	sub    $0x28,%esp
    if (prompt != NULL) {
c0100377:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c010037b:	74 13                	je     c0100390 <readline+0x23>
        cprintf("%s", prompt);
c010037d:	8b 45 08             	mov    0x8(%ebp),%eax
c0100380:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100384:	c7 04 24 c7 62 10 c0 	movl   $0xc01062c7,(%esp)
c010038b:	e8 2a ff ff ff       	call   c01002ba <cprintf>
    }
    int i = 0, c;
c0100390:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while (1) {
        c = getchar();
c0100397:	e8 b3 ff ff ff       	call   c010034f <getchar>
c010039c:	89 45 f0             	mov    %eax,-0x10(%ebp)
        if (c < 0) {
c010039f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c01003a3:	79 07                	jns    c01003ac <readline+0x3f>
            return NULL;
c01003a5:	b8 00 00 00 00       	mov    $0x0,%eax
c01003aa:	eb 78                	jmp    c0100424 <readline+0xb7>
        }
        else if (c >= ' ' && i < BUFSIZE - 1) {
c01003ac:	83 7d f0 1f          	cmpl   $0x1f,-0x10(%ebp)
c01003b0:	7e 28                	jle    c01003da <readline+0x6d>
c01003b2:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
c01003b9:	7f 1f                	jg     c01003da <readline+0x6d>
            cputchar(c);
c01003bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01003be:	89 04 24             	mov    %eax,(%esp)
c01003c1:	e8 1e ff ff ff       	call   c01002e4 <cputchar>
            buf[i ++] = c;
c01003c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01003c9:	8d 50 01             	lea    0x1(%eax),%edx
c01003cc:	89 55 f4             	mov    %edx,-0xc(%ebp)
c01003cf:	8b 55 f0             	mov    -0x10(%ebp),%edx
c01003d2:	88 90 20 c0 11 c0    	mov    %dl,-0x3fee3fe0(%eax)
c01003d8:	eb 45                	jmp    c010041f <readline+0xb2>
        }
        else if (c == '\b' && i > 0) {
c01003da:	83 7d f0 08          	cmpl   $0x8,-0x10(%ebp)
c01003de:	75 16                	jne    c01003f6 <readline+0x89>
c01003e0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01003e4:	7e 10                	jle    c01003f6 <readline+0x89>
            cputchar(c);
c01003e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01003e9:	89 04 24             	mov    %eax,(%esp)
c01003ec:	e8 f3 fe ff ff       	call   c01002e4 <cputchar>
            i --;
c01003f1:	ff 4d f4             	decl   -0xc(%ebp)
c01003f4:	eb 29                	jmp    c010041f <readline+0xb2>
        }
        else if (c == '\n' || c == '\r') {
c01003f6:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
c01003fa:	74 06                	je     c0100402 <readline+0x95>
c01003fc:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
c0100400:	75 95                	jne    c0100397 <readline+0x2a>
            cputchar(c);
c0100402:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0100405:	89 04 24             	mov    %eax,(%esp)
c0100408:	e8 d7 fe ff ff       	call   c01002e4 <cputchar>
            buf[i] = '\0';
c010040d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100410:	05 20 c0 11 c0       	add    $0xc011c020,%eax
c0100415:	c6 00 00             	movb   $0x0,(%eax)
            return buf;
c0100418:	b8 20 c0 11 c0       	mov    $0xc011c020,%eax
c010041d:	eb 05                	jmp    c0100424 <readline+0xb7>
        c = getchar();
c010041f:	e9 73 ff ff ff       	jmp    c0100397 <readline+0x2a>
        }
    }
}
c0100424:	c9                   	leave  
c0100425:	c3                   	ret    

c0100426 <__panic>:
/* *
 * __panic - __panic is called on unresolvable fatal errors. it prints
 * "panic: 'message'", and then enters the kernel monitor.
 * */
void
__panic(const char *file, int line, const char *fmt, ...) {
c0100426:	f3 0f 1e fb          	endbr32 
c010042a:	55                   	push   %ebp
c010042b:	89 e5                	mov    %esp,%ebp
c010042d:	83 ec 28             	sub    $0x28,%esp
    if (is_panic) {
c0100430:	a1 20 c4 11 c0       	mov    0xc011c420,%eax
c0100435:	85 c0                	test   %eax,%eax
c0100437:	75 5b                	jne    c0100494 <__panic+0x6e>
        goto panic_dead;
    }
    is_panic = 1;
c0100439:	c7 05 20 c4 11 c0 01 	movl   $0x1,0xc011c420
c0100440:	00 00 00 

    // print the 'message'
    va_list ap;
    va_start(ap, fmt);
c0100443:	8d 45 14             	lea    0x14(%ebp),%eax
c0100446:	89 45 f4             	mov    %eax,-0xc(%ebp)
    cprintf("kernel panic at %s:%d:\n    ", file, line);
c0100449:	8b 45 0c             	mov    0xc(%ebp),%eax
c010044c:	89 44 24 08          	mov    %eax,0x8(%esp)
c0100450:	8b 45 08             	mov    0x8(%ebp),%eax
c0100453:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100457:	c7 04 24 ca 62 10 c0 	movl   $0xc01062ca,(%esp)
c010045e:	e8 57 fe ff ff       	call   c01002ba <cprintf>
    vcprintf(fmt, ap);
c0100463:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100466:	89 44 24 04          	mov    %eax,0x4(%esp)
c010046a:	8b 45 10             	mov    0x10(%ebp),%eax
c010046d:	89 04 24             	mov    %eax,(%esp)
c0100470:	e8 0e fe ff ff       	call   c0100283 <vcprintf>
    cprintf("\n");
c0100475:	c7 04 24 e6 62 10 c0 	movl   $0xc01062e6,(%esp)
c010047c:	e8 39 fe ff ff       	call   c01002ba <cprintf>
    
    cprintf("stack trackback:\n");
c0100481:	c7 04 24 e8 62 10 c0 	movl   $0xc01062e8,(%esp)
c0100488:	e8 2d fe ff ff       	call   c01002ba <cprintf>
    print_stackframe();
c010048d:	e8 3d 06 00 00       	call   c0100acf <print_stackframe>
c0100492:	eb 01                	jmp    c0100495 <__panic+0x6f>
        goto panic_dead;
c0100494:	90                   	nop
    
    va_end(ap);

panic_dead:
    intr_disable();
c0100495:	e8 ef 14 00 00       	call   c0101989 <intr_disable>
    while (1) {
        kmonitor(NULL);
c010049a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
c01004a1:	e8 68 08 00 00       	call   c0100d0e <kmonitor>
c01004a6:	eb f2                	jmp    c010049a <__panic+0x74>

c01004a8 <__warn>:
    }
}

/* __warn - like panic, but don't */
void
__warn(const char *file, int line, const char *fmt, ...) {
c01004a8:	f3 0f 1e fb          	endbr32 
c01004ac:	55                   	push   %ebp
c01004ad:	89 e5                	mov    %esp,%ebp
c01004af:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    va_start(ap, fmt);
c01004b2:	8d 45 14             	lea    0x14(%ebp),%eax
c01004b5:	89 45 f4             	mov    %eax,-0xc(%ebp)
    cprintf("kernel warning at %s:%d:\n    ", file, line);
c01004b8:	8b 45 0c             	mov    0xc(%ebp),%eax
c01004bb:	89 44 24 08          	mov    %eax,0x8(%esp)
c01004bf:	8b 45 08             	mov    0x8(%ebp),%eax
c01004c2:	89 44 24 04          	mov    %eax,0x4(%esp)
c01004c6:	c7 04 24 fa 62 10 c0 	movl   $0xc01062fa,(%esp)
c01004cd:	e8 e8 fd ff ff       	call   c01002ba <cprintf>
    vcprintf(fmt, ap);
c01004d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01004d5:	89 44 24 04          	mov    %eax,0x4(%esp)
c01004d9:	8b 45 10             	mov    0x10(%ebp),%eax
c01004dc:	89 04 24             	mov    %eax,(%esp)
c01004df:	e8 9f fd ff ff       	call   c0100283 <vcprintf>
    cprintf("\n");
c01004e4:	c7 04 24 e6 62 10 c0 	movl   $0xc01062e6,(%esp)
c01004eb:	e8 ca fd ff ff       	call   c01002ba <cprintf>
    va_end(ap);
}
c01004f0:	90                   	nop
c01004f1:	c9                   	leave  
c01004f2:	c3                   	ret    

c01004f3 <is_kernel_panic>:

bool
is_kernel_panic(void) {
c01004f3:	f3 0f 1e fb          	endbr32 
c01004f7:	55                   	push   %ebp
c01004f8:	89 e5                	mov    %esp,%ebp
    return is_panic;
c01004fa:	a1 20 c4 11 c0       	mov    0xc011c420,%eax
}
c01004ff:	5d                   	pop    %ebp
c0100500:	c3                   	ret    

c0100501 <stab_binsearch>:
 *      stab_binsearch(stabs, &left, &right, N_SO, 0xf0100184);
 * will exit setting left = 118, right = 554.
 * */
static void
stab_binsearch(const struct stab *stabs, int *region_left, int *region_right,
           int type, uintptr_t addr) {
c0100501:	f3 0f 1e fb          	endbr32 
c0100505:	55                   	push   %ebp
c0100506:	89 e5                	mov    %esp,%ebp
c0100508:	83 ec 20             	sub    $0x20,%esp
    int l = *region_left, r = *region_right, any_matches = 0;
c010050b:	8b 45 0c             	mov    0xc(%ebp),%eax
c010050e:	8b 00                	mov    (%eax),%eax
c0100510:	89 45 fc             	mov    %eax,-0x4(%ebp)
c0100513:	8b 45 10             	mov    0x10(%ebp),%eax
c0100516:	8b 00                	mov    (%eax),%eax
c0100518:	89 45 f8             	mov    %eax,-0x8(%ebp)
c010051b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

    while (l <= r) {
c0100522:	e9 ca 00 00 00       	jmp    c01005f1 <stab_binsearch+0xf0>
        int true_m = (l + r) / 2, m = true_m;
c0100527:	8b 55 fc             	mov    -0x4(%ebp),%edx
c010052a:	8b 45 f8             	mov    -0x8(%ebp),%eax
c010052d:	01 d0                	add    %edx,%eax
c010052f:	89 c2                	mov    %eax,%edx
c0100531:	c1 ea 1f             	shr    $0x1f,%edx
c0100534:	01 d0                	add    %edx,%eax
c0100536:	d1 f8                	sar    %eax
c0100538:	89 45 ec             	mov    %eax,-0x14(%ebp)
c010053b:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010053e:	89 45 f0             	mov    %eax,-0x10(%ebp)

        // search for earliest stab with right type
        while (m >= l && stabs[m].n_type != type) {
c0100541:	eb 03                	jmp    c0100546 <stab_binsearch+0x45>
            m --;
c0100543:	ff 4d f0             	decl   -0x10(%ebp)
        while (m >= l && stabs[m].n_type != type) {
c0100546:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0100549:	3b 45 fc             	cmp    -0x4(%ebp),%eax
c010054c:	7c 1f                	jl     c010056d <stab_binsearch+0x6c>
c010054e:	8b 55 f0             	mov    -0x10(%ebp),%edx
c0100551:	89 d0                	mov    %edx,%eax
c0100553:	01 c0                	add    %eax,%eax
c0100555:	01 d0                	add    %edx,%eax
c0100557:	c1 e0 02             	shl    $0x2,%eax
c010055a:	89 c2                	mov    %eax,%edx
c010055c:	8b 45 08             	mov    0x8(%ebp),%eax
c010055f:	01 d0                	add    %edx,%eax
c0100561:	0f b6 40 04          	movzbl 0x4(%eax),%eax
c0100565:	0f b6 c0             	movzbl %al,%eax
c0100568:	39 45 14             	cmp    %eax,0x14(%ebp)
c010056b:	75 d6                	jne    c0100543 <stab_binsearch+0x42>
        }
        if (m < l) {    // no match in [l, m]
c010056d:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0100570:	3b 45 fc             	cmp    -0x4(%ebp),%eax
c0100573:	7d 09                	jge    c010057e <stab_binsearch+0x7d>
            l = true_m + 1;
c0100575:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0100578:	40                   	inc    %eax
c0100579:	89 45 fc             	mov    %eax,-0x4(%ebp)
            continue;
c010057c:	eb 73                	jmp    c01005f1 <stab_binsearch+0xf0>
        }

        // actual binary search
        any_matches = 1;
c010057e:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
        if (stabs[m].n_value < addr) {
c0100585:	8b 55 f0             	mov    -0x10(%ebp),%edx
c0100588:	89 d0                	mov    %edx,%eax
c010058a:	01 c0                	add    %eax,%eax
c010058c:	01 d0                	add    %edx,%eax
c010058e:	c1 e0 02             	shl    $0x2,%eax
c0100591:	89 c2                	mov    %eax,%edx
c0100593:	8b 45 08             	mov    0x8(%ebp),%eax
c0100596:	01 d0                	add    %edx,%eax
c0100598:	8b 40 08             	mov    0x8(%eax),%eax
c010059b:	39 45 18             	cmp    %eax,0x18(%ebp)
c010059e:	76 11                	jbe    c01005b1 <stab_binsearch+0xb0>
            *region_left = m;
c01005a0:	8b 45 0c             	mov    0xc(%ebp),%eax
c01005a3:	8b 55 f0             	mov    -0x10(%ebp),%edx
c01005a6:	89 10                	mov    %edx,(%eax)
            l = true_m + 1;
c01005a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01005ab:	40                   	inc    %eax
c01005ac:	89 45 fc             	mov    %eax,-0x4(%ebp)
c01005af:	eb 40                	jmp    c01005f1 <stab_binsearch+0xf0>
        } else if (stabs[m].n_value > addr) {
c01005b1:	8b 55 f0             	mov    -0x10(%ebp),%edx
c01005b4:	89 d0                	mov    %edx,%eax
c01005b6:	01 c0                	add    %eax,%eax
c01005b8:	01 d0                	add    %edx,%eax
c01005ba:	c1 e0 02             	shl    $0x2,%eax
c01005bd:	89 c2                	mov    %eax,%edx
c01005bf:	8b 45 08             	mov    0x8(%ebp),%eax
c01005c2:	01 d0                	add    %edx,%eax
c01005c4:	8b 40 08             	mov    0x8(%eax),%eax
c01005c7:	39 45 18             	cmp    %eax,0x18(%ebp)
c01005ca:	73 14                	jae    c01005e0 <stab_binsearch+0xdf>
            *region_right = m - 1;
c01005cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01005cf:	8d 50 ff             	lea    -0x1(%eax),%edx
c01005d2:	8b 45 10             	mov    0x10(%ebp),%eax
c01005d5:	89 10                	mov    %edx,(%eax)
            r = m - 1;
c01005d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01005da:	48                   	dec    %eax
c01005db:	89 45 f8             	mov    %eax,-0x8(%ebp)
c01005de:	eb 11                	jmp    c01005f1 <stab_binsearch+0xf0>
        } else {
            // exact match for 'addr', but continue loop to find
            // *region_right
            *region_left = m;
c01005e0:	8b 45 0c             	mov    0xc(%ebp),%eax
c01005e3:	8b 55 f0             	mov    -0x10(%ebp),%edx
c01005e6:	89 10                	mov    %edx,(%eax)
            l = m;
c01005e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01005eb:	89 45 fc             	mov    %eax,-0x4(%ebp)
            addr ++;
c01005ee:	ff 45 18             	incl   0x18(%ebp)
    while (l <= r) {
c01005f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01005f4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
c01005f7:	0f 8e 2a ff ff ff    	jle    c0100527 <stab_binsearch+0x26>
        }
    }

    if (!any_matches) {
c01005fd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0100601:	75 0f                	jne    c0100612 <stab_binsearch+0x111>
        *region_right = *region_left - 1;
c0100603:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100606:	8b 00                	mov    (%eax),%eax
c0100608:	8d 50 ff             	lea    -0x1(%eax),%edx
c010060b:	8b 45 10             	mov    0x10(%ebp),%eax
c010060e:	89 10                	mov    %edx,(%eax)
        l = *region_right;
        for (; l > *region_left && stabs[l].n_type != type; l --)
            /* do nothing */;
        *region_left = l;
    }
}
c0100610:	eb 3e                	jmp    c0100650 <stab_binsearch+0x14f>
        l = *region_right;
c0100612:	8b 45 10             	mov    0x10(%ebp),%eax
c0100615:	8b 00                	mov    (%eax),%eax
c0100617:	89 45 fc             	mov    %eax,-0x4(%ebp)
        for (; l > *region_left && stabs[l].n_type != type; l --)
c010061a:	eb 03                	jmp    c010061f <stab_binsearch+0x11e>
c010061c:	ff 4d fc             	decl   -0x4(%ebp)
c010061f:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100622:	8b 00                	mov    (%eax),%eax
c0100624:	39 45 fc             	cmp    %eax,-0x4(%ebp)
c0100627:	7e 1f                	jle    c0100648 <stab_binsearch+0x147>
c0100629:	8b 55 fc             	mov    -0x4(%ebp),%edx
c010062c:	89 d0                	mov    %edx,%eax
c010062e:	01 c0                	add    %eax,%eax
c0100630:	01 d0                	add    %edx,%eax
c0100632:	c1 e0 02             	shl    $0x2,%eax
c0100635:	89 c2                	mov    %eax,%edx
c0100637:	8b 45 08             	mov    0x8(%ebp),%eax
c010063a:	01 d0                	add    %edx,%eax
c010063c:	0f b6 40 04          	movzbl 0x4(%eax),%eax
c0100640:	0f b6 c0             	movzbl %al,%eax
c0100643:	39 45 14             	cmp    %eax,0x14(%ebp)
c0100646:	75 d4                	jne    c010061c <stab_binsearch+0x11b>
        *region_left = l;
c0100648:	8b 45 0c             	mov    0xc(%ebp),%eax
c010064b:	8b 55 fc             	mov    -0x4(%ebp),%edx
c010064e:	89 10                	mov    %edx,(%eax)
}
c0100650:	90                   	nop
c0100651:	c9                   	leave  
c0100652:	c3                   	ret    

c0100653 <debuginfo_eip>:
 * the specified instruction address, @addr.  Returns 0 if information
 * was found, and negative if not.  But even if it returns negative it
 * has stored some information into '*info'.
 * */
int
debuginfo_eip(uintptr_t addr, struct eipdebuginfo *info) {
c0100653:	f3 0f 1e fb          	endbr32 
c0100657:	55                   	push   %ebp
c0100658:	89 e5                	mov    %esp,%ebp
c010065a:	83 ec 58             	sub    $0x58,%esp
    const struct stab *stabs, *stab_end;
    const char *stabstr, *stabstr_end;

    info->eip_file = "<unknown>";
c010065d:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100660:	c7 00 18 63 10 c0    	movl   $0xc0106318,(%eax)
    info->eip_line = 0;
c0100666:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100669:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
    info->eip_fn_name = "<unknown>";
c0100670:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100673:	c7 40 08 18 63 10 c0 	movl   $0xc0106318,0x8(%eax)
    info->eip_fn_namelen = 9;
c010067a:	8b 45 0c             	mov    0xc(%ebp),%eax
c010067d:	c7 40 0c 09 00 00 00 	movl   $0x9,0xc(%eax)
    info->eip_fn_addr = addr;
c0100684:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100687:	8b 55 08             	mov    0x8(%ebp),%edx
c010068a:	89 50 10             	mov    %edx,0x10(%eax)
    info->eip_fn_narg = 0;
c010068d:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100690:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)

    stabs = __STAB_BEGIN__;
c0100697:	c7 45 f4 98 75 10 c0 	movl   $0xc0107598,-0xc(%ebp)
    stab_end = __STAB_END__;
c010069e:	c7 45 f0 88 3f 11 c0 	movl   $0xc0113f88,-0x10(%ebp)
    stabstr = __STABSTR_BEGIN__;
c01006a5:	c7 45 ec 89 3f 11 c0 	movl   $0xc0113f89,-0x14(%ebp)
    stabstr_end = __STABSTR_END__;
c01006ac:	c7 45 e8 a1 6a 11 c0 	movl   $0xc0116aa1,-0x18(%ebp)

    // String table validity checks
    if (stabstr_end <= stabstr || stabstr_end[-1] != 0) {
c01006b3:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01006b6:	3b 45 ec             	cmp    -0x14(%ebp),%eax
c01006b9:	76 0b                	jbe    c01006c6 <debuginfo_eip+0x73>
c01006bb:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01006be:	48                   	dec    %eax
c01006bf:	0f b6 00             	movzbl (%eax),%eax
c01006c2:	84 c0                	test   %al,%al
c01006c4:	74 0a                	je     c01006d0 <debuginfo_eip+0x7d>
        return -1;
c01006c6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
c01006cb:	e9 ab 02 00 00       	jmp    c010097b <debuginfo_eip+0x328>
    // 'eip'.  First, we find the basic source file containing 'eip'.
    // Then, we look in that source file for the function.  Then we look
    // for the line number.

    // Search the entire set of stabs for the source file (type N_SO).
    int lfile = 0, rfile = (stab_end - stabs) - 1;
c01006d0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
c01006d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01006da:	2b 45 f4             	sub    -0xc(%ebp),%eax
c01006dd:	c1 f8 02             	sar    $0x2,%eax
c01006e0:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
c01006e6:	48                   	dec    %eax
c01006e7:	89 45 e0             	mov    %eax,-0x20(%ebp)
    stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
c01006ea:	8b 45 08             	mov    0x8(%ebp),%eax
c01006ed:	89 44 24 10          	mov    %eax,0x10(%esp)
c01006f1:	c7 44 24 0c 64 00 00 	movl   $0x64,0xc(%esp)
c01006f8:	00 
c01006f9:	8d 45 e0             	lea    -0x20(%ebp),%eax
c01006fc:	89 44 24 08          	mov    %eax,0x8(%esp)
c0100700:	8d 45 e4             	lea    -0x1c(%ebp),%eax
c0100703:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100707:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010070a:	89 04 24             	mov    %eax,(%esp)
c010070d:	e8 ef fd ff ff       	call   c0100501 <stab_binsearch>
    if (lfile == 0)
c0100712:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0100715:	85 c0                	test   %eax,%eax
c0100717:	75 0a                	jne    c0100723 <debuginfo_eip+0xd0>
        return -1;
c0100719:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
c010071e:	e9 58 02 00 00       	jmp    c010097b <debuginfo_eip+0x328>

    // Search within that file's stabs for the function definition
    // (N_FUN).
    int lfun = lfile, rfun = rfile;
c0100723:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0100726:	89 45 dc             	mov    %eax,-0x24(%ebp)
c0100729:	8b 45 e0             	mov    -0x20(%ebp),%eax
c010072c:	89 45 d8             	mov    %eax,-0x28(%ebp)
    int lline, rline;
    stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
c010072f:	8b 45 08             	mov    0x8(%ebp),%eax
c0100732:	89 44 24 10          	mov    %eax,0x10(%esp)
c0100736:	c7 44 24 0c 24 00 00 	movl   $0x24,0xc(%esp)
c010073d:	00 
c010073e:	8d 45 d8             	lea    -0x28(%ebp),%eax
c0100741:	89 44 24 08          	mov    %eax,0x8(%esp)
c0100745:	8d 45 dc             	lea    -0x24(%ebp),%eax
c0100748:	89 44 24 04          	mov    %eax,0x4(%esp)
c010074c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010074f:	89 04 24             	mov    %eax,(%esp)
c0100752:	e8 aa fd ff ff       	call   c0100501 <stab_binsearch>

    if (lfun <= rfun) {
c0100757:	8b 55 dc             	mov    -0x24(%ebp),%edx
c010075a:	8b 45 d8             	mov    -0x28(%ebp),%eax
c010075d:	39 c2                	cmp    %eax,%edx
c010075f:	7f 78                	jg     c01007d9 <debuginfo_eip+0x186>
        // stabs[lfun] points to the function name
        // in the string table, but check bounds just in case.
        if (stabs[lfun].n_strx < stabstr_end - stabstr) {
c0100761:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0100764:	89 c2                	mov    %eax,%edx
c0100766:	89 d0                	mov    %edx,%eax
c0100768:	01 c0                	add    %eax,%eax
c010076a:	01 d0                	add    %edx,%eax
c010076c:	c1 e0 02             	shl    $0x2,%eax
c010076f:	89 c2                	mov    %eax,%edx
c0100771:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100774:	01 d0                	add    %edx,%eax
c0100776:	8b 10                	mov    (%eax),%edx
c0100778:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010077b:	2b 45 ec             	sub    -0x14(%ebp),%eax
c010077e:	39 c2                	cmp    %eax,%edx
c0100780:	73 22                	jae    c01007a4 <debuginfo_eip+0x151>
            info->eip_fn_name = stabstr + stabs[lfun].n_strx;
c0100782:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0100785:	89 c2                	mov    %eax,%edx
c0100787:	89 d0                	mov    %edx,%eax
c0100789:	01 c0                	add    %eax,%eax
c010078b:	01 d0                	add    %edx,%eax
c010078d:	c1 e0 02             	shl    $0x2,%eax
c0100790:	89 c2                	mov    %eax,%edx
c0100792:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100795:	01 d0                	add    %edx,%eax
c0100797:	8b 10                	mov    (%eax),%edx
c0100799:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010079c:	01 c2                	add    %eax,%edx
c010079e:	8b 45 0c             	mov    0xc(%ebp),%eax
c01007a1:	89 50 08             	mov    %edx,0x8(%eax)
        }
        info->eip_fn_addr = stabs[lfun].n_value;
c01007a4:	8b 45 dc             	mov    -0x24(%ebp),%eax
c01007a7:	89 c2                	mov    %eax,%edx
c01007a9:	89 d0                	mov    %edx,%eax
c01007ab:	01 c0                	add    %eax,%eax
c01007ad:	01 d0                	add    %edx,%eax
c01007af:	c1 e0 02             	shl    $0x2,%eax
c01007b2:	89 c2                	mov    %eax,%edx
c01007b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01007b7:	01 d0                	add    %edx,%eax
c01007b9:	8b 50 08             	mov    0x8(%eax),%edx
c01007bc:	8b 45 0c             	mov    0xc(%ebp),%eax
c01007bf:	89 50 10             	mov    %edx,0x10(%eax)
        addr -= info->eip_fn_addr;
c01007c2:	8b 45 0c             	mov    0xc(%ebp),%eax
c01007c5:	8b 40 10             	mov    0x10(%eax),%eax
c01007c8:	29 45 08             	sub    %eax,0x8(%ebp)
        // Search within the function definition for the line number.
        lline = lfun;
c01007cb:	8b 45 dc             	mov    -0x24(%ebp),%eax
c01007ce:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        rline = rfun;
c01007d1:	8b 45 d8             	mov    -0x28(%ebp),%eax
c01007d4:	89 45 d0             	mov    %eax,-0x30(%ebp)
c01007d7:	eb 15                	jmp    c01007ee <debuginfo_eip+0x19b>
    } else {
        // Couldn't find function stab!  Maybe we're in an assembly
        // file.  Search the whole file for the line number.
        info->eip_fn_addr = addr;
c01007d9:	8b 45 0c             	mov    0xc(%ebp),%eax
c01007dc:	8b 55 08             	mov    0x8(%ebp),%edx
c01007df:	89 50 10             	mov    %edx,0x10(%eax)
        lline = lfile;
c01007e2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c01007e5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        rline = rfile;
c01007e8:	8b 45 e0             	mov    -0x20(%ebp),%eax
c01007eb:	89 45 d0             	mov    %eax,-0x30(%ebp)
    }
    info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
c01007ee:	8b 45 0c             	mov    0xc(%ebp),%eax
c01007f1:	8b 40 08             	mov    0x8(%eax),%eax
c01007f4:	c7 44 24 04 3a 00 00 	movl   $0x3a,0x4(%esp)
c01007fb:	00 
c01007fc:	89 04 24             	mov    %eax,(%esp)
c01007ff:	e8 67 50 00 00       	call   c010586b <strfind>
c0100804:	8b 55 0c             	mov    0xc(%ebp),%edx
c0100807:	8b 52 08             	mov    0x8(%edx),%edx
c010080a:	29 d0                	sub    %edx,%eax
c010080c:	89 c2                	mov    %eax,%edx
c010080e:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100811:	89 50 0c             	mov    %edx,0xc(%eax)

    // Search within [lline, rline] for the line number stab.
    // If found, set info->eip_line to the right line number.
    // If not found, return -1.
    stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
c0100814:	8b 45 08             	mov    0x8(%ebp),%eax
c0100817:	89 44 24 10          	mov    %eax,0x10(%esp)
c010081b:	c7 44 24 0c 44 00 00 	movl   $0x44,0xc(%esp)
c0100822:	00 
c0100823:	8d 45 d0             	lea    -0x30(%ebp),%eax
c0100826:	89 44 24 08          	mov    %eax,0x8(%esp)
c010082a:	8d 45 d4             	lea    -0x2c(%ebp),%eax
c010082d:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100831:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100834:	89 04 24             	mov    %eax,(%esp)
c0100837:	e8 c5 fc ff ff       	call   c0100501 <stab_binsearch>
    if (lline <= rline) {
c010083c:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c010083f:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0100842:	39 c2                	cmp    %eax,%edx
c0100844:	7f 23                	jg     c0100869 <debuginfo_eip+0x216>
        info->eip_line = stabs[rline].n_desc;
c0100846:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0100849:	89 c2                	mov    %eax,%edx
c010084b:	89 d0                	mov    %edx,%eax
c010084d:	01 c0                	add    %eax,%eax
c010084f:	01 d0                	add    %edx,%eax
c0100851:	c1 e0 02             	shl    $0x2,%eax
c0100854:	89 c2                	mov    %eax,%edx
c0100856:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100859:	01 d0                	add    %edx,%eax
c010085b:	0f b7 40 06          	movzwl 0x6(%eax),%eax
c010085f:	89 c2                	mov    %eax,%edx
c0100861:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100864:	89 50 04             	mov    %edx,0x4(%eax)

    // Search backwards from the line number for the relevant filename stab.
    // We can't just use the "lfile" stab because inlined functions
    // can interpolate code from a different file!
    // Such included source files use the N_SOL stab type.
    while (lline >= lfile
c0100867:	eb 11                	jmp    c010087a <debuginfo_eip+0x227>
        return -1;
c0100869:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
c010086e:	e9 08 01 00 00       	jmp    c010097b <debuginfo_eip+0x328>
           && stabs[lline].n_type != N_SOL
           && (stabs[lline].n_type != N_SO || !stabs[lline].n_value)) {
        lline --;
c0100873:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0100876:	48                   	dec    %eax
c0100877:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    while (lline >= lfile
c010087a:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c010087d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0100880:	39 c2                	cmp    %eax,%edx
c0100882:	7c 56                	jl     c01008da <debuginfo_eip+0x287>
           && stabs[lline].n_type != N_SOL
c0100884:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0100887:	89 c2                	mov    %eax,%edx
c0100889:	89 d0                	mov    %edx,%eax
c010088b:	01 c0                	add    %eax,%eax
c010088d:	01 d0                	add    %edx,%eax
c010088f:	c1 e0 02             	shl    $0x2,%eax
c0100892:	89 c2                	mov    %eax,%edx
c0100894:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100897:	01 d0                	add    %edx,%eax
c0100899:	0f b6 40 04          	movzbl 0x4(%eax),%eax
c010089d:	3c 84                	cmp    $0x84,%al
c010089f:	74 39                	je     c01008da <debuginfo_eip+0x287>
           && (stabs[lline].n_type != N_SO || !stabs[lline].n_value)) {
c01008a1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01008a4:	89 c2                	mov    %eax,%edx
c01008a6:	89 d0                	mov    %edx,%eax
c01008a8:	01 c0                	add    %eax,%eax
c01008aa:	01 d0                	add    %edx,%eax
c01008ac:	c1 e0 02             	shl    $0x2,%eax
c01008af:	89 c2                	mov    %eax,%edx
c01008b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01008b4:	01 d0                	add    %edx,%eax
c01008b6:	0f b6 40 04          	movzbl 0x4(%eax),%eax
c01008ba:	3c 64                	cmp    $0x64,%al
c01008bc:	75 b5                	jne    c0100873 <debuginfo_eip+0x220>
c01008be:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01008c1:	89 c2                	mov    %eax,%edx
c01008c3:	89 d0                	mov    %edx,%eax
c01008c5:	01 c0                	add    %eax,%eax
c01008c7:	01 d0                	add    %edx,%eax
c01008c9:	c1 e0 02             	shl    $0x2,%eax
c01008cc:	89 c2                	mov    %eax,%edx
c01008ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01008d1:	01 d0                	add    %edx,%eax
c01008d3:	8b 40 08             	mov    0x8(%eax),%eax
c01008d6:	85 c0                	test   %eax,%eax
c01008d8:	74 99                	je     c0100873 <debuginfo_eip+0x220>
    }
    if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr) {
c01008da:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c01008dd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c01008e0:	39 c2                	cmp    %eax,%edx
c01008e2:	7c 42                	jl     c0100926 <debuginfo_eip+0x2d3>
c01008e4:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01008e7:	89 c2                	mov    %eax,%edx
c01008e9:	89 d0                	mov    %edx,%eax
c01008eb:	01 c0                	add    %eax,%eax
c01008ed:	01 d0                	add    %edx,%eax
c01008ef:	c1 e0 02             	shl    $0x2,%eax
c01008f2:	89 c2                	mov    %eax,%edx
c01008f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01008f7:	01 d0                	add    %edx,%eax
c01008f9:	8b 10                	mov    (%eax),%edx
c01008fb:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01008fe:	2b 45 ec             	sub    -0x14(%ebp),%eax
c0100901:	39 c2                	cmp    %eax,%edx
c0100903:	73 21                	jae    c0100926 <debuginfo_eip+0x2d3>
        info->eip_file = stabstr + stabs[lline].n_strx;
c0100905:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0100908:	89 c2                	mov    %eax,%edx
c010090a:	89 d0                	mov    %edx,%eax
c010090c:	01 c0                	add    %eax,%eax
c010090e:	01 d0                	add    %edx,%eax
c0100910:	c1 e0 02             	shl    $0x2,%eax
c0100913:	89 c2                	mov    %eax,%edx
c0100915:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100918:	01 d0                	add    %edx,%eax
c010091a:	8b 10                	mov    (%eax),%edx
c010091c:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010091f:	01 c2                	add    %eax,%edx
c0100921:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100924:	89 10                	mov    %edx,(%eax)
    }

    // Set eip_fn_narg to the number of arguments taken by the function,
    // or 0 if there was no containing function.
    if (lfun < rfun) {
c0100926:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0100929:	8b 45 d8             	mov    -0x28(%ebp),%eax
c010092c:	39 c2                	cmp    %eax,%edx
c010092e:	7d 46                	jge    c0100976 <debuginfo_eip+0x323>
        for (lline = lfun + 1;
c0100930:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0100933:	40                   	inc    %eax
c0100934:	89 45 d4             	mov    %eax,-0x2c(%ebp)
c0100937:	eb 16                	jmp    c010094f <debuginfo_eip+0x2fc>
             lline < rfun && stabs[lline].n_type == N_PSYM;
             lline ++) {
            info->eip_fn_narg ++;
c0100939:	8b 45 0c             	mov    0xc(%ebp),%eax
c010093c:	8b 40 14             	mov    0x14(%eax),%eax
c010093f:	8d 50 01             	lea    0x1(%eax),%edx
c0100942:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100945:	89 50 14             	mov    %edx,0x14(%eax)
             lline ++) {
c0100948:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c010094b:	40                   	inc    %eax
c010094c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
             lline < rfun && stabs[lline].n_type == N_PSYM;
c010094f:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c0100952:	8b 45 d8             	mov    -0x28(%ebp),%eax
        for (lline = lfun + 1;
c0100955:	39 c2                	cmp    %eax,%edx
c0100957:	7d 1d                	jge    c0100976 <debuginfo_eip+0x323>
             lline < rfun && stabs[lline].n_type == N_PSYM;
c0100959:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c010095c:	89 c2                	mov    %eax,%edx
c010095e:	89 d0                	mov    %edx,%eax
c0100960:	01 c0                	add    %eax,%eax
c0100962:	01 d0                	add    %edx,%eax
c0100964:	c1 e0 02             	shl    $0x2,%eax
c0100967:	89 c2                	mov    %eax,%edx
c0100969:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010096c:	01 d0                	add    %edx,%eax
c010096e:	0f b6 40 04          	movzbl 0x4(%eax),%eax
c0100972:	3c a0                	cmp    $0xa0,%al
c0100974:	74 c3                	je     c0100939 <debuginfo_eip+0x2e6>
        }
    }
    return 0;
c0100976:	b8 00 00 00 00       	mov    $0x0,%eax
}
c010097b:	c9                   	leave  
c010097c:	c3                   	ret    

c010097d <print_kerninfo>:
 * print_kerninfo - print the information about kernel, including the location
 * of kernel entry, the start addresses of data and text segements, the start
 * address of free memory and how many memory that kernel has used.
 * */
void
print_kerninfo(void) {
c010097d:	f3 0f 1e fb          	endbr32 
c0100981:	55                   	push   %ebp
c0100982:	89 e5                	mov    %esp,%ebp
c0100984:	83 ec 18             	sub    $0x18,%esp
    extern char etext[], edata[], end[], kern_init[];
    cprintf("Special kernel symbols:\n");
c0100987:	c7 04 24 22 63 10 c0 	movl   $0xc0106322,(%esp)
c010098e:	e8 27 f9 ff ff       	call   c01002ba <cprintf>
    cprintf("  entry  0x%08x (phys)\n", kern_init);
c0100993:	c7 44 24 04 36 00 10 	movl   $0xc0100036,0x4(%esp)
c010099a:	c0 
c010099b:	c7 04 24 3b 63 10 c0 	movl   $0xc010633b,(%esp)
c01009a2:	e8 13 f9 ff ff       	call   c01002ba <cprintf>
    cprintf("  etext  0x%08x (phys)\n", etext);
c01009a7:	c7 44 24 04 1b 62 10 	movl   $0xc010621b,0x4(%esp)
c01009ae:	c0 
c01009af:	c7 04 24 53 63 10 c0 	movl   $0xc0106353,(%esp)
c01009b6:	e8 ff f8 ff ff       	call   c01002ba <cprintf>
    cprintf("  edata  0x%08x (phys)\n", edata);
c01009bb:	c7 44 24 04 00 c0 11 	movl   $0xc011c000,0x4(%esp)
c01009c2:	c0 
c01009c3:	c7 04 24 6b 63 10 c0 	movl   $0xc010636b,(%esp)
c01009ca:	e8 eb f8 ff ff       	call   c01002ba <cprintf>
    cprintf("  end    0x%08x (phys)\n", end);
c01009cf:	c7 44 24 04 28 cf 11 	movl   $0xc011cf28,0x4(%esp)
c01009d6:	c0 
c01009d7:	c7 04 24 83 63 10 c0 	movl   $0xc0106383,(%esp)
c01009de:	e8 d7 f8 ff ff       	call   c01002ba <cprintf>
    cprintf("Kernel executable memory footprint: %dKB\n", (end - kern_init + 1023)/1024);
c01009e3:	b8 28 cf 11 c0       	mov    $0xc011cf28,%eax
c01009e8:	2d 36 00 10 c0       	sub    $0xc0100036,%eax
c01009ed:	05 ff 03 00 00       	add    $0x3ff,%eax
c01009f2:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
c01009f8:	85 c0                	test   %eax,%eax
c01009fa:	0f 48 c2             	cmovs  %edx,%eax
c01009fd:	c1 f8 0a             	sar    $0xa,%eax
c0100a00:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100a04:	c7 04 24 9c 63 10 c0 	movl   $0xc010639c,(%esp)
c0100a0b:	e8 aa f8 ff ff       	call   c01002ba <cprintf>
}
c0100a10:	90                   	nop
c0100a11:	c9                   	leave  
c0100a12:	c3                   	ret    

c0100a13 <print_debuginfo>:
/* *
 * print_debuginfo - read and print the stat information for the address @eip,
 * and info.eip_fn_addr should be the first address of the related function.
 * */
void
print_debuginfo(uintptr_t eip) {
c0100a13:	f3 0f 1e fb          	endbr32 
c0100a17:	55                   	push   %ebp
c0100a18:	89 e5                	mov    %esp,%ebp
c0100a1a:	81 ec 48 01 00 00    	sub    $0x148,%esp
    struct eipdebuginfo info;
    if (debuginfo_eip(eip, &info) != 0) {
c0100a20:	8d 45 dc             	lea    -0x24(%ebp),%eax
c0100a23:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100a27:	8b 45 08             	mov    0x8(%ebp),%eax
c0100a2a:	89 04 24             	mov    %eax,(%esp)
c0100a2d:	e8 21 fc ff ff       	call   c0100653 <debuginfo_eip>
c0100a32:	85 c0                	test   %eax,%eax
c0100a34:	74 15                	je     c0100a4b <print_debuginfo+0x38>
        cprintf("    <unknow>: -- 0x%08x --\n", eip);
c0100a36:	8b 45 08             	mov    0x8(%ebp),%eax
c0100a39:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100a3d:	c7 04 24 c6 63 10 c0 	movl   $0xc01063c6,(%esp)
c0100a44:	e8 71 f8 ff ff       	call   c01002ba <cprintf>
        }
        fnname[j] = '\0';
        cprintf("    %s:%d: %s+%d\n", info.eip_file, info.eip_line,
                fnname, eip - info.eip_fn_addr);
    }
}
c0100a49:	eb 6c                	jmp    c0100ab7 <print_debuginfo+0xa4>
        for (j = 0; j < info.eip_fn_namelen; j ++) {
c0100a4b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0100a52:	eb 1b                	jmp    c0100a6f <print_debuginfo+0x5c>
            fnname[j] = info.eip_fn_name[j];
c0100a54:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0100a57:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100a5a:	01 d0                	add    %edx,%eax
c0100a5c:	0f b6 10             	movzbl (%eax),%edx
c0100a5f:	8d 8d dc fe ff ff    	lea    -0x124(%ebp),%ecx
c0100a65:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100a68:	01 c8                	add    %ecx,%eax
c0100a6a:	88 10                	mov    %dl,(%eax)
        for (j = 0; j < info.eip_fn_namelen; j ++) {
c0100a6c:	ff 45 f4             	incl   -0xc(%ebp)
c0100a6f:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0100a72:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c0100a75:	7c dd                	jl     c0100a54 <print_debuginfo+0x41>
        fnname[j] = '\0';
c0100a77:	8d 95 dc fe ff ff    	lea    -0x124(%ebp),%edx
c0100a7d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100a80:	01 d0                	add    %edx,%eax
c0100a82:	c6 00 00             	movb   $0x0,(%eax)
                fnname, eip - info.eip_fn_addr);
c0100a85:	8b 45 ec             	mov    -0x14(%ebp),%eax
        cprintf("    %s:%d: %s+%d\n", info.eip_file, info.eip_line,
c0100a88:	8b 55 08             	mov    0x8(%ebp),%edx
c0100a8b:	89 d1                	mov    %edx,%ecx
c0100a8d:	29 c1                	sub    %eax,%ecx
c0100a8f:	8b 55 e0             	mov    -0x20(%ebp),%edx
c0100a92:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0100a95:	89 4c 24 10          	mov    %ecx,0x10(%esp)
c0100a99:	8d 8d dc fe ff ff    	lea    -0x124(%ebp),%ecx
c0100a9f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
c0100aa3:	89 54 24 08          	mov    %edx,0x8(%esp)
c0100aa7:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100aab:	c7 04 24 e2 63 10 c0 	movl   $0xc01063e2,(%esp)
c0100ab2:	e8 03 f8 ff ff       	call   c01002ba <cprintf>
}
c0100ab7:	90                   	nop
c0100ab8:	c9                   	leave  
c0100ab9:	c3                   	ret    

c0100aba <read_eip>:

static __noinline uint32_t
read_eip(void) {
c0100aba:	f3 0f 1e fb          	endbr32 
c0100abe:	55                   	push   %ebp
c0100abf:	89 e5                	mov    %esp,%ebp
c0100ac1:	83 ec 10             	sub    $0x10,%esp
    uint32_t eip;
    asm volatile("movl 4(%%ebp), %0" : "=r" (eip));
c0100ac4:	8b 45 04             	mov    0x4(%ebp),%eax
c0100ac7:	89 45 fc             	mov    %eax,-0x4(%ebp)
    return eip;
c0100aca:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
c0100acd:	c9                   	leave  
c0100ace:	c3                   	ret    

c0100acf <print_stackframe>:
 * calling-chain. Finally print_stackframe() will trace and print them for debugging.
 *
 * Note that, the length of ebp-chain is limited. In boot/bootasm.S, before jumping
 * to the kernel entry, the value of ebp has been set to zero, that's the boundary.
 * */
void print_stackframe(void) {
c0100acf:	f3 0f 1e fb          	endbr32 
c0100ad3:	55                   	push   %ebp
c0100ad4:	89 e5                	mov    %esp,%ebp
c0100ad6:	53                   	push   %ebx
c0100ad7:	83 ec 44             	sub    $0x44,%esp
}

static inline uint32_t
read_ebp(void) {
    uint32_t ebp;
    asm volatile ("movl %%ebp, %0" : "=r" (ebp));
c0100ada:	89 e8                	mov    %ebp,%eax
c0100adc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return ebp;
c0100adf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
      *    (3.4) call print_debuginfo(eip-1) to print the C calling function name and line number, etc.
      *    (3.5) popup a calling stackframe
      *           NOTICE: the calling funciton's return addr eip  = ss:[ebp+4]
      *                   the calling funciton's ebp = ss:[ebp]
      */
      uint32_t ebp=read_ebp();
c0100ae2:	89 45 f4             	mov    %eax,-0xc(%ebp)
      uint32_t eip=read_eip();
c0100ae5:	e8 d0 ff ff ff       	call   c0100aba <read_eip>
c0100aea:	89 45 f0             	mov    %eax,-0x10(%ebp)
      for(int i=0;i<STACKFRAME_DEPTH&&ebp!=0;i++)
c0100aed:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
c0100af4:	e9 8a 00 00 00       	jmp    c0100b83 <print_stackframe+0xb4>
      {
      cprintf(" ebp:0x%08x eip:0x%08x",ebp,eip);
c0100af9:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0100afc:	89 44 24 08          	mov    %eax,0x8(%esp)
c0100b00:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100b03:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100b07:	c7 04 24 f4 63 10 c0 	movl   $0xc01063f4,(%esp)
c0100b0e:	e8 a7 f7 ff ff       	call   c01002ba <cprintf>
      uint32_t *arg=(uint32_t *)ebp+2;
c0100b13:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100b16:	83 c0 08             	add    $0x8,%eax
c0100b19:	89 45 e8             	mov    %eax,-0x18(%ebp)
      cprintf(" args:0x%08x 0x%08x 0x%08x 0x%08x",*(arg+0),*(arg+1),*(arg+2),*(arg+3));
c0100b1c:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0100b1f:	83 c0 0c             	add    $0xc,%eax
c0100b22:	8b 18                	mov    (%eax),%ebx
c0100b24:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0100b27:	83 c0 08             	add    $0x8,%eax
c0100b2a:	8b 08                	mov    (%eax),%ecx
c0100b2c:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0100b2f:	83 c0 04             	add    $0x4,%eax
c0100b32:	8b 10                	mov    (%eax),%edx
c0100b34:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0100b37:	8b 00                	mov    (%eax),%eax
c0100b39:	89 5c 24 10          	mov    %ebx,0x10(%esp)
c0100b3d:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
c0100b41:	89 54 24 08          	mov    %edx,0x8(%esp)
c0100b45:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100b49:	c7 04 24 0c 64 10 c0 	movl   $0xc010640c,(%esp)
c0100b50:	e8 65 f7 ff ff       	call   c01002ba <cprintf>
      cprintf("\n");
c0100b55:	c7 04 24 2e 64 10 c0 	movl   $0xc010642e,(%esp)
c0100b5c:	e8 59 f7 ff ff       	call   c01002ba <cprintf>
      print_debuginfo(eip-1);
c0100b61:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0100b64:	48                   	dec    %eax
c0100b65:	89 04 24             	mov    %eax,(%esp)
c0100b68:	e8 a6 fe ff ff       	call   c0100a13 <print_debuginfo>
      eip=((uint32_t *)ebp)[1];
c0100b6d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100b70:	83 c0 04             	add    $0x4,%eax
c0100b73:	8b 00                	mov    (%eax),%eax
c0100b75:	89 45 f0             	mov    %eax,-0x10(%ebp)
      ebp=((uint32_t *)ebp)[0];
c0100b78:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100b7b:	8b 00                	mov    (%eax),%eax
c0100b7d:	89 45 f4             	mov    %eax,-0xc(%ebp)
      for(int i=0;i<STACKFRAME_DEPTH&&ebp!=0;i++)
c0100b80:	ff 45 ec             	incl   -0x14(%ebp)
c0100b83:	83 7d ec 13          	cmpl   $0x13,-0x14(%ebp)
c0100b87:	7f 0a                	jg     c0100b93 <print_stackframe+0xc4>
c0100b89:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0100b8d:	0f 85 66 ff ff ff    	jne    c0100af9 <print_stackframe+0x2a>
      }
}
c0100b93:	90                   	nop
c0100b94:	83 c4 44             	add    $0x44,%esp
c0100b97:	5b                   	pop    %ebx
c0100b98:	5d                   	pop    %ebp
c0100b99:	c3                   	ret    

c0100b9a <parse>:
#define MAXARGS         16
#define WHITESPACE      " \t\n\r"

/* parse - parse the command buffer into whitespace-separated arguments */
static int
parse(char *buf, char **argv) {
c0100b9a:	f3 0f 1e fb          	endbr32 
c0100b9e:	55                   	push   %ebp
c0100b9f:	89 e5                	mov    %esp,%ebp
c0100ba1:	83 ec 28             	sub    $0x28,%esp
    int argc = 0;
c0100ba4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while (1) {
        // find global whitespace
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
c0100bab:	eb 0c                	jmp    c0100bb9 <parse+0x1f>
            *buf ++ = '\0';
c0100bad:	8b 45 08             	mov    0x8(%ebp),%eax
c0100bb0:	8d 50 01             	lea    0x1(%eax),%edx
c0100bb3:	89 55 08             	mov    %edx,0x8(%ebp)
c0100bb6:	c6 00 00             	movb   $0x0,(%eax)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
c0100bb9:	8b 45 08             	mov    0x8(%ebp),%eax
c0100bbc:	0f b6 00             	movzbl (%eax),%eax
c0100bbf:	84 c0                	test   %al,%al
c0100bc1:	74 1d                	je     c0100be0 <parse+0x46>
c0100bc3:	8b 45 08             	mov    0x8(%ebp),%eax
c0100bc6:	0f b6 00             	movzbl (%eax),%eax
c0100bc9:	0f be c0             	movsbl %al,%eax
c0100bcc:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100bd0:	c7 04 24 b0 64 10 c0 	movl   $0xc01064b0,(%esp)
c0100bd7:	e8 59 4c 00 00       	call   c0105835 <strchr>
c0100bdc:	85 c0                	test   %eax,%eax
c0100bde:	75 cd                	jne    c0100bad <parse+0x13>
        }
        if (*buf == '\0') {
c0100be0:	8b 45 08             	mov    0x8(%ebp),%eax
c0100be3:	0f b6 00             	movzbl (%eax),%eax
c0100be6:	84 c0                	test   %al,%al
c0100be8:	74 65                	je     c0100c4f <parse+0xb5>
            break;
        }

        // save and scan past next arg
        if (argc == MAXARGS - 1) {
c0100bea:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
c0100bee:	75 14                	jne    c0100c04 <parse+0x6a>
            cprintf("Too many arguments (max %d).\n", MAXARGS);
c0100bf0:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
c0100bf7:	00 
c0100bf8:	c7 04 24 b5 64 10 c0 	movl   $0xc01064b5,(%esp)
c0100bff:	e8 b6 f6 ff ff       	call   c01002ba <cprintf>
        }
        argv[argc ++] = buf;
c0100c04:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100c07:	8d 50 01             	lea    0x1(%eax),%edx
c0100c0a:	89 55 f4             	mov    %edx,-0xc(%ebp)
c0100c0d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c0100c14:	8b 45 0c             	mov    0xc(%ebp),%eax
c0100c17:	01 c2                	add    %eax,%edx
c0100c19:	8b 45 08             	mov    0x8(%ebp),%eax
c0100c1c:	89 02                	mov    %eax,(%edx)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) == NULL) {
c0100c1e:	eb 03                	jmp    c0100c23 <parse+0x89>
            buf ++;
c0100c20:	ff 45 08             	incl   0x8(%ebp)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) == NULL) {
c0100c23:	8b 45 08             	mov    0x8(%ebp),%eax
c0100c26:	0f b6 00             	movzbl (%eax),%eax
c0100c29:	84 c0                	test   %al,%al
c0100c2b:	74 8c                	je     c0100bb9 <parse+0x1f>
c0100c2d:	8b 45 08             	mov    0x8(%ebp),%eax
c0100c30:	0f b6 00             	movzbl (%eax),%eax
c0100c33:	0f be c0             	movsbl %al,%eax
c0100c36:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100c3a:	c7 04 24 b0 64 10 c0 	movl   $0xc01064b0,(%esp)
c0100c41:	e8 ef 4b 00 00       	call   c0105835 <strchr>
c0100c46:	85 c0                	test   %eax,%eax
c0100c48:	74 d6                	je     c0100c20 <parse+0x86>
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
c0100c4a:	e9 6a ff ff ff       	jmp    c0100bb9 <parse+0x1f>
            break;
c0100c4f:	90                   	nop
        }
    }
    return argc;
c0100c50:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c0100c53:	c9                   	leave  
c0100c54:	c3                   	ret    

c0100c55 <runcmd>:
/* *
 * runcmd - parse the input string, split it into separated arguments
 * and then lookup and invoke some related commands/
 * */
static int
runcmd(char *buf, struct trapframe *tf) {
c0100c55:	f3 0f 1e fb          	endbr32 
c0100c59:	55                   	push   %ebp
c0100c5a:	89 e5                	mov    %esp,%ebp
c0100c5c:	53                   	push   %ebx
c0100c5d:	83 ec 64             	sub    $0x64,%esp
    char *argv[MAXARGS];
    int argc = parse(buf, argv);
c0100c60:	8d 45 b0             	lea    -0x50(%ebp),%eax
c0100c63:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100c67:	8b 45 08             	mov    0x8(%ebp),%eax
c0100c6a:	89 04 24             	mov    %eax,(%esp)
c0100c6d:	e8 28 ff ff ff       	call   c0100b9a <parse>
c0100c72:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if (argc == 0) {
c0100c75:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0100c79:	75 0a                	jne    c0100c85 <runcmd+0x30>
        return 0;
c0100c7b:	b8 00 00 00 00       	mov    $0x0,%eax
c0100c80:	e9 83 00 00 00       	jmp    c0100d08 <runcmd+0xb3>
    }
    int i;
    for (i = 0; i < NCOMMANDS; i ++) {
c0100c85:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0100c8c:	eb 5a                	jmp    c0100ce8 <runcmd+0x93>
        if (strcmp(commands[i].name, argv[0]) == 0) {
c0100c8e:	8b 4d b0             	mov    -0x50(%ebp),%ecx
c0100c91:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0100c94:	89 d0                	mov    %edx,%eax
c0100c96:	01 c0                	add    %eax,%eax
c0100c98:	01 d0                	add    %edx,%eax
c0100c9a:	c1 e0 02             	shl    $0x2,%eax
c0100c9d:	05 00 90 11 c0       	add    $0xc0119000,%eax
c0100ca2:	8b 00                	mov    (%eax),%eax
c0100ca4:	89 4c 24 04          	mov    %ecx,0x4(%esp)
c0100ca8:	89 04 24             	mov    %eax,(%esp)
c0100cab:	e8 e1 4a 00 00       	call   c0105791 <strcmp>
c0100cb0:	85 c0                	test   %eax,%eax
c0100cb2:	75 31                	jne    c0100ce5 <runcmd+0x90>
            return commands[i].func(argc - 1, argv + 1, tf);
c0100cb4:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0100cb7:	89 d0                	mov    %edx,%eax
c0100cb9:	01 c0                	add    %eax,%eax
c0100cbb:	01 d0                	add    %edx,%eax
c0100cbd:	c1 e0 02             	shl    $0x2,%eax
c0100cc0:	05 08 90 11 c0       	add    $0xc0119008,%eax
c0100cc5:	8b 10                	mov    (%eax),%edx
c0100cc7:	8d 45 b0             	lea    -0x50(%ebp),%eax
c0100cca:	83 c0 04             	add    $0x4,%eax
c0100ccd:	8b 4d f0             	mov    -0x10(%ebp),%ecx
c0100cd0:	8d 59 ff             	lea    -0x1(%ecx),%ebx
c0100cd3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
c0100cd6:	89 4c 24 08          	mov    %ecx,0x8(%esp)
c0100cda:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100cde:	89 1c 24             	mov    %ebx,(%esp)
c0100ce1:	ff d2                	call   *%edx
c0100ce3:	eb 23                	jmp    c0100d08 <runcmd+0xb3>
    for (i = 0; i < NCOMMANDS; i ++) {
c0100ce5:	ff 45 f4             	incl   -0xc(%ebp)
c0100ce8:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100ceb:	83 f8 02             	cmp    $0x2,%eax
c0100cee:	76 9e                	jbe    c0100c8e <runcmd+0x39>
        }
    }
    cprintf("Unknown command '%s'\n", argv[0]);
c0100cf0:	8b 45 b0             	mov    -0x50(%ebp),%eax
c0100cf3:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100cf7:	c7 04 24 d3 64 10 c0 	movl   $0xc01064d3,(%esp)
c0100cfe:	e8 b7 f5 ff ff       	call   c01002ba <cprintf>
    return 0;
c0100d03:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0100d08:	83 c4 64             	add    $0x64,%esp
c0100d0b:	5b                   	pop    %ebx
c0100d0c:	5d                   	pop    %ebp
c0100d0d:	c3                   	ret    

c0100d0e <kmonitor>:

/***** Implementations of basic kernel monitor commands *****/

void
kmonitor(struct trapframe *tf) {
c0100d0e:	f3 0f 1e fb          	endbr32 
c0100d12:	55                   	push   %ebp
c0100d13:	89 e5                	mov    %esp,%ebp
c0100d15:	83 ec 28             	sub    $0x28,%esp
    cprintf("Welcome to the kernel debug monitor!!\n");
c0100d18:	c7 04 24 ec 64 10 c0 	movl   $0xc01064ec,(%esp)
c0100d1f:	e8 96 f5 ff ff       	call   c01002ba <cprintf>
    cprintf("Type 'help' for a list of commands.\n");
c0100d24:	c7 04 24 14 65 10 c0 	movl   $0xc0106514,(%esp)
c0100d2b:	e8 8a f5 ff ff       	call   c01002ba <cprintf>

    if (tf != NULL) {
c0100d30:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c0100d34:	74 0b                	je     c0100d41 <kmonitor+0x33>
        print_trapframe(tf);
c0100d36:	8b 45 08             	mov    0x8(%ebp),%eax
c0100d39:	89 04 24             	mov    %eax,(%esp)
c0100d3c:	e8 60 0e 00 00       	call   c0101ba1 <print_trapframe>
    }

    char *buf;
    while (1) {
        if ((buf = readline("K> ")) != NULL) {
c0100d41:	c7 04 24 39 65 10 c0 	movl   $0xc0106539,(%esp)
c0100d48:	e8 20 f6 ff ff       	call   c010036d <readline>
c0100d4d:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0100d50:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0100d54:	74 eb                	je     c0100d41 <kmonitor+0x33>
            if (runcmd(buf, tf) < 0) {
c0100d56:	8b 45 08             	mov    0x8(%ebp),%eax
c0100d59:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100d5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100d60:	89 04 24             	mov    %eax,(%esp)
c0100d63:	e8 ed fe ff ff       	call   c0100c55 <runcmd>
c0100d68:	85 c0                	test   %eax,%eax
c0100d6a:	78 02                	js     c0100d6e <kmonitor+0x60>
        if ((buf = readline("K> ")) != NULL) {
c0100d6c:	eb d3                	jmp    c0100d41 <kmonitor+0x33>
                break;
c0100d6e:	90                   	nop
            }
        }
    }
}
c0100d6f:	90                   	nop
c0100d70:	c9                   	leave  
c0100d71:	c3                   	ret    

c0100d72 <mon_help>:

/* mon_help - print the information about mon_* functions */
int
mon_help(int argc, char **argv, struct trapframe *tf) {
c0100d72:	f3 0f 1e fb          	endbr32 
c0100d76:	55                   	push   %ebp
c0100d77:	89 e5                	mov    %esp,%ebp
c0100d79:	83 ec 28             	sub    $0x28,%esp
    int i;
    for (i = 0; i < NCOMMANDS; i ++) {
c0100d7c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0100d83:	eb 3d                	jmp    c0100dc2 <mon_help+0x50>
        cprintf("%s - %s\n", commands[i].name, commands[i].desc);
c0100d85:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0100d88:	89 d0                	mov    %edx,%eax
c0100d8a:	01 c0                	add    %eax,%eax
c0100d8c:	01 d0                	add    %edx,%eax
c0100d8e:	c1 e0 02             	shl    $0x2,%eax
c0100d91:	05 04 90 11 c0       	add    $0xc0119004,%eax
c0100d96:	8b 08                	mov    (%eax),%ecx
c0100d98:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0100d9b:	89 d0                	mov    %edx,%eax
c0100d9d:	01 c0                	add    %eax,%eax
c0100d9f:	01 d0                	add    %edx,%eax
c0100da1:	c1 e0 02             	shl    $0x2,%eax
c0100da4:	05 00 90 11 c0       	add    $0xc0119000,%eax
c0100da9:	8b 00                	mov    (%eax),%eax
c0100dab:	89 4c 24 08          	mov    %ecx,0x8(%esp)
c0100daf:	89 44 24 04          	mov    %eax,0x4(%esp)
c0100db3:	c7 04 24 3d 65 10 c0 	movl   $0xc010653d,(%esp)
c0100dba:	e8 fb f4 ff ff       	call   c01002ba <cprintf>
    for (i = 0; i < NCOMMANDS; i ++) {
c0100dbf:	ff 45 f4             	incl   -0xc(%ebp)
c0100dc2:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100dc5:	83 f8 02             	cmp    $0x2,%eax
c0100dc8:	76 bb                	jbe    c0100d85 <mon_help+0x13>
    }
    return 0;
c0100dca:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0100dcf:	c9                   	leave  
c0100dd0:	c3                   	ret    

c0100dd1 <mon_kerninfo>:
/* *
 * mon_kerninfo - call print_kerninfo in kern/debug/kdebug.c to
 * print the memory occupancy in kernel.
 * */
int
mon_kerninfo(int argc, char **argv, struct trapframe *tf) {
c0100dd1:	f3 0f 1e fb          	endbr32 
c0100dd5:	55                   	push   %ebp
c0100dd6:	89 e5                	mov    %esp,%ebp
c0100dd8:	83 ec 08             	sub    $0x8,%esp
    print_kerninfo();
c0100ddb:	e8 9d fb ff ff       	call   c010097d <print_kerninfo>
    return 0;
c0100de0:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0100de5:	c9                   	leave  
c0100de6:	c3                   	ret    

c0100de7 <mon_backtrace>:
/* *
 * mon_backtrace - call print_stackframe in kern/debug/kdebug.c to
 * print a backtrace of the stack.
 * */
int
mon_backtrace(int argc, char **argv, struct trapframe *tf) {
c0100de7:	f3 0f 1e fb          	endbr32 
c0100deb:	55                   	push   %ebp
c0100dec:	89 e5                	mov    %esp,%ebp
c0100dee:	83 ec 08             	sub    $0x8,%esp
    print_stackframe();
c0100df1:	e8 d9 fc ff ff       	call   c0100acf <print_stackframe>
    return 0;
c0100df6:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0100dfb:	c9                   	leave  
c0100dfc:	c3                   	ret    

c0100dfd <clock_init>:
/* *
 * clock_init - initialize 8253 clock to interrupt 100 times per second,
 * and then enable IRQ_TIMER.
 * */
void
clock_init(void) {
c0100dfd:	f3 0f 1e fb          	endbr32 
c0100e01:	55                   	push   %ebp
c0100e02:	89 e5                	mov    %esp,%ebp
c0100e04:	83 ec 28             	sub    $0x28,%esp
c0100e07:	66 c7 45 ee 43 00    	movw   $0x43,-0x12(%ebp)
c0100e0d:	c6 45 ed 34          	movb   $0x34,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100e11:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
c0100e15:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
c0100e19:	ee                   	out    %al,(%dx)
}
c0100e1a:	90                   	nop
c0100e1b:	66 c7 45 f2 40 00    	movw   $0x40,-0xe(%ebp)
c0100e21:	c6 45 f1 9c          	movb   $0x9c,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100e25:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
c0100e29:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
c0100e2d:	ee                   	out    %al,(%dx)
}
c0100e2e:	90                   	nop
c0100e2f:	66 c7 45 f6 40 00    	movw   $0x40,-0xa(%ebp)
c0100e35:	c6 45 f5 2e          	movb   $0x2e,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100e39:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
c0100e3d:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
c0100e41:	ee                   	out    %al,(%dx)
}
c0100e42:	90                   	nop
    outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
    outb(IO_TIMER1, TIMER_DIV(100) % 256);
    outb(IO_TIMER1, TIMER_DIV(100) / 256);

    // initialize time counter 'ticks' to zero
    ticks = 0;
c0100e43:	c7 05 0c cf 11 c0 00 	movl   $0x0,0xc011cf0c
c0100e4a:	00 00 00 

    cprintf("++ setup timer interrupts\n");
c0100e4d:	c7 04 24 46 65 10 c0 	movl   $0xc0106546,(%esp)
c0100e54:	e8 61 f4 ff ff       	call   c01002ba <cprintf>
    pic_enable(IRQ_TIMER);
c0100e59:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
c0100e60:	e8 95 09 00 00       	call   c01017fa <pic_enable>
}
c0100e65:	90                   	nop
c0100e66:	c9                   	leave  
c0100e67:	c3                   	ret    

c0100e68 <__intr_save>:
#include <x86.h>
#include <intr.h>
#include <mmu.h>

static inline bool
__intr_save(void) {
c0100e68:	55                   	push   %ebp
c0100e69:	89 e5                	mov    %esp,%ebp
c0100e6b:	83 ec 18             	sub    $0x18,%esp
}

static inline uint32_t
read_eflags(void) {
    uint32_t eflags;
    asm volatile ("pushfl; popl %0" : "=r" (eflags));
c0100e6e:	9c                   	pushf  
c0100e6f:	58                   	pop    %eax
c0100e70:	89 45 f4             	mov    %eax,-0xc(%ebp)
    return eflags;
c0100e73:	8b 45 f4             	mov    -0xc(%ebp),%eax
    if (read_eflags() & FL_IF) {
c0100e76:	25 00 02 00 00       	and    $0x200,%eax
c0100e7b:	85 c0                	test   %eax,%eax
c0100e7d:	74 0c                	je     c0100e8b <__intr_save+0x23>
        intr_disable();
c0100e7f:	e8 05 0b 00 00       	call   c0101989 <intr_disable>
        return 1;
c0100e84:	b8 01 00 00 00       	mov    $0x1,%eax
c0100e89:	eb 05                	jmp    c0100e90 <__intr_save+0x28>
    }
    return 0;
c0100e8b:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0100e90:	c9                   	leave  
c0100e91:	c3                   	ret    

c0100e92 <__intr_restore>:

static inline void
__intr_restore(bool flag) {
c0100e92:	55                   	push   %ebp
c0100e93:	89 e5                	mov    %esp,%ebp
c0100e95:	83 ec 08             	sub    $0x8,%esp
    if (flag) {
c0100e98:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c0100e9c:	74 05                	je     c0100ea3 <__intr_restore+0x11>
        intr_enable();
c0100e9e:	e8 da 0a 00 00       	call   c010197d <intr_enable>
    }
}
c0100ea3:	90                   	nop
c0100ea4:	c9                   	leave  
c0100ea5:	c3                   	ret    

c0100ea6 <delay>:
#include <memlayout.h>
#include <sync.h>

/* stupid I/O delay routine necessitated by historical PC design flaws */
static void
delay(void) {
c0100ea6:	f3 0f 1e fb          	endbr32 
c0100eaa:	55                   	push   %ebp
c0100eab:	89 e5                	mov    %esp,%ebp
c0100ead:	83 ec 10             	sub    $0x10,%esp
c0100eb0:	66 c7 45 f2 84 00    	movw   $0x84,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0100eb6:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
c0100eba:	89 c2                	mov    %eax,%edx
c0100ebc:	ec                   	in     (%dx),%al
c0100ebd:	88 45 f1             	mov    %al,-0xf(%ebp)
c0100ec0:	66 c7 45 f6 84 00    	movw   $0x84,-0xa(%ebp)
c0100ec6:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
c0100eca:	89 c2                	mov    %eax,%edx
c0100ecc:	ec                   	in     (%dx),%al
c0100ecd:	88 45 f5             	mov    %al,-0xb(%ebp)
c0100ed0:	66 c7 45 fa 84 00    	movw   $0x84,-0x6(%ebp)
c0100ed6:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
c0100eda:	89 c2                	mov    %eax,%edx
c0100edc:	ec                   	in     (%dx),%al
c0100edd:	88 45 f9             	mov    %al,-0x7(%ebp)
c0100ee0:	66 c7 45 fe 84 00    	movw   $0x84,-0x2(%ebp)
c0100ee6:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
c0100eea:	89 c2                	mov    %eax,%edx
c0100eec:	ec                   	in     (%dx),%al
c0100eed:	88 45 fd             	mov    %al,-0x3(%ebp)
    inb(0x84);
    inb(0x84);
    inb(0x84);
    inb(0x84);
}
c0100ef0:	90                   	nop
c0100ef1:	c9                   	leave  
c0100ef2:	c3                   	ret    

c0100ef3 <cga_init>:
static uint16_t addr_6845;

/* TEXT-mode CGA/VGA display output */

static void
cga_init(void) {
c0100ef3:	f3 0f 1e fb          	endbr32 
c0100ef7:	55                   	push   %ebp
c0100ef8:	89 e5                	mov    %esp,%ebp
c0100efa:	83 ec 20             	sub    $0x20,%esp
    volatile uint16_t *cp = (uint16_t *)(CGA_BUF + KERNBASE);
c0100efd:	c7 45 fc 00 80 0b c0 	movl   $0xc00b8000,-0x4(%ebp)
    uint16_t was = *cp;
c0100f04:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0100f07:	0f b7 00             	movzwl (%eax),%eax
c0100f0a:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
    *cp = (uint16_t) 0xA55A;
c0100f0e:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0100f11:	66 c7 00 5a a5       	movw   $0xa55a,(%eax)
    if (*cp != 0xA55A) {
c0100f16:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0100f19:	0f b7 00             	movzwl (%eax),%eax
c0100f1c:	0f b7 c0             	movzwl %ax,%eax
c0100f1f:	3d 5a a5 00 00       	cmp    $0xa55a,%eax
c0100f24:	74 12                	je     c0100f38 <cga_init+0x45>
        cp = (uint16_t*)(MONO_BUF + KERNBASE);
c0100f26:	c7 45 fc 00 00 0b c0 	movl   $0xc00b0000,-0x4(%ebp)
        addr_6845 = MONO_BASE;
c0100f2d:	66 c7 05 46 c4 11 c0 	movw   $0x3b4,0xc011c446
c0100f34:	b4 03 
c0100f36:	eb 13                	jmp    c0100f4b <cga_init+0x58>
    } else {
        *cp = was;
c0100f38:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0100f3b:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
c0100f3f:	66 89 10             	mov    %dx,(%eax)
        addr_6845 = CGA_BASE;
c0100f42:	66 c7 05 46 c4 11 c0 	movw   $0x3d4,0xc011c446
c0100f49:	d4 03 
    }

    // Extract cursor location
    uint32_t pos;
    outb(addr_6845, 14);
c0100f4b:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0100f52:	66 89 45 e6          	mov    %ax,-0x1a(%ebp)
c0100f56:	c6 45 e5 0e          	movb   $0xe,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100f5a:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
c0100f5e:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
c0100f62:	ee                   	out    %al,(%dx)
}
c0100f63:	90                   	nop
    pos = inb(addr_6845 + 1) << 8;
c0100f64:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0100f6b:	40                   	inc    %eax
c0100f6c:	0f b7 c0             	movzwl %ax,%eax
c0100f6f:	66 89 45 ea          	mov    %ax,-0x16(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0100f73:	0f b7 45 ea          	movzwl -0x16(%ebp),%eax
c0100f77:	89 c2                	mov    %eax,%edx
c0100f79:	ec                   	in     (%dx),%al
c0100f7a:	88 45 e9             	mov    %al,-0x17(%ebp)
    return data;
c0100f7d:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
c0100f81:	0f b6 c0             	movzbl %al,%eax
c0100f84:	c1 e0 08             	shl    $0x8,%eax
c0100f87:	89 45 f4             	mov    %eax,-0xc(%ebp)
    outb(addr_6845, 15);
c0100f8a:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0100f91:	66 89 45 ee          	mov    %ax,-0x12(%ebp)
c0100f95:	c6 45 ed 0f          	movb   $0xf,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100f99:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
c0100f9d:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
c0100fa1:	ee                   	out    %al,(%dx)
}
c0100fa2:	90                   	nop
    pos |= inb(addr_6845 + 1);
c0100fa3:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0100faa:	40                   	inc    %eax
c0100fab:	0f b7 c0             	movzwl %ax,%eax
c0100fae:	66 89 45 f2          	mov    %ax,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0100fb2:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
c0100fb6:	89 c2                	mov    %eax,%edx
c0100fb8:	ec                   	in     (%dx),%al
c0100fb9:	88 45 f1             	mov    %al,-0xf(%ebp)
    return data;
c0100fbc:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
c0100fc0:	0f b6 c0             	movzbl %al,%eax
c0100fc3:	09 45 f4             	or     %eax,-0xc(%ebp)

    crt_buf = (uint16_t*) cp;
c0100fc6:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0100fc9:	a3 40 c4 11 c0       	mov    %eax,0xc011c440
    crt_pos = pos;
c0100fce:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0100fd1:	0f b7 c0             	movzwl %ax,%eax
c0100fd4:	66 a3 44 c4 11 c0    	mov    %ax,0xc011c444
}
c0100fda:	90                   	nop
c0100fdb:	c9                   	leave  
c0100fdc:	c3                   	ret    

c0100fdd <serial_init>:

static bool serial_exists = 0;

static void
serial_init(void) {
c0100fdd:	f3 0f 1e fb          	endbr32 
c0100fe1:	55                   	push   %ebp
c0100fe2:	89 e5                	mov    %esp,%ebp
c0100fe4:	83 ec 48             	sub    $0x48,%esp
c0100fe7:	66 c7 45 d2 fa 03    	movw   $0x3fa,-0x2e(%ebp)
c0100fed:	c6 45 d1 00          	movb   $0x0,-0x2f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0100ff1:	0f b6 45 d1          	movzbl -0x2f(%ebp),%eax
c0100ff5:	0f b7 55 d2          	movzwl -0x2e(%ebp),%edx
c0100ff9:	ee                   	out    %al,(%dx)
}
c0100ffa:	90                   	nop
c0100ffb:	66 c7 45 d6 fb 03    	movw   $0x3fb,-0x2a(%ebp)
c0101001:	c6 45 d5 80          	movb   $0x80,-0x2b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101005:	0f b6 45 d5          	movzbl -0x2b(%ebp),%eax
c0101009:	0f b7 55 d6          	movzwl -0x2a(%ebp),%edx
c010100d:	ee                   	out    %al,(%dx)
}
c010100e:	90                   	nop
c010100f:	66 c7 45 da f8 03    	movw   $0x3f8,-0x26(%ebp)
c0101015:	c6 45 d9 0c          	movb   $0xc,-0x27(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101019:	0f b6 45 d9          	movzbl -0x27(%ebp),%eax
c010101d:	0f b7 55 da          	movzwl -0x26(%ebp),%edx
c0101021:	ee                   	out    %al,(%dx)
}
c0101022:	90                   	nop
c0101023:	66 c7 45 de f9 03    	movw   $0x3f9,-0x22(%ebp)
c0101029:	c6 45 dd 00          	movb   $0x0,-0x23(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010102d:	0f b6 45 dd          	movzbl -0x23(%ebp),%eax
c0101031:	0f b7 55 de          	movzwl -0x22(%ebp),%edx
c0101035:	ee                   	out    %al,(%dx)
}
c0101036:	90                   	nop
c0101037:	66 c7 45 e2 fb 03    	movw   $0x3fb,-0x1e(%ebp)
c010103d:	c6 45 e1 03          	movb   $0x3,-0x1f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101041:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
c0101045:	0f b7 55 e2          	movzwl -0x1e(%ebp),%edx
c0101049:	ee                   	out    %al,(%dx)
}
c010104a:	90                   	nop
c010104b:	66 c7 45 e6 fc 03    	movw   $0x3fc,-0x1a(%ebp)
c0101051:	c6 45 e5 00          	movb   $0x0,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101055:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
c0101059:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
c010105d:	ee                   	out    %al,(%dx)
}
c010105e:	90                   	nop
c010105f:	66 c7 45 ea f9 03    	movw   $0x3f9,-0x16(%ebp)
c0101065:	c6 45 e9 01          	movb   $0x1,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101069:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
c010106d:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
c0101071:	ee                   	out    %al,(%dx)
}
c0101072:	90                   	nop
c0101073:	66 c7 45 ee fd 03    	movw   $0x3fd,-0x12(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0101079:	0f b7 45 ee          	movzwl -0x12(%ebp),%eax
c010107d:	89 c2                	mov    %eax,%edx
c010107f:	ec                   	in     (%dx),%al
c0101080:	88 45 ed             	mov    %al,-0x13(%ebp)
    return data;
c0101083:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
    // Enable rcv interrupts
    outb(COM1 + COM_IER, COM_IER_RDI);

    // Clear any preexisting overrun indications and interrupts
    // Serial port doesn't exist if COM_LSR returns 0xFF
    serial_exists = (inb(COM1 + COM_LSR) != 0xFF);
c0101087:	3c ff                	cmp    $0xff,%al
c0101089:	0f 95 c0             	setne  %al
c010108c:	0f b6 c0             	movzbl %al,%eax
c010108f:	a3 48 c4 11 c0       	mov    %eax,0xc011c448
c0101094:	66 c7 45 f2 fa 03    	movw   $0x3fa,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c010109a:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
c010109e:	89 c2                	mov    %eax,%edx
c01010a0:	ec                   	in     (%dx),%al
c01010a1:	88 45 f1             	mov    %al,-0xf(%ebp)
c01010a4:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
c01010aa:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
c01010ae:	89 c2                	mov    %eax,%edx
c01010b0:	ec                   	in     (%dx),%al
c01010b1:	88 45 f5             	mov    %al,-0xb(%ebp)
    (void) inb(COM1+COM_IIR);
    (void) inb(COM1+COM_RX);

    if (serial_exists) {
c01010b4:	a1 48 c4 11 c0       	mov    0xc011c448,%eax
c01010b9:	85 c0                	test   %eax,%eax
c01010bb:	74 0c                	je     c01010c9 <serial_init+0xec>
        pic_enable(IRQ_COM1);
c01010bd:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
c01010c4:	e8 31 07 00 00       	call   c01017fa <pic_enable>
    }
}
c01010c9:	90                   	nop
c01010ca:	c9                   	leave  
c01010cb:	c3                   	ret    

c01010cc <lpt_putc_sub>:

static void
lpt_putc_sub(int c) {
c01010cc:	f3 0f 1e fb          	endbr32 
c01010d0:	55                   	push   %ebp
c01010d1:	89 e5                	mov    %esp,%ebp
c01010d3:	83 ec 20             	sub    $0x20,%esp
    int i;
    for (i = 0; !(inb(LPTPORT + 1) & 0x80) && i < 12800; i ++) {
c01010d6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
c01010dd:	eb 08                	jmp    c01010e7 <lpt_putc_sub+0x1b>
        delay();
c01010df:	e8 c2 fd ff ff       	call   c0100ea6 <delay>
    for (i = 0; !(inb(LPTPORT + 1) & 0x80) && i < 12800; i ++) {
c01010e4:	ff 45 fc             	incl   -0x4(%ebp)
c01010e7:	66 c7 45 fa 79 03    	movw   $0x379,-0x6(%ebp)
c01010ed:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
c01010f1:	89 c2                	mov    %eax,%edx
c01010f3:	ec                   	in     (%dx),%al
c01010f4:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
c01010f7:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
c01010fb:	84 c0                	test   %al,%al
c01010fd:	78 09                	js     c0101108 <lpt_putc_sub+0x3c>
c01010ff:	81 7d fc ff 31 00 00 	cmpl   $0x31ff,-0x4(%ebp)
c0101106:	7e d7                	jle    c01010df <lpt_putc_sub+0x13>
    }
    outb(LPTPORT + 0, c);
c0101108:	8b 45 08             	mov    0x8(%ebp),%eax
c010110b:	0f b6 c0             	movzbl %al,%eax
c010110e:	66 c7 45 ee 78 03    	movw   $0x378,-0x12(%ebp)
c0101114:	88 45 ed             	mov    %al,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101117:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
c010111b:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
c010111f:	ee                   	out    %al,(%dx)
}
c0101120:	90                   	nop
c0101121:	66 c7 45 f2 7a 03    	movw   $0x37a,-0xe(%ebp)
c0101127:	c6 45 f1 0d          	movb   $0xd,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010112b:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
c010112f:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
c0101133:	ee                   	out    %al,(%dx)
}
c0101134:	90                   	nop
c0101135:	66 c7 45 f6 7a 03    	movw   $0x37a,-0xa(%ebp)
c010113b:	c6 45 f5 08          	movb   $0x8,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010113f:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
c0101143:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
c0101147:	ee                   	out    %al,(%dx)
}
c0101148:	90                   	nop
    outb(LPTPORT + 2, 0x08 | 0x04 | 0x01);
    outb(LPTPORT + 2, 0x08);
}
c0101149:	90                   	nop
c010114a:	c9                   	leave  
c010114b:	c3                   	ret    

c010114c <lpt_putc>:

/* lpt_putc - copy console output to parallel port */
static void
lpt_putc(int c) {
c010114c:	f3 0f 1e fb          	endbr32 
c0101150:	55                   	push   %ebp
c0101151:	89 e5                	mov    %esp,%ebp
c0101153:	83 ec 04             	sub    $0x4,%esp
    if (c != '\b') {
c0101156:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
c010115a:	74 0d                	je     c0101169 <lpt_putc+0x1d>
        lpt_putc_sub(c);
c010115c:	8b 45 08             	mov    0x8(%ebp),%eax
c010115f:	89 04 24             	mov    %eax,(%esp)
c0101162:	e8 65 ff ff ff       	call   c01010cc <lpt_putc_sub>
    else {
        lpt_putc_sub('\b');
        lpt_putc_sub(' ');
        lpt_putc_sub('\b');
    }
}
c0101167:	eb 24                	jmp    c010118d <lpt_putc+0x41>
        lpt_putc_sub('\b');
c0101169:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
c0101170:	e8 57 ff ff ff       	call   c01010cc <lpt_putc_sub>
        lpt_putc_sub(' ');
c0101175:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
c010117c:	e8 4b ff ff ff       	call   c01010cc <lpt_putc_sub>
        lpt_putc_sub('\b');
c0101181:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
c0101188:	e8 3f ff ff ff       	call   c01010cc <lpt_putc_sub>
}
c010118d:	90                   	nop
c010118e:	c9                   	leave  
c010118f:	c3                   	ret    

c0101190 <cga_putc>:

/* cga_putc - print character to console */
static void
cga_putc(int c) {
c0101190:	f3 0f 1e fb          	endbr32 
c0101194:	55                   	push   %ebp
c0101195:	89 e5                	mov    %esp,%ebp
c0101197:	53                   	push   %ebx
c0101198:	83 ec 34             	sub    $0x34,%esp
    // set black on white
    if (!(c & ~0xFF)) {
c010119b:	8b 45 08             	mov    0x8(%ebp),%eax
c010119e:	25 00 ff ff ff       	and    $0xffffff00,%eax
c01011a3:	85 c0                	test   %eax,%eax
c01011a5:	75 07                	jne    c01011ae <cga_putc+0x1e>
        c |= 0x0700;
c01011a7:	81 4d 08 00 07 00 00 	orl    $0x700,0x8(%ebp)
    }

    switch (c & 0xff) {
c01011ae:	8b 45 08             	mov    0x8(%ebp),%eax
c01011b1:	0f b6 c0             	movzbl %al,%eax
c01011b4:	83 f8 0d             	cmp    $0xd,%eax
c01011b7:	74 72                	je     c010122b <cga_putc+0x9b>
c01011b9:	83 f8 0d             	cmp    $0xd,%eax
c01011bc:	0f 8f a3 00 00 00    	jg     c0101265 <cga_putc+0xd5>
c01011c2:	83 f8 08             	cmp    $0x8,%eax
c01011c5:	74 0a                	je     c01011d1 <cga_putc+0x41>
c01011c7:	83 f8 0a             	cmp    $0xa,%eax
c01011ca:	74 4c                	je     c0101218 <cga_putc+0x88>
c01011cc:	e9 94 00 00 00       	jmp    c0101265 <cga_putc+0xd5>
    case '\b':
        if (crt_pos > 0) {
c01011d1:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c01011d8:	85 c0                	test   %eax,%eax
c01011da:	0f 84 af 00 00 00    	je     c010128f <cga_putc+0xff>
            crt_pos --;
c01011e0:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c01011e7:	48                   	dec    %eax
c01011e8:	0f b7 c0             	movzwl %ax,%eax
c01011eb:	66 a3 44 c4 11 c0    	mov    %ax,0xc011c444
            crt_buf[crt_pos] = (c & ~0xff) | ' ';
c01011f1:	8b 45 08             	mov    0x8(%ebp),%eax
c01011f4:	98                   	cwtl   
c01011f5:	25 00 ff ff ff       	and    $0xffffff00,%eax
c01011fa:	98                   	cwtl   
c01011fb:	83 c8 20             	or     $0x20,%eax
c01011fe:	98                   	cwtl   
c01011ff:	8b 15 40 c4 11 c0    	mov    0xc011c440,%edx
c0101205:	0f b7 0d 44 c4 11 c0 	movzwl 0xc011c444,%ecx
c010120c:	01 c9                	add    %ecx,%ecx
c010120e:	01 ca                	add    %ecx,%edx
c0101210:	0f b7 c0             	movzwl %ax,%eax
c0101213:	66 89 02             	mov    %ax,(%edx)
        }
        break;
c0101216:	eb 77                	jmp    c010128f <cga_putc+0xff>
    case '\n':
        crt_pos += CRT_COLS;
c0101218:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c010121f:	83 c0 50             	add    $0x50,%eax
c0101222:	0f b7 c0             	movzwl %ax,%eax
c0101225:	66 a3 44 c4 11 c0    	mov    %ax,0xc011c444
    case '\r':
        crt_pos -= (crt_pos % CRT_COLS);
c010122b:	0f b7 1d 44 c4 11 c0 	movzwl 0xc011c444,%ebx
c0101232:	0f b7 0d 44 c4 11 c0 	movzwl 0xc011c444,%ecx
c0101239:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
c010123e:	89 c8                	mov    %ecx,%eax
c0101240:	f7 e2                	mul    %edx
c0101242:	c1 ea 06             	shr    $0x6,%edx
c0101245:	89 d0                	mov    %edx,%eax
c0101247:	c1 e0 02             	shl    $0x2,%eax
c010124a:	01 d0                	add    %edx,%eax
c010124c:	c1 e0 04             	shl    $0x4,%eax
c010124f:	29 c1                	sub    %eax,%ecx
c0101251:	89 c8                	mov    %ecx,%eax
c0101253:	0f b7 c0             	movzwl %ax,%eax
c0101256:	29 c3                	sub    %eax,%ebx
c0101258:	89 d8                	mov    %ebx,%eax
c010125a:	0f b7 c0             	movzwl %ax,%eax
c010125d:	66 a3 44 c4 11 c0    	mov    %ax,0xc011c444
        break;
c0101263:	eb 2b                	jmp    c0101290 <cga_putc+0x100>
    default:
        crt_buf[crt_pos ++] = c;     // write the character
c0101265:	8b 0d 40 c4 11 c0    	mov    0xc011c440,%ecx
c010126b:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c0101272:	8d 50 01             	lea    0x1(%eax),%edx
c0101275:	0f b7 d2             	movzwl %dx,%edx
c0101278:	66 89 15 44 c4 11 c0 	mov    %dx,0xc011c444
c010127f:	01 c0                	add    %eax,%eax
c0101281:	8d 14 01             	lea    (%ecx,%eax,1),%edx
c0101284:	8b 45 08             	mov    0x8(%ebp),%eax
c0101287:	0f b7 c0             	movzwl %ax,%eax
c010128a:	66 89 02             	mov    %ax,(%edx)
        break;
c010128d:	eb 01                	jmp    c0101290 <cga_putc+0x100>
        break;
c010128f:	90                   	nop
    }

    // What is the purpose of this?
    if (crt_pos >= CRT_SIZE) {
c0101290:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c0101297:	3d cf 07 00 00       	cmp    $0x7cf,%eax
c010129c:	76 5d                	jbe    c01012fb <cga_putc+0x16b>
        int i;
        memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
c010129e:	a1 40 c4 11 c0       	mov    0xc011c440,%eax
c01012a3:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
c01012a9:	a1 40 c4 11 c0       	mov    0xc011c440,%eax
c01012ae:	c7 44 24 08 00 0f 00 	movl   $0xf00,0x8(%esp)
c01012b5:	00 
c01012b6:	89 54 24 04          	mov    %edx,0x4(%esp)
c01012ba:	89 04 24             	mov    %eax,(%esp)
c01012bd:	e8 78 47 00 00       	call   c0105a3a <memmove>
        for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i ++) {
c01012c2:	c7 45 f4 80 07 00 00 	movl   $0x780,-0xc(%ebp)
c01012c9:	eb 14                	jmp    c01012df <cga_putc+0x14f>
            crt_buf[i] = 0x0700 | ' ';
c01012cb:	a1 40 c4 11 c0       	mov    0xc011c440,%eax
c01012d0:	8b 55 f4             	mov    -0xc(%ebp),%edx
c01012d3:	01 d2                	add    %edx,%edx
c01012d5:	01 d0                	add    %edx,%eax
c01012d7:	66 c7 00 20 07       	movw   $0x720,(%eax)
        for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i ++) {
c01012dc:	ff 45 f4             	incl   -0xc(%ebp)
c01012df:	81 7d f4 cf 07 00 00 	cmpl   $0x7cf,-0xc(%ebp)
c01012e6:	7e e3                	jle    c01012cb <cga_putc+0x13b>
        }
        crt_pos -= CRT_COLS;
c01012e8:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c01012ef:	83 e8 50             	sub    $0x50,%eax
c01012f2:	0f b7 c0             	movzwl %ax,%eax
c01012f5:	66 a3 44 c4 11 c0    	mov    %ax,0xc011c444
    }

    // move that little blinky thing
    outb(addr_6845, 14);
c01012fb:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0101302:	66 89 45 e6          	mov    %ax,-0x1a(%ebp)
c0101306:	c6 45 e5 0e          	movb   $0xe,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010130a:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
c010130e:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
c0101312:	ee                   	out    %al,(%dx)
}
c0101313:	90                   	nop
    outb(addr_6845 + 1, crt_pos >> 8);
c0101314:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c010131b:	c1 e8 08             	shr    $0x8,%eax
c010131e:	0f b7 c0             	movzwl %ax,%eax
c0101321:	0f b6 c0             	movzbl %al,%eax
c0101324:	0f b7 15 46 c4 11 c0 	movzwl 0xc011c446,%edx
c010132b:	42                   	inc    %edx
c010132c:	0f b7 d2             	movzwl %dx,%edx
c010132f:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
c0101333:	88 45 e9             	mov    %al,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101336:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
c010133a:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
c010133e:	ee                   	out    %al,(%dx)
}
c010133f:	90                   	nop
    outb(addr_6845, 15);
c0101340:	0f b7 05 46 c4 11 c0 	movzwl 0xc011c446,%eax
c0101347:	66 89 45 ee          	mov    %ax,-0x12(%ebp)
c010134b:	c6 45 ed 0f          	movb   $0xf,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010134f:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
c0101353:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
c0101357:	ee                   	out    %al,(%dx)
}
c0101358:	90                   	nop
    outb(addr_6845 + 1, crt_pos);
c0101359:	0f b7 05 44 c4 11 c0 	movzwl 0xc011c444,%eax
c0101360:	0f b6 c0             	movzbl %al,%eax
c0101363:	0f b7 15 46 c4 11 c0 	movzwl 0xc011c446,%edx
c010136a:	42                   	inc    %edx
c010136b:	0f b7 d2             	movzwl %dx,%edx
c010136e:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
c0101372:	88 45 f1             	mov    %al,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101375:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
c0101379:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
c010137d:	ee                   	out    %al,(%dx)
}
c010137e:	90                   	nop
}
c010137f:	90                   	nop
c0101380:	83 c4 34             	add    $0x34,%esp
c0101383:	5b                   	pop    %ebx
c0101384:	5d                   	pop    %ebp
c0101385:	c3                   	ret    

c0101386 <serial_putc_sub>:

static void
serial_putc_sub(int c) {
c0101386:	f3 0f 1e fb          	endbr32 
c010138a:	55                   	push   %ebp
c010138b:	89 e5                	mov    %esp,%ebp
c010138d:	83 ec 10             	sub    $0x10,%esp
    int i;
    for (i = 0; !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800; i ++) {
c0101390:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
c0101397:	eb 08                	jmp    c01013a1 <serial_putc_sub+0x1b>
        delay();
c0101399:	e8 08 fb ff ff       	call   c0100ea6 <delay>
    for (i = 0; !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800; i ++) {
c010139e:	ff 45 fc             	incl   -0x4(%ebp)
c01013a1:	66 c7 45 fa fd 03    	movw   $0x3fd,-0x6(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c01013a7:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
c01013ab:	89 c2                	mov    %eax,%edx
c01013ad:	ec                   	in     (%dx),%al
c01013ae:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
c01013b1:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
c01013b5:	0f b6 c0             	movzbl %al,%eax
c01013b8:	83 e0 20             	and    $0x20,%eax
c01013bb:	85 c0                	test   %eax,%eax
c01013bd:	75 09                	jne    c01013c8 <serial_putc_sub+0x42>
c01013bf:	81 7d fc ff 31 00 00 	cmpl   $0x31ff,-0x4(%ebp)
c01013c6:	7e d1                	jle    c0101399 <serial_putc_sub+0x13>
    }
    outb(COM1 + COM_TX, c);
c01013c8:	8b 45 08             	mov    0x8(%ebp),%eax
c01013cb:	0f b6 c0             	movzbl %al,%eax
c01013ce:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
c01013d4:	88 45 f5             	mov    %al,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01013d7:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
c01013db:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
c01013df:	ee                   	out    %al,(%dx)
}
c01013e0:	90                   	nop
}
c01013e1:	90                   	nop
c01013e2:	c9                   	leave  
c01013e3:	c3                   	ret    

c01013e4 <serial_putc>:

/* serial_putc - print character to serial port */
static void
serial_putc(int c) {
c01013e4:	f3 0f 1e fb          	endbr32 
c01013e8:	55                   	push   %ebp
c01013e9:	89 e5                	mov    %esp,%ebp
c01013eb:	83 ec 04             	sub    $0x4,%esp
    if (c != '\b') {
c01013ee:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
c01013f2:	74 0d                	je     c0101401 <serial_putc+0x1d>
        serial_putc_sub(c);
c01013f4:	8b 45 08             	mov    0x8(%ebp),%eax
c01013f7:	89 04 24             	mov    %eax,(%esp)
c01013fa:	e8 87 ff ff ff       	call   c0101386 <serial_putc_sub>
    else {
        serial_putc_sub('\b');
        serial_putc_sub(' ');
        serial_putc_sub('\b');
    }
}
c01013ff:	eb 24                	jmp    c0101425 <serial_putc+0x41>
        serial_putc_sub('\b');
c0101401:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
c0101408:	e8 79 ff ff ff       	call   c0101386 <serial_putc_sub>
        serial_putc_sub(' ');
c010140d:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
c0101414:	e8 6d ff ff ff       	call   c0101386 <serial_putc_sub>
        serial_putc_sub('\b');
c0101419:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
c0101420:	e8 61 ff ff ff       	call   c0101386 <serial_putc_sub>
}
c0101425:	90                   	nop
c0101426:	c9                   	leave  
c0101427:	c3                   	ret    

c0101428 <cons_intr>:
/* *
 * cons_intr - called by device interrupt routines to feed input
 * characters into the circular console input buffer.
 * */
static void
cons_intr(int (*proc)(void)) {
c0101428:	f3 0f 1e fb          	endbr32 
c010142c:	55                   	push   %ebp
c010142d:	89 e5                	mov    %esp,%ebp
c010142f:	83 ec 18             	sub    $0x18,%esp
    int c;
    while ((c = (*proc)()) != -1) {
c0101432:	eb 33                	jmp    c0101467 <cons_intr+0x3f>
        if (c != 0) {
c0101434:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0101438:	74 2d                	je     c0101467 <cons_intr+0x3f>
            cons.buf[cons.wpos ++] = c;
c010143a:	a1 64 c6 11 c0       	mov    0xc011c664,%eax
c010143f:	8d 50 01             	lea    0x1(%eax),%edx
c0101442:	89 15 64 c6 11 c0    	mov    %edx,0xc011c664
c0101448:	8b 55 f4             	mov    -0xc(%ebp),%edx
c010144b:	88 90 60 c4 11 c0    	mov    %dl,-0x3fee3ba0(%eax)
            if (cons.wpos == CONSBUFSIZE) {
c0101451:	a1 64 c6 11 c0       	mov    0xc011c664,%eax
c0101456:	3d 00 02 00 00       	cmp    $0x200,%eax
c010145b:	75 0a                	jne    c0101467 <cons_intr+0x3f>
                cons.wpos = 0;
c010145d:	c7 05 64 c6 11 c0 00 	movl   $0x0,0xc011c664
c0101464:	00 00 00 
    while ((c = (*proc)()) != -1) {
c0101467:	8b 45 08             	mov    0x8(%ebp),%eax
c010146a:	ff d0                	call   *%eax
c010146c:	89 45 f4             	mov    %eax,-0xc(%ebp)
c010146f:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
c0101473:	75 bf                	jne    c0101434 <cons_intr+0xc>
            }
        }
    }
}
c0101475:	90                   	nop
c0101476:	90                   	nop
c0101477:	c9                   	leave  
c0101478:	c3                   	ret    

c0101479 <serial_proc_data>:

/* serial_proc_data - get data from serial port */
static int
serial_proc_data(void) {
c0101479:	f3 0f 1e fb          	endbr32 
c010147d:	55                   	push   %ebp
c010147e:	89 e5                	mov    %esp,%ebp
c0101480:	83 ec 10             	sub    $0x10,%esp
c0101483:	66 c7 45 fa fd 03    	movw   $0x3fd,-0x6(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0101489:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
c010148d:	89 c2                	mov    %eax,%edx
c010148f:	ec                   	in     (%dx),%al
c0101490:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
c0101493:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
    if (!(inb(COM1 + COM_LSR) & COM_LSR_DATA)) {
c0101497:	0f b6 c0             	movzbl %al,%eax
c010149a:	83 e0 01             	and    $0x1,%eax
c010149d:	85 c0                	test   %eax,%eax
c010149f:	75 07                	jne    c01014a8 <serial_proc_data+0x2f>
        return -1;
c01014a1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
c01014a6:	eb 2a                	jmp    c01014d2 <serial_proc_data+0x59>
c01014a8:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c01014ae:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
c01014b2:	89 c2                	mov    %eax,%edx
c01014b4:	ec                   	in     (%dx),%al
c01014b5:	88 45 f5             	mov    %al,-0xb(%ebp)
    return data;
c01014b8:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
    }
    int c = inb(COM1 + COM_RX);
c01014bc:	0f b6 c0             	movzbl %al,%eax
c01014bf:	89 45 fc             	mov    %eax,-0x4(%ebp)
    if (c == 127) {
c01014c2:	83 7d fc 7f          	cmpl   $0x7f,-0x4(%ebp)
c01014c6:	75 07                	jne    c01014cf <serial_proc_data+0x56>
        c = '\b';
c01014c8:	c7 45 fc 08 00 00 00 	movl   $0x8,-0x4(%ebp)
    }
    return c;
c01014cf:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
c01014d2:	c9                   	leave  
c01014d3:	c3                   	ret    

c01014d4 <serial_intr>:

/* serial_intr - try to feed input characters from serial port */
void
serial_intr(void) {
c01014d4:	f3 0f 1e fb          	endbr32 
c01014d8:	55                   	push   %ebp
c01014d9:	89 e5                	mov    %esp,%ebp
c01014db:	83 ec 18             	sub    $0x18,%esp
    if (serial_exists) {
c01014de:	a1 48 c4 11 c0       	mov    0xc011c448,%eax
c01014e3:	85 c0                	test   %eax,%eax
c01014e5:	74 0c                	je     c01014f3 <serial_intr+0x1f>
        cons_intr(serial_proc_data);
c01014e7:	c7 04 24 79 14 10 c0 	movl   $0xc0101479,(%esp)
c01014ee:	e8 35 ff ff ff       	call   c0101428 <cons_intr>
    }
}
c01014f3:	90                   	nop
c01014f4:	c9                   	leave  
c01014f5:	c3                   	ret    

c01014f6 <kbd_proc_data>:
 *
 * The kbd_proc_data() function gets data from the keyboard.
 * If we finish a character, return it, else 0. And return -1 if no data.
 * */
static int
kbd_proc_data(void) {
c01014f6:	f3 0f 1e fb          	endbr32 
c01014fa:	55                   	push   %ebp
c01014fb:	89 e5                	mov    %esp,%ebp
c01014fd:	83 ec 38             	sub    $0x38,%esp
c0101500:	66 c7 45 f0 64 00    	movw   $0x64,-0x10(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c0101506:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0101509:	89 c2                	mov    %eax,%edx
c010150b:	ec                   	in     (%dx),%al
c010150c:	88 45 ef             	mov    %al,-0x11(%ebp)
    return data;
c010150f:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
    int c;
    uint8_t data;
    static uint32_t shift;

    if ((inb(KBSTATP) & KBS_DIB) == 0) {
c0101513:	0f b6 c0             	movzbl %al,%eax
c0101516:	83 e0 01             	and    $0x1,%eax
c0101519:	85 c0                	test   %eax,%eax
c010151b:	75 0a                	jne    c0101527 <kbd_proc_data+0x31>
        return -1;
c010151d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
c0101522:	e9 56 01 00 00       	jmp    c010167d <kbd_proc_data+0x187>
c0101527:	66 c7 45 ec 60 00    	movw   $0x60,-0x14(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
c010152d:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0101530:	89 c2                	mov    %eax,%edx
c0101532:	ec                   	in     (%dx),%al
c0101533:	88 45 eb             	mov    %al,-0x15(%ebp)
    return data;
c0101536:	0f b6 45 eb          	movzbl -0x15(%ebp),%eax
    }

    data = inb(KBDATAP);
c010153a:	88 45 f3             	mov    %al,-0xd(%ebp)

    if (data == 0xE0) {
c010153d:	80 7d f3 e0          	cmpb   $0xe0,-0xd(%ebp)
c0101541:	75 17                	jne    c010155a <kbd_proc_data+0x64>
        // E0 escape character
        shift |= E0ESC;
c0101543:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c0101548:	83 c8 40             	or     $0x40,%eax
c010154b:	a3 68 c6 11 c0       	mov    %eax,0xc011c668
        return 0;
c0101550:	b8 00 00 00 00       	mov    $0x0,%eax
c0101555:	e9 23 01 00 00       	jmp    c010167d <kbd_proc_data+0x187>
    } else if (data & 0x80) {
c010155a:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c010155e:	84 c0                	test   %al,%al
c0101560:	79 45                	jns    c01015a7 <kbd_proc_data+0xb1>
        // Key released
        data = (shift & E0ESC ? data : data & 0x7F);
c0101562:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c0101567:	83 e0 40             	and    $0x40,%eax
c010156a:	85 c0                	test   %eax,%eax
c010156c:	75 08                	jne    c0101576 <kbd_proc_data+0x80>
c010156e:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c0101572:	24 7f                	and    $0x7f,%al
c0101574:	eb 04                	jmp    c010157a <kbd_proc_data+0x84>
c0101576:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c010157a:	88 45 f3             	mov    %al,-0xd(%ebp)
        shift &= ~(shiftcode[data] | E0ESC);
c010157d:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c0101581:	0f b6 80 40 90 11 c0 	movzbl -0x3fee6fc0(%eax),%eax
c0101588:	0c 40                	or     $0x40,%al
c010158a:	0f b6 c0             	movzbl %al,%eax
c010158d:	f7 d0                	not    %eax
c010158f:	89 c2                	mov    %eax,%edx
c0101591:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c0101596:	21 d0                	and    %edx,%eax
c0101598:	a3 68 c6 11 c0       	mov    %eax,0xc011c668
        return 0;
c010159d:	b8 00 00 00 00       	mov    $0x0,%eax
c01015a2:	e9 d6 00 00 00       	jmp    c010167d <kbd_proc_data+0x187>
    } else if (shift & E0ESC) {
c01015a7:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c01015ac:	83 e0 40             	and    $0x40,%eax
c01015af:	85 c0                	test   %eax,%eax
c01015b1:	74 11                	je     c01015c4 <kbd_proc_data+0xce>
        // Last character was an E0 escape; or with 0x80
        data |= 0x80;
c01015b3:	80 4d f3 80          	orb    $0x80,-0xd(%ebp)
        shift &= ~E0ESC;
c01015b7:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c01015bc:	83 e0 bf             	and    $0xffffffbf,%eax
c01015bf:	a3 68 c6 11 c0       	mov    %eax,0xc011c668
    }

    shift |= shiftcode[data];
c01015c4:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c01015c8:	0f b6 80 40 90 11 c0 	movzbl -0x3fee6fc0(%eax),%eax
c01015cf:	0f b6 d0             	movzbl %al,%edx
c01015d2:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c01015d7:	09 d0                	or     %edx,%eax
c01015d9:	a3 68 c6 11 c0       	mov    %eax,0xc011c668
    shift ^= togglecode[data];
c01015de:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c01015e2:	0f b6 80 40 91 11 c0 	movzbl -0x3fee6ec0(%eax),%eax
c01015e9:	0f b6 d0             	movzbl %al,%edx
c01015ec:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c01015f1:	31 d0                	xor    %edx,%eax
c01015f3:	a3 68 c6 11 c0       	mov    %eax,0xc011c668

    c = charcode[shift & (CTL | SHIFT)][data];
c01015f8:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c01015fd:	83 e0 03             	and    $0x3,%eax
c0101600:	8b 14 85 40 95 11 c0 	mov    -0x3fee6ac0(,%eax,4),%edx
c0101607:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
c010160b:	01 d0                	add    %edx,%eax
c010160d:	0f b6 00             	movzbl (%eax),%eax
c0101610:	0f b6 c0             	movzbl %al,%eax
c0101613:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (shift & CAPSLOCK) {
c0101616:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c010161b:	83 e0 08             	and    $0x8,%eax
c010161e:	85 c0                	test   %eax,%eax
c0101620:	74 22                	je     c0101644 <kbd_proc_data+0x14e>
        if ('a' <= c && c <= 'z')
c0101622:	83 7d f4 60          	cmpl   $0x60,-0xc(%ebp)
c0101626:	7e 0c                	jle    c0101634 <kbd_proc_data+0x13e>
c0101628:	83 7d f4 7a          	cmpl   $0x7a,-0xc(%ebp)
c010162c:	7f 06                	jg     c0101634 <kbd_proc_data+0x13e>
            c += 'A' - 'a';
c010162e:	83 6d f4 20          	subl   $0x20,-0xc(%ebp)
c0101632:	eb 10                	jmp    c0101644 <kbd_proc_data+0x14e>
        else if ('A' <= c && c <= 'Z')
c0101634:	83 7d f4 40          	cmpl   $0x40,-0xc(%ebp)
c0101638:	7e 0a                	jle    c0101644 <kbd_proc_data+0x14e>
c010163a:	83 7d f4 5a          	cmpl   $0x5a,-0xc(%ebp)
c010163e:	7f 04                	jg     c0101644 <kbd_proc_data+0x14e>
            c += 'a' - 'A';
c0101640:	83 45 f4 20          	addl   $0x20,-0xc(%ebp)
    }

    // Process special keys
    // Ctrl-Alt-Del: reboot
    if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
c0101644:	a1 68 c6 11 c0       	mov    0xc011c668,%eax
c0101649:	f7 d0                	not    %eax
c010164b:	83 e0 06             	and    $0x6,%eax
c010164e:	85 c0                	test   %eax,%eax
c0101650:	75 28                	jne    c010167a <kbd_proc_data+0x184>
c0101652:	81 7d f4 e9 00 00 00 	cmpl   $0xe9,-0xc(%ebp)
c0101659:	75 1f                	jne    c010167a <kbd_proc_data+0x184>
        cprintf("Rebooting!\n");
c010165b:	c7 04 24 61 65 10 c0 	movl   $0xc0106561,(%esp)
c0101662:	e8 53 ec ff ff       	call   c01002ba <cprintf>
c0101667:	66 c7 45 e8 92 00    	movw   $0x92,-0x18(%ebp)
c010166d:	c6 45 e7 03          	movb   $0x3,-0x19(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101671:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
c0101675:	8b 55 e8             	mov    -0x18(%ebp),%edx
c0101678:	ee                   	out    %al,(%dx)
}
c0101679:	90                   	nop
        outb(0x92, 0x3); // courtesy of Chris Frost
    }
    return c;
c010167a:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c010167d:	c9                   	leave  
c010167e:	c3                   	ret    

c010167f <kbd_intr>:

/* kbd_intr - try to feed input characters from keyboard */
static void
kbd_intr(void) {
c010167f:	f3 0f 1e fb          	endbr32 
c0101683:	55                   	push   %ebp
c0101684:	89 e5                	mov    %esp,%ebp
c0101686:	83 ec 18             	sub    $0x18,%esp
    cons_intr(kbd_proc_data);
c0101689:	c7 04 24 f6 14 10 c0 	movl   $0xc01014f6,(%esp)
c0101690:	e8 93 fd ff ff       	call   c0101428 <cons_intr>
}
c0101695:	90                   	nop
c0101696:	c9                   	leave  
c0101697:	c3                   	ret    

c0101698 <kbd_init>:

static void
kbd_init(void) {
c0101698:	f3 0f 1e fb          	endbr32 
c010169c:	55                   	push   %ebp
c010169d:	89 e5                	mov    %esp,%ebp
c010169f:	83 ec 18             	sub    $0x18,%esp
    // drain the kbd buffer
    kbd_intr();
c01016a2:	e8 d8 ff ff ff       	call   c010167f <kbd_intr>
    pic_enable(IRQ_KBD);
c01016a7:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c01016ae:	e8 47 01 00 00       	call   c01017fa <pic_enable>
}
c01016b3:	90                   	nop
c01016b4:	c9                   	leave  
c01016b5:	c3                   	ret    

c01016b6 <cons_init>:

/* cons_init - initializes the console devices */
void
cons_init(void) {
c01016b6:	f3 0f 1e fb          	endbr32 
c01016ba:	55                   	push   %ebp
c01016bb:	89 e5                	mov    %esp,%ebp
c01016bd:	83 ec 18             	sub    $0x18,%esp
    cga_init();
c01016c0:	e8 2e f8 ff ff       	call   c0100ef3 <cga_init>
    serial_init();
c01016c5:	e8 13 f9 ff ff       	call   c0100fdd <serial_init>
    kbd_init();
c01016ca:	e8 c9 ff ff ff       	call   c0101698 <kbd_init>
    if (!serial_exists) {
c01016cf:	a1 48 c4 11 c0       	mov    0xc011c448,%eax
c01016d4:	85 c0                	test   %eax,%eax
c01016d6:	75 0c                	jne    c01016e4 <cons_init+0x2e>
        cprintf("serial port does not exist!!\n");
c01016d8:	c7 04 24 6d 65 10 c0 	movl   $0xc010656d,(%esp)
c01016df:	e8 d6 eb ff ff       	call   c01002ba <cprintf>
    }
}
c01016e4:	90                   	nop
c01016e5:	c9                   	leave  
c01016e6:	c3                   	ret    

c01016e7 <cons_putc>:

/* cons_putc - print a single character @c to console devices */
void
cons_putc(int c) {
c01016e7:	f3 0f 1e fb          	endbr32 
c01016eb:	55                   	push   %ebp
c01016ec:	89 e5                	mov    %esp,%ebp
c01016ee:	83 ec 28             	sub    $0x28,%esp
    bool intr_flag;
    local_intr_save(intr_flag);
c01016f1:	e8 72 f7 ff ff       	call   c0100e68 <__intr_save>
c01016f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        lpt_putc(c);
c01016f9:	8b 45 08             	mov    0x8(%ebp),%eax
c01016fc:	89 04 24             	mov    %eax,(%esp)
c01016ff:	e8 48 fa ff ff       	call   c010114c <lpt_putc>
        cga_putc(c);
c0101704:	8b 45 08             	mov    0x8(%ebp),%eax
c0101707:	89 04 24             	mov    %eax,(%esp)
c010170a:	e8 81 fa ff ff       	call   c0101190 <cga_putc>
        serial_putc(c);
c010170f:	8b 45 08             	mov    0x8(%ebp),%eax
c0101712:	89 04 24             	mov    %eax,(%esp)
c0101715:	e8 ca fc ff ff       	call   c01013e4 <serial_putc>
    }
    local_intr_restore(intr_flag);
c010171a:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010171d:	89 04 24             	mov    %eax,(%esp)
c0101720:	e8 6d f7 ff ff       	call   c0100e92 <__intr_restore>
}
c0101725:	90                   	nop
c0101726:	c9                   	leave  
c0101727:	c3                   	ret    

c0101728 <cons_getc>:
/* *
 * cons_getc - return the next input character from console,
 * or 0 if none waiting.
 * */
int
cons_getc(void) {
c0101728:	f3 0f 1e fb          	endbr32 
c010172c:	55                   	push   %ebp
c010172d:	89 e5                	mov    %esp,%ebp
c010172f:	83 ec 28             	sub    $0x28,%esp
    int c = 0;
c0101732:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    bool intr_flag;
    local_intr_save(intr_flag);
c0101739:	e8 2a f7 ff ff       	call   c0100e68 <__intr_save>
c010173e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    {
        // poll for any pending input characters,
        // so that this function works even when interrupts are disabled
        // (e.g., when called from the kernel monitor).
        serial_intr();
c0101741:	e8 8e fd ff ff       	call   c01014d4 <serial_intr>
        kbd_intr();
c0101746:	e8 34 ff ff ff       	call   c010167f <kbd_intr>

        // grab the next character from the input buffer.
        if (cons.rpos != cons.wpos) {
c010174b:	8b 15 60 c6 11 c0    	mov    0xc011c660,%edx
c0101751:	a1 64 c6 11 c0       	mov    0xc011c664,%eax
c0101756:	39 c2                	cmp    %eax,%edx
c0101758:	74 31                	je     c010178b <cons_getc+0x63>
            c = cons.buf[cons.rpos ++];
c010175a:	a1 60 c6 11 c0       	mov    0xc011c660,%eax
c010175f:	8d 50 01             	lea    0x1(%eax),%edx
c0101762:	89 15 60 c6 11 c0    	mov    %edx,0xc011c660
c0101768:	0f b6 80 60 c4 11 c0 	movzbl -0x3fee3ba0(%eax),%eax
c010176f:	0f b6 c0             	movzbl %al,%eax
c0101772:	89 45 f4             	mov    %eax,-0xc(%ebp)
            if (cons.rpos == CONSBUFSIZE) {
c0101775:	a1 60 c6 11 c0       	mov    0xc011c660,%eax
c010177a:	3d 00 02 00 00       	cmp    $0x200,%eax
c010177f:	75 0a                	jne    c010178b <cons_getc+0x63>
                cons.rpos = 0;
c0101781:	c7 05 60 c6 11 c0 00 	movl   $0x0,0xc011c660
c0101788:	00 00 00 
            }
        }
    }
    local_intr_restore(intr_flag);
c010178b:	8b 45 f0             	mov    -0x10(%ebp),%eax
c010178e:	89 04 24             	mov    %eax,(%esp)
c0101791:	e8 fc f6 ff ff       	call   c0100e92 <__intr_restore>
    return c;
c0101796:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c0101799:	c9                   	leave  
c010179a:	c3                   	ret    

c010179b <pic_setmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static uint16_t irq_mask = 0xFFFF & ~(1 << IRQ_SLAVE);
static bool did_init = 0;

static void
pic_setmask(uint16_t mask) {
c010179b:	f3 0f 1e fb          	endbr32 
c010179f:	55                   	push   %ebp
c01017a0:	89 e5                	mov    %esp,%ebp
c01017a2:	83 ec 14             	sub    $0x14,%esp
c01017a5:	8b 45 08             	mov    0x8(%ebp),%eax
c01017a8:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
    irq_mask = mask;
c01017ac:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01017af:	66 a3 50 95 11 c0    	mov    %ax,0xc0119550
    if (did_init) {
c01017b5:	a1 6c c6 11 c0       	mov    0xc011c66c,%eax
c01017ba:	85 c0                	test   %eax,%eax
c01017bc:	74 39                	je     c01017f7 <pic_setmask+0x5c>
        outb(IO_PIC1 + 1, mask);
c01017be:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01017c1:	0f b6 c0             	movzbl %al,%eax
c01017c4:	66 c7 45 fa 21 00    	movw   $0x21,-0x6(%ebp)
c01017ca:	88 45 f9             	mov    %al,-0x7(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01017cd:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
c01017d1:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
c01017d5:	ee                   	out    %al,(%dx)
}
c01017d6:	90                   	nop
        outb(IO_PIC2 + 1, mask >> 8);
c01017d7:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
c01017db:	c1 e8 08             	shr    $0x8,%eax
c01017de:	0f b7 c0             	movzwl %ax,%eax
c01017e1:	0f b6 c0             	movzbl %al,%eax
c01017e4:	66 c7 45 fe a1 00    	movw   $0xa1,-0x2(%ebp)
c01017ea:	88 45 fd             	mov    %al,-0x3(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01017ed:	0f b6 45 fd          	movzbl -0x3(%ebp),%eax
c01017f1:	0f b7 55 fe          	movzwl -0x2(%ebp),%edx
c01017f5:	ee                   	out    %al,(%dx)
}
c01017f6:	90                   	nop
    }
}
c01017f7:	90                   	nop
c01017f8:	c9                   	leave  
c01017f9:	c3                   	ret    

c01017fa <pic_enable>:

void
pic_enable(unsigned int irq) {
c01017fa:	f3 0f 1e fb          	endbr32 
c01017fe:	55                   	push   %ebp
c01017ff:	89 e5                	mov    %esp,%ebp
c0101801:	83 ec 04             	sub    $0x4,%esp
    pic_setmask(irq_mask & ~(1 << irq));
c0101804:	8b 45 08             	mov    0x8(%ebp),%eax
c0101807:	ba 01 00 00 00       	mov    $0x1,%edx
c010180c:	88 c1                	mov    %al,%cl
c010180e:	d3 e2                	shl    %cl,%edx
c0101810:	89 d0                	mov    %edx,%eax
c0101812:	98                   	cwtl   
c0101813:	f7 d0                	not    %eax
c0101815:	0f bf d0             	movswl %ax,%edx
c0101818:	0f b7 05 50 95 11 c0 	movzwl 0xc0119550,%eax
c010181f:	98                   	cwtl   
c0101820:	21 d0                	and    %edx,%eax
c0101822:	98                   	cwtl   
c0101823:	0f b7 c0             	movzwl %ax,%eax
c0101826:	89 04 24             	mov    %eax,(%esp)
c0101829:	e8 6d ff ff ff       	call   c010179b <pic_setmask>
}
c010182e:	90                   	nop
c010182f:	c9                   	leave  
c0101830:	c3                   	ret    

c0101831 <pic_init>:

/* pic_init - initialize the 8259A interrupt controllers */
void
pic_init(void) {
c0101831:	f3 0f 1e fb          	endbr32 
c0101835:	55                   	push   %ebp
c0101836:	89 e5                	mov    %esp,%ebp
c0101838:	83 ec 44             	sub    $0x44,%esp
    did_init = 1;
c010183b:	c7 05 6c c6 11 c0 01 	movl   $0x1,0xc011c66c
c0101842:	00 00 00 
c0101845:	66 c7 45 ca 21 00    	movw   $0x21,-0x36(%ebp)
c010184b:	c6 45 c9 ff          	movb   $0xff,-0x37(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010184f:	0f b6 45 c9          	movzbl -0x37(%ebp),%eax
c0101853:	0f b7 55 ca          	movzwl -0x36(%ebp),%edx
c0101857:	ee                   	out    %al,(%dx)
}
c0101858:	90                   	nop
c0101859:	66 c7 45 ce a1 00    	movw   $0xa1,-0x32(%ebp)
c010185f:	c6 45 cd ff          	movb   $0xff,-0x33(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101863:	0f b6 45 cd          	movzbl -0x33(%ebp),%eax
c0101867:	0f b7 55 ce          	movzwl -0x32(%ebp),%edx
c010186b:	ee                   	out    %al,(%dx)
}
c010186c:	90                   	nop
c010186d:	66 c7 45 d2 20 00    	movw   $0x20,-0x2e(%ebp)
c0101873:	c6 45 d1 11          	movb   $0x11,-0x2f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101877:	0f b6 45 d1          	movzbl -0x2f(%ebp),%eax
c010187b:	0f b7 55 d2          	movzwl -0x2e(%ebp),%edx
c010187f:	ee                   	out    %al,(%dx)
}
c0101880:	90                   	nop
c0101881:	66 c7 45 d6 21 00    	movw   $0x21,-0x2a(%ebp)
c0101887:	c6 45 d5 20          	movb   $0x20,-0x2b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010188b:	0f b6 45 d5          	movzbl -0x2b(%ebp),%eax
c010188f:	0f b7 55 d6          	movzwl -0x2a(%ebp),%edx
c0101893:	ee                   	out    %al,(%dx)
}
c0101894:	90                   	nop
c0101895:	66 c7 45 da 21 00    	movw   $0x21,-0x26(%ebp)
c010189b:	c6 45 d9 04          	movb   $0x4,-0x27(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010189f:	0f b6 45 d9          	movzbl -0x27(%ebp),%eax
c01018a3:	0f b7 55 da          	movzwl -0x26(%ebp),%edx
c01018a7:	ee                   	out    %al,(%dx)
}
c01018a8:	90                   	nop
c01018a9:	66 c7 45 de 21 00    	movw   $0x21,-0x22(%ebp)
c01018af:	c6 45 dd 03          	movb   $0x3,-0x23(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01018b3:	0f b6 45 dd          	movzbl -0x23(%ebp),%eax
c01018b7:	0f b7 55 de          	movzwl -0x22(%ebp),%edx
c01018bb:	ee                   	out    %al,(%dx)
}
c01018bc:	90                   	nop
c01018bd:	66 c7 45 e2 a0 00    	movw   $0xa0,-0x1e(%ebp)
c01018c3:	c6 45 e1 11          	movb   $0x11,-0x1f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01018c7:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
c01018cb:	0f b7 55 e2          	movzwl -0x1e(%ebp),%edx
c01018cf:	ee                   	out    %al,(%dx)
}
c01018d0:	90                   	nop
c01018d1:	66 c7 45 e6 a1 00    	movw   $0xa1,-0x1a(%ebp)
c01018d7:	c6 45 e5 28          	movb   $0x28,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01018db:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
c01018df:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
c01018e3:	ee                   	out    %al,(%dx)
}
c01018e4:	90                   	nop
c01018e5:	66 c7 45 ea a1 00    	movw   $0xa1,-0x16(%ebp)
c01018eb:	c6 45 e9 02          	movb   $0x2,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c01018ef:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
c01018f3:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
c01018f7:	ee                   	out    %al,(%dx)
}
c01018f8:	90                   	nop
c01018f9:	66 c7 45 ee a1 00    	movw   $0xa1,-0x12(%ebp)
c01018ff:	c6 45 ed 03          	movb   $0x3,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101903:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
c0101907:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
c010190b:	ee                   	out    %al,(%dx)
}
c010190c:	90                   	nop
c010190d:	66 c7 45 f2 20 00    	movw   $0x20,-0xe(%ebp)
c0101913:	c6 45 f1 68          	movb   $0x68,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101917:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
c010191b:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
c010191f:	ee                   	out    %al,(%dx)
}
c0101920:	90                   	nop
c0101921:	66 c7 45 f6 20 00    	movw   $0x20,-0xa(%ebp)
c0101927:	c6 45 f5 0a          	movb   $0xa,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010192b:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
c010192f:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
c0101933:	ee                   	out    %al,(%dx)
}
c0101934:	90                   	nop
c0101935:	66 c7 45 fa a0 00    	movw   $0xa0,-0x6(%ebp)
c010193b:	c6 45 f9 68          	movb   $0x68,-0x7(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c010193f:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
c0101943:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
c0101947:	ee                   	out    %al,(%dx)
}
c0101948:	90                   	nop
c0101949:	66 c7 45 fe a0 00    	movw   $0xa0,-0x2(%ebp)
c010194f:	c6 45 fd 0a          	movb   $0xa,-0x3(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
c0101953:	0f b6 45 fd          	movzbl -0x3(%ebp),%eax
c0101957:	0f b7 55 fe          	movzwl -0x2(%ebp),%edx
c010195b:	ee                   	out    %al,(%dx)
}
c010195c:	90                   	nop
    outb(IO_PIC1, 0x0a);    // read IRR by default

    outb(IO_PIC2, 0x68);    // OCW3
    outb(IO_PIC2, 0x0a);    // OCW3

    if (irq_mask != 0xFFFF) {
c010195d:	0f b7 05 50 95 11 c0 	movzwl 0xc0119550,%eax
c0101964:	3d ff ff 00 00       	cmp    $0xffff,%eax
c0101969:	74 0f                	je     c010197a <pic_init+0x149>
        pic_setmask(irq_mask);
c010196b:	0f b7 05 50 95 11 c0 	movzwl 0xc0119550,%eax
c0101972:	89 04 24             	mov    %eax,(%esp)
c0101975:	e8 21 fe ff ff       	call   c010179b <pic_setmask>
    }
}
c010197a:	90                   	nop
c010197b:	c9                   	leave  
c010197c:	c3                   	ret    

c010197d <intr_enable>:
#include <x86.h>
#include <intr.h>

/* intr_enable - enable irq interrupt */
void
intr_enable(void) {
c010197d:	f3 0f 1e fb          	endbr32 
c0101981:	55                   	push   %ebp
c0101982:	89 e5                	mov    %esp,%ebp
    asm volatile ("sti");
c0101984:	fb                   	sti    
}
c0101985:	90                   	nop
    sti();
}
c0101986:	90                   	nop
c0101987:	5d                   	pop    %ebp
c0101988:	c3                   	ret    

c0101989 <intr_disable>:

/* intr_disable - disable irq interrupt */
void
intr_disable(void) {
c0101989:	f3 0f 1e fb          	endbr32 
c010198d:	55                   	push   %ebp
c010198e:	89 e5                	mov    %esp,%ebp
    asm volatile ("cli" ::: "memory");
c0101990:	fa                   	cli    
}
c0101991:	90                   	nop
    cli();
}
c0101992:	90                   	nop
c0101993:	5d                   	pop    %ebp
c0101994:	c3                   	ret    

c0101995 <print_ticks>:
#include <console.h>
#include <kdebug.h>

#define TICK_NUM 100

static void print_ticks() {
c0101995:	f3 0f 1e fb          	endbr32 
c0101999:	55                   	push   %ebp
c010199a:	89 e5                	mov    %esp,%ebp
c010199c:	83 ec 18             	sub    $0x18,%esp
    cprintf("%d ticks\n",TICK_NUM);
c010199f:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
c01019a6:	00 
c01019a7:	c7 04 24 a0 65 10 c0 	movl   $0xc01065a0,(%esp)
c01019ae:	e8 07 e9 ff ff       	call   c01002ba <cprintf>
#ifdef DEBUG_GRADE
    cprintf("End of Test.\n");
c01019b3:	c7 04 24 aa 65 10 c0 	movl   $0xc01065aa,(%esp)
c01019ba:	e8 fb e8 ff ff       	call   c01002ba <cprintf>
    panic("EOT: kernel seems ok.");
c01019bf:	c7 44 24 08 b8 65 10 	movl   $0xc01065b8,0x8(%esp)
c01019c6:	c0 
c01019c7:	c7 44 24 04 12 00 00 	movl   $0x12,0x4(%esp)
c01019ce:	00 
c01019cf:	c7 04 24 ce 65 10 c0 	movl   $0xc01065ce,(%esp)
c01019d6:	e8 4b ea ff ff       	call   c0100426 <__panic>

c01019db <idt_init>:
    sizeof(idt) - 1, (uintptr_t)idt
};

/* idt_init - initialize IDT to each of the entry points in kern/trap/vectors.S */
void
idt_init(void) {
c01019db:	f3 0f 1e fb          	endbr32 
c01019df:	55                   	push   %ebp
c01019e0:	89 e5                	mov    %esp,%ebp
c01019e2:	83 ec 10             	sub    $0x10,%esp
      *     You don't know the meaning of this instruction? just google it! and check the libs/x86.h to know more.
      *     Notice: the argument of lidt is idt_pd. try to find it!
      */
      extern uintptr_t __vectors[];
      int i;
      for(i=0;i<sizeof(idt)/sizeof(struct gatedesc);i++)
c01019e5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
c01019ec:	e9 50 01 00 00       	jmp    c0101b41 <idt_init+0x166>
      {
      SETGATE(idt[i],0,GD_KTEXT,__vectors[i],DPL_KERNEL);
c01019f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01019f4:	8b 04 85 e0 95 11 c0 	mov    -0x3fee6a20(,%eax,4),%eax
c01019fb:	0f b7 d0             	movzwl %ax,%edx
c01019fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a01:	66 89 14 c5 80 c6 11 	mov    %dx,-0x3fee3980(,%eax,8)
c0101a08:	c0 
c0101a09:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a0c:	66 c7 04 c5 82 c6 11 	movw   $0x8,-0x3fee397e(,%eax,8)
c0101a13:	c0 08 00 
c0101a16:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a19:	0f b6 14 c5 84 c6 11 	movzbl -0x3fee397c(,%eax,8),%edx
c0101a20:	c0 
c0101a21:	80 e2 e0             	and    $0xe0,%dl
c0101a24:	88 14 c5 84 c6 11 c0 	mov    %dl,-0x3fee397c(,%eax,8)
c0101a2b:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a2e:	0f b6 14 c5 84 c6 11 	movzbl -0x3fee397c(,%eax,8),%edx
c0101a35:	c0 
c0101a36:	80 e2 1f             	and    $0x1f,%dl
c0101a39:	88 14 c5 84 c6 11 c0 	mov    %dl,-0x3fee397c(,%eax,8)
c0101a40:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a43:	0f b6 14 c5 85 c6 11 	movzbl -0x3fee397b(,%eax,8),%edx
c0101a4a:	c0 
c0101a4b:	80 e2 f0             	and    $0xf0,%dl
c0101a4e:	80 ca 0e             	or     $0xe,%dl
c0101a51:	88 14 c5 85 c6 11 c0 	mov    %dl,-0x3fee397b(,%eax,8)
c0101a58:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a5b:	0f b6 14 c5 85 c6 11 	movzbl -0x3fee397b(,%eax,8),%edx
c0101a62:	c0 
c0101a63:	80 e2 ef             	and    $0xef,%dl
c0101a66:	88 14 c5 85 c6 11 c0 	mov    %dl,-0x3fee397b(,%eax,8)
c0101a6d:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a70:	0f b6 14 c5 85 c6 11 	movzbl -0x3fee397b(,%eax,8),%edx
c0101a77:	c0 
c0101a78:	80 e2 9f             	and    $0x9f,%dl
c0101a7b:	88 14 c5 85 c6 11 c0 	mov    %dl,-0x3fee397b(,%eax,8)
c0101a82:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a85:	0f b6 14 c5 85 c6 11 	movzbl -0x3fee397b(,%eax,8),%edx
c0101a8c:	c0 
c0101a8d:	80 ca 80             	or     $0x80,%dl
c0101a90:	88 14 c5 85 c6 11 c0 	mov    %dl,-0x3fee397b(,%eax,8)
c0101a97:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101a9a:	8b 04 85 e0 95 11 c0 	mov    -0x3fee6a20(,%eax,4),%eax
c0101aa1:	c1 e8 10             	shr    $0x10,%eax
c0101aa4:	0f b7 d0             	movzwl %ax,%edx
c0101aa7:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101aaa:	66 89 14 c5 86 c6 11 	mov    %dx,-0x3fee397a(,%eax,8)
c0101ab1:	c0 
      SETGATE(idt[T_SWITCH_TOK],0,GD_KTEXT,__vectors[T_SWITCH_TOK],DPL_USER);
c0101ab2:	a1 c4 97 11 c0       	mov    0xc01197c4,%eax
c0101ab7:	0f b7 c0             	movzwl %ax,%eax
c0101aba:	66 a3 48 ca 11 c0    	mov    %ax,0xc011ca48
c0101ac0:	66 c7 05 4a ca 11 c0 	movw   $0x8,0xc011ca4a
c0101ac7:	08 00 
c0101ac9:	0f b6 05 4c ca 11 c0 	movzbl 0xc011ca4c,%eax
c0101ad0:	24 e0                	and    $0xe0,%al
c0101ad2:	a2 4c ca 11 c0       	mov    %al,0xc011ca4c
c0101ad7:	0f b6 05 4c ca 11 c0 	movzbl 0xc011ca4c,%eax
c0101ade:	24 1f                	and    $0x1f,%al
c0101ae0:	a2 4c ca 11 c0       	mov    %al,0xc011ca4c
c0101ae5:	0f b6 05 4d ca 11 c0 	movzbl 0xc011ca4d,%eax
c0101aec:	24 f0                	and    $0xf0,%al
c0101aee:	0c 0e                	or     $0xe,%al
c0101af0:	a2 4d ca 11 c0       	mov    %al,0xc011ca4d
c0101af5:	0f b6 05 4d ca 11 c0 	movzbl 0xc011ca4d,%eax
c0101afc:	24 ef                	and    $0xef,%al
c0101afe:	a2 4d ca 11 c0       	mov    %al,0xc011ca4d
c0101b03:	0f b6 05 4d ca 11 c0 	movzbl 0xc011ca4d,%eax
c0101b0a:	0c 60                	or     $0x60,%al
c0101b0c:	a2 4d ca 11 c0       	mov    %al,0xc011ca4d
c0101b11:	0f b6 05 4d ca 11 c0 	movzbl 0xc011ca4d,%eax
c0101b18:	0c 80                	or     $0x80,%al
c0101b1a:	a2 4d ca 11 c0       	mov    %al,0xc011ca4d
c0101b1f:	a1 c4 97 11 c0       	mov    0xc01197c4,%eax
c0101b24:	c1 e8 10             	shr    $0x10,%eax
c0101b27:	0f b7 c0             	movzwl %ax,%eax
c0101b2a:	66 a3 4e ca 11 c0    	mov    %ax,0xc011ca4e
c0101b30:	c7 45 f8 60 95 11 c0 	movl   $0xc0119560,-0x8(%ebp)
    asm volatile ("lidt (%0)" :: "r" (pd) : "memory");
c0101b37:	8b 45 f8             	mov    -0x8(%ebp),%eax
c0101b3a:	0f 01 18             	lidtl  (%eax)
}
c0101b3d:	90                   	nop
      for(i=0;i<sizeof(idt)/sizeof(struct gatedesc);i++)
c0101b3e:	ff 45 fc             	incl   -0x4(%ebp)
c0101b41:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0101b44:	3d ff 00 00 00       	cmp    $0xff,%eax
c0101b49:	0f 86 a2 fe ff ff    	jbe    c01019f1 <idt_init+0x16>
      lidt(&idt_pd);
      }
}
c0101b4f:	90                   	nop
c0101b50:	90                   	nop
c0101b51:	c9                   	leave  
c0101b52:	c3                   	ret    

c0101b53 <trapname>:

static const char *
trapname(int trapno) {
c0101b53:	f3 0f 1e fb          	endbr32 
c0101b57:	55                   	push   %ebp
c0101b58:	89 e5                	mov    %esp,%ebp
        "Alignment Check",
        "Machine-Check",
        "SIMD Floating-Point Exception"
    };

    if (trapno < sizeof(excnames)/sizeof(const char * const)) {
c0101b5a:	8b 45 08             	mov    0x8(%ebp),%eax
c0101b5d:	83 f8 13             	cmp    $0x13,%eax
c0101b60:	77 0c                	ja     c0101b6e <trapname+0x1b>
        return excnames[trapno];
c0101b62:	8b 45 08             	mov    0x8(%ebp),%eax
c0101b65:	8b 04 85 20 69 10 c0 	mov    -0x3fef96e0(,%eax,4),%eax
c0101b6c:	eb 18                	jmp    c0101b86 <trapname+0x33>
    }
    if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16) {
c0101b6e:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
c0101b72:	7e 0d                	jle    c0101b81 <trapname+0x2e>
c0101b74:	83 7d 08 2f          	cmpl   $0x2f,0x8(%ebp)
c0101b78:	7f 07                	jg     c0101b81 <trapname+0x2e>
        return "Hardware Interrupt";
c0101b7a:	b8 df 65 10 c0       	mov    $0xc01065df,%eax
c0101b7f:	eb 05                	jmp    c0101b86 <trapname+0x33>
    }
    return "(unknown trap)";
c0101b81:	b8 f2 65 10 c0       	mov    $0xc01065f2,%eax
}
c0101b86:	5d                   	pop    %ebp
c0101b87:	c3                   	ret    

c0101b88 <trap_in_kernel>:

/* trap_in_kernel - test if trap happened in kernel */
bool
trap_in_kernel(struct trapframe *tf) {
c0101b88:	f3 0f 1e fb          	endbr32 
c0101b8c:	55                   	push   %ebp
c0101b8d:	89 e5                	mov    %esp,%ebp
    return (tf->tf_cs == (uint16_t)KERNEL_CS);
c0101b8f:	8b 45 08             	mov    0x8(%ebp),%eax
c0101b92:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
c0101b96:	83 f8 08             	cmp    $0x8,%eax
c0101b99:	0f 94 c0             	sete   %al
c0101b9c:	0f b6 c0             	movzbl %al,%eax
}
c0101b9f:	5d                   	pop    %ebp
c0101ba0:	c3                   	ret    

c0101ba1 <print_trapframe>:
    "TF", "IF", "DF", "OF", NULL, NULL, "NT", NULL,
    "RF", "VM", "AC", "VIF", "VIP", "ID", NULL, NULL,
};

void
print_trapframe(struct trapframe *tf) {
c0101ba1:	f3 0f 1e fb          	endbr32 
c0101ba5:	55                   	push   %ebp
c0101ba6:	89 e5                	mov    %esp,%ebp
c0101ba8:	83 ec 28             	sub    $0x28,%esp
    cprintf("trapframe at %p\n", tf);
c0101bab:	8b 45 08             	mov    0x8(%ebp),%eax
c0101bae:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101bb2:	c7 04 24 33 66 10 c0 	movl   $0xc0106633,(%esp)
c0101bb9:	e8 fc e6 ff ff       	call   c01002ba <cprintf>
    print_regs(&tf->tf_regs);
c0101bbe:	8b 45 08             	mov    0x8(%ebp),%eax
c0101bc1:	89 04 24             	mov    %eax,(%esp)
c0101bc4:	e8 8d 01 00 00       	call   c0101d56 <print_regs>
    cprintf("  ds   0x----%04x\n", tf->tf_ds);
c0101bc9:	8b 45 08             	mov    0x8(%ebp),%eax
c0101bcc:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
c0101bd0:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101bd4:	c7 04 24 44 66 10 c0 	movl   $0xc0106644,(%esp)
c0101bdb:	e8 da e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  es   0x----%04x\n", tf->tf_es);
c0101be0:	8b 45 08             	mov    0x8(%ebp),%eax
c0101be3:	0f b7 40 28          	movzwl 0x28(%eax),%eax
c0101be7:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101beb:	c7 04 24 57 66 10 c0 	movl   $0xc0106657,(%esp)
c0101bf2:	e8 c3 e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  fs   0x----%04x\n", tf->tf_fs);
c0101bf7:	8b 45 08             	mov    0x8(%ebp),%eax
c0101bfa:	0f b7 40 24          	movzwl 0x24(%eax),%eax
c0101bfe:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c02:	c7 04 24 6a 66 10 c0 	movl   $0xc010666a,(%esp)
c0101c09:	e8 ac e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  gs   0x----%04x\n", tf->tf_gs);
c0101c0e:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c11:	0f b7 40 20          	movzwl 0x20(%eax),%eax
c0101c15:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c19:	c7 04 24 7d 66 10 c0 	movl   $0xc010667d,(%esp)
c0101c20:	e8 95 e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
c0101c25:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c28:	8b 40 30             	mov    0x30(%eax),%eax
c0101c2b:	89 04 24             	mov    %eax,(%esp)
c0101c2e:	e8 20 ff ff ff       	call   c0101b53 <trapname>
c0101c33:	8b 55 08             	mov    0x8(%ebp),%edx
c0101c36:	8b 52 30             	mov    0x30(%edx),%edx
c0101c39:	89 44 24 08          	mov    %eax,0x8(%esp)
c0101c3d:	89 54 24 04          	mov    %edx,0x4(%esp)
c0101c41:	c7 04 24 90 66 10 c0 	movl   $0xc0106690,(%esp)
c0101c48:	e8 6d e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  err  0x%08x\n", tf->tf_err);
c0101c4d:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c50:	8b 40 34             	mov    0x34(%eax),%eax
c0101c53:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c57:	c7 04 24 a2 66 10 c0 	movl   $0xc01066a2,(%esp)
c0101c5e:	e8 57 e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  eip  0x%08x\n", tf->tf_eip);
c0101c63:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c66:	8b 40 38             	mov    0x38(%eax),%eax
c0101c69:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c6d:	c7 04 24 b1 66 10 c0 	movl   $0xc01066b1,(%esp)
c0101c74:	e8 41 e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  cs   0x----%04x\n", tf->tf_cs);
c0101c79:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c7c:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
c0101c80:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c84:	c7 04 24 c0 66 10 c0 	movl   $0xc01066c0,(%esp)
c0101c8b:	e8 2a e6 ff ff       	call   c01002ba <cprintf>
    cprintf("  flag 0x%08x ", tf->tf_eflags);
c0101c90:	8b 45 08             	mov    0x8(%ebp),%eax
c0101c93:	8b 40 40             	mov    0x40(%eax),%eax
c0101c96:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101c9a:	c7 04 24 d3 66 10 c0 	movl   $0xc01066d3,(%esp)
c0101ca1:	e8 14 e6 ff ff       	call   c01002ba <cprintf>

    int i, j;
    for (i = 0, j = 1; i < sizeof(IA32flags) / sizeof(IA32flags[0]); i ++, j <<= 1) {
c0101ca6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0101cad:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
c0101cb4:	eb 3d                	jmp    c0101cf3 <print_trapframe+0x152>
        if ((tf->tf_eflags & j) && IA32flags[i] != NULL) {
c0101cb6:	8b 45 08             	mov    0x8(%ebp),%eax
c0101cb9:	8b 50 40             	mov    0x40(%eax),%edx
c0101cbc:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0101cbf:	21 d0                	and    %edx,%eax
c0101cc1:	85 c0                	test   %eax,%eax
c0101cc3:	74 28                	je     c0101ced <print_trapframe+0x14c>
c0101cc5:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0101cc8:	8b 04 85 80 95 11 c0 	mov    -0x3fee6a80(,%eax,4),%eax
c0101ccf:	85 c0                	test   %eax,%eax
c0101cd1:	74 1a                	je     c0101ced <print_trapframe+0x14c>
            cprintf("%s,", IA32flags[i]);
c0101cd3:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0101cd6:	8b 04 85 80 95 11 c0 	mov    -0x3fee6a80(,%eax,4),%eax
c0101cdd:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101ce1:	c7 04 24 e2 66 10 c0 	movl   $0xc01066e2,(%esp)
c0101ce8:	e8 cd e5 ff ff       	call   c01002ba <cprintf>
    for (i = 0, j = 1; i < sizeof(IA32flags) / sizeof(IA32flags[0]); i ++, j <<= 1) {
c0101ced:	ff 45 f4             	incl   -0xc(%ebp)
c0101cf0:	d1 65 f0             	shll   -0x10(%ebp)
c0101cf3:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0101cf6:	83 f8 17             	cmp    $0x17,%eax
c0101cf9:	76 bb                	jbe    c0101cb6 <print_trapframe+0x115>
        }
    }
    cprintf("IOPL=%d\n", (tf->tf_eflags & FL_IOPL_MASK) >> 12);
c0101cfb:	8b 45 08             	mov    0x8(%ebp),%eax
c0101cfe:	8b 40 40             	mov    0x40(%eax),%eax
c0101d01:	c1 e8 0c             	shr    $0xc,%eax
c0101d04:	83 e0 03             	and    $0x3,%eax
c0101d07:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d0b:	c7 04 24 e6 66 10 c0 	movl   $0xc01066e6,(%esp)
c0101d12:	e8 a3 e5 ff ff       	call   c01002ba <cprintf>

    if (!trap_in_kernel(tf)) {
c0101d17:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d1a:	89 04 24             	mov    %eax,(%esp)
c0101d1d:	e8 66 fe ff ff       	call   c0101b88 <trap_in_kernel>
c0101d22:	85 c0                	test   %eax,%eax
c0101d24:	75 2d                	jne    c0101d53 <print_trapframe+0x1b2>
        cprintf("  esp  0x%08x\n", tf->tf_esp);
c0101d26:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d29:	8b 40 44             	mov    0x44(%eax),%eax
c0101d2c:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d30:	c7 04 24 ef 66 10 c0 	movl   $0xc01066ef,(%esp)
c0101d37:	e8 7e e5 ff ff       	call   c01002ba <cprintf>
        cprintf("  ss   0x----%04x\n", tf->tf_ss);
c0101d3c:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d3f:	0f b7 40 48          	movzwl 0x48(%eax),%eax
c0101d43:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d47:	c7 04 24 fe 66 10 c0 	movl   $0xc01066fe,(%esp)
c0101d4e:	e8 67 e5 ff ff       	call   c01002ba <cprintf>
    }
}
c0101d53:	90                   	nop
c0101d54:	c9                   	leave  
c0101d55:	c3                   	ret    

c0101d56 <print_regs>:

void
print_regs(struct pushregs *regs) {
c0101d56:	f3 0f 1e fb          	endbr32 
c0101d5a:	55                   	push   %ebp
c0101d5b:	89 e5                	mov    %esp,%ebp
c0101d5d:	83 ec 18             	sub    $0x18,%esp
    cprintf("  edi  0x%08x\n", regs->reg_edi);
c0101d60:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d63:	8b 00                	mov    (%eax),%eax
c0101d65:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d69:	c7 04 24 11 67 10 c0 	movl   $0xc0106711,(%esp)
c0101d70:	e8 45 e5 ff ff       	call   c01002ba <cprintf>
    cprintf("  esi  0x%08x\n", regs->reg_esi);
c0101d75:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d78:	8b 40 04             	mov    0x4(%eax),%eax
c0101d7b:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d7f:	c7 04 24 20 67 10 c0 	movl   $0xc0106720,(%esp)
c0101d86:	e8 2f e5 ff ff       	call   c01002ba <cprintf>
    cprintf("  ebp  0x%08x\n", regs->reg_ebp);
c0101d8b:	8b 45 08             	mov    0x8(%ebp),%eax
c0101d8e:	8b 40 08             	mov    0x8(%eax),%eax
c0101d91:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101d95:	c7 04 24 2f 67 10 c0 	movl   $0xc010672f,(%esp)
c0101d9c:	e8 19 e5 ff ff       	call   c01002ba <cprintf>
    cprintf("  oesp 0x%08x\n", regs->reg_oesp);
c0101da1:	8b 45 08             	mov    0x8(%ebp),%eax
c0101da4:	8b 40 0c             	mov    0xc(%eax),%eax
c0101da7:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101dab:	c7 04 24 3e 67 10 c0 	movl   $0xc010673e,(%esp)
c0101db2:	e8 03 e5 ff ff       	call   c01002ba <cprintf>
    cprintf("  ebx  0x%08x\n", regs->reg_ebx);
c0101db7:	8b 45 08             	mov    0x8(%ebp),%eax
c0101dba:	8b 40 10             	mov    0x10(%eax),%eax
c0101dbd:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101dc1:	c7 04 24 4d 67 10 c0 	movl   $0xc010674d,(%esp)
c0101dc8:	e8 ed e4 ff ff       	call   c01002ba <cprintf>
    cprintf("  edx  0x%08x\n", regs->reg_edx);
c0101dcd:	8b 45 08             	mov    0x8(%ebp),%eax
c0101dd0:	8b 40 14             	mov    0x14(%eax),%eax
c0101dd3:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101dd7:	c7 04 24 5c 67 10 c0 	movl   $0xc010675c,(%esp)
c0101dde:	e8 d7 e4 ff ff       	call   c01002ba <cprintf>
    cprintf("  ecx  0x%08x\n", regs->reg_ecx);
c0101de3:	8b 45 08             	mov    0x8(%ebp),%eax
c0101de6:	8b 40 18             	mov    0x18(%eax),%eax
c0101de9:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101ded:	c7 04 24 6b 67 10 c0 	movl   $0xc010676b,(%esp)
c0101df4:	e8 c1 e4 ff ff       	call   c01002ba <cprintf>
    cprintf("  eax  0x%08x\n", regs->reg_eax);
c0101df9:	8b 45 08             	mov    0x8(%ebp),%eax
c0101dfc:	8b 40 1c             	mov    0x1c(%eax),%eax
c0101dff:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101e03:	c7 04 24 7a 67 10 c0 	movl   $0xc010677a,(%esp)
c0101e0a:	e8 ab e4 ff ff       	call   c01002ba <cprintf>
}
c0101e0f:	90                   	nop
c0101e10:	c9                   	leave  
c0101e11:	c3                   	ret    

c0101e12 <trap_dispatch>:

/* trap_dispatch - dispatch based on what type of trap occurred */
static void
trap_dispatch(struct trapframe *tf) {
c0101e12:	f3 0f 1e fb          	endbr32 
c0101e16:	55                   	push   %ebp
c0101e17:	89 e5                	mov    %esp,%ebp
c0101e19:	83 ec 28             	sub    $0x28,%esp
    char c;

    switch (tf->tf_trapno) {
c0101e1c:	8b 45 08             	mov    0x8(%ebp),%eax
c0101e1f:	8b 40 30             	mov    0x30(%eax),%eax
c0101e22:	83 f8 79             	cmp    $0x79,%eax
c0101e25:	0f 87 e6 00 00 00    	ja     c0101f11 <trap_dispatch+0xff>
c0101e2b:	83 f8 78             	cmp    $0x78,%eax
c0101e2e:	0f 83 c1 00 00 00    	jae    c0101ef5 <trap_dispatch+0xe3>
c0101e34:	83 f8 2f             	cmp    $0x2f,%eax
c0101e37:	0f 87 d4 00 00 00    	ja     c0101f11 <trap_dispatch+0xff>
c0101e3d:	83 f8 2e             	cmp    $0x2e,%eax
c0101e40:	0f 83 00 01 00 00    	jae    c0101f46 <trap_dispatch+0x134>
c0101e46:	83 f8 24             	cmp    $0x24,%eax
c0101e49:	74 5e                	je     c0101ea9 <trap_dispatch+0x97>
c0101e4b:	83 f8 24             	cmp    $0x24,%eax
c0101e4e:	0f 87 bd 00 00 00    	ja     c0101f11 <trap_dispatch+0xff>
c0101e54:	83 f8 20             	cmp    $0x20,%eax
c0101e57:	74 0a                	je     c0101e63 <trap_dispatch+0x51>
c0101e59:	83 f8 21             	cmp    $0x21,%eax
c0101e5c:	74 71                	je     c0101ecf <trap_dispatch+0xbd>
c0101e5e:	e9 ae 00 00 00       	jmp    c0101f11 <trap_dispatch+0xff>
        /* handle the timer interrupt */
        /* (1) After a timer interrupt, you should record this event using a global variable (increase it), such as ticks in kern/driver/clock.c
         * (2) Every TICK_NUM cycle, you can print some info using a funciton, such as print_ticks().
         * (3) Too Simple? Yes, I think so!
         */
         ticks++;
c0101e63:	a1 0c cf 11 c0       	mov    0xc011cf0c,%eax
c0101e68:	40                   	inc    %eax
c0101e69:	a3 0c cf 11 c0       	mov    %eax,0xc011cf0c
         if(ticks%TICK_NUM==0)
c0101e6e:	8b 0d 0c cf 11 c0    	mov    0xc011cf0c,%ecx
c0101e74:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
c0101e79:	89 c8                	mov    %ecx,%eax
c0101e7b:	f7 e2                	mul    %edx
c0101e7d:	c1 ea 05             	shr    $0x5,%edx
c0101e80:	89 d0                	mov    %edx,%eax
c0101e82:	c1 e0 02             	shl    $0x2,%eax
c0101e85:	01 d0                	add    %edx,%eax
c0101e87:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c0101e8e:	01 d0                	add    %edx,%eax
c0101e90:	c1 e0 02             	shl    $0x2,%eax
c0101e93:	29 c1                	sub    %eax,%ecx
c0101e95:	89 ca                	mov    %ecx,%edx
c0101e97:	85 d2                	test   %edx,%edx
c0101e99:	0f 85 aa 00 00 00    	jne    c0101f49 <trap_dispatch+0x137>
         print_ticks();
c0101e9f:	e8 f1 fa ff ff       	call   c0101995 <print_ticks>
         break;
c0101ea4:	e9 a0 00 00 00       	jmp    c0101f49 <trap_dispatch+0x137>
    case IRQ_OFFSET + IRQ_COM1:
        c = cons_getc();
c0101ea9:	e8 7a f8 ff ff       	call   c0101728 <cons_getc>
c0101eae:	88 45 f7             	mov    %al,-0x9(%ebp)
        cprintf("serial [%03d] %c\n", c, c);
c0101eb1:	0f be 55 f7          	movsbl -0x9(%ebp),%edx
c0101eb5:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
c0101eb9:	89 54 24 08          	mov    %edx,0x8(%esp)
c0101ebd:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101ec1:	c7 04 24 89 67 10 c0 	movl   $0xc0106789,(%esp)
c0101ec8:	e8 ed e3 ff ff       	call   c01002ba <cprintf>
        break;
c0101ecd:	eb 7b                	jmp    c0101f4a <trap_dispatch+0x138>
    case IRQ_OFFSET + IRQ_KBD:
        c = cons_getc();
c0101ecf:	e8 54 f8 ff ff       	call   c0101728 <cons_getc>
c0101ed4:	88 45 f7             	mov    %al,-0x9(%ebp)
        cprintf("kbd [%03d] %c\n", c, c);
c0101ed7:	0f be 55 f7          	movsbl -0x9(%ebp),%edx
c0101edb:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
c0101edf:	89 54 24 08          	mov    %edx,0x8(%esp)
c0101ee3:	89 44 24 04          	mov    %eax,0x4(%esp)
c0101ee7:	c7 04 24 9b 67 10 c0 	movl   $0xc010679b,(%esp)
c0101eee:	e8 c7 e3 ff ff       	call   c01002ba <cprintf>
        break;
c0101ef3:	eb 55                	jmp    c0101f4a <trap_dispatch+0x138>
    //LAB1 CHALLENGE 1 : YOUR CODE you should modify below codes.
    case T_SWITCH_TOU:
    case T_SWITCH_TOK:
        panic("T_SWITCH_** ??\n");
c0101ef5:	c7 44 24 08 aa 67 10 	movl   $0xc01067aa,0x8(%esp)
c0101efc:	c0 
c0101efd:	c7 44 24 04 ad 00 00 	movl   $0xad,0x4(%esp)
c0101f04:	00 
c0101f05:	c7 04 24 ce 65 10 c0 	movl   $0xc01065ce,(%esp)
c0101f0c:	e8 15 e5 ff ff       	call   c0100426 <__panic>
    case IRQ_OFFSET + IRQ_IDE2:
        /* do nothing */
        break;
    default:
        // in kernel, it must be a mistake
        if ((tf->tf_cs & 3) == 0) {
c0101f11:	8b 45 08             	mov    0x8(%ebp),%eax
c0101f14:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
c0101f18:	83 e0 03             	and    $0x3,%eax
c0101f1b:	85 c0                	test   %eax,%eax
c0101f1d:	75 2b                	jne    c0101f4a <trap_dispatch+0x138>
            print_trapframe(tf);
c0101f1f:	8b 45 08             	mov    0x8(%ebp),%eax
c0101f22:	89 04 24             	mov    %eax,(%esp)
c0101f25:	e8 77 fc ff ff       	call   c0101ba1 <print_trapframe>
            panic("unexpected trap in kernel.\n");
c0101f2a:	c7 44 24 08 ba 67 10 	movl   $0xc01067ba,0x8(%esp)
c0101f31:	c0 
c0101f32:	c7 44 24 04 b7 00 00 	movl   $0xb7,0x4(%esp)
c0101f39:	00 
c0101f3a:	c7 04 24 ce 65 10 c0 	movl   $0xc01065ce,(%esp)
c0101f41:	e8 e0 e4 ff ff       	call   c0100426 <__panic>
        break;
c0101f46:	90                   	nop
c0101f47:	eb 01                	jmp    c0101f4a <trap_dispatch+0x138>
         break;
c0101f49:	90                   	nop
        }
    }
}
c0101f4a:	90                   	nop
c0101f4b:	c9                   	leave  
c0101f4c:	c3                   	ret    

c0101f4d <trap>:
 * trap - handles or dispatches an exception/interrupt. if and when trap() returns,
 * the code in kern/trap/trapentry.S restores the old CPU state saved in the
 * trapframe and then uses the iret instruction to return from the exception.
 * */
void
trap(struct trapframe *tf) {
c0101f4d:	f3 0f 1e fb          	endbr32 
c0101f51:	55                   	push   %ebp
c0101f52:	89 e5                	mov    %esp,%ebp
c0101f54:	83 ec 18             	sub    $0x18,%esp
    // dispatch based on what type of trap occurred
    trap_dispatch(tf);
c0101f57:	8b 45 08             	mov    0x8(%ebp),%eax
c0101f5a:	89 04 24             	mov    %eax,(%esp)
c0101f5d:	e8 b0 fe ff ff       	call   c0101e12 <trap_dispatch>
}
c0101f62:	90                   	nop
c0101f63:	c9                   	leave  
c0101f64:	c3                   	ret    

c0101f65 <vector0>:
# handler
.text
.globl __alltraps
.globl vector0
vector0:
  pushl $0
c0101f65:	6a 00                	push   $0x0
  pushl $0
c0101f67:	6a 00                	push   $0x0
  jmp __alltraps
c0101f69:	e9 69 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f6e <vector1>:
.globl vector1
vector1:
  pushl $0
c0101f6e:	6a 00                	push   $0x0
  pushl $1
c0101f70:	6a 01                	push   $0x1
  jmp __alltraps
c0101f72:	e9 60 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f77 <vector2>:
.globl vector2
vector2:
  pushl $0
c0101f77:	6a 00                	push   $0x0
  pushl $2
c0101f79:	6a 02                	push   $0x2
  jmp __alltraps
c0101f7b:	e9 57 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f80 <vector3>:
.globl vector3
vector3:
  pushl $0
c0101f80:	6a 00                	push   $0x0
  pushl $3
c0101f82:	6a 03                	push   $0x3
  jmp __alltraps
c0101f84:	e9 4e 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f89 <vector4>:
.globl vector4
vector4:
  pushl $0
c0101f89:	6a 00                	push   $0x0
  pushl $4
c0101f8b:	6a 04                	push   $0x4
  jmp __alltraps
c0101f8d:	e9 45 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f92 <vector5>:
.globl vector5
vector5:
  pushl $0
c0101f92:	6a 00                	push   $0x0
  pushl $5
c0101f94:	6a 05                	push   $0x5
  jmp __alltraps
c0101f96:	e9 3c 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101f9b <vector6>:
.globl vector6
vector6:
  pushl $0
c0101f9b:	6a 00                	push   $0x0
  pushl $6
c0101f9d:	6a 06                	push   $0x6
  jmp __alltraps
c0101f9f:	e9 33 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fa4 <vector7>:
.globl vector7
vector7:
  pushl $0
c0101fa4:	6a 00                	push   $0x0
  pushl $7
c0101fa6:	6a 07                	push   $0x7
  jmp __alltraps
c0101fa8:	e9 2a 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fad <vector8>:
.globl vector8
vector8:
  pushl $8
c0101fad:	6a 08                	push   $0x8
  jmp __alltraps
c0101faf:	e9 23 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fb4 <vector9>:
.globl vector9
vector9:
  pushl $0
c0101fb4:	6a 00                	push   $0x0
  pushl $9
c0101fb6:	6a 09                	push   $0x9
  jmp __alltraps
c0101fb8:	e9 1a 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fbd <vector10>:
.globl vector10
vector10:
  pushl $10
c0101fbd:	6a 0a                	push   $0xa
  jmp __alltraps
c0101fbf:	e9 13 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fc4 <vector11>:
.globl vector11
vector11:
  pushl $11
c0101fc4:	6a 0b                	push   $0xb
  jmp __alltraps
c0101fc6:	e9 0c 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fcb <vector12>:
.globl vector12
vector12:
  pushl $12
c0101fcb:	6a 0c                	push   $0xc
  jmp __alltraps
c0101fcd:	e9 05 0a 00 00       	jmp    c01029d7 <__alltraps>

c0101fd2 <vector13>:
.globl vector13
vector13:
  pushl $13
c0101fd2:	6a 0d                	push   $0xd
  jmp __alltraps
c0101fd4:	e9 fe 09 00 00       	jmp    c01029d7 <__alltraps>

c0101fd9 <vector14>:
.globl vector14
vector14:
  pushl $14
c0101fd9:	6a 0e                	push   $0xe
  jmp __alltraps
c0101fdb:	e9 f7 09 00 00       	jmp    c01029d7 <__alltraps>

c0101fe0 <vector15>:
.globl vector15
vector15:
  pushl $0
c0101fe0:	6a 00                	push   $0x0
  pushl $15
c0101fe2:	6a 0f                	push   $0xf
  jmp __alltraps
c0101fe4:	e9 ee 09 00 00       	jmp    c01029d7 <__alltraps>

c0101fe9 <vector16>:
.globl vector16
vector16:
  pushl $0
c0101fe9:	6a 00                	push   $0x0
  pushl $16
c0101feb:	6a 10                	push   $0x10
  jmp __alltraps
c0101fed:	e9 e5 09 00 00       	jmp    c01029d7 <__alltraps>

c0101ff2 <vector17>:
.globl vector17
vector17:
  pushl $17
c0101ff2:	6a 11                	push   $0x11
  jmp __alltraps
c0101ff4:	e9 de 09 00 00       	jmp    c01029d7 <__alltraps>

c0101ff9 <vector18>:
.globl vector18
vector18:
  pushl $0
c0101ff9:	6a 00                	push   $0x0
  pushl $18
c0101ffb:	6a 12                	push   $0x12
  jmp __alltraps
c0101ffd:	e9 d5 09 00 00       	jmp    c01029d7 <__alltraps>

c0102002 <vector19>:
.globl vector19
vector19:
  pushl $0
c0102002:	6a 00                	push   $0x0
  pushl $19
c0102004:	6a 13                	push   $0x13
  jmp __alltraps
c0102006:	e9 cc 09 00 00       	jmp    c01029d7 <__alltraps>

c010200b <vector20>:
.globl vector20
vector20:
  pushl $0
c010200b:	6a 00                	push   $0x0
  pushl $20
c010200d:	6a 14                	push   $0x14
  jmp __alltraps
c010200f:	e9 c3 09 00 00       	jmp    c01029d7 <__alltraps>

c0102014 <vector21>:
.globl vector21
vector21:
  pushl $0
c0102014:	6a 00                	push   $0x0
  pushl $21
c0102016:	6a 15                	push   $0x15
  jmp __alltraps
c0102018:	e9 ba 09 00 00       	jmp    c01029d7 <__alltraps>

c010201d <vector22>:
.globl vector22
vector22:
  pushl $0
c010201d:	6a 00                	push   $0x0
  pushl $22
c010201f:	6a 16                	push   $0x16
  jmp __alltraps
c0102021:	e9 b1 09 00 00       	jmp    c01029d7 <__alltraps>

c0102026 <vector23>:
.globl vector23
vector23:
  pushl $0
c0102026:	6a 00                	push   $0x0
  pushl $23
c0102028:	6a 17                	push   $0x17
  jmp __alltraps
c010202a:	e9 a8 09 00 00       	jmp    c01029d7 <__alltraps>

c010202f <vector24>:
.globl vector24
vector24:
  pushl $0
c010202f:	6a 00                	push   $0x0
  pushl $24
c0102031:	6a 18                	push   $0x18
  jmp __alltraps
c0102033:	e9 9f 09 00 00       	jmp    c01029d7 <__alltraps>

c0102038 <vector25>:
.globl vector25
vector25:
  pushl $0
c0102038:	6a 00                	push   $0x0
  pushl $25
c010203a:	6a 19                	push   $0x19
  jmp __alltraps
c010203c:	e9 96 09 00 00       	jmp    c01029d7 <__alltraps>

c0102041 <vector26>:
.globl vector26
vector26:
  pushl $0
c0102041:	6a 00                	push   $0x0
  pushl $26
c0102043:	6a 1a                	push   $0x1a
  jmp __alltraps
c0102045:	e9 8d 09 00 00       	jmp    c01029d7 <__alltraps>

c010204a <vector27>:
.globl vector27
vector27:
  pushl $0
c010204a:	6a 00                	push   $0x0
  pushl $27
c010204c:	6a 1b                	push   $0x1b
  jmp __alltraps
c010204e:	e9 84 09 00 00       	jmp    c01029d7 <__alltraps>

c0102053 <vector28>:
.globl vector28
vector28:
  pushl $0
c0102053:	6a 00                	push   $0x0
  pushl $28
c0102055:	6a 1c                	push   $0x1c
  jmp __alltraps
c0102057:	e9 7b 09 00 00       	jmp    c01029d7 <__alltraps>

c010205c <vector29>:
.globl vector29
vector29:
  pushl $0
c010205c:	6a 00                	push   $0x0
  pushl $29
c010205e:	6a 1d                	push   $0x1d
  jmp __alltraps
c0102060:	e9 72 09 00 00       	jmp    c01029d7 <__alltraps>

c0102065 <vector30>:
.globl vector30
vector30:
  pushl $0
c0102065:	6a 00                	push   $0x0
  pushl $30
c0102067:	6a 1e                	push   $0x1e
  jmp __alltraps
c0102069:	e9 69 09 00 00       	jmp    c01029d7 <__alltraps>

c010206e <vector31>:
.globl vector31
vector31:
  pushl $0
c010206e:	6a 00                	push   $0x0
  pushl $31
c0102070:	6a 1f                	push   $0x1f
  jmp __alltraps
c0102072:	e9 60 09 00 00       	jmp    c01029d7 <__alltraps>

c0102077 <vector32>:
.globl vector32
vector32:
  pushl $0
c0102077:	6a 00                	push   $0x0
  pushl $32
c0102079:	6a 20                	push   $0x20
  jmp __alltraps
c010207b:	e9 57 09 00 00       	jmp    c01029d7 <__alltraps>

c0102080 <vector33>:
.globl vector33
vector33:
  pushl $0
c0102080:	6a 00                	push   $0x0
  pushl $33
c0102082:	6a 21                	push   $0x21
  jmp __alltraps
c0102084:	e9 4e 09 00 00       	jmp    c01029d7 <__alltraps>

c0102089 <vector34>:
.globl vector34
vector34:
  pushl $0
c0102089:	6a 00                	push   $0x0
  pushl $34
c010208b:	6a 22                	push   $0x22
  jmp __alltraps
c010208d:	e9 45 09 00 00       	jmp    c01029d7 <__alltraps>

c0102092 <vector35>:
.globl vector35
vector35:
  pushl $0
c0102092:	6a 00                	push   $0x0
  pushl $35
c0102094:	6a 23                	push   $0x23
  jmp __alltraps
c0102096:	e9 3c 09 00 00       	jmp    c01029d7 <__alltraps>

c010209b <vector36>:
.globl vector36
vector36:
  pushl $0
c010209b:	6a 00                	push   $0x0
  pushl $36
c010209d:	6a 24                	push   $0x24
  jmp __alltraps
c010209f:	e9 33 09 00 00       	jmp    c01029d7 <__alltraps>

c01020a4 <vector37>:
.globl vector37
vector37:
  pushl $0
c01020a4:	6a 00                	push   $0x0
  pushl $37
c01020a6:	6a 25                	push   $0x25
  jmp __alltraps
c01020a8:	e9 2a 09 00 00       	jmp    c01029d7 <__alltraps>

c01020ad <vector38>:
.globl vector38
vector38:
  pushl $0
c01020ad:	6a 00                	push   $0x0
  pushl $38
c01020af:	6a 26                	push   $0x26
  jmp __alltraps
c01020b1:	e9 21 09 00 00       	jmp    c01029d7 <__alltraps>

c01020b6 <vector39>:
.globl vector39
vector39:
  pushl $0
c01020b6:	6a 00                	push   $0x0
  pushl $39
c01020b8:	6a 27                	push   $0x27
  jmp __alltraps
c01020ba:	e9 18 09 00 00       	jmp    c01029d7 <__alltraps>

c01020bf <vector40>:
.globl vector40
vector40:
  pushl $0
c01020bf:	6a 00                	push   $0x0
  pushl $40
c01020c1:	6a 28                	push   $0x28
  jmp __alltraps
c01020c3:	e9 0f 09 00 00       	jmp    c01029d7 <__alltraps>

c01020c8 <vector41>:
.globl vector41
vector41:
  pushl $0
c01020c8:	6a 00                	push   $0x0
  pushl $41
c01020ca:	6a 29                	push   $0x29
  jmp __alltraps
c01020cc:	e9 06 09 00 00       	jmp    c01029d7 <__alltraps>

c01020d1 <vector42>:
.globl vector42
vector42:
  pushl $0
c01020d1:	6a 00                	push   $0x0
  pushl $42
c01020d3:	6a 2a                	push   $0x2a
  jmp __alltraps
c01020d5:	e9 fd 08 00 00       	jmp    c01029d7 <__alltraps>

c01020da <vector43>:
.globl vector43
vector43:
  pushl $0
c01020da:	6a 00                	push   $0x0
  pushl $43
c01020dc:	6a 2b                	push   $0x2b
  jmp __alltraps
c01020de:	e9 f4 08 00 00       	jmp    c01029d7 <__alltraps>

c01020e3 <vector44>:
.globl vector44
vector44:
  pushl $0
c01020e3:	6a 00                	push   $0x0
  pushl $44
c01020e5:	6a 2c                	push   $0x2c
  jmp __alltraps
c01020e7:	e9 eb 08 00 00       	jmp    c01029d7 <__alltraps>

c01020ec <vector45>:
.globl vector45
vector45:
  pushl $0
c01020ec:	6a 00                	push   $0x0
  pushl $45
c01020ee:	6a 2d                	push   $0x2d
  jmp __alltraps
c01020f0:	e9 e2 08 00 00       	jmp    c01029d7 <__alltraps>

c01020f5 <vector46>:
.globl vector46
vector46:
  pushl $0
c01020f5:	6a 00                	push   $0x0
  pushl $46
c01020f7:	6a 2e                	push   $0x2e
  jmp __alltraps
c01020f9:	e9 d9 08 00 00       	jmp    c01029d7 <__alltraps>

c01020fe <vector47>:
.globl vector47
vector47:
  pushl $0
c01020fe:	6a 00                	push   $0x0
  pushl $47
c0102100:	6a 2f                	push   $0x2f
  jmp __alltraps
c0102102:	e9 d0 08 00 00       	jmp    c01029d7 <__alltraps>

c0102107 <vector48>:
.globl vector48
vector48:
  pushl $0
c0102107:	6a 00                	push   $0x0
  pushl $48
c0102109:	6a 30                	push   $0x30
  jmp __alltraps
c010210b:	e9 c7 08 00 00       	jmp    c01029d7 <__alltraps>

c0102110 <vector49>:
.globl vector49
vector49:
  pushl $0
c0102110:	6a 00                	push   $0x0
  pushl $49
c0102112:	6a 31                	push   $0x31
  jmp __alltraps
c0102114:	e9 be 08 00 00       	jmp    c01029d7 <__alltraps>

c0102119 <vector50>:
.globl vector50
vector50:
  pushl $0
c0102119:	6a 00                	push   $0x0
  pushl $50
c010211b:	6a 32                	push   $0x32
  jmp __alltraps
c010211d:	e9 b5 08 00 00       	jmp    c01029d7 <__alltraps>

c0102122 <vector51>:
.globl vector51
vector51:
  pushl $0
c0102122:	6a 00                	push   $0x0
  pushl $51
c0102124:	6a 33                	push   $0x33
  jmp __alltraps
c0102126:	e9 ac 08 00 00       	jmp    c01029d7 <__alltraps>

c010212b <vector52>:
.globl vector52
vector52:
  pushl $0
c010212b:	6a 00                	push   $0x0
  pushl $52
c010212d:	6a 34                	push   $0x34
  jmp __alltraps
c010212f:	e9 a3 08 00 00       	jmp    c01029d7 <__alltraps>

c0102134 <vector53>:
.globl vector53
vector53:
  pushl $0
c0102134:	6a 00                	push   $0x0
  pushl $53
c0102136:	6a 35                	push   $0x35
  jmp __alltraps
c0102138:	e9 9a 08 00 00       	jmp    c01029d7 <__alltraps>

c010213d <vector54>:
.globl vector54
vector54:
  pushl $0
c010213d:	6a 00                	push   $0x0
  pushl $54
c010213f:	6a 36                	push   $0x36
  jmp __alltraps
c0102141:	e9 91 08 00 00       	jmp    c01029d7 <__alltraps>

c0102146 <vector55>:
.globl vector55
vector55:
  pushl $0
c0102146:	6a 00                	push   $0x0
  pushl $55
c0102148:	6a 37                	push   $0x37
  jmp __alltraps
c010214a:	e9 88 08 00 00       	jmp    c01029d7 <__alltraps>

c010214f <vector56>:
.globl vector56
vector56:
  pushl $0
c010214f:	6a 00                	push   $0x0
  pushl $56
c0102151:	6a 38                	push   $0x38
  jmp __alltraps
c0102153:	e9 7f 08 00 00       	jmp    c01029d7 <__alltraps>

c0102158 <vector57>:
.globl vector57
vector57:
  pushl $0
c0102158:	6a 00                	push   $0x0
  pushl $57
c010215a:	6a 39                	push   $0x39
  jmp __alltraps
c010215c:	e9 76 08 00 00       	jmp    c01029d7 <__alltraps>

c0102161 <vector58>:
.globl vector58
vector58:
  pushl $0
c0102161:	6a 00                	push   $0x0
  pushl $58
c0102163:	6a 3a                	push   $0x3a
  jmp __alltraps
c0102165:	e9 6d 08 00 00       	jmp    c01029d7 <__alltraps>

c010216a <vector59>:
.globl vector59
vector59:
  pushl $0
c010216a:	6a 00                	push   $0x0
  pushl $59
c010216c:	6a 3b                	push   $0x3b
  jmp __alltraps
c010216e:	e9 64 08 00 00       	jmp    c01029d7 <__alltraps>

c0102173 <vector60>:
.globl vector60
vector60:
  pushl $0
c0102173:	6a 00                	push   $0x0
  pushl $60
c0102175:	6a 3c                	push   $0x3c
  jmp __alltraps
c0102177:	e9 5b 08 00 00       	jmp    c01029d7 <__alltraps>

c010217c <vector61>:
.globl vector61
vector61:
  pushl $0
c010217c:	6a 00                	push   $0x0
  pushl $61
c010217e:	6a 3d                	push   $0x3d
  jmp __alltraps
c0102180:	e9 52 08 00 00       	jmp    c01029d7 <__alltraps>

c0102185 <vector62>:
.globl vector62
vector62:
  pushl $0
c0102185:	6a 00                	push   $0x0
  pushl $62
c0102187:	6a 3e                	push   $0x3e
  jmp __alltraps
c0102189:	e9 49 08 00 00       	jmp    c01029d7 <__alltraps>

c010218e <vector63>:
.globl vector63
vector63:
  pushl $0
c010218e:	6a 00                	push   $0x0
  pushl $63
c0102190:	6a 3f                	push   $0x3f
  jmp __alltraps
c0102192:	e9 40 08 00 00       	jmp    c01029d7 <__alltraps>

c0102197 <vector64>:
.globl vector64
vector64:
  pushl $0
c0102197:	6a 00                	push   $0x0
  pushl $64
c0102199:	6a 40                	push   $0x40
  jmp __alltraps
c010219b:	e9 37 08 00 00       	jmp    c01029d7 <__alltraps>

c01021a0 <vector65>:
.globl vector65
vector65:
  pushl $0
c01021a0:	6a 00                	push   $0x0
  pushl $65
c01021a2:	6a 41                	push   $0x41
  jmp __alltraps
c01021a4:	e9 2e 08 00 00       	jmp    c01029d7 <__alltraps>

c01021a9 <vector66>:
.globl vector66
vector66:
  pushl $0
c01021a9:	6a 00                	push   $0x0
  pushl $66
c01021ab:	6a 42                	push   $0x42
  jmp __alltraps
c01021ad:	e9 25 08 00 00       	jmp    c01029d7 <__alltraps>

c01021b2 <vector67>:
.globl vector67
vector67:
  pushl $0
c01021b2:	6a 00                	push   $0x0
  pushl $67
c01021b4:	6a 43                	push   $0x43
  jmp __alltraps
c01021b6:	e9 1c 08 00 00       	jmp    c01029d7 <__alltraps>

c01021bb <vector68>:
.globl vector68
vector68:
  pushl $0
c01021bb:	6a 00                	push   $0x0
  pushl $68
c01021bd:	6a 44                	push   $0x44
  jmp __alltraps
c01021bf:	e9 13 08 00 00       	jmp    c01029d7 <__alltraps>

c01021c4 <vector69>:
.globl vector69
vector69:
  pushl $0
c01021c4:	6a 00                	push   $0x0
  pushl $69
c01021c6:	6a 45                	push   $0x45
  jmp __alltraps
c01021c8:	e9 0a 08 00 00       	jmp    c01029d7 <__alltraps>

c01021cd <vector70>:
.globl vector70
vector70:
  pushl $0
c01021cd:	6a 00                	push   $0x0
  pushl $70
c01021cf:	6a 46                	push   $0x46
  jmp __alltraps
c01021d1:	e9 01 08 00 00       	jmp    c01029d7 <__alltraps>

c01021d6 <vector71>:
.globl vector71
vector71:
  pushl $0
c01021d6:	6a 00                	push   $0x0
  pushl $71
c01021d8:	6a 47                	push   $0x47
  jmp __alltraps
c01021da:	e9 f8 07 00 00       	jmp    c01029d7 <__alltraps>

c01021df <vector72>:
.globl vector72
vector72:
  pushl $0
c01021df:	6a 00                	push   $0x0
  pushl $72
c01021e1:	6a 48                	push   $0x48
  jmp __alltraps
c01021e3:	e9 ef 07 00 00       	jmp    c01029d7 <__alltraps>

c01021e8 <vector73>:
.globl vector73
vector73:
  pushl $0
c01021e8:	6a 00                	push   $0x0
  pushl $73
c01021ea:	6a 49                	push   $0x49
  jmp __alltraps
c01021ec:	e9 e6 07 00 00       	jmp    c01029d7 <__alltraps>

c01021f1 <vector74>:
.globl vector74
vector74:
  pushl $0
c01021f1:	6a 00                	push   $0x0
  pushl $74
c01021f3:	6a 4a                	push   $0x4a
  jmp __alltraps
c01021f5:	e9 dd 07 00 00       	jmp    c01029d7 <__alltraps>

c01021fa <vector75>:
.globl vector75
vector75:
  pushl $0
c01021fa:	6a 00                	push   $0x0
  pushl $75
c01021fc:	6a 4b                	push   $0x4b
  jmp __alltraps
c01021fe:	e9 d4 07 00 00       	jmp    c01029d7 <__alltraps>

c0102203 <vector76>:
.globl vector76
vector76:
  pushl $0
c0102203:	6a 00                	push   $0x0
  pushl $76
c0102205:	6a 4c                	push   $0x4c
  jmp __alltraps
c0102207:	e9 cb 07 00 00       	jmp    c01029d7 <__alltraps>

c010220c <vector77>:
.globl vector77
vector77:
  pushl $0
c010220c:	6a 00                	push   $0x0
  pushl $77
c010220e:	6a 4d                	push   $0x4d
  jmp __alltraps
c0102210:	e9 c2 07 00 00       	jmp    c01029d7 <__alltraps>

c0102215 <vector78>:
.globl vector78
vector78:
  pushl $0
c0102215:	6a 00                	push   $0x0
  pushl $78
c0102217:	6a 4e                	push   $0x4e
  jmp __alltraps
c0102219:	e9 b9 07 00 00       	jmp    c01029d7 <__alltraps>

c010221e <vector79>:
.globl vector79
vector79:
  pushl $0
c010221e:	6a 00                	push   $0x0
  pushl $79
c0102220:	6a 4f                	push   $0x4f
  jmp __alltraps
c0102222:	e9 b0 07 00 00       	jmp    c01029d7 <__alltraps>

c0102227 <vector80>:
.globl vector80
vector80:
  pushl $0
c0102227:	6a 00                	push   $0x0
  pushl $80
c0102229:	6a 50                	push   $0x50
  jmp __alltraps
c010222b:	e9 a7 07 00 00       	jmp    c01029d7 <__alltraps>

c0102230 <vector81>:
.globl vector81
vector81:
  pushl $0
c0102230:	6a 00                	push   $0x0
  pushl $81
c0102232:	6a 51                	push   $0x51
  jmp __alltraps
c0102234:	e9 9e 07 00 00       	jmp    c01029d7 <__alltraps>

c0102239 <vector82>:
.globl vector82
vector82:
  pushl $0
c0102239:	6a 00                	push   $0x0
  pushl $82
c010223b:	6a 52                	push   $0x52
  jmp __alltraps
c010223d:	e9 95 07 00 00       	jmp    c01029d7 <__alltraps>

c0102242 <vector83>:
.globl vector83
vector83:
  pushl $0
c0102242:	6a 00                	push   $0x0
  pushl $83
c0102244:	6a 53                	push   $0x53
  jmp __alltraps
c0102246:	e9 8c 07 00 00       	jmp    c01029d7 <__alltraps>

c010224b <vector84>:
.globl vector84
vector84:
  pushl $0
c010224b:	6a 00                	push   $0x0
  pushl $84
c010224d:	6a 54                	push   $0x54
  jmp __alltraps
c010224f:	e9 83 07 00 00       	jmp    c01029d7 <__alltraps>

c0102254 <vector85>:
.globl vector85
vector85:
  pushl $0
c0102254:	6a 00                	push   $0x0
  pushl $85
c0102256:	6a 55                	push   $0x55
  jmp __alltraps
c0102258:	e9 7a 07 00 00       	jmp    c01029d7 <__alltraps>

c010225d <vector86>:
.globl vector86
vector86:
  pushl $0
c010225d:	6a 00                	push   $0x0
  pushl $86
c010225f:	6a 56                	push   $0x56
  jmp __alltraps
c0102261:	e9 71 07 00 00       	jmp    c01029d7 <__alltraps>

c0102266 <vector87>:
.globl vector87
vector87:
  pushl $0
c0102266:	6a 00                	push   $0x0
  pushl $87
c0102268:	6a 57                	push   $0x57
  jmp __alltraps
c010226a:	e9 68 07 00 00       	jmp    c01029d7 <__alltraps>

c010226f <vector88>:
.globl vector88
vector88:
  pushl $0
c010226f:	6a 00                	push   $0x0
  pushl $88
c0102271:	6a 58                	push   $0x58
  jmp __alltraps
c0102273:	e9 5f 07 00 00       	jmp    c01029d7 <__alltraps>

c0102278 <vector89>:
.globl vector89
vector89:
  pushl $0
c0102278:	6a 00                	push   $0x0
  pushl $89
c010227a:	6a 59                	push   $0x59
  jmp __alltraps
c010227c:	e9 56 07 00 00       	jmp    c01029d7 <__alltraps>

c0102281 <vector90>:
.globl vector90
vector90:
  pushl $0
c0102281:	6a 00                	push   $0x0
  pushl $90
c0102283:	6a 5a                	push   $0x5a
  jmp __alltraps
c0102285:	e9 4d 07 00 00       	jmp    c01029d7 <__alltraps>

c010228a <vector91>:
.globl vector91
vector91:
  pushl $0
c010228a:	6a 00                	push   $0x0
  pushl $91
c010228c:	6a 5b                	push   $0x5b
  jmp __alltraps
c010228e:	e9 44 07 00 00       	jmp    c01029d7 <__alltraps>

c0102293 <vector92>:
.globl vector92
vector92:
  pushl $0
c0102293:	6a 00                	push   $0x0
  pushl $92
c0102295:	6a 5c                	push   $0x5c
  jmp __alltraps
c0102297:	e9 3b 07 00 00       	jmp    c01029d7 <__alltraps>

c010229c <vector93>:
.globl vector93
vector93:
  pushl $0
c010229c:	6a 00                	push   $0x0
  pushl $93
c010229e:	6a 5d                	push   $0x5d
  jmp __alltraps
c01022a0:	e9 32 07 00 00       	jmp    c01029d7 <__alltraps>

c01022a5 <vector94>:
.globl vector94
vector94:
  pushl $0
c01022a5:	6a 00                	push   $0x0
  pushl $94
c01022a7:	6a 5e                	push   $0x5e
  jmp __alltraps
c01022a9:	e9 29 07 00 00       	jmp    c01029d7 <__alltraps>

c01022ae <vector95>:
.globl vector95
vector95:
  pushl $0
c01022ae:	6a 00                	push   $0x0
  pushl $95
c01022b0:	6a 5f                	push   $0x5f
  jmp __alltraps
c01022b2:	e9 20 07 00 00       	jmp    c01029d7 <__alltraps>

c01022b7 <vector96>:
.globl vector96
vector96:
  pushl $0
c01022b7:	6a 00                	push   $0x0
  pushl $96
c01022b9:	6a 60                	push   $0x60
  jmp __alltraps
c01022bb:	e9 17 07 00 00       	jmp    c01029d7 <__alltraps>

c01022c0 <vector97>:
.globl vector97
vector97:
  pushl $0
c01022c0:	6a 00                	push   $0x0
  pushl $97
c01022c2:	6a 61                	push   $0x61
  jmp __alltraps
c01022c4:	e9 0e 07 00 00       	jmp    c01029d7 <__alltraps>

c01022c9 <vector98>:
.globl vector98
vector98:
  pushl $0
c01022c9:	6a 00                	push   $0x0
  pushl $98
c01022cb:	6a 62                	push   $0x62
  jmp __alltraps
c01022cd:	e9 05 07 00 00       	jmp    c01029d7 <__alltraps>

c01022d2 <vector99>:
.globl vector99
vector99:
  pushl $0
c01022d2:	6a 00                	push   $0x0
  pushl $99
c01022d4:	6a 63                	push   $0x63
  jmp __alltraps
c01022d6:	e9 fc 06 00 00       	jmp    c01029d7 <__alltraps>

c01022db <vector100>:
.globl vector100
vector100:
  pushl $0
c01022db:	6a 00                	push   $0x0
  pushl $100
c01022dd:	6a 64                	push   $0x64
  jmp __alltraps
c01022df:	e9 f3 06 00 00       	jmp    c01029d7 <__alltraps>

c01022e4 <vector101>:
.globl vector101
vector101:
  pushl $0
c01022e4:	6a 00                	push   $0x0
  pushl $101
c01022e6:	6a 65                	push   $0x65
  jmp __alltraps
c01022e8:	e9 ea 06 00 00       	jmp    c01029d7 <__alltraps>

c01022ed <vector102>:
.globl vector102
vector102:
  pushl $0
c01022ed:	6a 00                	push   $0x0
  pushl $102
c01022ef:	6a 66                	push   $0x66
  jmp __alltraps
c01022f1:	e9 e1 06 00 00       	jmp    c01029d7 <__alltraps>

c01022f6 <vector103>:
.globl vector103
vector103:
  pushl $0
c01022f6:	6a 00                	push   $0x0
  pushl $103
c01022f8:	6a 67                	push   $0x67
  jmp __alltraps
c01022fa:	e9 d8 06 00 00       	jmp    c01029d7 <__alltraps>

c01022ff <vector104>:
.globl vector104
vector104:
  pushl $0
c01022ff:	6a 00                	push   $0x0
  pushl $104
c0102301:	6a 68                	push   $0x68
  jmp __alltraps
c0102303:	e9 cf 06 00 00       	jmp    c01029d7 <__alltraps>

c0102308 <vector105>:
.globl vector105
vector105:
  pushl $0
c0102308:	6a 00                	push   $0x0
  pushl $105
c010230a:	6a 69                	push   $0x69
  jmp __alltraps
c010230c:	e9 c6 06 00 00       	jmp    c01029d7 <__alltraps>

c0102311 <vector106>:
.globl vector106
vector106:
  pushl $0
c0102311:	6a 00                	push   $0x0
  pushl $106
c0102313:	6a 6a                	push   $0x6a
  jmp __alltraps
c0102315:	e9 bd 06 00 00       	jmp    c01029d7 <__alltraps>

c010231a <vector107>:
.globl vector107
vector107:
  pushl $0
c010231a:	6a 00                	push   $0x0
  pushl $107
c010231c:	6a 6b                	push   $0x6b
  jmp __alltraps
c010231e:	e9 b4 06 00 00       	jmp    c01029d7 <__alltraps>

c0102323 <vector108>:
.globl vector108
vector108:
  pushl $0
c0102323:	6a 00                	push   $0x0
  pushl $108
c0102325:	6a 6c                	push   $0x6c
  jmp __alltraps
c0102327:	e9 ab 06 00 00       	jmp    c01029d7 <__alltraps>

c010232c <vector109>:
.globl vector109
vector109:
  pushl $0
c010232c:	6a 00                	push   $0x0
  pushl $109
c010232e:	6a 6d                	push   $0x6d
  jmp __alltraps
c0102330:	e9 a2 06 00 00       	jmp    c01029d7 <__alltraps>

c0102335 <vector110>:
.globl vector110
vector110:
  pushl $0
c0102335:	6a 00                	push   $0x0
  pushl $110
c0102337:	6a 6e                	push   $0x6e
  jmp __alltraps
c0102339:	e9 99 06 00 00       	jmp    c01029d7 <__alltraps>

c010233e <vector111>:
.globl vector111
vector111:
  pushl $0
c010233e:	6a 00                	push   $0x0
  pushl $111
c0102340:	6a 6f                	push   $0x6f
  jmp __alltraps
c0102342:	e9 90 06 00 00       	jmp    c01029d7 <__alltraps>

c0102347 <vector112>:
.globl vector112
vector112:
  pushl $0
c0102347:	6a 00                	push   $0x0
  pushl $112
c0102349:	6a 70                	push   $0x70
  jmp __alltraps
c010234b:	e9 87 06 00 00       	jmp    c01029d7 <__alltraps>

c0102350 <vector113>:
.globl vector113
vector113:
  pushl $0
c0102350:	6a 00                	push   $0x0
  pushl $113
c0102352:	6a 71                	push   $0x71
  jmp __alltraps
c0102354:	e9 7e 06 00 00       	jmp    c01029d7 <__alltraps>

c0102359 <vector114>:
.globl vector114
vector114:
  pushl $0
c0102359:	6a 00                	push   $0x0
  pushl $114
c010235b:	6a 72                	push   $0x72
  jmp __alltraps
c010235d:	e9 75 06 00 00       	jmp    c01029d7 <__alltraps>

c0102362 <vector115>:
.globl vector115
vector115:
  pushl $0
c0102362:	6a 00                	push   $0x0
  pushl $115
c0102364:	6a 73                	push   $0x73
  jmp __alltraps
c0102366:	e9 6c 06 00 00       	jmp    c01029d7 <__alltraps>

c010236b <vector116>:
.globl vector116
vector116:
  pushl $0
c010236b:	6a 00                	push   $0x0
  pushl $116
c010236d:	6a 74                	push   $0x74
  jmp __alltraps
c010236f:	e9 63 06 00 00       	jmp    c01029d7 <__alltraps>

c0102374 <vector117>:
.globl vector117
vector117:
  pushl $0
c0102374:	6a 00                	push   $0x0
  pushl $117
c0102376:	6a 75                	push   $0x75
  jmp __alltraps
c0102378:	e9 5a 06 00 00       	jmp    c01029d7 <__alltraps>

c010237d <vector118>:
.globl vector118
vector118:
  pushl $0
c010237d:	6a 00                	push   $0x0
  pushl $118
c010237f:	6a 76                	push   $0x76
  jmp __alltraps
c0102381:	e9 51 06 00 00       	jmp    c01029d7 <__alltraps>

c0102386 <vector119>:
.globl vector119
vector119:
  pushl $0
c0102386:	6a 00                	push   $0x0
  pushl $119
c0102388:	6a 77                	push   $0x77
  jmp __alltraps
c010238a:	e9 48 06 00 00       	jmp    c01029d7 <__alltraps>

c010238f <vector120>:
.globl vector120
vector120:
  pushl $0
c010238f:	6a 00                	push   $0x0
  pushl $120
c0102391:	6a 78                	push   $0x78
  jmp __alltraps
c0102393:	e9 3f 06 00 00       	jmp    c01029d7 <__alltraps>

c0102398 <vector121>:
.globl vector121
vector121:
  pushl $0
c0102398:	6a 00                	push   $0x0
  pushl $121
c010239a:	6a 79                	push   $0x79
  jmp __alltraps
c010239c:	e9 36 06 00 00       	jmp    c01029d7 <__alltraps>

c01023a1 <vector122>:
.globl vector122
vector122:
  pushl $0
c01023a1:	6a 00                	push   $0x0
  pushl $122
c01023a3:	6a 7a                	push   $0x7a
  jmp __alltraps
c01023a5:	e9 2d 06 00 00       	jmp    c01029d7 <__alltraps>

c01023aa <vector123>:
.globl vector123
vector123:
  pushl $0
c01023aa:	6a 00                	push   $0x0
  pushl $123
c01023ac:	6a 7b                	push   $0x7b
  jmp __alltraps
c01023ae:	e9 24 06 00 00       	jmp    c01029d7 <__alltraps>

c01023b3 <vector124>:
.globl vector124
vector124:
  pushl $0
c01023b3:	6a 00                	push   $0x0
  pushl $124
c01023b5:	6a 7c                	push   $0x7c
  jmp __alltraps
c01023b7:	e9 1b 06 00 00       	jmp    c01029d7 <__alltraps>

c01023bc <vector125>:
.globl vector125
vector125:
  pushl $0
c01023bc:	6a 00                	push   $0x0
  pushl $125
c01023be:	6a 7d                	push   $0x7d
  jmp __alltraps
c01023c0:	e9 12 06 00 00       	jmp    c01029d7 <__alltraps>

c01023c5 <vector126>:
.globl vector126
vector126:
  pushl $0
c01023c5:	6a 00                	push   $0x0
  pushl $126
c01023c7:	6a 7e                	push   $0x7e
  jmp __alltraps
c01023c9:	e9 09 06 00 00       	jmp    c01029d7 <__alltraps>

c01023ce <vector127>:
.globl vector127
vector127:
  pushl $0
c01023ce:	6a 00                	push   $0x0
  pushl $127
c01023d0:	6a 7f                	push   $0x7f
  jmp __alltraps
c01023d2:	e9 00 06 00 00       	jmp    c01029d7 <__alltraps>

c01023d7 <vector128>:
.globl vector128
vector128:
  pushl $0
c01023d7:	6a 00                	push   $0x0
  pushl $128
c01023d9:	68 80 00 00 00       	push   $0x80
  jmp __alltraps
c01023de:	e9 f4 05 00 00       	jmp    c01029d7 <__alltraps>

c01023e3 <vector129>:
.globl vector129
vector129:
  pushl $0
c01023e3:	6a 00                	push   $0x0
  pushl $129
c01023e5:	68 81 00 00 00       	push   $0x81
  jmp __alltraps
c01023ea:	e9 e8 05 00 00       	jmp    c01029d7 <__alltraps>

c01023ef <vector130>:
.globl vector130
vector130:
  pushl $0
c01023ef:	6a 00                	push   $0x0
  pushl $130
c01023f1:	68 82 00 00 00       	push   $0x82
  jmp __alltraps
c01023f6:	e9 dc 05 00 00       	jmp    c01029d7 <__alltraps>

c01023fb <vector131>:
.globl vector131
vector131:
  pushl $0
c01023fb:	6a 00                	push   $0x0
  pushl $131
c01023fd:	68 83 00 00 00       	push   $0x83
  jmp __alltraps
c0102402:	e9 d0 05 00 00       	jmp    c01029d7 <__alltraps>

c0102407 <vector132>:
.globl vector132
vector132:
  pushl $0
c0102407:	6a 00                	push   $0x0
  pushl $132
c0102409:	68 84 00 00 00       	push   $0x84
  jmp __alltraps
c010240e:	e9 c4 05 00 00       	jmp    c01029d7 <__alltraps>

c0102413 <vector133>:
.globl vector133
vector133:
  pushl $0
c0102413:	6a 00                	push   $0x0
  pushl $133
c0102415:	68 85 00 00 00       	push   $0x85
  jmp __alltraps
c010241a:	e9 b8 05 00 00       	jmp    c01029d7 <__alltraps>

c010241f <vector134>:
.globl vector134
vector134:
  pushl $0
c010241f:	6a 00                	push   $0x0
  pushl $134
c0102421:	68 86 00 00 00       	push   $0x86
  jmp __alltraps
c0102426:	e9 ac 05 00 00       	jmp    c01029d7 <__alltraps>

c010242b <vector135>:
.globl vector135
vector135:
  pushl $0
c010242b:	6a 00                	push   $0x0
  pushl $135
c010242d:	68 87 00 00 00       	push   $0x87
  jmp __alltraps
c0102432:	e9 a0 05 00 00       	jmp    c01029d7 <__alltraps>

c0102437 <vector136>:
.globl vector136
vector136:
  pushl $0
c0102437:	6a 00                	push   $0x0
  pushl $136
c0102439:	68 88 00 00 00       	push   $0x88
  jmp __alltraps
c010243e:	e9 94 05 00 00       	jmp    c01029d7 <__alltraps>

c0102443 <vector137>:
.globl vector137
vector137:
  pushl $0
c0102443:	6a 00                	push   $0x0
  pushl $137
c0102445:	68 89 00 00 00       	push   $0x89
  jmp __alltraps
c010244a:	e9 88 05 00 00       	jmp    c01029d7 <__alltraps>

c010244f <vector138>:
.globl vector138
vector138:
  pushl $0
c010244f:	6a 00                	push   $0x0
  pushl $138
c0102451:	68 8a 00 00 00       	push   $0x8a
  jmp __alltraps
c0102456:	e9 7c 05 00 00       	jmp    c01029d7 <__alltraps>

c010245b <vector139>:
.globl vector139
vector139:
  pushl $0
c010245b:	6a 00                	push   $0x0
  pushl $139
c010245d:	68 8b 00 00 00       	push   $0x8b
  jmp __alltraps
c0102462:	e9 70 05 00 00       	jmp    c01029d7 <__alltraps>

c0102467 <vector140>:
.globl vector140
vector140:
  pushl $0
c0102467:	6a 00                	push   $0x0
  pushl $140
c0102469:	68 8c 00 00 00       	push   $0x8c
  jmp __alltraps
c010246e:	e9 64 05 00 00       	jmp    c01029d7 <__alltraps>

c0102473 <vector141>:
.globl vector141
vector141:
  pushl $0
c0102473:	6a 00                	push   $0x0
  pushl $141
c0102475:	68 8d 00 00 00       	push   $0x8d
  jmp __alltraps
c010247a:	e9 58 05 00 00       	jmp    c01029d7 <__alltraps>

c010247f <vector142>:
.globl vector142
vector142:
  pushl $0
c010247f:	6a 00                	push   $0x0
  pushl $142
c0102481:	68 8e 00 00 00       	push   $0x8e
  jmp __alltraps
c0102486:	e9 4c 05 00 00       	jmp    c01029d7 <__alltraps>

c010248b <vector143>:
.globl vector143
vector143:
  pushl $0
c010248b:	6a 00                	push   $0x0
  pushl $143
c010248d:	68 8f 00 00 00       	push   $0x8f
  jmp __alltraps
c0102492:	e9 40 05 00 00       	jmp    c01029d7 <__alltraps>

c0102497 <vector144>:
.globl vector144
vector144:
  pushl $0
c0102497:	6a 00                	push   $0x0
  pushl $144
c0102499:	68 90 00 00 00       	push   $0x90
  jmp __alltraps
c010249e:	e9 34 05 00 00       	jmp    c01029d7 <__alltraps>

c01024a3 <vector145>:
.globl vector145
vector145:
  pushl $0
c01024a3:	6a 00                	push   $0x0
  pushl $145
c01024a5:	68 91 00 00 00       	push   $0x91
  jmp __alltraps
c01024aa:	e9 28 05 00 00       	jmp    c01029d7 <__alltraps>

c01024af <vector146>:
.globl vector146
vector146:
  pushl $0
c01024af:	6a 00                	push   $0x0
  pushl $146
c01024b1:	68 92 00 00 00       	push   $0x92
  jmp __alltraps
c01024b6:	e9 1c 05 00 00       	jmp    c01029d7 <__alltraps>

c01024bb <vector147>:
.globl vector147
vector147:
  pushl $0
c01024bb:	6a 00                	push   $0x0
  pushl $147
c01024bd:	68 93 00 00 00       	push   $0x93
  jmp __alltraps
c01024c2:	e9 10 05 00 00       	jmp    c01029d7 <__alltraps>

c01024c7 <vector148>:
.globl vector148
vector148:
  pushl $0
c01024c7:	6a 00                	push   $0x0
  pushl $148
c01024c9:	68 94 00 00 00       	push   $0x94
  jmp __alltraps
c01024ce:	e9 04 05 00 00       	jmp    c01029d7 <__alltraps>

c01024d3 <vector149>:
.globl vector149
vector149:
  pushl $0
c01024d3:	6a 00                	push   $0x0
  pushl $149
c01024d5:	68 95 00 00 00       	push   $0x95
  jmp __alltraps
c01024da:	e9 f8 04 00 00       	jmp    c01029d7 <__alltraps>

c01024df <vector150>:
.globl vector150
vector150:
  pushl $0
c01024df:	6a 00                	push   $0x0
  pushl $150
c01024e1:	68 96 00 00 00       	push   $0x96
  jmp __alltraps
c01024e6:	e9 ec 04 00 00       	jmp    c01029d7 <__alltraps>

c01024eb <vector151>:
.globl vector151
vector151:
  pushl $0
c01024eb:	6a 00                	push   $0x0
  pushl $151
c01024ed:	68 97 00 00 00       	push   $0x97
  jmp __alltraps
c01024f2:	e9 e0 04 00 00       	jmp    c01029d7 <__alltraps>

c01024f7 <vector152>:
.globl vector152
vector152:
  pushl $0
c01024f7:	6a 00                	push   $0x0
  pushl $152
c01024f9:	68 98 00 00 00       	push   $0x98
  jmp __alltraps
c01024fe:	e9 d4 04 00 00       	jmp    c01029d7 <__alltraps>

c0102503 <vector153>:
.globl vector153
vector153:
  pushl $0
c0102503:	6a 00                	push   $0x0
  pushl $153
c0102505:	68 99 00 00 00       	push   $0x99
  jmp __alltraps
c010250a:	e9 c8 04 00 00       	jmp    c01029d7 <__alltraps>

c010250f <vector154>:
.globl vector154
vector154:
  pushl $0
c010250f:	6a 00                	push   $0x0
  pushl $154
c0102511:	68 9a 00 00 00       	push   $0x9a
  jmp __alltraps
c0102516:	e9 bc 04 00 00       	jmp    c01029d7 <__alltraps>

c010251b <vector155>:
.globl vector155
vector155:
  pushl $0
c010251b:	6a 00                	push   $0x0
  pushl $155
c010251d:	68 9b 00 00 00       	push   $0x9b
  jmp __alltraps
c0102522:	e9 b0 04 00 00       	jmp    c01029d7 <__alltraps>

c0102527 <vector156>:
.globl vector156
vector156:
  pushl $0
c0102527:	6a 00                	push   $0x0
  pushl $156
c0102529:	68 9c 00 00 00       	push   $0x9c
  jmp __alltraps
c010252e:	e9 a4 04 00 00       	jmp    c01029d7 <__alltraps>

c0102533 <vector157>:
.globl vector157
vector157:
  pushl $0
c0102533:	6a 00                	push   $0x0
  pushl $157
c0102535:	68 9d 00 00 00       	push   $0x9d
  jmp __alltraps
c010253a:	e9 98 04 00 00       	jmp    c01029d7 <__alltraps>

c010253f <vector158>:
.globl vector158
vector158:
  pushl $0
c010253f:	6a 00                	push   $0x0
  pushl $158
c0102541:	68 9e 00 00 00       	push   $0x9e
  jmp __alltraps
c0102546:	e9 8c 04 00 00       	jmp    c01029d7 <__alltraps>

c010254b <vector159>:
.globl vector159
vector159:
  pushl $0
c010254b:	6a 00                	push   $0x0
  pushl $159
c010254d:	68 9f 00 00 00       	push   $0x9f
  jmp __alltraps
c0102552:	e9 80 04 00 00       	jmp    c01029d7 <__alltraps>

c0102557 <vector160>:
.globl vector160
vector160:
  pushl $0
c0102557:	6a 00                	push   $0x0
  pushl $160
c0102559:	68 a0 00 00 00       	push   $0xa0
  jmp __alltraps
c010255e:	e9 74 04 00 00       	jmp    c01029d7 <__alltraps>

c0102563 <vector161>:
.globl vector161
vector161:
  pushl $0
c0102563:	6a 00                	push   $0x0
  pushl $161
c0102565:	68 a1 00 00 00       	push   $0xa1
  jmp __alltraps
c010256a:	e9 68 04 00 00       	jmp    c01029d7 <__alltraps>

c010256f <vector162>:
.globl vector162
vector162:
  pushl $0
c010256f:	6a 00                	push   $0x0
  pushl $162
c0102571:	68 a2 00 00 00       	push   $0xa2
  jmp __alltraps
c0102576:	e9 5c 04 00 00       	jmp    c01029d7 <__alltraps>

c010257b <vector163>:
.globl vector163
vector163:
  pushl $0
c010257b:	6a 00                	push   $0x0
  pushl $163
c010257d:	68 a3 00 00 00       	push   $0xa3
  jmp __alltraps
c0102582:	e9 50 04 00 00       	jmp    c01029d7 <__alltraps>

c0102587 <vector164>:
.globl vector164
vector164:
  pushl $0
c0102587:	6a 00                	push   $0x0
  pushl $164
c0102589:	68 a4 00 00 00       	push   $0xa4
  jmp __alltraps
c010258e:	e9 44 04 00 00       	jmp    c01029d7 <__alltraps>

c0102593 <vector165>:
.globl vector165
vector165:
  pushl $0
c0102593:	6a 00                	push   $0x0
  pushl $165
c0102595:	68 a5 00 00 00       	push   $0xa5
  jmp __alltraps
c010259a:	e9 38 04 00 00       	jmp    c01029d7 <__alltraps>

c010259f <vector166>:
.globl vector166
vector166:
  pushl $0
c010259f:	6a 00                	push   $0x0
  pushl $166
c01025a1:	68 a6 00 00 00       	push   $0xa6
  jmp __alltraps
c01025a6:	e9 2c 04 00 00       	jmp    c01029d7 <__alltraps>

c01025ab <vector167>:
.globl vector167
vector167:
  pushl $0
c01025ab:	6a 00                	push   $0x0
  pushl $167
c01025ad:	68 a7 00 00 00       	push   $0xa7
  jmp __alltraps
c01025b2:	e9 20 04 00 00       	jmp    c01029d7 <__alltraps>

c01025b7 <vector168>:
.globl vector168
vector168:
  pushl $0
c01025b7:	6a 00                	push   $0x0
  pushl $168
c01025b9:	68 a8 00 00 00       	push   $0xa8
  jmp __alltraps
c01025be:	e9 14 04 00 00       	jmp    c01029d7 <__alltraps>

c01025c3 <vector169>:
.globl vector169
vector169:
  pushl $0
c01025c3:	6a 00                	push   $0x0
  pushl $169
c01025c5:	68 a9 00 00 00       	push   $0xa9
  jmp __alltraps
c01025ca:	e9 08 04 00 00       	jmp    c01029d7 <__alltraps>

c01025cf <vector170>:
.globl vector170
vector170:
  pushl $0
c01025cf:	6a 00                	push   $0x0
  pushl $170
c01025d1:	68 aa 00 00 00       	push   $0xaa
  jmp __alltraps
c01025d6:	e9 fc 03 00 00       	jmp    c01029d7 <__alltraps>

c01025db <vector171>:
.globl vector171
vector171:
  pushl $0
c01025db:	6a 00                	push   $0x0
  pushl $171
c01025dd:	68 ab 00 00 00       	push   $0xab
  jmp __alltraps
c01025e2:	e9 f0 03 00 00       	jmp    c01029d7 <__alltraps>

c01025e7 <vector172>:
.globl vector172
vector172:
  pushl $0
c01025e7:	6a 00                	push   $0x0
  pushl $172
c01025e9:	68 ac 00 00 00       	push   $0xac
  jmp __alltraps
c01025ee:	e9 e4 03 00 00       	jmp    c01029d7 <__alltraps>

c01025f3 <vector173>:
.globl vector173
vector173:
  pushl $0
c01025f3:	6a 00                	push   $0x0
  pushl $173
c01025f5:	68 ad 00 00 00       	push   $0xad
  jmp __alltraps
c01025fa:	e9 d8 03 00 00       	jmp    c01029d7 <__alltraps>

c01025ff <vector174>:
.globl vector174
vector174:
  pushl $0
c01025ff:	6a 00                	push   $0x0
  pushl $174
c0102601:	68 ae 00 00 00       	push   $0xae
  jmp __alltraps
c0102606:	e9 cc 03 00 00       	jmp    c01029d7 <__alltraps>

c010260b <vector175>:
.globl vector175
vector175:
  pushl $0
c010260b:	6a 00                	push   $0x0
  pushl $175
c010260d:	68 af 00 00 00       	push   $0xaf
  jmp __alltraps
c0102612:	e9 c0 03 00 00       	jmp    c01029d7 <__alltraps>

c0102617 <vector176>:
.globl vector176
vector176:
  pushl $0
c0102617:	6a 00                	push   $0x0
  pushl $176
c0102619:	68 b0 00 00 00       	push   $0xb0
  jmp __alltraps
c010261e:	e9 b4 03 00 00       	jmp    c01029d7 <__alltraps>

c0102623 <vector177>:
.globl vector177
vector177:
  pushl $0
c0102623:	6a 00                	push   $0x0
  pushl $177
c0102625:	68 b1 00 00 00       	push   $0xb1
  jmp __alltraps
c010262a:	e9 a8 03 00 00       	jmp    c01029d7 <__alltraps>

c010262f <vector178>:
.globl vector178
vector178:
  pushl $0
c010262f:	6a 00                	push   $0x0
  pushl $178
c0102631:	68 b2 00 00 00       	push   $0xb2
  jmp __alltraps
c0102636:	e9 9c 03 00 00       	jmp    c01029d7 <__alltraps>

c010263b <vector179>:
.globl vector179
vector179:
  pushl $0
c010263b:	6a 00                	push   $0x0
  pushl $179
c010263d:	68 b3 00 00 00       	push   $0xb3
  jmp __alltraps
c0102642:	e9 90 03 00 00       	jmp    c01029d7 <__alltraps>

c0102647 <vector180>:
.globl vector180
vector180:
  pushl $0
c0102647:	6a 00                	push   $0x0
  pushl $180
c0102649:	68 b4 00 00 00       	push   $0xb4
  jmp __alltraps
c010264e:	e9 84 03 00 00       	jmp    c01029d7 <__alltraps>

c0102653 <vector181>:
.globl vector181
vector181:
  pushl $0
c0102653:	6a 00                	push   $0x0
  pushl $181
c0102655:	68 b5 00 00 00       	push   $0xb5
  jmp __alltraps
c010265a:	e9 78 03 00 00       	jmp    c01029d7 <__alltraps>

c010265f <vector182>:
.globl vector182
vector182:
  pushl $0
c010265f:	6a 00                	push   $0x0
  pushl $182
c0102661:	68 b6 00 00 00       	push   $0xb6
  jmp __alltraps
c0102666:	e9 6c 03 00 00       	jmp    c01029d7 <__alltraps>

c010266b <vector183>:
.globl vector183
vector183:
  pushl $0
c010266b:	6a 00                	push   $0x0
  pushl $183
c010266d:	68 b7 00 00 00       	push   $0xb7
  jmp __alltraps
c0102672:	e9 60 03 00 00       	jmp    c01029d7 <__alltraps>

c0102677 <vector184>:
.globl vector184
vector184:
  pushl $0
c0102677:	6a 00                	push   $0x0
  pushl $184
c0102679:	68 b8 00 00 00       	push   $0xb8
  jmp __alltraps
c010267e:	e9 54 03 00 00       	jmp    c01029d7 <__alltraps>

c0102683 <vector185>:
.globl vector185
vector185:
  pushl $0
c0102683:	6a 00                	push   $0x0
  pushl $185
c0102685:	68 b9 00 00 00       	push   $0xb9
  jmp __alltraps
c010268a:	e9 48 03 00 00       	jmp    c01029d7 <__alltraps>

c010268f <vector186>:
.globl vector186
vector186:
  pushl $0
c010268f:	6a 00                	push   $0x0
  pushl $186
c0102691:	68 ba 00 00 00       	push   $0xba
  jmp __alltraps
c0102696:	e9 3c 03 00 00       	jmp    c01029d7 <__alltraps>

c010269b <vector187>:
.globl vector187
vector187:
  pushl $0
c010269b:	6a 00                	push   $0x0
  pushl $187
c010269d:	68 bb 00 00 00       	push   $0xbb
  jmp __alltraps
c01026a2:	e9 30 03 00 00       	jmp    c01029d7 <__alltraps>

c01026a7 <vector188>:
.globl vector188
vector188:
  pushl $0
c01026a7:	6a 00                	push   $0x0
  pushl $188
c01026a9:	68 bc 00 00 00       	push   $0xbc
  jmp __alltraps
c01026ae:	e9 24 03 00 00       	jmp    c01029d7 <__alltraps>

c01026b3 <vector189>:
.globl vector189
vector189:
  pushl $0
c01026b3:	6a 00                	push   $0x0
  pushl $189
c01026b5:	68 bd 00 00 00       	push   $0xbd
  jmp __alltraps
c01026ba:	e9 18 03 00 00       	jmp    c01029d7 <__alltraps>

c01026bf <vector190>:
.globl vector190
vector190:
  pushl $0
c01026bf:	6a 00                	push   $0x0
  pushl $190
c01026c1:	68 be 00 00 00       	push   $0xbe
  jmp __alltraps
c01026c6:	e9 0c 03 00 00       	jmp    c01029d7 <__alltraps>

c01026cb <vector191>:
.globl vector191
vector191:
  pushl $0
c01026cb:	6a 00                	push   $0x0
  pushl $191
c01026cd:	68 bf 00 00 00       	push   $0xbf
  jmp __alltraps
c01026d2:	e9 00 03 00 00       	jmp    c01029d7 <__alltraps>

c01026d7 <vector192>:
.globl vector192
vector192:
  pushl $0
c01026d7:	6a 00                	push   $0x0
  pushl $192
c01026d9:	68 c0 00 00 00       	push   $0xc0
  jmp __alltraps
c01026de:	e9 f4 02 00 00       	jmp    c01029d7 <__alltraps>

c01026e3 <vector193>:
.globl vector193
vector193:
  pushl $0
c01026e3:	6a 00                	push   $0x0
  pushl $193
c01026e5:	68 c1 00 00 00       	push   $0xc1
  jmp __alltraps
c01026ea:	e9 e8 02 00 00       	jmp    c01029d7 <__alltraps>

c01026ef <vector194>:
.globl vector194
vector194:
  pushl $0
c01026ef:	6a 00                	push   $0x0
  pushl $194
c01026f1:	68 c2 00 00 00       	push   $0xc2
  jmp __alltraps
c01026f6:	e9 dc 02 00 00       	jmp    c01029d7 <__alltraps>

c01026fb <vector195>:
.globl vector195
vector195:
  pushl $0
c01026fb:	6a 00                	push   $0x0
  pushl $195
c01026fd:	68 c3 00 00 00       	push   $0xc3
  jmp __alltraps
c0102702:	e9 d0 02 00 00       	jmp    c01029d7 <__alltraps>

c0102707 <vector196>:
.globl vector196
vector196:
  pushl $0
c0102707:	6a 00                	push   $0x0
  pushl $196
c0102709:	68 c4 00 00 00       	push   $0xc4
  jmp __alltraps
c010270e:	e9 c4 02 00 00       	jmp    c01029d7 <__alltraps>

c0102713 <vector197>:
.globl vector197
vector197:
  pushl $0
c0102713:	6a 00                	push   $0x0
  pushl $197
c0102715:	68 c5 00 00 00       	push   $0xc5
  jmp __alltraps
c010271a:	e9 b8 02 00 00       	jmp    c01029d7 <__alltraps>

c010271f <vector198>:
.globl vector198
vector198:
  pushl $0
c010271f:	6a 00                	push   $0x0
  pushl $198
c0102721:	68 c6 00 00 00       	push   $0xc6
  jmp __alltraps
c0102726:	e9 ac 02 00 00       	jmp    c01029d7 <__alltraps>

c010272b <vector199>:
.globl vector199
vector199:
  pushl $0
c010272b:	6a 00                	push   $0x0
  pushl $199
c010272d:	68 c7 00 00 00       	push   $0xc7
  jmp __alltraps
c0102732:	e9 a0 02 00 00       	jmp    c01029d7 <__alltraps>

c0102737 <vector200>:
.globl vector200
vector200:
  pushl $0
c0102737:	6a 00                	push   $0x0
  pushl $200
c0102739:	68 c8 00 00 00       	push   $0xc8
  jmp __alltraps
c010273e:	e9 94 02 00 00       	jmp    c01029d7 <__alltraps>

c0102743 <vector201>:
.globl vector201
vector201:
  pushl $0
c0102743:	6a 00                	push   $0x0
  pushl $201
c0102745:	68 c9 00 00 00       	push   $0xc9
  jmp __alltraps
c010274a:	e9 88 02 00 00       	jmp    c01029d7 <__alltraps>

c010274f <vector202>:
.globl vector202
vector202:
  pushl $0
c010274f:	6a 00                	push   $0x0
  pushl $202
c0102751:	68 ca 00 00 00       	push   $0xca
  jmp __alltraps
c0102756:	e9 7c 02 00 00       	jmp    c01029d7 <__alltraps>

c010275b <vector203>:
.globl vector203
vector203:
  pushl $0
c010275b:	6a 00                	push   $0x0
  pushl $203
c010275d:	68 cb 00 00 00       	push   $0xcb
  jmp __alltraps
c0102762:	e9 70 02 00 00       	jmp    c01029d7 <__alltraps>

c0102767 <vector204>:
.globl vector204
vector204:
  pushl $0
c0102767:	6a 00                	push   $0x0
  pushl $204
c0102769:	68 cc 00 00 00       	push   $0xcc
  jmp __alltraps
c010276e:	e9 64 02 00 00       	jmp    c01029d7 <__alltraps>

c0102773 <vector205>:
.globl vector205
vector205:
  pushl $0
c0102773:	6a 00                	push   $0x0
  pushl $205
c0102775:	68 cd 00 00 00       	push   $0xcd
  jmp __alltraps
c010277a:	e9 58 02 00 00       	jmp    c01029d7 <__alltraps>

c010277f <vector206>:
.globl vector206
vector206:
  pushl $0
c010277f:	6a 00                	push   $0x0
  pushl $206
c0102781:	68 ce 00 00 00       	push   $0xce
  jmp __alltraps
c0102786:	e9 4c 02 00 00       	jmp    c01029d7 <__alltraps>

c010278b <vector207>:
.globl vector207
vector207:
  pushl $0
c010278b:	6a 00                	push   $0x0
  pushl $207
c010278d:	68 cf 00 00 00       	push   $0xcf
  jmp __alltraps
c0102792:	e9 40 02 00 00       	jmp    c01029d7 <__alltraps>

c0102797 <vector208>:
.globl vector208
vector208:
  pushl $0
c0102797:	6a 00                	push   $0x0
  pushl $208
c0102799:	68 d0 00 00 00       	push   $0xd0
  jmp __alltraps
c010279e:	e9 34 02 00 00       	jmp    c01029d7 <__alltraps>

c01027a3 <vector209>:
.globl vector209
vector209:
  pushl $0
c01027a3:	6a 00                	push   $0x0
  pushl $209
c01027a5:	68 d1 00 00 00       	push   $0xd1
  jmp __alltraps
c01027aa:	e9 28 02 00 00       	jmp    c01029d7 <__alltraps>

c01027af <vector210>:
.globl vector210
vector210:
  pushl $0
c01027af:	6a 00                	push   $0x0
  pushl $210
c01027b1:	68 d2 00 00 00       	push   $0xd2
  jmp __alltraps
c01027b6:	e9 1c 02 00 00       	jmp    c01029d7 <__alltraps>

c01027bb <vector211>:
.globl vector211
vector211:
  pushl $0
c01027bb:	6a 00                	push   $0x0
  pushl $211
c01027bd:	68 d3 00 00 00       	push   $0xd3
  jmp __alltraps
c01027c2:	e9 10 02 00 00       	jmp    c01029d7 <__alltraps>

c01027c7 <vector212>:
.globl vector212
vector212:
  pushl $0
c01027c7:	6a 00                	push   $0x0
  pushl $212
c01027c9:	68 d4 00 00 00       	push   $0xd4
  jmp __alltraps
c01027ce:	e9 04 02 00 00       	jmp    c01029d7 <__alltraps>

c01027d3 <vector213>:
.globl vector213
vector213:
  pushl $0
c01027d3:	6a 00                	push   $0x0
  pushl $213
c01027d5:	68 d5 00 00 00       	push   $0xd5
  jmp __alltraps
c01027da:	e9 f8 01 00 00       	jmp    c01029d7 <__alltraps>

c01027df <vector214>:
.globl vector214
vector214:
  pushl $0
c01027df:	6a 00                	push   $0x0
  pushl $214
c01027e1:	68 d6 00 00 00       	push   $0xd6
  jmp __alltraps
c01027e6:	e9 ec 01 00 00       	jmp    c01029d7 <__alltraps>

c01027eb <vector215>:
.globl vector215
vector215:
  pushl $0
c01027eb:	6a 00                	push   $0x0
  pushl $215
c01027ed:	68 d7 00 00 00       	push   $0xd7
  jmp __alltraps
c01027f2:	e9 e0 01 00 00       	jmp    c01029d7 <__alltraps>

c01027f7 <vector216>:
.globl vector216
vector216:
  pushl $0
c01027f7:	6a 00                	push   $0x0
  pushl $216
c01027f9:	68 d8 00 00 00       	push   $0xd8
  jmp __alltraps
c01027fe:	e9 d4 01 00 00       	jmp    c01029d7 <__alltraps>

c0102803 <vector217>:
.globl vector217
vector217:
  pushl $0
c0102803:	6a 00                	push   $0x0
  pushl $217
c0102805:	68 d9 00 00 00       	push   $0xd9
  jmp __alltraps
c010280a:	e9 c8 01 00 00       	jmp    c01029d7 <__alltraps>

c010280f <vector218>:
.globl vector218
vector218:
  pushl $0
c010280f:	6a 00                	push   $0x0
  pushl $218
c0102811:	68 da 00 00 00       	push   $0xda
  jmp __alltraps
c0102816:	e9 bc 01 00 00       	jmp    c01029d7 <__alltraps>

c010281b <vector219>:
.globl vector219
vector219:
  pushl $0
c010281b:	6a 00                	push   $0x0
  pushl $219
c010281d:	68 db 00 00 00       	push   $0xdb
  jmp __alltraps
c0102822:	e9 b0 01 00 00       	jmp    c01029d7 <__alltraps>

c0102827 <vector220>:
.globl vector220
vector220:
  pushl $0
c0102827:	6a 00                	push   $0x0
  pushl $220
c0102829:	68 dc 00 00 00       	push   $0xdc
  jmp __alltraps
c010282e:	e9 a4 01 00 00       	jmp    c01029d7 <__alltraps>

c0102833 <vector221>:
.globl vector221
vector221:
  pushl $0
c0102833:	6a 00                	push   $0x0
  pushl $221
c0102835:	68 dd 00 00 00       	push   $0xdd
  jmp __alltraps
c010283a:	e9 98 01 00 00       	jmp    c01029d7 <__alltraps>

c010283f <vector222>:
.globl vector222
vector222:
  pushl $0
c010283f:	6a 00                	push   $0x0
  pushl $222
c0102841:	68 de 00 00 00       	push   $0xde
  jmp __alltraps
c0102846:	e9 8c 01 00 00       	jmp    c01029d7 <__alltraps>

c010284b <vector223>:
.globl vector223
vector223:
  pushl $0
c010284b:	6a 00                	push   $0x0
  pushl $223
c010284d:	68 df 00 00 00       	push   $0xdf
  jmp __alltraps
c0102852:	e9 80 01 00 00       	jmp    c01029d7 <__alltraps>

c0102857 <vector224>:
.globl vector224
vector224:
  pushl $0
c0102857:	6a 00                	push   $0x0
  pushl $224
c0102859:	68 e0 00 00 00       	push   $0xe0
  jmp __alltraps
c010285e:	e9 74 01 00 00       	jmp    c01029d7 <__alltraps>

c0102863 <vector225>:
.globl vector225
vector225:
  pushl $0
c0102863:	6a 00                	push   $0x0
  pushl $225
c0102865:	68 e1 00 00 00       	push   $0xe1
  jmp __alltraps
c010286a:	e9 68 01 00 00       	jmp    c01029d7 <__alltraps>

c010286f <vector226>:
.globl vector226
vector226:
  pushl $0
c010286f:	6a 00                	push   $0x0
  pushl $226
c0102871:	68 e2 00 00 00       	push   $0xe2
  jmp __alltraps
c0102876:	e9 5c 01 00 00       	jmp    c01029d7 <__alltraps>

c010287b <vector227>:
.globl vector227
vector227:
  pushl $0
c010287b:	6a 00                	push   $0x0
  pushl $227
c010287d:	68 e3 00 00 00       	push   $0xe3
  jmp __alltraps
c0102882:	e9 50 01 00 00       	jmp    c01029d7 <__alltraps>

c0102887 <vector228>:
.globl vector228
vector228:
  pushl $0
c0102887:	6a 00                	push   $0x0
  pushl $228
c0102889:	68 e4 00 00 00       	push   $0xe4
  jmp __alltraps
c010288e:	e9 44 01 00 00       	jmp    c01029d7 <__alltraps>

c0102893 <vector229>:
.globl vector229
vector229:
  pushl $0
c0102893:	6a 00                	push   $0x0
  pushl $229
c0102895:	68 e5 00 00 00       	push   $0xe5
  jmp __alltraps
c010289a:	e9 38 01 00 00       	jmp    c01029d7 <__alltraps>

c010289f <vector230>:
.globl vector230
vector230:
  pushl $0
c010289f:	6a 00                	push   $0x0
  pushl $230
c01028a1:	68 e6 00 00 00       	push   $0xe6
  jmp __alltraps
c01028a6:	e9 2c 01 00 00       	jmp    c01029d7 <__alltraps>

c01028ab <vector231>:
.globl vector231
vector231:
  pushl $0
c01028ab:	6a 00                	push   $0x0
  pushl $231
c01028ad:	68 e7 00 00 00       	push   $0xe7
  jmp __alltraps
c01028b2:	e9 20 01 00 00       	jmp    c01029d7 <__alltraps>

c01028b7 <vector232>:
.globl vector232
vector232:
  pushl $0
c01028b7:	6a 00                	push   $0x0
  pushl $232
c01028b9:	68 e8 00 00 00       	push   $0xe8
  jmp __alltraps
c01028be:	e9 14 01 00 00       	jmp    c01029d7 <__alltraps>

c01028c3 <vector233>:
.globl vector233
vector233:
  pushl $0
c01028c3:	6a 00                	push   $0x0
  pushl $233
c01028c5:	68 e9 00 00 00       	push   $0xe9
  jmp __alltraps
c01028ca:	e9 08 01 00 00       	jmp    c01029d7 <__alltraps>

c01028cf <vector234>:
.globl vector234
vector234:
  pushl $0
c01028cf:	6a 00                	push   $0x0
  pushl $234
c01028d1:	68 ea 00 00 00       	push   $0xea
  jmp __alltraps
c01028d6:	e9 fc 00 00 00       	jmp    c01029d7 <__alltraps>

c01028db <vector235>:
.globl vector235
vector235:
  pushl $0
c01028db:	6a 00                	push   $0x0
  pushl $235
c01028dd:	68 eb 00 00 00       	push   $0xeb
  jmp __alltraps
c01028e2:	e9 f0 00 00 00       	jmp    c01029d7 <__alltraps>

c01028e7 <vector236>:
.globl vector236
vector236:
  pushl $0
c01028e7:	6a 00                	push   $0x0
  pushl $236
c01028e9:	68 ec 00 00 00       	push   $0xec
  jmp __alltraps
c01028ee:	e9 e4 00 00 00       	jmp    c01029d7 <__alltraps>

c01028f3 <vector237>:
.globl vector237
vector237:
  pushl $0
c01028f3:	6a 00                	push   $0x0
  pushl $237
c01028f5:	68 ed 00 00 00       	push   $0xed
  jmp __alltraps
c01028fa:	e9 d8 00 00 00       	jmp    c01029d7 <__alltraps>

c01028ff <vector238>:
.globl vector238
vector238:
  pushl $0
c01028ff:	6a 00                	push   $0x0
  pushl $238
c0102901:	68 ee 00 00 00       	push   $0xee
  jmp __alltraps
c0102906:	e9 cc 00 00 00       	jmp    c01029d7 <__alltraps>

c010290b <vector239>:
.globl vector239
vector239:
  pushl $0
c010290b:	6a 00                	push   $0x0
  pushl $239
c010290d:	68 ef 00 00 00       	push   $0xef
  jmp __alltraps
c0102912:	e9 c0 00 00 00       	jmp    c01029d7 <__alltraps>

c0102917 <vector240>:
.globl vector240
vector240:
  pushl $0
c0102917:	6a 00                	push   $0x0
  pushl $240
c0102919:	68 f0 00 00 00       	push   $0xf0
  jmp __alltraps
c010291e:	e9 b4 00 00 00       	jmp    c01029d7 <__alltraps>

c0102923 <vector241>:
.globl vector241
vector241:
  pushl $0
c0102923:	6a 00                	push   $0x0
  pushl $241
c0102925:	68 f1 00 00 00       	push   $0xf1
  jmp __alltraps
c010292a:	e9 a8 00 00 00       	jmp    c01029d7 <__alltraps>

c010292f <vector242>:
.globl vector242
vector242:
  pushl $0
c010292f:	6a 00                	push   $0x0
  pushl $242
c0102931:	68 f2 00 00 00       	push   $0xf2
  jmp __alltraps
c0102936:	e9 9c 00 00 00       	jmp    c01029d7 <__alltraps>

c010293b <vector243>:
.globl vector243
vector243:
  pushl $0
c010293b:	6a 00                	push   $0x0
  pushl $243
c010293d:	68 f3 00 00 00       	push   $0xf3
  jmp __alltraps
c0102942:	e9 90 00 00 00       	jmp    c01029d7 <__alltraps>

c0102947 <vector244>:
.globl vector244
vector244:
  pushl $0
c0102947:	6a 00                	push   $0x0
  pushl $244
c0102949:	68 f4 00 00 00       	push   $0xf4
  jmp __alltraps
c010294e:	e9 84 00 00 00       	jmp    c01029d7 <__alltraps>

c0102953 <vector245>:
.globl vector245
vector245:
  pushl $0
c0102953:	6a 00                	push   $0x0
  pushl $245
c0102955:	68 f5 00 00 00       	push   $0xf5
  jmp __alltraps
c010295a:	e9 78 00 00 00       	jmp    c01029d7 <__alltraps>

c010295f <vector246>:
.globl vector246
vector246:
  pushl $0
c010295f:	6a 00                	push   $0x0
  pushl $246
c0102961:	68 f6 00 00 00       	push   $0xf6
  jmp __alltraps
c0102966:	e9 6c 00 00 00       	jmp    c01029d7 <__alltraps>

c010296b <vector247>:
.globl vector247
vector247:
  pushl $0
c010296b:	6a 00                	push   $0x0
  pushl $247
c010296d:	68 f7 00 00 00       	push   $0xf7
  jmp __alltraps
c0102972:	e9 60 00 00 00       	jmp    c01029d7 <__alltraps>

c0102977 <vector248>:
.globl vector248
vector248:
  pushl $0
c0102977:	6a 00                	push   $0x0
  pushl $248
c0102979:	68 f8 00 00 00       	push   $0xf8
  jmp __alltraps
c010297e:	e9 54 00 00 00       	jmp    c01029d7 <__alltraps>

c0102983 <vector249>:
.globl vector249
vector249:
  pushl $0
c0102983:	6a 00                	push   $0x0
  pushl $249
c0102985:	68 f9 00 00 00       	push   $0xf9
  jmp __alltraps
c010298a:	e9 48 00 00 00       	jmp    c01029d7 <__alltraps>

c010298f <vector250>:
.globl vector250
vector250:
  pushl $0
c010298f:	6a 00                	push   $0x0
  pushl $250
c0102991:	68 fa 00 00 00       	push   $0xfa
  jmp __alltraps
c0102996:	e9 3c 00 00 00       	jmp    c01029d7 <__alltraps>

c010299b <vector251>:
.globl vector251
vector251:
  pushl $0
c010299b:	6a 00                	push   $0x0
  pushl $251
c010299d:	68 fb 00 00 00       	push   $0xfb
  jmp __alltraps
c01029a2:	e9 30 00 00 00       	jmp    c01029d7 <__alltraps>

c01029a7 <vector252>:
.globl vector252
vector252:
  pushl $0
c01029a7:	6a 00                	push   $0x0
  pushl $252
c01029a9:	68 fc 00 00 00       	push   $0xfc
  jmp __alltraps
c01029ae:	e9 24 00 00 00       	jmp    c01029d7 <__alltraps>

c01029b3 <vector253>:
.globl vector253
vector253:
  pushl $0
c01029b3:	6a 00                	push   $0x0
  pushl $253
c01029b5:	68 fd 00 00 00       	push   $0xfd
  jmp __alltraps
c01029ba:	e9 18 00 00 00       	jmp    c01029d7 <__alltraps>

c01029bf <vector254>:
.globl vector254
vector254:
  pushl $0
c01029bf:	6a 00                	push   $0x0
  pushl $254
c01029c1:	68 fe 00 00 00       	push   $0xfe
  jmp __alltraps
c01029c6:	e9 0c 00 00 00       	jmp    c01029d7 <__alltraps>

c01029cb <vector255>:
.globl vector255
vector255:
  pushl $0
c01029cb:	6a 00                	push   $0x0
  pushl $255
c01029cd:	68 ff 00 00 00       	push   $0xff
  jmp __alltraps
c01029d2:	e9 00 00 00 00       	jmp    c01029d7 <__alltraps>

c01029d7 <__alltraps>:
.text
.globl __alltraps
__alltraps:
    # push registers to build a trap frame
    # therefore make the stack look like a struct trapframe
    pushl %ds
c01029d7:	1e                   	push   %ds
    pushl %es
c01029d8:	06                   	push   %es
    pushl %fs
c01029d9:	0f a0                	push   %fs
    pushl %gs
c01029db:	0f a8                	push   %gs
    pushal
c01029dd:	60                   	pusha  

    # load GD_KDATA into %ds and %es to set up data segments for kernel
    movl $GD_KDATA, %eax
c01029de:	b8 10 00 00 00       	mov    $0x10,%eax
    movw %ax, %ds
c01029e3:	8e d8                	mov    %eax,%ds
    movw %ax, %es
c01029e5:	8e c0                	mov    %eax,%es

    # push %esp to pass a pointer to the trapframe as an argument to trap()
    pushl %esp
c01029e7:	54                   	push   %esp

    # call trap(tf), where tf=%esp
    call trap
c01029e8:	e8 60 f5 ff ff       	call   c0101f4d <trap>

    # pop the pushed stack pointer
    popl %esp
c01029ed:	5c                   	pop    %esp

c01029ee <__trapret>:

    # return falls through to trapret...
.globl __trapret
__trapret:
    # restore registers from stack
    popal
c01029ee:	61                   	popa   

    # restore %ds, %es, %fs and %gs
    popl %gs
c01029ef:	0f a9                	pop    %gs
    popl %fs
c01029f1:	0f a1                	pop    %fs
    popl %es
c01029f3:	07                   	pop    %es
    popl %ds
c01029f4:	1f                   	pop    %ds

    # get rid of the trap number and error code
    addl $0x8, %esp
c01029f5:	83 c4 08             	add    $0x8,%esp
    iret
c01029f8:	cf                   	iret   

c01029f9 <page2ppn>:

extern struct Page *pages;
extern size_t npage;

static inline ppn_t
page2ppn(struct Page *page) {
c01029f9:	55                   	push   %ebp
c01029fa:	89 e5                	mov    %esp,%ebp
    return page - pages;
c01029fc:	a1 18 cf 11 c0       	mov    0xc011cf18,%eax
c0102a01:	8b 55 08             	mov    0x8(%ebp),%edx
c0102a04:	29 c2                	sub    %eax,%edx
c0102a06:	89 d0                	mov    %edx,%eax
c0102a08:	c1 f8 02             	sar    $0x2,%eax
c0102a0b:	69 c0 cd cc cc cc    	imul   $0xcccccccd,%eax,%eax
}
c0102a11:	5d                   	pop    %ebp
c0102a12:	c3                   	ret    

c0102a13 <page2pa>:

static inline uintptr_t
page2pa(struct Page *page) {
c0102a13:	55                   	push   %ebp
c0102a14:	89 e5                	mov    %esp,%ebp
c0102a16:	83 ec 04             	sub    $0x4,%esp
    return page2ppn(page) << PGSHIFT;
c0102a19:	8b 45 08             	mov    0x8(%ebp),%eax
c0102a1c:	89 04 24             	mov    %eax,(%esp)
c0102a1f:	e8 d5 ff ff ff       	call   c01029f9 <page2ppn>
c0102a24:	c1 e0 0c             	shl    $0xc,%eax
}
c0102a27:	c9                   	leave  
c0102a28:	c3                   	ret    

c0102a29 <pa2page>:

static inline struct Page *
pa2page(uintptr_t pa) {
c0102a29:	55                   	push   %ebp
c0102a2a:	89 e5                	mov    %esp,%ebp
c0102a2c:	83 ec 18             	sub    $0x18,%esp
    if (PPN(pa) >= npage) {
c0102a2f:	8b 45 08             	mov    0x8(%ebp),%eax
c0102a32:	c1 e8 0c             	shr    $0xc,%eax
c0102a35:	89 c2                	mov    %eax,%edx
c0102a37:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0102a3c:	39 c2                	cmp    %eax,%edx
c0102a3e:	72 1c                	jb     c0102a5c <pa2page+0x33>
        panic("pa2page called with invalid pa");
c0102a40:	c7 44 24 08 70 69 10 	movl   $0xc0106970,0x8(%esp)
c0102a47:	c0 
c0102a48:	c7 44 24 04 5a 00 00 	movl   $0x5a,0x4(%esp)
c0102a4f:	00 
c0102a50:	c7 04 24 8f 69 10 c0 	movl   $0xc010698f,(%esp)
c0102a57:	e8 ca d9 ff ff       	call   c0100426 <__panic>
    }
    return &pages[PPN(pa)];
c0102a5c:	8b 0d 18 cf 11 c0    	mov    0xc011cf18,%ecx
c0102a62:	8b 45 08             	mov    0x8(%ebp),%eax
c0102a65:	c1 e8 0c             	shr    $0xc,%eax
c0102a68:	89 c2                	mov    %eax,%edx
c0102a6a:	89 d0                	mov    %edx,%eax
c0102a6c:	c1 e0 02             	shl    $0x2,%eax
c0102a6f:	01 d0                	add    %edx,%eax
c0102a71:	c1 e0 02             	shl    $0x2,%eax
c0102a74:	01 c8                	add    %ecx,%eax
}
c0102a76:	c9                   	leave  
c0102a77:	c3                   	ret    

c0102a78 <page2kva>:

static inline void *
page2kva(struct Page *page) {
c0102a78:	55                   	push   %ebp
c0102a79:	89 e5                	mov    %esp,%ebp
c0102a7b:	83 ec 28             	sub    $0x28,%esp
    return KADDR(page2pa(page));
c0102a7e:	8b 45 08             	mov    0x8(%ebp),%eax
c0102a81:	89 04 24             	mov    %eax,(%esp)
c0102a84:	e8 8a ff ff ff       	call   c0102a13 <page2pa>
c0102a89:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0102a8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0102a8f:	c1 e8 0c             	shr    $0xc,%eax
c0102a92:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0102a95:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0102a9a:	39 45 f0             	cmp    %eax,-0x10(%ebp)
c0102a9d:	72 23                	jb     c0102ac2 <page2kva+0x4a>
c0102a9f:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0102aa2:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0102aa6:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c0102aad:	c0 
c0102aae:	c7 44 24 04 61 00 00 	movl   $0x61,0x4(%esp)
c0102ab5:	00 
c0102ab6:	c7 04 24 8f 69 10 c0 	movl   $0xc010698f,(%esp)
c0102abd:	e8 64 d9 ff ff       	call   c0100426 <__panic>
c0102ac2:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0102ac5:	2d 00 00 00 40       	sub    $0x40000000,%eax
}
c0102aca:	c9                   	leave  
c0102acb:	c3                   	ret    

c0102acc <pte2page>:
kva2page(void *kva) {
    return pa2page(PADDR(kva));
}

static inline struct Page *
pte2page(pte_t pte) {
c0102acc:	55                   	push   %ebp
c0102acd:	89 e5                	mov    %esp,%ebp
c0102acf:	83 ec 18             	sub    $0x18,%esp
    if (!(pte & PTE_P)) {
c0102ad2:	8b 45 08             	mov    0x8(%ebp),%eax
c0102ad5:	83 e0 01             	and    $0x1,%eax
c0102ad8:	85 c0                	test   %eax,%eax
c0102ada:	75 1c                	jne    c0102af8 <pte2page+0x2c>
        panic("pte2page called with invalid pte");
c0102adc:	c7 44 24 08 c4 69 10 	movl   $0xc01069c4,0x8(%esp)
c0102ae3:	c0 
c0102ae4:	c7 44 24 04 6c 00 00 	movl   $0x6c,0x4(%esp)
c0102aeb:	00 
c0102aec:	c7 04 24 8f 69 10 c0 	movl   $0xc010698f,(%esp)
c0102af3:	e8 2e d9 ff ff       	call   c0100426 <__panic>
    }
    return pa2page(PTE_ADDR(pte));
c0102af8:	8b 45 08             	mov    0x8(%ebp),%eax
c0102afb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0102b00:	89 04 24             	mov    %eax,(%esp)
c0102b03:	e8 21 ff ff ff       	call   c0102a29 <pa2page>
}
c0102b08:	c9                   	leave  
c0102b09:	c3                   	ret    

c0102b0a <pde2page>:

static inline struct Page *
pde2page(pde_t pde) {
c0102b0a:	55                   	push   %ebp
c0102b0b:	89 e5                	mov    %esp,%ebp
c0102b0d:	83 ec 18             	sub    $0x18,%esp
    return pa2page(PDE_ADDR(pde));
c0102b10:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b13:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0102b18:	89 04 24             	mov    %eax,(%esp)
c0102b1b:	e8 09 ff ff ff       	call   c0102a29 <pa2page>
}
c0102b20:	c9                   	leave  
c0102b21:	c3                   	ret    

c0102b22 <page_ref>:

static inline int
page_ref(struct Page *page) {
c0102b22:	55                   	push   %ebp
c0102b23:	89 e5                	mov    %esp,%ebp
    return page->ref;
c0102b25:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b28:	8b 00                	mov    (%eax),%eax
}
c0102b2a:	5d                   	pop    %ebp
c0102b2b:	c3                   	ret    

c0102b2c <set_page_ref>:

static inline void
set_page_ref(struct Page *page, int val) {
c0102b2c:	55                   	push   %ebp
c0102b2d:	89 e5                	mov    %esp,%ebp
    page->ref = val;
c0102b2f:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b32:	8b 55 0c             	mov    0xc(%ebp),%edx
c0102b35:	89 10                	mov    %edx,(%eax)
}
c0102b37:	90                   	nop
c0102b38:	5d                   	pop    %ebp
c0102b39:	c3                   	ret    

c0102b3a <page_ref_inc>:

static inline int
page_ref_inc(struct Page *page) {
c0102b3a:	55                   	push   %ebp
c0102b3b:	89 e5                	mov    %esp,%ebp
    page->ref += 1;
c0102b3d:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b40:	8b 00                	mov    (%eax),%eax
c0102b42:	8d 50 01             	lea    0x1(%eax),%edx
c0102b45:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b48:	89 10                	mov    %edx,(%eax)
    return page->ref;
c0102b4a:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b4d:	8b 00                	mov    (%eax),%eax
}
c0102b4f:	5d                   	pop    %ebp
c0102b50:	c3                   	ret    

c0102b51 <page_ref_dec>:

static inline int
page_ref_dec(struct Page *page) {
c0102b51:	55                   	push   %ebp
c0102b52:	89 e5                	mov    %esp,%ebp
    page->ref -= 1;
c0102b54:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b57:	8b 00                	mov    (%eax),%eax
c0102b59:	8d 50 ff             	lea    -0x1(%eax),%edx
c0102b5c:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b5f:	89 10                	mov    %edx,(%eax)
    return page->ref;
c0102b61:	8b 45 08             	mov    0x8(%ebp),%eax
c0102b64:	8b 00                	mov    (%eax),%eax
}
c0102b66:	5d                   	pop    %ebp
c0102b67:	c3                   	ret    

c0102b68 <__intr_save>:
__intr_save(void) {
c0102b68:	55                   	push   %ebp
c0102b69:	89 e5                	mov    %esp,%ebp
c0102b6b:	83 ec 18             	sub    $0x18,%esp
    asm volatile ("pushfl; popl %0" : "=r" (eflags));
c0102b6e:	9c                   	pushf  
c0102b6f:	58                   	pop    %eax
c0102b70:	89 45 f4             	mov    %eax,-0xc(%ebp)
    return eflags;
c0102b73:	8b 45 f4             	mov    -0xc(%ebp),%eax
    if (read_eflags() & FL_IF) {
c0102b76:	25 00 02 00 00       	and    $0x200,%eax
c0102b7b:	85 c0                	test   %eax,%eax
c0102b7d:	74 0c                	je     c0102b8b <__intr_save+0x23>
        intr_disable();
c0102b7f:	e8 05 ee ff ff       	call   c0101989 <intr_disable>
        return 1;
c0102b84:	b8 01 00 00 00       	mov    $0x1,%eax
c0102b89:	eb 05                	jmp    c0102b90 <__intr_save+0x28>
    return 0;
c0102b8b:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0102b90:	c9                   	leave  
c0102b91:	c3                   	ret    

c0102b92 <__intr_restore>:
__intr_restore(bool flag) {
c0102b92:	55                   	push   %ebp
c0102b93:	89 e5                	mov    %esp,%ebp
c0102b95:	83 ec 08             	sub    $0x8,%esp
    if (flag) {
c0102b98:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c0102b9c:	74 05                	je     c0102ba3 <__intr_restore+0x11>
        intr_enable();
c0102b9e:	e8 da ed ff ff       	call   c010197d <intr_enable>
}
c0102ba3:	90                   	nop
c0102ba4:	c9                   	leave  
c0102ba5:	c3                   	ret    

c0102ba6 <lgdt>:
/* *
 * lgdt - load the global descriptor table register and reset the
 * data/code segement registers for kernel.
 * */
static inline void
lgdt(struct pseudodesc *pd) {
c0102ba6:	55                   	push   %ebp
c0102ba7:	89 e5                	mov    %esp,%ebp
    asm volatile ("lgdt (%0)" :: "r" (pd));
c0102ba9:	8b 45 08             	mov    0x8(%ebp),%eax
c0102bac:	0f 01 10             	lgdtl  (%eax)
    asm volatile ("movw %%ax, %%gs" :: "a" (USER_DS));
c0102baf:	b8 23 00 00 00       	mov    $0x23,%eax
c0102bb4:	8e e8                	mov    %eax,%gs
    asm volatile ("movw %%ax, %%fs" :: "a" (USER_DS));
c0102bb6:	b8 23 00 00 00       	mov    $0x23,%eax
c0102bbb:	8e e0                	mov    %eax,%fs
    asm volatile ("movw %%ax, %%es" :: "a" (KERNEL_DS));
c0102bbd:	b8 10 00 00 00       	mov    $0x10,%eax
c0102bc2:	8e c0                	mov    %eax,%es
    asm volatile ("movw %%ax, %%ds" :: "a" (KERNEL_DS));
c0102bc4:	b8 10 00 00 00       	mov    $0x10,%eax
c0102bc9:	8e d8                	mov    %eax,%ds
    asm volatile ("movw %%ax, %%ss" :: "a" (KERNEL_DS));
c0102bcb:	b8 10 00 00 00       	mov    $0x10,%eax
c0102bd0:	8e d0                	mov    %eax,%ss
    // reload cs
    asm volatile ("ljmp %0, $1f\n 1:\n" :: "i" (KERNEL_CS));
c0102bd2:	ea d9 2b 10 c0 08 00 	ljmp   $0x8,$0xc0102bd9
}
c0102bd9:	90                   	nop
c0102bda:	5d                   	pop    %ebp
c0102bdb:	c3                   	ret    

c0102bdc <load_esp0>:
 * load_esp0 - change the ESP0 in default task state segment,
 * so that we can use different kernel stack when we trap frame
 * user to kernel.
 * */
void
load_esp0(uintptr_t esp0) {
c0102bdc:	f3 0f 1e fb          	endbr32 
c0102be0:	55                   	push   %ebp
c0102be1:	89 e5                	mov    %esp,%ebp
    ts.ts_esp0 = esp0;
c0102be3:	8b 45 08             	mov    0x8(%ebp),%eax
c0102be6:	a3 a4 ce 11 c0       	mov    %eax,0xc011cea4
}
c0102beb:	90                   	nop
c0102bec:	5d                   	pop    %ebp
c0102bed:	c3                   	ret    

c0102bee <gdt_init>:

/* gdt_init - initialize the default GDT and TSS */
static void
gdt_init(void) {
c0102bee:	f3 0f 1e fb          	endbr32 
c0102bf2:	55                   	push   %ebp
c0102bf3:	89 e5                	mov    %esp,%ebp
c0102bf5:	83 ec 14             	sub    $0x14,%esp
    // set boot kernel stack and default SS0
    load_esp0((uintptr_t)bootstacktop);
c0102bf8:	b8 00 90 11 c0       	mov    $0xc0119000,%eax
c0102bfd:	89 04 24             	mov    %eax,(%esp)
c0102c00:	e8 d7 ff ff ff       	call   c0102bdc <load_esp0>
    ts.ts_ss0 = KERNEL_DS;
c0102c05:	66 c7 05 a8 ce 11 c0 	movw   $0x10,0xc011cea8
c0102c0c:	10 00 

    // initialize the TSS filed of the gdt
    gdt[SEG_TSS] = SEGTSS(STS_T32A, (uintptr_t)&ts, sizeof(ts), DPL_KERNEL);
c0102c0e:	66 c7 05 28 9a 11 c0 	movw   $0x68,0xc0119a28
c0102c15:	68 00 
c0102c17:	b8 a0 ce 11 c0       	mov    $0xc011cea0,%eax
c0102c1c:	0f b7 c0             	movzwl %ax,%eax
c0102c1f:	66 a3 2a 9a 11 c0    	mov    %ax,0xc0119a2a
c0102c25:	b8 a0 ce 11 c0       	mov    $0xc011cea0,%eax
c0102c2a:	c1 e8 10             	shr    $0x10,%eax
c0102c2d:	a2 2c 9a 11 c0       	mov    %al,0xc0119a2c
c0102c32:	0f b6 05 2d 9a 11 c0 	movzbl 0xc0119a2d,%eax
c0102c39:	24 f0                	and    $0xf0,%al
c0102c3b:	0c 09                	or     $0x9,%al
c0102c3d:	a2 2d 9a 11 c0       	mov    %al,0xc0119a2d
c0102c42:	0f b6 05 2d 9a 11 c0 	movzbl 0xc0119a2d,%eax
c0102c49:	24 ef                	and    $0xef,%al
c0102c4b:	a2 2d 9a 11 c0       	mov    %al,0xc0119a2d
c0102c50:	0f b6 05 2d 9a 11 c0 	movzbl 0xc0119a2d,%eax
c0102c57:	24 9f                	and    $0x9f,%al
c0102c59:	a2 2d 9a 11 c0       	mov    %al,0xc0119a2d
c0102c5e:	0f b6 05 2d 9a 11 c0 	movzbl 0xc0119a2d,%eax
c0102c65:	0c 80                	or     $0x80,%al
c0102c67:	a2 2d 9a 11 c0       	mov    %al,0xc0119a2d
c0102c6c:	0f b6 05 2e 9a 11 c0 	movzbl 0xc0119a2e,%eax
c0102c73:	24 f0                	and    $0xf0,%al
c0102c75:	a2 2e 9a 11 c0       	mov    %al,0xc0119a2e
c0102c7a:	0f b6 05 2e 9a 11 c0 	movzbl 0xc0119a2e,%eax
c0102c81:	24 ef                	and    $0xef,%al
c0102c83:	a2 2e 9a 11 c0       	mov    %al,0xc0119a2e
c0102c88:	0f b6 05 2e 9a 11 c0 	movzbl 0xc0119a2e,%eax
c0102c8f:	24 df                	and    $0xdf,%al
c0102c91:	a2 2e 9a 11 c0       	mov    %al,0xc0119a2e
c0102c96:	0f b6 05 2e 9a 11 c0 	movzbl 0xc0119a2e,%eax
c0102c9d:	0c 40                	or     $0x40,%al
c0102c9f:	a2 2e 9a 11 c0       	mov    %al,0xc0119a2e
c0102ca4:	0f b6 05 2e 9a 11 c0 	movzbl 0xc0119a2e,%eax
c0102cab:	24 7f                	and    $0x7f,%al
c0102cad:	a2 2e 9a 11 c0       	mov    %al,0xc0119a2e
c0102cb2:	b8 a0 ce 11 c0       	mov    $0xc011cea0,%eax
c0102cb7:	c1 e8 18             	shr    $0x18,%eax
c0102cba:	a2 2f 9a 11 c0       	mov    %al,0xc0119a2f

    // reload all segment registers
    lgdt(&gdt_pd);
c0102cbf:	c7 04 24 30 9a 11 c0 	movl   $0xc0119a30,(%esp)
c0102cc6:	e8 db fe ff ff       	call   c0102ba6 <lgdt>
c0102ccb:	66 c7 45 fe 28 00    	movw   $0x28,-0x2(%ebp)
    asm volatile ("ltr %0" :: "r" (sel) : "memory");
c0102cd1:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
c0102cd5:	0f 00 d8             	ltr    %ax
}
c0102cd8:	90                   	nop

    // load the TSS
    ltr(GD_TSS);
}
c0102cd9:	90                   	nop
c0102cda:	c9                   	leave  
c0102cdb:	c3                   	ret    

c0102cdc <init_pmm_manager>:

//init_pmm_manager - initialize a pmm_manager instance
static void
init_pmm_manager(void) {
c0102cdc:	f3 0f 1e fb          	endbr32 
c0102ce0:	55                   	push   %ebp
c0102ce1:	89 e5                	mov    %esp,%ebp
c0102ce3:	83 ec 18             	sub    $0x18,%esp
    pmm_manager = &default_pmm_manager;
c0102ce6:	c7 05 10 cf 11 c0 80 	movl   $0xc0107380,0xc011cf10
c0102ced:	73 10 c0 
    cprintf("memory management: %s\n", pmm_manager->name);
c0102cf0:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102cf5:	8b 00                	mov    (%eax),%eax
c0102cf7:	89 44 24 04          	mov    %eax,0x4(%esp)
c0102cfb:	c7 04 24 f0 69 10 c0 	movl   $0xc01069f0,(%esp)
c0102d02:	e8 b3 d5 ff ff       	call   c01002ba <cprintf>
    pmm_manager->init();
c0102d07:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102d0c:	8b 40 04             	mov    0x4(%eax),%eax
c0102d0f:	ff d0                	call   *%eax
}
c0102d11:	90                   	nop
c0102d12:	c9                   	leave  
c0102d13:	c3                   	ret    

c0102d14 <init_memmap>:

//init_memmap - call pmm->init_memmap to build Page struct for free memory  
static void
init_memmap(struct Page *base, size_t n) {
c0102d14:	f3 0f 1e fb          	endbr32 
c0102d18:	55                   	push   %ebp
c0102d19:	89 e5                	mov    %esp,%ebp
c0102d1b:	83 ec 18             	sub    $0x18,%esp
    pmm_manager->init_memmap(base, n);
c0102d1e:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102d23:	8b 40 08             	mov    0x8(%eax),%eax
c0102d26:	8b 55 0c             	mov    0xc(%ebp),%edx
c0102d29:	89 54 24 04          	mov    %edx,0x4(%esp)
c0102d2d:	8b 55 08             	mov    0x8(%ebp),%edx
c0102d30:	89 14 24             	mov    %edx,(%esp)
c0102d33:	ff d0                	call   *%eax
}
c0102d35:	90                   	nop
c0102d36:	c9                   	leave  
c0102d37:	c3                   	ret    

c0102d38 <alloc_pages>:

//alloc_pages - call pmm->alloc_pages to allocate a continuous n*PAGESIZE memory 
struct Page *
alloc_pages(size_t n) {
c0102d38:	f3 0f 1e fb          	endbr32 
c0102d3c:	55                   	push   %ebp
c0102d3d:	89 e5                	mov    %esp,%ebp
c0102d3f:	83 ec 28             	sub    $0x28,%esp
    struct Page *page=NULL;
c0102d42:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    bool intr_flag;
    local_intr_save(intr_flag);
c0102d49:	e8 1a fe ff ff       	call   c0102b68 <__intr_save>
c0102d4e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    {
        page = pmm_manager->alloc_pages(n);
c0102d51:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102d56:	8b 40 0c             	mov    0xc(%eax),%eax
c0102d59:	8b 55 08             	mov    0x8(%ebp),%edx
c0102d5c:	89 14 24             	mov    %edx,(%esp)
c0102d5f:	ff d0                	call   *%eax
c0102d61:	89 45 f4             	mov    %eax,-0xc(%ebp)
    }
    local_intr_restore(intr_flag);
c0102d64:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0102d67:	89 04 24             	mov    %eax,(%esp)
c0102d6a:	e8 23 fe ff ff       	call   c0102b92 <__intr_restore>
    return page;
c0102d6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c0102d72:	c9                   	leave  
c0102d73:	c3                   	ret    

c0102d74 <free_pages>:

//free_pages - call pmm->free_pages to free a continuous n*PAGESIZE memory 
void
free_pages(struct Page *base, size_t n) {
c0102d74:	f3 0f 1e fb          	endbr32 
c0102d78:	55                   	push   %ebp
c0102d79:	89 e5                	mov    %esp,%ebp
c0102d7b:	83 ec 28             	sub    $0x28,%esp
    bool intr_flag;
    local_intr_save(intr_flag);
c0102d7e:	e8 e5 fd ff ff       	call   c0102b68 <__intr_save>
c0102d83:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        pmm_manager->free_pages(base, n);
c0102d86:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102d8b:	8b 40 10             	mov    0x10(%eax),%eax
c0102d8e:	8b 55 0c             	mov    0xc(%ebp),%edx
c0102d91:	89 54 24 04          	mov    %edx,0x4(%esp)
c0102d95:	8b 55 08             	mov    0x8(%ebp),%edx
c0102d98:	89 14 24             	mov    %edx,(%esp)
c0102d9b:	ff d0                	call   *%eax
    }
    local_intr_restore(intr_flag);
c0102d9d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0102da0:	89 04 24             	mov    %eax,(%esp)
c0102da3:	e8 ea fd ff ff       	call   c0102b92 <__intr_restore>
}
c0102da8:	90                   	nop
c0102da9:	c9                   	leave  
c0102daa:	c3                   	ret    

c0102dab <nr_free_pages>:

//nr_free_pages - call pmm->nr_free_pages to get the size (nr*PAGESIZE) 
//of current free memory
size_t
nr_free_pages(void) {
c0102dab:	f3 0f 1e fb          	endbr32 
c0102daf:	55                   	push   %ebp
c0102db0:	89 e5                	mov    %esp,%ebp
c0102db2:	83 ec 28             	sub    $0x28,%esp
    size_t ret;
    bool intr_flag;
    local_intr_save(intr_flag);
c0102db5:	e8 ae fd ff ff       	call   c0102b68 <__intr_save>
c0102dba:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        ret = pmm_manager->nr_free_pages();
c0102dbd:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c0102dc2:	8b 40 14             	mov    0x14(%eax),%eax
c0102dc5:	ff d0                	call   *%eax
c0102dc7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    }
    local_intr_restore(intr_flag);
c0102dca:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0102dcd:	89 04 24             	mov    %eax,(%esp)
c0102dd0:	e8 bd fd ff ff       	call   c0102b92 <__intr_restore>
    return ret;
c0102dd5:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
c0102dd8:	c9                   	leave  
c0102dd9:	c3                   	ret    

c0102dda <page_init>:

/* pmm_init - initialize the physical memory management */
static void
page_init(void) {
c0102dda:	f3 0f 1e fb          	endbr32 
c0102dde:	55                   	push   %ebp
c0102ddf:	89 e5                	mov    %esp,%ebp
c0102de1:	57                   	push   %edi
c0102de2:	56                   	push   %esi
c0102de3:	53                   	push   %ebx
c0102de4:	81 ec 9c 00 00 00    	sub    $0x9c,%esp
    struct e820map *memmap = (struct e820map *)(0x8000 + KERNBASE);
c0102dea:	c7 45 c4 00 80 00 c0 	movl   $0xc0008000,-0x3c(%ebp)
    uint64_t maxpa = 0;
c0102df1:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
c0102df8:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)

    cprintf("e820map:\n");
c0102dff:	c7 04 24 07 6a 10 c0 	movl   $0xc0106a07,(%esp)
c0102e06:	e8 af d4 ff ff       	call   c01002ba <cprintf>
    int i;
    for (i = 0; i < memmap->nr_map; i ++) {
c0102e0b:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
c0102e12:	e9 1a 01 00 00       	jmp    c0102f31 <page_init+0x157>
        uint64_t begin = memmap->map[i].addr, end = begin + memmap->map[i].size;
c0102e17:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0102e1a:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102e1d:	89 d0                	mov    %edx,%eax
c0102e1f:	c1 e0 02             	shl    $0x2,%eax
c0102e22:	01 d0                	add    %edx,%eax
c0102e24:	c1 e0 02             	shl    $0x2,%eax
c0102e27:	01 c8                	add    %ecx,%eax
c0102e29:	8b 50 08             	mov    0x8(%eax),%edx
c0102e2c:	8b 40 04             	mov    0x4(%eax),%eax
c0102e2f:	89 45 a0             	mov    %eax,-0x60(%ebp)
c0102e32:	89 55 a4             	mov    %edx,-0x5c(%ebp)
c0102e35:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0102e38:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102e3b:	89 d0                	mov    %edx,%eax
c0102e3d:	c1 e0 02             	shl    $0x2,%eax
c0102e40:	01 d0                	add    %edx,%eax
c0102e42:	c1 e0 02             	shl    $0x2,%eax
c0102e45:	01 c8                	add    %ecx,%eax
c0102e47:	8b 48 0c             	mov    0xc(%eax),%ecx
c0102e4a:	8b 58 10             	mov    0x10(%eax),%ebx
c0102e4d:	8b 45 a0             	mov    -0x60(%ebp),%eax
c0102e50:	8b 55 a4             	mov    -0x5c(%ebp),%edx
c0102e53:	01 c8                	add    %ecx,%eax
c0102e55:	11 da                	adc    %ebx,%edx
c0102e57:	89 45 98             	mov    %eax,-0x68(%ebp)
c0102e5a:	89 55 9c             	mov    %edx,-0x64(%ebp)
        cprintf("  memory: %08llx, [%08llx, %08llx], type = %d.\n",
c0102e5d:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0102e60:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102e63:	89 d0                	mov    %edx,%eax
c0102e65:	c1 e0 02             	shl    $0x2,%eax
c0102e68:	01 d0                	add    %edx,%eax
c0102e6a:	c1 e0 02             	shl    $0x2,%eax
c0102e6d:	01 c8                	add    %ecx,%eax
c0102e6f:	83 c0 14             	add    $0x14,%eax
c0102e72:	8b 00                	mov    (%eax),%eax
c0102e74:	89 45 84             	mov    %eax,-0x7c(%ebp)
c0102e77:	8b 45 98             	mov    -0x68(%ebp),%eax
c0102e7a:	8b 55 9c             	mov    -0x64(%ebp),%edx
c0102e7d:	83 c0 ff             	add    $0xffffffff,%eax
c0102e80:	83 d2 ff             	adc    $0xffffffff,%edx
c0102e83:	89 85 78 ff ff ff    	mov    %eax,-0x88(%ebp)
c0102e89:	89 95 7c ff ff ff    	mov    %edx,-0x84(%ebp)
c0102e8f:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0102e92:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102e95:	89 d0                	mov    %edx,%eax
c0102e97:	c1 e0 02             	shl    $0x2,%eax
c0102e9a:	01 d0                	add    %edx,%eax
c0102e9c:	c1 e0 02             	shl    $0x2,%eax
c0102e9f:	01 c8                	add    %ecx,%eax
c0102ea1:	8b 48 0c             	mov    0xc(%eax),%ecx
c0102ea4:	8b 58 10             	mov    0x10(%eax),%ebx
c0102ea7:	8b 55 84             	mov    -0x7c(%ebp),%edx
c0102eaa:	89 54 24 1c          	mov    %edx,0x1c(%esp)
c0102eae:	8b 85 78 ff ff ff    	mov    -0x88(%ebp),%eax
c0102eb4:	8b 95 7c ff ff ff    	mov    -0x84(%ebp),%edx
c0102eba:	89 44 24 14          	mov    %eax,0x14(%esp)
c0102ebe:	89 54 24 18          	mov    %edx,0x18(%esp)
c0102ec2:	8b 45 a0             	mov    -0x60(%ebp),%eax
c0102ec5:	8b 55 a4             	mov    -0x5c(%ebp),%edx
c0102ec8:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0102ecc:	89 54 24 10          	mov    %edx,0x10(%esp)
c0102ed0:	89 4c 24 04          	mov    %ecx,0x4(%esp)
c0102ed4:	89 5c 24 08          	mov    %ebx,0x8(%esp)
c0102ed8:	c7 04 24 14 6a 10 c0 	movl   $0xc0106a14,(%esp)
c0102edf:	e8 d6 d3 ff ff       	call   c01002ba <cprintf>
                memmap->map[i].size, begin, end - 1, memmap->map[i].type);
        if (memmap->map[i].type == E820_ARM) {
c0102ee4:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0102ee7:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102eea:	89 d0                	mov    %edx,%eax
c0102eec:	c1 e0 02             	shl    $0x2,%eax
c0102eef:	01 d0                	add    %edx,%eax
c0102ef1:	c1 e0 02             	shl    $0x2,%eax
c0102ef4:	01 c8                	add    %ecx,%eax
c0102ef6:	83 c0 14             	add    $0x14,%eax
c0102ef9:	8b 00                	mov    (%eax),%eax
c0102efb:	83 f8 01             	cmp    $0x1,%eax
c0102efe:	75 2e                	jne    c0102f2e <page_init+0x154>
            if (maxpa < end && begin < KMEMSIZE) {
c0102f00:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0102f03:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0102f06:	3b 45 98             	cmp    -0x68(%ebp),%eax
c0102f09:	89 d0                	mov    %edx,%eax
c0102f0b:	1b 45 9c             	sbb    -0x64(%ebp),%eax
c0102f0e:	73 1e                	jae    c0102f2e <page_init+0x154>
c0102f10:	ba ff ff ff 37       	mov    $0x37ffffff,%edx
c0102f15:	b8 00 00 00 00       	mov    $0x0,%eax
c0102f1a:	3b 55 a0             	cmp    -0x60(%ebp),%edx
c0102f1d:	1b 45 a4             	sbb    -0x5c(%ebp),%eax
c0102f20:	72 0c                	jb     c0102f2e <page_init+0x154>
                maxpa = end;
c0102f22:	8b 45 98             	mov    -0x68(%ebp),%eax
c0102f25:	8b 55 9c             	mov    -0x64(%ebp),%edx
c0102f28:	89 45 e0             	mov    %eax,-0x20(%ebp)
c0102f2b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
    for (i = 0; i < memmap->nr_map; i ++) {
c0102f2e:	ff 45 dc             	incl   -0x24(%ebp)
c0102f31:	8b 45 c4             	mov    -0x3c(%ebp),%eax
c0102f34:	8b 00                	mov    (%eax),%eax
c0102f36:	39 45 dc             	cmp    %eax,-0x24(%ebp)
c0102f39:	0f 8c d8 fe ff ff    	jl     c0102e17 <page_init+0x3d>
            }
        }
    }
    if (maxpa > KMEMSIZE) {
c0102f3f:	ba 00 00 00 38       	mov    $0x38000000,%edx
c0102f44:	b8 00 00 00 00       	mov    $0x0,%eax
c0102f49:	3b 55 e0             	cmp    -0x20(%ebp),%edx
c0102f4c:	1b 45 e4             	sbb    -0x1c(%ebp),%eax
c0102f4f:	73 0e                	jae    c0102f5f <page_init+0x185>
        maxpa = KMEMSIZE;
c0102f51:	c7 45 e0 00 00 00 38 	movl   $0x38000000,-0x20(%ebp)
c0102f58:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    }

    extern char end[];

    npage = maxpa / PGSIZE;
c0102f5f:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0102f62:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0102f65:	0f ac d0 0c          	shrd   $0xc,%edx,%eax
c0102f69:	c1 ea 0c             	shr    $0xc,%edx
c0102f6c:	a3 80 ce 11 c0       	mov    %eax,0xc011ce80
    pages = (struct Page *)ROUNDUP((void *)end, PGSIZE);
c0102f71:	c7 45 c0 00 10 00 00 	movl   $0x1000,-0x40(%ebp)
c0102f78:	b8 28 cf 11 c0       	mov    $0xc011cf28,%eax
c0102f7d:	8d 50 ff             	lea    -0x1(%eax),%edx
c0102f80:	8b 45 c0             	mov    -0x40(%ebp),%eax
c0102f83:	01 d0                	add    %edx,%eax
c0102f85:	89 45 bc             	mov    %eax,-0x44(%ebp)
c0102f88:	8b 45 bc             	mov    -0x44(%ebp),%eax
c0102f8b:	ba 00 00 00 00       	mov    $0x0,%edx
c0102f90:	f7 75 c0             	divl   -0x40(%ebp)
c0102f93:	8b 45 bc             	mov    -0x44(%ebp),%eax
c0102f96:	29 d0                	sub    %edx,%eax
c0102f98:	a3 18 cf 11 c0       	mov    %eax,0xc011cf18

    for (i = 0; i < npage; i ++) {
c0102f9d:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
c0102fa4:	eb 2f                	jmp    c0102fd5 <page_init+0x1fb>
        SetPageReserved(pages + i);
c0102fa6:	8b 0d 18 cf 11 c0    	mov    0xc011cf18,%ecx
c0102fac:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102faf:	89 d0                	mov    %edx,%eax
c0102fb1:	c1 e0 02             	shl    $0x2,%eax
c0102fb4:	01 d0                	add    %edx,%eax
c0102fb6:	c1 e0 02             	shl    $0x2,%eax
c0102fb9:	01 c8                	add    %ecx,%eax
c0102fbb:	83 c0 04             	add    $0x4,%eax
c0102fbe:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
c0102fc5:	89 45 90             	mov    %eax,-0x70(%ebp)
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 * */
static inline void
set_bit(int nr, volatile void *addr) {
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c0102fc8:	8b 45 90             	mov    -0x70(%ebp),%eax
c0102fcb:	8b 55 94             	mov    -0x6c(%ebp),%edx
c0102fce:	0f ab 10             	bts    %edx,(%eax)
}
c0102fd1:	90                   	nop
    for (i = 0; i < npage; i ++) {
c0102fd2:	ff 45 dc             	incl   -0x24(%ebp)
c0102fd5:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0102fd8:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0102fdd:	39 c2                	cmp    %eax,%edx
c0102fdf:	72 c5                	jb     c0102fa6 <page_init+0x1cc>
    }

    uintptr_t freemem = PADDR((uintptr_t)pages + sizeof(struct Page) * npage);
c0102fe1:	8b 15 80 ce 11 c0    	mov    0xc011ce80,%edx
c0102fe7:	89 d0                	mov    %edx,%eax
c0102fe9:	c1 e0 02             	shl    $0x2,%eax
c0102fec:	01 d0                	add    %edx,%eax
c0102fee:	c1 e0 02             	shl    $0x2,%eax
c0102ff1:	89 c2                	mov    %eax,%edx
c0102ff3:	a1 18 cf 11 c0       	mov    0xc011cf18,%eax
c0102ff8:	01 d0                	add    %edx,%eax
c0102ffa:	89 45 b8             	mov    %eax,-0x48(%ebp)
c0102ffd:	81 7d b8 ff ff ff bf 	cmpl   $0xbfffffff,-0x48(%ebp)
c0103004:	77 23                	ja     c0103029 <page_init+0x24f>
c0103006:	8b 45 b8             	mov    -0x48(%ebp),%eax
c0103009:	89 44 24 0c          	mov    %eax,0xc(%esp)
c010300d:	c7 44 24 08 44 6a 10 	movl   $0xc0106a44,0x8(%esp)
c0103014:	c0 
c0103015:	c7 44 24 04 dc 00 00 	movl   $0xdc,0x4(%esp)
c010301c:	00 
c010301d:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103024:	e8 fd d3 ff ff       	call   c0100426 <__panic>
c0103029:	8b 45 b8             	mov    -0x48(%ebp),%eax
c010302c:	05 00 00 00 40       	add    $0x40000000,%eax
c0103031:	89 45 b4             	mov    %eax,-0x4c(%ebp)

    for (i = 0; i < memmap->nr_map; i ++) {
c0103034:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
c010303b:	e9 4b 01 00 00       	jmp    c010318b <page_init+0x3b1>
        uint64_t begin = memmap->map[i].addr, end = begin + memmap->map[i].size;
c0103040:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0103043:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0103046:	89 d0                	mov    %edx,%eax
c0103048:	c1 e0 02             	shl    $0x2,%eax
c010304b:	01 d0                	add    %edx,%eax
c010304d:	c1 e0 02             	shl    $0x2,%eax
c0103050:	01 c8                	add    %ecx,%eax
c0103052:	8b 50 08             	mov    0x8(%eax),%edx
c0103055:	8b 40 04             	mov    0x4(%eax),%eax
c0103058:	89 45 d0             	mov    %eax,-0x30(%ebp)
c010305b:	89 55 d4             	mov    %edx,-0x2c(%ebp)
c010305e:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0103061:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0103064:	89 d0                	mov    %edx,%eax
c0103066:	c1 e0 02             	shl    $0x2,%eax
c0103069:	01 d0                	add    %edx,%eax
c010306b:	c1 e0 02             	shl    $0x2,%eax
c010306e:	01 c8                	add    %ecx,%eax
c0103070:	8b 48 0c             	mov    0xc(%eax),%ecx
c0103073:	8b 58 10             	mov    0x10(%eax),%ebx
c0103076:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0103079:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c010307c:	01 c8                	add    %ecx,%eax
c010307e:	11 da                	adc    %ebx,%edx
c0103080:	89 45 c8             	mov    %eax,-0x38(%ebp)
c0103083:	89 55 cc             	mov    %edx,-0x34(%ebp)
        if (memmap->map[i].type == E820_ARM) {
c0103086:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
c0103089:	8b 55 dc             	mov    -0x24(%ebp),%edx
c010308c:	89 d0                	mov    %edx,%eax
c010308e:	c1 e0 02             	shl    $0x2,%eax
c0103091:	01 d0                	add    %edx,%eax
c0103093:	c1 e0 02             	shl    $0x2,%eax
c0103096:	01 c8                	add    %ecx,%eax
c0103098:	83 c0 14             	add    $0x14,%eax
c010309b:	8b 00                	mov    (%eax),%eax
c010309d:	83 f8 01             	cmp    $0x1,%eax
c01030a0:	0f 85 e2 00 00 00    	jne    c0103188 <page_init+0x3ae>
            if (begin < freemem) {
c01030a6:	8b 45 b4             	mov    -0x4c(%ebp),%eax
c01030a9:	ba 00 00 00 00       	mov    $0x0,%edx
c01030ae:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
c01030b1:	39 45 d0             	cmp    %eax,-0x30(%ebp)
c01030b4:	19 d1                	sbb    %edx,%ecx
c01030b6:	73 0d                	jae    c01030c5 <page_init+0x2eb>
                begin = freemem;
c01030b8:	8b 45 b4             	mov    -0x4c(%ebp),%eax
c01030bb:	89 45 d0             	mov    %eax,-0x30(%ebp)
c01030be:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
            }
            if (end > KMEMSIZE) {
c01030c5:	ba 00 00 00 38       	mov    $0x38000000,%edx
c01030ca:	b8 00 00 00 00       	mov    $0x0,%eax
c01030cf:	3b 55 c8             	cmp    -0x38(%ebp),%edx
c01030d2:	1b 45 cc             	sbb    -0x34(%ebp),%eax
c01030d5:	73 0e                	jae    c01030e5 <page_init+0x30b>
                end = KMEMSIZE;
c01030d7:	c7 45 c8 00 00 00 38 	movl   $0x38000000,-0x38(%ebp)
c01030de:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%ebp)
            }
            if (begin < end) {
c01030e5:	8b 45 d0             	mov    -0x30(%ebp),%eax
c01030e8:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c01030eb:	3b 45 c8             	cmp    -0x38(%ebp),%eax
c01030ee:	89 d0                	mov    %edx,%eax
c01030f0:	1b 45 cc             	sbb    -0x34(%ebp),%eax
c01030f3:	0f 83 8f 00 00 00    	jae    c0103188 <page_init+0x3ae>
                begin = ROUNDUP(begin, PGSIZE);
c01030f9:	c7 45 b0 00 10 00 00 	movl   $0x1000,-0x50(%ebp)
c0103100:	8b 55 d0             	mov    -0x30(%ebp),%edx
c0103103:	8b 45 b0             	mov    -0x50(%ebp),%eax
c0103106:	01 d0                	add    %edx,%eax
c0103108:	48                   	dec    %eax
c0103109:	89 45 ac             	mov    %eax,-0x54(%ebp)
c010310c:	8b 45 ac             	mov    -0x54(%ebp),%eax
c010310f:	ba 00 00 00 00       	mov    $0x0,%edx
c0103114:	f7 75 b0             	divl   -0x50(%ebp)
c0103117:	8b 45 ac             	mov    -0x54(%ebp),%eax
c010311a:	29 d0                	sub    %edx,%eax
c010311c:	ba 00 00 00 00       	mov    $0x0,%edx
c0103121:	89 45 d0             	mov    %eax,-0x30(%ebp)
c0103124:	89 55 d4             	mov    %edx,-0x2c(%ebp)
                end = ROUNDDOWN(end, PGSIZE);
c0103127:	8b 45 c8             	mov    -0x38(%ebp),%eax
c010312a:	89 45 a8             	mov    %eax,-0x58(%ebp)
c010312d:	8b 45 a8             	mov    -0x58(%ebp),%eax
c0103130:	ba 00 00 00 00       	mov    $0x0,%edx
c0103135:	89 c3                	mov    %eax,%ebx
c0103137:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
c010313d:	89 de                	mov    %ebx,%esi
c010313f:	89 d0                	mov    %edx,%eax
c0103141:	83 e0 00             	and    $0x0,%eax
c0103144:	89 c7                	mov    %eax,%edi
c0103146:	89 75 c8             	mov    %esi,-0x38(%ebp)
c0103149:	89 7d cc             	mov    %edi,-0x34(%ebp)
                if (begin < end) {
c010314c:	8b 45 d0             	mov    -0x30(%ebp),%eax
c010314f:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c0103152:	3b 45 c8             	cmp    -0x38(%ebp),%eax
c0103155:	89 d0                	mov    %edx,%eax
c0103157:	1b 45 cc             	sbb    -0x34(%ebp),%eax
c010315a:	73 2c                	jae    c0103188 <page_init+0x3ae>
                    init_memmap(pa2page(begin), (end - begin) / PGSIZE);
c010315c:	8b 45 c8             	mov    -0x38(%ebp),%eax
c010315f:	8b 55 cc             	mov    -0x34(%ebp),%edx
c0103162:	2b 45 d0             	sub    -0x30(%ebp),%eax
c0103165:	1b 55 d4             	sbb    -0x2c(%ebp),%edx
c0103168:	0f ac d0 0c          	shrd   $0xc,%edx,%eax
c010316c:	c1 ea 0c             	shr    $0xc,%edx
c010316f:	89 c3                	mov    %eax,%ebx
c0103171:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0103174:	89 04 24             	mov    %eax,(%esp)
c0103177:	e8 ad f8 ff ff       	call   c0102a29 <pa2page>
c010317c:	89 5c 24 04          	mov    %ebx,0x4(%esp)
c0103180:	89 04 24             	mov    %eax,(%esp)
c0103183:	e8 8c fb ff ff       	call   c0102d14 <init_memmap>
    for (i = 0; i < memmap->nr_map; i ++) {
c0103188:	ff 45 dc             	incl   -0x24(%ebp)
c010318b:	8b 45 c4             	mov    -0x3c(%ebp),%eax
c010318e:	8b 00                	mov    (%eax),%eax
c0103190:	39 45 dc             	cmp    %eax,-0x24(%ebp)
c0103193:	0f 8c a7 fe ff ff    	jl     c0103040 <page_init+0x266>
                }
            }
        }
    }
}
c0103199:	90                   	nop
c010319a:	90                   	nop
c010319b:	81 c4 9c 00 00 00    	add    $0x9c,%esp
c01031a1:	5b                   	pop    %ebx
c01031a2:	5e                   	pop    %esi
c01031a3:	5f                   	pop    %edi
c01031a4:	5d                   	pop    %ebp
c01031a5:	c3                   	ret    

c01031a6 <boot_map_segment>:
//  la:   linear address of this memory need to map (after x86 segment map)
//  size: memory size
//  pa:   physical address of this memory
//  perm: permission of this memory  
static void
boot_map_segment(pde_t *pgdir, uintptr_t la, size_t size, uintptr_t pa, uint32_t perm) {
c01031a6:	f3 0f 1e fb          	endbr32 
c01031aa:	55                   	push   %ebp
c01031ab:	89 e5                	mov    %esp,%ebp
c01031ad:	83 ec 38             	sub    $0x38,%esp
    assert(PGOFF(la) == PGOFF(pa));
c01031b0:	8b 45 0c             	mov    0xc(%ebp),%eax
c01031b3:	33 45 14             	xor    0x14(%ebp),%eax
c01031b6:	25 ff 0f 00 00       	and    $0xfff,%eax
c01031bb:	85 c0                	test   %eax,%eax
c01031bd:	74 24                	je     c01031e3 <boot_map_segment+0x3d>
c01031bf:	c7 44 24 0c 76 6a 10 	movl   $0xc0106a76,0xc(%esp)
c01031c6:	c0 
c01031c7:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c01031ce:	c0 
c01031cf:	c7 44 24 04 fa 00 00 	movl   $0xfa,0x4(%esp)
c01031d6:	00 
c01031d7:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01031de:	e8 43 d2 ff ff       	call   c0100426 <__panic>
    size_t n = ROUNDUP(size + PGOFF(la), PGSIZE) / PGSIZE;
c01031e3:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
c01031ea:	8b 45 0c             	mov    0xc(%ebp),%eax
c01031ed:	25 ff 0f 00 00       	and    $0xfff,%eax
c01031f2:	89 c2                	mov    %eax,%edx
c01031f4:	8b 45 10             	mov    0x10(%ebp),%eax
c01031f7:	01 c2                	add    %eax,%edx
c01031f9:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01031fc:	01 d0                	add    %edx,%eax
c01031fe:	48                   	dec    %eax
c01031ff:	89 45 ec             	mov    %eax,-0x14(%ebp)
c0103202:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0103205:	ba 00 00 00 00       	mov    $0x0,%edx
c010320a:	f7 75 f0             	divl   -0x10(%ebp)
c010320d:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0103210:	29 d0                	sub    %edx,%eax
c0103212:	c1 e8 0c             	shr    $0xc,%eax
c0103215:	89 45 f4             	mov    %eax,-0xc(%ebp)
    la = ROUNDDOWN(la, PGSIZE);
c0103218:	8b 45 0c             	mov    0xc(%ebp),%eax
c010321b:	89 45 e8             	mov    %eax,-0x18(%ebp)
c010321e:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0103221:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0103226:	89 45 0c             	mov    %eax,0xc(%ebp)
    pa = ROUNDDOWN(pa, PGSIZE);
c0103229:	8b 45 14             	mov    0x14(%ebp),%eax
c010322c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c010322f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103232:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0103237:	89 45 14             	mov    %eax,0x14(%ebp)
    for (; n > 0; n --, la += PGSIZE, pa += PGSIZE) {
c010323a:	eb 68                	jmp    c01032a4 <boot_map_segment+0xfe>
        pte_t *ptep = get_pte(pgdir, la, 1);
c010323c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
c0103243:	00 
c0103244:	8b 45 0c             	mov    0xc(%ebp),%eax
c0103247:	89 44 24 04          	mov    %eax,0x4(%esp)
c010324b:	8b 45 08             	mov    0x8(%ebp),%eax
c010324e:	89 04 24             	mov    %eax,(%esp)
c0103251:	e8 8a 01 00 00       	call   c01033e0 <get_pte>
c0103256:	89 45 e0             	mov    %eax,-0x20(%ebp)
        assert(ptep != NULL);
c0103259:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
c010325d:	75 24                	jne    c0103283 <boot_map_segment+0xdd>
c010325f:	c7 44 24 0c a2 6a 10 	movl   $0xc0106aa2,0xc(%esp)
c0103266:	c0 
c0103267:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010326e:	c0 
c010326f:	c7 44 24 04 00 01 00 	movl   $0x100,0x4(%esp)
c0103276:	00 
c0103277:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010327e:	e8 a3 d1 ff ff       	call   c0100426 <__panic>
        *ptep = pa | PTE_P | perm;
c0103283:	8b 45 14             	mov    0x14(%ebp),%eax
c0103286:	0b 45 18             	or     0x18(%ebp),%eax
c0103289:	83 c8 01             	or     $0x1,%eax
c010328c:	89 c2                	mov    %eax,%edx
c010328e:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0103291:	89 10                	mov    %edx,(%eax)
    for (; n > 0; n --, la += PGSIZE, pa += PGSIZE) {
c0103293:	ff 4d f4             	decl   -0xc(%ebp)
c0103296:	81 45 0c 00 10 00 00 	addl   $0x1000,0xc(%ebp)
c010329d:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
c01032a4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01032a8:	75 92                	jne    c010323c <boot_map_segment+0x96>
    }
}
c01032aa:	90                   	nop
c01032ab:	90                   	nop
c01032ac:	c9                   	leave  
c01032ad:	c3                   	ret    

c01032ae <boot_alloc_page>:

//boot_alloc_page - allocate one page using pmm->alloc_pages(1) 
// return value: the kernel virtual address of this allocated page
//note: this function is used to get the memory for PDT(Page Directory Table)&PT(Page Table)
static void *
boot_alloc_page(void) {
c01032ae:	f3 0f 1e fb          	endbr32 
c01032b2:	55                   	push   %ebp
c01032b3:	89 e5                	mov    %esp,%ebp
c01032b5:	83 ec 28             	sub    $0x28,%esp
    struct Page *p = alloc_page();
c01032b8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c01032bf:	e8 74 fa ff ff       	call   c0102d38 <alloc_pages>
c01032c4:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (p == NULL) {
c01032c7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01032cb:	75 1c                	jne    c01032e9 <boot_alloc_page+0x3b>
        panic("boot_alloc_page failed.\n");
c01032cd:	c7 44 24 08 af 6a 10 	movl   $0xc0106aaf,0x8(%esp)
c01032d4:	c0 
c01032d5:	c7 44 24 04 0c 01 00 	movl   $0x10c,0x4(%esp)
c01032dc:	00 
c01032dd:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01032e4:	e8 3d d1 ff ff       	call   c0100426 <__panic>
    }
    return page2kva(p);
c01032e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01032ec:	89 04 24             	mov    %eax,(%esp)
c01032ef:	e8 84 f7 ff ff       	call   c0102a78 <page2kva>
}
c01032f4:	c9                   	leave  
c01032f5:	c3                   	ret    

c01032f6 <pmm_init>:

//pmm_init - setup a pmm to manage physical memory, build PDT&PT to setup paging mechanism 
//         - check the correctness of pmm & paging mechanism, print PDT&PT
void
pmm_init(void) {
c01032f6:	f3 0f 1e fb          	endbr32 
c01032fa:	55                   	push   %ebp
c01032fb:	89 e5                	mov    %esp,%ebp
c01032fd:	83 ec 38             	sub    $0x38,%esp
    // We've already enabled paging
    boot_cr3 = PADDR(boot_pgdir);
c0103300:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103305:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0103308:	81 7d f4 ff ff ff bf 	cmpl   $0xbfffffff,-0xc(%ebp)
c010330f:	77 23                	ja     c0103334 <pmm_init+0x3e>
c0103311:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103314:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103318:	c7 44 24 08 44 6a 10 	movl   $0xc0106a44,0x8(%esp)
c010331f:	c0 
c0103320:	c7 44 24 04 16 01 00 	movl   $0x116,0x4(%esp)
c0103327:	00 
c0103328:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010332f:	e8 f2 d0 ff ff       	call   c0100426 <__panic>
c0103334:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103337:	05 00 00 00 40       	add    $0x40000000,%eax
c010333c:	a3 14 cf 11 c0       	mov    %eax,0xc011cf14
    //We need to alloc/free the physical memory (granularity is 4KB or other size). 
    //So a framework of physical memory manager (struct pmm_manager)is defined in pmm.h
    //First we should init a physical memory manager(pmm) based on the framework.
    //Then pmm can alloc/free the physical memory. 
    //Now the first_fit/best_fit/worst_fit/buddy_system pmm are available.
    init_pmm_manager();
c0103341:	e8 96 f9 ff ff       	call   c0102cdc <init_pmm_manager>

    // detect physical memory space, reserve already used memory,
    // then use pmm->init_memmap to create free page list
    page_init();
c0103346:	e8 8f fa ff ff       	call   c0102dda <page_init>

    //use pmm->check to verify the correctness of the alloc/free function in a pmm
    check_alloc_page();
c010334b:	e8 5b 04 00 00       	call   c01037ab <check_alloc_page>

    check_pgdir();
c0103350:	e8 79 04 00 00       	call   c01037ce <check_pgdir>

    static_assert(KERNBASE % PTSIZE == 0 && KERNTOP % PTSIZE == 0);

    // recursively insert boot_pgdir in itself
    // to form a virtual page table at virtual address VPT
    boot_pgdir[PDX(VPT)] = PADDR(boot_pgdir) | PTE_P | PTE_W;
c0103355:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c010335a:	89 45 f0             	mov    %eax,-0x10(%ebp)
c010335d:	81 7d f0 ff ff ff bf 	cmpl   $0xbfffffff,-0x10(%ebp)
c0103364:	77 23                	ja     c0103389 <pmm_init+0x93>
c0103366:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103369:	89 44 24 0c          	mov    %eax,0xc(%esp)
c010336d:	c7 44 24 08 44 6a 10 	movl   $0xc0106a44,0x8(%esp)
c0103374:	c0 
c0103375:	c7 44 24 04 2c 01 00 	movl   $0x12c,0x4(%esp)
c010337c:	00 
c010337d:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103384:	e8 9d d0 ff ff       	call   c0100426 <__panic>
c0103389:	8b 45 f0             	mov    -0x10(%ebp),%eax
c010338c:	8d 90 00 00 00 40    	lea    0x40000000(%eax),%edx
c0103392:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103397:	05 ac 0f 00 00       	add    $0xfac,%eax
c010339c:	83 ca 03             	or     $0x3,%edx
c010339f:	89 10                	mov    %edx,(%eax)

    // map all physical memory to linear memory with base linear addr KERNBASE
    // linear_addr KERNBASE ~ KERNBASE + KMEMSIZE = phy_addr 0 ~ KMEMSIZE
    boot_map_segment(boot_pgdir, KERNBASE, KMEMSIZE, 0, PTE_W);
c01033a1:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c01033a6:	c7 44 24 10 02 00 00 	movl   $0x2,0x10(%esp)
c01033ad:	00 
c01033ae:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
c01033b5:	00 
c01033b6:	c7 44 24 08 00 00 00 	movl   $0x38000000,0x8(%esp)
c01033bd:	38 
c01033be:	c7 44 24 04 00 00 00 	movl   $0xc0000000,0x4(%esp)
c01033c5:	c0 
c01033c6:	89 04 24             	mov    %eax,(%esp)
c01033c9:	e8 d8 fd ff ff       	call   c01031a6 <boot_map_segment>

    // Since we are using bootloader's GDT,
    // we should reload gdt (second time, the last time) to get user segments and the TSS
    // map virtual_addr 0 ~ 4G = linear_addr 0 ~ 4G
    // then set kernel stack (ss:esp) in TSS, setup TSS in gdt, load TSS
    gdt_init();
c01033ce:	e8 1b f8 ff ff       	call   c0102bee <gdt_init>

    //now the basic virtual memory map(see memalyout.h) is established.
    //check the correctness of the basic virtual memory map.
    check_boot_pgdir();
c01033d3:	e8 96 0a 00 00       	call   c0103e6e <check_boot_pgdir>

    print_pgdir();
c01033d8:	e8 1b 0f 00 00       	call   c01042f8 <print_pgdir>

}
c01033dd:	90                   	nop
c01033de:	c9                   	leave  
c01033df:	c3                   	ret    

c01033e0 <get_pte>:
//  la:     the linear address need to map
//  create: a logical value to decide if alloc a page for PT
// return vaule: the kernel virtual address of this pte

pte_t *
get_pte(pde_t *pgdir, uintptr_t la, bool create) {
c01033e0:	f3 0f 1e fb          	endbr32 
c01033e4:	55                   	push   %ebp
c01033e5:	89 e5                	mov    %esp,%ebp
c01033e7:	83 ec 48             	sub    $0x48,%esp
                          // (7) set page directory entry's permission
    }
    return NULL;          // (8) return page table entry
#endif
    //找PDE
    pde_t *pdep = pgdir + PDX(la);
c01033ea:	8b 45 0c             	mov    0xc(%ebp),%eax
c01033ed:	c1 e8 16             	shr    $0x16,%eax
c01033f0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c01033f7:	8b 45 08             	mov    0x8(%ebp),%eax
c01033fa:	01 d0                	add    %edx,%eax
c01033fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //如果存在的话，返回对应的PTE即可
    if (*pdep & PTE_P) {
c01033ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103402:	8b 00                	mov    (%eax),%eax
c0103404:	83 e0 01             	and    $0x1,%eax
c0103407:	85 c0                	test   %eax,%eax
c0103409:	74 68                	je     c0103473 <get_pte+0x93>
        pte_t *ptep = (pte_t *)KADDR(*pdep & ~0x0fff) + PTX(la);
c010340b:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010340e:	8b 00                	mov    (%eax),%eax
c0103410:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0103415:	89 45 d4             	mov    %eax,-0x2c(%ebp)
c0103418:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c010341b:	c1 e8 0c             	shr    $0xc,%eax
c010341e:	89 45 d0             	mov    %eax,-0x30(%ebp)
c0103421:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0103426:	39 45 d0             	cmp    %eax,-0x30(%ebp)
c0103429:	72 23                	jb     c010344e <get_pte+0x6e>
c010342b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c010342e:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103432:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c0103439:	c0 
c010343a:	c7 44 24 04 6f 01 00 	movl   $0x16f,0x4(%esp)
c0103441:	00 
c0103442:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103449:	e8 d8 cf ff ff       	call   c0100426 <__panic>
c010344e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0103451:	2d 00 00 00 40       	sub    $0x40000000,%eax
c0103456:	89 c2                	mov    %eax,%edx
c0103458:	8b 45 0c             	mov    0xc(%ebp),%eax
c010345b:	c1 e8 0c             	shr    $0xc,%eax
c010345e:	25 ff 03 00 00       	and    $0x3ff,%eax
c0103463:	c1 e0 02             	shl    $0x2,%eax
c0103466:	01 d0                	add    %edx,%eax
c0103468:	89 45 cc             	mov    %eax,-0x34(%ebp)
        return ptep;
c010346b:	8b 45 cc             	mov    -0x34(%ebp),%eax
c010346e:	e9 10 01 00 00       	jmp    c0103583 <get_pte+0x1a3>
    }
    //如果不存在的话分配给它page
    struct Page *page;
    if (!create || ((page = alloc_page()) == NULL))
c0103473:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c0103477:	74 15                	je     c010348e <get_pte+0xae>
c0103479:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0103480:	e8 b3 f8 ff ff       	call   c0102d38 <alloc_pages>
c0103485:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0103488:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c010348c:	75 0a                	jne    c0103498 <get_pte+0xb8>
     {
        return NULL;
c010348e:	b8 00 00 00 00       	mov    $0x0,%eax
c0103493:	e9 eb 00 00 00       	jmp    c0103583 <get_pte+0x1a3>
    }
    //设置page reference
    set_page_ref(page, 1);
c0103498:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c010349f:	00 
c01034a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01034a3:	89 04 24             	mov    %eax,(%esp)
c01034a6:	e8 81 f6 ff ff       	call   c0102b2c <set_page_ref>
    //得到page的线性地址
    uintptr_t pa = page2pa(page) & ~0x0fff;
c01034ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01034ae:	89 04 24             	mov    %eax,(%esp)
c01034b1:	e8 5d f5 ff ff       	call   c0102a13 <page2pa>
c01034b6:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c01034bb:	89 45 ec             	mov    %eax,-0x14(%ebp)
    //用memset清除page
    memset((void *)KADDR(pa), 0, PGSIZE);
c01034be:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01034c1:	89 45 e8             	mov    %eax,-0x18(%ebp)
c01034c4:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01034c7:	c1 e8 0c             	shr    $0xc,%eax
c01034ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c01034cd:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c01034d2:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
c01034d5:	72 23                	jb     c01034fa <get_pte+0x11a>
c01034d7:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01034da:	89 44 24 0c          	mov    %eax,0xc(%esp)
c01034de:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c01034e5:	c0 
c01034e6:	c7 44 24 04 7d 01 00 	movl   $0x17d,0x4(%esp)
c01034ed:	00 
c01034ee:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01034f5:	e8 2c cf ff ff       	call   c0100426 <__panic>
c01034fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01034fd:	2d 00 00 00 40       	sub    $0x40000000,%eax
c0103502:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
c0103509:	00 
c010350a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c0103511:	00 
c0103512:	89 04 24             	mov    %eax,(%esp)
c0103515:	e8 dd 24 00 00       	call   c01059f7 <memset>
    //设置PDE权限
    *pdep = pa | PTE_P | PTE_W | PTE_U;
c010351a:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010351d:	83 c8 07             	or     $0x7,%eax
c0103520:	89 c2                	mov    %eax,%edx
c0103522:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103525:	89 10                	mov    %edx,(%eax)
    //返回PTE
    pte_t *ptep = (pte_t *)KADDR(pa) + PTX(la);
c0103527:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010352a:	89 45 e0             	mov    %eax,-0x20(%ebp)
c010352d:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0103530:	c1 e8 0c             	shr    $0xc,%eax
c0103533:	89 45 dc             	mov    %eax,-0x24(%ebp)
c0103536:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c010353b:	39 45 dc             	cmp    %eax,-0x24(%ebp)
c010353e:	72 23                	jb     c0103563 <get_pte+0x183>
c0103540:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0103543:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103547:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c010354e:	c0 
c010354f:	c7 44 24 04 81 01 00 	movl   $0x181,0x4(%esp)
c0103556:	00 
c0103557:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010355e:	e8 c3 ce ff ff       	call   c0100426 <__panic>
c0103563:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0103566:	2d 00 00 00 40       	sub    $0x40000000,%eax
c010356b:	89 c2                	mov    %eax,%edx
c010356d:	8b 45 0c             	mov    0xc(%ebp),%eax
c0103570:	c1 e8 0c             	shr    $0xc,%eax
c0103573:	25 ff 03 00 00       	and    $0x3ff,%eax
c0103578:	c1 e0 02             	shl    $0x2,%eax
c010357b:	01 d0                	add    %edx,%eax
c010357d:	89 45 d8             	mov    %eax,-0x28(%ebp)

    return ptep;
c0103580:	8b 45 d8             	mov    -0x28(%ebp),%eax
}
c0103583:	c9                   	leave  
c0103584:	c3                   	ret    

c0103585 <get_page>:


//get_page - get related Page struct for linear address la using PDT pgdir
struct Page *
get_page(pde_t *pgdir, uintptr_t la, pte_t **ptep_store) {
c0103585:	f3 0f 1e fb          	endbr32 
c0103589:	55                   	push   %ebp
c010358a:	89 e5                	mov    %esp,%ebp
c010358c:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 0);
c010358f:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c0103596:	00 
c0103597:	8b 45 0c             	mov    0xc(%ebp),%eax
c010359a:	89 44 24 04          	mov    %eax,0x4(%esp)
c010359e:	8b 45 08             	mov    0x8(%ebp),%eax
c01035a1:	89 04 24             	mov    %eax,(%esp)
c01035a4:	e8 37 fe ff ff       	call   c01033e0 <get_pte>
c01035a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep_store != NULL) {
c01035ac:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c01035b0:	74 08                	je     c01035ba <get_page+0x35>
        *ptep_store = ptep;
c01035b2:	8b 45 10             	mov    0x10(%ebp),%eax
c01035b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
c01035b8:	89 10                	mov    %edx,(%eax)
    }
    if (ptep != NULL && *ptep & PTE_P) {
c01035ba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01035be:	74 1b                	je     c01035db <get_page+0x56>
c01035c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01035c3:	8b 00                	mov    (%eax),%eax
c01035c5:	83 e0 01             	and    $0x1,%eax
c01035c8:	85 c0                	test   %eax,%eax
c01035ca:	74 0f                	je     c01035db <get_page+0x56>
        return pte2page(*ptep);
c01035cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01035cf:	8b 00                	mov    (%eax),%eax
c01035d1:	89 04 24             	mov    %eax,(%esp)
c01035d4:	e8 f3 f4 ff ff       	call   c0102acc <pte2page>
c01035d9:	eb 05                	jmp    c01035e0 <get_page+0x5b>
    }
    return NULL;
c01035db:	b8 00 00 00 00       	mov    $0x0,%eax
}
c01035e0:	c9                   	leave  
c01035e1:	c3                   	ret    

c01035e2 <page_remove_pte>:

//page_remove_pte - free an Page sturct which is related linear address la
//                - and clean(invalidate) pte which is related linear address la
//note: PT is changed, so the TLB need to be invalidate 
static inline void
page_remove_pte(pde_t *pgdir, uintptr_t la, pte_t *ptep) {
c01035e2:	55                   	push   %ebp
c01035e3:	89 e5                	mov    %esp,%ebp
c01035e5:	83 ec 28             	sub    $0x28,%esp
                                  //(6) flush tlb
    }
#endif
    //检查page directory是否存在
    //练习二我们已经学到 PTE_P用于表示page dir是否存在
    if (*ptep & PTE_P) 
c01035e8:	8b 45 10             	mov    0x10(%ebp),%eax
c01035eb:	8b 00                	mov    (%eax),%eax
c01035ed:	83 e0 01             	and    $0x1,%eax
c01035f0:	85 c0                	test   %eax,%eax
c01035f2:	74 4d                	je     c0103641 <page_remove_pte+0x5f>
    {
        struct Page *page = pte2page(*ptep);
c01035f4:	8b 45 10             	mov    0x10(%ebp),%eax
c01035f7:	8b 00                	mov    (%eax),%eax
c01035f9:	89 04 24             	mov    %eax,(%esp)
c01035fc:	e8 cb f4 ff ff       	call   c0102acc <pte2page>
c0103601:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //  (page_ref_dec(page)将page的ref减1,并返回减1之后的ref  
        if (page_ref_dec(page) == 0) //如果ref为0,则free即可
c0103604:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103607:	89 04 24             	mov    %eax,(%esp)
c010360a:	e8 42 f5 ff ff       	call   c0102b51 <page_ref_dec>
c010360f:	85 c0                	test   %eax,%eax
c0103611:	75 13                	jne    c0103626 <page_remove_pte+0x44>
        {
            free_page(page);
c0103613:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c010361a:	00 
c010361b:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010361e:	89 04 24             	mov    %eax,(%esp)
c0103621:	e8 4e f7 ff ff       	call   c0102d74 <free_pages>
        }
        //清除第二个页表 PTE
        *ptep = 0; 
c0103626:	8b 45 10             	mov    0x10(%ebp),%eax
c0103629:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
        //刷新 tlb  
        tlb_invalidate(pgdir, la);
c010362f:	8b 45 0c             	mov    0xc(%ebp),%eax
c0103632:	89 44 24 04          	mov    %eax,0x4(%esp)
c0103636:	8b 45 08             	mov    0x8(%ebp),%eax
c0103639:	89 04 24             	mov    %eax,(%esp)
c010363c:	e8 09 01 00 00       	call   c010374a <tlb_invalidate>
    }
}
c0103641:	90                   	nop
c0103642:	c9                   	leave  
c0103643:	c3                   	ret    

c0103644 <page_remove>:

//page_remove - free an Page which is related linear address la and has an validated pte
void
page_remove(pde_t *pgdir, uintptr_t la) {
c0103644:	f3 0f 1e fb          	endbr32 
c0103648:	55                   	push   %ebp
c0103649:	89 e5                	mov    %esp,%ebp
c010364b:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 0);
c010364e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c0103655:	00 
c0103656:	8b 45 0c             	mov    0xc(%ebp),%eax
c0103659:	89 44 24 04          	mov    %eax,0x4(%esp)
c010365d:	8b 45 08             	mov    0x8(%ebp),%eax
c0103660:	89 04 24             	mov    %eax,(%esp)
c0103663:	e8 78 fd ff ff       	call   c01033e0 <get_pte>
c0103668:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep != NULL) {
c010366b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c010366f:	74 19                	je     c010368a <page_remove+0x46>
        page_remove_pte(pgdir, la, ptep);
c0103671:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103674:	89 44 24 08          	mov    %eax,0x8(%esp)
c0103678:	8b 45 0c             	mov    0xc(%ebp),%eax
c010367b:	89 44 24 04          	mov    %eax,0x4(%esp)
c010367f:	8b 45 08             	mov    0x8(%ebp),%eax
c0103682:	89 04 24             	mov    %eax,(%esp)
c0103685:	e8 58 ff ff ff       	call   c01035e2 <page_remove_pte>
    }
}
c010368a:	90                   	nop
c010368b:	c9                   	leave  
c010368c:	c3                   	ret    

c010368d <page_insert>:
//  la:    the linear address need to map
//  perm:  the permission of this Page which is setted in related pte
// return value: always 0
//note: PT is changed, so the TLB need to be invalidate 
int
page_insert(pde_t *pgdir, struct Page *page, uintptr_t la, uint32_t perm) {
c010368d:	f3 0f 1e fb          	endbr32 
c0103691:	55                   	push   %ebp
c0103692:	89 e5                	mov    %esp,%ebp
c0103694:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 1);
c0103697:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
c010369e:	00 
c010369f:	8b 45 10             	mov    0x10(%ebp),%eax
c01036a2:	89 44 24 04          	mov    %eax,0x4(%esp)
c01036a6:	8b 45 08             	mov    0x8(%ebp),%eax
c01036a9:	89 04 24             	mov    %eax,(%esp)
c01036ac:	e8 2f fd ff ff       	call   c01033e0 <get_pte>
c01036b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep == NULL) {
c01036b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01036b8:	75 0a                	jne    c01036c4 <page_insert+0x37>
        return -E_NO_MEM;
c01036ba:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
c01036bf:	e9 84 00 00 00       	jmp    c0103748 <page_insert+0xbb>
    }
    page_ref_inc(page);
c01036c4:	8b 45 0c             	mov    0xc(%ebp),%eax
c01036c7:	89 04 24             	mov    %eax,(%esp)
c01036ca:	e8 6b f4 ff ff       	call   c0102b3a <page_ref_inc>
    if (*ptep & PTE_P) {
c01036cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01036d2:	8b 00                	mov    (%eax),%eax
c01036d4:	83 e0 01             	and    $0x1,%eax
c01036d7:	85 c0                	test   %eax,%eax
c01036d9:	74 3e                	je     c0103719 <page_insert+0x8c>
        struct Page *p = pte2page(*ptep);
c01036db:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01036de:	8b 00                	mov    (%eax),%eax
c01036e0:	89 04 24             	mov    %eax,(%esp)
c01036e3:	e8 e4 f3 ff ff       	call   c0102acc <pte2page>
c01036e8:	89 45 f0             	mov    %eax,-0x10(%ebp)
        if (p == page) {
c01036eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01036ee:	3b 45 0c             	cmp    0xc(%ebp),%eax
c01036f1:	75 0d                	jne    c0103700 <page_insert+0x73>
            page_ref_dec(page);
c01036f3:	8b 45 0c             	mov    0xc(%ebp),%eax
c01036f6:	89 04 24             	mov    %eax,(%esp)
c01036f9:	e8 53 f4 ff ff       	call   c0102b51 <page_ref_dec>
c01036fe:	eb 19                	jmp    c0103719 <page_insert+0x8c>
        }
        else {
            page_remove_pte(pgdir, la, ptep);
c0103700:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103703:	89 44 24 08          	mov    %eax,0x8(%esp)
c0103707:	8b 45 10             	mov    0x10(%ebp),%eax
c010370a:	89 44 24 04          	mov    %eax,0x4(%esp)
c010370e:	8b 45 08             	mov    0x8(%ebp),%eax
c0103711:	89 04 24             	mov    %eax,(%esp)
c0103714:	e8 c9 fe ff ff       	call   c01035e2 <page_remove_pte>
        }
    }
    *ptep = page2pa(page) | PTE_P | perm;
c0103719:	8b 45 0c             	mov    0xc(%ebp),%eax
c010371c:	89 04 24             	mov    %eax,(%esp)
c010371f:	e8 ef f2 ff ff       	call   c0102a13 <page2pa>
c0103724:	0b 45 14             	or     0x14(%ebp),%eax
c0103727:	83 c8 01             	or     $0x1,%eax
c010372a:	89 c2                	mov    %eax,%edx
c010372c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010372f:	89 10                	mov    %edx,(%eax)
    tlb_invalidate(pgdir, la);
c0103731:	8b 45 10             	mov    0x10(%ebp),%eax
c0103734:	89 44 24 04          	mov    %eax,0x4(%esp)
c0103738:	8b 45 08             	mov    0x8(%ebp),%eax
c010373b:	89 04 24             	mov    %eax,(%esp)
c010373e:	e8 07 00 00 00       	call   c010374a <tlb_invalidate>
    return 0;
c0103743:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0103748:	c9                   	leave  
c0103749:	c3                   	ret    

c010374a <tlb_invalidate>:

// invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
void
tlb_invalidate(pde_t *pgdir, uintptr_t la) {
c010374a:	f3 0f 1e fb          	endbr32 
c010374e:	55                   	push   %ebp
c010374f:	89 e5                	mov    %esp,%ebp
c0103751:	83 ec 28             	sub    $0x28,%esp
}

static inline uintptr_t
rcr3(void) {
    uintptr_t cr3;
    asm volatile ("mov %%cr3, %0" : "=r" (cr3) :: "memory");
c0103754:	0f 20 d8             	mov    %cr3,%eax
c0103757:	89 45 f0             	mov    %eax,-0x10(%ebp)
    return cr3;
c010375a:	8b 55 f0             	mov    -0x10(%ebp),%edx
    if (rcr3() == PADDR(pgdir)) {
c010375d:	8b 45 08             	mov    0x8(%ebp),%eax
c0103760:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0103763:	81 7d f4 ff ff ff bf 	cmpl   $0xbfffffff,-0xc(%ebp)
c010376a:	77 23                	ja     c010378f <tlb_invalidate+0x45>
c010376c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010376f:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103773:	c7 44 24 08 44 6a 10 	movl   $0xc0106a44,0x8(%esp)
c010377a:	c0 
c010377b:	c7 44 24 04 ed 01 00 	movl   $0x1ed,0x4(%esp)
c0103782:	00 
c0103783:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010378a:	e8 97 cc ff ff       	call   c0100426 <__panic>
c010378f:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103792:	05 00 00 00 40       	add    $0x40000000,%eax
c0103797:	39 d0                	cmp    %edx,%eax
c0103799:	75 0d                	jne    c01037a8 <tlb_invalidate+0x5e>
        invlpg((void *)la);
c010379b:	8b 45 0c             	mov    0xc(%ebp),%eax
c010379e:	89 45 ec             	mov    %eax,-0x14(%ebp)
}

static inline void
invlpg(void *addr) {
    asm volatile ("invlpg (%0)" :: "r" (addr) : "memory");
c01037a1:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01037a4:	0f 01 38             	invlpg (%eax)
}
c01037a7:	90                   	nop
    }
}
c01037a8:	90                   	nop
c01037a9:	c9                   	leave  
c01037aa:	c3                   	ret    

c01037ab <check_alloc_page>:

static void
check_alloc_page(void) {
c01037ab:	f3 0f 1e fb          	endbr32 
c01037af:	55                   	push   %ebp
c01037b0:	89 e5                	mov    %esp,%ebp
c01037b2:	83 ec 18             	sub    $0x18,%esp
    pmm_manager->check();
c01037b5:	a1 10 cf 11 c0       	mov    0xc011cf10,%eax
c01037ba:	8b 40 18             	mov    0x18(%eax),%eax
c01037bd:	ff d0                	call   *%eax
    cprintf("check_alloc_page() succeeded!\n");
c01037bf:	c7 04 24 c8 6a 10 c0 	movl   $0xc0106ac8,(%esp)
c01037c6:	e8 ef ca ff ff       	call   c01002ba <cprintf>
}
c01037cb:	90                   	nop
c01037cc:	c9                   	leave  
c01037cd:	c3                   	ret    

c01037ce <check_pgdir>:

static void
check_pgdir(void) {
c01037ce:	f3 0f 1e fb          	endbr32 
c01037d2:	55                   	push   %ebp
c01037d3:	89 e5                	mov    %esp,%ebp
c01037d5:	83 ec 38             	sub    $0x38,%esp
    assert(npage <= KMEMSIZE / PGSIZE);
c01037d8:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c01037dd:	3d 00 80 03 00       	cmp    $0x38000,%eax
c01037e2:	76 24                	jbe    c0103808 <check_pgdir+0x3a>
c01037e4:	c7 44 24 0c e7 6a 10 	movl   $0xc0106ae7,0xc(%esp)
c01037eb:	c0 
c01037ec:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c01037f3:	c0 
c01037f4:	c7 44 24 04 fa 01 00 	movl   $0x1fa,0x4(%esp)
c01037fb:	00 
c01037fc:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103803:	e8 1e cc ff ff       	call   c0100426 <__panic>
    assert(boot_pgdir != NULL && (uint32_t)PGOFF(boot_pgdir) == 0);
c0103808:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c010380d:	85 c0                	test   %eax,%eax
c010380f:	74 0e                	je     c010381f <check_pgdir+0x51>
c0103811:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103816:	25 ff 0f 00 00       	and    $0xfff,%eax
c010381b:	85 c0                	test   %eax,%eax
c010381d:	74 24                	je     c0103843 <check_pgdir+0x75>
c010381f:	c7 44 24 0c 04 6b 10 	movl   $0xc0106b04,0xc(%esp)
c0103826:	c0 
c0103827:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010382e:	c0 
c010382f:	c7 44 24 04 fb 01 00 	movl   $0x1fb,0x4(%esp)
c0103836:	00 
c0103837:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010383e:	e8 e3 cb ff ff       	call   c0100426 <__panic>
    assert(get_page(boot_pgdir, 0x0, NULL) == NULL);
c0103843:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103848:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c010384f:	00 
c0103850:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c0103857:	00 
c0103858:	89 04 24             	mov    %eax,(%esp)
c010385b:	e8 25 fd ff ff       	call   c0103585 <get_page>
c0103860:	85 c0                	test   %eax,%eax
c0103862:	74 24                	je     c0103888 <check_pgdir+0xba>
c0103864:	c7 44 24 0c 3c 6b 10 	movl   $0xc0106b3c,0xc(%esp)
c010386b:	c0 
c010386c:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103873:	c0 
c0103874:	c7 44 24 04 fc 01 00 	movl   $0x1fc,0x4(%esp)
c010387b:	00 
c010387c:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103883:	e8 9e cb ff ff       	call   c0100426 <__panic>

    struct Page *p1, *p2;
    p1 = alloc_page();
c0103888:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c010388f:	e8 a4 f4 ff ff       	call   c0102d38 <alloc_pages>
c0103894:	89 45 f4             	mov    %eax,-0xc(%ebp)
    assert(page_insert(boot_pgdir, p1, 0x0, 0) == 0);
c0103897:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c010389c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
c01038a3:	00 
c01038a4:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c01038ab:	00 
c01038ac:	8b 55 f4             	mov    -0xc(%ebp),%edx
c01038af:	89 54 24 04          	mov    %edx,0x4(%esp)
c01038b3:	89 04 24             	mov    %eax,(%esp)
c01038b6:	e8 d2 fd ff ff       	call   c010368d <page_insert>
c01038bb:	85 c0                	test   %eax,%eax
c01038bd:	74 24                	je     c01038e3 <check_pgdir+0x115>
c01038bf:	c7 44 24 0c 64 6b 10 	movl   $0xc0106b64,0xc(%esp)
c01038c6:	c0 
c01038c7:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c01038ce:	c0 
c01038cf:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
c01038d6:	00 
c01038d7:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01038de:	e8 43 cb ff ff       	call   c0100426 <__panic>

    pte_t *ptep;
    assert((ptep = get_pte(boot_pgdir, 0x0, 0)) != NULL);
c01038e3:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c01038e8:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c01038ef:	00 
c01038f0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c01038f7:	00 
c01038f8:	89 04 24             	mov    %eax,(%esp)
c01038fb:	e8 e0 fa ff ff       	call   c01033e0 <get_pte>
c0103900:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0103903:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0103907:	75 24                	jne    c010392d <check_pgdir+0x15f>
c0103909:	c7 44 24 0c 90 6b 10 	movl   $0xc0106b90,0xc(%esp)
c0103910:	c0 
c0103911:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103918:	c0 
c0103919:	c7 44 24 04 03 02 00 	movl   $0x203,0x4(%esp)
c0103920:	00 
c0103921:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103928:	e8 f9 ca ff ff       	call   c0100426 <__panic>
    assert(pte2page(*ptep) == p1);
c010392d:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103930:	8b 00                	mov    (%eax),%eax
c0103932:	89 04 24             	mov    %eax,(%esp)
c0103935:	e8 92 f1 ff ff       	call   c0102acc <pte2page>
c010393a:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c010393d:	74 24                	je     c0103963 <check_pgdir+0x195>
c010393f:	c7 44 24 0c bd 6b 10 	movl   $0xc0106bbd,0xc(%esp)
c0103946:	c0 
c0103947:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010394e:	c0 
c010394f:	c7 44 24 04 04 02 00 	movl   $0x204,0x4(%esp)
c0103956:	00 
c0103957:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010395e:	e8 c3 ca ff ff       	call   c0100426 <__panic>
    assert(page_ref(p1) == 1);
c0103963:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103966:	89 04 24             	mov    %eax,(%esp)
c0103969:	e8 b4 f1 ff ff       	call   c0102b22 <page_ref>
c010396e:	83 f8 01             	cmp    $0x1,%eax
c0103971:	74 24                	je     c0103997 <check_pgdir+0x1c9>
c0103973:	c7 44 24 0c d3 6b 10 	movl   $0xc0106bd3,0xc(%esp)
c010397a:	c0 
c010397b:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103982:	c0 
c0103983:	c7 44 24 04 05 02 00 	movl   $0x205,0x4(%esp)
c010398a:	00 
c010398b:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103992:	e8 8f ca ff ff       	call   c0100426 <__panic>

    ptep = &((pte_t *)KADDR(PDE_ADDR(boot_pgdir[0])))[1];
c0103997:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c010399c:	8b 00                	mov    (%eax),%eax
c010399e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c01039a3:	89 45 ec             	mov    %eax,-0x14(%ebp)
c01039a6:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01039a9:	c1 e8 0c             	shr    $0xc,%eax
c01039ac:	89 45 e8             	mov    %eax,-0x18(%ebp)
c01039af:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c01039b4:	39 45 e8             	cmp    %eax,-0x18(%ebp)
c01039b7:	72 23                	jb     c01039dc <check_pgdir+0x20e>
c01039b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01039bc:	89 44 24 0c          	mov    %eax,0xc(%esp)
c01039c0:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c01039c7:	c0 
c01039c8:	c7 44 24 04 07 02 00 	movl   $0x207,0x4(%esp)
c01039cf:	00 
c01039d0:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01039d7:	e8 4a ca ff ff       	call   c0100426 <__panic>
c01039dc:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01039df:	2d 00 00 00 40       	sub    $0x40000000,%eax
c01039e4:	83 c0 04             	add    $0x4,%eax
c01039e7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    assert(get_pte(boot_pgdir, PGSIZE, 0) == ptep);
c01039ea:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c01039ef:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c01039f6:	00 
c01039f7:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
c01039fe:	00 
c01039ff:	89 04 24             	mov    %eax,(%esp)
c0103a02:	e8 d9 f9 ff ff       	call   c01033e0 <get_pte>
c0103a07:	39 45 f0             	cmp    %eax,-0x10(%ebp)
c0103a0a:	74 24                	je     c0103a30 <check_pgdir+0x262>
c0103a0c:	c7 44 24 0c e8 6b 10 	movl   $0xc0106be8,0xc(%esp)
c0103a13:	c0 
c0103a14:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103a1b:	c0 
c0103a1c:	c7 44 24 04 08 02 00 	movl   $0x208,0x4(%esp)
c0103a23:	00 
c0103a24:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103a2b:	e8 f6 c9 ff ff       	call   c0100426 <__panic>

    p2 = alloc_page();
c0103a30:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0103a37:	e8 fc f2 ff ff       	call   c0102d38 <alloc_pages>
c0103a3c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    assert(page_insert(boot_pgdir, p2, PGSIZE, PTE_U | PTE_W) == 0);
c0103a3f:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103a44:	c7 44 24 0c 06 00 00 	movl   $0x6,0xc(%esp)
c0103a4b:	00 
c0103a4c:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
c0103a53:	00 
c0103a54:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0103a57:	89 54 24 04          	mov    %edx,0x4(%esp)
c0103a5b:	89 04 24             	mov    %eax,(%esp)
c0103a5e:	e8 2a fc ff ff       	call   c010368d <page_insert>
c0103a63:	85 c0                	test   %eax,%eax
c0103a65:	74 24                	je     c0103a8b <check_pgdir+0x2bd>
c0103a67:	c7 44 24 0c 10 6c 10 	movl   $0xc0106c10,0xc(%esp)
c0103a6e:	c0 
c0103a6f:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103a76:	c0 
c0103a77:	c7 44 24 04 0b 02 00 	movl   $0x20b,0x4(%esp)
c0103a7e:	00 
c0103a7f:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103a86:	e8 9b c9 ff ff       	call   c0100426 <__panic>
    assert((ptep = get_pte(boot_pgdir, PGSIZE, 0)) != NULL);
c0103a8b:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103a90:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c0103a97:	00 
c0103a98:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
c0103a9f:	00 
c0103aa0:	89 04 24             	mov    %eax,(%esp)
c0103aa3:	e8 38 f9 ff ff       	call   c01033e0 <get_pte>
c0103aa8:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0103aab:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0103aaf:	75 24                	jne    c0103ad5 <check_pgdir+0x307>
c0103ab1:	c7 44 24 0c 48 6c 10 	movl   $0xc0106c48,0xc(%esp)
c0103ab8:	c0 
c0103ab9:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103ac0:	c0 
c0103ac1:	c7 44 24 04 0c 02 00 	movl   $0x20c,0x4(%esp)
c0103ac8:	00 
c0103ac9:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103ad0:	e8 51 c9 ff ff       	call   c0100426 <__panic>
    assert(*ptep & PTE_U);
c0103ad5:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103ad8:	8b 00                	mov    (%eax),%eax
c0103ada:	83 e0 04             	and    $0x4,%eax
c0103add:	85 c0                	test   %eax,%eax
c0103adf:	75 24                	jne    c0103b05 <check_pgdir+0x337>
c0103ae1:	c7 44 24 0c 78 6c 10 	movl   $0xc0106c78,0xc(%esp)
c0103ae8:	c0 
c0103ae9:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103af0:	c0 
c0103af1:	c7 44 24 04 0d 02 00 	movl   $0x20d,0x4(%esp)
c0103af8:	00 
c0103af9:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103b00:	e8 21 c9 ff ff       	call   c0100426 <__panic>
    assert(*ptep & PTE_W);
c0103b05:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103b08:	8b 00                	mov    (%eax),%eax
c0103b0a:	83 e0 02             	and    $0x2,%eax
c0103b0d:	85 c0                	test   %eax,%eax
c0103b0f:	75 24                	jne    c0103b35 <check_pgdir+0x367>
c0103b11:	c7 44 24 0c 86 6c 10 	movl   $0xc0106c86,0xc(%esp)
c0103b18:	c0 
c0103b19:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103b20:	c0 
c0103b21:	c7 44 24 04 0e 02 00 	movl   $0x20e,0x4(%esp)
c0103b28:	00 
c0103b29:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103b30:	e8 f1 c8 ff ff       	call   c0100426 <__panic>
    assert(boot_pgdir[0] & PTE_U);
c0103b35:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103b3a:	8b 00                	mov    (%eax),%eax
c0103b3c:	83 e0 04             	and    $0x4,%eax
c0103b3f:	85 c0                	test   %eax,%eax
c0103b41:	75 24                	jne    c0103b67 <check_pgdir+0x399>
c0103b43:	c7 44 24 0c 94 6c 10 	movl   $0xc0106c94,0xc(%esp)
c0103b4a:	c0 
c0103b4b:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103b52:	c0 
c0103b53:	c7 44 24 04 0f 02 00 	movl   $0x20f,0x4(%esp)
c0103b5a:	00 
c0103b5b:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103b62:	e8 bf c8 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p2) == 1);
c0103b67:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103b6a:	89 04 24             	mov    %eax,(%esp)
c0103b6d:	e8 b0 ef ff ff       	call   c0102b22 <page_ref>
c0103b72:	83 f8 01             	cmp    $0x1,%eax
c0103b75:	74 24                	je     c0103b9b <check_pgdir+0x3cd>
c0103b77:	c7 44 24 0c aa 6c 10 	movl   $0xc0106caa,0xc(%esp)
c0103b7e:	c0 
c0103b7f:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103b86:	c0 
c0103b87:	c7 44 24 04 10 02 00 	movl   $0x210,0x4(%esp)
c0103b8e:	00 
c0103b8f:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103b96:	e8 8b c8 ff ff       	call   c0100426 <__panic>

    assert(page_insert(boot_pgdir, p1, PGSIZE, 0) == 0);
c0103b9b:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103ba0:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
c0103ba7:	00 
c0103ba8:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
c0103baf:	00 
c0103bb0:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0103bb3:	89 54 24 04          	mov    %edx,0x4(%esp)
c0103bb7:	89 04 24             	mov    %eax,(%esp)
c0103bba:	e8 ce fa ff ff       	call   c010368d <page_insert>
c0103bbf:	85 c0                	test   %eax,%eax
c0103bc1:	74 24                	je     c0103be7 <check_pgdir+0x419>
c0103bc3:	c7 44 24 0c bc 6c 10 	movl   $0xc0106cbc,0xc(%esp)
c0103bca:	c0 
c0103bcb:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103bd2:	c0 
c0103bd3:	c7 44 24 04 12 02 00 	movl   $0x212,0x4(%esp)
c0103bda:	00 
c0103bdb:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103be2:	e8 3f c8 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p1) == 2);
c0103be7:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103bea:	89 04 24             	mov    %eax,(%esp)
c0103bed:	e8 30 ef ff ff       	call   c0102b22 <page_ref>
c0103bf2:	83 f8 02             	cmp    $0x2,%eax
c0103bf5:	74 24                	je     c0103c1b <check_pgdir+0x44d>
c0103bf7:	c7 44 24 0c e8 6c 10 	movl   $0xc0106ce8,0xc(%esp)
c0103bfe:	c0 
c0103bff:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103c06:	c0 
c0103c07:	c7 44 24 04 13 02 00 	movl   $0x213,0x4(%esp)
c0103c0e:	00 
c0103c0f:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103c16:	e8 0b c8 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p2) == 0);
c0103c1b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103c1e:	89 04 24             	mov    %eax,(%esp)
c0103c21:	e8 fc ee ff ff       	call   c0102b22 <page_ref>
c0103c26:	85 c0                	test   %eax,%eax
c0103c28:	74 24                	je     c0103c4e <check_pgdir+0x480>
c0103c2a:	c7 44 24 0c fa 6c 10 	movl   $0xc0106cfa,0xc(%esp)
c0103c31:	c0 
c0103c32:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103c39:	c0 
c0103c3a:	c7 44 24 04 14 02 00 	movl   $0x214,0x4(%esp)
c0103c41:	00 
c0103c42:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103c49:	e8 d8 c7 ff ff       	call   c0100426 <__panic>
    assert((ptep = get_pte(boot_pgdir, PGSIZE, 0)) != NULL);
c0103c4e:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103c53:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c0103c5a:	00 
c0103c5b:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
c0103c62:	00 
c0103c63:	89 04 24             	mov    %eax,(%esp)
c0103c66:	e8 75 f7 ff ff       	call   c01033e0 <get_pte>
c0103c6b:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0103c6e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0103c72:	75 24                	jne    c0103c98 <check_pgdir+0x4ca>
c0103c74:	c7 44 24 0c 48 6c 10 	movl   $0xc0106c48,0xc(%esp)
c0103c7b:	c0 
c0103c7c:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103c83:	c0 
c0103c84:	c7 44 24 04 15 02 00 	movl   $0x215,0x4(%esp)
c0103c8b:	00 
c0103c8c:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103c93:	e8 8e c7 ff ff       	call   c0100426 <__panic>
    assert(pte2page(*ptep) == p1);
c0103c98:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103c9b:	8b 00                	mov    (%eax),%eax
c0103c9d:	89 04 24             	mov    %eax,(%esp)
c0103ca0:	e8 27 ee ff ff       	call   c0102acc <pte2page>
c0103ca5:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c0103ca8:	74 24                	je     c0103cce <check_pgdir+0x500>
c0103caa:	c7 44 24 0c bd 6b 10 	movl   $0xc0106bbd,0xc(%esp)
c0103cb1:	c0 
c0103cb2:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103cb9:	c0 
c0103cba:	c7 44 24 04 16 02 00 	movl   $0x216,0x4(%esp)
c0103cc1:	00 
c0103cc2:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103cc9:	e8 58 c7 ff ff       	call   c0100426 <__panic>
    assert((*ptep & PTE_U) == 0);
c0103cce:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103cd1:	8b 00                	mov    (%eax),%eax
c0103cd3:	83 e0 04             	and    $0x4,%eax
c0103cd6:	85 c0                	test   %eax,%eax
c0103cd8:	74 24                	je     c0103cfe <check_pgdir+0x530>
c0103cda:	c7 44 24 0c 0c 6d 10 	movl   $0xc0106d0c,0xc(%esp)
c0103ce1:	c0 
c0103ce2:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103ce9:	c0 
c0103cea:	c7 44 24 04 17 02 00 	movl   $0x217,0x4(%esp)
c0103cf1:	00 
c0103cf2:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103cf9:	e8 28 c7 ff ff       	call   c0100426 <__panic>

    page_remove(boot_pgdir, 0x0);
c0103cfe:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103d03:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c0103d0a:	00 
c0103d0b:	89 04 24             	mov    %eax,(%esp)
c0103d0e:	e8 31 f9 ff ff       	call   c0103644 <page_remove>
    assert(page_ref(p1) == 1);
c0103d13:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103d16:	89 04 24             	mov    %eax,(%esp)
c0103d19:	e8 04 ee ff ff       	call   c0102b22 <page_ref>
c0103d1e:	83 f8 01             	cmp    $0x1,%eax
c0103d21:	74 24                	je     c0103d47 <check_pgdir+0x579>
c0103d23:	c7 44 24 0c d3 6b 10 	movl   $0xc0106bd3,0xc(%esp)
c0103d2a:	c0 
c0103d2b:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103d32:	c0 
c0103d33:	c7 44 24 04 1a 02 00 	movl   $0x21a,0x4(%esp)
c0103d3a:	00 
c0103d3b:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103d42:	e8 df c6 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p2) == 0);
c0103d47:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103d4a:	89 04 24             	mov    %eax,(%esp)
c0103d4d:	e8 d0 ed ff ff       	call   c0102b22 <page_ref>
c0103d52:	85 c0                	test   %eax,%eax
c0103d54:	74 24                	je     c0103d7a <check_pgdir+0x5ac>
c0103d56:	c7 44 24 0c fa 6c 10 	movl   $0xc0106cfa,0xc(%esp)
c0103d5d:	c0 
c0103d5e:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103d65:	c0 
c0103d66:	c7 44 24 04 1b 02 00 	movl   $0x21b,0x4(%esp)
c0103d6d:	00 
c0103d6e:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103d75:	e8 ac c6 ff ff       	call   c0100426 <__panic>

    page_remove(boot_pgdir, PGSIZE);
c0103d7a:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103d7f:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
c0103d86:	00 
c0103d87:	89 04 24             	mov    %eax,(%esp)
c0103d8a:	e8 b5 f8 ff ff       	call   c0103644 <page_remove>
    assert(page_ref(p1) == 0);
c0103d8f:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103d92:	89 04 24             	mov    %eax,(%esp)
c0103d95:	e8 88 ed ff ff       	call   c0102b22 <page_ref>
c0103d9a:	85 c0                	test   %eax,%eax
c0103d9c:	74 24                	je     c0103dc2 <check_pgdir+0x5f4>
c0103d9e:	c7 44 24 0c 21 6d 10 	movl   $0xc0106d21,0xc(%esp)
c0103da5:	c0 
c0103da6:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103dad:	c0 
c0103dae:	c7 44 24 04 1e 02 00 	movl   $0x21e,0x4(%esp)
c0103db5:	00 
c0103db6:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103dbd:	e8 64 c6 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p2) == 0);
c0103dc2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103dc5:	89 04 24             	mov    %eax,(%esp)
c0103dc8:	e8 55 ed ff ff       	call   c0102b22 <page_ref>
c0103dcd:	85 c0                	test   %eax,%eax
c0103dcf:	74 24                	je     c0103df5 <check_pgdir+0x627>
c0103dd1:	c7 44 24 0c fa 6c 10 	movl   $0xc0106cfa,0xc(%esp)
c0103dd8:	c0 
c0103dd9:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103de0:	c0 
c0103de1:	c7 44 24 04 1f 02 00 	movl   $0x21f,0x4(%esp)
c0103de8:	00 
c0103de9:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103df0:	e8 31 c6 ff ff       	call   c0100426 <__panic>

    assert(page_ref(pde2page(boot_pgdir[0])) == 1);
c0103df5:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103dfa:	8b 00                	mov    (%eax),%eax
c0103dfc:	89 04 24             	mov    %eax,(%esp)
c0103dff:	e8 06 ed ff ff       	call   c0102b0a <pde2page>
c0103e04:	89 04 24             	mov    %eax,(%esp)
c0103e07:	e8 16 ed ff ff       	call   c0102b22 <page_ref>
c0103e0c:	83 f8 01             	cmp    $0x1,%eax
c0103e0f:	74 24                	je     c0103e35 <check_pgdir+0x667>
c0103e11:	c7 44 24 0c 34 6d 10 	movl   $0xc0106d34,0xc(%esp)
c0103e18:	c0 
c0103e19:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103e20:	c0 
c0103e21:	c7 44 24 04 21 02 00 	movl   $0x221,0x4(%esp)
c0103e28:	00 
c0103e29:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103e30:	e8 f1 c5 ff ff       	call   c0100426 <__panic>
    free_page(pde2page(boot_pgdir[0]));
c0103e35:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103e3a:	8b 00                	mov    (%eax),%eax
c0103e3c:	89 04 24             	mov    %eax,(%esp)
c0103e3f:	e8 c6 ec ff ff       	call   c0102b0a <pde2page>
c0103e44:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0103e4b:	00 
c0103e4c:	89 04 24             	mov    %eax,(%esp)
c0103e4f:	e8 20 ef ff ff       	call   c0102d74 <free_pages>
    boot_pgdir[0] = 0;
c0103e54:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103e59:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

    cprintf("check_pgdir() succeeded!\n");
c0103e5f:	c7 04 24 5b 6d 10 c0 	movl   $0xc0106d5b,(%esp)
c0103e66:	e8 4f c4 ff ff       	call   c01002ba <cprintf>
}
c0103e6b:	90                   	nop
c0103e6c:	c9                   	leave  
c0103e6d:	c3                   	ret    

c0103e6e <check_boot_pgdir>:

static void
check_boot_pgdir(void) {
c0103e6e:	f3 0f 1e fb          	endbr32 
c0103e72:	55                   	push   %ebp
c0103e73:	89 e5                	mov    %esp,%ebp
c0103e75:	83 ec 38             	sub    $0x38,%esp
    pte_t *ptep;
    int i;
    for (i = 0; i < npage; i += PGSIZE) {
c0103e78:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0103e7f:	e9 ca 00 00 00       	jmp    c0103f4e <check_boot_pgdir+0xe0>
        assert((ptep = get_pte(boot_pgdir, (uintptr_t)KADDR(i), 0)) != NULL);
c0103e84:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103e87:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c0103e8a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103e8d:	c1 e8 0c             	shr    $0xc,%eax
c0103e90:	89 45 e0             	mov    %eax,-0x20(%ebp)
c0103e93:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0103e98:	39 45 e0             	cmp    %eax,-0x20(%ebp)
c0103e9b:	72 23                	jb     c0103ec0 <check_boot_pgdir+0x52>
c0103e9d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103ea0:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103ea4:	c7 44 24 08 a0 69 10 	movl   $0xc01069a0,0x8(%esp)
c0103eab:	c0 
c0103eac:	c7 44 24 04 2d 02 00 	movl   $0x22d,0x4(%esp)
c0103eb3:	00 
c0103eb4:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103ebb:	e8 66 c5 ff ff       	call   c0100426 <__panic>
c0103ec0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0103ec3:	2d 00 00 00 40       	sub    $0x40000000,%eax
c0103ec8:	89 c2                	mov    %eax,%edx
c0103eca:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103ecf:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
c0103ed6:	00 
c0103ed7:	89 54 24 04          	mov    %edx,0x4(%esp)
c0103edb:	89 04 24             	mov    %eax,(%esp)
c0103ede:	e8 fd f4 ff ff       	call   c01033e0 <get_pte>
c0103ee3:	89 45 dc             	mov    %eax,-0x24(%ebp)
c0103ee6:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
c0103eea:	75 24                	jne    c0103f10 <check_boot_pgdir+0xa2>
c0103eec:	c7 44 24 0c 78 6d 10 	movl   $0xc0106d78,0xc(%esp)
c0103ef3:	c0 
c0103ef4:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103efb:	c0 
c0103efc:	c7 44 24 04 2d 02 00 	movl   $0x22d,0x4(%esp)
c0103f03:	00 
c0103f04:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103f0b:	e8 16 c5 ff ff       	call   c0100426 <__panic>
        assert(PTE_ADDR(*ptep) == i);
c0103f10:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0103f13:	8b 00                	mov    (%eax),%eax
c0103f15:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0103f1a:	89 c2                	mov    %eax,%edx
c0103f1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0103f1f:	39 c2                	cmp    %eax,%edx
c0103f21:	74 24                	je     c0103f47 <check_boot_pgdir+0xd9>
c0103f23:	c7 44 24 0c b5 6d 10 	movl   $0xc0106db5,0xc(%esp)
c0103f2a:	c0 
c0103f2b:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103f32:	c0 
c0103f33:	c7 44 24 04 2e 02 00 	movl   $0x22e,0x4(%esp)
c0103f3a:	00 
c0103f3b:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103f42:	e8 df c4 ff ff       	call   c0100426 <__panic>
    for (i = 0; i < npage; i += PGSIZE) {
c0103f47:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
c0103f4e:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0103f51:	a1 80 ce 11 c0       	mov    0xc011ce80,%eax
c0103f56:	39 c2                	cmp    %eax,%edx
c0103f58:	0f 82 26 ff ff ff    	jb     c0103e84 <check_boot_pgdir+0x16>
    }

    assert(PDE_ADDR(boot_pgdir[PDX(VPT)]) == PADDR(boot_pgdir));
c0103f5e:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103f63:	05 ac 0f 00 00       	add    $0xfac,%eax
c0103f68:	8b 00                	mov    (%eax),%eax
c0103f6a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
c0103f6f:	89 c2                	mov    %eax,%edx
c0103f71:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103f76:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0103f79:	81 7d f0 ff ff ff bf 	cmpl   $0xbfffffff,-0x10(%ebp)
c0103f80:	77 23                	ja     c0103fa5 <check_boot_pgdir+0x137>
c0103f82:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103f85:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0103f89:	c7 44 24 08 44 6a 10 	movl   $0xc0106a44,0x8(%esp)
c0103f90:	c0 
c0103f91:	c7 44 24 04 31 02 00 	movl   $0x231,0x4(%esp)
c0103f98:	00 
c0103f99:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103fa0:	e8 81 c4 ff ff       	call   c0100426 <__panic>
c0103fa5:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0103fa8:	05 00 00 00 40       	add    $0x40000000,%eax
c0103fad:	39 d0                	cmp    %edx,%eax
c0103faf:	74 24                	je     c0103fd5 <check_boot_pgdir+0x167>
c0103fb1:	c7 44 24 0c cc 6d 10 	movl   $0xc0106dcc,0xc(%esp)
c0103fb8:	c0 
c0103fb9:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103fc0:	c0 
c0103fc1:	c7 44 24 04 31 02 00 	movl   $0x231,0x4(%esp)
c0103fc8:	00 
c0103fc9:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103fd0:	e8 51 c4 ff ff       	call   c0100426 <__panic>

    assert(boot_pgdir[0] == 0);
c0103fd5:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0103fda:	8b 00                	mov    (%eax),%eax
c0103fdc:	85 c0                	test   %eax,%eax
c0103fde:	74 24                	je     c0104004 <check_boot_pgdir+0x196>
c0103fe0:	c7 44 24 0c 00 6e 10 	movl   $0xc0106e00,0xc(%esp)
c0103fe7:	c0 
c0103fe8:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0103fef:	c0 
c0103ff0:	c7 44 24 04 33 02 00 	movl   $0x233,0x4(%esp)
c0103ff7:	00 
c0103ff8:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0103fff:	e8 22 c4 ff ff       	call   c0100426 <__panic>

    struct Page *p;
    p = alloc_page();
c0104004:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c010400b:	e8 28 ed ff ff       	call   c0102d38 <alloc_pages>
c0104010:	89 45 ec             	mov    %eax,-0x14(%ebp)
    assert(page_insert(boot_pgdir, p, 0x100, PTE_W) == 0);
c0104013:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0104018:	c7 44 24 0c 02 00 00 	movl   $0x2,0xc(%esp)
c010401f:	00 
c0104020:	c7 44 24 08 00 01 00 	movl   $0x100,0x8(%esp)
c0104027:	00 
c0104028:	8b 55 ec             	mov    -0x14(%ebp),%edx
c010402b:	89 54 24 04          	mov    %edx,0x4(%esp)
c010402f:	89 04 24             	mov    %eax,(%esp)
c0104032:	e8 56 f6 ff ff       	call   c010368d <page_insert>
c0104037:	85 c0                	test   %eax,%eax
c0104039:	74 24                	je     c010405f <check_boot_pgdir+0x1f1>
c010403b:	c7 44 24 0c 14 6e 10 	movl   $0xc0106e14,0xc(%esp)
c0104042:	c0 
c0104043:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010404a:	c0 
c010404b:	c7 44 24 04 37 02 00 	movl   $0x237,0x4(%esp)
c0104052:	00 
c0104053:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010405a:	e8 c7 c3 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p) == 1);
c010405f:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104062:	89 04 24             	mov    %eax,(%esp)
c0104065:	e8 b8 ea ff ff       	call   c0102b22 <page_ref>
c010406a:	83 f8 01             	cmp    $0x1,%eax
c010406d:	74 24                	je     c0104093 <check_boot_pgdir+0x225>
c010406f:	c7 44 24 0c 42 6e 10 	movl   $0xc0106e42,0xc(%esp)
c0104076:	c0 
c0104077:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010407e:	c0 
c010407f:	c7 44 24 04 38 02 00 	movl   $0x238,0x4(%esp)
c0104086:	00 
c0104087:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010408e:	e8 93 c3 ff ff       	call   c0100426 <__panic>
    assert(page_insert(boot_pgdir, p, 0x100 + PGSIZE, PTE_W) == 0);
c0104093:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c0104098:	c7 44 24 0c 02 00 00 	movl   $0x2,0xc(%esp)
c010409f:	00 
c01040a0:	c7 44 24 08 00 11 00 	movl   $0x1100,0x8(%esp)
c01040a7:	00 
c01040a8:	8b 55 ec             	mov    -0x14(%ebp),%edx
c01040ab:	89 54 24 04          	mov    %edx,0x4(%esp)
c01040af:	89 04 24             	mov    %eax,(%esp)
c01040b2:	e8 d6 f5 ff ff       	call   c010368d <page_insert>
c01040b7:	85 c0                	test   %eax,%eax
c01040b9:	74 24                	je     c01040df <check_boot_pgdir+0x271>
c01040bb:	c7 44 24 0c 54 6e 10 	movl   $0xc0106e54,0xc(%esp)
c01040c2:	c0 
c01040c3:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c01040ca:	c0 
c01040cb:	c7 44 24 04 39 02 00 	movl   $0x239,0x4(%esp)
c01040d2:	00 
c01040d3:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01040da:	e8 47 c3 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p) == 2);
c01040df:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01040e2:	89 04 24             	mov    %eax,(%esp)
c01040e5:	e8 38 ea ff ff       	call   c0102b22 <page_ref>
c01040ea:	83 f8 02             	cmp    $0x2,%eax
c01040ed:	74 24                	je     c0104113 <check_boot_pgdir+0x2a5>
c01040ef:	c7 44 24 0c 8b 6e 10 	movl   $0xc0106e8b,0xc(%esp)
c01040f6:	c0 
c01040f7:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c01040fe:	c0 
c01040ff:	c7 44 24 04 3a 02 00 	movl   $0x23a,0x4(%esp)
c0104106:	00 
c0104107:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c010410e:	e8 13 c3 ff ff       	call   c0100426 <__panic>

    const char *str = "ucore: Hello world!!";
c0104113:	c7 45 e8 9c 6e 10 c0 	movl   $0xc0106e9c,-0x18(%ebp)
    strcpy((void *)0x100, str);
c010411a:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010411d:	89 44 24 04          	mov    %eax,0x4(%esp)
c0104121:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
c0104128:	e8 e6 15 00 00       	call   c0105713 <strcpy>
    assert(strcmp((void *)0x100, (void *)(0x100 + PGSIZE)) == 0);
c010412d:	c7 44 24 04 00 11 00 	movl   $0x1100,0x4(%esp)
c0104134:	00 
c0104135:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
c010413c:	e8 50 16 00 00       	call   c0105791 <strcmp>
c0104141:	85 c0                	test   %eax,%eax
c0104143:	74 24                	je     c0104169 <check_boot_pgdir+0x2fb>
c0104145:	c7 44 24 0c b4 6e 10 	movl   $0xc0106eb4,0xc(%esp)
c010414c:	c0 
c010414d:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c0104154:	c0 
c0104155:	c7 44 24 04 3e 02 00 	movl   $0x23e,0x4(%esp)
c010415c:	00 
c010415d:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c0104164:	e8 bd c2 ff ff       	call   c0100426 <__panic>

    *(char *)(page2kva(p) + 0x100) = '\0';
c0104169:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010416c:	89 04 24             	mov    %eax,(%esp)
c010416f:	e8 04 e9 ff ff       	call   c0102a78 <page2kva>
c0104174:	05 00 01 00 00       	add    $0x100,%eax
c0104179:	c6 00 00             	movb   $0x0,(%eax)
    assert(strlen((const char *)0x100) == 0);
c010417c:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
c0104183:	e8 2d 15 00 00       	call   c01056b5 <strlen>
c0104188:	85 c0                	test   %eax,%eax
c010418a:	74 24                	je     c01041b0 <check_boot_pgdir+0x342>
c010418c:	c7 44 24 0c ec 6e 10 	movl   $0xc0106eec,0xc(%esp)
c0104193:	c0 
c0104194:	c7 44 24 08 8d 6a 10 	movl   $0xc0106a8d,0x8(%esp)
c010419b:	c0 
c010419c:	c7 44 24 04 41 02 00 	movl   $0x241,0x4(%esp)
c01041a3:	00 
c01041a4:	c7 04 24 68 6a 10 c0 	movl   $0xc0106a68,(%esp)
c01041ab:	e8 76 c2 ff ff       	call   c0100426 <__panic>

    free_page(p);
c01041b0:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c01041b7:	00 
c01041b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01041bb:	89 04 24             	mov    %eax,(%esp)
c01041be:	e8 b1 eb ff ff       	call   c0102d74 <free_pages>
    free_page(pde2page(boot_pgdir[0]));
c01041c3:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c01041c8:	8b 00                	mov    (%eax),%eax
c01041ca:	89 04 24             	mov    %eax,(%esp)
c01041cd:	e8 38 e9 ff ff       	call   c0102b0a <pde2page>
c01041d2:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c01041d9:	00 
c01041da:	89 04 24             	mov    %eax,(%esp)
c01041dd:	e8 92 eb ff ff       	call   c0102d74 <free_pages>
    boot_pgdir[0] = 0;
c01041e2:	a1 e0 99 11 c0       	mov    0xc01199e0,%eax
c01041e7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

    cprintf("check_boot_pgdir() succeeded!\n");
c01041ed:	c7 04 24 10 6f 10 c0 	movl   $0xc0106f10,(%esp)
c01041f4:	e8 c1 c0 ff ff       	call   c01002ba <cprintf>
}
c01041f9:	90                   	nop
c01041fa:	c9                   	leave  
c01041fb:	c3                   	ret    

c01041fc <perm2str>:

//perm2str - use string 'u,r,w,-' to present the permission
static const char *
perm2str(int perm) {
c01041fc:	f3 0f 1e fb          	endbr32 
c0104200:	55                   	push   %ebp
c0104201:	89 e5                	mov    %esp,%ebp
    static char str[4];
    str[0] = (perm & PTE_U) ? 'u' : '-';
c0104203:	8b 45 08             	mov    0x8(%ebp),%eax
c0104206:	83 e0 04             	and    $0x4,%eax
c0104209:	85 c0                	test   %eax,%eax
c010420b:	74 04                	je     c0104211 <perm2str+0x15>
c010420d:	b0 75                	mov    $0x75,%al
c010420f:	eb 02                	jmp    c0104213 <perm2str+0x17>
c0104211:	b0 2d                	mov    $0x2d,%al
c0104213:	a2 08 cf 11 c0       	mov    %al,0xc011cf08
    str[1] = 'r';
c0104218:	c6 05 09 cf 11 c0 72 	movb   $0x72,0xc011cf09
    str[2] = (perm & PTE_W) ? 'w' : '-';
c010421f:	8b 45 08             	mov    0x8(%ebp),%eax
c0104222:	83 e0 02             	and    $0x2,%eax
c0104225:	85 c0                	test   %eax,%eax
c0104227:	74 04                	je     c010422d <perm2str+0x31>
c0104229:	b0 77                	mov    $0x77,%al
c010422b:	eb 02                	jmp    c010422f <perm2str+0x33>
c010422d:	b0 2d                	mov    $0x2d,%al
c010422f:	a2 0a cf 11 c0       	mov    %al,0xc011cf0a
    str[3] = '\0';
c0104234:	c6 05 0b cf 11 c0 00 	movb   $0x0,0xc011cf0b
    return str;
c010423b:	b8 08 cf 11 c0       	mov    $0xc011cf08,%eax
}
c0104240:	5d                   	pop    %ebp
c0104241:	c3                   	ret    

c0104242 <get_pgtable_items>:
//  table:       the beginning addr of table
//  left_store:  the pointer of the high side of table's next range
//  right_store: the pointer of the low side of table's next range
// return value: 0 - not a invalid item range, perm - a valid item range with perm permission 
static int
get_pgtable_items(size_t left, size_t right, size_t start, uintptr_t *table, size_t *left_store, size_t *right_store) {
c0104242:	f3 0f 1e fb          	endbr32 
c0104246:	55                   	push   %ebp
c0104247:	89 e5                	mov    %esp,%ebp
c0104249:	83 ec 10             	sub    $0x10,%esp
    if (start >= right) {
c010424c:	8b 45 10             	mov    0x10(%ebp),%eax
c010424f:	3b 45 0c             	cmp    0xc(%ebp),%eax
c0104252:	72 0d                	jb     c0104261 <get_pgtable_items+0x1f>
        return 0;
c0104254:	b8 00 00 00 00       	mov    $0x0,%eax
c0104259:	e9 98 00 00 00       	jmp    c01042f6 <get_pgtable_items+0xb4>
    }
    while (start < right && !(table[start] & PTE_P)) {
        start ++;
c010425e:	ff 45 10             	incl   0x10(%ebp)
    while (start < right && !(table[start] & PTE_P)) {
c0104261:	8b 45 10             	mov    0x10(%ebp),%eax
c0104264:	3b 45 0c             	cmp    0xc(%ebp),%eax
c0104267:	73 18                	jae    c0104281 <get_pgtable_items+0x3f>
c0104269:	8b 45 10             	mov    0x10(%ebp),%eax
c010426c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c0104273:	8b 45 14             	mov    0x14(%ebp),%eax
c0104276:	01 d0                	add    %edx,%eax
c0104278:	8b 00                	mov    (%eax),%eax
c010427a:	83 e0 01             	and    $0x1,%eax
c010427d:	85 c0                	test   %eax,%eax
c010427f:	74 dd                	je     c010425e <get_pgtable_items+0x1c>
    }
    if (start < right) {
c0104281:	8b 45 10             	mov    0x10(%ebp),%eax
c0104284:	3b 45 0c             	cmp    0xc(%ebp),%eax
c0104287:	73 68                	jae    c01042f1 <get_pgtable_items+0xaf>
        if (left_store != NULL) {
c0104289:	83 7d 18 00          	cmpl   $0x0,0x18(%ebp)
c010428d:	74 08                	je     c0104297 <get_pgtable_items+0x55>
            *left_store = start;
c010428f:	8b 45 18             	mov    0x18(%ebp),%eax
c0104292:	8b 55 10             	mov    0x10(%ebp),%edx
c0104295:	89 10                	mov    %edx,(%eax)
        }
        int perm = (table[start ++] & PTE_USER);
c0104297:	8b 45 10             	mov    0x10(%ebp),%eax
c010429a:	8d 50 01             	lea    0x1(%eax),%edx
c010429d:	89 55 10             	mov    %edx,0x10(%ebp)
c01042a0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c01042a7:	8b 45 14             	mov    0x14(%ebp),%eax
c01042aa:	01 d0                	add    %edx,%eax
c01042ac:	8b 00                	mov    (%eax),%eax
c01042ae:	83 e0 07             	and    $0x7,%eax
c01042b1:	89 45 fc             	mov    %eax,-0x4(%ebp)
        while (start < right && (table[start] & PTE_USER) == perm) {
c01042b4:	eb 03                	jmp    c01042b9 <get_pgtable_items+0x77>
            start ++;
c01042b6:	ff 45 10             	incl   0x10(%ebp)
        while (start < right && (table[start] & PTE_USER) == perm) {
c01042b9:	8b 45 10             	mov    0x10(%ebp),%eax
c01042bc:	3b 45 0c             	cmp    0xc(%ebp),%eax
c01042bf:	73 1d                	jae    c01042de <get_pgtable_items+0x9c>
c01042c1:	8b 45 10             	mov    0x10(%ebp),%eax
c01042c4:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
c01042cb:	8b 45 14             	mov    0x14(%ebp),%eax
c01042ce:	01 d0                	add    %edx,%eax
c01042d0:	8b 00                	mov    (%eax),%eax
c01042d2:	83 e0 07             	and    $0x7,%eax
c01042d5:	89 c2                	mov    %eax,%edx
c01042d7:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01042da:	39 c2                	cmp    %eax,%edx
c01042dc:	74 d8                	je     c01042b6 <get_pgtable_items+0x74>
        }
        if (right_store != NULL) {
c01042de:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
c01042e2:	74 08                	je     c01042ec <get_pgtable_items+0xaa>
            *right_store = start;
c01042e4:	8b 45 1c             	mov    0x1c(%ebp),%eax
c01042e7:	8b 55 10             	mov    0x10(%ebp),%edx
c01042ea:	89 10                	mov    %edx,(%eax)
        }
        return perm;
c01042ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01042ef:	eb 05                	jmp    c01042f6 <get_pgtable_items+0xb4>
    }
    return 0;
c01042f1:	b8 00 00 00 00       	mov    $0x0,%eax
}
c01042f6:	c9                   	leave  
c01042f7:	c3                   	ret    

c01042f8 <print_pgdir>:

//print_pgdir - print the PDT&PT
void
print_pgdir(void) {
c01042f8:	f3 0f 1e fb          	endbr32 
c01042fc:	55                   	push   %ebp
c01042fd:	89 e5                	mov    %esp,%ebp
c01042ff:	57                   	push   %edi
c0104300:	56                   	push   %esi
c0104301:	53                   	push   %ebx
c0104302:	83 ec 4c             	sub    $0x4c,%esp
    cprintf("-------------------- BEGIN --------------------\n");
c0104305:	c7 04 24 30 6f 10 c0 	movl   $0xc0106f30,(%esp)
c010430c:	e8 a9 bf ff ff       	call   c01002ba <cprintf>
    size_t left, right = 0, perm;
c0104311:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    while ((perm = get_pgtable_items(0, NPDEENTRY, right, vpd, &left, &right)) != 0) {
c0104318:	e9 fa 00 00 00       	jmp    c0104417 <print_pgdir+0x11f>
        cprintf("PDE(%03x) %08x-%08x %08x %s\n", right - left,
c010431d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0104320:	89 04 24             	mov    %eax,(%esp)
c0104323:	e8 d4 fe ff ff       	call   c01041fc <perm2str>
                left * PTSIZE, right * PTSIZE, (right - left) * PTSIZE, perm2str(perm));
c0104328:	8b 4d dc             	mov    -0x24(%ebp),%ecx
c010432b:	8b 55 e0             	mov    -0x20(%ebp),%edx
c010432e:	29 d1                	sub    %edx,%ecx
c0104330:	89 ca                	mov    %ecx,%edx
        cprintf("PDE(%03x) %08x-%08x %08x %s\n", right - left,
c0104332:	89 d6                	mov    %edx,%esi
c0104334:	c1 e6 16             	shl    $0x16,%esi
c0104337:	8b 55 dc             	mov    -0x24(%ebp),%edx
c010433a:	89 d3                	mov    %edx,%ebx
c010433c:	c1 e3 16             	shl    $0x16,%ebx
c010433f:	8b 55 e0             	mov    -0x20(%ebp),%edx
c0104342:	89 d1                	mov    %edx,%ecx
c0104344:	c1 e1 16             	shl    $0x16,%ecx
c0104347:	8b 7d dc             	mov    -0x24(%ebp),%edi
c010434a:	8b 55 e0             	mov    -0x20(%ebp),%edx
c010434d:	29 d7                	sub    %edx,%edi
c010434f:	89 fa                	mov    %edi,%edx
c0104351:	89 44 24 14          	mov    %eax,0x14(%esp)
c0104355:	89 74 24 10          	mov    %esi,0x10(%esp)
c0104359:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
c010435d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
c0104361:	89 54 24 04          	mov    %edx,0x4(%esp)
c0104365:	c7 04 24 61 6f 10 c0 	movl   $0xc0106f61,(%esp)
c010436c:	e8 49 bf ff ff       	call   c01002ba <cprintf>
        size_t l, r = left * NPTEENTRY;
c0104371:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0104374:	c1 e0 0a             	shl    $0xa,%eax
c0104377:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        while ((perm = get_pgtable_items(left * NPTEENTRY, right * NPTEENTRY, r, vpt, &l, &r)) != 0) {
c010437a:	eb 54                	jmp    c01043d0 <print_pgdir+0xd8>
            cprintf("  |-- PTE(%05x) %08x-%08x %08x %s\n", r - l,
c010437c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c010437f:	89 04 24             	mov    %eax,(%esp)
c0104382:	e8 75 fe ff ff       	call   c01041fc <perm2str>
                    l * PGSIZE, r * PGSIZE, (r - l) * PGSIZE, perm2str(perm));
c0104387:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
c010438a:	8b 55 d8             	mov    -0x28(%ebp),%edx
c010438d:	29 d1                	sub    %edx,%ecx
c010438f:	89 ca                	mov    %ecx,%edx
            cprintf("  |-- PTE(%05x) %08x-%08x %08x %s\n", r - l,
c0104391:	89 d6                	mov    %edx,%esi
c0104393:	c1 e6 0c             	shl    $0xc,%esi
c0104396:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c0104399:	89 d3                	mov    %edx,%ebx
c010439b:	c1 e3 0c             	shl    $0xc,%ebx
c010439e:	8b 55 d8             	mov    -0x28(%ebp),%edx
c01043a1:	89 d1                	mov    %edx,%ecx
c01043a3:	c1 e1 0c             	shl    $0xc,%ecx
c01043a6:	8b 7d d4             	mov    -0x2c(%ebp),%edi
c01043a9:	8b 55 d8             	mov    -0x28(%ebp),%edx
c01043ac:	29 d7                	sub    %edx,%edi
c01043ae:	89 fa                	mov    %edi,%edx
c01043b0:	89 44 24 14          	mov    %eax,0x14(%esp)
c01043b4:	89 74 24 10          	mov    %esi,0x10(%esp)
c01043b8:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
c01043bc:	89 4c 24 08          	mov    %ecx,0x8(%esp)
c01043c0:	89 54 24 04          	mov    %edx,0x4(%esp)
c01043c4:	c7 04 24 80 6f 10 c0 	movl   $0xc0106f80,(%esp)
c01043cb:	e8 ea be ff ff       	call   c01002ba <cprintf>
        while ((perm = get_pgtable_items(left * NPTEENTRY, right * NPTEENTRY, r, vpt, &l, &r)) != 0) {
c01043d0:	be 00 00 c0 fa       	mov    $0xfac00000,%esi
c01043d5:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01043d8:	8b 55 dc             	mov    -0x24(%ebp),%edx
c01043db:	89 d3                	mov    %edx,%ebx
c01043dd:	c1 e3 0a             	shl    $0xa,%ebx
c01043e0:	8b 55 e0             	mov    -0x20(%ebp),%edx
c01043e3:	89 d1                	mov    %edx,%ecx
c01043e5:	c1 e1 0a             	shl    $0xa,%ecx
c01043e8:	8d 55 d4             	lea    -0x2c(%ebp),%edx
c01043eb:	89 54 24 14          	mov    %edx,0x14(%esp)
c01043ef:	8d 55 d8             	lea    -0x28(%ebp),%edx
c01043f2:	89 54 24 10          	mov    %edx,0x10(%esp)
c01043f6:	89 74 24 0c          	mov    %esi,0xc(%esp)
c01043fa:	89 44 24 08          	mov    %eax,0x8(%esp)
c01043fe:	89 5c 24 04          	mov    %ebx,0x4(%esp)
c0104402:	89 0c 24             	mov    %ecx,(%esp)
c0104405:	e8 38 fe ff ff       	call   c0104242 <get_pgtable_items>
c010440a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c010440d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
c0104411:	0f 85 65 ff ff ff    	jne    c010437c <print_pgdir+0x84>
    while ((perm = get_pgtable_items(0, NPDEENTRY, right, vpd, &left, &right)) != 0) {
c0104417:	b9 00 b0 fe fa       	mov    $0xfafeb000,%ecx
c010441c:	8b 45 dc             	mov    -0x24(%ebp),%eax
c010441f:	8d 55 dc             	lea    -0x24(%ebp),%edx
c0104422:	89 54 24 14          	mov    %edx,0x14(%esp)
c0104426:	8d 55 e0             	lea    -0x20(%ebp),%edx
c0104429:	89 54 24 10          	mov    %edx,0x10(%esp)
c010442d:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
c0104431:	89 44 24 08          	mov    %eax,0x8(%esp)
c0104435:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
c010443c:	00 
c010443d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
c0104444:	e8 f9 fd ff ff       	call   c0104242 <get_pgtable_items>
c0104449:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c010444c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
c0104450:	0f 85 c7 fe ff ff    	jne    c010431d <print_pgdir+0x25>
        }
    }
    cprintf("--------------------- END ---------------------\n");
c0104456:	c7 04 24 a4 6f 10 c0 	movl   $0xc0106fa4,(%esp)
c010445d:	e8 58 be ff ff       	call   c01002ba <cprintf>
}
c0104462:	90                   	nop
c0104463:	83 c4 4c             	add    $0x4c,%esp
c0104466:	5b                   	pop    %ebx
c0104467:	5e                   	pop    %esi
c0104468:	5f                   	pop    %edi
c0104469:	5d                   	pop    %ebp
c010446a:	c3                   	ret    

c010446b <page2ppn>:
page2ppn(struct Page *page) {
c010446b:	55                   	push   %ebp
c010446c:	89 e5                	mov    %esp,%ebp
    return page - pages;
c010446e:	a1 18 cf 11 c0       	mov    0xc011cf18,%eax
c0104473:	8b 55 08             	mov    0x8(%ebp),%edx
c0104476:	29 c2                	sub    %eax,%edx
c0104478:	89 d0                	mov    %edx,%eax
c010447a:	c1 f8 02             	sar    $0x2,%eax
c010447d:	69 c0 cd cc cc cc    	imul   $0xcccccccd,%eax,%eax
}
c0104483:	5d                   	pop    %ebp
c0104484:	c3                   	ret    

c0104485 <page2pa>:
page2pa(struct Page *page) {
c0104485:	55                   	push   %ebp
c0104486:	89 e5                	mov    %esp,%ebp
c0104488:	83 ec 04             	sub    $0x4,%esp
    return page2ppn(page) << PGSHIFT;
c010448b:	8b 45 08             	mov    0x8(%ebp),%eax
c010448e:	89 04 24             	mov    %eax,(%esp)
c0104491:	e8 d5 ff ff ff       	call   c010446b <page2ppn>
c0104496:	c1 e0 0c             	shl    $0xc,%eax
}
c0104499:	c9                   	leave  
c010449a:	c3                   	ret    

c010449b <page_ref>:
page_ref(struct Page *page) {
c010449b:	55                   	push   %ebp
c010449c:	89 e5                	mov    %esp,%ebp
    return page->ref;
c010449e:	8b 45 08             	mov    0x8(%ebp),%eax
c01044a1:	8b 00                	mov    (%eax),%eax
}
c01044a3:	5d                   	pop    %ebp
c01044a4:	c3                   	ret    

c01044a5 <set_page_ref>:
set_page_ref(struct Page *page, int val) {
c01044a5:	55                   	push   %ebp
c01044a6:	89 e5                	mov    %esp,%ebp
    page->ref = val;
c01044a8:	8b 45 08             	mov    0x8(%ebp),%eax
c01044ab:	8b 55 0c             	mov    0xc(%ebp),%edx
c01044ae:	89 10                	mov    %edx,(%eax)
}
c01044b0:	90                   	nop
c01044b1:	5d                   	pop    %ebp
c01044b2:	c3                   	ret    

c01044b3 <default_init>:
#define free_list (free_area.free_list) //列表的头
#define nr_free (free_area.nr_free)   //空闲页的数目

//初始化free_area
static void
default_init(void) {
c01044b3:	f3 0f 1e fb          	endbr32 
c01044b7:	55                   	push   %ebp
c01044b8:	89 e5                	mov    %esp,%ebp
c01044ba:	83 ec 10             	sub    $0x10,%esp
c01044bd:	c7 45 fc 1c cf 11 c0 	movl   $0xc011cf1c,-0x4(%ebp)
 * list_init - initialize a new entry
 * @elm:        new entry to be initialized
 * */
static inline void
list_init(list_entry_t *elm) {
    elm->prev = elm->next = elm;
c01044c4:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01044c7:	8b 55 fc             	mov    -0x4(%ebp),%edx
c01044ca:	89 50 04             	mov    %edx,0x4(%eax)
c01044cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01044d0:	8b 50 04             	mov    0x4(%eax),%edx
c01044d3:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01044d6:	89 10                	mov    %edx,(%eax)
}
c01044d8:	90                   	nop
    list_init(&free_list);  //列表只有一个头指针
    nr_free = 0;   //空闲页数设为0
c01044d9:	c7 05 24 cf 11 c0 00 	movl   $0x0,0xc011cf24
c01044e0:	00 00 00 
}
c01044e3:	90                   	nop
c01044e4:	c9                   	leave  
c01044e5:	c3                   	ret    

c01044e6 <default_init_memmap>:
//初始化n个空闲页链表
static void
default_init_memmap(struct Page *base, size_t n) {
c01044e6:	f3 0f 1e fb          	endbr32 
c01044ea:	55                   	push   %ebp
c01044eb:	89 e5                	mov    %esp,%ebp
c01044ed:	83 ec 48             	sub    $0x48,%esp
    assert(n > 0);
c01044f0:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
c01044f4:	75 24                	jne    c010451a <default_init_memmap+0x34>
c01044f6:	c7 44 24 0c d8 6f 10 	movl   $0xc0106fd8,0xc(%esp)
c01044fd:	c0 
c01044fe:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104505:	c0 
c0104506:	c7 44 24 04 6e 00 00 	movl   $0x6e,0x4(%esp)
c010450d:	00 
c010450e:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104515:	e8 0c bf ff ff       	call   c0100426 <__panic>
    struct Page *p = base;  //让p为最开始的空闲页表
c010451a:	8b 45 08             	mov    0x8(%ebp),%eax
c010451d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for (; p != base + n; p ++)   
c0104520:	eb 7d                	jmp    c010459f <default_init_memmap+0xb9>
    {
        assert(PageReserved(p));//检查是否为保留页
c0104522:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104525:	83 c0 04             	add    $0x4,%eax
c0104528:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
c010452f:	89 45 ec             	mov    %eax,-0x14(%ebp)
 * @addr:   the address to count from
 * */
static inline bool
test_bit(int nr, volatile void *addr) {
    int oldbit;
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c0104532:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104535:	8b 55 f0             	mov    -0x10(%ebp),%edx
c0104538:	0f a3 10             	bt     %edx,(%eax)
c010453b:	19 c0                	sbb    %eax,%eax
c010453d:	89 45 e8             	mov    %eax,-0x18(%ebp)
    return oldbit != 0;
c0104540:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0104544:	0f 95 c0             	setne  %al
c0104547:	0f b6 c0             	movzbl %al,%eax
c010454a:	85 c0                	test   %eax,%eax
c010454c:	75 24                	jne    c0104572 <default_init_memmap+0x8c>
c010454e:	c7 44 24 0c 09 70 10 	movl   $0xc0107009,0xc(%esp)
c0104555:	c0 
c0104556:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010455d:	c0 
c010455e:	c7 44 24 04 72 00 00 	movl   $0x72,0x4(%esp)
c0104565:	00 
c0104566:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010456d:	e8 b4 be ff ff       	call   c0100426 <__panic>
        p->flags = p->property = 0;//设置标记，flag为0表示可以分配
c0104572:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104575:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
c010457c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010457f:	8b 50 08             	mov    0x8(%eax),%edx
c0104582:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104585:	89 50 04             	mov    %edx,0x4(%eax)
                                   //property为0表示不是base
        set_page_ref(p, 0); //将p的ref设置为0(调用已经写过的函数)
c0104588:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c010458f:	00 
c0104590:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104593:	89 04 24             	mov    %eax,(%esp)
c0104596:	e8 0a ff ff ff       	call   c01044a5 <set_page_ref>
    for (; p != base + n; p ++)   
c010459b:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
c010459f:	8b 55 0c             	mov    0xc(%ebp),%edx
c01045a2:	89 d0                	mov    %edx,%eax
c01045a4:	c1 e0 02             	shl    $0x2,%eax
c01045a7:	01 d0                	add    %edx,%eax
c01045a9:	c1 e0 02             	shl    $0x2,%eax
c01045ac:	89 c2                	mov    %eax,%edx
c01045ae:	8b 45 08             	mov    0x8(%ebp),%eax
c01045b1:	01 d0                	add    %edx,%eax
c01045b3:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c01045b6:	0f 85 66 ff ff ff    	jne    c0104522 <default_init_memmap+0x3c>
    }
    base->property = n;  //第一个页表也就是base的property设置为n，因为有n个空闲块
c01045bc:	8b 45 08             	mov    0x8(%ebp),%eax
c01045bf:	8b 55 0c             	mov    0xc(%ebp),%edx
c01045c2:	89 50 08             	mov    %edx,0x8(%eax)
    
    SetPageProperty(base);  
c01045c5:	8b 45 08             	mov    0x8(%ebp),%eax
c01045c8:	83 c0 04             	add    $0x4,%eax
c01045cb:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
c01045d2:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c01045d5:	8b 45 cc             	mov    -0x34(%ebp),%eax
c01045d8:	8b 55 d0             	mov    -0x30(%ebp),%edx
c01045db:	0f ab 10             	bts    %edx,(%eax)
}
c01045de:	90                   	nop
    nr_free += n;   //将空闲页数目设置为n
c01045df:	8b 15 24 cf 11 c0    	mov    0xc011cf24,%edx
c01045e5:	8b 45 0c             	mov    0xc(%ebp),%eax
c01045e8:	01 d0                	add    %edx,%eax
c01045ea:	a3 24 cf 11 c0       	mov    %eax,0xc011cf24
  // list_add(&free_list, &(base->page_link));
  //之前是list_add,等价于list_add_after,这样的话就把第一个块浪费了，所以有小问题，虽然检测不会出问题
   //改为list_add_before()就更合适了
    list_add_before(&free_list, &(base->page_link));
c01045ef:	8b 45 08             	mov    0x8(%ebp),%eax
c01045f2:	83 c0 0c             	add    $0xc,%eax
c01045f5:	c7 45 e4 1c cf 11 c0 	movl   $0xc011cf1c,-0x1c(%ebp)
c01045fc:	89 45 e0             	mov    %eax,-0x20(%ebp)
 * Insert the new element @elm *before* the element @listelm which
 * is already in the list.
 * */
static inline void
list_add_before(list_entry_t *listelm, list_entry_t *elm) {
    __list_add(elm, listelm->prev, listelm);
c01045ff:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0104602:	8b 00                	mov    (%eax),%eax
c0104604:	8b 55 e0             	mov    -0x20(%ebp),%edx
c0104607:	89 55 dc             	mov    %edx,-0x24(%ebp)
c010460a:	89 45 d8             	mov    %eax,-0x28(%ebp)
c010460d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0104610:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 * */
static inline void
__list_add(list_entry_t *elm, list_entry_t *prev, list_entry_t *next) {
    prev->next = next->prev = elm;
c0104613:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0104616:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0104619:	89 10                	mov    %edx,(%eax)
c010461b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c010461e:	8b 10                	mov    (%eax),%edx
c0104620:	8b 45 d8             	mov    -0x28(%ebp),%eax
c0104623:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
c0104626:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104629:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c010462c:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
c010462f:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104632:	8b 55 d8             	mov    -0x28(%ebp),%edx
c0104635:	89 10                	mov    %edx,(%eax)
}
c0104637:	90                   	nop
}
c0104638:	90                   	nop
}
c0104639:	90                   	nop
c010463a:	c9                   	leave  
c010463b:	c3                   	ret    

c010463c <default_alloc_pages>:


//由于是first-fit函数，故把遇到的第一个可以用于分配的连续内存进行分配即可
static struct Page * default_alloc_pages(size_t n) {
c010463c:	f3 0f 1e fb          	endbr32 
c0104640:	55                   	push   %ebp
c0104641:	89 e5                	mov    %esp,%ebp
c0104643:	83 ec 68             	sub    $0x68,%esp
    assert(n > 0);  //n的值应该大于0
c0104646:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c010464a:	75 24                	jne    c0104670 <default_alloc_pages+0x34>
c010464c:	c7 44 24 0c d8 6f 10 	movl   $0xc0106fd8,0xc(%esp)
c0104653:	c0 
c0104654:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010465b:	c0 
c010465c:	c7 44 24 04 84 00 00 	movl   $0x84,0x4(%esp)
c0104663:	00 
c0104664:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010466b:	e8 b6 bd ff ff       	call   c0100426 <__panic>
    //如果n>nf_free，表示无法分配这么大内存，返回NULL即可
    if (n > nr_free)
c0104670:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c0104675:	39 45 08             	cmp    %eax,0x8(%ebp)
c0104678:	76 0a                	jbe    c0104684 <default_alloc_pages+0x48>
     {
        return NULL;
c010467a:	b8 00 00 00 00       	mov    $0x0,%eax
c010467f:	e9 50 01 00 00       	jmp    c01047d4 <default_alloc_pages+0x198>
    }
    //说明够分配，因此找到即可
    struct Page *page = NULL;
c0104684:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    list_entry_t *le = &free_list;
c010468b:	c7 45 f0 1c cf 11 c0 	movl   $0xc011cf1c,-0x10(%ebp)
    // 查找n个或以上空闲页块，若找到，则判断是否大过n，大于n的话则将其拆分 
    //  并将拆分后的剩下的空闲页块加回到链表中
    while ((le = list_next(le)) != &free_list) 
c0104692:	eb 1c                	jmp    c01046b0 <default_alloc_pages+0x74>
    // 如果list_next(le)) == &free_list说明已经遍历完了整个双向链表
    {
        // 此处le2page就是将le的地址-page_link 在Page的偏移，从而找到 Page 的地址
        struct Page *p = le2page(le, page_link);
c0104694:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104697:	83 e8 0c             	sub    $0xc,%eax
c010469a:	89 45 ec             	mov    %eax,-0x14(%ebp)
        //说明找到可以满足的连续空闲内存了，让page等于p即可，退出循环
        if (p->property >= n) 
c010469d:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01046a0:	8b 40 08             	mov    0x8(%eax),%eax
c01046a3:	39 45 08             	cmp    %eax,0x8(%ebp)
c01046a6:	77 08                	ja     c01046b0 <default_alloc_pages+0x74>
        {
            page = p;
c01046a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01046ab:	89 45 f4             	mov    %eax,-0xc(%ebp)
            break;
c01046ae:	eb 18                	jmp    c01046c8 <default_alloc_pages+0x8c>
c01046b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01046b3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return listelm->next;
c01046b6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c01046b9:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) 
c01046bc:	89 45 f0             	mov    %eax,-0x10(%ebp)
c01046bf:	81 7d f0 1c cf 11 c0 	cmpl   $0xc011cf1c,-0x10(%ebp)
c01046c6:	75 cc                	jne    c0104694 <default_alloc_pages+0x58>
        }
    }
    if (page != NULL) 
c01046c8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c01046cc:	0f 84 ff 00 00 00    	je     c01047d1 <default_alloc_pages+0x195>
    {
        //如果property>n的话，我们需要把多出的内存加到链表里
        if (page->property > n) 
c01046d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01046d5:	8b 40 08             	mov    0x8(%eax),%eax
c01046d8:	39 45 08             	cmp    %eax,0x8(%ebp)
c01046db:	0f 83 9c 00 00 00    	jae    c010477d <default_alloc_pages+0x141>
        {
            //创建一个新的Page，起始地址为page+n
            struct Page *p = page + n; 
c01046e1:	8b 55 08             	mov    0x8(%ebp),%edx
c01046e4:	89 d0                	mov    %edx,%eax
c01046e6:	c1 e0 02             	shl    $0x2,%eax
c01046e9:	01 d0                	add    %edx,%eax
c01046eb:	c1 e0 02             	shl    $0x2,%eax
c01046ee:	89 c2                	mov    %eax,%edx
c01046f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01046f3:	01 d0                	add    %edx,%eax
c01046f5:	89 45 e8             	mov    %eax,-0x18(%ebp)
            p->property = page->property - n;  
c01046f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01046fb:	8b 40 08             	mov    0x8(%eax),%eax
c01046fe:	2b 45 08             	sub    0x8(%ebp),%eax
c0104701:	89 c2                	mov    %eax,%edx
c0104703:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0104706:	89 50 08             	mov    %edx,0x8(%eax)
            //将其property设置为page->property-n
            SetPageProperty(p);   
c0104709:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010470c:	83 c0 04             	add    $0x4,%eax
c010470f:	c7 45 c4 01 00 00 00 	movl   $0x1,-0x3c(%ebp)
c0104716:	89 45 c0             	mov    %eax,-0x40(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c0104719:	8b 45 c0             	mov    -0x40(%ebp),%eax
c010471c:	8b 55 c4             	mov    -0x3c(%ebp),%edx
c010471f:	0f ab 10             	bts    %edx,(%eax)
}
c0104722:	90                   	nop
            // 将多出来的插入到被分配掉的页块后面
            list_add(&(page->page_link), &(p->page_link));
c0104723:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0104726:	83 c0 0c             	add    $0xc,%eax
c0104729:	8b 55 f4             	mov    -0xc(%ebp),%edx
c010472c:	83 c2 0c             	add    $0xc,%edx
c010472f:	89 55 e0             	mov    %edx,-0x20(%ebp)
c0104732:	89 45 dc             	mov    %eax,-0x24(%ebp)
c0104735:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0104738:	89 45 d8             	mov    %eax,-0x28(%ebp)
c010473b:	8b 45 dc             	mov    -0x24(%ebp),%eax
c010473e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    __list_add(elm, listelm, listelm->next);
c0104741:	8b 45 d8             	mov    -0x28(%ebp),%eax
c0104744:	8b 40 04             	mov    0x4(%eax),%eax
c0104747:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c010474a:	89 55 d0             	mov    %edx,-0x30(%ebp)
c010474d:	8b 55 d8             	mov    -0x28(%ebp),%edx
c0104750:	89 55 cc             	mov    %edx,-0x34(%ebp)
c0104753:	89 45 c8             	mov    %eax,-0x38(%ebp)
    prev->next = next->prev = elm;
c0104756:	8b 45 c8             	mov    -0x38(%ebp),%eax
c0104759:	8b 55 d0             	mov    -0x30(%ebp),%edx
c010475c:	89 10                	mov    %edx,(%eax)
c010475e:	8b 45 c8             	mov    -0x38(%ebp),%eax
c0104761:	8b 10                	mov    (%eax),%edx
c0104763:	8b 45 cc             	mov    -0x34(%ebp),%eax
c0104766:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
c0104769:	8b 45 d0             	mov    -0x30(%ebp),%eax
c010476c:	8b 55 c8             	mov    -0x38(%ebp),%edx
c010476f:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
c0104772:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0104775:	8b 55 cc             	mov    -0x34(%ebp),%edx
c0104778:	89 10                	mov    %edx,(%eax)
}
c010477a:	90                   	nop
}
c010477b:	90                   	nop
}
c010477c:	90                   	nop
        }
        // 在空闲页链表中删去刚才分配的空闲页
        list_del(&(page->page_link));
c010477d:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104780:	83 c0 0c             	add    $0xc,%eax
c0104783:	89 45 b4             	mov    %eax,-0x4c(%ebp)
    __list_del(listelm->prev, listelm->next);
c0104786:	8b 45 b4             	mov    -0x4c(%ebp),%eax
c0104789:	8b 40 04             	mov    0x4(%eax),%eax
c010478c:	8b 55 b4             	mov    -0x4c(%ebp),%edx
c010478f:	8b 12                	mov    (%edx),%edx
c0104791:	89 55 b0             	mov    %edx,-0x50(%ebp)
c0104794:	89 45 ac             	mov    %eax,-0x54(%ebp)
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 * */
static inline void
__list_del(list_entry_t *prev, list_entry_t *next) {
    prev->next = next;
c0104797:	8b 45 b0             	mov    -0x50(%ebp),%eax
c010479a:	8b 55 ac             	mov    -0x54(%ebp),%edx
c010479d:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
c01047a0:	8b 45 ac             	mov    -0x54(%ebp),%eax
c01047a3:	8b 55 b0             	mov    -0x50(%ebp),%edx
c01047a6:	89 10                	mov    %edx,(%eax)
}
c01047a8:	90                   	nop
}
c01047a9:	90                   	nop
        //因为分配了n个内存，故nr_free-n即可
        nr_free -= n;
c01047aa:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c01047af:	2b 45 08             	sub    0x8(%ebp),%eax
c01047b2:	a3 24 cf 11 c0       	mov    %eax,0xc011cf24
        ClearPageProperty(page);
c01047b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01047ba:	83 c0 04             	add    $0x4,%eax
c01047bd:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
c01047c4:	89 45 b8             	mov    %eax,-0x48(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c01047c7:	8b 45 b8             	mov    -0x48(%ebp),%eax
c01047ca:	8b 55 bc             	mov    -0x44(%ebp),%edx
c01047cd:	0f b3 10             	btr    %edx,(%eax)
}
c01047d0:	90                   	nop
    }
    //返回分配的内存
    return page;
c01047d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c01047d4:	c9                   	leave  
c01047d5:	c3                   	ret    

c01047d6 <default_free_pages>:

//释放掉n个页块，释放后也要考虑释放的块是否和已有的空闲块是紧挨着的，也就是可以合并的
//如果可以合并，则合并，否则直接加入双向链表

static void default_free_pages(struct Page *base, size_t n) 
{
c01047d6:	f3 0f 1e fb          	endbr32 
c01047da:	55                   	push   %ebp
c01047db:	89 e5                	mov    %esp,%ebp
c01047dd:	81 ec 98 00 00 00    	sub    $0x98,%esp
    assert(n > 0);  //n必须大于0
c01047e3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
c01047e7:	75 24                	jne    c010480d <default_free_pages+0x37>
c01047e9:	c7 44 24 0c d8 6f 10 	movl   $0xc0106fd8,0xc(%esp)
c01047f0:	c0 
c01047f1:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01047f8:	c0 
c01047f9:	c7 44 24 04 b8 00 00 	movl   $0xb8,0x4(%esp)
c0104800:	00 
c0104801:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104808:	e8 19 bc ff ff       	call   c0100426 <__panic>
    struct Page *p = base;  
c010480d:	8b 45 08             	mov    0x8(%ebp),%eax
c0104810:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //首先将base-->base+n之间的内存的标记以及ref初始化
    for (; p != base + n; p ++)
c0104813:	e9 9d 00 00 00       	jmp    c01048b5 <default_free_pages+0xdf>
     {
        assert(!PageReserved(p) && !PageProperty(p));
c0104818:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010481b:	83 c0 04             	add    $0x4,%eax
c010481e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
c0104825:	89 45 e8             	mov    %eax,-0x18(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c0104828:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010482b:	8b 55 ec             	mov    -0x14(%ebp),%edx
c010482e:	0f a3 10             	bt     %edx,(%eax)
c0104831:	19 c0                	sbb    %eax,%eax
c0104833:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return oldbit != 0;
c0104836:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
c010483a:	0f 95 c0             	setne  %al
c010483d:	0f b6 c0             	movzbl %al,%eax
c0104840:	85 c0                	test   %eax,%eax
c0104842:	75 2c                	jne    c0104870 <default_free_pages+0x9a>
c0104844:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104847:	83 c0 04             	add    $0x4,%eax
c010484a:	c7 45 e0 01 00 00 00 	movl   $0x1,-0x20(%ebp)
c0104851:	89 45 dc             	mov    %eax,-0x24(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c0104854:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104857:	8b 55 e0             	mov    -0x20(%ebp),%edx
c010485a:	0f a3 10             	bt     %edx,(%eax)
c010485d:	19 c0                	sbb    %eax,%eax
c010485f:	89 45 d8             	mov    %eax,-0x28(%ebp)
    return oldbit != 0;
c0104862:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
c0104866:	0f 95 c0             	setne  %al
c0104869:	0f b6 c0             	movzbl %al,%eax
c010486c:	85 c0                	test   %eax,%eax
c010486e:	74 24                	je     c0104894 <default_free_pages+0xbe>
c0104870:	c7 44 24 0c 1c 70 10 	movl   $0xc010701c,0xc(%esp)
c0104877:	c0 
c0104878:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010487f:	c0 
c0104880:	c7 44 24 04 bd 00 00 	movl   $0xbd,0x4(%esp)
c0104887:	00 
c0104888:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010488f:	e8 92 bb ff ff       	call   c0100426 <__panic>
        //将flags和ref设为0
        p->flags = 0; 
c0104894:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104897:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
        set_page_ref(p, 0);
c010489e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
c01048a5:	00 
c01048a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01048a9:	89 04 24             	mov    %eax,(%esp)
c01048ac:	e8 f4 fb ff ff       	call   c01044a5 <set_page_ref>
    for (; p != base + n; p ++)
c01048b1:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
c01048b5:	8b 55 0c             	mov    0xc(%ebp),%edx
c01048b8:	89 d0                	mov    %edx,%eax
c01048ba:	c1 e0 02             	shl    $0x2,%eax
c01048bd:	01 d0                	add    %edx,%eax
c01048bf:	c1 e0 02             	shl    $0x2,%eax
c01048c2:	89 c2                	mov    %eax,%edx
c01048c4:	8b 45 08             	mov    0x8(%ebp),%eax
c01048c7:	01 d0                	add    %edx,%eax
c01048c9:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c01048cc:	0f 85 46 ff ff ff    	jne    c0104818 <default_free_pages+0x42>
    }
    //释放完毕后先将这一块的property改为n
    base->property = n;
c01048d2:	8b 45 08             	mov    0x8(%ebp),%eax
c01048d5:	8b 55 0c             	mov    0xc(%ebp),%edx
c01048d8:	89 50 08             	mov    %edx,0x8(%eax)
    SetPageProperty(base);
c01048db:	8b 45 08             	mov    0x8(%ebp),%eax
c01048de:	83 c0 04             	add    $0x4,%eax
c01048e1:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
c01048e8:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c01048eb:	8b 45 cc             	mov    -0x34(%ebp),%eax
c01048ee:	8b 55 d0             	mov    -0x30(%ebp),%edx
c01048f1:	0f ab 10             	bts    %edx,(%eax)
}
c01048f4:	90                   	nop
c01048f5:	c7 45 d4 1c cf 11 c0 	movl   $0xc011cf1c,-0x2c(%ebp)
    return listelm->next;
c01048fc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01048ff:	8b 40 04             	mov    0x4(%eax),%eax
    list_entry_t *le = list_next(&free_list);
c0104902:	89 45 f0             	mov    %eax,-0x10(%ebp)

    // 检查能否将其合并到合适的页块中
    while (le != &free_list)
c0104905:	e9 0e 01 00 00       	jmp    c0104a18 <default_free_pages+0x242>
    {
        p = le2page(le, page_link);
c010490a:	8b 45 f0             	mov    -0x10(%ebp),%eax
c010490d:	83 e8 0c             	sub    $0xc,%eax
c0104910:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0104913:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104916:	89 45 c8             	mov    %eax,-0x38(%ebp)
c0104919:	8b 45 c8             	mov    -0x38(%ebp),%eax
c010491c:	8b 40 04             	mov    0x4(%eax),%eax
        le = list_next(le);
c010491f:	89 45 f0             	mov    %eax,-0x10(%ebp)
        //如果这个块在下一个空闲块前面，二者可以合并
        if (base + base->property == p) 
c0104922:	8b 45 08             	mov    0x8(%ebp),%eax
c0104925:	8b 50 08             	mov    0x8(%eax),%edx
c0104928:	89 d0                	mov    %edx,%eax
c010492a:	c1 e0 02             	shl    $0x2,%eax
c010492d:	01 d0                	add    %edx,%eax
c010492f:	c1 e0 02             	shl    $0x2,%eax
c0104932:	89 c2                	mov    %eax,%edx
c0104934:	8b 45 08             	mov    0x8(%ebp),%eax
c0104937:	01 d0                	add    %edx,%eax
c0104939:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c010493c:	75 5d                	jne    c010499b <default_free_pages+0x1c5>
        {
            //让base的property等于两个块的大小之和
            base->property += p->property;
c010493e:	8b 45 08             	mov    0x8(%ebp),%eax
c0104941:	8b 50 08             	mov    0x8(%eax),%edx
c0104944:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104947:	8b 40 08             	mov    0x8(%eax),%eax
c010494a:	01 c2                	add    %eax,%edx
c010494c:	8b 45 08             	mov    0x8(%ebp),%eax
c010494f:	89 50 08             	mov    %edx,0x8(%eax)
            //将另一个空闲块删除即可
            ClearPageProperty(p);
c0104952:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104955:	83 c0 04             	add    $0x4,%eax
c0104958:	c7 45 b8 01 00 00 00 	movl   $0x1,-0x48(%ebp)
c010495f:	89 45 b4             	mov    %eax,-0x4c(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c0104962:	8b 45 b4             	mov    -0x4c(%ebp),%eax
c0104965:	8b 55 b8             	mov    -0x48(%ebp),%edx
c0104968:	0f b3 10             	btr    %edx,(%eax)
}
c010496b:	90                   	nop
            list_del(&(p->page_link));
c010496c:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010496f:	83 c0 0c             	add    $0xc,%eax
c0104972:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    __list_del(listelm->prev, listelm->next);
c0104975:	8b 45 c4             	mov    -0x3c(%ebp),%eax
c0104978:	8b 40 04             	mov    0x4(%eax),%eax
c010497b:	8b 55 c4             	mov    -0x3c(%ebp),%edx
c010497e:	8b 12                	mov    (%edx),%edx
c0104980:	89 55 c0             	mov    %edx,-0x40(%ebp)
c0104983:	89 45 bc             	mov    %eax,-0x44(%ebp)
    prev->next = next;
c0104986:	8b 45 c0             	mov    -0x40(%ebp),%eax
c0104989:	8b 55 bc             	mov    -0x44(%ebp),%edx
c010498c:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
c010498f:	8b 45 bc             	mov    -0x44(%ebp),%eax
c0104992:	8b 55 c0             	mov    -0x40(%ebp),%edx
c0104995:	89 10                	mov    %edx,(%eax)
}
c0104997:	90                   	nop
}
c0104998:	90                   	nop
c0104999:	eb 7d                	jmp    c0104a18 <default_free_pages+0x242>
        }
        //如果这个块在上一个空闲块的后面，二者可以合并
        else if (p + p->property == base) 
c010499b:	8b 45 f4             	mov    -0xc(%ebp),%eax
c010499e:	8b 50 08             	mov    0x8(%eax),%edx
c01049a1:	89 d0                	mov    %edx,%eax
c01049a3:	c1 e0 02             	shl    $0x2,%eax
c01049a6:	01 d0                	add    %edx,%eax
c01049a8:	c1 e0 02             	shl    $0x2,%eax
c01049ab:	89 c2                	mov    %eax,%edx
c01049ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01049b0:	01 d0                	add    %edx,%eax
c01049b2:	39 45 08             	cmp    %eax,0x8(%ebp)
c01049b5:	75 61                	jne    c0104a18 <default_free_pages+0x242>
        {
            //将p的property设置为二者之和
            p->property += base->property;
c01049b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01049ba:	8b 50 08             	mov    0x8(%eax),%edx
c01049bd:	8b 45 08             	mov    0x8(%ebp),%eax
c01049c0:	8b 40 08             	mov    0x8(%eax),%eax
c01049c3:	01 c2                	add    %eax,%edx
c01049c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01049c8:	89 50 08             	mov    %edx,0x8(%eax)
            //将后面的空闲块删除即可
            ClearPageProperty(base);
c01049cb:	8b 45 08             	mov    0x8(%ebp),%eax
c01049ce:	83 c0 04             	add    $0x4,%eax
c01049d1:	c7 45 a4 01 00 00 00 	movl   $0x1,-0x5c(%ebp)
c01049d8:	89 45 a0             	mov    %eax,-0x60(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
c01049db:	8b 45 a0             	mov    -0x60(%ebp),%eax
c01049de:	8b 55 a4             	mov    -0x5c(%ebp),%edx
c01049e1:	0f b3 10             	btr    %edx,(%eax)
}
c01049e4:	90                   	nop
            base = p;
c01049e5:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01049e8:	89 45 08             	mov    %eax,0x8(%ebp)
            //注意这里需要把p删除，因为之后再次去确认插入的位置
            list_del(&(p->page_link));
c01049eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01049ee:	83 c0 0c             	add    $0xc,%eax
c01049f1:	89 45 b0             	mov    %eax,-0x50(%ebp)
    __list_del(listelm->prev, listelm->next);
c01049f4:	8b 45 b0             	mov    -0x50(%ebp),%eax
c01049f7:	8b 40 04             	mov    0x4(%eax),%eax
c01049fa:	8b 55 b0             	mov    -0x50(%ebp),%edx
c01049fd:	8b 12                	mov    (%edx),%edx
c01049ff:	89 55 ac             	mov    %edx,-0x54(%ebp)
c0104a02:	89 45 a8             	mov    %eax,-0x58(%ebp)
    prev->next = next;
c0104a05:	8b 45 ac             	mov    -0x54(%ebp),%eax
c0104a08:	8b 55 a8             	mov    -0x58(%ebp),%edx
c0104a0b:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
c0104a0e:	8b 45 a8             	mov    -0x58(%ebp),%eax
c0104a11:	8b 55 ac             	mov    -0x54(%ebp),%edx
c0104a14:	89 10                	mov    %edx,(%eax)
}
c0104a16:	90                   	nop
}
c0104a17:	90                   	nop
    while (le != &free_list)
c0104a18:	81 7d f0 1c cf 11 c0 	cmpl   $0xc011cf1c,-0x10(%ebp)
c0104a1f:	0f 85 e5 fe ff ff    	jne    c010490a <default_free_pages+0x134>
        }
    }
    //整体上空闲空间增大了n
    nr_free += n;
c0104a25:	8b 15 24 cf 11 c0    	mov    0xc011cf24,%edx
c0104a2b:	8b 45 0c             	mov    0xc(%ebp),%eax
c0104a2e:	01 d0                	add    %edx,%eax
c0104a30:	a3 24 cf 11 c0       	mov    %eax,0xc011cf24
c0104a35:	c7 45 9c 1c cf 11 c0 	movl   $0xc011cf1c,-0x64(%ebp)
    return listelm->next;
c0104a3c:	8b 45 9c             	mov    -0x64(%ebp),%eax
c0104a3f:	8b 40 04             	mov    0x4(%eax),%eax
    le = list_next(&free_list);
c0104a42:	89 45 f0             	mov    %eax,-0x10(%ebp)
    // 将合并好的合适的页块添加回空闲页块链表
    //因为需要按照内存从小到大的顺序排列列表，故需要找到应该插入的位置
    while (le != &free_list) 
c0104a45:	eb 34                	jmp    c0104a7b <default_free_pages+0x2a5>
    {
        p = le2page(le, page_link);
c0104a47:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104a4a:	83 e8 0c             	sub    $0xc,%eax
c0104a4d:	89 45 f4             	mov    %eax,-0xc(%ebp)
        //找到正确的位置：
        if (base + base->property <= p)
c0104a50:	8b 45 08             	mov    0x8(%ebp),%eax
c0104a53:	8b 50 08             	mov    0x8(%eax),%edx
c0104a56:	89 d0                	mov    %edx,%eax
c0104a58:	c1 e0 02             	shl    $0x2,%eax
c0104a5b:	01 d0                	add    %edx,%eax
c0104a5d:	c1 e0 02             	shl    $0x2,%eax
c0104a60:	89 c2                	mov    %eax,%edx
c0104a62:	8b 45 08             	mov    0x8(%ebp),%eax
c0104a65:	01 d0                	add    %edx,%eax
c0104a67:	39 45 f4             	cmp    %eax,-0xc(%ebp)
c0104a6a:	73 1a                	jae    c0104a86 <default_free_pages+0x2b0>
c0104a6c:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104a6f:	89 45 98             	mov    %eax,-0x68(%ebp)
c0104a72:	8b 45 98             	mov    -0x68(%ebp),%eax
c0104a75:	8b 40 04             	mov    0x4(%eax),%eax
        {
            break;
        }
        //否则链表项向后，继续查找
        le = list_next(le);
c0104a78:	89 45 f0             	mov    %eax,-0x10(%ebp)
    while (le != &free_list) 
c0104a7b:	81 7d f0 1c cf 11 c0 	cmpl   $0xc011cf1c,-0x10(%ebp)
c0104a82:	75 c3                	jne    c0104a47 <default_free_pages+0x271>
c0104a84:	eb 01                	jmp    c0104a87 <default_free_pages+0x2b1>
            break;
c0104a86:	90                   	nop
    }
    //将base插入到刚才找到的正确位置即可
    list_add_before(le, &(base->page_link));
c0104a87:	8b 45 08             	mov    0x8(%ebp),%eax
c0104a8a:	8d 50 0c             	lea    0xc(%eax),%edx
c0104a8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104a90:	89 45 94             	mov    %eax,-0x6c(%ebp)
c0104a93:	89 55 90             	mov    %edx,-0x70(%ebp)
    __list_add(elm, listelm->prev, listelm);
c0104a96:	8b 45 94             	mov    -0x6c(%ebp),%eax
c0104a99:	8b 00                	mov    (%eax),%eax
c0104a9b:	8b 55 90             	mov    -0x70(%ebp),%edx
c0104a9e:	89 55 8c             	mov    %edx,-0x74(%ebp)
c0104aa1:	89 45 88             	mov    %eax,-0x78(%ebp)
c0104aa4:	8b 45 94             	mov    -0x6c(%ebp),%eax
c0104aa7:	89 45 84             	mov    %eax,-0x7c(%ebp)
    prev->next = next->prev = elm;
c0104aaa:	8b 45 84             	mov    -0x7c(%ebp),%eax
c0104aad:	8b 55 8c             	mov    -0x74(%ebp),%edx
c0104ab0:	89 10                	mov    %edx,(%eax)
c0104ab2:	8b 45 84             	mov    -0x7c(%ebp),%eax
c0104ab5:	8b 10                	mov    (%eax),%edx
c0104ab7:	8b 45 88             	mov    -0x78(%ebp),%eax
c0104aba:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
c0104abd:	8b 45 8c             	mov    -0x74(%ebp),%eax
c0104ac0:	8b 55 84             	mov    -0x7c(%ebp),%edx
c0104ac3:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
c0104ac6:	8b 45 8c             	mov    -0x74(%ebp),%eax
c0104ac9:	8b 55 88             	mov    -0x78(%ebp),%edx
c0104acc:	89 10                	mov    %edx,(%eax)
}
c0104ace:	90                   	nop
}
c0104acf:	90                   	nop
}
c0104ad0:	90                   	nop
c0104ad1:	c9                   	leave  
c0104ad2:	c3                   	ret    

c0104ad3 <default_nr_free_pages>:


static size_t
default_nr_free_pages(void) {
c0104ad3:	f3 0f 1e fb          	endbr32 
c0104ad7:	55                   	push   %ebp
c0104ad8:	89 e5                	mov    %esp,%ebp
    return nr_free;
c0104ada:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
}
c0104adf:	5d                   	pop    %ebp
c0104ae0:	c3                   	ret    

c0104ae1 <basic_check>:

static void
basic_check(void) {
c0104ae1:	f3 0f 1e fb          	endbr32 
c0104ae5:	55                   	push   %ebp
c0104ae6:	89 e5                	mov    %esp,%ebp
c0104ae8:	83 ec 48             	sub    $0x48,%esp
    struct Page *p0, *p1, *p2;
    p0 = p1 = p2 = NULL;
c0104aeb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0104af2:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104af5:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0104af8:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104afb:	89 45 ec             	mov    %eax,-0x14(%ebp)
    assert((p0 = alloc_page()) != NULL);
c0104afe:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104b05:	e8 2e e2 ff ff       	call   c0102d38 <alloc_pages>
c0104b0a:	89 45 ec             	mov    %eax,-0x14(%ebp)
c0104b0d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
c0104b11:	75 24                	jne    c0104b37 <basic_check+0x56>
c0104b13:	c7 44 24 0c 41 70 10 	movl   $0xc0107041,0xc(%esp)
c0104b1a:	c0 
c0104b1b:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104b22:	c0 
c0104b23:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
c0104b2a:	00 
c0104b2b:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104b32:	e8 ef b8 ff ff       	call   c0100426 <__panic>
    assert((p1 = alloc_page()) != NULL);
c0104b37:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104b3e:	e8 f5 e1 ff ff       	call   c0102d38 <alloc_pages>
c0104b43:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0104b46:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0104b4a:	75 24                	jne    c0104b70 <basic_check+0x8f>
c0104b4c:	c7 44 24 0c 5d 70 10 	movl   $0xc010705d,0xc(%esp)
c0104b53:	c0 
c0104b54:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104b5b:	c0 
c0104b5c:	c7 44 24 04 00 01 00 	movl   $0x100,0x4(%esp)
c0104b63:	00 
c0104b64:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104b6b:	e8 b6 b8 ff ff       	call   c0100426 <__panic>
    assert((p2 = alloc_page()) != NULL);
c0104b70:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104b77:	e8 bc e1 ff ff       	call   c0102d38 <alloc_pages>
c0104b7c:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0104b7f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0104b83:	75 24                	jne    c0104ba9 <basic_check+0xc8>
c0104b85:	c7 44 24 0c 79 70 10 	movl   $0xc0107079,0xc(%esp)
c0104b8c:	c0 
c0104b8d:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104b94:	c0 
c0104b95:	c7 44 24 04 01 01 00 	movl   $0x101,0x4(%esp)
c0104b9c:	00 
c0104b9d:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104ba4:	e8 7d b8 ff ff       	call   c0100426 <__panic>

    assert(p0 != p1 && p0 != p2 && p1 != p2);
c0104ba9:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104bac:	3b 45 f0             	cmp    -0x10(%ebp),%eax
c0104baf:	74 10                	je     c0104bc1 <basic_check+0xe0>
c0104bb1:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104bb4:	3b 45 f4             	cmp    -0xc(%ebp),%eax
c0104bb7:	74 08                	je     c0104bc1 <basic_check+0xe0>
c0104bb9:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104bbc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
c0104bbf:	75 24                	jne    c0104be5 <basic_check+0x104>
c0104bc1:	c7 44 24 0c 98 70 10 	movl   $0xc0107098,0xc(%esp)
c0104bc8:	c0 
c0104bc9:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104bd0:	c0 
c0104bd1:	c7 44 24 04 03 01 00 	movl   $0x103,0x4(%esp)
c0104bd8:	00 
c0104bd9:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104be0:	e8 41 b8 ff ff       	call   c0100426 <__panic>
    assert(page_ref(p0) == 0 && page_ref(p1) == 0 && page_ref(p2) == 0);
c0104be5:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104be8:	89 04 24             	mov    %eax,(%esp)
c0104beb:	e8 ab f8 ff ff       	call   c010449b <page_ref>
c0104bf0:	85 c0                	test   %eax,%eax
c0104bf2:	75 1e                	jne    c0104c12 <basic_check+0x131>
c0104bf4:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104bf7:	89 04 24             	mov    %eax,(%esp)
c0104bfa:	e8 9c f8 ff ff       	call   c010449b <page_ref>
c0104bff:	85 c0                	test   %eax,%eax
c0104c01:	75 0f                	jne    c0104c12 <basic_check+0x131>
c0104c03:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104c06:	89 04 24             	mov    %eax,(%esp)
c0104c09:	e8 8d f8 ff ff       	call   c010449b <page_ref>
c0104c0e:	85 c0                	test   %eax,%eax
c0104c10:	74 24                	je     c0104c36 <basic_check+0x155>
c0104c12:	c7 44 24 0c bc 70 10 	movl   $0xc01070bc,0xc(%esp)
c0104c19:	c0 
c0104c1a:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104c21:	c0 
c0104c22:	c7 44 24 04 04 01 00 	movl   $0x104,0x4(%esp)
c0104c29:	00 
c0104c2a:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104c31:	e8 f0 b7 ff ff       	call   c0100426 <__panic>

    assert(page2pa(p0) < npage * PGSIZE);
c0104c36:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104c39:	89 04 24             	mov    %eax,(%esp)
c0104c3c:	e8 44 f8 ff ff       	call   c0104485 <page2pa>
c0104c41:	8b 15 80 ce 11 c0    	mov    0xc011ce80,%edx
c0104c47:	c1 e2 0c             	shl    $0xc,%edx
c0104c4a:	39 d0                	cmp    %edx,%eax
c0104c4c:	72 24                	jb     c0104c72 <basic_check+0x191>
c0104c4e:	c7 44 24 0c f8 70 10 	movl   $0xc01070f8,0xc(%esp)
c0104c55:	c0 
c0104c56:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104c5d:	c0 
c0104c5e:	c7 44 24 04 06 01 00 	movl   $0x106,0x4(%esp)
c0104c65:	00 
c0104c66:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104c6d:	e8 b4 b7 ff ff       	call   c0100426 <__panic>
    assert(page2pa(p1) < npage * PGSIZE);
c0104c72:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104c75:	89 04 24             	mov    %eax,(%esp)
c0104c78:	e8 08 f8 ff ff       	call   c0104485 <page2pa>
c0104c7d:	8b 15 80 ce 11 c0    	mov    0xc011ce80,%edx
c0104c83:	c1 e2 0c             	shl    $0xc,%edx
c0104c86:	39 d0                	cmp    %edx,%eax
c0104c88:	72 24                	jb     c0104cae <basic_check+0x1cd>
c0104c8a:	c7 44 24 0c 15 71 10 	movl   $0xc0107115,0xc(%esp)
c0104c91:	c0 
c0104c92:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104c99:	c0 
c0104c9a:	c7 44 24 04 07 01 00 	movl   $0x107,0x4(%esp)
c0104ca1:	00 
c0104ca2:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104ca9:	e8 78 b7 ff ff       	call   c0100426 <__panic>
    assert(page2pa(p2) < npage * PGSIZE);
c0104cae:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104cb1:	89 04 24             	mov    %eax,(%esp)
c0104cb4:	e8 cc f7 ff ff       	call   c0104485 <page2pa>
c0104cb9:	8b 15 80 ce 11 c0    	mov    0xc011ce80,%edx
c0104cbf:	c1 e2 0c             	shl    $0xc,%edx
c0104cc2:	39 d0                	cmp    %edx,%eax
c0104cc4:	72 24                	jb     c0104cea <basic_check+0x209>
c0104cc6:	c7 44 24 0c 32 71 10 	movl   $0xc0107132,0xc(%esp)
c0104ccd:	c0 
c0104cce:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104cd5:	c0 
c0104cd6:	c7 44 24 04 08 01 00 	movl   $0x108,0x4(%esp)
c0104cdd:	00 
c0104cde:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104ce5:	e8 3c b7 ff ff       	call   c0100426 <__panic>

    list_entry_t free_list_store = free_list;
c0104cea:	a1 1c cf 11 c0       	mov    0xc011cf1c,%eax
c0104cef:	8b 15 20 cf 11 c0    	mov    0xc011cf20,%edx
c0104cf5:	89 45 d0             	mov    %eax,-0x30(%ebp)
c0104cf8:	89 55 d4             	mov    %edx,-0x2c(%ebp)
c0104cfb:	c7 45 dc 1c cf 11 c0 	movl   $0xc011cf1c,-0x24(%ebp)
    elm->prev = elm->next = elm;
c0104d02:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104d05:	8b 55 dc             	mov    -0x24(%ebp),%edx
c0104d08:	89 50 04             	mov    %edx,0x4(%eax)
c0104d0b:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104d0e:	8b 50 04             	mov    0x4(%eax),%edx
c0104d11:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0104d14:	89 10                	mov    %edx,(%eax)
}
c0104d16:	90                   	nop
c0104d17:	c7 45 e0 1c cf 11 c0 	movl   $0xc011cf1c,-0x20(%ebp)
    return list->next == list;
c0104d1e:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0104d21:	8b 40 04             	mov    0x4(%eax),%eax
c0104d24:	39 45 e0             	cmp    %eax,-0x20(%ebp)
c0104d27:	0f 94 c0             	sete   %al
c0104d2a:	0f b6 c0             	movzbl %al,%eax
    list_init(&free_list);
    assert(list_empty(&free_list));
c0104d2d:	85 c0                	test   %eax,%eax
c0104d2f:	75 24                	jne    c0104d55 <basic_check+0x274>
c0104d31:	c7 44 24 0c 4f 71 10 	movl   $0xc010714f,0xc(%esp)
c0104d38:	c0 
c0104d39:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104d40:	c0 
c0104d41:	c7 44 24 04 0c 01 00 	movl   $0x10c,0x4(%esp)
c0104d48:	00 
c0104d49:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104d50:	e8 d1 b6 ff ff       	call   c0100426 <__panic>

    unsigned int nr_free_store = nr_free;
c0104d55:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c0104d5a:	89 45 e8             	mov    %eax,-0x18(%ebp)
    nr_free = 0;
c0104d5d:	c7 05 24 cf 11 c0 00 	movl   $0x0,0xc011cf24
c0104d64:	00 00 00 

    assert(alloc_page() == NULL);
c0104d67:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104d6e:	e8 c5 df ff ff       	call   c0102d38 <alloc_pages>
c0104d73:	85 c0                	test   %eax,%eax
c0104d75:	74 24                	je     c0104d9b <basic_check+0x2ba>
c0104d77:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c0104d7e:	c0 
c0104d7f:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104d86:	c0 
c0104d87:	c7 44 24 04 11 01 00 	movl   $0x111,0x4(%esp)
c0104d8e:	00 
c0104d8f:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104d96:	e8 8b b6 ff ff       	call   c0100426 <__panic>

    free_page(p0);
c0104d9b:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0104da2:	00 
c0104da3:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104da6:	89 04 24             	mov    %eax,(%esp)
c0104da9:	e8 c6 df ff ff       	call   c0102d74 <free_pages>
    free_page(p1);
c0104dae:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0104db5:	00 
c0104db6:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0104db9:	89 04 24             	mov    %eax,(%esp)
c0104dbc:	e8 b3 df ff ff       	call   c0102d74 <free_pages>
    free_page(p2);
c0104dc1:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0104dc8:	00 
c0104dc9:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0104dcc:	89 04 24             	mov    %eax,(%esp)
c0104dcf:	e8 a0 df ff ff       	call   c0102d74 <free_pages>
    assert(nr_free == 3);
c0104dd4:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c0104dd9:	83 f8 03             	cmp    $0x3,%eax
c0104ddc:	74 24                	je     c0104e02 <basic_check+0x321>
c0104dde:	c7 44 24 0c 7b 71 10 	movl   $0xc010717b,0xc(%esp)
c0104de5:	c0 
c0104de6:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104ded:	c0 
c0104dee:	c7 44 24 04 16 01 00 	movl   $0x116,0x4(%esp)
c0104df5:	00 
c0104df6:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104dfd:	e8 24 b6 ff ff       	call   c0100426 <__panic>

    assert((p0 = alloc_page()) != NULL);
c0104e02:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104e09:	e8 2a df ff ff       	call   c0102d38 <alloc_pages>
c0104e0e:	89 45 ec             	mov    %eax,-0x14(%ebp)
c0104e11:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
c0104e15:	75 24                	jne    c0104e3b <basic_check+0x35a>
c0104e17:	c7 44 24 0c 41 70 10 	movl   $0xc0107041,0xc(%esp)
c0104e1e:	c0 
c0104e1f:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104e26:	c0 
c0104e27:	c7 44 24 04 18 01 00 	movl   $0x118,0x4(%esp)
c0104e2e:	00 
c0104e2f:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104e36:	e8 eb b5 ff ff       	call   c0100426 <__panic>
    assert((p1 = alloc_page()) != NULL);
c0104e3b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104e42:	e8 f1 de ff ff       	call   c0102d38 <alloc_pages>
c0104e47:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0104e4a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0104e4e:	75 24                	jne    c0104e74 <basic_check+0x393>
c0104e50:	c7 44 24 0c 5d 70 10 	movl   $0xc010705d,0xc(%esp)
c0104e57:	c0 
c0104e58:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104e5f:	c0 
c0104e60:	c7 44 24 04 19 01 00 	movl   $0x119,0x4(%esp)
c0104e67:	00 
c0104e68:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104e6f:	e8 b2 b5 ff ff       	call   c0100426 <__panic>
    assert((p2 = alloc_page()) != NULL);
c0104e74:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104e7b:	e8 b8 de ff ff       	call   c0102d38 <alloc_pages>
c0104e80:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0104e83:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0104e87:	75 24                	jne    c0104ead <basic_check+0x3cc>
c0104e89:	c7 44 24 0c 79 70 10 	movl   $0xc0107079,0xc(%esp)
c0104e90:	c0 
c0104e91:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104e98:	c0 
c0104e99:	c7 44 24 04 1a 01 00 	movl   $0x11a,0x4(%esp)
c0104ea0:	00 
c0104ea1:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104ea8:	e8 79 b5 ff ff       	call   c0100426 <__panic>

    assert(alloc_page() == NULL);
c0104ead:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104eb4:	e8 7f de ff ff       	call   c0102d38 <alloc_pages>
c0104eb9:	85 c0                	test   %eax,%eax
c0104ebb:	74 24                	je     c0104ee1 <basic_check+0x400>
c0104ebd:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c0104ec4:	c0 
c0104ec5:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104ecc:	c0 
c0104ecd:	c7 44 24 04 1c 01 00 	movl   $0x11c,0x4(%esp)
c0104ed4:	00 
c0104ed5:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104edc:	e8 45 b5 ff ff       	call   c0100426 <__panic>

    free_page(p0);
c0104ee1:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0104ee8:	00 
c0104ee9:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0104eec:	89 04 24             	mov    %eax,(%esp)
c0104eef:	e8 80 de ff ff       	call   c0102d74 <free_pages>
c0104ef4:	c7 45 d8 1c cf 11 c0 	movl   $0xc011cf1c,-0x28(%ebp)
c0104efb:	8b 45 d8             	mov    -0x28(%ebp),%eax
c0104efe:	8b 40 04             	mov    0x4(%eax),%eax
c0104f01:	39 45 d8             	cmp    %eax,-0x28(%ebp)
c0104f04:	0f 94 c0             	sete   %al
c0104f07:	0f b6 c0             	movzbl %al,%eax
    assert(!list_empty(&free_list));
c0104f0a:	85 c0                	test   %eax,%eax
c0104f0c:	74 24                	je     c0104f32 <basic_check+0x451>
c0104f0e:	c7 44 24 0c 88 71 10 	movl   $0xc0107188,0xc(%esp)
c0104f15:	c0 
c0104f16:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104f1d:	c0 
c0104f1e:	c7 44 24 04 1f 01 00 	movl   $0x11f,0x4(%esp)
c0104f25:	00 
c0104f26:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104f2d:	e8 f4 b4 ff ff       	call   c0100426 <__panic>

    struct Page *p;
    assert((p = alloc_page()) == p0);
c0104f32:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104f39:	e8 fa dd ff ff       	call   c0102d38 <alloc_pages>
c0104f3e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c0104f41:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0104f44:	3b 45 ec             	cmp    -0x14(%ebp),%eax
c0104f47:	74 24                	je     c0104f6d <basic_check+0x48c>
c0104f49:	c7 44 24 0c a0 71 10 	movl   $0xc01071a0,0xc(%esp)
c0104f50:	c0 
c0104f51:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104f58:	c0 
c0104f59:	c7 44 24 04 22 01 00 	movl   $0x122,0x4(%esp)
c0104f60:	00 
c0104f61:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104f68:	e8 b9 b4 ff ff       	call   c0100426 <__panic>
    assert(alloc_page() == NULL);
c0104f6d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0104f74:	e8 bf dd ff ff       	call   c0102d38 <alloc_pages>
c0104f79:	85 c0                	test   %eax,%eax
c0104f7b:	74 24                	je     c0104fa1 <basic_check+0x4c0>
c0104f7d:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c0104f84:	c0 
c0104f85:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104f8c:	c0 
c0104f8d:	c7 44 24 04 23 01 00 	movl   $0x123,0x4(%esp)
c0104f94:	00 
c0104f95:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104f9c:	e8 85 b4 ff ff       	call   c0100426 <__panic>

    assert(nr_free == 0);
c0104fa1:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c0104fa6:	85 c0                	test   %eax,%eax
c0104fa8:	74 24                	je     c0104fce <basic_check+0x4ed>
c0104faa:	c7 44 24 0c b9 71 10 	movl   $0xc01071b9,0xc(%esp)
c0104fb1:	c0 
c0104fb2:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0104fb9:	c0 
c0104fba:	c7 44 24 04 25 01 00 	movl   $0x125,0x4(%esp)
c0104fc1:	00 
c0104fc2:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0104fc9:	e8 58 b4 ff ff       	call   c0100426 <__panic>
    free_list = free_list_store;
c0104fce:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0104fd1:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c0104fd4:	a3 1c cf 11 c0       	mov    %eax,0xc011cf1c
c0104fd9:	89 15 20 cf 11 c0    	mov    %edx,0xc011cf20
    nr_free = nr_free_store;
c0104fdf:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0104fe2:	a3 24 cf 11 c0       	mov    %eax,0xc011cf24

    free_page(p);
c0104fe7:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0104fee:	00 
c0104fef:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0104ff2:	89 04 24             	mov    %eax,(%esp)
c0104ff5:	e8 7a dd ff ff       	call   c0102d74 <free_pages>
    free_page(p1);
c0104ffa:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0105001:	00 
c0105002:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105005:	89 04 24             	mov    %eax,(%esp)
c0105008:	e8 67 dd ff ff       	call   c0102d74 <free_pages>
    free_page(p2);
c010500d:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0105014:	00 
c0105015:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0105018:	89 04 24             	mov    %eax,(%esp)
c010501b:	e8 54 dd ff ff       	call   c0102d74 <free_pages>
}
c0105020:	90                   	nop
c0105021:	c9                   	leave  
c0105022:	c3                   	ret    

c0105023 <default_check>:

// LAB2: below code is used to check the first fit allocation algorithm (your EXERCISE 1) 
// NOTICE: You SHOULD NOT CHANGE basic_check, default_check functions!
static void
default_check(void) {
c0105023:	f3 0f 1e fb          	endbr32 
c0105027:	55                   	push   %ebp
c0105028:	89 e5                	mov    %esp,%ebp
c010502a:	81 ec 98 00 00 00    	sub    $0x98,%esp
    int count = 0, total = 0;
c0105030:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
c0105037:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    list_entry_t *le = &free_list;
c010503e:	c7 45 ec 1c cf 11 c0 	movl   $0xc011cf1c,-0x14(%ebp)
    while ((le = list_next(le)) != &free_list) {
c0105045:	eb 6a                	jmp    c01050b1 <default_check+0x8e>
        struct Page *p = le2page(le, page_link);
c0105047:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010504a:	83 e8 0c             	sub    $0xc,%eax
c010504d:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        assert(PageProperty(p));
c0105050:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c0105053:	83 c0 04             	add    $0x4,%eax
c0105056:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
c010505d:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c0105060:	8b 45 cc             	mov    -0x34(%ebp),%eax
c0105063:	8b 55 d0             	mov    -0x30(%ebp),%edx
c0105066:	0f a3 10             	bt     %edx,(%eax)
c0105069:	19 c0                	sbb    %eax,%eax
c010506b:	89 45 c8             	mov    %eax,-0x38(%ebp)
    return oldbit != 0;
c010506e:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
c0105072:	0f 95 c0             	setne  %al
c0105075:	0f b6 c0             	movzbl %al,%eax
c0105078:	85 c0                	test   %eax,%eax
c010507a:	75 24                	jne    c01050a0 <default_check+0x7d>
c010507c:	c7 44 24 0c c6 71 10 	movl   $0xc01071c6,0xc(%esp)
c0105083:	c0 
c0105084:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010508b:	c0 
c010508c:	c7 44 24 04 36 01 00 	movl   $0x136,0x4(%esp)
c0105093:	00 
c0105094:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010509b:	e8 86 b3 ff ff       	call   c0100426 <__panic>
        count ++, total += p->property;
c01050a0:	ff 45 f4             	incl   -0xc(%ebp)
c01050a3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
c01050a6:	8b 50 08             	mov    0x8(%eax),%edx
c01050a9:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01050ac:	01 d0                	add    %edx,%eax
c01050ae:	89 45 f0             	mov    %eax,-0x10(%ebp)
c01050b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01050b4:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    return listelm->next;
c01050b7:	8b 45 c4             	mov    -0x3c(%ebp),%eax
c01050ba:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) {
c01050bd:	89 45 ec             	mov    %eax,-0x14(%ebp)
c01050c0:	81 7d ec 1c cf 11 c0 	cmpl   $0xc011cf1c,-0x14(%ebp)
c01050c7:	0f 85 7a ff ff ff    	jne    c0105047 <default_check+0x24>
    }
    assert(total == nr_free_pages());
c01050cd:	e8 d9 dc ff ff       	call   c0102dab <nr_free_pages>
c01050d2:	8b 55 f0             	mov    -0x10(%ebp),%edx
c01050d5:	39 d0                	cmp    %edx,%eax
c01050d7:	74 24                	je     c01050fd <default_check+0xda>
c01050d9:	c7 44 24 0c d6 71 10 	movl   $0xc01071d6,0xc(%esp)
c01050e0:	c0 
c01050e1:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01050e8:	c0 
c01050e9:	c7 44 24 04 39 01 00 	movl   $0x139,0x4(%esp)
c01050f0:	00 
c01050f1:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01050f8:	e8 29 b3 ff ff       	call   c0100426 <__panic>

    basic_check();
c01050fd:	e8 df f9 ff ff       	call   c0104ae1 <basic_check>

    struct Page *p0 = alloc_pages(5), *p1, *p2;
c0105102:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
c0105109:	e8 2a dc ff ff       	call   c0102d38 <alloc_pages>
c010510e:	89 45 e8             	mov    %eax,-0x18(%ebp)
    assert(p0 != NULL);
c0105111:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105115:	75 24                	jne    c010513b <default_check+0x118>
c0105117:	c7 44 24 0c ef 71 10 	movl   $0xc01071ef,0xc(%esp)
c010511e:	c0 
c010511f:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105126:	c0 
c0105127:	c7 44 24 04 3e 01 00 	movl   $0x13e,0x4(%esp)
c010512e:	00 
c010512f:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105136:	e8 eb b2 ff ff       	call   c0100426 <__panic>
    assert(!PageProperty(p0));
c010513b:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010513e:	83 c0 04             	add    $0x4,%eax
c0105141:	c7 45 c0 01 00 00 00 	movl   $0x1,-0x40(%ebp)
c0105148:	89 45 bc             	mov    %eax,-0x44(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c010514b:	8b 45 bc             	mov    -0x44(%ebp),%eax
c010514e:	8b 55 c0             	mov    -0x40(%ebp),%edx
c0105151:	0f a3 10             	bt     %edx,(%eax)
c0105154:	19 c0                	sbb    %eax,%eax
c0105156:	89 45 b8             	mov    %eax,-0x48(%ebp)
    return oldbit != 0;
c0105159:	83 7d b8 00          	cmpl   $0x0,-0x48(%ebp)
c010515d:	0f 95 c0             	setne  %al
c0105160:	0f b6 c0             	movzbl %al,%eax
c0105163:	85 c0                	test   %eax,%eax
c0105165:	74 24                	je     c010518b <default_check+0x168>
c0105167:	c7 44 24 0c fa 71 10 	movl   $0xc01071fa,0xc(%esp)
c010516e:	c0 
c010516f:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105176:	c0 
c0105177:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
c010517e:	00 
c010517f:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105186:	e8 9b b2 ff ff       	call   c0100426 <__panic>

    list_entry_t free_list_store = free_list;
c010518b:	a1 1c cf 11 c0       	mov    0xc011cf1c,%eax
c0105190:	8b 15 20 cf 11 c0    	mov    0xc011cf20,%edx
c0105196:	89 45 80             	mov    %eax,-0x80(%ebp)
c0105199:	89 55 84             	mov    %edx,-0x7c(%ebp)
c010519c:	c7 45 b0 1c cf 11 c0 	movl   $0xc011cf1c,-0x50(%ebp)
    elm->prev = elm->next = elm;
c01051a3:	8b 45 b0             	mov    -0x50(%ebp),%eax
c01051a6:	8b 55 b0             	mov    -0x50(%ebp),%edx
c01051a9:	89 50 04             	mov    %edx,0x4(%eax)
c01051ac:	8b 45 b0             	mov    -0x50(%ebp),%eax
c01051af:	8b 50 04             	mov    0x4(%eax),%edx
c01051b2:	8b 45 b0             	mov    -0x50(%ebp),%eax
c01051b5:	89 10                	mov    %edx,(%eax)
}
c01051b7:	90                   	nop
c01051b8:	c7 45 b4 1c cf 11 c0 	movl   $0xc011cf1c,-0x4c(%ebp)
    return list->next == list;
c01051bf:	8b 45 b4             	mov    -0x4c(%ebp),%eax
c01051c2:	8b 40 04             	mov    0x4(%eax),%eax
c01051c5:	39 45 b4             	cmp    %eax,-0x4c(%ebp)
c01051c8:	0f 94 c0             	sete   %al
c01051cb:	0f b6 c0             	movzbl %al,%eax
    list_init(&free_list);
    assert(list_empty(&free_list));
c01051ce:	85 c0                	test   %eax,%eax
c01051d0:	75 24                	jne    c01051f6 <default_check+0x1d3>
c01051d2:	c7 44 24 0c 4f 71 10 	movl   $0xc010714f,0xc(%esp)
c01051d9:	c0 
c01051da:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01051e1:	c0 
c01051e2:	c7 44 24 04 43 01 00 	movl   $0x143,0x4(%esp)
c01051e9:	00 
c01051ea:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01051f1:	e8 30 b2 ff ff       	call   c0100426 <__panic>
    assert(alloc_page() == NULL);
c01051f6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c01051fd:	e8 36 db ff ff       	call   c0102d38 <alloc_pages>
c0105202:	85 c0                	test   %eax,%eax
c0105204:	74 24                	je     c010522a <default_check+0x207>
c0105206:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c010520d:	c0 
c010520e:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105215:	c0 
c0105216:	c7 44 24 04 44 01 00 	movl   $0x144,0x4(%esp)
c010521d:	00 
c010521e:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105225:	e8 fc b1 ff ff       	call   c0100426 <__panic>

    unsigned int nr_free_store = nr_free;
c010522a:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c010522f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    nr_free = 0;
c0105232:	c7 05 24 cf 11 c0 00 	movl   $0x0,0xc011cf24
c0105239:	00 00 00 

    free_pages(p0 + 2, 3);
c010523c:	8b 45 e8             	mov    -0x18(%ebp),%eax
c010523f:	83 c0 28             	add    $0x28,%eax
c0105242:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
c0105249:	00 
c010524a:	89 04 24             	mov    %eax,(%esp)
c010524d:	e8 22 db ff ff       	call   c0102d74 <free_pages>
    assert(alloc_pages(4) == NULL);
c0105252:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
c0105259:	e8 da da ff ff       	call   c0102d38 <alloc_pages>
c010525e:	85 c0                	test   %eax,%eax
c0105260:	74 24                	je     c0105286 <default_check+0x263>
c0105262:	c7 44 24 0c 0c 72 10 	movl   $0xc010720c,0xc(%esp)
c0105269:	c0 
c010526a:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105271:	c0 
c0105272:	c7 44 24 04 4a 01 00 	movl   $0x14a,0x4(%esp)
c0105279:	00 
c010527a:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105281:	e8 a0 b1 ff ff       	call   c0100426 <__panic>
    assert(PageProperty(p0 + 2) && p0[2].property == 3);
c0105286:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105289:	83 c0 28             	add    $0x28,%eax
c010528c:	83 c0 04             	add    $0x4,%eax
c010528f:	c7 45 ac 01 00 00 00 	movl   $0x1,-0x54(%ebp)
c0105296:	89 45 a8             	mov    %eax,-0x58(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c0105299:	8b 45 a8             	mov    -0x58(%ebp),%eax
c010529c:	8b 55 ac             	mov    -0x54(%ebp),%edx
c010529f:	0f a3 10             	bt     %edx,(%eax)
c01052a2:	19 c0                	sbb    %eax,%eax
c01052a4:	89 45 a4             	mov    %eax,-0x5c(%ebp)
    return oldbit != 0;
c01052a7:	83 7d a4 00          	cmpl   $0x0,-0x5c(%ebp)
c01052ab:	0f 95 c0             	setne  %al
c01052ae:	0f b6 c0             	movzbl %al,%eax
c01052b1:	85 c0                	test   %eax,%eax
c01052b3:	74 0e                	je     c01052c3 <default_check+0x2a0>
c01052b5:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01052b8:	83 c0 28             	add    $0x28,%eax
c01052bb:	8b 40 08             	mov    0x8(%eax),%eax
c01052be:	83 f8 03             	cmp    $0x3,%eax
c01052c1:	74 24                	je     c01052e7 <default_check+0x2c4>
c01052c3:	c7 44 24 0c 24 72 10 	movl   $0xc0107224,0xc(%esp)
c01052ca:	c0 
c01052cb:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01052d2:	c0 
c01052d3:	c7 44 24 04 4b 01 00 	movl   $0x14b,0x4(%esp)
c01052da:	00 
c01052db:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01052e2:	e8 3f b1 ff ff       	call   c0100426 <__panic>
    assert((p1 = alloc_pages(3)) != NULL);
c01052e7:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
c01052ee:	e8 45 da ff ff       	call   c0102d38 <alloc_pages>
c01052f3:	89 45 e0             	mov    %eax,-0x20(%ebp)
c01052f6:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
c01052fa:	75 24                	jne    c0105320 <default_check+0x2fd>
c01052fc:	c7 44 24 0c 50 72 10 	movl   $0xc0107250,0xc(%esp)
c0105303:	c0 
c0105304:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010530b:	c0 
c010530c:	c7 44 24 04 4c 01 00 	movl   $0x14c,0x4(%esp)
c0105313:	00 
c0105314:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010531b:	e8 06 b1 ff ff       	call   c0100426 <__panic>
    assert(alloc_page() == NULL);
c0105320:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c0105327:	e8 0c da ff ff       	call   c0102d38 <alloc_pages>
c010532c:	85 c0                	test   %eax,%eax
c010532e:	74 24                	je     c0105354 <default_check+0x331>
c0105330:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c0105337:	c0 
c0105338:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010533f:	c0 
c0105340:	c7 44 24 04 4d 01 00 	movl   $0x14d,0x4(%esp)
c0105347:	00 
c0105348:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010534f:	e8 d2 b0 ff ff       	call   c0100426 <__panic>
    assert(p0 + 2 == p1);
c0105354:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105357:	83 c0 28             	add    $0x28,%eax
c010535a:	39 45 e0             	cmp    %eax,-0x20(%ebp)
c010535d:	74 24                	je     c0105383 <default_check+0x360>
c010535f:	c7 44 24 0c 6e 72 10 	movl   $0xc010726e,0xc(%esp)
c0105366:	c0 
c0105367:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010536e:	c0 
c010536f:	c7 44 24 04 4e 01 00 	movl   $0x14e,0x4(%esp)
c0105376:	00 
c0105377:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c010537e:	e8 a3 b0 ff ff       	call   c0100426 <__panic>

    p2 = p0 + 1;
c0105383:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105386:	83 c0 14             	add    $0x14,%eax
c0105389:	89 45 dc             	mov    %eax,-0x24(%ebp)
    free_page(p0);
c010538c:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0105393:	00 
c0105394:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105397:	89 04 24             	mov    %eax,(%esp)
c010539a:	e8 d5 d9 ff ff       	call   c0102d74 <free_pages>
    free_pages(p1, 3);
c010539f:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
c01053a6:	00 
c01053a7:	8b 45 e0             	mov    -0x20(%ebp),%eax
c01053aa:	89 04 24             	mov    %eax,(%esp)
c01053ad:	e8 c2 d9 ff ff       	call   c0102d74 <free_pages>
    assert(PageProperty(p0) && p0->property == 1);
c01053b2:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01053b5:	83 c0 04             	add    $0x4,%eax
c01053b8:	c7 45 a0 01 00 00 00 	movl   $0x1,-0x60(%ebp)
c01053bf:	89 45 9c             	mov    %eax,-0x64(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c01053c2:	8b 45 9c             	mov    -0x64(%ebp),%eax
c01053c5:	8b 55 a0             	mov    -0x60(%ebp),%edx
c01053c8:	0f a3 10             	bt     %edx,(%eax)
c01053cb:	19 c0                	sbb    %eax,%eax
c01053cd:	89 45 98             	mov    %eax,-0x68(%ebp)
    return oldbit != 0;
c01053d0:	83 7d 98 00          	cmpl   $0x0,-0x68(%ebp)
c01053d4:	0f 95 c0             	setne  %al
c01053d7:	0f b6 c0             	movzbl %al,%eax
c01053da:	85 c0                	test   %eax,%eax
c01053dc:	74 0b                	je     c01053e9 <default_check+0x3c6>
c01053de:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01053e1:	8b 40 08             	mov    0x8(%eax),%eax
c01053e4:	83 f8 01             	cmp    $0x1,%eax
c01053e7:	74 24                	je     c010540d <default_check+0x3ea>
c01053e9:	c7 44 24 0c 7c 72 10 	movl   $0xc010727c,0xc(%esp)
c01053f0:	c0 
c01053f1:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01053f8:	c0 
c01053f9:	c7 44 24 04 53 01 00 	movl   $0x153,0x4(%esp)
c0105400:	00 
c0105401:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105408:	e8 19 b0 ff ff       	call   c0100426 <__panic>
    assert(PageProperty(p1) && p1->property == 3);
c010540d:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0105410:	83 c0 04             	add    $0x4,%eax
c0105413:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
c010541a:	89 45 90             	mov    %eax,-0x70(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
c010541d:	8b 45 90             	mov    -0x70(%ebp),%eax
c0105420:	8b 55 94             	mov    -0x6c(%ebp),%edx
c0105423:	0f a3 10             	bt     %edx,(%eax)
c0105426:	19 c0                	sbb    %eax,%eax
c0105428:	89 45 8c             	mov    %eax,-0x74(%ebp)
    return oldbit != 0;
c010542b:	83 7d 8c 00          	cmpl   $0x0,-0x74(%ebp)
c010542f:	0f 95 c0             	setne  %al
c0105432:	0f b6 c0             	movzbl %al,%eax
c0105435:	85 c0                	test   %eax,%eax
c0105437:	74 0b                	je     c0105444 <default_check+0x421>
c0105439:	8b 45 e0             	mov    -0x20(%ebp),%eax
c010543c:	8b 40 08             	mov    0x8(%eax),%eax
c010543f:	83 f8 03             	cmp    $0x3,%eax
c0105442:	74 24                	je     c0105468 <default_check+0x445>
c0105444:	c7 44 24 0c a4 72 10 	movl   $0xc01072a4,0xc(%esp)
c010544b:	c0 
c010544c:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105453:	c0 
c0105454:	c7 44 24 04 54 01 00 	movl   $0x154,0x4(%esp)
c010545b:	00 
c010545c:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105463:	e8 be af ff ff       	call   c0100426 <__panic>

    assert((p0 = alloc_page()) == p2 - 1);
c0105468:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c010546f:	e8 c4 d8 ff ff       	call   c0102d38 <alloc_pages>
c0105474:	89 45 e8             	mov    %eax,-0x18(%ebp)
c0105477:	8b 45 dc             	mov    -0x24(%ebp),%eax
c010547a:	83 e8 14             	sub    $0x14,%eax
c010547d:	39 45 e8             	cmp    %eax,-0x18(%ebp)
c0105480:	74 24                	je     c01054a6 <default_check+0x483>
c0105482:	c7 44 24 0c ca 72 10 	movl   $0xc01072ca,0xc(%esp)
c0105489:	c0 
c010548a:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105491:	c0 
c0105492:	c7 44 24 04 56 01 00 	movl   $0x156,0x4(%esp)
c0105499:	00 
c010549a:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01054a1:	e8 80 af ff ff       	call   c0100426 <__panic>
    free_page(p0);
c01054a6:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c01054ad:	00 
c01054ae:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01054b1:	89 04 24             	mov    %eax,(%esp)
c01054b4:	e8 bb d8 ff ff       	call   c0102d74 <free_pages>
    assert((p0 = alloc_pages(2)) == p2 + 1);
c01054b9:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
c01054c0:	e8 73 d8 ff ff       	call   c0102d38 <alloc_pages>
c01054c5:	89 45 e8             	mov    %eax,-0x18(%ebp)
c01054c8:	8b 45 dc             	mov    -0x24(%ebp),%eax
c01054cb:	83 c0 14             	add    $0x14,%eax
c01054ce:	39 45 e8             	cmp    %eax,-0x18(%ebp)
c01054d1:	74 24                	je     c01054f7 <default_check+0x4d4>
c01054d3:	c7 44 24 0c e8 72 10 	movl   $0xc01072e8,0xc(%esp)
c01054da:	c0 
c01054db:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01054e2:	c0 
c01054e3:	c7 44 24 04 58 01 00 	movl   $0x158,0x4(%esp)
c01054ea:	00 
c01054eb:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01054f2:	e8 2f af ff ff       	call   c0100426 <__panic>

    free_pages(p0, 2);
c01054f7:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
c01054fe:	00 
c01054ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105502:	89 04 24             	mov    %eax,(%esp)
c0105505:	e8 6a d8 ff ff       	call   c0102d74 <free_pages>
    free_page(p2);
c010550a:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
c0105511:	00 
c0105512:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0105515:	89 04 24             	mov    %eax,(%esp)
c0105518:	e8 57 d8 ff ff       	call   c0102d74 <free_pages>

    assert((p0 = alloc_pages(5)) != NULL);
c010551d:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
c0105524:	e8 0f d8 ff ff       	call   c0102d38 <alloc_pages>
c0105529:	89 45 e8             	mov    %eax,-0x18(%ebp)
c010552c:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105530:	75 24                	jne    c0105556 <default_check+0x533>
c0105532:	c7 44 24 0c 08 73 10 	movl   $0xc0107308,0xc(%esp)
c0105539:	c0 
c010553a:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105541:	c0 
c0105542:	c7 44 24 04 5d 01 00 	movl   $0x15d,0x4(%esp)
c0105549:	00 
c010554a:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105551:	e8 d0 ae ff ff       	call   c0100426 <__panic>
    assert(alloc_page() == NULL);
c0105556:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
c010555d:	e8 d6 d7 ff ff       	call   c0102d38 <alloc_pages>
c0105562:	85 c0                	test   %eax,%eax
c0105564:	74 24                	je     c010558a <default_check+0x567>
c0105566:	c7 44 24 0c 66 71 10 	movl   $0xc0107166,0xc(%esp)
c010556d:	c0 
c010556e:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105575:	c0 
c0105576:	c7 44 24 04 5e 01 00 	movl   $0x15e,0x4(%esp)
c010557d:	00 
c010557e:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105585:	e8 9c ae ff ff       	call   c0100426 <__panic>

    assert(nr_free == 0);
c010558a:	a1 24 cf 11 c0       	mov    0xc011cf24,%eax
c010558f:	85 c0                	test   %eax,%eax
c0105591:	74 24                	je     c01055b7 <default_check+0x594>
c0105593:	c7 44 24 0c b9 71 10 	movl   $0xc01071b9,0xc(%esp)
c010559a:	c0 
c010559b:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c01055a2:	c0 
c01055a3:	c7 44 24 04 60 01 00 	movl   $0x160,0x4(%esp)
c01055aa:	00 
c01055ab:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01055b2:	e8 6f ae ff ff       	call   c0100426 <__panic>
    nr_free = nr_free_store;
c01055b7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c01055ba:	a3 24 cf 11 c0       	mov    %eax,0xc011cf24

    free_list = free_list_store;
c01055bf:	8b 45 80             	mov    -0x80(%ebp),%eax
c01055c2:	8b 55 84             	mov    -0x7c(%ebp),%edx
c01055c5:	a3 1c cf 11 c0       	mov    %eax,0xc011cf1c
c01055ca:	89 15 20 cf 11 c0    	mov    %edx,0xc011cf20
    free_pages(p0, 5);
c01055d0:	c7 44 24 04 05 00 00 	movl   $0x5,0x4(%esp)
c01055d7:	00 
c01055d8:	8b 45 e8             	mov    -0x18(%ebp),%eax
c01055db:	89 04 24             	mov    %eax,(%esp)
c01055de:	e8 91 d7 ff ff       	call   c0102d74 <free_pages>

    le = &free_list;
c01055e3:	c7 45 ec 1c cf 11 c0 	movl   $0xc011cf1c,-0x14(%ebp)
    while ((le = list_next(le)) != &free_list) {
c01055ea:	eb 5a                	jmp    c0105646 <default_check+0x623>
        assert(le->next->prev == le && le->prev->next == le);
c01055ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01055ef:	8b 40 04             	mov    0x4(%eax),%eax
c01055f2:	8b 00                	mov    (%eax),%eax
c01055f4:	39 45 ec             	cmp    %eax,-0x14(%ebp)
c01055f7:	75 0d                	jne    c0105606 <default_check+0x5e3>
c01055f9:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01055fc:	8b 00                	mov    (%eax),%eax
c01055fe:	8b 40 04             	mov    0x4(%eax),%eax
c0105601:	39 45 ec             	cmp    %eax,-0x14(%ebp)
c0105604:	74 24                	je     c010562a <default_check+0x607>
c0105606:	c7 44 24 0c 28 73 10 	movl   $0xc0107328,0xc(%esp)
c010560d:	c0 
c010560e:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105615:	c0 
c0105616:	c7 44 24 04 68 01 00 	movl   $0x168,0x4(%esp)
c010561d:	00 
c010561e:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105625:	e8 fc ad ff ff       	call   c0100426 <__panic>
        struct Page *p = le2page(le, page_link);
c010562a:	8b 45 ec             	mov    -0x14(%ebp),%eax
c010562d:	83 e8 0c             	sub    $0xc,%eax
c0105630:	89 45 d8             	mov    %eax,-0x28(%ebp)
        count --, total -= p->property;
c0105633:	ff 4d f4             	decl   -0xc(%ebp)
c0105636:	8b 55 f0             	mov    -0x10(%ebp),%edx
c0105639:	8b 45 d8             	mov    -0x28(%ebp),%eax
c010563c:	8b 40 08             	mov    0x8(%eax),%eax
c010563f:	29 c2                	sub    %eax,%edx
c0105641:	89 d0                	mov    %edx,%eax
c0105643:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0105646:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0105649:	89 45 88             	mov    %eax,-0x78(%ebp)
    return listelm->next;
c010564c:	8b 45 88             	mov    -0x78(%ebp),%eax
c010564f:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) {
c0105652:	89 45 ec             	mov    %eax,-0x14(%ebp)
c0105655:	81 7d ec 1c cf 11 c0 	cmpl   $0xc011cf1c,-0x14(%ebp)
c010565c:	75 8e                	jne    c01055ec <default_check+0x5c9>
    }
    assert(count == 0);
c010565e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
c0105662:	74 24                	je     c0105688 <default_check+0x665>
c0105664:	c7 44 24 0c 55 73 10 	movl   $0xc0107355,0xc(%esp)
c010566b:	c0 
c010566c:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c0105673:	c0 
c0105674:	c7 44 24 04 6c 01 00 	movl   $0x16c,0x4(%esp)
c010567b:	00 
c010567c:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c0105683:	e8 9e ad ff ff       	call   c0100426 <__panic>
    assert(total == 0);
c0105688:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c010568c:	74 24                	je     c01056b2 <default_check+0x68f>
c010568e:	c7 44 24 0c 60 73 10 	movl   $0xc0107360,0xc(%esp)
c0105695:	c0 
c0105696:	c7 44 24 08 de 6f 10 	movl   $0xc0106fde,0x8(%esp)
c010569d:	c0 
c010569e:	c7 44 24 04 6d 01 00 	movl   $0x16d,0x4(%esp)
c01056a5:	00 
c01056a6:	c7 04 24 f3 6f 10 c0 	movl   $0xc0106ff3,(%esp)
c01056ad:	e8 74 ad ff ff       	call   c0100426 <__panic>
}
c01056b2:	90                   	nop
c01056b3:	c9                   	leave  
c01056b4:	c3                   	ret    

c01056b5 <strlen>:
 * @s:      the input string
 *
 * The strlen() function returns the length of string @s.
 * */
size_t
strlen(const char *s) {
c01056b5:	f3 0f 1e fb          	endbr32 
c01056b9:	55                   	push   %ebp
c01056ba:	89 e5                	mov    %esp,%ebp
c01056bc:	83 ec 10             	sub    $0x10,%esp
    size_t cnt = 0;
c01056bf:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    while (*s ++ != '\0') {
c01056c6:	eb 03                	jmp    c01056cb <strlen+0x16>
        cnt ++;
c01056c8:	ff 45 fc             	incl   -0x4(%ebp)
    while (*s ++ != '\0') {
c01056cb:	8b 45 08             	mov    0x8(%ebp),%eax
c01056ce:	8d 50 01             	lea    0x1(%eax),%edx
c01056d1:	89 55 08             	mov    %edx,0x8(%ebp)
c01056d4:	0f b6 00             	movzbl (%eax),%eax
c01056d7:	84 c0                	test   %al,%al
c01056d9:	75 ed                	jne    c01056c8 <strlen+0x13>
    }
    return cnt;
c01056db:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
c01056de:	c9                   	leave  
c01056df:	c3                   	ret    

c01056e0 <strnlen>:
 * The return value is strlen(s), if that is less than @len, or
 * @len if there is no '\0' character among the first @len characters
 * pointed by @s.
 * */
size_t
strnlen(const char *s, size_t len) {
c01056e0:	f3 0f 1e fb          	endbr32 
c01056e4:	55                   	push   %ebp
c01056e5:	89 e5                	mov    %esp,%ebp
c01056e7:	83 ec 10             	sub    $0x10,%esp
    size_t cnt = 0;
c01056ea:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    while (cnt < len && *s ++ != '\0') {
c01056f1:	eb 03                	jmp    c01056f6 <strnlen+0x16>
        cnt ++;
c01056f3:	ff 45 fc             	incl   -0x4(%ebp)
    while (cnt < len && *s ++ != '\0') {
c01056f6:	8b 45 fc             	mov    -0x4(%ebp),%eax
c01056f9:	3b 45 0c             	cmp    0xc(%ebp),%eax
c01056fc:	73 10                	jae    c010570e <strnlen+0x2e>
c01056fe:	8b 45 08             	mov    0x8(%ebp),%eax
c0105701:	8d 50 01             	lea    0x1(%eax),%edx
c0105704:	89 55 08             	mov    %edx,0x8(%ebp)
c0105707:	0f b6 00             	movzbl (%eax),%eax
c010570a:	84 c0                	test   %al,%al
c010570c:	75 e5                	jne    c01056f3 <strnlen+0x13>
    }
    return cnt;
c010570e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
c0105711:	c9                   	leave  
c0105712:	c3                   	ret    

c0105713 <strcpy>:
 * To avoid overflows, the size of array pointed by @dst should be long enough to
 * contain the same string as @src (including the terminating null character), and
 * should not overlap in memory with @src.
 * */
char *
strcpy(char *dst, const char *src) {
c0105713:	f3 0f 1e fb          	endbr32 
c0105717:	55                   	push   %ebp
c0105718:	89 e5                	mov    %esp,%ebp
c010571a:	57                   	push   %edi
c010571b:	56                   	push   %esi
c010571c:	83 ec 20             	sub    $0x20,%esp
c010571f:	8b 45 08             	mov    0x8(%ebp),%eax
c0105722:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0105725:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105728:	89 45 f0             	mov    %eax,-0x10(%ebp)
#ifndef __HAVE_ARCH_STRCPY
#define __HAVE_ARCH_STRCPY
static inline char *
__strcpy(char *dst, const char *src) {
    int d0, d1, d2;
    asm volatile (
c010572b:	8b 55 f0             	mov    -0x10(%ebp),%edx
c010572e:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0105731:	89 d1                	mov    %edx,%ecx
c0105733:	89 c2                	mov    %eax,%edx
c0105735:	89 ce                	mov    %ecx,%esi
c0105737:	89 d7                	mov    %edx,%edi
c0105739:	ac                   	lods   %ds:(%esi),%al
c010573a:	aa                   	stos   %al,%es:(%edi)
c010573b:	84 c0                	test   %al,%al
c010573d:	75 fa                	jne    c0105739 <strcpy+0x26>
c010573f:	89 fa                	mov    %edi,%edx
c0105741:	89 f1                	mov    %esi,%ecx
c0105743:	89 4d ec             	mov    %ecx,-0x14(%ebp)
c0105746:	89 55 e8             	mov    %edx,-0x18(%ebp)
c0105749:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        "stosb;"
        "testb %%al, %%al;"
        "jne 1b;"
        : "=&S" (d0), "=&D" (d1), "=&a" (d2)
        : "0" (src), "1" (dst) : "memory");
    return dst;
c010574c:	8b 45 f4             	mov    -0xc(%ebp),%eax
    char *p = dst;
    while ((*p ++ = *src ++) != '\0')
        /* nothing */;
    return dst;
#endif /* __HAVE_ARCH_STRCPY */
}
c010574f:	83 c4 20             	add    $0x20,%esp
c0105752:	5e                   	pop    %esi
c0105753:	5f                   	pop    %edi
c0105754:	5d                   	pop    %ebp
c0105755:	c3                   	ret    

c0105756 <strncpy>:
 * @len:    maximum number of characters to be copied from @src
 *
 * The return value is @dst
 * */
char *
strncpy(char *dst, const char *src, size_t len) {
c0105756:	f3 0f 1e fb          	endbr32 
c010575a:	55                   	push   %ebp
c010575b:	89 e5                	mov    %esp,%ebp
c010575d:	83 ec 10             	sub    $0x10,%esp
    char *p = dst;
c0105760:	8b 45 08             	mov    0x8(%ebp),%eax
c0105763:	89 45 fc             	mov    %eax,-0x4(%ebp)
    while (len > 0) {
c0105766:	eb 1e                	jmp    c0105786 <strncpy+0x30>
        if ((*p = *src) != '\0') {
c0105768:	8b 45 0c             	mov    0xc(%ebp),%eax
c010576b:	0f b6 10             	movzbl (%eax),%edx
c010576e:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0105771:	88 10                	mov    %dl,(%eax)
c0105773:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0105776:	0f b6 00             	movzbl (%eax),%eax
c0105779:	84 c0                	test   %al,%al
c010577b:	74 03                	je     c0105780 <strncpy+0x2a>
            src ++;
c010577d:	ff 45 0c             	incl   0xc(%ebp)
        }
        p ++, len --;
c0105780:	ff 45 fc             	incl   -0x4(%ebp)
c0105783:	ff 4d 10             	decl   0x10(%ebp)
    while (len > 0) {
c0105786:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c010578a:	75 dc                	jne    c0105768 <strncpy+0x12>
    }
    return dst;
c010578c:	8b 45 08             	mov    0x8(%ebp),%eax
}
c010578f:	c9                   	leave  
c0105790:	c3                   	ret    

c0105791 <strcmp>:
 * - A value greater than zero indicates that the first character that does
 *   not match has a greater value in @s1 than in @s2;
 * - And a value less than zero indicates the opposite.
 * */
int
strcmp(const char *s1, const char *s2) {
c0105791:	f3 0f 1e fb          	endbr32 
c0105795:	55                   	push   %ebp
c0105796:	89 e5                	mov    %esp,%ebp
c0105798:	57                   	push   %edi
c0105799:	56                   	push   %esi
c010579a:	83 ec 20             	sub    $0x20,%esp
c010579d:	8b 45 08             	mov    0x8(%ebp),%eax
c01057a0:	89 45 f4             	mov    %eax,-0xc(%ebp)
c01057a3:	8b 45 0c             	mov    0xc(%ebp),%eax
c01057a6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    asm volatile (
c01057a9:	8b 55 f4             	mov    -0xc(%ebp),%edx
c01057ac:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01057af:	89 d1                	mov    %edx,%ecx
c01057b1:	89 c2                	mov    %eax,%edx
c01057b3:	89 ce                	mov    %ecx,%esi
c01057b5:	89 d7                	mov    %edx,%edi
c01057b7:	ac                   	lods   %ds:(%esi),%al
c01057b8:	ae                   	scas   %es:(%edi),%al
c01057b9:	75 08                	jne    c01057c3 <strcmp+0x32>
c01057bb:	84 c0                	test   %al,%al
c01057bd:	75 f8                	jne    c01057b7 <strcmp+0x26>
c01057bf:	31 c0                	xor    %eax,%eax
c01057c1:	eb 04                	jmp    c01057c7 <strcmp+0x36>
c01057c3:	19 c0                	sbb    %eax,%eax
c01057c5:	0c 01                	or     $0x1,%al
c01057c7:	89 fa                	mov    %edi,%edx
c01057c9:	89 f1                	mov    %esi,%ecx
c01057cb:	89 45 ec             	mov    %eax,-0x14(%ebp)
c01057ce:	89 4d e8             	mov    %ecx,-0x18(%ebp)
c01057d1:	89 55 e4             	mov    %edx,-0x1c(%ebp)
    return ret;
c01057d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
    while (*s1 != '\0' && *s1 == *s2) {
        s1 ++, s2 ++;
    }
    return (int)((unsigned char)*s1 - (unsigned char)*s2);
#endif /* __HAVE_ARCH_STRCMP */
}
c01057d7:	83 c4 20             	add    $0x20,%esp
c01057da:	5e                   	pop    %esi
c01057db:	5f                   	pop    %edi
c01057dc:	5d                   	pop    %ebp
c01057dd:	c3                   	ret    

c01057de <strncmp>:
 * they are equal to each other, it continues with the following pairs until
 * the characters differ, until a terminating null-character is reached, or
 * until @n characters match in both strings, whichever happens first.
 * */
int
strncmp(const char *s1, const char *s2, size_t n) {
c01057de:	f3 0f 1e fb          	endbr32 
c01057e2:	55                   	push   %ebp
c01057e3:	89 e5                	mov    %esp,%ebp
    while (n > 0 && *s1 != '\0' && *s1 == *s2) {
c01057e5:	eb 09                	jmp    c01057f0 <strncmp+0x12>
        n --, s1 ++, s2 ++;
c01057e7:	ff 4d 10             	decl   0x10(%ebp)
c01057ea:	ff 45 08             	incl   0x8(%ebp)
c01057ed:	ff 45 0c             	incl   0xc(%ebp)
    while (n > 0 && *s1 != '\0' && *s1 == *s2) {
c01057f0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c01057f4:	74 1a                	je     c0105810 <strncmp+0x32>
c01057f6:	8b 45 08             	mov    0x8(%ebp),%eax
c01057f9:	0f b6 00             	movzbl (%eax),%eax
c01057fc:	84 c0                	test   %al,%al
c01057fe:	74 10                	je     c0105810 <strncmp+0x32>
c0105800:	8b 45 08             	mov    0x8(%ebp),%eax
c0105803:	0f b6 10             	movzbl (%eax),%edx
c0105806:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105809:	0f b6 00             	movzbl (%eax),%eax
c010580c:	38 c2                	cmp    %al,%dl
c010580e:	74 d7                	je     c01057e7 <strncmp+0x9>
    }
    return (n == 0) ? 0 : (int)((unsigned char)*s1 - (unsigned char)*s2);
c0105810:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c0105814:	74 18                	je     c010582e <strncmp+0x50>
c0105816:	8b 45 08             	mov    0x8(%ebp),%eax
c0105819:	0f b6 00             	movzbl (%eax),%eax
c010581c:	0f b6 d0             	movzbl %al,%edx
c010581f:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105822:	0f b6 00             	movzbl (%eax),%eax
c0105825:	0f b6 c0             	movzbl %al,%eax
c0105828:	29 c2                	sub    %eax,%edx
c010582a:	89 d0                	mov    %edx,%eax
c010582c:	eb 05                	jmp    c0105833 <strncmp+0x55>
c010582e:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0105833:	5d                   	pop    %ebp
c0105834:	c3                   	ret    

c0105835 <strchr>:
 *
 * The strchr() function returns a pointer to the first occurrence of
 * character in @s. If the value is not found, the function returns 'NULL'.
 * */
char *
strchr(const char *s, char c) {
c0105835:	f3 0f 1e fb          	endbr32 
c0105839:	55                   	push   %ebp
c010583a:	89 e5                	mov    %esp,%ebp
c010583c:	83 ec 04             	sub    $0x4,%esp
c010583f:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105842:	88 45 fc             	mov    %al,-0x4(%ebp)
    while (*s != '\0') {
c0105845:	eb 13                	jmp    c010585a <strchr+0x25>
        if (*s == c) {
c0105847:	8b 45 08             	mov    0x8(%ebp),%eax
c010584a:	0f b6 00             	movzbl (%eax),%eax
c010584d:	38 45 fc             	cmp    %al,-0x4(%ebp)
c0105850:	75 05                	jne    c0105857 <strchr+0x22>
            return (char *)s;
c0105852:	8b 45 08             	mov    0x8(%ebp),%eax
c0105855:	eb 12                	jmp    c0105869 <strchr+0x34>
        }
        s ++;
c0105857:	ff 45 08             	incl   0x8(%ebp)
    while (*s != '\0') {
c010585a:	8b 45 08             	mov    0x8(%ebp),%eax
c010585d:	0f b6 00             	movzbl (%eax),%eax
c0105860:	84 c0                	test   %al,%al
c0105862:	75 e3                	jne    c0105847 <strchr+0x12>
    }
    return NULL;
c0105864:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0105869:	c9                   	leave  
c010586a:	c3                   	ret    

c010586b <strfind>:
 * The strfind() function is like strchr() except that if @c is
 * not found in @s, then it returns a pointer to the null byte at the
 * end of @s, rather than 'NULL'.
 * */
char *
strfind(const char *s, char c) {
c010586b:	f3 0f 1e fb          	endbr32 
c010586f:	55                   	push   %ebp
c0105870:	89 e5                	mov    %esp,%ebp
c0105872:	83 ec 04             	sub    $0x4,%esp
c0105875:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105878:	88 45 fc             	mov    %al,-0x4(%ebp)
    while (*s != '\0') {
c010587b:	eb 0e                	jmp    c010588b <strfind+0x20>
        if (*s == c) {
c010587d:	8b 45 08             	mov    0x8(%ebp),%eax
c0105880:	0f b6 00             	movzbl (%eax),%eax
c0105883:	38 45 fc             	cmp    %al,-0x4(%ebp)
c0105886:	74 0f                	je     c0105897 <strfind+0x2c>
            break;
        }
        s ++;
c0105888:	ff 45 08             	incl   0x8(%ebp)
    while (*s != '\0') {
c010588b:	8b 45 08             	mov    0x8(%ebp),%eax
c010588e:	0f b6 00             	movzbl (%eax),%eax
c0105891:	84 c0                	test   %al,%al
c0105893:	75 e8                	jne    c010587d <strfind+0x12>
c0105895:	eb 01                	jmp    c0105898 <strfind+0x2d>
            break;
c0105897:	90                   	nop
    }
    return (char *)s;
c0105898:	8b 45 08             	mov    0x8(%ebp),%eax
}
c010589b:	c9                   	leave  
c010589c:	c3                   	ret    

c010589d <strtol>:
 * an optional "0x" or "0X" prefix.
 *
 * The strtol() function returns the converted integral number as a long int value.
 * */
long
strtol(const char *s, char **endptr, int base) {
c010589d:	f3 0f 1e fb          	endbr32 
c01058a1:	55                   	push   %ebp
c01058a2:	89 e5                	mov    %esp,%ebp
c01058a4:	83 ec 10             	sub    $0x10,%esp
    int neg = 0;
c01058a7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    long val = 0;
c01058ae:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

    // gobble initial whitespace
    while (*s == ' ' || *s == '\t') {
c01058b5:	eb 03                	jmp    c01058ba <strtol+0x1d>
        s ++;
c01058b7:	ff 45 08             	incl   0x8(%ebp)
    while (*s == ' ' || *s == '\t') {
c01058ba:	8b 45 08             	mov    0x8(%ebp),%eax
c01058bd:	0f b6 00             	movzbl (%eax),%eax
c01058c0:	3c 20                	cmp    $0x20,%al
c01058c2:	74 f3                	je     c01058b7 <strtol+0x1a>
c01058c4:	8b 45 08             	mov    0x8(%ebp),%eax
c01058c7:	0f b6 00             	movzbl (%eax),%eax
c01058ca:	3c 09                	cmp    $0x9,%al
c01058cc:	74 e9                	je     c01058b7 <strtol+0x1a>
    }

    // plus/minus sign
    if (*s == '+') {
c01058ce:	8b 45 08             	mov    0x8(%ebp),%eax
c01058d1:	0f b6 00             	movzbl (%eax),%eax
c01058d4:	3c 2b                	cmp    $0x2b,%al
c01058d6:	75 05                	jne    c01058dd <strtol+0x40>
        s ++;
c01058d8:	ff 45 08             	incl   0x8(%ebp)
c01058db:	eb 14                	jmp    c01058f1 <strtol+0x54>
    }
    else if (*s == '-') {
c01058dd:	8b 45 08             	mov    0x8(%ebp),%eax
c01058e0:	0f b6 00             	movzbl (%eax),%eax
c01058e3:	3c 2d                	cmp    $0x2d,%al
c01058e5:	75 0a                	jne    c01058f1 <strtol+0x54>
        s ++, neg = 1;
c01058e7:	ff 45 08             	incl   0x8(%ebp)
c01058ea:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)
    }

    // hex or octal base prefix
    if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x')) {
c01058f1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c01058f5:	74 06                	je     c01058fd <strtol+0x60>
c01058f7:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
c01058fb:	75 22                	jne    c010591f <strtol+0x82>
c01058fd:	8b 45 08             	mov    0x8(%ebp),%eax
c0105900:	0f b6 00             	movzbl (%eax),%eax
c0105903:	3c 30                	cmp    $0x30,%al
c0105905:	75 18                	jne    c010591f <strtol+0x82>
c0105907:	8b 45 08             	mov    0x8(%ebp),%eax
c010590a:	40                   	inc    %eax
c010590b:	0f b6 00             	movzbl (%eax),%eax
c010590e:	3c 78                	cmp    $0x78,%al
c0105910:	75 0d                	jne    c010591f <strtol+0x82>
        s += 2, base = 16;
c0105912:	83 45 08 02          	addl   $0x2,0x8(%ebp)
c0105916:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
c010591d:	eb 29                	jmp    c0105948 <strtol+0xab>
    }
    else if (base == 0 && s[0] == '0') {
c010591f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c0105923:	75 16                	jne    c010593b <strtol+0x9e>
c0105925:	8b 45 08             	mov    0x8(%ebp),%eax
c0105928:	0f b6 00             	movzbl (%eax),%eax
c010592b:	3c 30                	cmp    $0x30,%al
c010592d:	75 0c                	jne    c010593b <strtol+0x9e>
        s ++, base = 8;
c010592f:	ff 45 08             	incl   0x8(%ebp)
c0105932:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
c0105939:	eb 0d                	jmp    c0105948 <strtol+0xab>
    }
    else if (base == 0) {
c010593b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
c010593f:	75 07                	jne    c0105948 <strtol+0xab>
        base = 10;
c0105941:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

    // digits
    while (1) {
        int dig;

        if (*s >= '0' && *s <= '9') {
c0105948:	8b 45 08             	mov    0x8(%ebp),%eax
c010594b:	0f b6 00             	movzbl (%eax),%eax
c010594e:	3c 2f                	cmp    $0x2f,%al
c0105950:	7e 1b                	jle    c010596d <strtol+0xd0>
c0105952:	8b 45 08             	mov    0x8(%ebp),%eax
c0105955:	0f b6 00             	movzbl (%eax),%eax
c0105958:	3c 39                	cmp    $0x39,%al
c010595a:	7f 11                	jg     c010596d <strtol+0xd0>
            dig = *s - '0';
c010595c:	8b 45 08             	mov    0x8(%ebp),%eax
c010595f:	0f b6 00             	movzbl (%eax),%eax
c0105962:	0f be c0             	movsbl %al,%eax
c0105965:	83 e8 30             	sub    $0x30,%eax
c0105968:	89 45 f4             	mov    %eax,-0xc(%ebp)
c010596b:	eb 48                	jmp    c01059b5 <strtol+0x118>
        }
        else if (*s >= 'a' && *s <= 'z') {
c010596d:	8b 45 08             	mov    0x8(%ebp),%eax
c0105970:	0f b6 00             	movzbl (%eax),%eax
c0105973:	3c 60                	cmp    $0x60,%al
c0105975:	7e 1b                	jle    c0105992 <strtol+0xf5>
c0105977:	8b 45 08             	mov    0x8(%ebp),%eax
c010597a:	0f b6 00             	movzbl (%eax),%eax
c010597d:	3c 7a                	cmp    $0x7a,%al
c010597f:	7f 11                	jg     c0105992 <strtol+0xf5>
            dig = *s - 'a' + 10;
c0105981:	8b 45 08             	mov    0x8(%ebp),%eax
c0105984:	0f b6 00             	movzbl (%eax),%eax
c0105987:	0f be c0             	movsbl %al,%eax
c010598a:	83 e8 57             	sub    $0x57,%eax
c010598d:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0105990:	eb 23                	jmp    c01059b5 <strtol+0x118>
        }
        else if (*s >= 'A' && *s <= 'Z') {
c0105992:	8b 45 08             	mov    0x8(%ebp),%eax
c0105995:	0f b6 00             	movzbl (%eax),%eax
c0105998:	3c 40                	cmp    $0x40,%al
c010599a:	7e 3b                	jle    c01059d7 <strtol+0x13a>
c010599c:	8b 45 08             	mov    0x8(%ebp),%eax
c010599f:	0f b6 00             	movzbl (%eax),%eax
c01059a2:	3c 5a                	cmp    $0x5a,%al
c01059a4:	7f 31                	jg     c01059d7 <strtol+0x13a>
            dig = *s - 'A' + 10;
c01059a6:	8b 45 08             	mov    0x8(%ebp),%eax
c01059a9:	0f b6 00             	movzbl (%eax),%eax
c01059ac:	0f be c0             	movsbl %al,%eax
c01059af:	83 e8 37             	sub    $0x37,%eax
c01059b2:	89 45 f4             	mov    %eax,-0xc(%ebp)
        }
        else {
            break;
        }
        if (dig >= base) {
c01059b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01059b8:	3b 45 10             	cmp    0x10(%ebp),%eax
c01059bb:	7d 19                	jge    c01059d6 <strtol+0x139>
            break;
        }
        s ++, val = (val * base) + dig;
c01059bd:	ff 45 08             	incl   0x8(%ebp)
c01059c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
c01059c3:	0f af 45 10          	imul   0x10(%ebp),%eax
c01059c7:	89 c2                	mov    %eax,%edx
c01059c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
c01059cc:	01 d0                	add    %edx,%eax
c01059ce:	89 45 f8             	mov    %eax,-0x8(%ebp)
    while (1) {
c01059d1:	e9 72 ff ff ff       	jmp    c0105948 <strtol+0xab>
            break;
c01059d6:	90                   	nop
        // we don't properly detect overflow!
    }

    if (endptr) {
c01059d7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
c01059db:	74 08                	je     c01059e5 <strtol+0x148>
        *endptr = (char *) s;
c01059dd:	8b 45 0c             	mov    0xc(%ebp),%eax
c01059e0:	8b 55 08             	mov    0x8(%ebp),%edx
c01059e3:	89 10                	mov    %edx,(%eax)
    }
    return (neg ? -val : val);
c01059e5:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
c01059e9:	74 07                	je     c01059f2 <strtol+0x155>
c01059eb:	8b 45 f8             	mov    -0x8(%ebp),%eax
c01059ee:	f7 d8                	neg    %eax
c01059f0:	eb 03                	jmp    c01059f5 <strtol+0x158>
c01059f2:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
c01059f5:	c9                   	leave  
c01059f6:	c3                   	ret    

c01059f7 <memset>:
 * @n:      number of bytes to be set to the value
 *
 * The memset() function returns @s.
 * */
void *
memset(void *s, char c, size_t n) {
c01059f7:	f3 0f 1e fb          	endbr32 
c01059fb:	55                   	push   %ebp
c01059fc:	89 e5                	mov    %esp,%ebp
c01059fe:	57                   	push   %edi
c01059ff:	83 ec 24             	sub    $0x24,%esp
c0105a02:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105a05:	88 45 d8             	mov    %al,-0x28(%ebp)
#ifdef __HAVE_ARCH_MEMSET
    return __memset(s, c, n);
c0105a08:	0f be 55 d8          	movsbl -0x28(%ebp),%edx
c0105a0c:	8b 45 08             	mov    0x8(%ebp),%eax
c0105a0f:	89 45 f8             	mov    %eax,-0x8(%ebp)
c0105a12:	88 55 f7             	mov    %dl,-0x9(%ebp)
c0105a15:	8b 45 10             	mov    0x10(%ebp),%eax
c0105a18:	89 45 f0             	mov    %eax,-0x10(%ebp)
#ifndef __HAVE_ARCH_MEMSET
#define __HAVE_ARCH_MEMSET
static inline void *
__memset(void *s, char c, size_t n) {
    int d0, d1;
    asm volatile (
c0105a1b:	8b 4d f0             	mov    -0x10(%ebp),%ecx
c0105a1e:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
c0105a22:	8b 55 f8             	mov    -0x8(%ebp),%edx
c0105a25:	89 d7                	mov    %edx,%edi
c0105a27:	f3 aa                	rep stos %al,%es:(%edi)
c0105a29:	89 fa                	mov    %edi,%edx
c0105a2b:	89 4d ec             	mov    %ecx,-0x14(%ebp)
c0105a2e:	89 55 e8             	mov    %edx,-0x18(%ebp)
        "rep; stosb;"
        : "=&c" (d0), "=&D" (d1)
        : "0" (n), "a" (c), "1" (s)
        : "memory");
    return s;
c0105a31:	8b 45 f8             	mov    -0x8(%ebp),%eax
    while (n -- > 0) {
        *p ++ = c;
    }
    return s;
#endif /* __HAVE_ARCH_MEMSET */
}
c0105a34:	83 c4 24             	add    $0x24,%esp
c0105a37:	5f                   	pop    %edi
c0105a38:	5d                   	pop    %ebp
c0105a39:	c3                   	ret    

c0105a3a <memmove>:
 * @n:      number of bytes to copy
 *
 * The memmove() function returns @dst.
 * */
void *
memmove(void *dst, const void *src, size_t n) {
c0105a3a:	f3 0f 1e fb          	endbr32 
c0105a3e:	55                   	push   %ebp
c0105a3f:	89 e5                	mov    %esp,%ebp
c0105a41:	57                   	push   %edi
c0105a42:	56                   	push   %esi
c0105a43:	53                   	push   %ebx
c0105a44:	83 ec 30             	sub    $0x30,%esp
c0105a47:	8b 45 08             	mov    0x8(%ebp),%eax
c0105a4a:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0105a4d:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105a50:	89 45 ec             	mov    %eax,-0x14(%ebp)
c0105a53:	8b 45 10             	mov    0x10(%ebp),%eax
c0105a56:	89 45 e8             	mov    %eax,-0x18(%ebp)

#ifndef __HAVE_ARCH_MEMMOVE
#define __HAVE_ARCH_MEMMOVE
static inline void *
__memmove(void *dst, const void *src, size_t n) {
    if (dst < src) {
c0105a59:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105a5c:	3b 45 ec             	cmp    -0x14(%ebp),%eax
c0105a5f:	73 42                	jae    c0105aa3 <memmove+0x69>
c0105a61:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105a64:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c0105a67:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0105a6a:	89 45 e0             	mov    %eax,-0x20(%ebp)
c0105a6d:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105a70:	89 45 dc             	mov    %eax,-0x24(%ebp)
        "andl $3, %%ecx;"
        "jz 1f;"
        "rep; movsb;"
        "1:"
        : "=&c" (d0), "=&D" (d1), "=&S" (d2)
        : "0" (n / 4), "g" (n), "1" (dst), "2" (src)
c0105a73:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0105a76:	c1 e8 02             	shr    $0x2,%eax
c0105a79:	89 c1                	mov    %eax,%ecx
    asm volatile (
c0105a7b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0105a7e:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0105a81:	89 d7                	mov    %edx,%edi
c0105a83:	89 c6                	mov    %eax,%esi
c0105a85:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
c0105a87:	8b 4d dc             	mov    -0x24(%ebp),%ecx
c0105a8a:	83 e1 03             	and    $0x3,%ecx
c0105a8d:	74 02                	je     c0105a91 <memmove+0x57>
c0105a8f:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
c0105a91:	89 f0                	mov    %esi,%eax
c0105a93:	89 fa                	mov    %edi,%edx
c0105a95:	89 4d d8             	mov    %ecx,-0x28(%ebp)
c0105a98:	89 55 d4             	mov    %edx,-0x2c(%ebp)
c0105a9b:	89 45 d0             	mov    %eax,-0x30(%ebp)
        : "memory");
    return dst;
c0105a9e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
        return __memcpy(dst, src, n);
c0105aa1:	eb 36                	jmp    c0105ad9 <memmove+0x9f>
        : "0" (n), "1" (n - 1 + src), "2" (n - 1 + dst)
c0105aa3:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105aa6:	8d 50 ff             	lea    -0x1(%eax),%edx
c0105aa9:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0105aac:	01 c2                	add    %eax,%edx
c0105aae:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105ab1:	8d 48 ff             	lea    -0x1(%eax),%ecx
c0105ab4:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105ab7:	8d 1c 01             	lea    (%ecx,%eax,1),%ebx
    asm volatile (
c0105aba:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105abd:	89 c1                	mov    %eax,%ecx
c0105abf:	89 d8                	mov    %ebx,%eax
c0105ac1:	89 d6                	mov    %edx,%esi
c0105ac3:	89 c7                	mov    %eax,%edi
c0105ac5:	fd                   	std    
c0105ac6:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
c0105ac8:	fc                   	cld    
c0105ac9:	89 f8                	mov    %edi,%eax
c0105acb:	89 f2                	mov    %esi,%edx
c0105acd:	89 4d cc             	mov    %ecx,-0x34(%ebp)
c0105ad0:	89 55 c8             	mov    %edx,-0x38(%ebp)
c0105ad3:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    return dst;
c0105ad6:	8b 45 f0             	mov    -0x10(%ebp),%eax
            *d ++ = *s ++;
        }
    }
    return dst;
#endif /* __HAVE_ARCH_MEMMOVE */
}
c0105ad9:	83 c4 30             	add    $0x30,%esp
c0105adc:	5b                   	pop    %ebx
c0105add:	5e                   	pop    %esi
c0105ade:	5f                   	pop    %edi
c0105adf:	5d                   	pop    %ebp
c0105ae0:	c3                   	ret    

c0105ae1 <memcpy>:
 * it always copies exactly @n bytes. To avoid overflows, the size of arrays pointed
 * by both @src and @dst, should be at least @n bytes, and should not overlap
 * (for overlapping memory area, memmove is a safer approach).
 * */
void *
memcpy(void *dst, const void *src, size_t n) {
c0105ae1:	f3 0f 1e fb          	endbr32 
c0105ae5:	55                   	push   %ebp
c0105ae6:	89 e5                	mov    %esp,%ebp
c0105ae8:	57                   	push   %edi
c0105ae9:	56                   	push   %esi
c0105aea:	83 ec 20             	sub    $0x20,%esp
c0105aed:	8b 45 08             	mov    0x8(%ebp),%eax
c0105af0:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0105af3:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105af6:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0105af9:	8b 45 10             	mov    0x10(%ebp),%eax
c0105afc:	89 45 ec             	mov    %eax,-0x14(%ebp)
        : "0" (n / 4), "g" (n), "1" (dst), "2" (src)
c0105aff:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0105b02:	c1 e8 02             	shr    $0x2,%eax
c0105b05:	89 c1                	mov    %eax,%ecx
    asm volatile (
c0105b07:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0105b0a:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105b0d:	89 d7                	mov    %edx,%edi
c0105b0f:	89 c6                	mov    %eax,%esi
c0105b11:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
c0105b13:	8b 4d ec             	mov    -0x14(%ebp),%ecx
c0105b16:	83 e1 03             	and    $0x3,%ecx
c0105b19:	74 02                	je     c0105b1d <memcpy+0x3c>
c0105b1b:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
c0105b1d:	89 f0                	mov    %esi,%eax
c0105b1f:	89 fa                	mov    %edi,%edx
c0105b21:	89 4d e8             	mov    %ecx,-0x18(%ebp)
c0105b24:	89 55 e4             	mov    %edx,-0x1c(%ebp)
c0105b27:	89 45 e0             	mov    %eax,-0x20(%ebp)
    return dst;
c0105b2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
    while (n -- > 0) {
        *d ++ = *s ++;
    }
    return dst;
#endif /* __HAVE_ARCH_MEMCPY */
}
c0105b2d:	83 c4 20             	add    $0x20,%esp
c0105b30:	5e                   	pop    %esi
c0105b31:	5f                   	pop    %edi
c0105b32:	5d                   	pop    %ebp
c0105b33:	c3                   	ret    

c0105b34 <memcmp>:
 *   match in both memory blocks has a greater value in @v1 than in @v2
 *   as if evaluated as unsigned char values;
 * - And a value less than zero indicates the opposite.
 * */
int
memcmp(const void *v1, const void *v2, size_t n) {
c0105b34:	f3 0f 1e fb          	endbr32 
c0105b38:	55                   	push   %ebp
c0105b39:	89 e5                	mov    %esp,%ebp
c0105b3b:	83 ec 10             	sub    $0x10,%esp
    const char *s1 = (const char *)v1;
c0105b3e:	8b 45 08             	mov    0x8(%ebp),%eax
c0105b41:	89 45 fc             	mov    %eax,-0x4(%ebp)
    const char *s2 = (const char *)v2;
c0105b44:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105b47:	89 45 f8             	mov    %eax,-0x8(%ebp)
    while (n -- > 0) {
c0105b4a:	eb 2e                	jmp    c0105b7a <memcmp+0x46>
        if (*s1 != *s2) {
c0105b4c:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0105b4f:	0f b6 10             	movzbl (%eax),%edx
c0105b52:	8b 45 f8             	mov    -0x8(%ebp),%eax
c0105b55:	0f b6 00             	movzbl (%eax),%eax
c0105b58:	38 c2                	cmp    %al,%dl
c0105b5a:	74 18                	je     c0105b74 <memcmp+0x40>
            return (int)((unsigned char)*s1 - (unsigned char)*s2);
c0105b5c:	8b 45 fc             	mov    -0x4(%ebp),%eax
c0105b5f:	0f b6 00             	movzbl (%eax),%eax
c0105b62:	0f b6 d0             	movzbl %al,%edx
c0105b65:	8b 45 f8             	mov    -0x8(%ebp),%eax
c0105b68:	0f b6 00             	movzbl (%eax),%eax
c0105b6b:	0f b6 c0             	movzbl %al,%eax
c0105b6e:	29 c2                	sub    %eax,%edx
c0105b70:	89 d0                	mov    %edx,%eax
c0105b72:	eb 18                	jmp    c0105b8c <memcmp+0x58>
        }
        s1 ++, s2 ++;
c0105b74:	ff 45 fc             	incl   -0x4(%ebp)
c0105b77:	ff 45 f8             	incl   -0x8(%ebp)
    while (n -- > 0) {
c0105b7a:	8b 45 10             	mov    0x10(%ebp),%eax
c0105b7d:	8d 50 ff             	lea    -0x1(%eax),%edx
c0105b80:	89 55 10             	mov    %edx,0x10(%ebp)
c0105b83:	85 c0                	test   %eax,%eax
c0105b85:	75 c5                	jne    c0105b4c <memcmp+0x18>
    }
    return 0;
c0105b87:	b8 00 00 00 00       	mov    $0x0,%eax
}
c0105b8c:	c9                   	leave  
c0105b8d:	c3                   	ret    

c0105b8e <printnum>:
 * @width:      maximum number of digits, if the actual width is less than @width, use @padc instead
 * @padc:       character that padded on the left if the actual width is less than @width
 * */
static void
printnum(void (*putch)(int, void*), void *putdat,
        unsigned long long num, unsigned base, int width, int padc) {
c0105b8e:	f3 0f 1e fb          	endbr32 
c0105b92:	55                   	push   %ebp
c0105b93:	89 e5                	mov    %esp,%ebp
c0105b95:	83 ec 58             	sub    $0x58,%esp
c0105b98:	8b 45 10             	mov    0x10(%ebp),%eax
c0105b9b:	89 45 d0             	mov    %eax,-0x30(%ebp)
c0105b9e:	8b 45 14             	mov    0x14(%ebp),%eax
c0105ba1:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    unsigned long long result = num;
c0105ba4:	8b 45 d0             	mov    -0x30(%ebp),%eax
c0105ba7:	8b 55 d4             	mov    -0x2c(%ebp),%edx
c0105baa:	89 45 e8             	mov    %eax,-0x18(%ebp)
c0105bad:	89 55 ec             	mov    %edx,-0x14(%ebp)
    unsigned mod = do_div(result, base);
c0105bb0:	8b 45 18             	mov    0x18(%ebp),%eax
c0105bb3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
c0105bb6:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105bb9:	8b 55 ec             	mov    -0x14(%ebp),%edx
c0105bbc:	89 45 e0             	mov    %eax,-0x20(%ebp)
c0105bbf:	89 55 f0             	mov    %edx,-0x10(%ebp)
c0105bc2:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105bc5:	89 45 f4             	mov    %eax,-0xc(%ebp)
c0105bc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
c0105bcc:	74 1c                	je     c0105bea <printnum+0x5c>
c0105bce:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105bd1:	ba 00 00 00 00       	mov    $0x0,%edx
c0105bd6:	f7 75 e4             	divl   -0x1c(%ebp)
c0105bd9:	89 55 f4             	mov    %edx,-0xc(%ebp)
c0105bdc:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105bdf:	ba 00 00 00 00       	mov    $0x0,%edx
c0105be4:	f7 75 e4             	divl   -0x1c(%ebp)
c0105be7:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0105bea:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0105bed:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0105bf0:	f7 75 e4             	divl   -0x1c(%ebp)
c0105bf3:	89 45 e0             	mov    %eax,-0x20(%ebp)
c0105bf6:	89 55 dc             	mov    %edx,-0x24(%ebp)
c0105bf9:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0105bfc:	8b 55 f0             	mov    -0x10(%ebp),%edx
c0105bff:	89 45 e8             	mov    %eax,-0x18(%ebp)
c0105c02:	89 55 ec             	mov    %edx,-0x14(%ebp)
c0105c05:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0105c08:	89 45 d8             	mov    %eax,-0x28(%ebp)

    // first recursively print all preceding (more significant) digits
    if (num >= base) {
c0105c0b:	8b 45 18             	mov    0x18(%ebp),%eax
c0105c0e:	ba 00 00 00 00       	mov    $0x0,%edx
c0105c13:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
c0105c16:	39 45 d0             	cmp    %eax,-0x30(%ebp)
c0105c19:	19 d1                	sbb    %edx,%ecx
c0105c1b:	72 4c                	jb     c0105c69 <printnum+0xdb>
        printnum(putch, putdat, result, base, width - 1, padc);
c0105c1d:	8b 45 1c             	mov    0x1c(%ebp),%eax
c0105c20:	8d 50 ff             	lea    -0x1(%eax),%edx
c0105c23:	8b 45 20             	mov    0x20(%ebp),%eax
c0105c26:	89 44 24 18          	mov    %eax,0x18(%esp)
c0105c2a:	89 54 24 14          	mov    %edx,0x14(%esp)
c0105c2e:	8b 45 18             	mov    0x18(%ebp),%eax
c0105c31:	89 44 24 10          	mov    %eax,0x10(%esp)
c0105c35:	8b 45 e8             	mov    -0x18(%ebp),%eax
c0105c38:	8b 55 ec             	mov    -0x14(%ebp),%edx
c0105c3b:	89 44 24 08          	mov    %eax,0x8(%esp)
c0105c3f:	89 54 24 0c          	mov    %edx,0xc(%esp)
c0105c43:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105c46:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105c4a:	8b 45 08             	mov    0x8(%ebp),%eax
c0105c4d:	89 04 24             	mov    %eax,(%esp)
c0105c50:	e8 39 ff ff ff       	call   c0105b8e <printnum>
c0105c55:	eb 1b                	jmp    c0105c72 <printnum+0xe4>
    } else {
        // print any needed pad characters before first digit
        while (-- width > 0)
            putch(padc, putdat);
c0105c57:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105c5a:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105c5e:	8b 45 20             	mov    0x20(%ebp),%eax
c0105c61:	89 04 24             	mov    %eax,(%esp)
c0105c64:	8b 45 08             	mov    0x8(%ebp),%eax
c0105c67:	ff d0                	call   *%eax
        while (-- width > 0)
c0105c69:	ff 4d 1c             	decl   0x1c(%ebp)
c0105c6c:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
c0105c70:	7f e5                	jg     c0105c57 <printnum+0xc9>
    }
    // then print this (the least significant) digit
    putch("0123456789abcdef"[mod], putdat);
c0105c72:	8b 45 d8             	mov    -0x28(%ebp),%eax
c0105c75:	05 1c 74 10 c0       	add    $0xc010741c,%eax
c0105c7a:	0f b6 00             	movzbl (%eax),%eax
c0105c7d:	0f be c0             	movsbl %al,%eax
c0105c80:	8b 55 0c             	mov    0xc(%ebp),%edx
c0105c83:	89 54 24 04          	mov    %edx,0x4(%esp)
c0105c87:	89 04 24             	mov    %eax,(%esp)
c0105c8a:	8b 45 08             	mov    0x8(%ebp),%eax
c0105c8d:	ff d0                	call   *%eax
}
c0105c8f:	90                   	nop
c0105c90:	c9                   	leave  
c0105c91:	c3                   	ret    

c0105c92 <getuint>:
 * getuint - get an unsigned int of various possible sizes from a varargs list
 * @ap:         a varargs list pointer
 * @lflag:      determines the size of the vararg that @ap points to
 * */
static unsigned long long
getuint(va_list *ap, int lflag) {
c0105c92:	f3 0f 1e fb          	endbr32 
c0105c96:	55                   	push   %ebp
c0105c97:	89 e5                	mov    %esp,%ebp
    if (lflag >= 2) {
c0105c99:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
c0105c9d:	7e 14                	jle    c0105cb3 <getuint+0x21>
        return va_arg(*ap, unsigned long long);
c0105c9f:	8b 45 08             	mov    0x8(%ebp),%eax
c0105ca2:	8b 00                	mov    (%eax),%eax
c0105ca4:	8d 48 08             	lea    0x8(%eax),%ecx
c0105ca7:	8b 55 08             	mov    0x8(%ebp),%edx
c0105caa:	89 0a                	mov    %ecx,(%edx)
c0105cac:	8b 50 04             	mov    0x4(%eax),%edx
c0105caf:	8b 00                	mov    (%eax),%eax
c0105cb1:	eb 30                	jmp    c0105ce3 <getuint+0x51>
    }
    else if (lflag) {
c0105cb3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
c0105cb7:	74 16                	je     c0105ccf <getuint+0x3d>
        return va_arg(*ap, unsigned long);
c0105cb9:	8b 45 08             	mov    0x8(%ebp),%eax
c0105cbc:	8b 00                	mov    (%eax),%eax
c0105cbe:	8d 48 04             	lea    0x4(%eax),%ecx
c0105cc1:	8b 55 08             	mov    0x8(%ebp),%edx
c0105cc4:	89 0a                	mov    %ecx,(%edx)
c0105cc6:	8b 00                	mov    (%eax),%eax
c0105cc8:	ba 00 00 00 00       	mov    $0x0,%edx
c0105ccd:	eb 14                	jmp    c0105ce3 <getuint+0x51>
    }
    else {
        return va_arg(*ap, unsigned int);
c0105ccf:	8b 45 08             	mov    0x8(%ebp),%eax
c0105cd2:	8b 00                	mov    (%eax),%eax
c0105cd4:	8d 48 04             	lea    0x4(%eax),%ecx
c0105cd7:	8b 55 08             	mov    0x8(%ebp),%edx
c0105cda:	89 0a                	mov    %ecx,(%edx)
c0105cdc:	8b 00                	mov    (%eax),%eax
c0105cde:	ba 00 00 00 00       	mov    $0x0,%edx
    }
}
c0105ce3:	5d                   	pop    %ebp
c0105ce4:	c3                   	ret    

c0105ce5 <getint>:
 * getint - same as getuint but signed, we can't use getuint because of sign extension
 * @ap:         a varargs list pointer
 * @lflag:      determines the size of the vararg that @ap points to
 * */
static long long
getint(va_list *ap, int lflag) {
c0105ce5:	f3 0f 1e fb          	endbr32 
c0105ce9:	55                   	push   %ebp
c0105cea:	89 e5                	mov    %esp,%ebp
    if (lflag >= 2) {
c0105cec:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
c0105cf0:	7e 14                	jle    c0105d06 <getint+0x21>
        return va_arg(*ap, long long);
c0105cf2:	8b 45 08             	mov    0x8(%ebp),%eax
c0105cf5:	8b 00                	mov    (%eax),%eax
c0105cf7:	8d 48 08             	lea    0x8(%eax),%ecx
c0105cfa:	8b 55 08             	mov    0x8(%ebp),%edx
c0105cfd:	89 0a                	mov    %ecx,(%edx)
c0105cff:	8b 50 04             	mov    0x4(%eax),%edx
c0105d02:	8b 00                	mov    (%eax),%eax
c0105d04:	eb 28                	jmp    c0105d2e <getint+0x49>
    }
    else if (lflag) {
c0105d06:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
c0105d0a:	74 12                	je     c0105d1e <getint+0x39>
        return va_arg(*ap, long);
c0105d0c:	8b 45 08             	mov    0x8(%ebp),%eax
c0105d0f:	8b 00                	mov    (%eax),%eax
c0105d11:	8d 48 04             	lea    0x4(%eax),%ecx
c0105d14:	8b 55 08             	mov    0x8(%ebp),%edx
c0105d17:	89 0a                	mov    %ecx,(%edx)
c0105d19:	8b 00                	mov    (%eax),%eax
c0105d1b:	99                   	cltd   
c0105d1c:	eb 10                	jmp    c0105d2e <getint+0x49>
    }
    else {
        return va_arg(*ap, int);
c0105d1e:	8b 45 08             	mov    0x8(%ebp),%eax
c0105d21:	8b 00                	mov    (%eax),%eax
c0105d23:	8d 48 04             	lea    0x4(%eax),%ecx
c0105d26:	8b 55 08             	mov    0x8(%ebp),%edx
c0105d29:	89 0a                	mov    %ecx,(%edx)
c0105d2b:	8b 00                	mov    (%eax),%eax
c0105d2d:	99                   	cltd   
    }
}
c0105d2e:	5d                   	pop    %ebp
c0105d2f:	c3                   	ret    

c0105d30 <printfmt>:
 * @putch:      specified putch function, print a single character
 * @putdat:     used by @putch function
 * @fmt:        the format string to use
 * */
void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...) {
c0105d30:	f3 0f 1e fb          	endbr32 
c0105d34:	55                   	push   %ebp
c0105d35:	89 e5                	mov    %esp,%ebp
c0105d37:	83 ec 28             	sub    $0x28,%esp
    va_list ap;

    va_start(ap, fmt);
c0105d3a:	8d 45 14             	lea    0x14(%ebp),%eax
c0105d3d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    vprintfmt(putch, putdat, fmt, ap);
c0105d40:	8b 45 f4             	mov    -0xc(%ebp),%eax
c0105d43:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0105d47:	8b 45 10             	mov    0x10(%ebp),%eax
c0105d4a:	89 44 24 08          	mov    %eax,0x8(%esp)
c0105d4e:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105d51:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105d55:	8b 45 08             	mov    0x8(%ebp),%eax
c0105d58:	89 04 24             	mov    %eax,(%esp)
c0105d5b:	e8 03 00 00 00       	call   c0105d63 <vprintfmt>
    va_end(ap);
}
c0105d60:	90                   	nop
c0105d61:	c9                   	leave  
c0105d62:	c3                   	ret    

c0105d63 <vprintfmt>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want printfmt() instead.
 * */
void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap) {
c0105d63:	f3 0f 1e fb          	endbr32 
c0105d67:	55                   	push   %ebp
c0105d68:	89 e5                	mov    %esp,%ebp
c0105d6a:	56                   	push   %esi
c0105d6b:	53                   	push   %ebx
c0105d6c:	83 ec 40             	sub    $0x40,%esp
    register int ch, err;
    unsigned long long num;
    int base, width, precision, lflag, altflag;

    while (1) {
        while ((ch = *(unsigned char *)fmt ++) != '%') {
c0105d6f:	eb 17                	jmp    c0105d88 <vprintfmt+0x25>
            if (ch == '\0') {
c0105d71:	85 db                	test   %ebx,%ebx
c0105d73:	0f 84 c0 03 00 00    	je     c0106139 <vprintfmt+0x3d6>
                return;
            }
            putch(ch, putdat);
c0105d79:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105d7c:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105d80:	89 1c 24             	mov    %ebx,(%esp)
c0105d83:	8b 45 08             	mov    0x8(%ebp),%eax
c0105d86:	ff d0                	call   *%eax
        while ((ch = *(unsigned char *)fmt ++) != '%') {
c0105d88:	8b 45 10             	mov    0x10(%ebp),%eax
c0105d8b:	8d 50 01             	lea    0x1(%eax),%edx
c0105d8e:	89 55 10             	mov    %edx,0x10(%ebp)
c0105d91:	0f b6 00             	movzbl (%eax),%eax
c0105d94:	0f b6 d8             	movzbl %al,%ebx
c0105d97:	83 fb 25             	cmp    $0x25,%ebx
c0105d9a:	75 d5                	jne    c0105d71 <vprintfmt+0xe>
        }

        // Process a %-escape sequence
        char padc = ' ';
c0105d9c:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
        width = precision = -1;
c0105da0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
c0105da7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0105daa:	89 45 e8             	mov    %eax,-0x18(%ebp)
        lflag = altflag = 0;
c0105dad:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
c0105db4:	8b 45 dc             	mov    -0x24(%ebp),%eax
c0105db7:	89 45 e0             	mov    %eax,-0x20(%ebp)

    reswitch:
        switch (ch = *(unsigned char *)fmt ++) {
c0105dba:	8b 45 10             	mov    0x10(%ebp),%eax
c0105dbd:	8d 50 01             	lea    0x1(%eax),%edx
c0105dc0:	89 55 10             	mov    %edx,0x10(%ebp)
c0105dc3:	0f b6 00             	movzbl (%eax),%eax
c0105dc6:	0f b6 d8             	movzbl %al,%ebx
c0105dc9:	8d 43 dd             	lea    -0x23(%ebx),%eax
c0105dcc:	83 f8 55             	cmp    $0x55,%eax
c0105dcf:	0f 87 38 03 00 00    	ja     c010610d <vprintfmt+0x3aa>
c0105dd5:	8b 04 85 40 74 10 c0 	mov    -0x3fef8bc0(,%eax,4),%eax
c0105ddc:	3e ff e0             	notrack jmp *%eax

        // flag to pad on the right
        case '-':
            padc = '-';
c0105ddf:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
            goto reswitch;
c0105de3:	eb d5                	jmp    c0105dba <vprintfmt+0x57>

        // flag to pad with 0's instead of spaces
        case '0':
            padc = '0';
c0105de5:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
            goto reswitch;
c0105de9:	eb cf                	jmp    c0105dba <vprintfmt+0x57>

        // width field
        case '1' ... '9':
            for (precision = 0; ; ++ fmt) {
c0105deb:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
                precision = precision * 10 + ch - '0';
c0105df2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
c0105df5:	89 d0                	mov    %edx,%eax
c0105df7:	c1 e0 02             	shl    $0x2,%eax
c0105dfa:	01 d0                	add    %edx,%eax
c0105dfc:	01 c0                	add    %eax,%eax
c0105dfe:	01 d8                	add    %ebx,%eax
c0105e00:	83 e8 30             	sub    $0x30,%eax
c0105e03:	89 45 e4             	mov    %eax,-0x1c(%ebp)
                ch = *fmt;
c0105e06:	8b 45 10             	mov    0x10(%ebp),%eax
c0105e09:	0f b6 00             	movzbl (%eax),%eax
c0105e0c:	0f be d8             	movsbl %al,%ebx
                if (ch < '0' || ch > '9') {
c0105e0f:	83 fb 2f             	cmp    $0x2f,%ebx
c0105e12:	7e 38                	jle    c0105e4c <vprintfmt+0xe9>
c0105e14:	83 fb 39             	cmp    $0x39,%ebx
c0105e17:	7f 33                	jg     c0105e4c <vprintfmt+0xe9>
            for (precision = 0; ; ++ fmt) {
c0105e19:	ff 45 10             	incl   0x10(%ebp)
                precision = precision * 10 + ch - '0';
c0105e1c:	eb d4                	jmp    c0105df2 <vprintfmt+0x8f>
                }
            }
            goto process_precision;

        case '*':
            precision = va_arg(ap, int);
c0105e1e:	8b 45 14             	mov    0x14(%ebp),%eax
c0105e21:	8d 50 04             	lea    0x4(%eax),%edx
c0105e24:	89 55 14             	mov    %edx,0x14(%ebp)
c0105e27:	8b 00                	mov    (%eax),%eax
c0105e29:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            goto process_precision;
c0105e2c:	eb 1f                	jmp    c0105e4d <vprintfmt+0xea>

        case '.':
            if (width < 0)
c0105e2e:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105e32:	79 86                	jns    c0105dba <vprintfmt+0x57>
                width = 0;
c0105e34:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
            goto reswitch;
c0105e3b:	e9 7a ff ff ff       	jmp    c0105dba <vprintfmt+0x57>

        case '#':
            altflag = 1;
c0105e40:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
            goto reswitch;
c0105e47:	e9 6e ff ff ff       	jmp    c0105dba <vprintfmt+0x57>
            goto process_precision;
c0105e4c:	90                   	nop

        process_precision:
            if (width < 0)
c0105e4d:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105e51:	0f 89 63 ff ff ff    	jns    c0105dba <vprintfmt+0x57>
                width = precision, precision = -1;
c0105e57:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0105e5a:	89 45 e8             	mov    %eax,-0x18(%ebp)
c0105e5d:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
            goto reswitch;
c0105e64:	e9 51 ff ff ff       	jmp    c0105dba <vprintfmt+0x57>

        // long flag (doubled for long long)
        case 'l':
            lflag ++;
c0105e69:	ff 45 e0             	incl   -0x20(%ebp)
            goto reswitch;
c0105e6c:	e9 49 ff ff ff       	jmp    c0105dba <vprintfmt+0x57>

        // character
        case 'c':
            putch(va_arg(ap, int), putdat);
c0105e71:	8b 45 14             	mov    0x14(%ebp),%eax
c0105e74:	8d 50 04             	lea    0x4(%eax),%edx
c0105e77:	89 55 14             	mov    %edx,0x14(%ebp)
c0105e7a:	8b 00                	mov    (%eax),%eax
c0105e7c:	8b 55 0c             	mov    0xc(%ebp),%edx
c0105e7f:	89 54 24 04          	mov    %edx,0x4(%esp)
c0105e83:	89 04 24             	mov    %eax,(%esp)
c0105e86:	8b 45 08             	mov    0x8(%ebp),%eax
c0105e89:	ff d0                	call   *%eax
            break;
c0105e8b:	e9 a4 02 00 00       	jmp    c0106134 <vprintfmt+0x3d1>

        // error message
        case 'e':
            err = va_arg(ap, int);
c0105e90:	8b 45 14             	mov    0x14(%ebp),%eax
c0105e93:	8d 50 04             	lea    0x4(%eax),%edx
c0105e96:	89 55 14             	mov    %edx,0x14(%ebp)
c0105e99:	8b 18                	mov    (%eax),%ebx
            if (err < 0) {
c0105e9b:	85 db                	test   %ebx,%ebx
c0105e9d:	79 02                	jns    c0105ea1 <vprintfmt+0x13e>
                err = -err;
c0105e9f:	f7 db                	neg    %ebx
            }
            if (err > MAXERROR || (p = error_string[err]) == NULL) {
c0105ea1:	83 fb 06             	cmp    $0x6,%ebx
c0105ea4:	7f 0b                	jg     c0105eb1 <vprintfmt+0x14e>
c0105ea6:	8b 34 9d 00 74 10 c0 	mov    -0x3fef8c00(,%ebx,4),%esi
c0105ead:	85 f6                	test   %esi,%esi
c0105eaf:	75 23                	jne    c0105ed4 <vprintfmt+0x171>
                printfmt(putch, putdat, "error %d", err);
c0105eb1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
c0105eb5:	c7 44 24 08 2d 74 10 	movl   $0xc010742d,0x8(%esp)
c0105ebc:	c0 
c0105ebd:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105ec0:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105ec4:	8b 45 08             	mov    0x8(%ebp),%eax
c0105ec7:	89 04 24             	mov    %eax,(%esp)
c0105eca:	e8 61 fe ff ff       	call   c0105d30 <printfmt>
            }
            else {
                printfmt(putch, putdat, "%s", p);
            }
            break;
c0105ecf:	e9 60 02 00 00       	jmp    c0106134 <vprintfmt+0x3d1>
                printfmt(putch, putdat, "%s", p);
c0105ed4:	89 74 24 0c          	mov    %esi,0xc(%esp)
c0105ed8:	c7 44 24 08 36 74 10 	movl   $0xc0107436,0x8(%esp)
c0105edf:	c0 
c0105ee0:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105ee3:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105ee7:	8b 45 08             	mov    0x8(%ebp),%eax
c0105eea:	89 04 24             	mov    %eax,(%esp)
c0105eed:	e8 3e fe ff ff       	call   c0105d30 <printfmt>
            break;
c0105ef2:	e9 3d 02 00 00       	jmp    c0106134 <vprintfmt+0x3d1>

        // string
        case 's':
            if ((p = va_arg(ap, char *)) == NULL) {
c0105ef7:	8b 45 14             	mov    0x14(%ebp),%eax
c0105efa:	8d 50 04             	lea    0x4(%eax),%edx
c0105efd:	89 55 14             	mov    %edx,0x14(%ebp)
c0105f00:	8b 30                	mov    (%eax),%esi
c0105f02:	85 f6                	test   %esi,%esi
c0105f04:	75 05                	jne    c0105f0b <vprintfmt+0x1a8>
                p = "(null)";
c0105f06:	be 39 74 10 c0       	mov    $0xc0107439,%esi
            }
            if (width > 0 && padc != '-') {
c0105f0b:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105f0f:	7e 76                	jle    c0105f87 <vprintfmt+0x224>
c0105f11:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
c0105f15:	74 70                	je     c0105f87 <vprintfmt+0x224>
                for (width -= strnlen(p, precision); width > 0; width --) {
c0105f17:	8b 45 e4             	mov    -0x1c(%ebp),%eax
c0105f1a:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105f1e:	89 34 24             	mov    %esi,(%esp)
c0105f21:	e8 ba f7 ff ff       	call   c01056e0 <strnlen>
c0105f26:	8b 55 e8             	mov    -0x18(%ebp),%edx
c0105f29:	29 c2                	sub    %eax,%edx
c0105f2b:	89 d0                	mov    %edx,%eax
c0105f2d:	89 45 e8             	mov    %eax,-0x18(%ebp)
c0105f30:	eb 16                	jmp    c0105f48 <vprintfmt+0x1e5>
                    putch(padc, putdat);
c0105f32:	0f be 45 db          	movsbl -0x25(%ebp),%eax
c0105f36:	8b 55 0c             	mov    0xc(%ebp),%edx
c0105f39:	89 54 24 04          	mov    %edx,0x4(%esp)
c0105f3d:	89 04 24             	mov    %eax,(%esp)
c0105f40:	8b 45 08             	mov    0x8(%ebp),%eax
c0105f43:	ff d0                	call   *%eax
                for (width -= strnlen(p, precision); width > 0; width --) {
c0105f45:	ff 4d e8             	decl   -0x18(%ebp)
c0105f48:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105f4c:	7f e4                	jg     c0105f32 <vprintfmt+0x1cf>
                }
            }
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
c0105f4e:	eb 37                	jmp    c0105f87 <vprintfmt+0x224>
                if (altflag && (ch < ' ' || ch > '~')) {
c0105f50:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
c0105f54:	74 1f                	je     c0105f75 <vprintfmt+0x212>
c0105f56:	83 fb 1f             	cmp    $0x1f,%ebx
c0105f59:	7e 05                	jle    c0105f60 <vprintfmt+0x1fd>
c0105f5b:	83 fb 7e             	cmp    $0x7e,%ebx
c0105f5e:	7e 15                	jle    c0105f75 <vprintfmt+0x212>
                    putch('?', putdat);
c0105f60:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105f63:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105f67:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
c0105f6e:	8b 45 08             	mov    0x8(%ebp),%eax
c0105f71:	ff d0                	call   *%eax
c0105f73:	eb 0f                	jmp    c0105f84 <vprintfmt+0x221>
                }
                else {
                    putch(ch, putdat);
c0105f75:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105f78:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105f7c:	89 1c 24             	mov    %ebx,(%esp)
c0105f7f:	8b 45 08             	mov    0x8(%ebp),%eax
c0105f82:	ff d0                	call   *%eax
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
c0105f84:	ff 4d e8             	decl   -0x18(%ebp)
c0105f87:	89 f0                	mov    %esi,%eax
c0105f89:	8d 70 01             	lea    0x1(%eax),%esi
c0105f8c:	0f b6 00             	movzbl (%eax),%eax
c0105f8f:	0f be d8             	movsbl %al,%ebx
c0105f92:	85 db                	test   %ebx,%ebx
c0105f94:	74 27                	je     c0105fbd <vprintfmt+0x25a>
c0105f96:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
c0105f9a:	78 b4                	js     c0105f50 <vprintfmt+0x1ed>
c0105f9c:	ff 4d e4             	decl   -0x1c(%ebp)
c0105f9f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
c0105fa3:	79 ab                	jns    c0105f50 <vprintfmt+0x1ed>
                }
            }
            for (; width > 0; width --) {
c0105fa5:	eb 16                	jmp    c0105fbd <vprintfmt+0x25a>
                putch(' ', putdat);
c0105fa7:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105faa:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105fae:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
c0105fb5:	8b 45 08             	mov    0x8(%ebp),%eax
c0105fb8:	ff d0                	call   *%eax
            for (; width > 0; width --) {
c0105fba:	ff 4d e8             	decl   -0x18(%ebp)
c0105fbd:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
c0105fc1:	7f e4                	jg     c0105fa7 <vprintfmt+0x244>
            }
            break;
c0105fc3:	e9 6c 01 00 00       	jmp    c0106134 <vprintfmt+0x3d1>

        // (signed) decimal
        case 'd':
            num = getint(&ap, lflag);
c0105fc8:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0105fcb:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105fcf:	8d 45 14             	lea    0x14(%ebp),%eax
c0105fd2:	89 04 24             	mov    %eax,(%esp)
c0105fd5:	e8 0b fd ff ff       	call   c0105ce5 <getint>
c0105fda:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0105fdd:	89 55 f4             	mov    %edx,-0xc(%ebp)
            if ((long long)num < 0) {
c0105fe0:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0105fe3:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0105fe6:	85 d2                	test   %edx,%edx
c0105fe8:	79 26                	jns    c0106010 <vprintfmt+0x2ad>
                putch('-', putdat);
c0105fea:	8b 45 0c             	mov    0xc(%ebp),%eax
c0105fed:	89 44 24 04          	mov    %eax,0x4(%esp)
c0105ff1:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
c0105ff8:	8b 45 08             	mov    0x8(%ebp),%eax
c0105ffb:	ff d0                	call   *%eax
                num = -(long long)num;
c0105ffd:	8b 45 f0             	mov    -0x10(%ebp),%eax
c0106000:	8b 55 f4             	mov    -0xc(%ebp),%edx
c0106003:	f7 d8                	neg    %eax
c0106005:	83 d2 00             	adc    $0x0,%edx
c0106008:	f7 da                	neg    %edx
c010600a:	89 45 f0             	mov    %eax,-0x10(%ebp)
c010600d:	89 55 f4             	mov    %edx,-0xc(%ebp)
            }
            base = 10;
c0106010:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
            goto number;
c0106017:	e9 a8 00 00 00       	jmp    c01060c4 <vprintfmt+0x361>

        // unsigned decimal
        case 'u':
            num = getuint(&ap, lflag);
c010601c:	8b 45 e0             	mov    -0x20(%ebp),%eax
c010601f:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106023:	8d 45 14             	lea    0x14(%ebp),%eax
c0106026:	89 04 24             	mov    %eax,(%esp)
c0106029:	e8 64 fc ff ff       	call   c0105c92 <getuint>
c010602e:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0106031:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 10;
c0106034:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
            goto number;
c010603b:	e9 84 00 00 00       	jmp    c01060c4 <vprintfmt+0x361>

        // (unsigned) octal
        case 'o':
            num = getuint(&ap, lflag);
c0106040:	8b 45 e0             	mov    -0x20(%ebp),%eax
c0106043:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106047:	8d 45 14             	lea    0x14(%ebp),%eax
c010604a:	89 04 24             	mov    %eax,(%esp)
c010604d:	e8 40 fc ff ff       	call   c0105c92 <getuint>
c0106052:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0106055:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 8;
c0106058:	c7 45 ec 08 00 00 00 	movl   $0x8,-0x14(%ebp)
            goto number;
c010605f:	eb 63                	jmp    c01060c4 <vprintfmt+0x361>

        // pointer
        case 'p':
            putch('0', putdat);
c0106061:	8b 45 0c             	mov    0xc(%ebp),%eax
c0106064:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106068:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
c010606f:	8b 45 08             	mov    0x8(%ebp),%eax
c0106072:	ff d0                	call   *%eax
            putch('x', putdat);
c0106074:	8b 45 0c             	mov    0xc(%ebp),%eax
c0106077:	89 44 24 04          	mov    %eax,0x4(%esp)
c010607b:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
c0106082:	8b 45 08             	mov    0x8(%ebp),%eax
c0106085:	ff d0                	call   *%eax
            num = (unsigned long long)(uintptr_t)va_arg(ap, void *);
c0106087:	8b 45 14             	mov    0x14(%ebp),%eax
c010608a:	8d 50 04             	lea    0x4(%eax),%edx
c010608d:	89 55 14             	mov    %edx,0x14(%ebp)
c0106090:	8b 00                	mov    (%eax),%eax
c0106092:	89 45 f0             	mov    %eax,-0x10(%ebp)
c0106095:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
            base = 16;
c010609c:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
            goto number;
c01060a3:	eb 1f                	jmp    c01060c4 <vprintfmt+0x361>

        // (unsigned) hexadecimal
        case 'x':
            num = getuint(&ap, lflag);
c01060a5:	8b 45 e0             	mov    -0x20(%ebp),%eax
c01060a8:	89 44 24 04          	mov    %eax,0x4(%esp)
c01060ac:	8d 45 14             	lea    0x14(%ebp),%eax
c01060af:	89 04 24             	mov    %eax,(%esp)
c01060b2:	e8 db fb ff ff       	call   c0105c92 <getuint>
c01060b7:	89 45 f0             	mov    %eax,-0x10(%ebp)
c01060ba:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 16;
c01060bd:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
        number:
            printnum(putch, putdat, num, base, width, padc);
c01060c4:	0f be 55 db          	movsbl -0x25(%ebp),%edx
c01060c8:	8b 45 ec             	mov    -0x14(%ebp),%eax
c01060cb:	89 54 24 18          	mov    %edx,0x18(%esp)
c01060cf:	8b 55 e8             	mov    -0x18(%ebp),%edx
c01060d2:	89 54 24 14          	mov    %edx,0x14(%esp)
c01060d6:	89 44 24 10          	mov    %eax,0x10(%esp)
c01060da:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01060dd:	8b 55 f4             	mov    -0xc(%ebp),%edx
c01060e0:	89 44 24 08          	mov    %eax,0x8(%esp)
c01060e4:	89 54 24 0c          	mov    %edx,0xc(%esp)
c01060e8:	8b 45 0c             	mov    0xc(%ebp),%eax
c01060eb:	89 44 24 04          	mov    %eax,0x4(%esp)
c01060ef:	8b 45 08             	mov    0x8(%ebp),%eax
c01060f2:	89 04 24             	mov    %eax,(%esp)
c01060f5:	e8 94 fa ff ff       	call   c0105b8e <printnum>
            break;
c01060fa:	eb 38                	jmp    c0106134 <vprintfmt+0x3d1>

        // escaped '%' character
        case '%':
            putch(ch, putdat);
c01060fc:	8b 45 0c             	mov    0xc(%ebp),%eax
c01060ff:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106103:	89 1c 24             	mov    %ebx,(%esp)
c0106106:	8b 45 08             	mov    0x8(%ebp),%eax
c0106109:	ff d0                	call   *%eax
            break;
c010610b:	eb 27                	jmp    c0106134 <vprintfmt+0x3d1>

        // unrecognized escape sequence - just print it literally
        default:
            putch('%', putdat);
c010610d:	8b 45 0c             	mov    0xc(%ebp),%eax
c0106110:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106114:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
c010611b:	8b 45 08             	mov    0x8(%ebp),%eax
c010611e:	ff d0                	call   *%eax
            for (fmt --; fmt[-1] != '%'; fmt --)
c0106120:	ff 4d 10             	decl   0x10(%ebp)
c0106123:	eb 03                	jmp    c0106128 <vprintfmt+0x3c5>
c0106125:	ff 4d 10             	decl   0x10(%ebp)
c0106128:	8b 45 10             	mov    0x10(%ebp),%eax
c010612b:	48                   	dec    %eax
c010612c:	0f b6 00             	movzbl (%eax),%eax
c010612f:	3c 25                	cmp    $0x25,%al
c0106131:	75 f2                	jne    c0106125 <vprintfmt+0x3c2>
                /* do nothing */;
            break;
c0106133:	90                   	nop
    while (1) {
c0106134:	e9 36 fc ff ff       	jmp    c0105d6f <vprintfmt+0xc>
                return;
c0106139:	90                   	nop
        }
    }
}
c010613a:	83 c4 40             	add    $0x40,%esp
c010613d:	5b                   	pop    %ebx
c010613e:	5e                   	pop    %esi
c010613f:	5d                   	pop    %ebp
c0106140:	c3                   	ret    

c0106141 <sprintputch>:
 * sprintputch - 'print' a single character in a buffer
 * @ch:         the character will be printed
 * @b:          the buffer to place the character @ch
 * */
static void
sprintputch(int ch, struct sprintbuf *b) {
c0106141:	f3 0f 1e fb          	endbr32 
c0106145:	55                   	push   %ebp
c0106146:	89 e5                	mov    %esp,%ebp
    b->cnt ++;
c0106148:	8b 45 0c             	mov    0xc(%ebp),%eax
c010614b:	8b 40 08             	mov    0x8(%eax),%eax
c010614e:	8d 50 01             	lea    0x1(%eax),%edx
c0106151:	8b 45 0c             	mov    0xc(%ebp),%eax
c0106154:	89 50 08             	mov    %edx,0x8(%eax)
    if (b->buf < b->ebuf) {
c0106157:	8b 45 0c             	mov    0xc(%ebp),%eax
c010615a:	8b 10                	mov    (%eax),%edx
c010615c:	8b 45 0c             	mov    0xc(%ebp),%eax
c010615f:	8b 40 04             	mov    0x4(%eax),%eax
c0106162:	39 c2                	cmp    %eax,%edx
c0106164:	73 12                	jae    c0106178 <sprintputch+0x37>
        *b->buf ++ = ch;
c0106166:	8b 45 0c             	mov    0xc(%ebp),%eax
c0106169:	8b 00                	mov    (%eax),%eax
c010616b:	8d 48 01             	lea    0x1(%eax),%ecx
c010616e:	8b 55 0c             	mov    0xc(%ebp),%edx
c0106171:	89 0a                	mov    %ecx,(%edx)
c0106173:	8b 55 08             	mov    0x8(%ebp),%edx
c0106176:	88 10                	mov    %dl,(%eax)
    }
}
c0106178:	90                   	nop
c0106179:	5d                   	pop    %ebp
c010617a:	c3                   	ret    

c010617b <snprintf>:
 * @str:        the buffer to place the result into
 * @size:       the size of buffer, including the trailing null space
 * @fmt:        the format string to use
 * */
int
snprintf(char *str, size_t size, const char *fmt, ...) {
c010617b:	f3 0f 1e fb          	endbr32 
c010617f:	55                   	push   %ebp
c0106180:	89 e5                	mov    %esp,%ebp
c0106182:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    int cnt;
    va_start(ap, fmt);
c0106185:	8d 45 14             	lea    0x14(%ebp),%eax
c0106188:	89 45 f0             	mov    %eax,-0x10(%ebp)
    cnt = vsnprintf(str, size, fmt, ap);
c010618b:	8b 45 f0             	mov    -0x10(%ebp),%eax
c010618e:	89 44 24 0c          	mov    %eax,0xc(%esp)
c0106192:	8b 45 10             	mov    0x10(%ebp),%eax
c0106195:	89 44 24 08          	mov    %eax,0x8(%esp)
c0106199:	8b 45 0c             	mov    0xc(%ebp),%eax
c010619c:	89 44 24 04          	mov    %eax,0x4(%esp)
c01061a0:	8b 45 08             	mov    0x8(%ebp),%eax
c01061a3:	89 04 24             	mov    %eax,(%esp)
c01061a6:	e8 08 00 00 00       	call   c01061b3 <vsnprintf>
c01061ab:	89 45 f4             	mov    %eax,-0xc(%ebp)
    va_end(ap);
    return cnt;
c01061ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c01061b1:	c9                   	leave  
c01061b2:	c3                   	ret    

c01061b3 <vsnprintf>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want snprintf() instead.
 * */
int
vsnprintf(char *str, size_t size, const char *fmt, va_list ap) {
c01061b3:	f3 0f 1e fb          	endbr32 
c01061b7:	55                   	push   %ebp
c01061b8:	89 e5                	mov    %esp,%ebp
c01061ba:	83 ec 28             	sub    $0x28,%esp
    struct sprintbuf b = {str, str + size - 1, 0};
c01061bd:	8b 45 08             	mov    0x8(%ebp),%eax
c01061c0:	89 45 ec             	mov    %eax,-0x14(%ebp)
c01061c3:	8b 45 0c             	mov    0xc(%ebp),%eax
c01061c6:	8d 50 ff             	lea    -0x1(%eax),%edx
c01061c9:	8b 45 08             	mov    0x8(%ebp),%eax
c01061cc:	01 d0                	add    %edx,%eax
c01061ce:	89 45 f0             	mov    %eax,-0x10(%ebp)
c01061d1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if (str == NULL || b.buf > b.ebuf) {
c01061d8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
c01061dc:	74 0a                	je     c01061e8 <vsnprintf+0x35>
c01061de:	8b 55 ec             	mov    -0x14(%ebp),%edx
c01061e1:	8b 45 f0             	mov    -0x10(%ebp),%eax
c01061e4:	39 c2                	cmp    %eax,%edx
c01061e6:	76 07                	jbe    c01061ef <vsnprintf+0x3c>
        return -E_INVAL;
c01061e8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
c01061ed:	eb 2a                	jmp    c0106219 <vsnprintf+0x66>
    }
    // print the string to the buffer
    vprintfmt((void*)sprintputch, &b, fmt, ap);
c01061ef:	8b 45 14             	mov    0x14(%ebp),%eax
c01061f2:	89 44 24 0c          	mov    %eax,0xc(%esp)
c01061f6:	8b 45 10             	mov    0x10(%ebp),%eax
c01061f9:	89 44 24 08          	mov    %eax,0x8(%esp)
c01061fd:	8d 45 ec             	lea    -0x14(%ebp),%eax
c0106200:	89 44 24 04          	mov    %eax,0x4(%esp)
c0106204:	c7 04 24 41 61 10 c0 	movl   $0xc0106141,(%esp)
c010620b:	e8 53 fb ff ff       	call   c0105d63 <vprintfmt>
    // null terminate the buffer
    *b.buf = '\0';
c0106210:	8b 45 ec             	mov    -0x14(%ebp),%eax
c0106213:	c6 00 00             	movb   $0x0,(%eax)
    return b.cnt;
c0106216:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
c0106219:	c9                   	leave  
c010621a:	c3                   	ret    
