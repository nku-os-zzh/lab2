
bin/kernel_nopage:     file format elf32-i386


Disassembly of section .text:

00100000 <kern_entry>:

.text
.globl kern_entry
kern_entry:
    # load pa of boot pgdir
    movl $REALLOC(__boot_pgdir), %eax
  100000:	b8 00 a0 11 40       	mov    $0x4011a000,%eax
    movl %eax, %cr3
  100005:	0f 22 d8             	mov    %eax,%cr3

    # enable paging
    movl %cr0, %eax
  100008:	0f 20 c0             	mov    %cr0,%eax
    orl $(CR0_PE | CR0_PG | CR0_AM | CR0_WP | CR0_NE | CR0_TS | CR0_EM | CR0_MP), %eax
  10000b:	0d 2f 00 05 80       	or     $0x8005002f,%eax
    andl $~(CR0_TS | CR0_EM), %eax
  100010:	83 e0 f3             	and    $0xfffffff3,%eax
    movl %eax, %cr0
  100013:	0f 22 c0             	mov    %eax,%cr0

    # update eip
    # now, eip = 0x1.....
    leal next, %eax
  100016:	8d 05 1e 00 10 00    	lea    0x10001e,%eax
    # set eip = KERNBASE + 0x1.....
    jmp *%eax
  10001c:	ff e0                	jmp    *%eax

0010001e <next>:
next:

    # unmap va 0 ~ 4M, it's temporary mapping
    xorl %eax, %eax
  10001e:	31 c0                	xor    %eax,%eax
    movl %eax, __boot_pgdir
  100020:	a3 00 a0 11 00       	mov    %eax,0x11a000

    # set ebp, esp
    movl $0x0, %ebp
  100025:	bd 00 00 00 00       	mov    $0x0,%ebp
    # the kernel stack region is from bootstack -- bootstacktop,
    # the kernel stack size is KSTACKSIZE (8KB)defined in memlayout.h
    movl $bootstacktop, %esp
  10002a:	bc 00 90 11 00       	mov    $0x119000,%esp
    # now kernel stack is ready , call the first C function
    call kern_init
  10002f:	e8 02 00 00 00       	call   100036 <kern_init>

00100034 <spin>:

# should never get here
spin:
    jmp spin
  100034:	eb fe                	jmp    100034 <spin>

00100036 <kern_init>:
int kern_init(void) __attribute__((noreturn));
void grade_backtrace(void);
static void lab1_switch_test(void);

int
kern_init(void) {
  100036:	f3 0f 1e fb          	endbr32 
  10003a:	55                   	push   %ebp
  10003b:	89 e5                	mov    %esp,%ebp
  10003d:	83 ec 28             	sub    $0x28,%esp
    extern char edata[], end[];
    memset(edata, 0, end - edata);
  100040:	b8 28 cf 11 00       	mov    $0x11cf28,%eax
  100045:	2d 36 9a 11 00       	sub    $0x119a36,%eax
  10004a:	89 44 24 08          	mov    %eax,0x8(%esp)
  10004e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  100055:	00 
  100056:	c7 04 24 36 9a 11 00 	movl   $0x119a36,(%esp)
  10005d:	e8 95 59 00 00       	call   1059f7 <memset>

    cons_init();                // init the console
  100062:	e8 4f 16 00 00       	call   1016b6 <cons_init>

    const char *message = "(THU.CST) os is loading ...";
  100067:	c7 45 f4 20 62 10 00 	movl   $0x106220,-0xc(%ebp)
    cprintf("%s\n\n", message);
  10006e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100071:	89 44 24 04          	mov    %eax,0x4(%esp)
  100075:	c7 04 24 3c 62 10 00 	movl   $0x10623c,(%esp)
  10007c:	e8 39 02 00 00       	call   1002ba <cprintf>

    print_kerninfo();
  100081:	e8 f7 08 00 00       	call   10097d <print_kerninfo>

    grade_backtrace();
  100086:	e8 95 00 00 00       	call   100120 <grade_backtrace>

    pmm_init();                 // init physical memory management
  10008b:	e8 66 32 00 00       	call   1032f6 <pmm_init>

    pic_init();                 // init interrupt controller
  100090:	e8 9c 17 00 00       	call   101831 <pic_init>
    idt_init();                 // init interrupt descriptor table
  100095:	e8 41 19 00 00       	call   1019db <idt_init>

    clock_init();               // init clock interrupt
  10009a:	e8 5e 0d 00 00       	call   100dfd <clock_init>
    intr_enable();              // enable irq interrupt
  10009f:	e8 d9 18 00 00       	call   10197d <intr_enable>
    //LAB1: CAHLLENGE 1 If you try to do it, uncomment lab1_switch_test()
    // user/kernel mode switch test
    //lab1_switch_test();

    /* do nothing */
    while (1);
  1000a4:	eb fe                	jmp    1000a4 <kern_init+0x6e>

001000a6 <grade_backtrace2>:
}

void __attribute__((noinline))
grade_backtrace2(int arg0, int arg1, int arg2, int arg3) {
  1000a6:	f3 0f 1e fb          	endbr32 
  1000aa:	55                   	push   %ebp
  1000ab:	89 e5                	mov    %esp,%ebp
  1000ad:	83 ec 18             	sub    $0x18,%esp
    mon_backtrace(0, NULL, NULL);
  1000b0:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1000b7:	00 
  1000b8:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1000bf:	00 
  1000c0:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1000c7:	e8 1b 0d 00 00       	call   100de7 <mon_backtrace>
}
  1000cc:	90                   	nop
  1000cd:	c9                   	leave  
  1000ce:	c3                   	ret    

001000cf <grade_backtrace1>:

void __attribute__((noinline))
grade_backtrace1(int arg0, int arg1) {
  1000cf:	f3 0f 1e fb          	endbr32 
  1000d3:	55                   	push   %ebp
  1000d4:	89 e5                	mov    %esp,%ebp
  1000d6:	53                   	push   %ebx
  1000d7:	83 ec 14             	sub    $0x14,%esp
    grade_backtrace2(arg0, (int)&arg0, arg1, (int)&arg1);
  1000da:	8d 4d 0c             	lea    0xc(%ebp),%ecx
  1000dd:	8b 55 0c             	mov    0xc(%ebp),%edx
  1000e0:	8d 5d 08             	lea    0x8(%ebp),%ebx
  1000e3:	8b 45 08             	mov    0x8(%ebp),%eax
  1000e6:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  1000ea:	89 54 24 08          	mov    %edx,0x8(%esp)
  1000ee:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  1000f2:	89 04 24             	mov    %eax,(%esp)
  1000f5:	e8 ac ff ff ff       	call   1000a6 <grade_backtrace2>
}
  1000fa:	90                   	nop
  1000fb:	83 c4 14             	add    $0x14,%esp
  1000fe:	5b                   	pop    %ebx
  1000ff:	5d                   	pop    %ebp
  100100:	c3                   	ret    

00100101 <grade_backtrace0>:

void __attribute__((noinline))
grade_backtrace0(int arg0, int arg1, int arg2) {
  100101:	f3 0f 1e fb          	endbr32 
  100105:	55                   	push   %ebp
  100106:	89 e5                	mov    %esp,%ebp
  100108:	83 ec 18             	sub    $0x18,%esp
    grade_backtrace1(arg0, arg2);
  10010b:	8b 45 10             	mov    0x10(%ebp),%eax
  10010e:	89 44 24 04          	mov    %eax,0x4(%esp)
  100112:	8b 45 08             	mov    0x8(%ebp),%eax
  100115:	89 04 24             	mov    %eax,(%esp)
  100118:	e8 b2 ff ff ff       	call   1000cf <grade_backtrace1>
}
  10011d:	90                   	nop
  10011e:	c9                   	leave  
  10011f:	c3                   	ret    

00100120 <grade_backtrace>:

void
grade_backtrace(void) {
  100120:	f3 0f 1e fb          	endbr32 
  100124:	55                   	push   %ebp
  100125:	89 e5                	mov    %esp,%ebp
  100127:	83 ec 18             	sub    $0x18,%esp
    grade_backtrace0(0, (int)kern_init, 0xffff0000);
  10012a:	b8 36 00 10 00       	mov    $0x100036,%eax
  10012f:	c7 44 24 08 00 00 ff 	movl   $0xffff0000,0x8(%esp)
  100136:	ff 
  100137:	89 44 24 04          	mov    %eax,0x4(%esp)
  10013b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  100142:	e8 ba ff ff ff       	call   100101 <grade_backtrace0>
}
  100147:	90                   	nop
  100148:	c9                   	leave  
  100149:	c3                   	ret    

0010014a <lab1_print_cur_status>:

static void
lab1_print_cur_status(void) {
  10014a:	f3 0f 1e fb          	endbr32 
  10014e:	55                   	push   %ebp
  10014f:	89 e5                	mov    %esp,%ebp
  100151:	83 ec 28             	sub    $0x28,%esp
    static int round = 0;
    uint16_t reg1, reg2, reg3, reg4;
    asm volatile (
  100154:	8c 4d f6             	mov    %cs,-0xa(%ebp)
  100157:	8c 5d f4             	mov    %ds,-0xc(%ebp)
  10015a:	8c 45 f2             	mov    %es,-0xe(%ebp)
  10015d:	8c 55 f0             	mov    %ss,-0x10(%ebp)
            "mov %%cs, %0;"
            "mov %%ds, %1;"
            "mov %%es, %2;"
            "mov %%ss, %3;"
            : "=m"(reg1), "=m"(reg2), "=m"(reg3), "=m"(reg4));
    cprintf("%d: @ring %d\n", round, reg1 & 3);
  100160:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
  100164:	83 e0 03             	and    $0x3,%eax
  100167:	89 c2                	mov    %eax,%edx
  100169:	a1 00 c0 11 00       	mov    0x11c000,%eax
  10016e:	89 54 24 08          	mov    %edx,0x8(%esp)
  100172:	89 44 24 04          	mov    %eax,0x4(%esp)
  100176:	c7 04 24 41 62 10 00 	movl   $0x106241,(%esp)
  10017d:	e8 38 01 00 00       	call   1002ba <cprintf>
    cprintf("%d:  cs = %x\n", round, reg1);
  100182:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
  100186:	89 c2                	mov    %eax,%edx
  100188:	a1 00 c0 11 00       	mov    0x11c000,%eax
  10018d:	89 54 24 08          	mov    %edx,0x8(%esp)
  100191:	89 44 24 04          	mov    %eax,0x4(%esp)
  100195:	c7 04 24 4f 62 10 00 	movl   $0x10624f,(%esp)
  10019c:	e8 19 01 00 00       	call   1002ba <cprintf>
    cprintf("%d:  ds = %x\n", round, reg2);
  1001a1:	0f b7 45 f4          	movzwl -0xc(%ebp),%eax
  1001a5:	89 c2                	mov    %eax,%edx
  1001a7:	a1 00 c0 11 00       	mov    0x11c000,%eax
  1001ac:	89 54 24 08          	mov    %edx,0x8(%esp)
  1001b0:	89 44 24 04          	mov    %eax,0x4(%esp)
  1001b4:	c7 04 24 5d 62 10 00 	movl   $0x10625d,(%esp)
  1001bb:	e8 fa 00 00 00       	call   1002ba <cprintf>
    cprintf("%d:  es = %x\n", round, reg3);
  1001c0:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
  1001c4:	89 c2                	mov    %eax,%edx
  1001c6:	a1 00 c0 11 00       	mov    0x11c000,%eax
  1001cb:	89 54 24 08          	mov    %edx,0x8(%esp)
  1001cf:	89 44 24 04          	mov    %eax,0x4(%esp)
  1001d3:	c7 04 24 6b 62 10 00 	movl   $0x10626b,(%esp)
  1001da:	e8 db 00 00 00       	call   1002ba <cprintf>
    cprintf("%d:  ss = %x\n", round, reg4);
  1001df:	0f b7 45 f0          	movzwl -0x10(%ebp),%eax
  1001e3:	89 c2                	mov    %eax,%edx
  1001e5:	a1 00 c0 11 00       	mov    0x11c000,%eax
  1001ea:	89 54 24 08          	mov    %edx,0x8(%esp)
  1001ee:	89 44 24 04          	mov    %eax,0x4(%esp)
  1001f2:	c7 04 24 79 62 10 00 	movl   $0x106279,(%esp)
  1001f9:	e8 bc 00 00 00       	call   1002ba <cprintf>
    round ++;
  1001fe:	a1 00 c0 11 00       	mov    0x11c000,%eax
  100203:	40                   	inc    %eax
  100204:	a3 00 c0 11 00       	mov    %eax,0x11c000
}
  100209:	90                   	nop
  10020a:	c9                   	leave  
  10020b:	c3                   	ret    

0010020c <lab1_switch_to_user>:

static void
lab1_switch_to_user(void) {
  10020c:	f3 0f 1e fb          	endbr32 
  100210:	55                   	push   %ebp
  100211:	89 e5                	mov    %esp,%ebp
    //LAB1 CHALLENGE 1 : TODO
}
  100213:	90                   	nop
  100214:	5d                   	pop    %ebp
  100215:	c3                   	ret    

00100216 <lab1_switch_to_kernel>:

static void
lab1_switch_to_kernel(void) {
  100216:	f3 0f 1e fb          	endbr32 
  10021a:	55                   	push   %ebp
  10021b:	89 e5                	mov    %esp,%ebp
    //LAB1 CHALLENGE 1 :  TODO
}
  10021d:	90                   	nop
  10021e:	5d                   	pop    %ebp
  10021f:	c3                   	ret    

00100220 <lab1_switch_test>:

static void
lab1_switch_test(void) {
  100220:	f3 0f 1e fb          	endbr32 
  100224:	55                   	push   %ebp
  100225:	89 e5                	mov    %esp,%ebp
  100227:	83 ec 18             	sub    $0x18,%esp
    lab1_print_cur_status();
  10022a:	e8 1b ff ff ff       	call   10014a <lab1_print_cur_status>
    cprintf("+++ switch to  user  mode +++\n");
  10022f:	c7 04 24 88 62 10 00 	movl   $0x106288,(%esp)
  100236:	e8 7f 00 00 00       	call   1002ba <cprintf>
    lab1_switch_to_user();
  10023b:	e8 cc ff ff ff       	call   10020c <lab1_switch_to_user>
    lab1_print_cur_status();
  100240:	e8 05 ff ff ff       	call   10014a <lab1_print_cur_status>
    cprintf("+++ switch to kernel mode +++\n");
  100245:	c7 04 24 a8 62 10 00 	movl   $0x1062a8,(%esp)
  10024c:	e8 69 00 00 00       	call   1002ba <cprintf>
    lab1_switch_to_kernel();
  100251:	e8 c0 ff ff ff       	call   100216 <lab1_switch_to_kernel>
    lab1_print_cur_status();
  100256:	e8 ef fe ff ff       	call   10014a <lab1_print_cur_status>
}
  10025b:	90                   	nop
  10025c:	c9                   	leave  
  10025d:	c3                   	ret    

0010025e <cputch>:
/* *
 * cputch - writes a single character @c to stdout, and it will
 * increace the value of counter pointed by @cnt.
 * */
static void
cputch(int c, int *cnt) {
  10025e:	f3 0f 1e fb          	endbr32 
  100262:	55                   	push   %ebp
  100263:	89 e5                	mov    %esp,%ebp
  100265:	83 ec 18             	sub    $0x18,%esp
    cons_putc(c);
  100268:	8b 45 08             	mov    0x8(%ebp),%eax
  10026b:	89 04 24             	mov    %eax,(%esp)
  10026e:	e8 74 14 00 00       	call   1016e7 <cons_putc>
    (*cnt) ++;
  100273:	8b 45 0c             	mov    0xc(%ebp),%eax
  100276:	8b 00                	mov    (%eax),%eax
  100278:	8d 50 01             	lea    0x1(%eax),%edx
  10027b:	8b 45 0c             	mov    0xc(%ebp),%eax
  10027e:	89 10                	mov    %edx,(%eax)
}
  100280:	90                   	nop
  100281:	c9                   	leave  
  100282:	c3                   	ret    

00100283 <vcprintf>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want cprintf() instead.
 * */
int
vcprintf(const char *fmt, va_list ap) {
  100283:	f3 0f 1e fb          	endbr32 
  100287:	55                   	push   %ebp
  100288:	89 e5                	mov    %esp,%ebp
  10028a:	83 ec 28             	sub    $0x28,%esp
    int cnt = 0;
  10028d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    vprintfmt((void*)cputch, &cnt, fmt, ap);
  100294:	8b 45 0c             	mov    0xc(%ebp),%eax
  100297:	89 44 24 0c          	mov    %eax,0xc(%esp)
  10029b:	8b 45 08             	mov    0x8(%ebp),%eax
  10029e:	89 44 24 08          	mov    %eax,0x8(%esp)
  1002a2:	8d 45 f4             	lea    -0xc(%ebp),%eax
  1002a5:	89 44 24 04          	mov    %eax,0x4(%esp)
  1002a9:	c7 04 24 5e 02 10 00 	movl   $0x10025e,(%esp)
  1002b0:	e8 ae 5a 00 00       	call   105d63 <vprintfmt>
    return cnt;
  1002b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  1002b8:	c9                   	leave  
  1002b9:	c3                   	ret    

001002ba <cprintf>:
 *
 * The return value is the number of characters which would be
 * written to stdout.
 * */
int
cprintf(const char *fmt, ...) {
  1002ba:	f3 0f 1e fb          	endbr32 
  1002be:	55                   	push   %ebp
  1002bf:	89 e5                	mov    %esp,%ebp
  1002c1:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    int cnt;
    va_start(ap, fmt);
  1002c4:	8d 45 0c             	lea    0xc(%ebp),%eax
  1002c7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    cnt = vcprintf(fmt, ap);
  1002ca:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1002cd:	89 44 24 04          	mov    %eax,0x4(%esp)
  1002d1:	8b 45 08             	mov    0x8(%ebp),%eax
  1002d4:	89 04 24             	mov    %eax,(%esp)
  1002d7:	e8 a7 ff ff ff       	call   100283 <vcprintf>
  1002dc:	89 45 f4             	mov    %eax,-0xc(%ebp)
    va_end(ap);
    return cnt;
  1002df:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  1002e2:	c9                   	leave  
  1002e3:	c3                   	ret    

001002e4 <cputchar>:

/* cputchar - writes a single character to stdout */
void
cputchar(int c) {
  1002e4:	f3 0f 1e fb          	endbr32 
  1002e8:	55                   	push   %ebp
  1002e9:	89 e5                	mov    %esp,%ebp
  1002eb:	83 ec 18             	sub    $0x18,%esp
    cons_putc(c);
  1002ee:	8b 45 08             	mov    0x8(%ebp),%eax
  1002f1:	89 04 24             	mov    %eax,(%esp)
  1002f4:	e8 ee 13 00 00       	call   1016e7 <cons_putc>
}
  1002f9:	90                   	nop
  1002fa:	c9                   	leave  
  1002fb:	c3                   	ret    

001002fc <cputs>:
/* *
 * cputs- writes the string pointed by @str to stdout and
 * appends a newline character.
 * */
int
cputs(const char *str) {
  1002fc:	f3 0f 1e fb          	endbr32 
  100300:	55                   	push   %ebp
  100301:	89 e5                	mov    %esp,%ebp
  100303:	83 ec 28             	sub    $0x28,%esp
    int cnt = 0;
  100306:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    char c;
    while ((c = *str ++) != '\0') {
  10030d:	eb 13                	jmp    100322 <cputs+0x26>
        cputch(c, &cnt);
  10030f:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  100313:	8d 55 f0             	lea    -0x10(%ebp),%edx
  100316:	89 54 24 04          	mov    %edx,0x4(%esp)
  10031a:	89 04 24             	mov    %eax,(%esp)
  10031d:	e8 3c ff ff ff       	call   10025e <cputch>
    while ((c = *str ++) != '\0') {
  100322:	8b 45 08             	mov    0x8(%ebp),%eax
  100325:	8d 50 01             	lea    0x1(%eax),%edx
  100328:	89 55 08             	mov    %edx,0x8(%ebp)
  10032b:	0f b6 00             	movzbl (%eax),%eax
  10032e:	88 45 f7             	mov    %al,-0x9(%ebp)
  100331:	80 7d f7 00          	cmpb   $0x0,-0x9(%ebp)
  100335:	75 d8                	jne    10030f <cputs+0x13>
    }
    cputch('\n', &cnt);
  100337:	8d 45 f0             	lea    -0x10(%ebp),%eax
  10033a:	89 44 24 04          	mov    %eax,0x4(%esp)
  10033e:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
  100345:	e8 14 ff ff ff       	call   10025e <cputch>
    return cnt;
  10034a:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  10034d:	c9                   	leave  
  10034e:	c3                   	ret    

0010034f <getchar>:

/* getchar - reads a single non-zero character from stdin */
int
getchar(void) {
  10034f:	f3 0f 1e fb          	endbr32 
  100353:	55                   	push   %ebp
  100354:	89 e5                	mov    %esp,%ebp
  100356:	83 ec 18             	sub    $0x18,%esp
    int c;
    while ((c = cons_getc()) == 0)
  100359:	90                   	nop
  10035a:	e8 c9 13 00 00       	call   101728 <cons_getc>
  10035f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  100362:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  100366:	74 f2                	je     10035a <getchar+0xb>
        /* do nothing */;
    return c;
  100368:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  10036b:	c9                   	leave  
  10036c:	c3                   	ret    

0010036d <readline>:
 * The readline() function returns the text of the line read. If some errors
 * are happened, NULL is returned. The return value is a global variable,
 * thus it should be copied before it is used.
 * */
char *
readline(const char *prompt) {
  10036d:	f3 0f 1e fb          	endbr32 
  100371:	55                   	push   %ebp
  100372:	89 e5                	mov    %esp,%ebp
  100374:	83 ec 28             	sub    $0x28,%esp
    if (prompt != NULL) {
  100377:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  10037b:	74 13                	je     100390 <readline+0x23>
        cprintf("%s", prompt);
  10037d:	8b 45 08             	mov    0x8(%ebp),%eax
  100380:	89 44 24 04          	mov    %eax,0x4(%esp)
  100384:	c7 04 24 c7 62 10 00 	movl   $0x1062c7,(%esp)
  10038b:	e8 2a ff ff ff       	call   1002ba <cprintf>
    }
    int i = 0, c;
  100390:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while (1) {
        c = getchar();
  100397:	e8 b3 ff ff ff       	call   10034f <getchar>
  10039c:	89 45 f0             	mov    %eax,-0x10(%ebp)
        if (c < 0) {
  10039f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  1003a3:	79 07                	jns    1003ac <readline+0x3f>
            return NULL;
  1003a5:	b8 00 00 00 00       	mov    $0x0,%eax
  1003aa:	eb 78                	jmp    100424 <readline+0xb7>
        }
        else if (c >= ' ' && i < BUFSIZE - 1) {
  1003ac:	83 7d f0 1f          	cmpl   $0x1f,-0x10(%ebp)
  1003b0:	7e 28                	jle    1003da <readline+0x6d>
  1003b2:	81 7d f4 fe 03 00 00 	cmpl   $0x3fe,-0xc(%ebp)
  1003b9:	7f 1f                	jg     1003da <readline+0x6d>
            cputchar(c);
  1003bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1003be:	89 04 24             	mov    %eax,(%esp)
  1003c1:	e8 1e ff ff ff       	call   1002e4 <cputchar>
            buf[i ++] = c;
  1003c6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1003c9:	8d 50 01             	lea    0x1(%eax),%edx
  1003cc:	89 55 f4             	mov    %edx,-0xc(%ebp)
  1003cf:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1003d2:	88 90 20 c0 11 00    	mov    %dl,0x11c020(%eax)
  1003d8:	eb 45                	jmp    10041f <readline+0xb2>
        }
        else if (c == '\b' && i > 0) {
  1003da:	83 7d f0 08          	cmpl   $0x8,-0x10(%ebp)
  1003de:	75 16                	jne    1003f6 <readline+0x89>
  1003e0:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1003e4:	7e 10                	jle    1003f6 <readline+0x89>
            cputchar(c);
  1003e6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1003e9:	89 04 24             	mov    %eax,(%esp)
  1003ec:	e8 f3 fe ff ff       	call   1002e4 <cputchar>
            i --;
  1003f1:	ff 4d f4             	decl   -0xc(%ebp)
  1003f4:	eb 29                	jmp    10041f <readline+0xb2>
        }
        else if (c == '\n' || c == '\r') {
  1003f6:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
  1003fa:	74 06                	je     100402 <readline+0x95>
  1003fc:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
  100400:	75 95                	jne    100397 <readline+0x2a>
            cputchar(c);
  100402:	8b 45 f0             	mov    -0x10(%ebp),%eax
  100405:	89 04 24             	mov    %eax,(%esp)
  100408:	e8 d7 fe ff ff       	call   1002e4 <cputchar>
            buf[i] = '\0';
  10040d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100410:	05 20 c0 11 00       	add    $0x11c020,%eax
  100415:	c6 00 00             	movb   $0x0,(%eax)
            return buf;
  100418:	b8 20 c0 11 00       	mov    $0x11c020,%eax
  10041d:	eb 05                	jmp    100424 <readline+0xb7>
        c = getchar();
  10041f:	e9 73 ff ff ff       	jmp    100397 <readline+0x2a>
        }
    }
}
  100424:	c9                   	leave  
  100425:	c3                   	ret    

00100426 <__panic>:
/* *
 * __panic - __panic is called on unresolvable fatal errors. it prints
 * "panic: 'message'", and then enters the kernel monitor.
 * */
void
__panic(const char *file, int line, const char *fmt, ...) {
  100426:	f3 0f 1e fb          	endbr32 
  10042a:	55                   	push   %ebp
  10042b:	89 e5                	mov    %esp,%ebp
  10042d:	83 ec 28             	sub    $0x28,%esp
    if (is_panic) {
  100430:	a1 20 c4 11 00       	mov    0x11c420,%eax
  100435:	85 c0                	test   %eax,%eax
  100437:	75 5b                	jne    100494 <__panic+0x6e>
        goto panic_dead;
    }
    is_panic = 1;
  100439:	c7 05 20 c4 11 00 01 	movl   $0x1,0x11c420
  100440:	00 00 00 

    // print the 'message'
    va_list ap;
    va_start(ap, fmt);
  100443:	8d 45 14             	lea    0x14(%ebp),%eax
  100446:	89 45 f4             	mov    %eax,-0xc(%ebp)
    cprintf("kernel panic at %s:%d:\n    ", file, line);
  100449:	8b 45 0c             	mov    0xc(%ebp),%eax
  10044c:	89 44 24 08          	mov    %eax,0x8(%esp)
  100450:	8b 45 08             	mov    0x8(%ebp),%eax
  100453:	89 44 24 04          	mov    %eax,0x4(%esp)
  100457:	c7 04 24 ca 62 10 00 	movl   $0x1062ca,(%esp)
  10045e:	e8 57 fe ff ff       	call   1002ba <cprintf>
    vcprintf(fmt, ap);
  100463:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100466:	89 44 24 04          	mov    %eax,0x4(%esp)
  10046a:	8b 45 10             	mov    0x10(%ebp),%eax
  10046d:	89 04 24             	mov    %eax,(%esp)
  100470:	e8 0e fe ff ff       	call   100283 <vcprintf>
    cprintf("\n");
  100475:	c7 04 24 e6 62 10 00 	movl   $0x1062e6,(%esp)
  10047c:	e8 39 fe ff ff       	call   1002ba <cprintf>
    
    cprintf("stack trackback:\n");
  100481:	c7 04 24 e8 62 10 00 	movl   $0x1062e8,(%esp)
  100488:	e8 2d fe ff ff       	call   1002ba <cprintf>
    print_stackframe();
  10048d:	e8 3d 06 00 00       	call   100acf <print_stackframe>
  100492:	eb 01                	jmp    100495 <__panic+0x6f>
        goto panic_dead;
  100494:	90                   	nop
    
    va_end(ap);

panic_dead:
    intr_disable();
  100495:	e8 ef 14 00 00       	call   101989 <intr_disable>
    while (1) {
        kmonitor(NULL);
  10049a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  1004a1:	e8 68 08 00 00       	call   100d0e <kmonitor>
  1004a6:	eb f2                	jmp    10049a <__panic+0x74>

001004a8 <__warn>:
    }
}

/* __warn - like panic, but don't */
void
__warn(const char *file, int line, const char *fmt, ...) {
  1004a8:	f3 0f 1e fb          	endbr32 
  1004ac:	55                   	push   %ebp
  1004ad:	89 e5                	mov    %esp,%ebp
  1004af:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    va_start(ap, fmt);
  1004b2:	8d 45 14             	lea    0x14(%ebp),%eax
  1004b5:	89 45 f4             	mov    %eax,-0xc(%ebp)
    cprintf("kernel warning at %s:%d:\n    ", file, line);
  1004b8:	8b 45 0c             	mov    0xc(%ebp),%eax
  1004bb:	89 44 24 08          	mov    %eax,0x8(%esp)
  1004bf:	8b 45 08             	mov    0x8(%ebp),%eax
  1004c2:	89 44 24 04          	mov    %eax,0x4(%esp)
  1004c6:	c7 04 24 fa 62 10 00 	movl   $0x1062fa,(%esp)
  1004cd:	e8 e8 fd ff ff       	call   1002ba <cprintf>
    vcprintf(fmt, ap);
  1004d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1004d5:	89 44 24 04          	mov    %eax,0x4(%esp)
  1004d9:	8b 45 10             	mov    0x10(%ebp),%eax
  1004dc:	89 04 24             	mov    %eax,(%esp)
  1004df:	e8 9f fd ff ff       	call   100283 <vcprintf>
    cprintf("\n");
  1004e4:	c7 04 24 e6 62 10 00 	movl   $0x1062e6,(%esp)
  1004eb:	e8 ca fd ff ff       	call   1002ba <cprintf>
    va_end(ap);
}
  1004f0:	90                   	nop
  1004f1:	c9                   	leave  
  1004f2:	c3                   	ret    

001004f3 <is_kernel_panic>:

bool
is_kernel_panic(void) {
  1004f3:	f3 0f 1e fb          	endbr32 
  1004f7:	55                   	push   %ebp
  1004f8:	89 e5                	mov    %esp,%ebp
    return is_panic;
  1004fa:	a1 20 c4 11 00       	mov    0x11c420,%eax
}
  1004ff:	5d                   	pop    %ebp
  100500:	c3                   	ret    

00100501 <stab_binsearch>:
 *      stab_binsearch(stabs, &left, &right, N_SO, 0xf0100184);
 * will exit setting left = 118, right = 554.
 * */
static void
stab_binsearch(const struct stab *stabs, int *region_left, int *region_right,
           int type, uintptr_t addr) {
  100501:	f3 0f 1e fb          	endbr32 
  100505:	55                   	push   %ebp
  100506:	89 e5                	mov    %esp,%ebp
  100508:	83 ec 20             	sub    $0x20,%esp
    int l = *region_left, r = *region_right, any_matches = 0;
  10050b:	8b 45 0c             	mov    0xc(%ebp),%eax
  10050e:	8b 00                	mov    (%eax),%eax
  100510:	89 45 fc             	mov    %eax,-0x4(%ebp)
  100513:	8b 45 10             	mov    0x10(%ebp),%eax
  100516:	8b 00                	mov    (%eax),%eax
  100518:	89 45 f8             	mov    %eax,-0x8(%ebp)
  10051b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

    while (l <= r) {
  100522:	e9 ca 00 00 00       	jmp    1005f1 <stab_binsearch+0xf0>
        int true_m = (l + r) / 2, m = true_m;
  100527:	8b 55 fc             	mov    -0x4(%ebp),%edx
  10052a:	8b 45 f8             	mov    -0x8(%ebp),%eax
  10052d:	01 d0                	add    %edx,%eax
  10052f:	89 c2                	mov    %eax,%edx
  100531:	c1 ea 1f             	shr    $0x1f,%edx
  100534:	01 d0                	add    %edx,%eax
  100536:	d1 f8                	sar    %eax
  100538:	89 45 ec             	mov    %eax,-0x14(%ebp)
  10053b:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10053e:	89 45 f0             	mov    %eax,-0x10(%ebp)

        // search for earliest stab with right type
        while (m >= l && stabs[m].n_type != type) {
  100541:	eb 03                	jmp    100546 <stab_binsearch+0x45>
            m --;
  100543:	ff 4d f0             	decl   -0x10(%ebp)
        while (m >= l && stabs[m].n_type != type) {
  100546:	8b 45 f0             	mov    -0x10(%ebp),%eax
  100549:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  10054c:	7c 1f                	jl     10056d <stab_binsearch+0x6c>
  10054e:	8b 55 f0             	mov    -0x10(%ebp),%edx
  100551:	89 d0                	mov    %edx,%eax
  100553:	01 c0                	add    %eax,%eax
  100555:	01 d0                	add    %edx,%eax
  100557:	c1 e0 02             	shl    $0x2,%eax
  10055a:	89 c2                	mov    %eax,%edx
  10055c:	8b 45 08             	mov    0x8(%ebp),%eax
  10055f:	01 d0                	add    %edx,%eax
  100561:	0f b6 40 04          	movzbl 0x4(%eax),%eax
  100565:	0f b6 c0             	movzbl %al,%eax
  100568:	39 45 14             	cmp    %eax,0x14(%ebp)
  10056b:	75 d6                	jne    100543 <stab_binsearch+0x42>
        }
        if (m < l) {    // no match in [l, m]
  10056d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  100570:	3b 45 fc             	cmp    -0x4(%ebp),%eax
  100573:	7d 09                	jge    10057e <stab_binsearch+0x7d>
            l = true_m + 1;
  100575:	8b 45 ec             	mov    -0x14(%ebp),%eax
  100578:	40                   	inc    %eax
  100579:	89 45 fc             	mov    %eax,-0x4(%ebp)
            continue;
  10057c:	eb 73                	jmp    1005f1 <stab_binsearch+0xf0>
        }

        // actual binary search
        any_matches = 1;
  10057e:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
        if (stabs[m].n_value < addr) {
  100585:	8b 55 f0             	mov    -0x10(%ebp),%edx
  100588:	89 d0                	mov    %edx,%eax
  10058a:	01 c0                	add    %eax,%eax
  10058c:	01 d0                	add    %edx,%eax
  10058e:	c1 e0 02             	shl    $0x2,%eax
  100591:	89 c2                	mov    %eax,%edx
  100593:	8b 45 08             	mov    0x8(%ebp),%eax
  100596:	01 d0                	add    %edx,%eax
  100598:	8b 40 08             	mov    0x8(%eax),%eax
  10059b:	39 45 18             	cmp    %eax,0x18(%ebp)
  10059e:	76 11                	jbe    1005b1 <stab_binsearch+0xb0>
            *region_left = m;
  1005a0:	8b 45 0c             	mov    0xc(%ebp),%eax
  1005a3:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1005a6:	89 10                	mov    %edx,(%eax)
            l = true_m + 1;
  1005a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1005ab:	40                   	inc    %eax
  1005ac:	89 45 fc             	mov    %eax,-0x4(%ebp)
  1005af:	eb 40                	jmp    1005f1 <stab_binsearch+0xf0>
        } else if (stabs[m].n_value > addr) {
  1005b1:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1005b4:	89 d0                	mov    %edx,%eax
  1005b6:	01 c0                	add    %eax,%eax
  1005b8:	01 d0                	add    %edx,%eax
  1005ba:	c1 e0 02             	shl    $0x2,%eax
  1005bd:	89 c2                	mov    %eax,%edx
  1005bf:	8b 45 08             	mov    0x8(%ebp),%eax
  1005c2:	01 d0                	add    %edx,%eax
  1005c4:	8b 40 08             	mov    0x8(%eax),%eax
  1005c7:	39 45 18             	cmp    %eax,0x18(%ebp)
  1005ca:	73 14                	jae    1005e0 <stab_binsearch+0xdf>
            *region_right = m - 1;
  1005cc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1005cf:	8d 50 ff             	lea    -0x1(%eax),%edx
  1005d2:	8b 45 10             	mov    0x10(%ebp),%eax
  1005d5:	89 10                	mov    %edx,(%eax)
            r = m - 1;
  1005d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1005da:	48                   	dec    %eax
  1005db:	89 45 f8             	mov    %eax,-0x8(%ebp)
  1005de:	eb 11                	jmp    1005f1 <stab_binsearch+0xf0>
        } else {
            // exact match for 'addr', but continue loop to find
            // *region_right
            *region_left = m;
  1005e0:	8b 45 0c             	mov    0xc(%ebp),%eax
  1005e3:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1005e6:	89 10                	mov    %edx,(%eax)
            l = m;
  1005e8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1005eb:	89 45 fc             	mov    %eax,-0x4(%ebp)
            addr ++;
  1005ee:	ff 45 18             	incl   0x18(%ebp)
    while (l <= r) {
  1005f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1005f4:	3b 45 f8             	cmp    -0x8(%ebp),%eax
  1005f7:	0f 8e 2a ff ff ff    	jle    100527 <stab_binsearch+0x26>
        }
    }

    if (!any_matches) {
  1005fd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  100601:	75 0f                	jne    100612 <stab_binsearch+0x111>
        *region_right = *region_left - 1;
  100603:	8b 45 0c             	mov    0xc(%ebp),%eax
  100606:	8b 00                	mov    (%eax),%eax
  100608:	8d 50 ff             	lea    -0x1(%eax),%edx
  10060b:	8b 45 10             	mov    0x10(%ebp),%eax
  10060e:	89 10                	mov    %edx,(%eax)
        l = *region_right;
        for (; l > *region_left && stabs[l].n_type != type; l --)
            /* do nothing */;
        *region_left = l;
    }
}
  100610:	eb 3e                	jmp    100650 <stab_binsearch+0x14f>
        l = *region_right;
  100612:	8b 45 10             	mov    0x10(%ebp),%eax
  100615:	8b 00                	mov    (%eax),%eax
  100617:	89 45 fc             	mov    %eax,-0x4(%ebp)
        for (; l > *region_left && stabs[l].n_type != type; l --)
  10061a:	eb 03                	jmp    10061f <stab_binsearch+0x11e>
  10061c:	ff 4d fc             	decl   -0x4(%ebp)
  10061f:	8b 45 0c             	mov    0xc(%ebp),%eax
  100622:	8b 00                	mov    (%eax),%eax
  100624:	39 45 fc             	cmp    %eax,-0x4(%ebp)
  100627:	7e 1f                	jle    100648 <stab_binsearch+0x147>
  100629:	8b 55 fc             	mov    -0x4(%ebp),%edx
  10062c:	89 d0                	mov    %edx,%eax
  10062e:	01 c0                	add    %eax,%eax
  100630:	01 d0                	add    %edx,%eax
  100632:	c1 e0 02             	shl    $0x2,%eax
  100635:	89 c2                	mov    %eax,%edx
  100637:	8b 45 08             	mov    0x8(%ebp),%eax
  10063a:	01 d0                	add    %edx,%eax
  10063c:	0f b6 40 04          	movzbl 0x4(%eax),%eax
  100640:	0f b6 c0             	movzbl %al,%eax
  100643:	39 45 14             	cmp    %eax,0x14(%ebp)
  100646:	75 d4                	jne    10061c <stab_binsearch+0x11b>
        *region_left = l;
  100648:	8b 45 0c             	mov    0xc(%ebp),%eax
  10064b:	8b 55 fc             	mov    -0x4(%ebp),%edx
  10064e:	89 10                	mov    %edx,(%eax)
}
  100650:	90                   	nop
  100651:	c9                   	leave  
  100652:	c3                   	ret    

00100653 <debuginfo_eip>:
 * the specified instruction address, @addr.  Returns 0 if information
 * was found, and negative if not.  But even if it returns negative it
 * has stored some information into '*info'.
 * */
int
debuginfo_eip(uintptr_t addr, struct eipdebuginfo *info) {
  100653:	f3 0f 1e fb          	endbr32 
  100657:	55                   	push   %ebp
  100658:	89 e5                	mov    %esp,%ebp
  10065a:	83 ec 58             	sub    $0x58,%esp
    const struct stab *stabs, *stab_end;
    const char *stabstr, *stabstr_end;

    info->eip_file = "<unknown>";
  10065d:	8b 45 0c             	mov    0xc(%ebp),%eax
  100660:	c7 00 18 63 10 00    	movl   $0x106318,(%eax)
    info->eip_line = 0;
  100666:	8b 45 0c             	mov    0xc(%ebp),%eax
  100669:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
    info->eip_fn_name = "<unknown>";
  100670:	8b 45 0c             	mov    0xc(%ebp),%eax
  100673:	c7 40 08 18 63 10 00 	movl   $0x106318,0x8(%eax)
    info->eip_fn_namelen = 9;
  10067a:	8b 45 0c             	mov    0xc(%ebp),%eax
  10067d:	c7 40 0c 09 00 00 00 	movl   $0x9,0xc(%eax)
    info->eip_fn_addr = addr;
  100684:	8b 45 0c             	mov    0xc(%ebp),%eax
  100687:	8b 55 08             	mov    0x8(%ebp),%edx
  10068a:	89 50 10             	mov    %edx,0x10(%eax)
    info->eip_fn_narg = 0;
  10068d:	8b 45 0c             	mov    0xc(%ebp),%eax
  100690:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)

    stabs = __STAB_BEGIN__;
  100697:	c7 45 f4 98 75 10 00 	movl   $0x107598,-0xc(%ebp)
    stab_end = __STAB_END__;
  10069e:	c7 45 f0 88 3f 11 00 	movl   $0x113f88,-0x10(%ebp)
    stabstr = __STABSTR_BEGIN__;
  1006a5:	c7 45 ec 89 3f 11 00 	movl   $0x113f89,-0x14(%ebp)
    stabstr_end = __STABSTR_END__;
  1006ac:	c7 45 e8 a1 6a 11 00 	movl   $0x116aa1,-0x18(%ebp)

    // String table validity checks
    if (stabstr_end <= stabstr || stabstr_end[-1] != 0) {
  1006b3:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1006b6:	3b 45 ec             	cmp    -0x14(%ebp),%eax
  1006b9:	76 0b                	jbe    1006c6 <debuginfo_eip+0x73>
  1006bb:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1006be:	48                   	dec    %eax
  1006bf:	0f b6 00             	movzbl (%eax),%eax
  1006c2:	84 c0                	test   %al,%al
  1006c4:	74 0a                	je     1006d0 <debuginfo_eip+0x7d>
        return -1;
  1006c6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  1006cb:	e9 ab 02 00 00       	jmp    10097b <debuginfo_eip+0x328>
    // 'eip'.  First, we find the basic source file containing 'eip'.
    // Then, we look in that source file for the function.  Then we look
    // for the line number.

    // Search the entire set of stabs for the source file (type N_SO).
    int lfile = 0, rfile = (stab_end - stabs) - 1;
  1006d0:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  1006d7:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1006da:	2b 45 f4             	sub    -0xc(%ebp),%eax
  1006dd:	c1 f8 02             	sar    $0x2,%eax
  1006e0:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
  1006e6:	48                   	dec    %eax
  1006e7:	89 45 e0             	mov    %eax,-0x20(%ebp)
    stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
  1006ea:	8b 45 08             	mov    0x8(%ebp),%eax
  1006ed:	89 44 24 10          	mov    %eax,0x10(%esp)
  1006f1:	c7 44 24 0c 64 00 00 	movl   $0x64,0xc(%esp)
  1006f8:	00 
  1006f9:	8d 45 e0             	lea    -0x20(%ebp),%eax
  1006fc:	89 44 24 08          	mov    %eax,0x8(%esp)
  100700:	8d 45 e4             	lea    -0x1c(%ebp),%eax
  100703:	89 44 24 04          	mov    %eax,0x4(%esp)
  100707:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10070a:	89 04 24             	mov    %eax,(%esp)
  10070d:	e8 ef fd ff ff       	call   100501 <stab_binsearch>
    if (lfile == 0)
  100712:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  100715:	85 c0                	test   %eax,%eax
  100717:	75 0a                	jne    100723 <debuginfo_eip+0xd0>
        return -1;
  100719:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  10071e:	e9 58 02 00 00       	jmp    10097b <debuginfo_eip+0x328>

    // Search within that file's stabs for the function definition
    // (N_FUN).
    int lfun = lfile, rfun = rfile;
  100723:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  100726:	89 45 dc             	mov    %eax,-0x24(%ebp)
  100729:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10072c:	89 45 d8             	mov    %eax,-0x28(%ebp)
    int lline, rline;
    stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
  10072f:	8b 45 08             	mov    0x8(%ebp),%eax
  100732:	89 44 24 10          	mov    %eax,0x10(%esp)
  100736:	c7 44 24 0c 24 00 00 	movl   $0x24,0xc(%esp)
  10073d:	00 
  10073e:	8d 45 d8             	lea    -0x28(%ebp),%eax
  100741:	89 44 24 08          	mov    %eax,0x8(%esp)
  100745:	8d 45 dc             	lea    -0x24(%ebp),%eax
  100748:	89 44 24 04          	mov    %eax,0x4(%esp)
  10074c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10074f:	89 04 24             	mov    %eax,(%esp)
  100752:	e8 aa fd ff ff       	call   100501 <stab_binsearch>

    if (lfun <= rfun) {
  100757:	8b 55 dc             	mov    -0x24(%ebp),%edx
  10075a:	8b 45 d8             	mov    -0x28(%ebp),%eax
  10075d:	39 c2                	cmp    %eax,%edx
  10075f:	7f 78                	jg     1007d9 <debuginfo_eip+0x186>
        // stabs[lfun] points to the function name
        // in the string table, but check bounds just in case.
        if (stabs[lfun].n_strx < stabstr_end - stabstr) {
  100761:	8b 45 dc             	mov    -0x24(%ebp),%eax
  100764:	89 c2                	mov    %eax,%edx
  100766:	89 d0                	mov    %edx,%eax
  100768:	01 c0                	add    %eax,%eax
  10076a:	01 d0                	add    %edx,%eax
  10076c:	c1 e0 02             	shl    $0x2,%eax
  10076f:	89 c2                	mov    %eax,%edx
  100771:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100774:	01 d0                	add    %edx,%eax
  100776:	8b 10                	mov    (%eax),%edx
  100778:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10077b:	2b 45 ec             	sub    -0x14(%ebp),%eax
  10077e:	39 c2                	cmp    %eax,%edx
  100780:	73 22                	jae    1007a4 <debuginfo_eip+0x151>
            info->eip_fn_name = stabstr + stabs[lfun].n_strx;
  100782:	8b 45 dc             	mov    -0x24(%ebp),%eax
  100785:	89 c2                	mov    %eax,%edx
  100787:	89 d0                	mov    %edx,%eax
  100789:	01 c0                	add    %eax,%eax
  10078b:	01 d0                	add    %edx,%eax
  10078d:	c1 e0 02             	shl    $0x2,%eax
  100790:	89 c2                	mov    %eax,%edx
  100792:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100795:	01 d0                	add    %edx,%eax
  100797:	8b 10                	mov    (%eax),%edx
  100799:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10079c:	01 c2                	add    %eax,%edx
  10079e:	8b 45 0c             	mov    0xc(%ebp),%eax
  1007a1:	89 50 08             	mov    %edx,0x8(%eax)
        }
        info->eip_fn_addr = stabs[lfun].n_value;
  1007a4:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1007a7:	89 c2                	mov    %eax,%edx
  1007a9:	89 d0                	mov    %edx,%eax
  1007ab:	01 c0                	add    %eax,%eax
  1007ad:	01 d0                	add    %edx,%eax
  1007af:	c1 e0 02             	shl    $0x2,%eax
  1007b2:	89 c2                	mov    %eax,%edx
  1007b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1007b7:	01 d0                	add    %edx,%eax
  1007b9:	8b 50 08             	mov    0x8(%eax),%edx
  1007bc:	8b 45 0c             	mov    0xc(%ebp),%eax
  1007bf:	89 50 10             	mov    %edx,0x10(%eax)
        addr -= info->eip_fn_addr;
  1007c2:	8b 45 0c             	mov    0xc(%ebp),%eax
  1007c5:	8b 40 10             	mov    0x10(%eax),%eax
  1007c8:	29 45 08             	sub    %eax,0x8(%ebp)
        // Search within the function definition for the line number.
        lline = lfun;
  1007cb:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1007ce:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        rline = rfun;
  1007d1:	8b 45 d8             	mov    -0x28(%ebp),%eax
  1007d4:	89 45 d0             	mov    %eax,-0x30(%ebp)
  1007d7:	eb 15                	jmp    1007ee <debuginfo_eip+0x19b>
    } else {
        // Couldn't find function stab!  Maybe we're in an assembly
        // file.  Search the whole file for the line number.
        info->eip_fn_addr = addr;
  1007d9:	8b 45 0c             	mov    0xc(%ebp),%eax
  1007dc:	8b 55 08             	mov    0x8(%ebp),%edx
  1007df:	89 50 10             	mov    %edx,0x10(%eax)
        lline = lfile;
  1007e2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  1007e5:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        rline = rfile;
  1007e8:	8b 45 e0             	mov    -0x20(%ebp),%eax
  1007eb:	89 45 d0             	mov    %eax,-0x30(%ebp)
    }
    info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
  1007ee:	8b 45 0c             	mov    0xc(%ebp),%eax
  1007f1:	8b 40 08             	mov    0x8(%eax),%eax
  1007f4:	c7 44 24 04 3a 00 00 	movl   $0x3a,0x4(%esp)
  1007fb:	00 
  1007fc:	89 04 24             	mov    %eax,(%esp)
  1007ff:	e8 67 50 00 00       	call   10586b <strfind>
  100804:	8b 55 0c             	mov    0xc(%ebp),%edx
  100807:	8b 52 08             	mov    0x8(%edx),%edx
  10080a:	29 d0                	sub    %edx,%eax
  10080c:	89 c2                	mov    %eax,%edx
  10080e:	8b 45 0c             	mov    0xc(%ebp),%eax
  100811:	89 50 0c             	mov    %edx,0xc(%eax)

    // Search within [lline, rline] for the line number stab.
    // If found, set info->eip_line to the right line number.
    // If not found, return -1.
    stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
  100814:	8b 45 08             	mov    0x8(%ebp),%eax
  100817:	89 44 24 10          	mov    %eax,0x10(%esp)
  10081b:	c7 44 24 0c 44 00 00 	movl   $0x44,0xc(%esp)
  100822:	00 
  100823:	8d 45 d0             	lea    -0x30(%ebp),%eax
  100826:	89 44 24 08          	mov    %eax,0x8(%esp)
  10082a:	8d 45 d4             	lea    -0x2c(%ebp),%eax
  10082d:	89 44 24 04          	mov    %eax,0x4(%esp)
  100831:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100834:	89 04 24             	mov    %eax,(%esp)
  100837:	e8 c5 fc ff ff       	call   100501 <stab_binsearch>
    if (lline <= rline) {
  10083c:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10083f:	8b 45 d0             	mov    -0x30(%ebp),%eax
  100842:	39 c2                	cmp    %eax,%edx
  100844:	7f 23                	jg     100869 <debuginfo_eip+0x216>
        info->eip_line = stabs[rline].n_desc;
  100846:	8b 45 d0             	mov    -0x30(%ebp),%eax
  100849:	89 c2                	mov    %eax,%edx
  10084b:	89 d0                	mov    %edx,%eax
  10084d:	01 c0                	add    %eax,%eax
  10084f:	01 d0                	add    %edx,%eax
  100851:	c1 e0 02             	shl    $0x2,%eax
  100854:	89 c2                	mov    %eax,%edx
  100856:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100859:	01 d0                	add    %edx,%eax
  10085b:	0f b7 40 06          	movzwl 0x6(%eax),%eax
  10085f:	89 c2                	mov    %eax,%edx
  100861:	8b 45 0c             	mov    0xc(%ebp),%eax
  100864:	89 50 04             	mov    %edx,0x4(%eax)

    // Search backwards from the line number for the relevant filename stab.
    // We can't just use the "lfile" stab because inlined functions
    // can interpolate code from a different file!
    // Such included source files use the N_SOL stab type.
    while (lline >= lfile
  100867:	eb 11                	jmp    10087a <debuginfo_eip+0x227>
        return -1;
  100869:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  10086e:	e9 08 01 00 00       	jmp    10097b <debuginfo_eip+0x328>
           && stabs[lline].n_type != N_SOL
           && (stabs[lline].n_type != N_SO || !stabs[lline].n_value)) {
        lline --;
  100873:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  100876:	48                   	dec    %eax
  100877:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    while (lline >= lfile
  10087a:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10087d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  100880:	39 c2                	cmp    %eax,%edx
  100882:	7c 56                	jl     1008da <debuginfo_eip+0x287>
           && stabs[lline].n_type != N_SOL
  100884:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  100887:	89 c2                	mov    %eax,%edx
  100889:	89 d0                	mov    %edx,%eax
  10088b:	01 c0                	add    %eax,%eax
  10088d:	01 d0                	add    %edx,%eax
  10088f:	c1 e0 02             	shl    $0x2,%eax
  100892:	89 c2                	mov    %eax,%edx
  100894:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100897:	01 d0                	add    %edx,%eax
  100899:	0f b6 40 04          	movzbl 0x4(%eax),%eax
  10089d:	3c 84                	cmp    $0x84,%al
  10089f:	74 39                	je     1008da <debuginfo_eip+0x287>
           && (stabs[lline].n_type != N_SO || !stabs[lline].n_value)) {
  1008a1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1008a4:	89 c2                	mov    %eax,%edx
  1008a6:	89 d0                	mov    %edx,%eax
  1008a8:	01 c0                	add    %eax,%eax
  1008aa:	01 d0                	add    %edx,%eax
  1008ac:	c1 e0 02             	shl    $0x2,%eax
  1008af:	89 c2                	mov    %eax,%edx
  1008b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1008b4:	01 d0                	add    %edx,%eax
  1008b6:	0f b6 40 04          	movzbl 0x4(%eax),%eax
  1008ba:	3c 64                	cmp    $0x64,%al
  1008bc:	75 b5                	jne    100873 <debuginfo_eip+0x220>
  1008be:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1008c1:	89 c2                	mov    %eax,%edx
  1008c3:	89 d0                	mov    %edx,%eax
  1008c5:	01 c0                	add    %eax,%eax
  1008c7:	01 d0                	add    %edx,%eax
  1008c9:	c1 e0 02             	shl    $0x2,%eax
  1008cc:	89 c2                	mov    %eax,%edx
  1008ce:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1008d1:	01 d0                	add    %edx,%eax
  1008d3:	8b 40 08             	mov    0x8(%eax),%eax
  1008d6:	85 c0                	test   %eax,%eax
  1008d8:	74 99                	je     100873 <debuginfo_eip+0x220>
    }
    if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr) {
  1008da:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  1008dd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  1008e0:	39 c2                	cmp    %eax,%edx
  1008e2:	7c 42                	jl     100926 <debuginfo_eip+0x2d3>
  1008e4:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1008e7:	89 c2                	mov    %eax,%edx
  1008e9:	89 d0                	mov    %edx,%eax
  1008eb:	01 c0                	add    %eax,%eax
  1008ed:	01 d0                	add    %edx,%eax
  1008ef:	c1 e0 02             	shl    $0x2,%eax
  1008f2:	89 c2                	mov    %eax,%edx
  1008f4:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1008f7:	01 d0                	add    %edx,%eax
  1008f9:	8b 10                	mov    (%eax),%edx
  1008fb:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1008fe:	2b 45 ec             	sub    -0x14(%ebp),%eax
  100901:	39 c2                	cmp    %eax,%edx
  100903:	73 21                	jae    100926 <debuginfo_eip+0x2d3>
        info->eip_file = stabstr + stabs[lline].n_strx;
  100905:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  100908:	89 c2                	mov    %eax,%edx
  10090a:	89 d0                	mov    %edx,%eax
  10090c:	01 c0                	add    %eax,%eax
  10090e:	01 d0                	add    %edx,%eax
  100910:	c1 e0 02             	shl    $0x2,%eax
  100913:	89 c2                	mov    %eax,%edx
  100915:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100918:	01 d0                	add    %edx,%eax
  10091a:	8b 10                	mov    (%eax),%edx
  10091c:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10091f:	01 c2                	add    %eax,%edx
  100921:	8b 45 0c             	mov    0xc(%ebp),%eax
  100924:	89 10                	mov    %edx,(%eax)
    }

    // Set eip_fn_narg to the number of arguments taken by the function,
    // or 0 if there was no containing function.
    if (lfun < rfun) {
  100926:	8b 55 dc             	mov    -0x24(%ebp),%edx
  100929:	8b 45 d8             	mov    -0x28(%ebp),%eax
  10092c:	39 c2                	cmp    %eax,%edx
  10092e:	7d 46                	jge    100976 <debuginfo_eip+0x323>
        for (lline = lfun + 1;
  100930:	8b 45 dc             	mov    -0x24(%ebp),%eax
  100933:	40                   	inc    %eax
  100934:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  100937:	eb 16                	jmp    10094f <debuginfo_eip+0x2fc>
             lline < rfun && stabs[lline].n_type == N_PSYM;
             lline ++) {
            info->eip_fn_narg ++;
  100939:	8b 45 0c             	mov    0xc(%ebp),%eax
  10093c:	8b 40 14             	mov    0x14(%eax),%eax
  10093f:	8d 50 01             	lea    0x1(%eax),%edx
  100942:	8b 45 0c             	mov    0xc(%ebp),%eax
  100945:	89 50 14             	mov    %edx,0x14(%eax)
             lline ++) {
  100948:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  10094b:	40                   	inc    %eax
  10094c:	89 45 d4             	mov    %eax,-0x2c(%ebp)
             lline < rfun && stabs[lline].n_type == N_PSYM;
  10094f:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  100952:	8b 45 d8             	mov    -0x28(%ebp),%eax
        for (lline = lfun + 1;
  100955:	39 c2                	cmp    %eax,%edx
  100957:	7d 1d                	jge    100976 <debuginfo_eip+0x323>
             lline < rfun && stabs[lline].n_type == N_PSYM;
  100959:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  10095c:	89 c2                	mov    %eax,%edx
  10095e:	89 d0                	mov    %edx,%eax
  100960:	01 c0                	add    %eax,%eax
  100962:	01 d0                	add    %edx,%eax
  100964:	c1 e0 02             	shl    $0x2,%eax
  100967:	89 c2                	mov    %eax,%edx
  100969:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10096c:	01 d0                	add    %edx,%eax
  10096e:	0f b6 40 04          	movzbl 0x4(%eax),%eax
  100972:	3c a0                	cmp    $0xa0,%al
  100974:	74 c3                	je     100939 <debuginfo_eip+0x2e6>
        }
    }
    return 0;
  100976:	b8 00 00 00 00       	mov    $0x0,%eax
}
  10097b:	c9                   	leave  
  10097c:	c3                   	ret    

0010097d <print_kerninfo>:
 * print_kerninfo - print the information about kernel, including the location
 * of kernel entry, the start addresses of data and text segements, the start
 * address of free memory and how many memory that kernel has used.
 * */
void
print_kerninfo(void) {
  10097d:	f3 0f 1e fb          	endbr32 
  100981:	55                   	push   %ebp
  100982:	89 e5                	mov    %esp,%ebp
  100984:	83 ec 18             	sub    $0x18,%esp
    extern char etext[], edata[], end[], kern_init[];
    cprintf("Special kernel symbols:\n");
  100987:	c7 04 24 22 63 10 00 	movl   $0x106322,(%esp)
  10098e:	e8 27 f9 ff ff       	call   1002ba <cprintf>
    cprintf("  entry  0x%08x (phys)\n", kern_init);
  100993:	c7 44 24 04 36 00 10 	movl   $0x100036,0x4(%esp)
  10099a:	00 
  10099b:	c7 04 24 3b 63 10 00 	movl   $0x10633b,(%esp)
  1009a2:	e8 13 f9 ff ff       	call   1002ba <cprintf>
    cprintf("  etext  0x%08x (phys)\n", etext);
  1009a7:	c7 44 24 04 1b 62 10 	movl   $0x10621b,0x4(%esp)
  1009ae:	00 
  1009af:	c7 04 24 53 63 10 00 	movl   $0x106353,(%esp)
  1009b6:	e8 ff f8 ff ff       	call   1002ba <cprintf>
    cprintf("  edata  0x%08x (phys)\n", edata);
  1009bb:	c7 44 24 04 36 9a 11 	movl   $0x119a36,0x4(%esp)
  1009c2:	00 
  1009c3:	c7 04 24 6b 63 10 00 	movl   $0x10636b,(%esp)
  1009ca:	e8 eb f8 ff ff       	call   1002ba <cprintf>
    cprintf("  end    0x%08x (phys)\n", end);
  1009cf:	c7 44 24 04 28 cf 11 	movl   $0x11cf28,0x4(%esp)
  1009d6:	00 
  1009d7:	c7 04 24 83 63 10 00 	movl   $0x106383,(%esp)
  1009de:	e8 d7 f8 ff ff       	call   1002ba <cprintf>
    cprintf("Kernel executable memory footprint: %dKB\n", (end - kern_init + 1023)/1024);
  1009e3:	b8 28 cf 11 00       	mov    $0x11cf28,%eax
  1009e8:	2d 36 00 10 00       	sub    $0x100036,%eax
  1009ed:	05 ff 03 00 00       	add    $0x3ff,%eax
  1009f2:	8d 90 ff 03 00 00    	lea    0x3ff(%eax),%edx
  1009f8:	85 c0                	test   %eax,%eax
  1009fa:	0f 48 c2             	cmovs  %edx,%eax
  1009fd:	c1 f8 0a             	sar    $0xa,%eax
  100a00:	89 44 24 04          	mov    %eax,0x4(%esp)
  100a04:	c7 04 24 9c 63 10 00 	movl   $0x10639c,(%esp)
  100a0b:	e8 aa f8 ff ff       	call   1002ba <cprintf>
}
  100a10:	90                   	nop
  100a11:	c9                   	leave  
  100a12:	c3                   	ret    

00100a13 <print_debuginfo>:
/* *
 * print_debuginfo - read and print the stat information for the address @eip,
 * and info.eip_fn_addr should be the first address of the related function.
 * */
void
print_debuginfo(uintptr_t eip) {
  100a13:	f3 0f 1e fb          	endbr32 
  100a17:	55                   	push   %ebp
  100a18:	89 e5                	mov    %esp,%ebp
  100a1a:	81 ec 48 01 00 00    	sub    $0x148,%esp
    struct eipdebuginfo info;
    if (debuginfo_eip(eip, &info) != 0) {
  100a20:	8d 45 dc             	lea    -0x24(%ebp),%eax
  100a23:	89 44 24 04          	mov    %eax,0x4(%esp)
  100a27:	8b 45 08             	mov    0x8(%ebp),%eax
  100a2a:	89 04 24             	mov    %eax,(%esp)
  100a2d:	e8 21 fc ff ff       	call   100653 <debuginfo_eip>
  100a32:	85 c0                	test   %eax,%eax
  100a34:	74 15                	je     100a4b <print_debuginfo+0x38>
        cprintf("    <unknow>: -- 0x%08x --\n", eip);
  100a36:	8b 45 08             	mov    0x8(%ebp),%eax
  100a39:	89 44 24 04          	mov    %eax,0x4(%esp)
  100a3d:	c7 04 24 c6 63 10 00 	movl   $0x1063c6,(%esp)
  100a44:	e8 71 f8 ff ff       	call   1002ba <cprintf>
        }
        fnname[j] = '\0';
        cprintf("    %s:%d: %s+%d\n", info.eip_file, info.eip_line,
                fnname, eip - info.eip_fn_addr);
    }
}
  100a49:	eb 6c                	jmp    100ab7 <print_debuginfo+0xa4>
        for (j = 0; j < info.eip_fn_namelen; j ++) {
  100a4b:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  100a52:	eb 1b                	jmp    100a6f <print_debuginfo+0x5c>
            fnname[j] = info.eip_fn_name[j];
  100a54:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  100a57:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100a5a:	01 d0                	add    %edx,%eax
  100a5c:	0f b6 10             	movzbl (%eax),%edx
  100a5f:	8d 8d dc fe ff ff    	lea    -0x124(%ebp),%ecx
  100a65:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100a68:	01 c8                	add    %ecx,%eax
  100a6a:	88 10                	mov    %dl,(%eax)
        for (j = 0; j < info.eip_fn_namelen; j ++) {
  100a6c:	ff 45 f4             	incl   -0xc(%ebp)
  100a6f:	8b 45 e8             	mov    -0x18(%ebp),%eax
  100a72:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  100a75:	7c dd                	jl     100a54 <print_debuginfo+0x41>
        fnname[j] = '\0';
  100a77:	8d 95 dc fe ff ff    	lea    -0x124(%ebp),%edx
  100a7d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100a80:	01 d0                	add    %edx,%eax
  100a82:	c6 00 00             	movb   $0x0,(%eax)
                fnname, eip - info.eip_fn_addr);
  100a85:	8b 45 ec             	mov    -0x14(%ebp),%eax
        cprintf("    %s:%d: %s+%d\n", info.eip_file, info.eip_line,
  100a88:	8b 55 08             	mov    0x8(%ebp),%edx
  100a8b:	89 d1                	mov    %edx,%ecx
  100a8d:	29 c1                	sub    %eax,%ecx
  100a8f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  100a92:	8b 45 dc             	mov    -0x24(%ebp),%eax
  100a95:	89 4c 24 10          	mov    %ecx,0x10(%esp)
  100a99:	8d 8d dc fe ff ff    	lea    -0x124(%ebp),%ecx
  100a9f:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  100aa3:	89 54 24 08          	mov    %edx,0x8(%esp)
  100aa7:	89 44 24 04          	mov    %eax,0x4(%esp)
  100aab:	c7 04 24 e2 63 10 00 	movl   $0x1063e2,(%esp)
  100ab2:	e8 03 f8 ff ff       	call   1002ba <cprintf>
}
  100ab7:	90                   	nop
  100ab8:	c9                   	leave  
  100ab9:	c3                   	ret    

00100aba <read_eip>:

static __noinline uint32_t
read_eip(void) {
  100aba:	f3 0f 1e fb          	endbr32 
  100abe:	55                   	push   %ebp
  100abf:	89 e5                	mov    %esp,%ebp
  100ac1:	83 ec 10             	sub    $0x10,%esp
    uint32_t eip;
    asm volatile("movl 4(%%ebp), %0" : "=r" (eip));
  100ac4:	8b 45 04             	mov    0x4(%ebp),%eax
  100ac7:	89 45 fc             	mov    %eax,-0x4(%ebp)
    return eip;
  100aca:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  100acd:	c9                   	leave  
  100ace:	c3                   	ret    

00100acf <print_stackframe>:
 * calling-chain. Finally print_stackframe() will trace and print them for debugging.
 *
 * Note that, the length of ebp-chain is limited. In boot/bootasm.S, before jumping
 * to the kernel entry, the value of ebp has been set to zero, that's the boundary.
 * */
void print_stackframe(void) {
  100acf:	f3 0f 1e fb          	endbr32 
  100ad3:	55                   	push   %ebp
  100ad4:	89 e5                	mov    %esp,%ebp
  100ad6:	53                   	push   %ebx
  100ad7:	83 ec 44             	sub    $0x44,%esp
}

static inline uint32_t
read_ebp(void) {
    uint32_t ebp;
    asm volatile ("movl %%ebp, %0" : "=r" (ebp));
  100ada:	89 e8                	mov    %ebp,%eax
  100adc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return ebp;
  100adf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
      *    (3.4) call print_debuginfo(eip-1) to print the C calling function name and line number, etc.
      *    (3.5) popup a calling stackframe
      *           NOTICE: the calling funciton's return addr eip  = ss:[ebp+4]
      *                   the calling funciton's ebp = ss:[ebp]
      */
      uint32_t ebp=read_ebp();
  100ae2:	89 45 f4             	mov    %eax,-0xc(%ebp)
      uint32_t eip=read_eip();
  100ae5:	e8 d0 ff ff ff       	call   100aba <read_eip>
  100aea:	89 45 f0             	mov    %eax,-0x10(%ebp)
      for(int i=0;i<STACKFRAME_DEPTH&&ebp!=0;i++)
  100aed:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  100af4:	e9 8a 00 00 00       	jmp    100b83 <print_stackframe+0xb4>
      {
      cprintf(" ebp:0x%08x eip:0x%08x",ebp,eip);
  100af9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  100afc:	89 44 24 08          	mov    %eax,0x8(%esp)
  100b00:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100b03:	89 44 24 04          	mov    %eax,0x4(%esp)
  100b07:	c7 04 24 f4 63 10 00 	movl   $0x1063f4,(%esp)
  100b0e:	e8 a7 f7 ff ff       	call   1002ba <cprintf>
      uint32_t *arg=(uint32_t *)ebp+2;
  100b13:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100b16:	83 c0 08             	add    $0x8,%eax
  100b19:	89 45 e8             	mov    %eax,-0x18(%ebp)
      cprintf(" args:0x%08x 0x%08x 0x%08x 0x%08x",*(arg+0),*(arg+1),*(arg+2),*(arg+3));
  100b1c:	8b 45 e8             	mov    -0x18(%ebp),%eax
  100b1f:	83 c0 0c             	add    $0xc,%eax
  100b22:	8b 18                	mov    (%eax),%ebx
  100b24:	8b 45 e8             	mov    -0x18(%ebp),%eax
  100b27:	83 c0 08             	add    $0x8,%eax
  100b2a:	8b 08                	mov    (%eax),%ecx
  100b2c:	8b 45 e8             	mov    -0x18(%ebp),%eax
  100b2f:	83 c0 04             	add    $0x4,%eax
  100b32:	8b 10                	mov    (%eax),%edx
  100b34:	8b 45 e8             	mov    -0x18(%ebp),%eax
  100b37:	8b 00                	mov    (%eax),%eax
  100b39:	89 5c 24 10          	mov    %ebx,0x10(%esp)
  100b3d:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  100b41:	89 54 24 08          	mov    %edx,0x8(%esp)
  100b45:	89 44 24 04          	mov    %eax,0x4(%esp)
  100b49:	c7 04 24 0c 64 10 00 	movl   $0x10640c,(%esp)
  100b50:	e8 65 f7 ff ff       	call   1002ba <cprintf>
      cprintf("\n");
  100b55:	c7 04 24 2e 64 10 00 	movl   $0x10642e,(%esp)
  100b5c:	e8 59 f7 ff ff       	call   1002ba <cprintf>
      print_debuginfo(eip-1);
  100b61:	8b 45 f0             	mov    -0x10(%ebp),%eax
  100b64:	48                   	dec    %eax
  100b65:	89 04 24             	mov    %eax,(%esp)
  100b68:	e8 a6 fe ff ff       	call   100a13 <print_debuginfo>
      eip=((uint32_t *)ebp)[1];
  100b6d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100b70:	83 c0 04             	add    $0x4,%eax
  100b73:	8b 00                	mov    (%eax),%eax
  100b75:	89 45 f0             	mov    %eax,-0x10(%ebp)
      ebp=((uint32_t *)ebp)[0];
  100b78:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100b7b:	8b 00                	mov    (%eax),%eax
  100b7d:	89 45 f4             	mov    %eax,-0xc(%ebp)
      for(int i=0;i<STACKFRAME_DEPTH&&ebp!=0;i++)
  100b80:	ff 45 ec             	incl   -0x14(%ebp)
  100b83:	83 7d ec 13          	cmpl   $0x13,-0x14(%ebp)
  100b87:	7f 0a                	jg     100b93 <print_stackframe+0xc4>
  100b89:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  100b8d:	0f 85 66 ff ff ff    	jne    100af9 <print_stackframe+0x2a>
      }
}
  100b93:	90                   	nop
  100b94:	83 c4 44             	add    $0x44,%esp
  100b97:	5b                   	pop    %ebx
  100b98:	5d                   	pop    %ebp
  100b99:	c3                   	ret    

00100b9a <parse>:
#define MAXARGS         16
#define WHITESPACE      " \t\n\r"

/* parse - parse the command buffer into whitespace-separated arguments */
static int
parse(char *buf, char **argv) {
  100b9a:	f3 0f 1e fb          	endbr32 
  100b9e:	55                   	push   %ebp
  100b9f:	89 e5                	mov    %esp,%ebp
  100ba1:	83 ec 28             	sub    $0x28,%esp
    int argc = 0;
  100ba4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while (1) {
        // find global whitespace
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
  100bab:	eb 0c                	jmp    100bb9 <parse+0x1f>
            *buf ++ = '\0';
  100bad:	8b 45 08             	mov    0x8(%ebp),%eax
  100bb0:	8d 50 01             	lea    0x1(%eax),%edx
  100bb3:	89 55 08             	mov    %edx,0x8(%ebp)
  100bb6:	c6 00 00             	movb   $0x0,(%eax)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
  100bb9:	8b 45 08             	mov    0x8(%ebp),%eax
  100bbc:	0f b6 00             	movzbl (%eax),%eax
  100bbf:	84 c0                	test   %al,%al
  100bc1:	74 1d                	je     100be0 <parse+0x46>
  100bc3:	8b 45 08             	mov    0x8(%ebp),%eax
  100bc6:	0f b6 00             	movzbl (%eax),%eax
  100bc9:	0f be c0             	movsbl %al,%eax
  100bcc:	89 44 24 04          	mov    %eax,0x4(%esp)
  100bd0:	c7 04 24 b0 64 10 00 	movl   $0x1064b0,(%esp)
  100bd7:	e8 59 4c 00 00       	call   105835 <strchr>
  100bdc:	85 c0                	test   %eax,%eax
  100bde:	75 cd                	jne    100bad <parse+0x13>
        }
        if (*buf == '\0') {
  100be0:	8b 45 08             	mov    0x8(%ebp),%eax
  100be3:	0f b6 00             	movzbl (%eax),%eax
  100be6:	84 c0                	test   %al,%al
  100be8:	74 65                	je     100c4f <parse+0xb5>
            break;
        }

        // save and scan past next arg
        if (argc == MAXARGS - 1) {
  100bea:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
  100bee:	75 14                	jne    100c04 <parse+0x6a>
            cprintf("Too many arguments (max %d).\n", MAXARGS);
  100bf0:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
  100bf7:	00 
  100bf8:	c7 04 24 b5 64 10 00 	movl   $0x1064b5,(%esp)
  100bff:	e8 b6 f6 ff ff       	call   1002ba <cprintf>
        }
        argv[argc ++] = buf;
  100c04:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100c07:	8d 50 01             	lea    0x1(%eax),%edx
  100c0a:	89 55 f4             	mov    %edx,-0xc(%ebp)
  100c0d:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  100c14:	8b 45 0c             	mov    0xc(%ebp),%eax
  100c17:	01 c2                	add    %eax,%edx
  100c19:	8b 45 08             	mov    0x8(%ebp),%eax
  100c1c:	89 02                	mov    %eax,(%edx)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) == NULL) {
  100c1e:	eb 03                	jmp    100c23 <parse+0x89>
            buf ++;
  100c20:	ff 45 08             	incl   0x8(%ebp)
        while (*buf != '\0' && strchr(WHITESPACE, *buf) == NULL) {
  100c23:	8b 45 08             	mov    0x8(%ebp),%eax
  100c26:	0f b6 00             	movzbl (%eax),%eax
  100c29:	84 c0                	test   %al,%al
  100c2b:	74 8c                	je     100bb9 <parse+0x1f>
  100c2d:	8b 45 08             	mov    0x8(%ebp),%eax
  100c30:	0f b6 00             	movzbl (%eax),%eax
  100c33:	0f be c0             	movsbl %al,%eax
  100c36:	89 44 24 04          	mov    %eax,0x4(%esp)
  100c3a:	c7 04 24 b0 64 10 00 	movl   $0x1064b0,(%esp)
  100c41:	e8 ef 4b 00 00       	call   105835 <strchr>
  100c46:	85 c0                	test   %eax,%eax
  100c48:	74 d6                	je     100c20 <parse+0x86>
        while (*buf != '\0' && strchr(WHITESPACE, *buf) != NULL) {
  100c4a:	e9 6a ff ff ff       	jmp    100bb9 <parse+0x1f>
            break;
  100c4f:	90                   	nop
        }
    }
    return argc;
  100c50:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  100c53:	c9                   	leave  
  100c54:	c3                   	ret    

00100c55 <runcmd>:
/* *
 * runcmd - parse the input string, split it into separated arguments
 * and then lookup and invoke some related commands/
 * */
static int
runcmd(char *buf, struct trapframe *tf) {
  100c55:	f3 0f 1e fb          	endbr32 
  100c59:	55                   	push   %ebp
  100c5a:	89 e5                	mov    %esp,%ebp
  100c5c:	53                   	push   %ebx
  100c5d:	83 ec 64             	sub    $0x64,%esp
    char *argv[MAXARGS];
    int argc = parse(buf, argv);
  100c60:	8d 45 b0             	lea    -0x50(%ebp),%eax
  100c63:	89 44 24 04          	mov    %eax,0x4(%esp)
  100c67:	8b 45 08             	mov    0x8(%ebp),%eax
  100c6a:	89 04 24             	mov    %eax,(%esp)
  100c6d:	e8 28 ff ff ff       	call   100b9a <parse>
  100c72:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if (argc == 0) {
  100c75:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  100c79:	75 0a                	jne    100c85 <runcmd+0x30>
        return 0;
  100c7b:	b8 00 00 00 00       	mov    $0x0,%eax
  100c80:	e9 83 00 00 00       	jmp    100d08 <runcmd+0xb3>
    }
    int i;
    for (i = 0; i < NCOMMANDS; i ++) {
  100c85:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  100c8c:	eb 5a                	jmp    100ce8 <runcmd+0x93>
        if (strcmp(commands[i].name, argv[0]) == 0) {
  100c8e:	8b 4d b0             	mov    -0x50(%ebp),%ecx
  100c91:	8b 55 f4             	mov    -0xc(%ebp),%edx
  100c94:	89 d0                	mov    %edx,%eax
  100c96:	01 c0                	add    %eax,%eax
  100c98:	01 d0                	add    %edx,%eax
  100c9a:	c1 e0 02             	shl    $0x2,%eax
  100c9d:	05 00 90 11 00       	add    $0x119000,%eax
  100ca2:	8b 00                	mov    (%eax),%eax
  100ca4:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  100ca8:	89 04 24             	mov    %eax,(%esp)
  100cab:	e8 e1 4a 00 00       	call   105791 <strcmp>
  100cb0:	85 c0                	test   %eax,%eax
  100cb2:	75 31                	jne    100ce5 <runcmd+0x90>
            return commands[i].func(argc - 1, argv + 1, tf);
  100cb4:	8b 55 f4             	mov    -0xc(%ebp),%edx
  100cb7:	89 d0                	mov    %edx,%eax
  100cb9:	01 c0                	add    %eax,%eax
  100cbb:	01 d0                	add    %edx,%eax
  100cbd:	c1 e0 02             	shl    $0x2,%eax
  100cc0:	05 08 90 11 00       	add    $0x119008,%eax
  100cc5:	8b 10                	mov    (%eax),%edx
  100cc7:	8d 45 b0             	lea    -0x50(%ebp),%eax
  100cca:	83 c0 04             	add    $0x4,%eax
  100ccd:	8b 4d f0             	mov    -0x10(%ebp),%ecx
  100cd0:	8d 59 ff             	lea    -0x1(%ecx),%ebx
  100cd3:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  100cd6:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  100cda:	89 44 24 04          	mov    %eax,0x4(%esp)
  100cde:	89 1c 24             	mov    %ebx,(%esp)
  100ce1:	ff d2                	call   *%edx
  100ce3:	eb 23                	jmp    100d08 <runcmd+0xb3>
    for (i = 0; i < NCOMMANDS; i ++) {
  100ce5:	ff 45 f4             	incl   -0xc(%ebp)
  100ce8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100ceb:	83 f8 02             	cmp    $0x2,%eax
  100cee:	76 9e                	jbe    100c8e <runcmd+0x39>
        }
    }
    cprintf("Unknown command '%s'\n", argv[0]);
  100cf0:	8b 45 b0             	mov    -0x50(%ebp),%eax
  100cf3:	89 44 24 04          	mov    %eax,0x4(%esp)
  100cf7:	c7 04 24 d3 64 10 00 	movl   $0x1064d3,(%esp)
  100cfe:	e8 b7 f5 ff ff       	call   1002ba <cprintf>
    return 0;
  100d03:	b8 00 00 00 00       	mov    $0x0,%eax
}
  100d08:	83 c4 64             	add    $0x64,%esp
  100d0b:	5b                   	pop    %ebx
  100d0c:	5d                   	pop    %ebp
  100d0d:	c3                   	ret    

00100d0e <kmonitor>:

/***** Implementations of basic kernel monitor commands *****/

void
kmonitor(struct trapframe *tf) {
  100d0e:	f3 0f 1e fb          	endbr32 
  100d12:	55                   	push   %ebp
  100d13:	89 e5                	mov    %esp,%ebp
  100d15:	83 ec 28             	sub    $0x28,%esp
    cprintf("Welcome to the kernel debug monitor!!\n");
  100d18:	c7 04 24 ec 64 10 00 	movl   $0x1064ec,(%esp)
  100d1f:	e8 96 f5 ff ff       	call   1002ba <cprintf>
    cprintf("Type 'help' for a list of commands.\n");
  100d24:	c7 04 24 14 65 10 00 	movl   $0x106514,(%esp)
  100d2b:	e8 8a f5 ff ff       	call   1002ba <cprintf>

    if (tf != NULL) {
  100d30:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  100d34:	74 0b                	je     100d41 <kmonitor+0x33>
        print_trapframe(tf);
  100d36:	8b 45 08             	mov    0x8(%ebp),%eax
  100d39:	89 04 24             	mov    %eax,(%esp)
  100d3c:	e8 60 0e 00 00       	call   101ba1 <print_trapframe>
    }

    char *buf;
    while (1) {
        if ((buf = readline("K> ")) != NULL) {
  100d41:	c7 04 24 39 65 10 00 	movl   $0x106539,(%esp)
  100d48:	e8 20 f6 ff ff       	call   10036d <readline>
  100d4d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  100d50:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  100d54:	74 eb                	je     100d41 <kmonitor+0x33>
            if (runcmd(buf, tf) < 0) {
  100d56:	8b 45 08             	mov    0x8(%ebp),%eax
  100d59:	89 44 24 04          	mov    %eax,0x4(%esp)
  100d5d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100d60:	89 04 24             	mov    %eax,(%esp)
  100d63:	e8 ed fe ff ff       	call   100c55 <runcmd>
  100d68:	85 c0                	test   %eax,%eax
  100d6a:	78 02                	js     100d6e <kmonitor+0x60>
        if ((buf = readline("K> ")) != NULL) {
  100d6c:	eb d3                	jmp    100d41 <kmonitor+0x33>
                break;
  100d6e:	90                   	nop
            }
        }
    }
}
  100d6f:	90                   	nop
  100d70:	c9                   	leave  
  100d71:	c3                   	ret    

00100d72 <mon_help>:

/* mon_help - print the information about mon_* functions */
int
mon_help(int argc, char **argv, struct trapframe *tf) {
  100d72:	f3 0f 1e fb          	endbr32 
  100d76:	55                   	push   %ebp
  100d77:	89 e5                	mov    %esp,%ebp
  100d79:	83 ec 28             	sub    $0x28,%esp
    int i;
    for (i = 0; i < NCOMMANDS; i ++) {
  100d7c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  100d83:	eb 3d                	jmp    100dc2 <mon_help+0x50>
        cprintf("%s - %s\n", commands[i].name, commands[i].desc);
  100d85:	8b 55 f4             	mov    -0xc(%ebp),%edx
  100d88:	89 d0                	mov    %edx,%eax
  100d8a:	01 c0                	add    %eax,%eax
  100d8c:	01 d0                	add    %edx,%eax
  100d8e:	c1 e0 02             	shl    $0x2,%eax
  100d91:	05 04 90 11 00       	add    $0x119004,%eax
  100d96:	8b 08                	mov    (%eax),%ecx
  100d98:	8b 55 f4             	mov    -0xc(%ebp),%edx
  100d9b:	89 d0                	mov    %edx,%eax
  100d9d:	01 c0                	add    %eax,%eax
  100d9f:	01 d0                	add    %edx,%eax
  100da1:	c1 e0 02             	shl    $0x2,%eax
  100da4:	05 00 90 11 00       	add    $0x119000,%eax
  100da9:	8b 00                	mov    (%eax),%eax
  100dab:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  100daf:	89 44 24 04          	mov    %eax,0x4(%esp)
  100db3:	c7 04 24 3d 65 10 00 	movl   $0x10653d,(%esp)
  100dba:	e8 fb f4 ff ff       	call   1002ba <cprintf>
    for (i = 0; i < NCOMMANDS; i ++) {
  100dbf:	ff 45 f4             	incl   -0xc(%ebp)
  100dc2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100dc5:	83 f8 02             	cmp    $0x2,%eax
  100dc8:	76 bb                	jbe    100d85 <mon_help+0x13>
    }
    return 0;
  100dca:	b8 00 00 00 00       	mov    $0x0,%eax
}
  100dcf:	c9                   	leave  
  100dd0:	c3                   	ret    

00100dd1 <mon_kerninfo>:
/* *
 * mon_kerninfo - call print_kerninfo in kern/debug/kdebug.c to
 * print the memory occupancy in kernel.
 * */
int
mon_kerninfo(int argc, char **argv, struct trapframe *tf) {
  100dd1:	f3 0f 1e fb          	endbr32 
  100dd5:	55                   	push   %ebp
  100dd6:	89 e5                	mov    %esp,%ebp
  100dd8:	83 ec 08             	sub    $0x8,%esp
    print_kerninfo();
  100ddb:	e8 9d fb ff ff       	call   10097d <print_kerninfo>
    return 0;
  100de0:	b8 00 00 00 00       	mov    $0x0,%eax
}
  100de5:	c9                   	leave  
  100de6:	c3                   	ret    

00100de7 <mon_backtrace>:
/* *
 * mon_backtrace - call print_stackframe in kern/debug/kdebug.c to
 * print a backtrace of the stack.
 * */
int
mon_backtrace(int argc, char **argv, struct trapframe *tf) {
  100de7:	f3 0f 1e fb          	endbr32 
  100deb:	55                   	push   %ebp
  100dec:	89 e5                	mov    %esp,%ebp
  100dee:	83 ec 08             	sub    $0x8,%esp
    print_stackframe();
  100df1:	e8 d9 fc ff ff       	call   100acf <print_stackframe>
    return 0;
  100df6:	b8 00 00 00 00       	mov    $0x0,%eax
}
  100dfb:	c9                   	leave  
  100dfc:	c3                   	ret    

00100dfd <clock_init>:
/* *
 * clock_init - initialize 8253 clock to interrupt 100 times per second,
 * and then enable IRQ_TIMER.
 * */
void
clock_init(void) {
  100dfd:	f3 0f 1e fb          	endbr32 
  100e01:	55                   	push   %ebp
  100e02:	89 e5                	mov    %esp,%ebp
  100e04:	83 ec 28             	sub    $0x28,%esp
  100e07:	66 c7 45 ee 43 00    	movw   $0x43,-0x12(%ebp)
  100e0d:	c6 45 ed 34          	movb   $0x34,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100e11:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
  100e15:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
  100e19:	ee                   	out    %al,(%dx)
}
  100e1a:	90                   	nop
  100e1b:	66 c7 45 f2 40 00    	movw   $0x40,-0xe(%ebp)
  100e21:	c6 45 f1 9c          	movb   $0x9c,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100e25:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
  100e29:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
  100e2d:	ee                   	out    %al,(%dx)
}
  100e2e:	90                   	nop
  100e2f:	66 c7 45 f6 40 00    	movw   $0x40,-0xa(%ebp)
  100e35:	c6 45 f5 2e          	movb   $0x2e,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100e39:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
  100e3d:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
  100e41:	ee                   	out    %al,(%dx)
}
  100e42:	90                   	nop
    outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
    outb(IO_TIMER1, TIMER_DIV(100) % 256);
    outb(IO_TIMER1, TIMER_DIV(100) / 256);

    // initialize time counter 'ticks' to zero
    ticks = 0;
  100e43:	c7 05 0c cf 11 00 00 	movl   $0x0,0x11cf0c
  100e4a:	00 00 00 

    cprintf("++ setup timer interrupts\n");
  100e4d:	c7 04 24 46 65 10 00 	movl   $0x106546,(%esp)
  100e54:	e8 61 f4 ff ff       	call   1002ba <cprintf>
    pic_enable(IRQ_TIMER);
  100e59:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  100e60:	e8 95 09 00 00       	call   1017fa <pic_enable>
}
  100e65:	90                   	nop
  100e66:	c9                   	leave  
  100e67:	c3                   	ret    

00100e68 <__intr_save>:
#include <x86.h>
#include <intr.h>
#include <mmu.h>

static inline bool
__intr_save(void) {
  100e68:	55                   	push   %ebp
  100e69:	89 e5                	mov    %esp,%ebp
  100e6b:	83 ec 18             	sub    $0x18,%esp
}

static inline uint32_t
read_eflags(void) {
    uint32_t eflags;
    asm volatile ("pushfl; popl %0" : "=r" (eflags));
  100e6e:	9c                   	pushf  
  100e6f:	58                   	pop    %eax
  100e70:	89 45 f4             	mov    %eax,-0xc(%ebp)
    return eflags;
  100e73:	8b 45 f4             	mov    -0xc(%ebp),%eax
    if (read_eflags() & FL_IF) {
  100e76:	25 00 02 00 00       	and    $0x200,%eax
  100e7b:	85 c0                	test   %eax,%eax
  100e7d:	74 0c                	je     100e8b <__intr_save+0x23>
        intr_disable();
  100e7f:	e8 05 0b 00 00       	call   101989 <intr_disable>
        return 1;
  100e84:	b8 01 00 00 00       	mov    $0x1,%eax
  100e89:	eb 05                	jmp    100e90 <__intr_save+0x28>
    }
    return 0;
  100e8b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  100e90:	c9                   	leave  
  100e91:	c3                   	ret    

00100e92 <__intr_restore>:

static inline void
__intr_restore(bool flag) {
  100e92:	55                   	push   %ebp
  100e93:	89 e5                	mov    %esp,%ebp
  100e95:	83 ec 08             	sub    $0x8,%esp
    if (flag) {
  100e98:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  100e9c:	74 05                	je     100ea3 <__intr_restore+0x11>
        intr_enable();
  100e9e:	e8 da 0a 00 00       	call   10197d <intr_enable>
    }
}
  100ea3:	90                   	nop
  100ea4:	c9                   	leave  
  100ea5:	c3                   	ret    

00100ea6 <delay>:
#include <memlayout.h>
#include <sync.h>

/* stupid I/O delay routine necessitated by historical PC design flaws */
static void
delay(void) {
  100ea6:	f3 0f 1e fb          	endbr32 
  100eaa:	55                   	push   %ebp
  100eab:	89 e5                	mov    %esp,%ebp
  100ead:	83 ec 10             	sub    $0x10,%esp
  100eb0:	66 c7 45 f2 84 00    	movw   $0x84,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  100eb6:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
  100eba:	89 c2                	mov    %eax,%edx
  100ebc:	ec                   	in     (%dx),%al
  100ebd:	88 45 f1             	mov    %al,-0xf(%ebp)
  100ec0:	66 c7 45 f6 84 00    	movw   $0x84,-0xa(%ebp)
  100ec6:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
  100eca:	89 c2                	mov    %eax,%edx
  100ecc:	ec                   	in     (%dx),%al
  100ecd:	88 45 f5             	mov    %al,-0xb(%ebp)
  100ed0:	66 c7 45 fa 84 00    	movw   $0x84,-0x6(%ebp)
  100ed6:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
  100eda:	89 c2                	mov    %eax,%edx
  100edc:	ec                   	in     (%dx),%al
  100edd:	88 45 f9             	mov    %al,-0x7(%ebp)
  100ee0:	66 c7 45 fe 84 00    	movw   $0x84,-0x2(%ebp)
  100ee6:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
  100eea:	89 c2                	mov    %eax,%edx
  100eec:	ec                   	in     (%dx),%al
  100eed:	88 45 fd             	mov    %al,-0x3(%ebp)
    inb(0x84);
    inb(0x84);
    inb(0x84);
    inb(0x84);
}
  100ef0:	90                   	nop
  100ef1:	c9                   	leave  
  100ef2:	c3                   	ret    

00100ef3 <cga_init>:
static uint16_t addr_6845;

/* TEXT-mode CGA/VGA display output */

static void
cga_init(void) {
  100ef3:	f3 0f 1e fb          	endbr32 
  100ef7:	55                   	push   %ebp
  100ef8:	89 e5                	mov    %esp,%ebp
  100efa:	83 ec 20             	sub    $0x20,%esp
    volatile uint16_t *cp = (uint16_t *)(CGA_BUF + KERNBASE);
  100efd:	c7 45 fc 00 80 0b c0 	movl   $0xc00b8000,-0x4(%ebp)
    uint16_t was = *cp;
  100f04:	8b 45 fc             	mov    -0x4(%ebp),%eax
  100f07:	0f b7 00             	movzwl (%eax),%eax
  100f0a:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
    *cp = (uint16_t) 0xA55A;
  100f0e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  100f11:	66 c7 00 5a a5       	movw   $0xa55a,(%eax)
    if (*cp != 0xA55A) {
  100f16:	8b 45 fc             	mov    -0x4(%ebp),%eax
  100f19:	0f b7 00             	movzwl (%eax),%eax
  100f1c:	0f b7 c0             	movzwl %ax,%eax
  100f1f:	3d 5a a5 00 00       	cmp    $0xa55a,%eax
  100f24:	74 12                	je     100f38 <cga_init+0x45>
        cp = (uint16_t*)(MONO_BUF + KERNBASE);
  100f26:	c7 45 fc 00 00 0b c0 	movl   $0xc00b0000,-0x4(%ebp)
        addr_6845 = MONO_BASE;
  100f2d:	66 c7 05 46 c4 11 00 	movw   $0x3b4,0x11c446
  100f34:	b4 03 
  100f36:	eb 13                	jmp    100f4b <cga_init+0x58>
    } else {
        *cp = was;
  100f38:	8b 45 fc             	mov    -0x4(%ebp),%eax
  100f3b:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
  100f3f:	66 89 10             	mov    %dx,(%eax)
        addr_6845 = CGA_BASE;
  100f42:	66 c7 05 46 c4 11 00 	movw   $0x3d4,0x11c446
  100f49:	d4 03 
    }

    // Extract cursor location
    uint32_t pos;
    outb(addr_6845, 14);
  100f4b:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  100f52:	66 89 45 e6          	mov    %ax,-0x1a(%ebp)
  100f56:	c6 45 e5 0e          	movb   $0xe,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100f5a:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
  100f5e:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
  100f62:	ee                   	out    %al,(%dx)
}
  100f63:	90                   	nop
    pos = inb(addr_6845 + 1) << 8;
  100f64:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  100f6b:	40                   	inc    %eax
  100f6c:	0f b7 c0             	movzwl %ax,%eax
  100f6f:	66 89 45 ea          	mov    %ax,-0x16(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  100f73:	0f b7 45 ea          	movzwl -0x16(%ebp),%eax
  100f77:	89 c2                	mov    %eax,%edx
  100f79:	ec                   	in     (%dx),%al
  100f7a:	88 45 e9             	mov    %al,-0x17(%ebp)
    return data;
  100f7d:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
  100f81:	0f b6 c0             	movzbl %al,%eax
  100f84:	c1 e0 08             	shl    $0x8,%eax
  100f87:	89 45 f4             	mov    %eax,-0xc(%ebp)
    outb(addr_6845, 15);
  100f8a:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  100f91:	66 89 45 ee          	mov    %ax,-0x12(%ebp)
  100f95:	c6 45 ed 0f          	movb   $0xf,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100f99:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
  100f9d:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
  100fa1:	ee                   	out    %al,(%dx)
}
  100fa2:	90                   	nop
    pos |= inb(addr_6845 + 1);
  100fa3:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  100faa:	40                   	inc    %eax
  100fab:	0f b7 c0             	movzwl %ax,%eax
  100fae:	66 89 45 f2          	mov    %ax,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  100fb2:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
  100fb6:	89 c2                	mov    %eax,%edx
  100fb8:	ec                   	in     (%dx),%al
  100fb9:	88 45 f1             	mov    %al,-0xf(%ebp)
    return data;
  100fbc:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
  100fc0:	0f b6 c0             	movzbl %al,%eax
  100fc3:	09 45 f4             	or     %eax,-0xc(%ebp)

    crt_buf = (uint16_t*) cp;
  100fc6:	8b 45 fc             	mov    -0x4(%ebp),%eax
  100fc9:	a3 40 c4 11 00       	mov    %eax,0x11c440
    crt_pos = pos;
  100fce:	8b 45 f4             	mov    -0xc(%ebp),%eax
  100fd1:	0f b7 c0             	movzwl %ax,%eax
  100fd4:	66 a3 44 c4 11 00    	mov    %ax,0x11c444
}
  100fda:	90                   	nop
  100fdb:	c9                   	leave  
  100fdc:	c3                   	ret    

00100fdd <serial_init>:

static bool serial_exists = 0;

static void
serial_init(void) {
  100fdd:	f3 0f 1e fb          	endbr32 
  100fe1:	55                   	push   %ebp
  100fe2:	89 e5                	mov    %esp,%ebp
  100fe4:	83 ec 48             	sub    $0x48,%esp
  100fe7:	66 c7 45 d2 fa 03    	movw   $0x3fa,-0x2e(%ebp)
  100fed:	c6 45 d1 00          	movb   $0x0,-0x2f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  100ff1:	0f b6 45 d1          	movzbl -0x2f(%ebp),%eax
  100ff5:	0f b7 55 d2          	movzwl -0x2e(%ebp),%edx
  100ff9:	ee                   	out    %al,(%dx)
}
  100ffa:	90                   	nop
  100ffb:	66 c7 45 d6 fb 03    	movw   $0x3fb,-0x2a(%ebp)
  101001:	c6 45 d5 80          	movb   $0x80,-0x2b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101005:	0f b6 45 d5          	movzbl -0x2b(%ebp),%eax
  101009:	0f b7 55 d6          	movzwl -0x2a(%ebp),%edx
  10100d:	ee                   	out    %al,(%dx)
}
  10100e:	90                   	nop
  10100f:	66 c7 45 da f8 03    	movw   $0x3f8,-0x26(%ebp)
  101015:	c6 45 d9 0c          	movb   $0xc,-0x27(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101019:	0f b6 45 d9          	movzbl -0x27(%ebp),%eax
  10101d:	0f b7 55 da          	movzwl -0x26(%ebp),%edx
  101021:	ee                   	out    %al,(%dx)
}
  101022:	90                   	nop
  101023:	66 c7 45 de f9 03    	movw   $0x3f9,-0x22(%ebp)
  101029:	c6 45 dd 00          	movb   $0x0,-0x23(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10102d:	0f b6 45 dd          	movzbl -0x23(%ebp),%eax
  101031:	0f b7 55 de          	movzwl -0x22(%ebp),%edx
  101035:	ee                   	out    %al,(%dx)
}
  101036:	90                   	nop
  101037:	66 c7 45 e2 fb 03    	movw   $0x3fb,-0x1e(%ebp)
  10103d:	c6 45 e1 03          	movb   $0x3,-0x1f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101041:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
  101045:	0f b7 55 e2          	movzwl -0x1e(%ebp),%edx
  101049:	ee                   	out    %al,(%dx)
}
  10104a:	90                   	nop
  10104b:	66 c7 45 e6 fc 03    	movw   $0x3fc,-0x1a(%ebp)
  101051:	c6 45 e5 00          	movb   $0x0,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101055:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
  101059:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
  10105d:	ee                   	out    %al,(%dx)
}
  10105e:	90                   	nop
  10105f:	66 c7 45 ea f9 03    	movw   $0x3f9,-0x16(%ebp)
  101065:	c6 45 e9 01          	movb   $0x1,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101069:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
  10106d:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
  101071:	ee                   	out    %al,(%dx)
}
  101072:	90                   	nop
  101073:	66 c7 45 ee fd 03    	movw   $0x3fd,-0x12(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  101079:	0f b7 45 ee          	movzwl -0x12(%ebp),%eax
  10107d:	89 c2                	mov    %eax,%edx
  10107f:	ec                   	in     (%dx),%al
  101080:	88 45 ed             	mov    %al,-0x13(%ebp)
    return data;
  101083:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
    // Enable rcv interrupts
    outb(COM1 + COM_IER, COM_IER_RDI);

    // Clear any preexisting overrun indications and interrupts
    // Serial port doesn't exist if COM_LSR returns 0xFF
    serial_exists = (inb(COM1 + COM_LSR) != 0xFF);
  101087:	3c ff                	cmp    $0xff,%al
  101089:	0f 95 c0             	setne  %al
  10108c:	0f b6 c0             	movzbl %al,%eax
  10108f:	a3 48 c4 11 00       	mov    %eax,0x11c448
  101094:	66 c7 45 f2 fa 03    	movw   $0x3fa,-0xe(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  10109a:	0f b7 45 f2          	movzwl -0xe(%ebp),%eax
  10109e:	89 c2                	mov    %eax,%edx
  1010a0:	ec                   	in     (%dx),%al
  1010a1:	88 45 f1             	mov    %al,-0xf(%ebp)
  1010a4:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
  1010aa:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
  1010ae:	89 c2                	mov    %eax,%edx
  1010b0:	ec                   	in     (%dx),%al
  1010b1:	88 45 f5             	mov    %al,-0xb(%ebp)
    (void) inb(COM1+COM_IIR);
    (void) inb(COM1+COM_RX);

    if (serial_exists) {
  1010b4:	a1 48 c4 11 00       	mov    0x11c448,%eax
  1010b9:	85 c0                	test   %eax,%eax
  1010bb:	74 0c                	je     1010c9 <serial_init+0xec>
        pic_enable(IRQ_COM1);
  1010bd:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  1010c4:	e8 31 07 00 00       	call   1017fa <pic_enable>
    }
}
  1010c9:	90                   	nop
  1010ca:	c9                   	leave  
  1010cb:	c3                   	ret    

001010cc <lpt_putc_sub>:

static void
lpt_putc_sub(int c) {
  1010cc:	f3 0f 1e fb          	endbr32 
  1010d0:	55                   	push   %ebp
  1010d1:	89 e5                	mov    %esp,%ebp
  1010d3:	83 ec 20             	sub    $0x20,%esp
    int i;
    for (i = 0; !(inb(LPTPORT + 1) & 0x80) && i < 12800; i ++) {
  1010d6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  1010dd:	eb 08                	jmp    1010e7 <lpt_putc_sub+0x1b>
        delay();
  1010df:	e8 c2 fd ff ff       	call   100ea6 <delay>
    for (i = 0; !(inb(LPTPORT + 1) & 0x80) && i < 12800; i ++) {
  1010e4:	ff 45 fc             	incl   -0x4(%ebp)
  1010e7:	66 c7 45 fa 79 03    	movw   $0x379,-0x6(%ebp)
  1010ed:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
  1010f1:	89 c2                	mov    %eax,%edx
  1010f3:	ec                   	in     (%dx),%al
  1010f4:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
  1010f7:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
  1010fb:	84 c0                	test   %al,%al
  1010fd:	78 09                	js     101108 <lpt_putc_sub+0x3c>
  1010ff:	81 7d fc ff 31 00 00 	cmpl   $0x31ff,-0x4(%ebp)
  101106:	7e d7                	jle    1010df <lpt_putc_sub+0x13>
    }
    outb(LPTPORT + 0, c);
  101108:	8b 45 08             	mov    0x8(%ebp),%eax
  10110b:	0f b6 c0             	movzbl %al,%eax
  10110e:	66 c7 45 ee 78 03    	movw   $0x378,-0x12(%ebp)
  101114:	88 45 ed             	mov    %al,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101117:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
  10111b:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
  10111f:	ee                   	out    %al,(%dx)
}
  101120:	90                   	nop
  101121:	66 c7 45 f2 7a 03    	movw   $0x37a,-0xe(%ebp)
  101127:	c6 45 f1 0d          	movb   $0xd,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10112b:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
  10112f:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
  101133:	ee                   	out    %al,(%dx)
}
  101134:	90                   	nop
  101135:	66 c7 45 f6 7a 03    	movw   $0x37a,-0xa(%ebp)
  10113b:	c6 45 f5 08          	movb   $0x8,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10113f:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
  101143:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
  101147:	ee                   	out    %al,(%dx)
}
  101148:	90                   	nop
    outb(LPTPORT + 2, 0x08 | 0x04 | 0x01);
    outb(LPTPORT + 2, 0x08);
}
  101149:	90                   	nop
  10114a:	c9                   	leave  
  10114b:	c3                   	ret    

0010114c <lpt_putc>:

/* lpt_putc - copy console output to parallel port */
static void
lpt_putc(int c) {
  10114c:	f3 0f 1e fb          	endbr32 
  101150:	55                   	push   %ebp
  101151:	89 e5                	mov    %esp,%ebp
  101153:	83 ec 04             	sub    $0x4,%esp
    if (c != '\b') {
  101156:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
  10115a:	74 0d                	je     101169 <lpt_putc+0x1d>
        lpt_putc_sub(c);
  10115c:	8b 45 08             	mov    0x8(%ebp),%eax
  10115f:	89 04 24             	mov    %eax,(%esp)
  101162:	e8 65 ff ff ff       	call   1010cc <lpt_putc_sub>
    else {
        lpt_putc_sub('\b');
        lpt_putc_sub(' ');
        lpt_putc_sub('\b');
    }
}
  101167:	eb 24                	jmp    10118d <lpt_putc+0x41>
        lpt_putc_sub('\b');
  101169:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  101170:	e8 57 ff ff ff       	call   1010cc <lpt_putc_sub>
        lpt_putc_sub(' ');
  101175:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  10117c:	e8 4b ff ff ff       	call   1010cc <lpt_putc_sub>
        lpt_putc_sub('\b');
  101181:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  101188:	e8 3f ff ff ff       	call   1010cc <lpt_putc_sub>
}
  10118d:	90                   	nop
  10118e:	c9                   	leave  
  10118f:	c3                   	ret    

00101190 <cga_putc>:

/* cga_putc - print character to console */
static void
cga_putc(int c) {
  101190:	f3 0f 1e fb          	endbr32 
  101194:	55                   	push   %ebp
  101195:	89 e5                	mov    %esp,%ebp
  101197:	53                   	push   %ebx
  101198:	83 ec 34             	sub    $0x34,%esp
    // set black on white
    if (!(c & ~0xFF)) {
  10119b:	8b 45 08             	mov    0x8(%ebp),%eax
  10119e:	25 00 ff ff ff       	and    $0xffffff00,%eax
  1011a3:	85 c0                	test   %eax,%eax
  1011a5:	75 07                	jne    1011ae <cga_putc+0x1e>
        c |= 0x0700;
  1011a7:	81 4d 08 00 07 00 00 	orl    $0x700,0x8(%ebp)
    }

    switch (c & 0xff) {
  1011ae:	8b 45 08             	mov    0x8(%ebp),%eax
  1011b1:	0f b6 c0             	movzbl %al,%eax
  1011b4:	83 f8 0d             	cmp    $0xd,%eax
  1011b7:	74 72                	je     10122b <cga_putc+0x9b>
  1011b9:	83 f8 0d             	cmp    $0xd,%eax
  1011bc:	0f 8f a3 00 00 00    	jg     101265 <cga_putc+0xd5>
  1011c2:	83 f8 08             	cmp    $0x8,%eax
  1011c5:	74 0a                	je     1011d1 <cga_putc+0x41>
  1011c7:	83 f8 0a             	cmp    $0xa,%eax
  1011ca:	74 4c                	je     101218 <cga_putc+0x88>
  1011cc:	e9 94 00 00 00       	jmp    101265 <cga_putc+0xd5>
    case '\b':
        if (crt_pos > 0) {
  1011d1:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  1011d8:	85 c0                	test   %eax,%eax
  1011da:	0f 84 af 00 00 00    	je     10128f <cga_putc+0xff>
            crt_pos --;
  1011e0:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  1011e7:	48                   	dec    %eax
  1011e8:	0f b7 c0             	movzwl %ax,%eax
  1011eb:	66 a3 44 c4 11 00    	mov    %ax,0x11c444
            crt_buf[crt_pos] = (c & ~0xff) | ' ';
  1011f1:	8b 45 08             	mov    0x8(%ebp),%eax
  1011f4:	98                   	cwtl   
  1011f5:	25 00 ff ff ff       	and    $0xffffff00,%eax
  1011fa:	98                   	cwtl   
  1011fb:	83 c8 20             	or     $0x20,%eax
  1011fe:	98                   	cwtl   
  1011ff:	8b 15 40 c4 11 00    	mov    0x11c440,%edx
  101205:	0f b7 0d 44 c4 11 00 	movzwl 0x11c444,%ecx
  10120c:	01 c9                	add    %ecx,%ecx
  10120e:	01 ca                	add    %ecx,%edx
  101210:	0f b7 c0             	movzwl %ax,%eax
  101213:	66 89 02             	mov    %ax,(%edx)
        }
        break;
  101216:	eb 77                	jmp    10128f <cga_putc+0xff>
    case '\n':
        crt_pos += CRT_COLS;
  101218:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  10121f:	83 c0 50             	add    $0x50,%eax
  101222:	0f b7 c0             	movzwl %ax,%eax
  101225:	66 a3 44 c4 11 00    	mov    %ax,0x11c444
    case '\r':
        crt_pos -= (crt_pos % CRT_COLS);
  10122b:	0f b7 1d 44 c4 11 00 	movzwl 0x11c444,%ebx
  101232:	0f b7 0d 44 c4 11 00 	movzwl 0x11c444,%ecx
  101239:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
  10123e:	89 c8                	mov    %ecx,%eax
  101240:	f7 e2                	mul    %edx
  101242:	c1 ea 06             	shr    $0x6,%edx
  101245:	89 d0                	mov    %edx,%eax
  101247:	c1 e0 02             	shl    $0x2,%eax
  10124a:	01 d0                	add    %edx,%eax
  10124c:	c1 e0 04             	shl    $0x4,%eax
  10124f:	29 c1                	sub    %eax,%ecx
  101251:	89 c8                	mov    %ecx,%eax
  101253:	0f b7 c0             	movzwl %ax,%eax
  101256:	29 c3                	sub    %eax,%ebx
  101258:	89 d8                	mov    %ebx,%eax
  10125a:	0f b7 c0             	movzwl %ax,%eax
  10125d:	66 a3 44 c4 11 00    	mov    %ax,0x11c444
        break;
  101263:	eb 2b                	jmp    101290 <cga_putc+0x100>
    default:
        crt_buf[crt_pos ++] = c;     // write the character
  101265:	8b 0d 40 c4 11 00    	mov    0x11c440,%ecx
  10126b:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  101272:	8d 50 01             	lea    0x1(%eax),%edx
  101275:	0f b7 d2             	movzwl %dx,%edx
  101278:	66 89 15 44 c4 11 00 	mov    %dx,0x11c444
  10127f:	01 c0                	add    %eax,%eax
  101281:	8d 14 01             	lea    (%ecx,%eax,1),%edx
  101284:	8b 45 08             	mov    0x8(%ebp),%eax
  101287:	0f b7 c0             	movzwl %ax,%eax
  10128a:	66 89 02             	mov    %ax,(%edx)
        break;
  10128d:	eb 01                	jmp    101290 <cga_putc+0x100>
        break;
  10128f:	90                   	nop
    }

    // What is the purpose of this?
    if (crt_pos >= CRT_SIZE) {
  101290:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  101297:	3d cf 07 00 00       	cmp    $0x7cf,%eax
  10129c:	76 5d                	jbe    1012fb <cga_putc+0x16b>
        int i;
        memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
  10129e:	a1 40 c4 11 00       	mov    0x11c440,%eax
  1012a3:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
  1012a9:	a1 40 c4 11 00       	mov    0x11c440,%eax
  1012ae:	c7 44 24 08 00 0f 00 	movl   $0xf00,0x8(%esp)
  1012b5:	00 
  1012b6:	89 54 24 04          	mov    %edx,0x4(%esp)
  1012ba:	89 04 24             	mov    %eax,(%esp)
  1012bd:	e8 78 47 00 00       	call   105a3a <memmove>
        for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i ++) {
  1012c2:	c7 45 f4 80 07 00 00 	movl   $0x780,-0xc(%ebp)
  1012c9:	eb 14                	jmp    1012df <cga_putc+0x14f>
            crt_buf[i] = 0x0700 | ' ';
  1012cb:	a1 40 c4 11 00       	mov    0x11c440,%eax
  1012d0:	8b 55 f4             	mov    -0xc(%ebp),%edx
  1012d3:	01 d2                	add    %edx,%edx
  1012d5:	01 d0                	add    %edx,%eax
  1012d7:	66 c7 00 20 07       	movw   $0x720,(%eax)
        for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i ++) {
  1012dc:	ff 45 f4             	incl   -0xc(%ebp)
  1012df:	81 7d f4 cf 07 00 00 	cmpl   $0x7cf,-0xc(%ebp)
  1012e6:	7e e3                	jle    1012cb <cga_putc+0x13b>
        }
        crt_pos -= CRT_COLS;
  1012e8:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  1012ef:	83 e8 50             	sub    $0x50,%eax
  1012f2:	0f b7 c0             	movzwl %ax,%eax
  1012f5:	66 a3 44 c4 11 00    	mov    %ax,0x11c444
    }

    // move that little blinky thing
    outb(addr_6845, 14);
  1012fb:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  101302:	66 89 45 e6          	mov    %ax,-0x1a(%ebp)
  101306:	c6 45 e5 0e          	movb   $0xe,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10130a:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
  10130e:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
  101312:	ee                   	out    %al,(%dx)
}
  101313:	90                   	nop
    outb(addr_6845 + 1, crt_pos >> 8);
  101314:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  10131b:	c1 e8 08             	shr    $0x8,%eax
  10131e:	0f b7 c0             	movzwl %ax,%eax
  101321:	0f b6 c0             	movzbl %al,%eax
  101324:	0f b7 15 46 c4 11 00 	movzwl 0x11c446,%edx
  10132b:	42                   	inc    %edx
  10132c:	0f b7 d2             	movzwl %dx,%edx
  10132f:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
  101333:	88 45 e9             	mov    %al,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101336:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
  10133a:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
  10133e:	ee                   	out    %al,(%dx)
}
  10133f:	90                   	nop
    outb(addr_6845, 15);
  101340:	0f b7 05 46 c4 11 00 	movzwl 0x11c446,%eax
  101347:	66 89 45 ee          	mov    %ax,-0x12(%ebp)
  10134b:	c6 45 ed 0f          	movb   $0xf,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10134f:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
  101353:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
  101357:	ee                   	out    %al,(%dx)
}
  101358:	90                   	nop
    outb(addr_6845 + 1, crt_pos);
  101359:	0f b7 05 44 c4 11 00 	movzwl 0x11c444,%eax
  101360:	0f b6 c0             	movzbl %al,%eax
  101363:	0f b7 15 46 c4 11 00 	movzwl 0x11c446,%edx
  10136a:	42                   	inc    %edx
  10136b:	0f b7 d2             	movzwl %dx,%edx
  10136e:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  101372:	88 45 f1             	mov    %al,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101375:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
  101379:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
  10137d:	ee                   	out    %al,(%dx)
}
  10137e:	90                   	nop
}
  10137f:	90                   	nop
  101380:	83 c4 34             	add    $0x34,%esp
  101383:	5b                   	pop    %ebx
  101384:	5d                   	pop    %ebp
  101385:	c3                   	ret    

00101386 <serial_putc_sub>:

static void
serial_putc_sub(int c) {
  101386:	f3 0f 1e fb          	endbr32 
  10138a:	55                   	push   %ebp
  10138b:	89 e5                	mov    %esp,%ebp
  10138d:	83 ec 10             	sub    $0x10,%esp
    int i;
    for (i = 0; !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800; i ++) {
  101390:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  101397:	eb 08                	jmp    1013a1 <serial_putc_sub+0x1b>
        delay();
  101399:	e8 08 fb ff ff       	call   100ea6 <delay>
    for (i = 0; !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800; i ++) {
  10139e:	ff 45 fc             	incl   -0x4(%ebp)
  1013a1:	66 c7 45 fa fd 03    	movw   $0x3fd,-0x6(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  1013a7:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
  1013ab:	89 c2                	mov    %eax,%edx
  1013ad:	ec                   	in     (%dx),%al
  1013ae:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
  1013b1:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
  1013b5:	0f b6 c0             	movzbl %al,%eax
  1013b8:	83 e0 20             	and    $0x20,%eax
  1013bb:	85 c0                	test   %eax,%eax
  1013bd:	75 09                	jne    1013c8 <serial_putc_sub+0x42>
  1013bf:	81 7d fc ff 31 00 00 	cmpl   $0x31ff,-0x4(%ebp)
  1013c6:	7e d1                	jle    101399 <serial_putc_sub+0x13>
    }
    outb(COM1 + COM_TX, c);
  1013c8:	8b 45 08             	mov    0x8(%ebp),%eax
  1013cb:	0f b6 c0             	movzbl %al,%eax
  1013ce:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
  1013d4:	88 45 f5             	mov    %al,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1013d7:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
  1013db:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
  1013df:	ee                   	out    %al,(%dx)
}
  1013e0:	90                   	nop
}
  1013e1:	90                   	nop
  1013e2:	c9                   	leave  
  1013e3:	c3                   	ret    

001013e4 <serial_putc>:

/* serial_putc - print character to serial port */
static void
serial_putc(int c) {
  1013e4:	f3 0f 1e fb          	endbr32 
  1013e8:	55                   	push   %ebp
  1013e9:	89 e5                	mov    %esp,%ebp
  1013eb:	83 ec 04             	sub    $0x4,%esp
    if (c != '\b') {
  1013ee:	83 7d 08 08          	cmpl   $0x8,0x8(%ebp)
  1013f2:	74 0d                	je     101401 <serial_putc+0x1d>
        serial_putc_sub(c);
  1013f4:	8b 45 08             	mov    0x8(%ebp),%eax
  1013f7:	89 04 24             	mov    %eax,(%esp)
  1013fa:	e8 87 ff ff ff       	call   101386 <serial_putc_sub>
    else {
        serial_putc_sub('\b');
        serial_putc_sub(' ');
        serial_putc_sub('\b');
    }
}
  1013ff:	eb 24                	jmp    101425 <serial_putc+0x41>
        serial_putc_sub('\b');
  101401:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  101408:	e8 79 ff ff ff       	call   101386 <serial_putc_sub>
        serial_putc_sub(' ');
  10140d:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  101414:	e8 6d ff ff ff       	call   101386 <serial_putc_sub>
        serial_putc_sub('\b');
  101419:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
  101420:	e8 61 ff ff ff       	call   101386 <serial_putc_sub>
}
  101425:	90                   	nop
  101426:	c9                   	leave  
  101427:	c3                   	ret    

00101428 <cons_intr>:
/* *
 * cons_intr - called by device interrupt routines to feed input
 * characters into the circular console input buffer.
 * */
static void
cons_intr(int (*proc)(void)) {
  101428:	f3 0f 1e fb          	endbr32 
  10142c:	55                   	push   %ebp
  10142d:	89 e5                	mov    %esp,%ebp
  10142f:	83 ec 18             	sub    $0x18,%esp
    int c;
    while ((c = (*proc)()) != -1) {
  101432:	eb 33                	jmp    101467 <cons_intr+0x3f>
        if (c != 0) {
  101434:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  101438:	74 2d                	je     101467 <cons_intr+0x3f>
            cons.buf[cons.wpos ++] = c;
  10143a:	a1 64 c6 11 00       	mov    0x11c664,%eax
  10143f:	8d 50 01             	lea    0x1(%eax),%edx
  101442:	89 15 64 c6 11 00    	mov    %edx,0x11c664
  101448:	8b 55 f4             	mov    -0xc(%ebp),%edx
  10144b:	88 90 60 c4 11 00    	mov    %dl,0x11c460(%eax)
            if (cons.wpos == CONSBUFSIZE) {
  101451:	a1 64 c6 11 00       	mov    0x11c664,%eax
  101456:	3d 00 02 00 00       	cmp    $0x200,%eax
  10145b:	75 0a                	jne    101467 <cons_intr+0x3f>
                cons.wpos = 0;
  10145d:	c7 05 64 c6 11 00 00 	movl   $0x0,0x11c664
  101464:	00 00 00 
    while ((c = (*proc)()) != -1) {
  101467:	8b 45 08             	mov    0x8(%ebp),%eax
  10146a:	ff d0                	call   *%eax
  10146c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  10146f:	83 7d f4 ff          	cmpl   $0xffffffff,-0xc(%ebp)
  101473:	75 bf                	jne    101434 <cons_intr+0xc>
            }
        }
    }
}
  101475:	90                   	nop
  101476:	90                   	nop
  101477:	c9                   	leave  
  101478:	c3                   	ret    

00101479 <serial_proc_data>:

/* serial_proc_data - get data from serial port */
static int
serial_proc_data(void) {
  101479:	f3 0f 1e fb          	endbr32 
  10147d:	55                   	push   %ebp
  10147e:	89 e5                	mov    %esp,%ebp
  101480:	83 ec 10             	sub    $0x10,%esp
  101483:	66 c7 45 fa fd 03    	movw   $0x3fd,-0x6(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  101489:	0f b7 45 fa          	movzwl -0x6(%ebp),%eax
  10148d:	89 c2                	mov    %eax,%edx
  10148f:	ec                   	in     (%dx),%al
  101490:	88 45 f9             	mov    %al,-0x7(%ebp)
    return data;
  101493:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
    if (!(inb(COM1 + COM_LSR) & COM_LSR_DATA)) {
  101497:	0f b6 c0             	movzbl %al,%eax
  10149a:	83 e0 01             	and    $0x1,%eax
  10149d:	85 c0                	test   %eax,%eax
  10149f:	75 07                	jne    1014a8 <serial_proc_data+0x2f>
        return -1;
  1014a1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  1014a6:	eb 2a                	jmp    1014d2 <serial_proc_data+0x59>
  1014a8:	66 c7 45 f6 f8 03    	movw   $0x3f8,-0xa(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  1014ae:	0f b7 45 f6          	movzwl -0xa(%ebp),%eax
  1014b2:	89 c2                	mov    %eax,%edx
  1014b4:	ec                   	in     (%dx),%al
  1014b5:	88 45 f5             	mov    %al,-0xb(%ebp)
    return data;
  1014b8:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
    }
    int c = inb(COM1 + COM_RX);
  1014bc:	0f b6 c0             	movzbl %al,%eax
  1014bf:	89 45 fc             	mov    %eax,-0x4(%ebp)
    if (c == 127) {
  1014c2:	83 7d fc 7f          	cmpl   $0x7f,-0x4(%ebp)
  1014c6:	75 07                	jne    1014cf <serial_proc_data+0x56>
        c = '\b';
  1014c8:	c7 45 fc 08 00 00 00 	movl   $0x8,-0x4(%ebp)
    }
    return c;
  1014cf:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  1014d2:	c9                   	leave  
  1014d3:	c3                   	ret    

001014d4 <serial_intr>:

/* serial_intr - try to feed input characters from serial port */
void
serial_intr(void) {
  1014d4:	f3 0f 1e fb          	endbr32 
  1014d8:	55                   	push   %ebp
  1014d9:	89 e5                	mov    %esp,%ebp
  1014db:	83 ec 18             	sub    $0x18,%esp
    if (serial_exists) {
  1014de:	a1 48 c4 11 00       	mov    0x11c448,%eax
  1014e3:	85 c0                	test   %eax,%eax
  1014e5:	74 0c                	je     1014f3 <serial_intr+0x1f>
        cons_intr(serial_proc_data);
  1014e7:	c7 04 24 79 14 10 00 	movl   $0x101479,(%esp)
  1014ee:	e8 35 ff ff ff       	call   101428 <cons_intr>
    }
}
  1014f3:	90                   	nop
  1014f4:	c9                   	leave  
  1014f5:	c3                   	ret    

001014f6 <kbd_proc_data>:
 *
 * The kbd_proc_data() function gets data from the keyboard.
 * If we finish a character, return it, else 0. And return -1 if no data.
 * */
static int
kbd_proc_data(void) {
  1014f6:	f3 0f 1e fb          	endbr32 
  1014fa:	55                   	push   %ebp
  1014fb:	89 e5                	mov    %esp,%ebp
  1014fd:	83 ec 38             	sub    $0x38,%esp
  101500:	66 c7 45 f0 64 00    	movw   $0x64,-0x10(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  101506:	8b 45 f0             	mov    -0x10(%ebp),%eax
  101509:	89 c2                	mov    %eax,%edx
  10150b:	ec                   	in     (%dx),%al
  10150c:	88 45 ef             	mov    %al,-0x11(%ebp)
    return data;
  10150f:	0f b6 45 ef          	movzbl -0x11(%ebp),%eax
    int c;
    uint8_t data;
    static uint32_t shift;

    if ((inb(KBSTATP) & KBS_DIB) == 0) {
  101513:	0f b6 c0             	movzbl %al,%eax
  101516:	83 e0 01             	and    $0x1,%eax
  101519:	85 c0                	test   %eax,%eax
  10151b:	75 0a                	jne    101527 <kbd_proc_data+0x31>
        return -1;
  10151d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  101522:	e9 56 01 00 00       	jmp    10167d <kbd_proc_data+0x187>
  101527:	66 c7 45 ec 60 00    	movw   $0x60,-0x14(%ebp)
    asm volatile ("inb %1, %0" : "=a" (data) : "d" (port) : "memory");
  10152d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  101530:	89 c2                	mov    %eax,%edx
  101532:	ec                   	in     (%dx),%al
  101533:	88 45 eb             	mov    %al,-0x15(%ebp)
    return data;
  101536:	0f b6 45 eb          	movzbl -0x15(%ebp),%eax
    }

    data = inb(KBDATAP);
  10153a:	88 45 f3             	mov    %al,-0xd(%ebp)

    if (data == 0xE0) {
  10153d:	80 7d f3 e0          	cmpb   $0xe0,-0xd(%ebp)
  101541:	75 17                	jne    10155a <kbd_proc_data+0x64>
        // E0 escape character
        shift |= E0ESC;
  101543:	a1 68 c6 11 00       	mov    0x11c668,%eax
  101548:	83 c8 40             	or     $0x40,%eax
  10154b:	a3 68 c6 11 00       	mov    %eax,0x11c668
        return 0;
  101550:	b8 00 00 00 00       	mov    $0x0,%eax
  101555:	e9 23 01 00 00       	jmp    10167d <kbd_proc_data+0x187>
    } else if (data & 0x80) {
  10155a:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  10155e:	84 c0                	test   %al,%al
  101560:	79 45                	jns    1015a7 <kbd_proc_data+0xb1>
        // Key released
        data = (shift & E0ESC ? data : data & 0x7F);
  101562:	a1 68 c6 11 00       	mov    0x11c668,%eax
  101567:	83 e0 40             	and    $0x40,%eax
  10156a:	85 c0                	test   %eax,%eax
  10156c:	75 08                	jne    101576 <kbd_proc_data+0x80>
  10156e:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  101572:	24 7f                	and    $0x7f,%al
  101574:	eb 04                	jmp    10157a <kbd_proc_data+0x84>
  101576:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  10157a:	88 45 f3             	mov    %al,-0xd(%ebp)
        shift &= ~(shiftcode[data] | E0ESC);
  10157d:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  101581:	0f b6 80 40 90 11 00 	movzbl 0x119040(%eax),%eax
  101588:	0c 40                	or     $0x40,%al
  10158a:	0f b6 c0             	movzbl %al,%eax
  10158d:	f7 d0                	not    %eax
  10158f:	89 c2                	mov    %eax,%edx
  101591:	a1 68 c6 11 00       	mov    0x11c668,%eax
  101596:	21 d0                	and    %edx,%eax
  101598:	a3 68 c6 11 00       	mov    %eax,0x11c668
        return 0;
  10159d:	b8 00 00 00 00       	mov    $0x0,%eax
  1015a2:	e9 d6 00 00 00       	jmp    10167d <kbd_proc_data+0x187>
    } else if (shift & E0ESC) {
  1015a7:	a1 68 c6 11 00       	mov    0x11c668,%eax
  1015ac:	83 e0 40             	and    $0x40,%eax
  1015af:	85 c0                	test   %eax,%eax
  1015b1:	74 11                	je     1015c4 <kbd_proc_data+0xce>
        // Last character was an E0 escape; or with 0x80
        data |= 0x80;
  1015b3:	80 4d f3 80          	orb    $0x80,-0xd(%ebp)
        shift &= ~E0ESC;
  1015b7:	a1 68 c6 11 00       	mov    0x11c668,%eax
  1015bc:	83 e0 bf             	and    $0xffffffbf,%eax
  1015bf:	a3 68 c6 11 00       	mov    %eax,0x11c668
    }

    shift |= shiftcode[data];
  1015c4:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  1015c8:	0f b6 80 40 90 11 00 	movzbl 0x119040(%eax),%eax
  1015cf:	0f b6 d0             	movzbl %al,%edx
  1015d2:	a1 68 c6 11 00       	mov    0x11c668,%eax
  1015d7:	09 d0                	or     %edx,%eax
  1015d9:	a3 68 c6 11 00       	mov    %eax,0x11c668
    shift ^= togglecode[data];
  1015de:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  1015e2:	0f b6 80 40 91 11 00 	movzbl 0x119140(%eax),%eax
  1015e9:	0f b6 d0             	movzbl %al,%edx
  1015ec:	a1 68 c6 11 00       	mov    0x11c668,%eax
  1015f1:	31 d0                	xor    %edx,%eax
  1015f3:	a3 68 c6 11 00       	mov    %eax,0x11c668

    c = charcode[shift & (CTL | SHIFT)][data];
  1015f8:	a1 68 c6 11 00       	mov    0x11c668,%eax
  1015fd:	83 e0 03             	and    $0x3,%eax
  101600:	8b 14 85 40 95 11 00 	mov    0x119540(,%eax,4),%edx
  101607:	0f b6 45 f3          	movzbl -0xd(%ebp),%eax
  10160b:	01 d0                	add    %edx,%eax
  10160d:	0f b6 00             	movzbl (%eax),%eax
  101610:	0f b6 c0             	movzbl %al,%eax
  101613:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (shift & CAPSLOCK) {
  101616:	a1 68 c6 11 00       	mov    0x11c668,%eax
  10161b:	83 e0 08             	and    $0x8,%eax
  10161e:	85 c0                	test   %eax,%eax
  101620:	74 22                	je     101644 <kbd_proc_data+0x14e>
        if ('a' <= c && c <= 'z')
  101622:	83 7d f4 60          	cmpl   $0x60,-0xc(%ebp)
  101626:	7e 0c                	jle    101634 <kbd_proc_data+0x13e>
  101628:	83 7d f4 7a          	cmpl   $0x7a,-0xc(%ebp)
  10162c:	7f 06                	jg     101634 <kbd_proc_data+0x13e>
            c += 'A' - 'a';
  10162e:	83 6d f4 20          	subl   $0x20,-0xc(%ebp)
  101632:	eb 10                	jmp    101644 <kbd_proc_data+0x14e>
        else if ('A' <= c && c <= 'Z')
  101634:	83 7d f4 40          	cmpl   $0x40,-0xc(%ebp)
  101638:	7e 0a                	jle    101644 <kbd_proc_data+0x14e>
  10163a:	83 7d f4 5a          	cmpl   $0x5a,-0xc(%ebp)
  10163e:	7f 04                	jg     101644 <kbd_proc_data+0x14e>
            c += 'a' - 'A';
  101640:	83 45 f4 20          	addl   $0x20,-0xc(%ebp)
    }

    // Process special keys
    // Ctrl-Alt-Del: reboot
    if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
  101644:	a1 68 c6 11 00       	mov    0x11c668,%eax
  101649:	f7 d0                	not    %eax
  10164b:	83 e0 06             	and    $0x6,%eax
  10164e:	85 c0                	test   %eax,%eax
  101650:	75 28                	jne    10167a <kbd_proc_data+0x184>
  101652:	81 7d f4 e9 00 00 00 	cmpl   $0xe9,-0xc(%ebp)
  101659:	75 1f                	jne    10167a <kbd_proc_data+0x184>
        cprintf("Rebooting!\n");
  10165b:	c7 04 24 61 65 10 00 	movl   $0x106561,(%esp)
  101662:	e8 53 ec ff ff       	call   1002ba <cprintf>
  101667:	66 c7 45 e8 92 00    	movw   $0x92,-0x18(%ebp)
  10166d:	c6 45 e7 03          	movb   $0x3,-0x19(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101671:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
  101675:	8b 55 e8             	mov    -0x18(%ebp),%edx
  101678:	ee                   	out    %al,(%dx)
}
  101679:	90                   	nop
        outb(0x92, 0x3); // courtesy of Chris Frost
    }
    return c;
  10167a:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  10167d:	c9                   	leave  
  10167e:	c3                   	ret    

0010167f <kbd_intr>:

/* kbd_intr - try to feed input characters from keyboard */
static void
kbd_intr(void) {
  10167f:	f3 0f 1e fb          	endbr32 
  101683:	55                   	push   %ebp
  101684:	89 e5                	mov    %esp,%ebp
  101686:	83 ec 18             	sub    $0x18,%esp
    cons_intr(kbd_proc_data);
  101689:	c7 04 24 f6 14 10 00 	movl   $0x1014f6,(%esp)
  101690:	e8 93 fd ff ff       	call   101428 <cons_intr>
}
  101695:	90                   	nop
  101696:	c9                   	leave  
  101697:	c3                   	ret    

00101698 <kbd_init>:

static void
kbd_init(void) {
  101698:	f3 0f 1e fb          	endbr32 
  10169c:	55                   	push   %ebp
  10169d:	89 e5                	mov    %esp,%ebp
  10169f:	83 ec 18             	sub    $0x18,%esp
    // drain the kbd buffer
    kbd_intr();
  1016a2:	e8 d8 ff ff ff       	call   10167f <kbd_intr>
    pic_enable(IRQ_KBD);
  1016a7:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  1016ae:	e8 47 01 00 00       	call   1017fa <pic_enable>
}
  1016b3:	90                   	nop
  1016b4:	c9                   	leave  
  1016b5:	c3                   	ret    

001016b6 <cons_init>:

/* cons_init - initializes the console devices */
void
cons_init(void) {
  1016b6:	f3 0f 1e fb          	endbr32 
  1016ba:	55                   	push   %ebp
  1016bb:	89 e5                	mov    %esp,%ebp
  1016bd:	83 ec 18             	sub    $0x18,%esp
    cga_init();
  1016c0:	e8 2e f8 ff ff       	call   100ef3 <cga_init>
    serial_init();
  1016c5:	e8 13 f9 ff ff       	call   100fdd <serial_init>
    kbd_init();
  1016ca:	e8 c9 ff ff ff       	call   101698 <kbd_init>
    if (!serial_exists) {
  1016cf:	a1 48 c4 11 00       	mov    0x11c448,%eax
  1016d4:	85 c0                	test   %eax,%eax
  1016d6:	75 0c                	jne    1016e4 <cons_init+0x2e>
        cprintf("serial port does not exist!!\n");
  1016d8:	c7 04 24 6d 65 10 00 	movl   $0x10656d,(%esp)
  1016df:	e8 d6 eb ff ff       	call   1002ba <cprintf>
    }
}
  1016e4:	90                   	nop
  1016e5:	c9                   	leave  
  1016e6:	c3                   	ret    

001016e7 <cons_putc>:

/* cons_putc - print a single character @c to console devices */
void
cons_putc(int c) {
  1016e7:	f3 0f 1e fb          	endbr32 
  1016eb:	55                   	push   %ebp
  1016ec:	89 e5                	mov    %esp,%ebp
  1016ee:	83 ec 28             	sub    $0x28,%esp
    bool intr_flag;
    local_intr_save(intr_flag);
  1016f1:	e8 72 f7 ff ff       	call   100e68 <__intr_save>
  1016f6:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        lpt_putc(c);
  1016f9:	8b 45 08             	mov    0x8(%ebp),%eax
  1016fc:	89 04 24             	mov    %eax,(%esp)
  1016ff:	e8 48 fa ff ff       	call   10114c <lpt_putc>
        cga_putc(c);
  101704:	8b 45 08             	mov    0x8(%ebp),%eax
  101707:	89 04 24             	mov    %eax,(%esp)
  10170a:	e8 81 fa ff ff       	call   101190 <cga_putc>
        serial_putc(c);
  10170f:	8b 45 08             	mov    0x8(%ebp),%eax
  101712:	89 04 24             	mov    %eax,(%esp)
  101715:	e8 ca fc ff ff       	call   1013e4 <serial_putc>
    }
    local_intr_restore(intr_flag);
  10171a:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10171d:	89 04 24             	mov    %eax,(%esp)
  101720:	e8 6d f7 ff ff       	call   100e92 <__intr_restore>
}
  101725:	90                   	nop
  101726:	c9                   	leave  
  101727:	c3                   	ret    

00101728 <cons_getc>:
/* *
 * cons_getc - return the next input character from console,
 * or 0 if none waiting.
 * */
int
cons_getc(void) {
  101728:	f3 0f 1e fb          	endbr32 
  10172c:	55                   	push   %ebp
  10172d:	89 e5                	mov    %esp,%ebp
  10172f:	83 ec 28             	sub    $0x28,%esp
    int c = 0;
  101732:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    bool intr_flag;
    local_intr_save(intr_flag);
  101739:	e8 2a f7 ff ff       	call   100e68 <__intr_save>
  10173e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    {
        // poll for any pending input characters,
        // so that this function works even when interrupts are disabled
        // (e.g., when called from the kernel monitor).
        serial_intr();
  101741:	e8 8e fd ff ff       	call   1014d4 <serial_intr>
        kbd_intr();
  101746:	e8 34 ff ff ff       	call   10167f <kbd_intr>

        // grab the next character from the input buffer.
        if (cons.rpos != cons.wpos) {
  10174b:	8b 15 60 c6 11 00    	mov    0x11c660,%edx
  101751:	a1 64 c6 11 00       	mov    0x11c664,%eax
  101756:	39 c2                	cmp    %eax,%edx
  101758:	74 31                	je     10178b <cons_getc+0x63>
            c = cons.buf[cons.rpos ++];
  10175a:	a1 60 c6 11 00       	mov    0x11c660,%eax
  10175f:	8d 50 01             	lea    0x1(%eax),%edx
  101762:	89 15 60 c6 11 00    	mov    %edx,0x11c660
  101768:	0f b6 80 60 c4 11 00 	movzbl 0x11c460(%eax),%eax
  10176f:	0f b6 c0             	movzbl %al,%eax
  101772:	89 45 f4             	mov    %eax,-0xc(%ebp)
            if (cons.rpos == CONSBUFSIZE) {
  101775:	a1 60 c6 11 00       	mov    0x11c660,%eax
  10177a:	3d 00 02 00 00       	cmp    $0x200,%eax
  10177f:	75 0a                	jne    10178b <cons_getc+0x63>
                cons.rpos = 0;
  101781:	c7 05 60 c6 11 00 00 	movl   $0x0,0x11c660
  101788:	00 00 00 
            }
        }
    }
    local_intr_restore(intr_flag);
  10178b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  10178e:	89 04 24             	mov    %eax,(%esp)
  101791:	e8 fc f6 ff ff       	call   100e92 <__intr_restore>
    return c;
  101796:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  101799:	c9                   	leave  
  10179a:	c3                   	ret    

0010179b <pic_setmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static uint16_t irq_mask = 0xFFFF & ~(1 << IRQ_SLAVE);
static bool did_init = 0;

static void
pic_setmask(uint16_t mask) {
  10179b:	f3 0f 1e fb          	endbr32 
  10179f:	55                   	push   %ebp
  1017a0:	89 e5                	mov    %esp,%ebp
  1017a2:	83 ec 14             	sub    $0x14,%esp
  1017a5:	8b 45 08             	mov    0x8(%ebp),%eax
  1017a8:	66 89 45 ec          	mov    %ax,-0x14(%ebp)
    irq_mask = mask;
  1017ac:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1017af:	66 a3 50 95 11 00    	mov    %ax,0x119550
    if (did_init) {
  1017b5:	a1 6c c6 11 00       	mov    0x11c66c,%eax
  1017ba:	85 c0                	test   %eax,%eax
  1017bc:	74 39                	je     1017f7 <pic_setmask+0x5c>
        outb(IO_PIC1 + 1, mask);
  1017be:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1017c1:	0f b6 c0             	movzbl %al,%eax
  1017c4:	66 c7 45 fa 21 00    	movw   $0x21,-0x6(%ebp)
  1017ca:	88 45 f9             	mov    %al,-0x7(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1017cd:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
  1017d1:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
  1017d5:	ee                   	out    %al,(%dx)
}
  1017d6:	90                   	nop
        outb(IO_PIC2 + 1, mask >> 8);
  1017d7:	0f b7 45 ec          	movzwl -0x14(%ebp),%eax
  1017db:	c1 e8 08             	shr    $0x8,%eax
  1017de:	0f b7 c0             	movzwl %ax,%eax
  1017e1:	0f b6 c0             	movzbl %al,%eax
  1017e4:	66 c7 45 fe a1 00    	movw   $0xa1,-0x2(%ebp)
  1017ea:	88 45 fd             	mov    %al,-0x3(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1017ed:	0f b6 45 fd          	movzbl -0x3(%ebp),%eax
  1017f1:	0f b7 55 fe          	movzwl -0x2(%ebp),%edx
  1017f5:	ee                   	out    %al,(%dx)
}
  1017f6:	90                   	nop
    }
}
  1017f7:	90                   	nop
  1017f8:	c9                   	leave  
  1017f9:	c3                   	ret    

001017fa <pic_enable>:

void
pic_enable(unsigned int irq) {
  1017fa:	f3 0f 1e fb          	endbr32 
  1017fe:	55                   	push   %ebp
  1017ff:	89 e5                	mov    %esp,%ebp
  101801:	83 ec 04             	sub    $0x4,%esp
    pic_setmask(irq_mask & ~(1 << irq));
  101804:	8b 45 08             	mov    0x8(%ebp),%eax
  101807:	ba 01 00 00 00       	mov    $0x1,%edx
  10180c:	88 c1                	mov    %al,%cl
  10180e:	d3 e2                	shl    %cl,%edx
  101810:	89 d0                	mov    %edx,%eax
  101812:	98                   	cwtl   
  101813:	f7 d0                	not    %eax
  101815:	0f bf d0             	movswl %ax,%edx
  101818:	0f b7 05 50 95 11 00 	movzwl 0x119550,%eax
  10181f:	98                   	cwtl   
  101820:	21 d0                	and    %edx,%eax
  101822:	98                   	cwtl   
  101823:	0f b7 c0             	movzwl %ax,%eax
  101826:	89 04 24             	mov    %eax,(%esp)
  101829:	e8 6d ff ff ff       	call   10179b <pic_setmask>
}
  10182e:	90                   	nop
  10182f:	c9                   	leave  
  101830:	c3                   	ret    

00101831 <pic_init>:

/* pic_init - initialize the 8259A interrupt controllers */
void
pic_init(void) {
  101831:	f3 0f 1e fb          	endbr32 
  101835:	55                   	push   %ebp
  101836:	89 e5                	mov    %esp,%ebp
  101838:	83 ec 44             	sub    $0x44,%esp
    did_init = 1;
  10183b:	c7 05 6c c6 11 00 01 	movl   $0x1,0x11c66c
  101842:	00 00 00 
  101845:	66 c7 45 ca 21 00    	movw   $0x21,-0x36(%ebp)
  10184b:	c6 45 c9 ff          	movb   $0xff,-0x37(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10184f:	0f b6 45 c9          	movzbl -0x37(%ebp),%eax
  101853:	0f b7 55 ca          	movzwl -0x36(%ebp),%edx
  101857:	ee                   	out    %al,(%dx)
}
  101858:	90                   	nop
  101859:	66 c7 45 ce a1 00    	movw   $0xa1,-0x32(%ebp)
  10185f:	c6 45 cd ff          	movb   $0xff,-0x33(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101863:	0f b6 45 cd          	movzbl -0x33(%ebp),%eax
  101867:	0f b7 55 ce          	movzwl -0x32(%ebp),%edx
  10186b:	ee                   	out    %al,(%dx)
}
  10186c:	90                   	nop
  10186d:	66 c7 45 d2 20 00    	movw   $0x20,-0x2e(%ebp)
  101873:	c6 45 d1 11          	movb   $0x11,-0x2f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101877:	0f b6 45 d1          	movzbl -0x2f(%ebp),%eax
  10187b:	0f b7 55 d2          	movzwl -0x2e(%ebp),%edx
  10187f:	ee                   	out    %al,(%dx)
}
  101880:	90                   	nop
  101881:	66 c7 45 d6 21 00    	movw   $0x21,-0x2a(%ebp)
  101887:	c6 45 d5 20          	movb   $0x20,-0x2b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10188b:	0f b6 45 d5          	movzbl -0x2b(%ebp),%eax
  10188f:	0f b7 55 d6          	movzwl -0x2a(%ebp),%edx
  101893:	ee                   	out    %al,(%dx)
}
  101894:	90                   	nop
  101895:	66 c7 45 da 21 00    	movw   $0x21,-0x26(%ebp)
  10189b:	c6 45 d9 04          	movb   $0x4,-0x27(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10189f:	0f b6 45 d9          	movzbl -0x27(%ebp),%eax
  1018a3:	0f b7 55 da          	movzwl -0x26(%ebp),%edx
  1018a7:	ee                   	out    %al,(%dx)
}
  1018a8:	90                   	nop
  1018a9:	66 c7 45 de 21 00    	movw   $0x21,-0x22(%ebp)
  1018af:	c6 45 dd 03          	movb   $0x3,-0x23(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1018b3:	0f b6 45 dd          	movzbl -0x23(%ebp),%eax
  1018b7:	0f b7 55 de          	movzwl -0x22(%ebp),%edx
  1018bb:	ee                   	out    %al,(%dx)
}
  1018bc:	90                   	nop
  1018bd:	66 c7 45 e2 a0 00    	movw   $0xa0,-0x1e(%ebp)
  1018c3:	c6 45 e1 11          	movb   $0x11,-0x1f(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1018c7:	0f b6 45 e1          	movzbl -0x1f(%ebp),%eax
  1018cb:	0f b7 55 e2          	movzwl -0x1e(%ebp),%edx
  1018cf:	ee                   	out    %al,(%dx)
}
  1018d0:	90                   	nop
  1018d1:	66 c7 45 e6 a1 00    	movw   $0xa1,-0x1a(%ebp)
  1018d7:	c6 45 e5 28          	movb   $0x28,-0x1b(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1018db:	0f b6 45 e5          	movzbl -0x1b(%ebp),%eax
  1018df:	0f b7 55 e6          	movzwl -0x1a(%ebp),%edx
  1018e3:	ee                   	out    %al,(%dx)
}
  1018e4:	90                   	nop
  1018e5:	66 c7 45 ea a1 00    	movw   $0xa1,-0x16(%ebp)
  1018eb:	c6 45 e9 02          	movb   $0x2,-0x17(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  1018ef:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
  1018f3:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
  1018f7:	ee                   	out    %al,(%dx)
}
  1018f8:	90                   	nop
  1018f9:	66 c7 45 ee a1 00    	movw   $0xa1,-0x12(%ebp)
  1018ff:	c6 45 ed 03          	movb   $0x3,-0x13(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101903:	0f b6 45 ed          	movzbl -0x13(%ebp),%eax
  101907:	0f b7 55 ee          	movzwl -0x12(%ebp),%edx
  10190b:	ee                   	out    %al,(%dx)
}
  10190c:	90                   	nop
  10190d:	66 c7 45 f2 20 00    	movw   $0x20,-0xe(%ebp)
  101913:	c6 45 f1 68          	movb   $0x68,-0xf(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101917:	0f b6 45 f1          	movzbl -0xf(%ebp),%eax
  10191b:	0f b7 55 f2          	movzwl -0xe(%ebp),%edx
  10191f:	ee                   	out    %al,(%dx)
}
  101920:	90                   	nop
  101921:	66 c7 45 f6 20 00    	movw   $0x20,-0xa(%ebp)
  101927:	c6 45 f5 0a          	movb   $0xa,-0xb(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10192b:	0f b6 45 f5          	movzbl -0xb(%ebp),%eax
  10192f:	0f b7 55 f6          	movzwl -0xa(%ebp),%edx
  101933:	ee                   	out    %al,(%dx)
}
  101934:	90                   	nop
  101935:	66 c7 45 fa a0 00    	movw   $0xa0,-0x6(%ebp)
  10193b:	c6 45 f9 68          	movb   $0x68,-0x7(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  10193f:	0f b6 45 f9          	movzbl -0x7(%ebp),%eax
  101943:	0f b7 55 fa          	movzwl -0x6(%ebp),%edx
  101947:	ee                   	out    %al,(%dx)
}
  101948:	90                   	nop
  101949:	66 c7 45 fe a0 00    	movw   $0xa0,-0x2(%ebp)
  10194f:	c6 45 fd 0a          	movb   $0xa,-0x3(%ebp)
    asm volatile ("outb %0, %1" :: "a" (data), "d" (port) : "memory");
  101953:	0f b6 45 fd          	movzbl -0x3(%ebp),%eax
  101957:	0f b7 55 fe          	movzwl -0x2(%ebp),%edx
  10195b:	ee                   	out    %al,(%dx)
}
  10195c:	90                   	nop
    outb(IO_PIC1, 0x0a);    // read IRR by default

    outb(IO_PIC2, 0x68);    // OCW3
    outb(IO_PIC2, 0x0a);    // OCW3

    if (irq_mask != 0xFFFF) {
  10195d:	0f b7 05 50 95 11 00 	movzwl 0x119550,%eax
  101964:	3d ff ff 00 00       	cmp    $0xffff,%eax
  101969:	74 0f                	je     10197a <pic_init+0x149>
        pic_setmask(irq_mask);
  10196b:	0f b7 05 50 95 11 00 	movzwl 0x119550,%eax
  101972:	89 04 24             	mov    %eax,(%esp)
  101975:	e8 21 fe ff ff       	call   10179b <pic_setmask>
    }
}
  10197a:	90                   	nop
  10197b:	c9                   	leave  
  10197c:	c3                   	ret    

0010197d <intr_enable>:
#include <x86.h>
#include <intr.h>

/* intr_enable - enable irq interrupt */
void
intr_enable(void) {
  10197d:	f3 0f 1e fb          	endbr32 
  101981:	55                   	push   %ebp
  101982:	89 e5                	mov    %esp,%ebp
    asm volatile ("sti");
  101984:	fb                   	sti    
}
  101985:	90                   	nop
    sti();
}
  101986:	90                   	nop
  101987:	5d                   	pop    %ebp
  101988:	c3                   	ret    

00101989 <intr_disable>:

/* intr_disable - disable irq interrupt */
void
intr_disable(void) {
  101989:	f3 0f 1e fb          	endbr32 
  10198d:	55                   	push   %ebp
  10198e:	89 e5                	mov    %esp,%ebp
    asm volatile ("cli" ::: "memory");
  101990:	fa                   	cli    
}
  101991:	90                   	nop
    cli();
}
  101992:	90                   	nop
  101993:	5d                   	pop    %ebp
  101994:	c3                   	ret    

00101995 <print_ticks>:
#include <console.h>
#include <kdebug.h>

#define TICK_NUM 100

static void print_ticks() {
  101995:	f3 0f 1e fb          	endbr32 
  101999:	55                   	push   %ebp
  10199a:	89 e5                	mov    %esp,%ebp
  10199c:	83 ec 18             	sub    $0x18,%esp
    cprintf("%d ticks\n",TICK_NUM);
  10199f:	c7 44 24 04 64 00 00 	movl   $0x64,0x4(%esp)
  1019a6:	00 
  1019a7:	c7 04 24 a0 65 10 00 	movl   $0x1065a0,(%esp)
  1019ae:	e8 07 e9 ff ff       	call   1002ba <cprintf>
#ifdef DEBUG_GRADE
    cprintf("End of Test.\n");
  1019b3:	c7 04 24 aa 65 10 00 	movl   $0x1065aa,(%esp)
  1019ba:	e8 fb e8 ff ff       	call   1002ba <cprintf>
    panic("EOT: kernel seems ok.");
  1019bf:	c7 44 24 08 b8 65 10 	movl   $0x1065b8,0x8(%esp)
  1019c6:	00 
  1019c7:	c7 44 24 04 12 00 00 	movl   $0x12,0x4(%esp)
  1019ce:	00 
  1019cf:	c7 04 24 ce 65 10 00 	movl   $0x1065ce,(%esp)
  1019d6:	e8 4b ea ff ff       	call   100426 <__panic>

001019db <idt_init>:
    sizeof(idt) - 1, (uintptr_t)idt
};

/* idt_init - initialize IDT to each of the entry points in kern/trap/vectors.S */
void
idt_init(void) {
  1019db:	f3 0f 1e fb          	endbr32 
  1019df:	55                   	push   %ebp
  1019e0:	89 e5                	mov    %esp,%ebp
  1019e2:	83 ec 10             	sub    $0x10,%esp
      *     You don't know the meaning of this instruction? just google it! and check the libs/x86.h to know more.
      *     Notice: the argument of lidt is idt_pd. try to find it!
      */
      extern uintptr_t __vectors[];
      int i;
      for(i=0;i<sizeof(idt)/sizeof(struct gatedesc);i++)
  1019e5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
  1019ec:	e9 50 01 00 00       	jmp    101b41 <idt_init+0x166>
      {
      SETGATE(idt[i],0,GD_KTEXT,__vectors[i],DPL_KERNEL);
  1019f1:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1019f4:	8b 04 85 e0 95 11 00 	mov    0x1195e0(,%eax,4),%eax
  1019fb:	0f b7 d0             	movzwl %ax,%edx
  1019fe:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a01:	66 89 14 c5 80 c6 11 	mov    %dx,0x11c680(,%eax,8)
  101a08:	00 
  101a09:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a0c:	66 c7 04 c5 82 c6 11 	movw   $0x8,0x11c682(,%eax,8)
  101a13:	00 08 00 
  101a16:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a19:	0f b6 14 c5 84 c6 11 	movzbl 0x11c684(,%eax,8),%edx
  101a20:	00 
  101a21:	80 e2 e0             	and    $0xe0,%dl
  101a24:	88 14 c5 84 c6 11 00 	mov    %dl,0x11c684(,%eax,8)
  101a2b:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a2e:	0f b6 14 c5 84 c6 11 	movzbl 0x11c684(,%eax,8),%edx
  101a35:	00 
  101a36:	80 e2 1f             	and    $0x1f,%dl
  101a39:	88 14 c5 84 c6 11 00 	mov    %dl,0x11c684(,%eax,8)
  101a40:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a43:	0f b6 14 c5 85 c6 11 	movzbl 0x11c685(,%eax,8),%edx
  101a4a:	00 
  101a4b:	80 e2 f0             	and    $0xf0,%dl
  101a4e:	80 ca 0e             	or     $0xe,%dl
  101a51:	88 14 c5 85 c6 11 00 	mov    %dl,0x11c685(,%eax,8)
  101a58:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a5b:	0f b6 14 c5 85 c6 11 	movzbl 0x11c685(,%eax,8),%edx
  101a62:	00 
  101a63:	80 e2 ef             	and    $0xef,%dl
  101a66:	88 14 c5 85 c6 11 00 	mov    %dl,0x11c685(,%eax,8)
  101a6d:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a70:	0f b6 14 c5 85 c6 11 	movzbl 0x11c685(,%eax,8),%edx
  101a77:	00 
  101a78:	80 e2 9f             	and    $0x9f,%dl
  101a7b:	88 14 c5 85 c6 11 00 	mov    %dl,0x11c685(,%eax,8)
  101a82:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a85:	0f b6 14 c5 85 c6 11 	movzbl 0x11c685(,%eax,8),%edx
  101a8c:	00 
  101a8d:	80 ca 80             	or     $0x80,%dl
  101a90:	88 14 c5 85 c6 11 00 	mov    %dl,0x11c685(,%eax,8)
  101a97:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101a9a:	8b 04 85 e0 95 11 00 	mov    0x1195e0(,%eax,4),%eax
  101aa1:	c1 e8 10             	shr    $0x10,%eax
  101aa4:	0f b7 d0             	movzwl %ax,%edx
  101aa7:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101aaa:	66 89 14 c5 86 c6 11 	mov    %dx,0x11c686(,%eax,8)
  101ab1:	00 
      SETGATE(idt[T_SWITCH_TOK],0,GD_KTEXT,__vectors[T_SWITCH_TOK],DPL_USER);
  101ab2:	a1 c4 97 11 00       	mov    0x1197c4,%eax
  101ab7:	0f b7 c0             	movzwl %ax,%eax
  101aba:	66 a3 48 ca 11 00    	mov    %ax,0x11ca48
  101ac0:	66 c7 05 4a ca 11 00 	movw   $0x8,0x11ca4a
  101ac7:	08 00 
  101ac9:	0f b6 05 4c ca 11 00 	movzbl 0x11ca4c,%eax
  101ad0:	24 e0                	and    $0xe0,%al
  101ad2:	a2 4c ca 11 00       	mov    %al,0x11ca4c
  101ad7:	0f b6 05 4c ca 11 00 	movzbl 0x11ca4c,%eax
  101ade:	24 1f                	and    $0x1f,%al
  101ae0:	a2 4c ca 11 00       	mov    %al,0x11ca4c
  101ae5:	0f b6 05 4d ca 11 00 	movzbl 0x11ca4d,%eax
  101aec:	24 f0                	and    $0xf0,%al
  101aee:	0c 0e                	or     $0xe,%al
  101af0:	a2 4d ca 11 00       	mov    %al,0x11ca4d
  101af5:	0f b6 05 4d ca 11 00 	movzbl 0x11ca4d,%eax
  101afc:	24 ef                	and    $0xef,%al
  101afe:	a2 4d ca 11 00       	mov    %al,0x11ca4d
  101b03:	0f b6 05 4d ca 11 00 	movzbl 0x11ca4d,%eax
  101b0a:	0c 60                	or     $0x60,%al
  101b0c:	a2 4d ca 11 00       	mov    %al,0x11ca4d
  101b11:	0f b6 05 4d ca 11 00 	movzbl 0x11ca4d,%eax
  101b18:	0c 80                	or     $0x80,%al
  101b1a:	a2 4d ca 11 00       	mov    %al,0x11ca4d
  101b1f:	a1 c4 97 11 00       	mov    0x1197c4,%eax
  101b24:	c1 e8 10             	shr    $0x10,%eax
  101b27:	0f b7 c0             	movzwl %ax,%eax
  101b2a:	66 a3 4e ca 11 00    	mov    %ax,0x11ca4e
  101b30:	c7 45 f8 60 95 11 00 	movl   $0x119560,-0x8(%ebp)
    asm volatile ("lidt (%0)" :: "r" (pd) : "memory");
  101b37:	8b 45 f8             	mov    -0x8(%ebp),%eax
  101b3a:	0f 01 18             	lidtl  (%eax)
}
  101b3d:	90                   	nop
      for(i=0;i<sizeof(idt)/sizeof(struct gatedesc);i++)
  101b3e:	ff 45 fc             	incl   -0x4(%ebp)
  101b41:	8b 45 fc             	mov    -0x4(%ebp),%eax
  101b44:	3d ff 00 00 00       	cmp    $0xff,%eax
  101b49:	0f 86 a2 fe ff ff    	jbe    1019f1 <idt_init+0x16>
      lidt(&idt_pd);
      }
}
  101b4f:	90                   	nop
  101b50:	90                   	nop
  101b51:	c9                   	leave  
  101b52:	c3                   	ret    

00101b53 <trapname>:

static const char *
trapname(int trapno) {
  101b53:	f3 0f 1e fb          	endbr32 
  101b57:	55                   	push   %ebp
  101b58:	89 e5                	mov    %esp,%ebp
        "Alignment Check",
        "Machine-Check",
        "SIMD Floating-Point Exception"
    };

    if (trapno < sizeof(excnames)/sizeof(const char * const)) {
  101b5a:	8b 45 08             	mov    0x8(%ebp),%eax
  101b5d:	83 f8 13             	cmp    $0x13,%eax
  101b60:	77 0c                	ja     101b6e <trapname+0x1b>
        return excnames[trapno];
  101b62:	8b 45 08             	mov    0x8(%ebp),%eax
  101b65:	8b 04 85 20 69 10 00 	mov    0x106920(,%eax,4),%eax
  101b6c:	eb 18                	jmp    101b86 <trapname+0x33>
    }
    if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16) {
  101b6e:	83 7d 08 1f          	cmpl   $0x1f,0x8(%ebp)
  101b72:	7e 0d                	jle    101b81 <trapname+0x2e>
  101b74:	83 7d 08 2f          	cmpl   $0x2f,0x8(%ebp)
  101b78:	7f 07                	jg     101b81 <trapname+0x2e>
        return "Hardware Interrupt";
  101b7a:	b8 df 65 10 00       	mov    $0x1065df,%eax
  101b7f:	eb 05                	jmp    101b86 <trapname+0x33>
    }
    return "(unknown trap)";
  101b81:	b8 f2 65 10 00       	mov    $0x1065f2,%eax
}
  101b86:	5d                   	pop    %ebp
  101b87:	c3                   	ret    

00101b88 <trap_in_kernel>:

/* trap_in_kernel - test if trap happened in kernel */
bool
trap_in_kernel(struct trapframe *tf) {
  101b88:	f3 0f 1e fb          	endbr32 
  101b8c:	55                   	push   %ebp
  101b8d:	89 e5                	mov    %esp,%ebp
    return (tf->tf_cs == (uint16_t)KERNEL_CS);
  101b8f:	8b 45 08             	mov    0x8(%ebp),%eax
  101b92:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
  101b96:	83 f8 08             	cmp    $0x8,%eax
  101b99:	0f 94 c0             	sete   %al
  101b9c:	0f b6 c0             	movzbl %al,%eax
}
  101b9f:	5d                   	pop    %ebp
  101ba0:	c3                   	ret    

00101ba1 <print_trapframe>:
    "TF", "IF", "DF", "OF", NULL, NULL, "NT", NULL,
    "RF", "VM", "AC", "VIF", "VIP", "ID", NULL, NULL,
};

void
print_trapframe(struct trapframe *tf) {
  101ba1:	f3 0f 1e fb          	endbr32 
  101ba5:	55                   	push   %ebp
  101ba6:	89 e5                	mov    %esp,%ebp
  101ba8:	83 ec 28             	sub    $0x28,%esp
    cprintf("trapframe at %p\n", tf);
  101bab:	8b 45 08             	mov    0x8(%ebp),%eax
  101bae:	89 44 24 04          	mov    %eax,0x4(%esp)
  101bb2:	c7 04 24 33 66 10 00 	movl   $0x106633,(%esp)
  101bb9:	e8 fc e6 ff ff       	call   1002ba <cprintf>
    print_regs(&tf->tf_regs);
  101bbe:	8b 45 08             	mov    0x8(%ebp),%eax
  101bc1:	89 04 24             	mov    %eax,(%esp)
  101bc4:	e8 8d 01 00 00       	call   101d56 <print_regs>
    cprintf("  ds   0x----%04x\n", tf->tf_ds);
  101bc9:	8b 45 08             	mov    0x8(%ebp),%eax
  101bcc:	0f b7 40 2c          	movzwl 0x2c(%eax),%eax
  101bd0:	89 44 24 04          	mov    %eax,0x4(%esp)
  101bd4:	c7 04 24 44 66 10 00 	movl   $0x106644,(%esp)
  101bdb:	e8 da e6 ff ff       	call   1002ba <cprintf>
    cprintf("  es   0x----%04x\n", tf->tf_es);
  101be0:	8b 45 08             	mov    0x8(%ebp),%eax
  101be3:	0f b7 40 28          	movzwl 0x28(%eax),%eax
  101be7:	89 44 24 04          	mov    %eax,0x4(%esp)
  101beb:	c7 04 24 57 66 10 00 	movl   $0x106657,(%esp)
  101bf2:	e8 c3 e6 ff ff       	call   1002ba <cprintf>
    cprintf("  fs   0x----%04x\n", tf->tf_fs);
  101bf7:	8b 45 08             	mov    0x8(%ebp),%eax
  101bfa:	0f b7 40 24          	movzwl 0x24(%eax),%eax
  101bfe:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c02:	c7 04 24 6a 66 10 00 	movl   $0x10666a,(%esp)
  101c09:	e8 ac e6 ff ff       	call   1002ba <cprintf>
    cprintf("  gs   0x----%04x\n", tf->tf_gs);
  101c0e:	8b 45 08             	mov    0x8(%ebp),%eax
  101c11:	0f b7 40 20          	movzwl 0x20(%eax),%eax
  101c15:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c19:	c7 04 24 7d 66 10 00 	movl   $0x10667d,(%esp)
  101c20:	e8 95 e6 ff ff       	call   1002ba <cprintf>
    cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
  101c25:	8b 45 08             	mov    0x8(%ebp),%eax
  101c28:	8b 40 30             	mov    0x30(%eax),%eax
  101c2b:	89 04 24             	mov    %eax,(%esp)
  101c2e:	e8 20 ff ff ff       	call   101b53 <trapname>
  101c33:	8b 55 08             	mov    0x8(%ebp),%edx
  101c36:	8b 52 30             	mov    0x30(%edx),%edx
  101c39:	89 44 24 08          	mov    %eax,0x8(%esp)
  101c3d:	89 54 24 04          	mov    %edx,0x4(%esp)
  101c41:	c7 04 24 90 66 10 00 	movl   $0x106690,(%esp)
  101c48:	e8 6d e6 ff ff       	call   1002ba <cprintf>
    cprintf("  err  0x%08x\n", tf->tf_err);
  101c4d:	8b 45 08             	mov    0x8(%ebp),%eax
  101c50:	8b 40 34             	mov    0x34(%eax),%eax
  101c53:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c57:	c7 04 24 a2 66 10 00 	movl   $0x1066a2,(%esp)
  101c5e:	e8 57 e6 ff ff       	call   1002ba <cprintf>
    cprintf("  eip  0x%08x\n", tf->tf_eip);
  101c63:	8b 45 08             	mov    0x8(%ebp),%eax
  101c66:	8b 40 38             	mov    0x38(%eax),%eax
  101c69:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c6d:	c7 04 24 b1 66 10 00 	movl   $0x1066b1,(%esp)
  101c74:	e8 41 e6 ff ff       	call   1002ba <cprintf>
    cprintf("  cs   0x----%04x\n", tf->tf_cs);
  101c79:	8b 45 08             	mov    0x8(%ebp),%eax
  101c7c:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
  101c80:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c84:	c7 04 24 c0 66 10 00 	movl   $0x1066c0,(%esp)
  101c8b:	e8 2a e6 ff ff       	call   1002ba <cprintf>
    cprintf("  flag 0x%08x ", tf->tf_eflags);
  101c90:	8b 45 08             	mov    0x8(%ebp),%eax
  101c93:	8b 40 40             	mov    0x40(%eax),%eax
  101c96:	89 44 24 04          	mov    %eax,0x4(%esp)
  101c9a:	c7 04 24 d3 66 10 00 	movl   $0x1066d3,(%esp)
  101ca1:	e8 14 e6 ff ff       	call   1002ba <cprintf>

    int i, j;
    for (i = 0, j = 1; i < sizeof(IA32flags) / sizeof(IA32flags[0]); i ++, j <<= 1) {
  101ca6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  101cad:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
  101cb4:	eb 3d                	jmp    101cf3 <print_trapframe+0x152>
        if ((tf->tf_eflags & j) && IA32flags[i] != NULL) {
  101cb6:	8b 45 08             	mov    0x8(%ebp),%eax
  101cb9:	8b 50 40             	mov    0x40(%eax),%edx
  101cbc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  101cbf:	21 d0                	and    %edx,%eax
  101cc1:	85 c0                	test   %eax,%eax
  101cc3:	74 28                	je     101ced <print_trapframe+0x14c>
  101cc5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  101cc8:	8b 04 85 80 95 11 00 	mov    0x119580(,%eax,4),%eax
  101ccf:	85 c0                	test   %eax,%eax
  101cd1:	74 1a                	je     101ced <print_trapframe+0x14c>
            cprintf("%s,", IA32flags[i]);
  101cd3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  101cd6:	8b 04 85 80 95 11 00 	mov    0x119580(,%eax,4),%eax
  101cdd:	89 44 24 04          	mov    %eax,0x4(%esp)
  101ce1:	c7 04 24 e2 66 10 00 	movl   $0x1066e2,(%esp)
  101ce8:	e8 cd e5 ff ff       	call   1002ba <cprintf>
    for (i = 0, j = 1; i < sizeof(IA32flags) / sizeof(IA32flags[0]); i ++, j <<= 1) {
  101ced:	ff 45 f4             	incl   -0xc(%ebp)
  101cf0:	d1 65 f0             	shll   -0x10(%ebp)
  101cf3:	8b 45 f4             	mov    -0xc(%ebp),%eax
  101cf6:	83 f8 17             	cmp    $0x17,%eax
  101cf9:	76 bb                	jbe    101cb6 <print_trapframe+0x115>
        }
    }
    cprintf("IOPL=%d\n", (tf->tf_eflags & FL_IOPL_MASK) >> 12);
  101cfb:	8b 45 08             	mov    0x8(%ebp),%eax
  101cfe:	8b 40 40             	mov    0x40(%eax),%eax
  101d01:	c1 e8 0c             	shr    $0xc,%eax
  101d04:	83 e0 03             	and    $0x3,%eax
  101d07:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d0b:	c7 04 24 e6 66 10 00 	movl   $0x1066e6,(%esp)
  101d12:	e8 a3 e5 ff ff       	call   1002ba <cprintf>

    if (!trap_in_kernel(tf)) {
  101d17:	8b 45 08             	mov    0x8(%ebp),%eax
  101d1a:	89 04 24             	mov    %eax,(%esp)
  101d1d:	e8 66 fe ff ff       	call   101b88 <trap_in_kernel>
  101d22:	85 c0                	test   %eax,%eax
  101d24:	75 2d                	jne    101d53 <print_trapframe+0x1b2>
        cprintf("  esp  0x%08x\n", tf->tf_esp);
  101d26:	8b 45 08             	mov    0x8(%ebp),%eax
  101d29:	8b 40 44             	mov    0x44(%eax),%eax
  101d2c:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d30:	c7 04 24 ef 66 10 00 	movl   $0x1066ef,(%esp)
  101d37:	e8 7e e5 ff ff       	call   1002ba <cprintf>
        cprintf("  ss   0x----%04x\n", tf->tf_ss);
  101d3c:	8b 45 08             	mov    0x8(%ebp),%eax
  101d3f:	0f b7 40 48          	movzwl 0x48(%eax),%eax
  101d43:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d47:	c7 04 24 fe 66 10 00 	movl   $0x1066fe,(%esp)
  101d4e:	e8 67 e5 ff ff       	call   1002ba <cprintf>
    }
}
  101d53:	90                   	nop
  101d54:	c9                   	leave  
  101d55:	c3                   	ret    

00101d56 <print_regs>:

void
print_regs(struct pushregs *regs) {
  101d56:	f3 0f 1e fb          	endbr32 
  101d5a:	55                   	push   %ebp
  101d5b:	89 e5                	mov    %esp,%ebp
  101d5d:	83 ec 18             	sub    $0x18,%esp
    cprintf("  edi  0x%08x\n", regs->reg_edi);
  101d60:	8b 45 08             	mov    0x8(%ebp),%eax
  101d63:	8b 00                	mov    (%eax),%eax
  101d65:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d69:	c7 04 24 11 67 10 00 	movl   $0x106711,(%esp)
  101d70:	e8 45 e5 ff ff       	call   1002ba <cprintf>
    cprintf("  esi  0x%08x\n", regs->reg_esi);
  101d75:	8b 45 08             	mov    0x8(%ebp),%eax
  101d78:	8b 40 04             	mov    0x4(%eax),%eax
  101d7b:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d7f:	c7 04 24 20 67 10 00 	movl   $0x106720,(%esp)
  101d86:	e8 2f e5 ff ff       	call   1002ba <cprintf>
    cprintf("  ebp  0x%08x\n", regs->reg_ebp);
  101d8b:	8b 45 08             	mov    0x8(%ebp),%eax
  101d8e:	8b 40 08             	mov    0x8(%eax),%eax
  101d91:	89 44 24 04          	mov    %eax,0x4(%esp)
  101d95:	c7 04 24 2f 67 10 00 	movl   $0x10672f,(%esp)
  101d9c:	e8 19 e5 ff ff       	call   1002ba <cprintf>
    cprintf("  oesp 0x%08x\n", regs->reg_oesp);
  101da1:	8b 45 08             	mov    0x8(%ebp),%eax
  101da4:	8b 40 0c             	mov    0xc(%eax),%eax
  101da7:	89 44 24 04          	mov    %eax,0x4(%esp)
  101dab:	c7 04 24 3e 67 10 00 	movl   $0x10673e,(%esp)
  101db2:	e8 03 e5 ff ff       	call   1002ba <cprintf>
    cprintf("  ebx  0x%08x\n", regs->reg_ebx);
  101db7:	8b 45 08             	mov    0x8(%ebp),%eax
  101dba:	8b 40 10             	mov    0x10(%eax),%eax
  101dbd:	89 44 24 04          	mov    %eax,0x4(%esp)
  101dc1:	c7 04 24 4d 67 10 00 	movl   $0x10674d,(%esp)
  101dc8:	e8 ed e4 ff ff       	call   1002ba <cprintf>
    cprintf("  edx  0x%08x\n", regs->reg_edx);
  101dcd:	8b 45 08             	mov    0x8(%ebp),%eax
  101dd0:	8b 40 14             	mov    0x14(%eax),%eax
  101dd3:	89 44 24 04          	mov    %eax,0x4(%esp)
  101dd7:	c7 04 24 5c 67 10 00 	movl   $0x10675c,(%esp)
  101dde:	e8 d7 e4 ff ff       	call   1002ba <cprintf>
    cprintf("  ecx  0x%08x\n", regs->reg_ecx);
  101de3:	8b 45 08             	mov    0x8(%ebp),%eax
  101de6:	8b 40 18             	mov    0x18(%eax),%eax
  101de9:	89 44 24 04          	mov    %eax,0x4(%esp)
  101ded:	c7 04 24 6b 67 10 00 	movl   $0x10676b,(%esp)
  101df4:	e8 c1 e4 ff ff       	call   1002ba <cprintf>
    cprintf("  eax  0x%08x\n", regs->reg_eax);
  101df9:	8b 45 08             	mov    0x8(%ebp),%eax
  101dfc:	8b 40 1c             	mov    0x1c(%eax),%eax
  101dff:	89 44 24 04          	mov    %eax,0x4(%esp)
  101e03:	c7 04 24 7a 67 10 00 	movl   $0x10677a,(%esp)
  101e0a:	e8 ab e4 ff ff       	call   1002ba <cprintf>
}
  101e0f:	90                   	nop
  101e10:	c9                   	leave  
  101e11:	c3                   	ret    

00101e12 <trap_dispatch>:

/* trap_dispatch - dispatch based on what type of trap occurred */
static void
trap_dispatch(struct trapframe *tf) {
  101e12:	f3 0f 1e fb          	endbr32 
  101e16:	55                   	push   %ebp
  101e17:	89 e5                	mov    %esp,%ebp
  101e19:	83 ec 28             	sub    $0x28,%esp
    char c;

    switch (tf->tf_trapno) {
  101e1c:	8b 45 08             	mov    0x8(%ebp),%eax
  101e1f:	8b 40 30             	mov    0x30(%eax),%eax
  101e22:	83 f8 79             	cmp    $0x79,%eax
  101e25:	0f 87 e6 00 00 00    	ja     101f11 <trap_dispatch+0xff>
  101e2b:	83 f8 78             	cmp    $0x78,%eax
  101e2e:	0f 83 c1 00 00 00    	jae    101ef5 <trap_dispatch+0xe3>
  101e34:	83 f8 2f             	cmp    $0x2f,%eax
  101e37:	0f 87 d4 00 00 00    	ja     101f11 <trap_dispatch+0xff>
  101e3d:	83 f8 2e             	cmp    $0x2e,%eax
  101e40:	0f 83 00 01 00 00    	jae    101f46 <trap_dispatch+0x134>
  101e46:	83 f8 24             	cmp    $0x24,%eax
  101e49:	74 5e                	je     101ea9 <trap_dispatch+0x97>
  101e4b:	83 f8 24             	cmp    $0x24,%eax
  101e4e:	0f 87 bd 00 00 00    	ja     101f11 <trap_dispatch+0xff>
  101e54:	83 f8 20             	cmp    $0x20,%eax
  101e57:	74 0a                	je     101e63 <trap_dispatch+0x51>
  101e59:	83 f8 21             	cmp    $0x21,%eax
  101e5c:	74 71                	je     101ecf <trap_dispatch+0xbd>
  101e5e:	e9 ae 00 00 00       	jmp    101f11 <trap_dispatch+0xff>
        /* handle the timer interrupt */
        /* (1) After a timer interrupt, you should record this event using a global variable (increase it), such as ticks in kern/driver/clock.c
         * (2) Every TICK_NUM cycle, you can print some info using a funciton, such as print_ticks().
         * (3) Too Simple? Yes, I think so!
         */
         ticks++;
  101e63:	a1 0c cf 11 00       	mov    0x11cf0c,%eax
  101e68:	40                   	inc    %eax
  101e69:	a3 0c cf 11 00       	mov    %eax,0x11cf0c
         if(ticks%TICK_NUM==0)
  101e6e:	8b 0d 0c cf 11 00    	mov    0x11cf0c,%ecx
  101e74:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
  101e79:	89 c8                	mov    %ecx,%eax
  101e7b:	f7 e2                	mul    %edx
  101e7d:	c1 ea 05             	shr    $0x5,%edx
  101e80:	89 d0                	mov    %edx,%eax
  101e82:	c1 e0 02             	shl    $0x2,%eax
  101e85:	01 d0                	add    %edx,%eax
  101e87:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  101e8e:	01 d0                	add    %edx,%eax
  101e90:	c1 e0 02             	shl    $0x2,%eax
  101e93:	29 c1                	sub    %eax,%ecx
  101e95:	89 ca                	mov    %ecx,%edx
  101e97:	85 d2                	test   %edx,%edx
  101e99:	0f 85 aa 00 00 00    	jne    101f49 <trap_dispatch+0x137>
         print_ticks();
  101e9f:	e8 f1 fa ff ff       	call   101995 <print_ticks>
         break;
  101ea4:	e9 a0 00 00 00       	jmp    101f49 <trap_dispatch+0x137>
    case IRQ_OFFSET + IRQ_COM1:
        c = cons_getc();
  101ea9:	e8 7a f8 ff ff       	call   101728 <cons_getc>
  101eae:	88 45 f7             	mov    %al,-0x9(%ebp)
        cprintf("serial [%03d] %c\n", c, c);
  101eb1:	0f be 55 f7          	movsbl -0x9(%ebp),%edx
  101eb5:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  101eb9:	89 54 24 08          	mov    %edx,0x8(%esp)
  101ebd:	89 44 24 04          	mov    %eax,0x4(%esp)
  101ec1:	c7 04 24 89 67 10 00 	movl   $0x106789,(%esp)
  101ec8:	e8 ed e3 ff ff       	call   1002ba <cprintf>
        break;
  101ecd:	eb 7b                	jmp    101f4a <trap_dispatch+0x138>
    case IRQ_OFFSET + IRQ_KBD:
        c = cons_getc();
  101ecf:	e8 54 f8 ff ff       	call   101728 <cons_getc>
  101ed4:	88 45 f7             	mov    %al,-0x9(%ebp)
        cprintf("kbd [%03d] %c\n", c, c);
  101ed7:	0f be 55 f7          	movsbl -0x9(%ebp),%edx
  101edb:	0f be 45 f7          	movsbl -0x9(%ebp),%eax
  101edf:	89 54 24 08          	mov    %edx,0x8(%esp)
  101ee3:	89 44 24 04          	mov    %eax,0x4(%esp)
  101ee7:	c7 04 24 9b 67 10 00 	movl   $0x10679b,(%esp)
  101eee:	e8 c7 e3 ff ff       	call   1002ba <cprintf>
        break;
  101ef3:	eb 55                	jmp    101f4a <trap_dispatch+0x138>
    //LAB1 CHALLENGE 1 : YOUR CODE you should modify below codes.
    case T_SWITCH_TOU:
    case T_SWITCH_TOK:
        panic("T_SWITCH_** ??\n");
  101ef5:	c7 44 24 08 aa 67 10 	movl   $0x1067aa,0x8(%esp)
  101efc:	00 
  101efd:	c7 44 24 04 ad 00 00 	movl   $0xad,0x4(%esp)
  101f04:	00 
  101f05:	c7 04 24 ce 65 10 00 	movl   $0x1065ce,(%esp)
  101f0c:	e8 15 e5 ff ff       	call   100426 <__panic>
    case IRQ_OFFSET + IRQ_IDE2:
        /* do nothing */
        break;
    default:
        // in kernel, it must be a mistake
        if ((tf->tf_cs & 3) == 0) {
  101f11:	8b 45 08             	mov    0x8(%ebp),%eax
  101f14:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
  101f18:	83 e0 03             	and    $0x3,%eax
  101f1b:	85 c0                	test   %eax,%eax
  101f1d:	75 2b                	jne    101f4a <trap_dispatch+0x138>
            print_trapframe(tf);
  101f1f:	8b 45 08             	mov    0x8(%ebp),%eax
  101f22:	89 04 24             	mov    %eax,(%esp)
  101f25:	e8 77 fc ff ff       	call   101ba1 <print_trapframe>
            panic("unexpected trap in kernel.\n");
  101f2a:	c7 44 24 08 ba 67 10 	movl   $0x1067ba,0x8(%esp)
  101f31:	00 
  101f32:	c7 44 24 04 b7 00 00 	movl   $0xb7,0x4(%esp)
  101f39:	00 
  101f3a:	c7 04 24 ce 65 10 00 	movl   $0x1065ce,(%esp)
  101f41:	e8 e0 e4 ff ff       	call   100426 <__panic>
        break;
  101f46:	90                   	nop
  101f47:	eb 01                	jmp    101f4a <trap_dispatch+0x138>
         break;
  101f49:	90                   	nop
        }
    }
}
  101f4a:	90                   	nop
  101f4b:	c9                   	leave  
  101f4c:	c3                   	ret    

00101f4d <trap>:
 * trap - handles or dispatches an exception/interrupt. if and when trap() returns,
 * the code in kern/trap/trapentry.S restores the old CPU state saved in the
 * trapframe and then uses the iret instruction to return from the exception.
 * */
void
trap(struct trapframe *tf) {
  101f4d:	f3 0f 1e fb          	endbr32 
  101f51:	55                   	push   %ebp
  101f52:	89 e5                	mov    %esp,%ebp
  101f54:	83 ec 18             	sub    $0x18,%esp
    // dispatch based on what type of trap occurred
    trap_dispatch(tf);
  101f57:	8b 45 08             	mov    0x8(%ebp),%eax
  101f5a:	89 04 24             	mov    %eax,(%esp)
  101f5d:	e8 b0 fe ff ff       	call   101e12 <trap_dispatch>
}
  101f62:	90                   	nop
  101f63:	c9                   	leave  
  101f64:	c3                   	ret    

00101f65 <vector0>:
# handler
.text
.globl __alltraps
.globl vector0
vector0:
  pushl $0
  101f65:	6a 00                	push   $0x0
  pushl $0
  101f67:	6a 00                	push   $0x0
  jmp __alltraps
  101f69:	e9 69 0a 00 00       	jmp    1029d7 <__alltraps>

00101f6e <vector1>:
.globl vector1
vector1:
  pushl $0
  101f6e:	6a 00                	push   $0x0
  pushl $1
  101f70:	6a 01                	push   $0x1
  jmp __alltraps
  101f72:	e9 60 0a 00 00       	jmp    1029d7 <__alltraps>

00101f77 <vector2>:
.globl vector2
vector2:
  pushl $0
  101f77:	6a 00                	push   $0x0
  pushl $2
  101f79:	6a 02                	push   $0x2
  jmp __alltraps
  101f7b:	e9 57 0a 00 00       	jmp    1029d7 <__alltraps>

00101f80 <vector3>:
.globl vector3
vector3:
  pushl $0
  101f80:	6a 00                	push   $0x0
  pushl $3
  101f82:	6a 03                	push   $0x3
  jmp __alltraps
  101f84:	e9 4e 0a 00 00       	jmp    1029d7 <__alltraps>

00101f89 <vector4>:
.globl vector4
vector4:
  pushl $0
  101f89:	6a 00                	push   $0x0
  pushl $4
  101f8b:	6a 04                	push   $0x4
  jmp __alltraps
  101f8d:	e9 45 0a 00 00       	jmp    1029d7 <__alltraps>

00101f92 <vector5>:
.globl vector5
vector5:
  pushl $0
  101f92:	6a 00                	push   $0x0
  pushl $5
  101f94:	6a 05                	push   $0x5
  jmp __alltraps
  101f96:	e9 3c 0a 00 00       	jmp    1029d7 <__alltraps>

00101f9b <vector6>:
.globl vector6
vector6:
  pushl $0
  101f9b:	6a 00                	push   $0x0
  pushl $6
  101f9d:	6a 06                	push   $0x6
  jmp __alltraps
  101f9f:	e9 33 0a 00 00       	jmp    1029d7 <__alltraps>

00101fa4 <vector7>:
.globl vector7
vector7:
  pushl $0
  101fa4:	6a 00                	push   $0x0
  pushl $7
  101fa6:	6a 07                	push   $0x7
  jmp __alltraps
  101fa8:	e9 2a 0a 00 00       	jmp    1029d7 <__alltraps>

00101fad <vector8>:
.globl vector8
vector8:
  pushl $8
  101fad:	6a 08                	push   $0x8
  jmp __alltraps
  101faf:	e9 23 0a 00 00       	jmp    1029d7 <__alltraps>

00101fb4 <vector9>:
.globl vector9
vector9:
  pushl $0
  101fb4:	6a 00                	push   $0x0
  pushl $9
  101fb6:	6a 09                	push   $0x9
  jmp __alltraps
  101fb8:	e9 1a 0a 00 00       	jmp    1029d7 <__alltraps>

00101fbd <vector10>:
.globl vector10
vector10:
  pushl $10
  101fbd:	6a 0a                	push   $0xa
  jmp __alltraps
  101fbf:	e9 13 0a 00 00       	jmp    1029d7 <__alltraps>

00101fc4 <vector11>:
.globl vector11
vector11:
  pushl $11
  101fc4:	6a 0b                	push   $0xb
  jmp __alltraps
  101fc6:	e9 0c 0a 00 00       	jmp    1029d7 <__alltraps>

00101fcb <vector12>:
.globl vector12
vector12:
  pushl $12
  101fcb:	6a 0c                	push   $0xc
  jmp __alltraps
  101fcd:	e9 05 0a 00 00       	jmp    1029d7 <__alltraps>

00101fd2 <vector13>:
.globl vector13
vector13:
  pushl $13
  101fd2:	6a 0d                	push   $0xd
  jmp __alltraps
  101fd4:	e9 fe 09 00 00       	jmp    1029d7 <__alltraps>

00101fd9 <vector14>:
.globl vector14
vector14:
  pushl $14
  101fd9:	6a 0e                	push   $0xe
  jmp __alltraps
  101fdb:	e9 f7 09 00 00       	jmp    1029d7 <__alltraps>

00101fe0 <vector15>:
.globl vector15
vector15:
  pushl $0
  101fe0:	6a 00                	push   $0x0
  pushl $15
  101fe2:	6a 0f                	push   $0xf
  jmp __alltraps
  101fe4:	e9 ee 09 00 00       	jmp    1029d7 <__alltraps>

00101fe9 <vector16>:
.globl vector16
vector16:
  pushl $0
  101fe9:	6a 00                	push   $0x0
  pushl $16
  101feb:	6a 10                	push   $0x10
  jmp __alltraps
  101fed:	e9 e5 09 00 00       	jmp    1029d7 <__alltraps>

00101ff2 <vector17>:
.globl vector17
vector17:
  pushl $17
  101ff2:	6a 11                	push   $0x11
  jmp __alltraps
  101ff4:	e9 de 09 00 00       	jmp    1029d7 <__alltraps>

00101ff9 <vector18>:
.globl vector18
vector18:
  pushl $0
  101ff9:	6a 00                	push   $0x0
  pushl $18
  101ffb:	6a 12                	push   $0x12
  jmp __alltraps
  101ffd:	e9 d5 09 00 00       	jmp    1029d7 <__alltraps>

00102002 <vector19>:
.globl vector19
vector19:
  pushl $0
  102002:	6a 00                	push   $0x0
  pushl $19
  102004:	6a 13                	push   $0x13
  jmp __alltraps
  102006:	e9 cc 09 00 00       	jmp    1029d7 <__alltraps>

0010200b <vector20>:
.globl vector20
vector20:
  pushl $0
  10200b:	6a 00                	push   $0x0
  pushl $20
  10200d:	6a 14                	push   $0x14
  jmp __alltraps
  10200f:	e9 c3 09 00 00       	jmp    1029d7 <__alltraps>

00102014 <vector21>:
.globl vector21
vector21:
  pushl $0
  102014:	6a 00                	push   $0x0
  pushl $21
  102016:	6a 15                	push   $0x15
  jmp __alltraps
  102018:	e9 ba 09 00 00       	jmp    1029d7 <__alltraps>

0010201d <vector22>:
.globl vector22
vector22:
  pushl $0
  10201d:	6a 00                	push   $0x0
  pushl $22
  10201f:	6a 16                	push   $0x16
  jmp __alltraps
  102021:	e9 b1 09 00 00       	jmp    1029d7 <__alltraps>

00102026 <vector23>:
.globl vector23
vector23:
  pushl $0
  102026:	6a 00                	push   $0x0
  pushl $23
  102028:	6a 17                	push   $0x17
  jmp __alltraps
  10202a:	e9 a8 09 00 00       	jmp    1029d7 <__alltraps>

0010202f <vector24>:
.globl vector24
vector24:
  pushl $0
  10202f:	6a 00                	push   $0x0
  pushl $24
  102031:	6a 18                	push   $0x18
  jmp __alltraps
  102033:	e9 9f 09 00 00       	jmp    1029d7 <__alltraps>

00102038 <vector25>:
.globl vector25
vector25:
  pushl $0
  102038:	6a 00                	push   $0x0
  pushl $25
  10203a:	6a 19                	push   $0x19
  jmp __alltraps
  10203c:	e9 96 09 00 00       	jmp    1029d7 <__alltraps>

00102041 <vector26>:
.globl vector26
vector26:
  pushl $0
  102041:	6a 00                	push   $0x0
  pushl $26
  102043:	6a 1a                	push   $0x1a
  jmp __alltraps
  102045:	e9 8d 09 00 00       	jmp    1029d7 <__alltraps>

0010204a <vector27>:
.globl vector27
vector27:
  pushl $0
  10204a:	6a 00                	push   $0x0
  pushl $27
  10204c:	6a 1b                	push   $0x1b
  jmp __alltraps
  10204e:	e9 84 09 00 00       	jmp    1029d7 <__alltraps>

00102053 <vector28>:
.globl vector28
vector28:
  pushl $0
  102053:	6a 00                	push   $0x0
  pushl $28
  102055:	6a 1c                	push   $0x1c
  jmp __alltraps
  102057:	e9 7b 09 00 00       	jmp    1029d7 <__alltraps>

0010205c <vector29>:
.globl vector29
vector29:
  pushl $0
  10205c:	6a 00                	push   $0x0
  pushl $29
  10205e:	6a 1d                	push   $0x1d
  jmp __alltraps
  102060:	e9 72 09 00 00       	jmp    1029d7 <__alltraps>

00102065 <vector30>:
.globl vector30
vector30:
  pushl $0
  102065:	6a 00                	push   $0x0
  pushl $30
  102067:	6a 1e                	push   $0x1e
  jmp __alltraps
  102069:	e9 69 09 00 00       	jmp    1029d7 <__alltraps>

0010206e <vector31>:
.globl vector31
vector31:
  pushl $0
  10206e:	6a 00                	push   $0x0
  pushl $31
  102070:	6a 1f                	push   $0x1f
  jmp __alltraps
  102072:	e9 60 09 00 00       	jmp    1029d7 <__alltraps>

00102077 <vector32>:
.globl vector32
vector32:
  pushl $0
  102077:	6a 00                	push   $0x0
  pushl $32
  102079:	6a 20                	push   $0x20
  jmp __alltraps
  10207b:	e9 57 09 00 00       	jmp    1029d7 <__alltraps>

00102080 <vector33>:
.globl vector33
vector33:
  pushl $0
  102080:	6a 00                	push   $0x0
  pushl $33
  102082:	6a 21                	push   $0x21
  jmp __alltraps
  102084:	e9 4e 09 00 00       	jmp    1029d7 <__alltraps>

00102089 <vector34>:
.globl vector34
vector34:
  pushl $0
  102089:	6a 00                	push   $0x0
  pushl $34
  10208b:	6a 22                	push   $0x22
  jmp __alltraps
  10208d:	e9 45 09 00 00       	jmp    1029d7 <__alltraps>

00102092 <vector35>:
.globl vector35
vector35:
  pushl $0
  102092:	6a 00                	push   $0x0
  pushl $35
  102094:	6a 23                	push   $0x23
  jmp __alltraps
  102096:	e9 3c 09 00 00       	jmp    1029d7 <__alltraps>

0010209b <vector36>:
.globl vector36
vector36:
  pushl $0
  10209b:	6a 00                	push   $0x0
  pushl $36
  10209d:	6a 24                	push   $0x24
  jmp __alltraps
  10209f:	e9 33 09 00 00       	jmp    1029d7 <__alltraps>

001020a4 <vector37>:
.globl vector37
vector37:
  pushl $0
  1020a4:	6a 00                	push   $0x0
  pushl $37
  1020a6:	6a 25                	push   $0x25
  jmp __alltraps
  1020a8:	e9 2a 09 00 00       	jmp    1029d7 <__alltraps>

001020ad <vector38>:
.globl vector38
vector38:
  pushl $0
  1020ad:	6a 00                	push   $0x0
  pushl $38
  1020af:	6a 26                	push   $0x26
  jmp __alltraps
  1020b1:	e9 21 09 00 00       	jmp    1029d7 <__alltraps>

001020b6 <vector39>:
.globl vector39
vector39:
  pushl $0
  1020b6:	6a 00                	push   $0x0
  pushl $39
  1020b8:	6a 27                	push   $0x27
  jmp __alltraps
  1020ba:	e9 18 09 00 00       	jmp    1029d7 <__alltraps>

001020bf <vector40>:
.globl vector40
vector40:
  pushl $0
  1020bf:	6a 00                	push   $0x0
  pushl $40
  1020c1:	6a 28                	push   $0x28
  jmp __alltraps
  1020c3:	e9 0f 09 00 00       	jmp    1029d7 <__alltraps>

001020c8 <vector41>:
.globl vector41
vector41:
  pushl $0
  1020c8:	6a 00                	push   $0x0
  pushl $41
  1020ca:	6a 29                	push   $0x29
  jmp __alltraps
  1020cc:	e9 06 09 00 00       	jmp    1029d7 <__alltraps>

001020d1 <vector42>:
.globl vector42
vector42:
  pushl $0
  1020d1:	6a 00                	push   $0x0
  pushl $42
  1020d3:	6a 2a                	push   $0x2a
  jmp __alltraps
  1020d5:	e9 fd 08 00 00       	jmp    1029d7 <__alltraps>

001020da <vector43>:
.globl vector43
vector43:
  pushl $0
  1020da:	6a 00                	push   $0x0
  pushl $43
  1020dc:	6a 2b                	push   $0x2b
  jmp __alltraps
  1020de:	e9 f4 08 00 00       	jmp    1029d7 <__alltraps>

001020e3 <vector44>:
.globl vector44
vector44:
  pushl $0
  1020e3:	6a 00                	push   $0x0
  pushl $44
  1020e5:	6a 2c                	push   $0x2c
  jmp __alltraps
  1020e7:	e9 eb 08 00 00       	jmp    1029d7 <__alltraps>

001020ec <vector45>:
.globl vector45
vector45:
  pushl $0
  1020ec:	6a 00                	push   $0x0
  pushl $45
  1020ee:	6a 2d                	push   $0x2d
  jmp __alltraps
  1020f0:	e9 e2 08 00 00       	jmp    1029d7 <__alltraps>

001020f5 <vector46>:
.globl vector46
vector46:
  pushl $0
  1020f5:	6a 00                	push   $0x0
  pushl $46
  1020f7:	6a 2e                	push   $0x2e
  jmp __alltraps
  1020f9:	e9 d9 08 00 00       	jmp    1029d7 <__alltraps>

001020fe <vector47>:
.globl vector47
vector47:
  pushl $0
  1020fe:	6a 00                	push   $0x0
  pushl $47
  102100:	6a 2f                	push   $0x2f
  jmp __alltraps
  102102:	e9 d0 08 00 00       	jmp    1029d7 <__alltraps>

00102107 <vector48>:
.globl vector48
vector48:
  pushl $0
  102107:	6a 00                	push   $0x0
  pushl $48
  102109:	6a 30                	push   $0x30
  jmp __alltraps
  10210b:	e9 c7 08 00 00       	jmp    1029d7 <__alltraps>

00102110 <vector49>:
.globl vector49
vector49:
  pushl $0
  102110:	6a 00                	push   $0x0
  pushl $49
  102112:	6a 31                	push   $0x31
  jmp __alltraps
  102114:	e9 be 08 00 00       	jmp    1029d7 <__alltraps>

00102119 <vector50>:
.globl vector50
vector50:
  pushl $0
  102119:	6a 00                	push   $0x0
  pushl $50
  10211b:	6a 32                	push   $0x32
  jmp __alltraps
  10211d:	e9 b5 08 00 00       	jmp    1029d7 <__alltraps>

00102122 <vector51>:
.globl vector51
vector51:
  pushl $0
  102122:	6a 00                	push   $0x0
  pushl $51
  102124:	6a 33                	push   $0x33
  jmp __alltraps
  102126:	e9 ac 08 00 00       	jmp    1029d7 <__alltraps>

0010212b <vector52>:
.globl vector52
vector52:
  pushl $0
  10212b:	6a 00                	push   $0x0
  pushl $52
  10212d:	6a 34                	push   $0x34
  jmp __alltraps
  10212f:	e9 a3 08 00 00       	jmp    1029d7 <__alltraps>

00102134 <vector53>:
.globl vector53
vector53:
  pushl $0
  102134:	6a 00                	push   $0x0
  pushl $53
  102136:	6a 35                	push   $0x35
  jmp __alltraps
  102138:	e9 9a 08 00 00       	jmp    1029d7 <__alltraps>

0010213d <vector54>:
.globl vector54
vector54:
  pushl $0
  10213d:	6a 00                	push   $0x0
  pushl $54
  10213f:	6a 36                	push   $0x36
  jmp __alltraps
  102141:	e9 91 08 00 00       	jmp    1029d7 <__alltraps>

00102146 <vector55>:
.globl vector55
vector55:
  pushl $0
  102146:	6a 00                	push   $0x0
  pushl $55
  102148:	6a 37                	push   $0x37
  jmp __alltraps
  10214a:	e9 88 08 00 00       	jmp    1029d7 <__alltraps>

0010214f <vector56>:
.globl vector56
vector56:
  pushl $0
  10214f:	6a 00                	push   $0x0
  pushl $56
  102151:	6a 38                	push   $0x38
  jmp __alltraps
  102153:	e9 7f 08 00 00       	jmp    1029d7 <__alltraps>

00102158 <vector57>:
.globl vector57
vector57:
  pushl $0
  102158:	6a 00                	push   $0x0
  pushl $57
  10215a:	6a 39                	push   $0x39
  jmp __alltraps
  10215c:	e9 76 08 00 00       	jmp    1029d7 <__alltraps>

00102161 <vector58>:
.globl vector58
vector58:
  pushl $0
  102161:	6a 00                	push   $0x0
  pushl $58
  102163:	6a 3a                	push   $0x3a
  jmp __alltraps
  102165:	e9 6d 08 00 00       	jmp    1029d7 <__alltraps>

0010216a <vector59>:
.globl vector59
vector59:
  pushl $0
  10216a:	6a 00                	push   $0x0
  pushl $59
  10216c:	6a 3b                	push   $0x3b
  jmp __alltraps
  10216e:	e9 64 08 00 00       	jmp    1029d7 <__alltraps>

00102173 <vector60>:
.globl vector60
vector60:
  pushl $0
  102173:	6a 00                	push   $0x0
  pushl $60
  102175:	6a 3c                	push   $0x3c
  jmp __alltraps
  102177:	e9 5b 08 00 00       	jmp    1029d7 <__alltraps>

0010217c <vector61>:
.globl vector61
vector61:
  pushl $0
  10217c:	6a 00                	push   $0x0
  pushl $61
  10217e:	6a 3d                	push   $0x3d
  jmp __alltraps
  102180:	e9 52 08 00 00       	jmp    1029d7 <__alltraps>

00102185 <vector62>:
.globl vector62
vector62:
  pushl $0
  102185:	6a 00                	push   $0x0
  pushl $62
  102187:	6a 3e                	push   $0x3e
  jmp __alltraps
  102189:	e9 49 08 00 00       	jmp    1029d7 <__alltraps>

0010218e <vector63>:
.globl vector63
vector63:
  pushl $0
  10218e:	6a 00                	push   $0x0
  pushl $63
  102190:	6a 3f                	push   $0x3f
  jmp __alltraps
  102192:	e9 40 08 00 00       	jmp    1029d7 <__alltraps>

00102197 <vector64>:
.globl vector64
vector64:
  pushl $0
  102197:	6a 00                	push   $0x0
  pushl $64
  102199:	6a 40                	push   $0x40
  jmp __alltraps
  10219b:	e9 37 08 00 00       	jmp    1029d7 <__alltraps>

001021a0 <vector65>:
.globl vector65
vector65:
  pushl $0
  1021a0:	6a 00                	push   $0x0
  pushl $65
  1021a2:	6a 41                	push   $0x41
  jmp __alltraps
  1021a4:	e9 2e 08 00 00       	jmp    1029d7 <__alltraps>

001021a9 <vector66>:
.globl vector66
vector66:
  pushl $0
  1021a9:	6a 00                	push   $0x0
  pushl $66
  1021ab:	6a 42                	push   $0x42
  jmp __alltraps
  1021ad:	e9 25 08 00 00       	jmp    1029d7 <__alltraps>

001021b2 <vector67>:
.globl vector67
vector67:
  pushl $0
  1021b2:	6a 00                	push   $0x0
  pushl $67
  1021b4:	6a 43                	push   $0x43
  jmp __alltraps
  1021b6:	e9 1c 08 00 00       	jmp    1029d7 <__alltraps>

001021bb <vector68>:
.globl vector68
vector68:
  pushl $0
  1021bb:	6a 00                	push   $0x0
  pushl $68
  1021bd:	6a 44                	push   $0x44
  jmp __alltraps
  1021bf:	e9 13 08 00 00       	jmp    1029d7 <__alltraps>

001021c4 <vector69>:
.globl vector69
vector69:
  pushl $0
  1021c4:	6a 00                	push   $0x0
  pushl $69
  1021c6:	6a 45                	push   $0x45
  jmp __alltraps
  1021c8:	e9 0a 08 00 00       	jmp    1029d7 <__alltraps>

001021cd <vector70>:
.globl vector70
vector70:
  pushl $0
  1021cd:	6a 00                	push   $0x0
  pushl $70
  1021cf:	6a 46                	push   $0x46
  jmp __alltraps
  1021d1:	e9 01 08 00 00       	jmp    1029d7 <__alltraps>

001021d6 <vector71>:
.globl vector71
vector71:
  pushl $0
  1021d6:	6a 00                	push   $0x0
  pushl $71
  1021d8:	6a 47                	push   $0x47
  jmp __alltraps
  1021da:	e9 f8 07 00 00       	jmp    1029d7 <__alltraps>

001021df <vector72>:
.globl vector72
vector72:
  pushl $0
  1021df:	6a 00                	push   $0x0
  pushl $72
  1021e1:	6a 48                	push   $0x48
  jmp __alltraps
  1021e3:	e9 ef 07 00 00       	jmp    1029d7 <__alltraps>

001021e8 <vector73>:
.globl vector73
vector73:
  pushl $0
  1021e8:	6a 00                	push   $0x0
  pushl $73
  1021ea:	6a 49                	push   $0x49
  jmp __alltraps
  1021ec:	e9 e6 07 00 00       	jmp    1029d7 <__alltraps>

001021f1 <vector74>:
.globl vector74
vector74:
  pushl $0
  1021f1:	6a 00                	push   $0x0
  pushl $74
  1021f3:	6a 4a                	push   $0x4a
  jmp __alltraps
  1021f5:	e9 dd 07 00 00       	jmp    1029d7 <__alltraps>

001021fa <vector75>:
.globl vector75
vector75:
  pushl $0
  1021fa:	6a 00                	push   $0x0
  pushl $75
  1021fc:	6a 4b                	push   $0x4b
  jmp __alltraps
  1021fe:	e9 d4 07 00 00       	jmp    1029d7 <__alltraps>

00102203 <vector76>:
.globl vector76
vector76:
  pushl $0
  102203:	6a 00                	push   $0x0
  pushl $76
  102205:	6a 4c                	push   $0x4c
  jmp __alltraps
  102207:	e9 cb 07 00 00       	jmp    1029d7 <__alltraps>

0010220c <vector77>:
.globl vector77
vector77:
  pushl $0
  10220c:	6a 00                	push   $0x0
  pushl $77
  10220e:	6a 4d                	push   $0x4d
  jmp __alltraps
  102210:	e9 c2 07 00 00       	jmp    1029d7 <__alltraps>

00102215 <vector78>:
.globl vector78
vector78:
  pushl $0
  102215:	6a 00                	push   $0x0
  pushl $78
  102217:	6a 4e                	push   $0x4e
  jmp __alltraps
  102219:	e9 b9 07 00 00       	jmp    1029d7 <__alltraps>

0010221e <vector79>:
.globl vector79
vector79:
  pushl $0
  10221e:	6a 00                	push   $0x0
  pushl $79
  102220:	6a 4f                	push   $0x4f
  jmp __alltraps
  102222:	e9 b0 07 00 00       	jmp    1029d7 <__alltraps>

00102227 <vector80>:
.globl vector80
vector80:
  pushl $0
  102227:	6a 00                	push   $0x0
  pushl $80
  102229:	6a 50                	push   $0x50
  jmp __alltraps
  10222b:	e9 a7 07 00 00       	jmp    1029d7 <__alltraps>

00102230 <vector81>:
.globl vector81
vector81:
  pushl $0
  102230:	6a 00                	push   $0x0
  pushl $81
  102232:	6a 51                	push   $0x51
  jmp __alltraps
  102234:	e9 9e 07 00 00       	jmp    1029d7 <__alltraps>

00102239 <vector82>:
.globl vector82
vector82:
  pushl $0
  102239:	6a 00                	push   $0x0
  pushl $82
  10223b:	6a 52                	push   $0x52
  jmp __alltraps
  10223d:	e9 95 07 00 00       	jmp    1029d7 <__alltraps>

00102242 <vector83>:
.globl vector83
vector83:
  pushl $0
  102242:	6a 00                	push   $0x0
  pushl $83
  102244:	6a 53                	push   $0x53
  jmp __alltraps
  102246:	e9 8c 07 00 00       	jmp    1029d7 <__alltraps>

0010224b <vector84>:
.globl vector84
vector84:
  pushl $0
  10224b:	6a 00                	push   $0x0
  pushl $84
  10224d:	6a 54                	push   $0x54
  jmp __alltraps
  10224f:	e9 83 07 00 00       	jmp    1029d7 <__alltraps>

00102254 <vector85>:
.globl vector85
vector85:
  pushl $0
  102254:	6a 00                	push   $0x0
  pushl $85
  102256:	6a 55                	push   $0x55
  jmp __alltraps
  102258:	e9 7a 07 00 00       	jmp    1029d7 <__alltraps>

0010225d <vector86>:
.globl vector86
vector86:
  pushl $0
  10225d:	6a 00                	push   $0x0
  pushl $86
  10225f:	6a 56                	push   $0x56
  jmp __alltraps
  102261:	e9 71 07 00 00       	jmp    1029d7 <__alltraps>

00102266 <vector87>:
.globl vector87
vector87:
  pushl $0
  102266:	6a 00                	push   $0x0
  pushl $87
  102268:	6a 57                	push   $0x57
  jmp __alltraps
  10226a:	e9 68 07 00 00       	jmp    1029d7 <__alltraps>

0010226f <vector88>:
.globl vector88
vector88:
  pushl $0
  10226f:	6a 00                	push   $0x0
  pushl $88
  102271:	6a 58                	push   $0x58
  jmp __alltraps
  102273:	e9 5f 07 00 00       	jmp    1029d7 <__alltraps>

00102278 <vector89>:
.globl vector89
vector89:
  pushl $0
  102278:	6a 00                	push   $0x0
  pushl $89
  10227a:	6a 59                	push   $0x59
  jmp __alltraps
  10227c:	e9 56 07 00 00       	jmp    1029d7 <__alltraps>

00102281 <vector90>:
.globl vector90
vector90:
  pushl $0
  102281:	6a 00                	push   $0x0
  pushl $90
  102283:	6a 5a                	push   $0x5a
  jmp __alltraps
  102285:	e9 4d 07 00 00       	jmp    1029d7 <__alltraps>

0010228a <vector91>:
.globl vector91
vector91:
  pushl $0
  10228a:	6a 00                	push   $0x0
  pushl $91
  10228c:	6a 5b                	push   $0x5b
  jmp __alltraps
  10228e:	e9 44 07 00 00       	jmp    1029d7 <__alltraps>

00102293 <vector92>:
.globl vector92
vector92:
  pushl $0
  102293:	6a 00                	push   $0x0
  pushl $92
  102295:	6a 5c                	push   $0x5c
  jmp __alltraps
  102297:	e9 3b 07 00 00       	jmp    1029d7 <__alltraps>

0010229c <vector93>:
.globl vector93
vector93:
  pushl $0
  10229c:	6a 00                	push   $0x0
  pushl $93
  10229e:	6a 5d                	push   $0x5d
  jmp __alltraps
  1022a0:	e9 32 07 00 00       	jmp    1029d7 <__alltraps>

001022a5 <vector94>:
.globl vector94
vector94:
  pushl $0
  1022a5:	6a 00                	push   $0x0
  pushl $94
  1022a7:	6a 5e                	push   $0x5e
  jmp __alltraps
  1022a9:	e9 29 07 00 00       	jmp    1029d7 <__alltraps>

001022ae <vector95>:
.globl vector95
vector95:
  pushl $0
  1022ae:	6a 00                	push   $0x0
  pushl $95
  1022b0:	6a 5f                	push   $0x5f
  jmp __alltraps
  1022b2:	e9 20 07 00 00       	jmp    1029d7 <__alltraps>

001022b7 <vector96>:
.globl vector96
vector96:
  pushl $0
  1022b7:	6a 00                	push   $0x0
  pushl $96
  1022b9:	6a 60                	push   $0x60
  jmp __alltraps
  1022bb:	e9 17 07 00 00       	jmp    1029d7 <__alltraps>

001022c0 <vector97>:
.globl vector97
vector97:
  pushl $0
  1022c0:	6a 00                	push   $0x0
  pushl $97
  1022c2:	6a 61                	push   $0x61
  jmp __alltraps
  1022c4:	e9 0e 07 00 00       	jmp    1029d7 <__alltraps>

001022c9 <vector98>:
.globl vector98
vector98:
  pushl $0
  1022c9:	6a 00                	push   $0x0
  pushl $98
  1022cb:	6a 62                	push   $0x62
  jmp __alltraps
  1022cd:	e9 05 07 00 00       	jmp    1029d7 <__alltraps>

001022d2 <vector99>:
.globl vector99
vector99:
  pushl $0
  1022d2:	6a 00                	push   $0x0
  pushl $99
  1022d4:	6a 63                	push   $0x63
  jmp __alltraps
  1022d6:	e9 fc 06 00 00       	jmp    1029d7 <__alltraps>

001022db <vector100>:
.globl vector100
vector100:
  pushl $0
  1022db:	6a 00                	push   $0x0
  pushl $100
  1022dd:	6a 64                	push   $0x64
  jmp __alltraps
  1022df:	e9 f3 06 00 00       	jmp    1029d7 <__alltraps>

001022e4 <vector101>:
.globl vector101
vector101:
  pushl $0
  1022e4:	6a 00                	push   $0x0
  pushl $101
  1022e6:	6a 65                	push   $0x65
  jmp __alltraps
  1022e8:	e9 ea 06 00 00       	jmp    1029d7 <__alltraps>

001022ed <vector102>:
.globl vector102
vector102:
  pushl $0
  1022ed:	6a 00                	push   $0x0
  pushl $102
  1022ef:	6a 66                	push   $0x66
  jmp __alltraps
  1022f1:	e9 e1 06 00 00       	jmp    1029d7 <__alltraps>

001022f6 <vector103>:
.globl vector103
vector103:
  pushl $0
  1022f6:	6a 00                	push   $0x0
  pushl $103
  1022f8:	6a 67                	push   $0x67
  jmp __alltraps
  1022fa:	e9 d8 06 00 00       	jmp    1029d7 <__alltraps>

001022ff <vector104>:
.globl vector104
vector104:
  pushl $0
  1022ff:	6a 00                	push   $0x0
  pushl $104
  102301:	6a 68                	push   $0x68
  jmp __alltraps
  102303:	e9 cf 06 00 00       	jmp    1029d7 <__alltraps>

00102308 <vector105>:
.globl vector105
vector105:
  pushl $0
  102308:	6a 00                	push   $0x0
  pushl $105
  10230a:	6a 69                	push   $0x69
  jmp __alltraps
  10230c:	e9 c6 06 00 00       	jmp    1029d7 <__alltraps>

00102311 <vector106>:
.globl vector106
vector106:
  pushl $0
  102311:	6a 00                	push   $0x0
  pushl $106
  102313:	6a 6a                	push   $0x6a
  jmp __alltraps
  102315:	e9 bd 06 00 00       	jmp    1029d7 <__alltraps>

0010231a <vector107>:
.globl vector107
vector107:
  pushl $0
  10231a:	6a 00                	push   $0x0
  pushl $107
  10231c:	6a 6b                	push   $0x6b
  jmp __alltraps
  10231e:	e9 b4 06 00 00       	jmp    1029d7 <__alltraps>

00102323 <vector108>:
.globl vector108
vector108:
  pushl $0
  102323:	6a 00                	push   $0x0
  pushl $108
  102325:	6a 6c                	push   $0x6c
  jmp __alltraps
  102327:	e9 ab 06 00 00       	jmp    1029d7 <__alltraps>

0010232c <vector109>:
.globl vector109
vector109:
  pushl $0
  10232c:	6a 00                	push   $0x0
  pushl $109
  10232e:	6a 6d                	push   $0x6d
  jmp __alltraps
  102330:	e9 a2 06 00 00       	jmp    1029d7 <__alltraps>

00102335 <vector110>:
.globl vector110
vector110:
  pushl $0
  102335:	6a 00                	push   $0x0
  pushl $110
  102337:	6a 6e                	push   $0x6e
  jmp __alltraps
  102339:	e9 99 06 00 00       	jmp    1029d7 <__alltraps>

0010233e <vector111>:
.globl vector111
vector111:
  pushl $0
  10233e:	6a 00                	push   $0x0
  pushl $111
  102340:	6a 6f                	push   $0x6f
  jmp __alltraps
  102342:	e9 90 06 00 00       	jmp    1029d7 <__alltraps>

00102347 <vector112>:
.globl vector112
vector112:
  pushl $0
  102347:	6a 00                	push   $0x0
  pushl $112
  102349:	6a 70                	push   $0x70
  jmp __alltraps
  10234b:	e9 87 06 00 00       	jmp    1029d7 <__alltraps>

00102350 <vector113>:
.globl vector113
vector113:
  pushl $0
  102350:	6a 00                	push   $0x0
  pushl $113
  102352:	6a 71                	push   $0x71
  jmp __alltraps
  102354:	e9 7e 06 00 00       	jmp    1029d7 <__alltraps>

00102359 <vector114>:
.globl vector114
vector114:
  pushl $0
  102359:	6a 00                	push   $0x0
  pushl $114
  10235b:	6a 72                	push   $0x72
  jmp __alltraps
  10235d:	e9 75 06 00 00       	jmp    1029d7 <__alltraps>

00102362 <vector115>:
.globl vector115
vector115:
  pushl $0
  102362:	6a 00                	push   $0x0
  pushl $115
  102364:	6a 73                	push   $0x73
  jmp __alltraps
  102366:	e9 6c 06 00 00       	jmp    1029d7 <__alltraps>

0010236b <vector116>:
.globl vector116
vector116:
  pushl $0
  10236b:	6a 00                	push   $0x0
  pushl $116
  10236d:	6a 74                	push   $0x74
  jmp __alltraps
  10236f:	e9 63 06 00 00       	jmp    1029d7 <__alltraps>

00102374 <vector117>:
.globl vector117
vector117:
  pushl $0
  102374:	6a 00                	push   $0x0
  pushl $117
  102376:	6a 75                	push   $0x75
  jmp __alltraps
  102378:	e9 5a 06 00 00       	jmp    1029d7 <__alltraps>

0010237d <vector118>:
.globl vector118
vector118:
  pushl $0
  10237d:	6a 00                	push   $0x0
  pushl $118
  10237f:	6a 76                	push   $0x76
  jmp __alltraps
  102381:	e9 51 06 00 00       	jmp    1029d7 <__alltraps>

00102386 <vector119>:
.globl vector119
vector119:
  pushl $0
  102386:	6a 00                	push   $0x0
  pushl $119
  102388:	6a 77                	push   $0x77
  jmp __alltraps
  10238a:	e9 48 06 00 00       	jmp    1029d7 <__alltraps>

0010238f <vector120>:
.globl vector120
vector120:
  pushl $0
  10238f:	6a 00                	push   $0x0
  pushl $120
  102391:	6a 78                	push   $0x78
  jmp __alltraps
  102393:	e9 3f 06 00 00       	jmp    1029d7 <__alltraps>

00102398 <vector121>:
.globl vector121
vector121:
  pushl $0
  102398:	6a 00                	push   $0x0
  pushl $121
  10239a:	6a 79                	push   $0x79
  jmp __alltraps
  10239c:	e9 36 06 00 00       	jmp    1029d7 <__alltraps>

001023a1 <vector122>:
.globl vector122
vector122:
  pushl $0
  1023a1:	6a 00                	push   $0x0
  pushl $122
  1023a3:	6a 7a                	push   $0x7a
  jmp __alltraps
  1023a5:	e9 2d 06 00 00       	jmp    1029d7 <__alltraps>

001023aa <vector123>:
.globl vector123
vector123:
  pushl $0
  1023aa:	6a 00                	push   $0x0
  pushl $123
  1023ac:	6a 7b                	push   $0x7b
  jmp __alltraps
  1023ae:	e9 24 06 00 00       	jmp    1029d7 <__alltraps>

001023b3 <vector124>:
.globl vector124
vector124:
  pushl $0
  1023b3:	6a 00                	push   $0x0
  pushl $124
  1023b5:	6a 7c                	push   $0x7c
  jmp __alltraps
  1023b7:	e9 1b 06 00 00       	jmp    1029d7 <__alltraps>

001023bc <vector125>:
.globl vector125
vector125:
  pushl $0
  1023bc:	6a 00                	push   $0x0
  pushl $125
  1023be:	6a 7d                	push   $0x7d
  jmp __alltraps
  1023c0:	e9 12 06 00 00       	jmp    1029d7 <__alltraps>

001023c5 <vector126>:
.globl vector126
vector126:
  pushl $0
  1023c5:	6a 00                	push   $0x0
  pushl $126
  1023c7:	6a 7e                	push   $0x7e
  jmp __alltraps
  1023c9:	e9 09 06 00 00       	jmp    1029d7 <__alltraps>

001023ce <vector127>:
.globl vector127
vector127:
  pushl $0
  1023ce:	6a 00                	push   $0x0
  pushl $127
  1023d0:	6a 7f                	push   $0x7f
  jmp __alltraps
  1023d2:	e9 00 06 00 00       	jmp    1029d7 <__alltraps>

001023d7 <vector128>:
.globl vector128
vector128:
  pushl $0
  1023d7:	6a 00                	push   $0x0
  pushl $128
  1023d9:	68 80 00 00 00       	push   $0x80
  jmp __alltraps
  1023de:	e9 f4 05 00 00       	jmp    1029d7 <__alltraps>

001023e3 <vector129>:
.globl vector129
vector129:
  pushl $0
  1023e3:	6a 00                	push   $0x0
  pushl $129
  1023e5:	68 81 00 00 00       	push   $0x81
  jmp __alltraps
  1023ea:	e9 e8 05 00 00       	jmp    1029d7 <__alltraps>

001023ef <vector130>:
.globl vector130
vector130:
  pushl $0
  1023ef:	6a 00                	push   $0x0
  pushl $130
  1023f1:	68 82 00 00 00       	push   $0x82
  jmp __alltraps
  1023f6:	e9 dc 05 00 00       	jmp    1029d7 <__alltraps>

001023fb <vector131>:
.globl vector131
vector131:
  pushl $0
  1023fb:	6a 00                	push   $0x0
  pushl $131
  1023fd:	68 83 00 00 00       	push   $0x83
  jmp __alltraps
  102402:	e9 d0 05 00 00       	jmp    1029d7 <__alltraps>

00102407 <vector132>:
.globl vector132
vector132:
  pushl $0
  102407:	6a 00                	push   $0x0
  pushl $132
  102409:	68 84 00 00 00       	push   $0x84
  jmp __alltraps
  10240e:	e9 c4 05 00 00       	jmp    1029d7 <__alltraps>

00102413 <vector133>:
.globl vector133
vector133:
  pushl $0
  102413:	6a 00                	push   $0x0
  pushl $133
  102415:	68 85 00 00 00       	push   $0x85
  jmp __alltraps
  10241a:	e9 b8 05 00 00       	jmp    1029d7 <__alltraps>

0010241f <vector134>:
.globl vector134
vector134:
  pushl $0
  10241f:	6a 00                	push   $0x0
  pushl $134
  102421:	68 86 00 00 00       	push   $0x86
  jmp __alltraps
  102426:	e9 ac 05 00 00       	jmp    1029d7 <__alltraps>

0010242b <vector135>:
.globl vector135
vector135:
  pushl $0
  10242b:	6a 00                	push   $0x0
  pushl $135
  10242d:	68 87 00 00 00       	push   $0x87
  jmp __alltraps
  102432:	e9 a0 05 00 00       	jmp    1029d7 <__alltraps>

00102437 <vector136>:
.globl vector136
vector136:
  pushl $0
  102437:	6a 00                	push   $0x0
  pushl $136
  102439:	68 88 00 00 00       	push   $0x88
  jmp __alltraps
  10243e:	e9 94 05 00 00       	jmp    1029d7 <__alltraps>

00102443 <vector137>:
.globl vector137
vector137:
  pushl $0
  102443:	6a 00                	push   $0x0
  pushl $137
  102445:	68 89 00 00 00       	push   $0x89
  jmp __alltraps
  10244a:	e9 88 05 00 00       	jmp    1029d7 <__alltraps>

0010244f <vector138>:
.globl vector138
vector138:
  pushl $0
  10244f:	6a 00                	push   $0x0
  pushl $138
  102451:	68 8a 00 00 00       	push   $0x8a
  jmp __alltraps
  102456:	e9 7c 05 00 00       	jmp    1029d7 <__alltraps>

0010245b <vector139>:
.globl vector139
vector139:
  pushl $0
  10245b:	6a 00                	push   $0x0
  pushl $139
  10245d:	68 8b 00 00 00       	push   $0x8b
  jmp __alltraps
  102462:	e9 70 05 00 00       	jmp    1029d7 <__alltraps>

00102467 <vector140>:
.globl vector140
vector140:
  pushl $0
  102467:	6a 00                	push   $0x0
  pushl $140
  102469:	68 8c 00 00 00       	push   $0x8c
  jmp __alltraps
  10246e:	e9 64 05 00 00       	jmp    1029d7 <__alltraps>

00102473 <vector141>:
.globl vector141
vector141:
  pushl $0
  102473:	6a 00                	push   $0x0
  pushl $141
  102475:	68 8d 00 00 00       	push   $0x8d
  jmp __alltraps
  10247a:	e9 58 05 00 00       	jmp    1029d7 <__alltraps>

0010247f <vector142>:
.globl vector142
vector142:
  pushl $0
  10247f:	6a 00                	push   $0x0
  pushl $142
  102481:	68 8e 00 00 00       	push   $0x8e
  jmp __alltraps
  102486:	e9 4c 05 00 00       	jmp    1029d7 <__alltraps>

0010248b <vector143>:
.globl vector143
vector143:
  pushl $0
  10248b:	6a 00                	push   $0x0
  pushl $143
  10248d:	68 8f 00 00 00       	push   $0x8f
  jmp __alltraps
  102492:	e9 40 05 00 00       	jmp    1029d7 <__alltraps>

00102497 <vector144>:
.globl vector144
vector144:
  pushl $0
  102497:	6a 00                	push   $0x0
  pushl $144
  102499:	68 90 00 00 00       	push   $0x90
  jmp __alltraps
  10249e:	e9 34 05 00 00       	jmp    1029d7 <__alltraps>

001024a3 <vector145>:
.globl vector145
vector145:
  pushl $0
  1024a3:	6a 00                	push   $0x0
  pushl $145
  1024a5:	68 91 00 00 00       	push   $0x91
  jmp __alltraps
  1024aa:	e9 28 05 00 00       	jmp    1029d7 <__alltraps>

001024af <vector146>:
.globl vector146
vector146:
  pushl $0
  1024af:	6a 00                	push   $0x0
  pushl $146
  1024b1:	68 92 00 00 00       	push   $0x92
  jmp __alltraps
  1024b6:	e9 1c 05 00 00       	jmp    1029d7 <__alltraps>

001024bb <vector147>:
.globl vector147
vector147:
  pushl $0
  1024bb:	6a 00                	push   $0x0
  pushl $147
  1024bd:	68 93 00 00 00       	push   $0x93
  jmp __alltraps
  1024c2:	e9 10 05 00 00       	jmp    1029d7 <__alltraps>

001024c7 <vector148>:
.globl vector148
vector148:
  pushl $0
  1024c7:	6a 00                	push   $0x0
  pushl $148
  1024c9:	68 94 00 00 00       	push   $0x94
  jmp __alltraps
  1024ce:	e9 04 05 00 00       	jmp    1029d7 <__alltraps>

001024d3 <vector149>:
.globl vector149
vector149:
  pushl $0
  1024d3:	6a 00                	push   $0x0
  pushl $149
  1024d5:	68 95 00 00 00       	push   $0x95
  jmp __alltraps
  1024da:	e9 f8 04 00 00       	jmp    1029d7 <__alltraps>

001024df <vector150>:
.globl vector150
vector150:
  pushl $0
  1024df:	6a 00                	push   $0x0
  pushl $150
  1024e1:	68 96 00 00 00       	push   $0x96
  jmp __alltraps
  1024e6:	e9 ec 04 00 00       	jmp    1029d7 <__alltraps>

001024eb <vector151>:
.globl vector151
vector151:
  pushl $0
  1024eb:	6a 00                	push   $0x0
  pushl $151
  1024ed:	68 97 00 00 00       	push   $0x97
  jmp __alltraps
  1024f2:	e9 e0 04 00 00       	jmp    1029d7 <__alltraps>

001024f7 <vector152>:
.globl vector152
vector152:
  pushl $0
  1024f7:	6a 00                	push   $0x0
  pushl $152
  1024f9:	68 98 00 00 00       	push   $0x98
  jmp __alltraps
  1024fe:	e9 d4 04 00 00       	jmp    1029d7 <__alltraps>

00102503 <vector153>:
.globl vector153
vector153:
  pushl $0
  102503:	6a 00                	push   $0x0
  pushl $153
  102505:	68 99 00 00 00       	push   $0x99
  jmp __alltraps
  10250a:	e9 c8 04 00 00       	jmp    1029d7 <__alltraps>

0010250f <vector154>:
.globl vector154
vector154:
  pushl $0
  10250f:	6a 00                	push   $0x0
  pushl $154
  102511:	68 9a 00 00 00       	push   $0x9a
  jmp __alltraps
  102516:	e9 bc 04 00 00       	jmp    1029d7 <__alltraps>

0010251b <vector155>:
.globl vector155
vector155:
  pushl $0
  10251b:	6a 00                	push   $0x0
  pushl $155
  10251d:	68 9b 00 00 00       	push   $0x9b
  jmp __alltraps
  102522:	e9 b0 04 00 00       	jmp    1029d7 <__alltraps>

00102527 <vector156>:
.globl vector156
vector156:
  pushl $0
  102527:	6a 00                	push   $0x0
  pushl $156
  102529:	68 9c 00 00 00       	push   $0x9c
  jmp __alltraps
  10252e:	e9 a4 04 00 00       	jmp    1029d7 <__alltraps>

00102533 <vector157>:
.globl vector157
vector157:
  pushl $0
  102533:	6a 00                	push   $0x0
  pushl $157
  102535:	68 9d 00 00 00       	push   $0x9d
  jmp __alltraps
  10253a:	e9 98 04 00 00       	jmp    1029d7 <__alltraps>

0010253f <vector158>:
.globl vector158
vector158:
  pushl $0
  10253f:	6a 00                	push   $0x0
  pushl $158
  102541:	68 9e 00 00 00       	push   $0x9e
  jmp __alltraps
  102546:	e9 8c 04 00 00       	jmp    1029d7 <__alltraps>

0010254b <vector159>:
.globl vector159
vector159:
  pushl $0
  10254b:	6a 00                	push   $0x0
  pushl $159
  10254d:	68 9f 00 00 00       	push   $0x9f
  jmp __alltraps
  102552:	e9 80 04 00 00       	jmp    1029d7 <__alltraps>

00102557 <vector160>:
.globl vector160
vector160:
  pushl $0
  102557:	6a 00                	push   $0x0
  pushl $160
  102559:	68 a0 00 00 00       	push   $0xa0
  jmp __alltraps
  10255e:	e9 74 04 00 00       	jmp    1029d7 <__alltraps>

00102563 <vector161>:
.globl vector161
vector161:
  pushl $0
  102563:	6a 00                	push   $0x0
  pushl $161
  102565:	68 a1 00 00 00       	push   $0xa1
  jmp __alltraps
  10256a:	e9 68 04 00 00       	jmp    1029d7 <__alltraps>

0010256f <vector162>:
.globl vector162
vector162:
  pushl $0
  10256f:	6a 00                	push   $0x0
  pushl $162
  102571:	68 a2 00 00 00       	push   $0xa2
  jmp __alltraps
  102576:	e9 5c 04 00 00       	jmp    1029d7 <__alltraps>

0010257b <vector163>:
.globl vector163
vector163:
  pushl $0
  10257b:	6a 00                	push   $0x0
  pushl $163
  10257d:	68 a3 00 00 00       	push   $0xa3
  jmp __alltraps
  102582:	e9 50 04 00 00       	jmp    1029d7 <__alltraps>

00102587 <vector164>:
.globl vector164
vector164:
  pushl $0
  102587:	6a 00                	push   $0x0
  pushl $164
  102589:	68 a4 00 00 00       	push   $0xa4
  jmp __alltraps
  10258e:	e9 44 04 00 00       	jmp    1029d7 <__alltraps>

00102593 <vector165>:
.globl vector165
vector165:
  pushl $0
  102593:	6a 00                	push   $0x0
  pushl $165
  102595:	68 a5 00 00 00       	push   $0xa5
  jmp __alltraps
  10259a:	e9 38 04 00 00       	jmp    1029d7 <__alltraps>

0010259f <vector166>:
.globl vector166
vector166:
  pushl $0
  10259f:	6a 00                	push   $0x0
  pushl $166
  1025a1:	68 a6 00 00 00       	push   $0xa6
  jmp __alltraps
  1025a6:	e9 2c 04 00 00       	jmp    1029d7 <__alltraps>

001025ab <vector167>:
.globl vector167
vector167:
  pushl $0
  1025ab:	6a 00                	push   $0x0
  pushl $167
  1025ad:	68 a7 00 00 00       	push   $0xa7
  jmp __alltraps
  1025b2:	e9 20 04 00 00       	jmp    1029d7 <__alltraps>

001025b7 <vector168>:
.globl vector168
vector168:
  pushl $0
  1025b7:	6a 00                	push   $0x0
  pushl $168
  1025b9:	68 a8 00 00 00       	push   $0xa8
  jmp __alltraps
  1025be:	e9 14 04 00 00       	jmp    1029d7 <__alltraps>

001025c3 <vector169>:
.globl vector169
vector169:
  pushl $0
  1025c3:	6a 00                	push   $0x0
  pushl $169
  1025c5:	68 a9 00 00 00       	push   $0xa9
  jmp __alltraps
  1025ca:	e9 08 04 00 00       	jmp    1029d7 <__alltraps>

001025cf <vector170>:
.globl vector170
vector170:
  pushl $0
  1025cf:	6a 00                	push   $0x0
  pushl $170
  1025d1:	68 aa 00 00 00       	push   $0xaa
  jmp __alltraps
  1025d6:	e9 fc 03 00 00       	jmp    1029d7 <__alltraps>

001025db <vector171>:
.globl vector171
vector171:
  pushl $0
  1025db:	6a 00                	push   $0x0
  pushl $171
  1025dd:	68 ab 00 00 00       	push   $0xab
  jmp __alltraps
  1025e2:	e9 f0 03 00 00       	jmp    1029d7 <__alltraps>

001025e7 <vector172>:
.globl vector172
vector172:
  pushl $0
  1025e7:	6a 00                	push   $0x0
  pushl $172
  1025e9:	68 ac 00 00 00       	push   $0xac
  jmp __alltraps
  1025ee:	e9 e4 03 00 00       	jmp    1029d7 <__alltraps>

001025f3 <vector173>:
.globl vector173
vector173:
  pushl $0
  1025f3:	6a 00                	push   $0x0
  pushl $173
  1025f5:	68 ad 00 00 00       	push   $0xad
  jmp __alltraps
  1025fa:	e9 d8 03 00 00       	jmp    1029d7 <__alltraps>

001025ff <vector174>:
.globl vector174
vector174:
  pushl $0
  1025ff:	6a 00                	push   $0x0
  pushl $174
  102601:	68 ae 00 00 00       	push   $0xae
  jmp __alltraps
  102606:	e9 cc 03 00 00       	jmp    1029d7 <__alltraps>

0010260b <vector175>:
.globl vector175
vector175:
  pushl $0
  10260b:	6a 00                	push   $0x0
  pushl $175
  10260d:	68 af 00 00 00       	push   $0xaf
  jmp __alltraps
  102612:	e9 c0 03 00 00       	jmp    1029d7 <__alltraps>

00102617 <vector176>:
.globl vector176
vector176:
  pushl $0
  102617:	6a 00                	push   $0x0
  pushl $176
  102619:	68 b0 00 00 00       	push   $0xb0
  jmp __alltraps
  10261e:	e9 b4 03 00 00       	jmp    1029d7 <__alltraps>

00102623 <vector177>:
.globl vector177
vector177:
  pushl $0
  102623:	6a 00                	push   $0x0
  pushl $177
  102625:	68 b1 00 00 00       	push   $0xb1
  jmp __alltraps
  10262a:	e9 a8 03 00 00       	jmp    1029d7 <__alltraps>

0010262f <vector178>:
.globl vector178
vector178:
  pushl $0
  10262f:	6a 00                	push   $0x0
  pushl $178
  102631:	68 b2 00 00 00       	push   $0xb2
  jmp __alltraps
  102636:	e9 9c 03 00 00       	jmp    1029d7 <__alltraps>

0010263b <vector179>:
.globl vector179
vector179:
  pushl $0
  10263b:	6a 00                	push   $0x0
  pushl $179
  10263d:	68 b3 00 00 00       	push   $0xb3
  jmp __alltraps
  102642:	e9 90 03 00 00       	jmp    1029d7 <__alltraps>

00102647 <vector180>:
.globl vector180
vector180:
  pushl $0
  102647:	6a 00                	push   $0x0
  pushl $180
  102649:	68 b4 00 00 00       	push   $0xb4
  jmp __alltraps
  10264e:	e9 84 03 00 00       	jmp    1029d7 <__alltraps>

00102653 <vector181>:
.globl vector181
vector181:
  pushl $0
  102653:	6a 00                	push   $0x0
  pushl $181
  102655:	68 b5 00 00 00       	push   $0xb5
  jmp __alltraps
  10265a:	e9 78 03 00 00       	jmp    1029d7 <__alltraps>

0010265f <vector182>:
.globl vector182
vector182:
  pushl $0
  10265f:	6a 00                	push   $0x0
  pushl $182
  102661:	68 b6 00 00 00       	push   $0xb6
  jmp __alltraps
  102666:	e9 6c 03 00 00       	jmp    1029d7 <__alltraps>

0010266b <vector183>:
.globl vector183
vector183:
  pushl $0
  10266b:	6a 00                	push   $0x0
  pushl $183
  10266d:	68 b7 00 00 00       	push   $0xb7
  jmp __alltraps
  102672:	e9 60 03 00 00       	jmp    1029d7 <__alltraps>

00102677 <vector184>:
.globl vector184
vector184:
  pushl $0
  102677:	6a 00                	push   $0x0
  pushl $184
  102679:	68 b8 00 00 00       	push   $0xb8
  jmp __alltraps
  10267e:	e9 54 03 00 00       	jmp    1029d7 <__alltraps>

00102683 <vector185>:
.globl vector185
vector185:
  pushl $0
  102683:	6a 00                	push   $0x0
  pushl $185
  102685:	68 b9 00 00 00       	push   $0xb9
  jmp __alltraps
  10268a:	e9 48 03 00 00       	jmp    1029d7 <__alltraps>

0010268f <vector186>:
.globl vector186
vector186:
  pushl $0
  10268f:	6a 00                	push   $0x0
  pushl $186
  102691:	68 ba 00 00 00       	push   $0xba
  jmp __alltraps
  102696:	e9 3c 03 00 00       	jmp    1029d7 <__alltraps>

0010269b <vector187>:
.globl vector187
vector187:
  pushl $0
  10269b:	6a 00                	push   $0x0
  pushl $187
  10269d:	68 bb 00 00 00       	push   $0xbb
  jmp __alltraps
  1026a2:	e9 30 03 00 00       	jmp    1029d7 <__alltraps>

001026a7 <vector188>:
.globl vector188
vector188:
  pushl $0
  1026a7:	6a 00                	push   $0x0
  pushl $188
  1026a9:	68 bc 00 00 00       	push   $0xbc
  jmp __alltraps
  1026ae:	e9 24 03 00 00       	jmp    1029d7 <__alltraps>

001026b3 <vector189>:
.globl vector189
vector189:
  pushl $0
  1026b3:	6a 00                	push   $0x0
  pushl $189
  1026b5:	68 bd 00 00 00       	push   $0xbd
  jmp __alltraps
  1026ba:	e9 18 03 00 00       	jmp    1029d7 <__alltraps>

001026bf <vector190>:
.globl vector190
vector190:
  pushl $0
  1026bf:	6a 00                	push   $0x0
  pushl $190
  1026c1:	68 be 00 00 00       	push   $0xbe
  jmp __alltraps
  1026c6:	e9 0c 03 00 00       	jmp    1029d7 <__alltraps>

001026cb <vector191>:
.globl vector191
vector191:
  pushl $0
  1026cb:	6a 00                	push   $0x0
  pushl $191
  1026cd:	68 bf 00 00 00       	push   $0xbf
  jmp __alltraps
  1026d2:	e9 00 03 00 00       	jmp    1029d7 <__alltraps>

001026d7 <vector192>:
.globl vector192
vector192:
  pushl $0
  1026d7:	6a 00                	push   $0x0
  pushl $192
  1026d9:	68 c0 00 00 00       	push   $0xc0
  jmp __alltraps
  1026de:	e9 f4 02 00 00       	jmp    1029d7 <__alltraps>

001026e3 <vector193>:
.globl vector193
vector193:
  pushl $0
  1026e3:	6a 00                	push   $0x0
  pushl $193
  1026e5:	68 c1 00 00 00       	push   $0xc1
  jmp __alltraps
  1026ea:	e9 e8 02 00 00       	jmp    1029d7 <__alltraps>

001026ef <vector194>:
.globl vector194
vector194:
  pushl $0
  1026ef:	6a 00                	push   $0x0
  pushl $194
  1026f1:	68 c2 00 00 00       	push   $0xc2
  jmp __alltraps
  1026f6:	e9 dc 02 00 00       	jmp    1029d7 <__alltraps>

001026fb <vector195>:
.globl vector195
vector195:
  pushl $0
  1026fb:	6a 00                	push   $0x0
  pushl $195
  1026fd:	68 c3 00 00 00       	push   $0xc3
  jmp __alltraps
  102702:	e9 d0 02 00 00       	jmp    1029d7 <__alltraps>

00102707 <vector196>:
.globl vector196
vector196:
  pushl $0
  102707:	6a 00                	push   $0x0
  pushl $196
  102709:	68 c4 00 00 00       	push   $0xc4
  jmp __alltraps
  10270e:	e9 c4 02 00 00       	jmp    1029d7 <__alltraps>

00102713 <vector197>:
.globl vector197
vector197:
  pushl $0
  102713:	6a 00                	push   $0x0
  pushl $197
  102715:	68 c5 00 00 00       	push   $0xc5
  jmp __alltraps
  10271a:	e9 b8 02 00 00       	jmp    1029d7 <__alltraps>

0010271f <vector198>:
.globl vector198
vector198:
  pushl $0
  10271f:	6a 00                	push   $0x0
  pushl $198
  102721:	68 c6 00 00 00       	push   $0xc6
  jmp __alltraps
  102726:	e9 ac 02 00 00       	jmp    1029d7 <__alltraps>

0010272b <vector199>:
.globl vector199
vector199:
  pushl $0
  10272b:	6a 00                	push   $0x0
  pushl $199
  10272d:	68 c7 00 00 00       	push   $0xc7
  jmp __alltraps
  102732:	e9 a0 02 00 00       	jmp    1029d7 <__alltraps>

00102737 <vector200>:
.globl vector200
vector200:
  pushl $0
  102737:	6a 00                	push   $0x0
  pushl $200
  102739:	68 c8 00 00 00       	push   $0xc8
  jmp __alltraps
  10273e:	e9 94 02 00 00       	jmp    1029d7 <__alltraps>

00102743 <vector201>:
.globl vector201
vector201:
  pushl $0
  102743:	6a 00                	push   $0x0
  pushl $201
  102745:	68 c9 00 00 00       	push   $0xc9
  jmp __alltraps
  10274a:	e9 88 02 00 00       	jmp    1029d7 <__alltraps>

0010274f <vector202>:
.globl vector202
vector202:
  pushl $0
  10274f:	6a 00                	push   $0x0
  pushl $202
  102751:	68 ca 00 00 00       	push   $0xca
  jmp __alltraps
  102756:	e9 7c 02 00 00       	jmp    1029d7 <__alltraps>

0010275b <vector203>:
.globl vector203
vector203:
  pushl $0
  10275b:	6a 00                	push   $0x0
  pushl $203
  10275d:	68 cb 00 00 00       	push   $0xcb
  jmp __alltraps
  102762:	e9 70 02 00 00       	jmp    1029d7 <__alltraps>

00102767 <vector204>:
.globl vector204
vector204:
  pushl $0
  102767:	6a 00                	push   $0x0
  pushl $204
  102769:	68 cc 00 00 00       	push   $0xcc
  jmp __alltraps
  10276e:	e9 64 02 00 00       	jmp    1029d7 <__alltraps>

00102773 <vector205>:
.globl vector205
vector205:
  pushl $0
  102773:	6a 00                	push   $0x0
  pushl $205
  102775:	68 cd 00 00 00       	push   $0xcd
  jmp __alltraps
  10277a:	e9 58 02 00 00       	jmp    1029d7 <__alltraps>

0010277f <vector206>:
.globl vector206
vector206:
  pushl $0
  10277f:	6a 00                	push   $0x0
  pushl $206
  102781:	68 ce 00 00 00       	push   $0xce
  jmp __alltraps
  102786:	e9 4c 02 00 00       	jmp    1029d7 <__alltraps>

0010278b <vector207>:
.globl vector207
vector207:
  pushl $0
  10278b:	6a 00                	push   $0x0
  pushl $207
  10278d:	68 cf 00 00 00       	push   $0xcf
  jmp __alltraps
  102792:	e9 40 02 00 00       	jmp    1029d7 <__alltraps>

00102797 <vector208>:
.globl vector208
vector208:
  pushl $0
  102797:	6a 00                	push   $0x0
  pushl $208
  102799:	68 d0 00 00 00       	push   $0xd0
  jmp __alltraps
  10279e:	e9 34 02 00 00       	jmp    1029d7 <__alltraps>

001027a3 <vector209>:
.globl vector209
vector209:
  pushl $0
  1027a3:	6a 00                	push   $0x0
  pushl $209
  1027a5:	68 d1 00 00 00       	push   $0xd1
  jmp __alltraps
  1027aa:	e9 28 02 00 00       	jmp    1029d7 <__alltraps>

001027af <vector210>:
.globl vector210
vector210:
  pushl $0
  1027af:	6a 00                	push   $0x0
  pushl $210
  1027b1:	68 d2 00 00 00       	push   $0xd2
  jmp __alltraps
  1027b6:	e9 1c 02 00 00       	jmp    1029d7 <__alltraps>

001027bb <vector211>:
.globl vector211
vector211:
  pushl $0
  1027bb:	6a 00                	push   $0x0
  pushl $211
  1027bd:	68 d3 00 00 00       	push   $0xd3
  jmp __alltraps
  1027c2:	e9 10 02 00 00       	jmp    1029d7 <__alltraps>

001027c7 <vector212>:
.globl vector212
vector212:
  pushl $0
  1027c7:	6a 00                	push   $0x0
  pushl $212
  1027c9:	68 d4 00 00 00       	push   $0xd4
  jmp __alltraps
  1027ce:	e9 04 02 00 00       	jmp    1029d7 <__alltraps>

001027d3 <vector213>:
.globl vector213
vector213:
  pushl $0
  1027d3:	6a 00                	push   $0x0
  pushl $213
  1027d5:	68 d5 00 00 00       	push   $0xd5
  jmp __alltraps
  1027da:	e9 f8 01 00 00       	jmp    1029d7 <__alltraps>

001027df <vector214>:
.globl vector214
vector214:
  pushl $0
  1027df:	6a 00                	push   $0x0
  pushl $214
  1027e1:	68 d6 00 00 00       	push   $0xd6
  jmp __alltraps
  1027e6:	e9 ec 01 00 00       	jmp    1029d7 <__alltraps>

001027eb <vector215>:
.globl vector215
vector215:
  pushl $0
  1027eb:	6a 00                	push   $0x0
  pushl $215
  1027ed:	68 d7 00 00 00       	push   $0xd7
  jmp __alltraps
  1027f2:	e9 e0 01 00 00       	jmp    1029d7 <__alltraps>

001027f7 <vector216>:
.globl vector216
vector216:
  pushl $0
  1027f7:	6a 00                	push   $0x0
  pushl $216
  1027f9:	68 d8 00 00 00       	push   $0xd8
  jmp __alltraps
  1027fe:	e9 d4 01 00 00       	jmp    1029d7 <__alltraps>

00102803 <vector217>:
.globl vector217
vector217:
  pushl $0
  102803:	6a 00                	push   $0x0
  pushl $217
  102805:	68 d9 00 00 00       	push   $0xd9
  jmp __alltraps
  10280a:	e9 c8 01 00 00       	jmp    1029d7 <__alltraps>

0010280f <vector218>:
.globl vector218
vector218:
  pushl $0
  10280f:	6a 00                	push   $0x0
  pushl $218
  102811:	68 da 00 00 00       	push   $0xda
  jmp __alltraps
  102816:	e9 bc 01 00 00       	jmp    1029d7 <__alltraps>

0010281b <vector219>:
.globl vector219
vector219:
  pushl $0
  10281b:	6a 00                	push   $0x0
  pushl $219
  10281d:	68 db 00 00 00       	push   $0xdb
  jmp __alltraps
  102822:	e9 b0 01 00 00       	jmp    1029d7 <__alltraps>

00102827 <vector220>:
.globl vector220
vector220:
  pushl $0
  102827:	6a 00                	push   $0x0
  pushl $220
  102829:	68 dc 00 00 00       	push   $0xdc
  jmp __alltraps
  10282e:	e9 a4 01 00 00       	jmp    1029d7 <__alltraps>

00102833 <vector221>:
.globl vector221
vector221:
  pushl $0
  102833:	6a 00                	push   $0x0
  pushl $221
  102835:	68 dd 00 00 00       	push   $0xdd
  jmp __alltraps
  10283a:	e9 98 01 00 00       	jmp    1029d7 <__alltraps>

0010283f <vector222>:
.globl vector222
vector222:
  pushl $0
  10283f:	6a 00                	push   $0x0
  pushl $222
  102841:	68 de 00 00 00       	push   $0xde
  jmp __alltraps
  102846:	e9 8c 01 00 00       	jmp    1029d7 <__alltraps>

0010284b <vector223>:
.globl vector223
vector223:
  pushl $0
  10284b:	6a 00                	push   $0x0
  pushl $223
  10284d:	68 df 00 00 00       	push   $0xdf
  jmp __alltraps
  102852:	e9 80 01 00 00       	jmp    1029d7 <__alltraps>

00102857 <vector224>:
.globl vector224
vector224:
  pushl $0
  102857:	6a 00                	push   $0x0
  pushl $224
  102859:	68 e0 00 00 00       	push   $0xe0
  jmp __alltraps
  10285e:	e9 74 01 00 00       	jmp    1029d7 <__alltraps>

00102863 <vector225>:
.globl vector225
vector225:
  pushl $0
  102863:	6a 00                	push   $0x0
  pushl $225
  102865:	68 e1 00 00 00       	push   $0xe1
  jmp __alltraps
  10286a:	e9 68 01 00 00       	jmp    1029d7 <__alltraps>

0010286f <vector226>:
.globl vector226
vector226:
  pushl $0
  10286f:	6a 00                	push   $0x0
  pushl $226
  102871:	68 e2 00 00 00       	push   $0xe2
  jmp __alltraps
  102876:	e9 5c 01 00 00       	jmp    1029d7 <__alltraps>

0010287b <vector227>:
.globl vector227
vector227:
  pushl $0
  10287b:	6a 00                	push   $0x0
  pushl $227
  10287d:	68 e3 00 00 00       	push   $0xe3
  jmp __alltraps
  102882:	e9 50 01 00 00       	jmp    1029d7 <__alltraps>

00102887 <vector228>:
.globl vector228
vector228:
  pushl $0
  102887:	6a 00                	push   $0x0
  pushl $228
  102889:	68 e4 00 00 00       	push   $0xe4
  jmp __alltraps
  10288e:	e9 44 01 00 00       	jmp    1029d7 <__alltraps>

00102893 <vector229>:
.globl vector229
vector229:
  pushl $0
  102893:	6a 00                	push   $0x0
  pushl $229
  102895:	68 e5 00 00 00       	push   $0xe5
  jmp __alltraps
  10289a:	e9 38 01 00 00       	jmp    1029d7 <__alltraps>

0010289f <vector230>:
.globl vector230
vector230:
  pushl $0
  10289f:	6a 00                	push   $0x0
  pushl $230
  1028a1:	68 e6 00 00 00       	push   $0xe6
  jmp __alltraps
  1028a6:	e9 2c 01 00 00       	jmp    1029d7 <__alltraps>

001028ab <vector231>:
.globl vector231
vector231:
  pushl $0
  1028ab:	6a 00                	push   $0x0
  pushl $231
  1028ad:	68 e7 00 00 00       	push   $0xe7
  jmp __alltraps
  1028b2:	e9 20 01 00 00       	jmp    1029d7 <__alltraps>

001028b7 <vector232>:
.globl vector232
vector232:
  pushl $0
  1028b7:	6a 00                	push   $0x0
  pushl $232
  1028b9:	68 e8 00 00 00       	push   $0xe8
  jmp __alltraps
  1028be:	e9 14 01 00 00       	jmp    1029d7 <__alltraps>

001028c3 <vector233>:
.globl vector233
vector233:
  pushl $0
  1028c3:	6a 00                	push   $0x0
  pushl $233
  1028c5:	68 e9 00 00 00       	push   $0xe9
  jmp __alltraps
  1028ca:	e9 08 01 00 00       	jmp    1029d7 <__alltraps>

001028cf <vector234>:
.globl vector234
vector234:
  pushl $0
  1028cf:	6a 00                	push   $0x0
  pushl $234
  1028d1:	68 ea 00 00 00       	push   $0xea
  jmp __alltraps
  1028d6:	e9 fc 00 00 00       	jmp    1029d7 <__alltraps>

001028db <vector235>:
.globl vector235
vector235:
  pushl $0
  1028db:	6a 00                	push   $0x0
  pushl $235
  1028dd:	68 eb 00 00 00       	push   $0xeb
  jmp __alltraps
  1028e2:	e9 f0 00 00 00       	jmp    1029d7 <__alltraps>

001028e7 <vector236>:
.globl vector236
vector236:
  pushl $0
  1028e7:	6a 00                	push   $0x0
  pushl $236
  1028e9:	68 ec 00 00 00       	push   $0xec
  jmp __alltraps
  1028ee:	e9 e4 00 00 00       	jmp    1029d7 <__alltraps>

001028f3 <vector237>:
.globl vector237
vector237:
  pushl $0
  1028f3:	6a 00                	push   $0x0
  pushl $237
  1028f5:	68 ed 00 00 00       	push   $0xed
  jmp __alltraps
  1028fa:	e9 d8 00 00 00       	jmp    1029d7 <__alltraps>

001028ff <vector238>:
.globl vector238
vector238:
  pushl $0
  1028ff:	6a 00                	push   $0x0
  pushl $238
  102901:	68 ee 00 00 00       	push   $0xee
  jmp __alltraps
  102906:	e9 cc 00 00 00       	jmp    1029d7 <__alltraps>

0010290b <vector239>:
.globl vector239
vector239:
  pushl $0
  10290b:	6a 00                	push   $0x0
  pushl $239
  10290d:	68 ef 00 00 00       	push   $0xef
  jmp __alltraps
  102912:	e9 c0 00 00 00       	jmp    1029d7 <__alltraps>

00102917 <vector240>:
.globl vector240
vector240:
  pushl $0
  102917:	6a 00                	push   $0x0
  pushl $240
  102919:	68 f0 00 00 00       	push   $0xf0
  jmp __alltraps
  10291e:	e9 b4 00 00 00       	jmp    1029d7 <__alltraps>

00102923 <vector241>:
.globl vector241
vector241:
  pushl $0
  102923:	6a 00                	push   $0x0
  pushl $241
  102925:	68 f1 00 00 00       	push   $0xf1
  jmp __alltraps
  10292a:	e9 a8 00 00 00       	jmp    1029d7 <__alltraps>

0010292f <vector242>:
.globl vector242
vector242:
  pushl $0
  10292f:	6a 00                	push   $0x0
  pushl $242
  102931:	68 f2 00 00 00       	push   $0xf2
  jmp __alltraps
  102936:	e9 9c 00 00 00       	jmp    1029d7 <__alltraps>

0010293b <vector243>:
.globl vector243
vector243:
  pushl $0
  10293b:	6a 00                	push   $0x0
  pushl $243
  10293d:	68 f3 00 00 00       	push   $0xf3
  jmp __alltraps
  102942:	e9 90 00 00 00       	jmp    1029d7 <__alltraps>

00102947 <vector244>:
.globl vector244
vector244:
  pushl $0
  102947:	6a 00                	push   $0x0
  pushl $244
  102949:	68 f4 00 00 00       	push   $0xf4
  jmp __alltraps
  10294e:	e9 84 00 00 00       	jmp    1029d7 <__alltraps>

00102953 <vector245>:
.globl vector245
vector245:
  pushl $0
  102953:	6a 00                	push   $0x0
  pushl $245
  102955:	68 f5 00 00 00       	push   $0xf5
  jmp __alltraps
  10295a:	e9 78 00 00 00       	jmp    1029d7 <__alltraps>

0010295f <vector246>:
.globl vector246
vector246:
  pushl $0
  10295f:	6a 00                	push   $0x0
  pushl $246
  102961:	68 f6 00 00 00       	push   $0xf6
  jmp __alltraps
  102966:	e9 6c 00 00 00       	jmp    1029d7 <__alltraps>

0010296b <vector247>:
.globl vector247
vector247:
  pushl $0
  10296b:	6a 00                	push   $0x0
  pushl $247
  10296d:	68 f7 00 00 00       	push   $0xf7
  jmp __alltraps
  102972:	e9 60 00 00 00       	jmp    1029d7 <__alltraps>

00102977 <vector248>:
.globl vector248
vector248:
  pushl $0
  102977:	6a 00                	push   $0x0
  pushl $248
  102979:	68 f8 00 00 00       	push   $0xf8
  jmp __alltraps
  10297e:	e9 54 00 00 00       	jmp    1029d7 <__alltraps>

00102983 <vector249>:
.globl vector249
vector249:
  pushl $0
  102983:	6a 00                	push   $0x0
  pushl $249
  102985:	68 f9 00 00 00       	push   $0xf9
  jmp __alltraps
  10298a:	e9 48 00 00 00       	jmp    1029d7 <__alltraps>

0010298f <vector250>:
.globl vector250
vector250:
  pushl $0
  10298f:	6a 00                	push   $0x0
  pushl $250
  102991:	68 fa 00 00 00       	push   $0xfa
  jmp __alltraps
  102996:	e9 3c 00 00 00       	jmp    1029d7 <__alltraps>

0010299b <vector251>:
.globl vector251
vector251:
  pushl $0
  10299b:	6a 00                	push   $0x0
  pushl $251
  10299d:	68 fb 00 00 00       	push   $0xfb
  jmp __alltraps
  1029a2:	e9 30 00 00 00       	jmp    1029d7 <__alltraps>

001029a7 <vector252>:
.globl vector252
vector252:
  pushl $0
  1029a7:	6a 00                	push   $0x0
  pushl $252
  1029a9:	68 fc 00 00 00       	push   $0xfc
  jmp __alltraps
  1029ae:	e9 24 00 00 00       	jmp    1029d7 <__alltraps>

001029b3 <vector253>:
.globl vector253
vector253:
  pushl $0
  1029b3:	6a 00                	push   $0x0
  pushl $253
  1029b5:	68 fd 00 00 00       	push   $0xfd
  jmp __alltraps
  1029ba:	e9 18 00 00 00       	jmp    1029d7 <__alltraps>

001029bf <vector254>:
.globl vector254
vector254:
  pushl $0
  1029bf:	6a 00                	push   $0x0
  pushl $254
  1029c1:	68 fe 00 00 00       	push   $0xfe
  jmp __alltraps
  1029c6:	e9 0c 00 00 00       	jmp    1029d7 <__alltraps>

001029cb <vector255>:
.globl vector255
vector255:
  pushl $0
  1029cb:	6a 00                	push   $0x0
  pushl $255
  1029cd:	68 ff 00 00 00       	push   $0xff
  jmp __alltraps
  1029d2:	e9 00 00 00 00       	jmp    1029d7 <__alltraps>

001029d7 <__alltraps>:
.text
.globl __alltraps
__alltraps:
    # push registers to build a trap frame
    # therefore make the stack look like a struct trapframe
    pushl %ds
  1029d7:	1e                   	push   %ds
    pushl %es
  1029d8:	06                   	push   %es
    pushl %fs
  1029d9:	0f a0                	push   %fs
    pushl %gs
  1029db:	0f a8                	push   %gs
    pushal
  1029dd:	60                   	pusha  

    # load GD_KDATA into %ds and %es to set up data segments for kernel
    movl $GD_KDATA, %eax
  1029de:	b8 10 00 00 00       	mov    $0x10,%eax
    movw %ax, %ds
  1029e3:	8e d8                	mov    %eax,%ds
    movw %ax, %es
  1029e5:	8e c0                	mov    %eax,%es

    # push %esp to pass a pointer to the trapframe as an argument to trap()
    pushl %esp
  1029e7:	54                   	push   %esp

    # call trap(tf), where tf=%esp
    call trap
  1029e8:	e8 60 f5 ff ff       	call   101f4d <trap>

    # pop the pushed stack pointer
    popl %esp
  1029ed:	5c                   	pop    %esp

001029ee <__trapret>:

    # return falls through to trapret...
.globl __trapret
__trapret:
    # restore registers from stack
    popal
  1029ee:	61                   	popa   

    # restore %ds, %es, %fs and %gs
    popl %gs
  1029ef:	0f a9                	pop    %gs
    popl %fs
  1029f1:	0f a1                	pop    %fs
    popl %es
  1029f3:	07                   	pop    %es
    popl %ds
  1029f4:	1f                   	pop    %ds

    # get rid of the trap number and error code
    addl $0x8, %esp
  1029f5:	83 c4 08             	add    $0x8,%esp
    iret
  1029f8:	cf                   	iret   

001029f9 <page2ppn>:

extern struct Page *pages;
extern size_t npage;

static inline ppn_t
page2ppn(struct Page *page) {
  1029f9:	55                   	push   %ebp
  1029fa:	89 e5                	mov    %esp,%ebp
    return page - pages;
  1029fc:	a1 18 cf 11 00       	mov    0x11cf18,%eax
  102a01:	8b 55 08             	mov    0x8(%ebp),%edx
  102a04:	29 c2                	sub    %eax,%edx
  102a06:	89 d0                	mov    %edx,%eax
  102a08:	c1 f8 02             	sar    $0x2,%eax
  102a0b:	69 c0 cd cc cc cc    	imul   $0xcccccccd,%eax,%eax
}
  102a11:	5d                   	pop    %ebp
  102a12:	c3                   	ret    

00102a13 <page2pa>:

static inline uintptr_t
page2pa(struct Page *page) {
  102a13:	55                   	push   %ebp
  102a14:	89 e5                	mov    %esp,%ebp
  102a16:	83 ec 04             	sub    $0x4,%esp
    return page2ppn(page) << PGSHIFT;
  102a19:	8b 45 08             	mov    0x8(%ebp),%eax
  102a1c:	89 04 24             	mov    %eax,(%esp)
  102a1f:	e8 d5 ff ff ff       	call   1029f9 <page2ppn>
  102a24:	c1 e0 0c             	shl    $0xc,%eax
}
  102a27:	c9                   	leave  
  102a28:	c3                   	ret    

00102a29 <pa2page>:

static inline struct Page *
pa2page(uintptr_t pa) {
  102a29:	55                   	push   %ebp
  102a2a:	89 e5                	mov    %esp,%ebp
  102a2c:	83 ec 18             	sub    $0x18,%esp
    if (PPN(pa) >= npage) {
  102a2f:	8b 45 08             	mov    0x8(%ebp),%eax
  102a32:	c1 e8 0c             	shr    $0xc,%eax
  102a35:	89 c2                	mov    %eax,%edx
  102a37:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  102a3c:	39 c2                	cmp    %eax,%edx
  102a3e:	72 1c                	jb     102a5c <pa2page+0x33>
        panic("pa2page called with invalid pa");
  102a40:	c7 44 24 08 70 69 10 	movl   $0x106970,0x8(%esp)
  102a47:	00 
  102a48:	c7 44 24 04 5a 00 00 	movl   $0x5a,0x4(%esp)
  102a4f:	00 
  102a50:	c7 04 24 8f 69 10 00 	movl   $0x10698f,(%esp)
  102a57:	e8 ca d9 ff ff       	call   100426 <__panic>
    }
    return &pages[PPN(pa)];
  102a5c:	8b 0d 18 cf 11 00    	mov    0x11cf18,%ecx
  102a62:	8b 45 08             	mov    0x8(%ebp),%eax
  102a65:	c1 e8 0c             	shr    $0xc,%eax
  102a68:	89 c2                	mov    %eax,%edx
  102a6a:	89 d0                	mov    %edx,%eax
  102a6c:	c1 e0 02             	shl    $0x2,%eax
  102a6f:	01 d0                	add    %edx,%eax
  102a71:	c1 e0 02             	shl    $0x2,%eax
  102a74:	01 c8                	add    %ecx,%eax
}
  102a76:	c9                   	leave  
  102a77:	c3                   	ret    

00102a78 <page2kva>:

static inline void *
page2kva(struct Page *page) {
  102a78:	55                   	push   %ebp
  102a79:	89 e5                	mov    %esp,%ebp
  102a7b:	83 ec 28             	sub    $0x28,%esp
    return KADDR(page2pa(page));
  102a7e:	8b 45 08             	mov    0x8(%ebp),%eax
  102a81:	89 04 24             	mov    %eax,(%esp)
  102a84:	e8 8a ff ff ff       	call   102a13 <page2pa>
  102a89:	89 45 f4             	mov    %eax,-0xc(%ebp)
  102a8c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  102a8f:	c1 e8 0c             	shr    $0xc,%eax
  102a92:	89 45 f0             	mov    %eax,-0x10(%ebp)
  102a95:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  102a9a:	39 45 f0             	cmp    %eax,-0x10(%ebp)
  102a9d:	72 23                	jb     102ac2 <page2kva+0x4a>
  102a9f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  102aa2:	89 44 24 0c          	mov    %eax,0xc(%esp)
  102aa6:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  102aad:	00 
  102aae:	c7 44 24 04 61 00 00 	movl   $0x61,0x4(%esp)
  102ab5:	00 
  102ab6:	c7 04 24 8f 69 10 00 	movl   $0x10698f,(%esp)
  102abd:	e8 64 d9 ff ff       	call   100426 <__panic>
  102ac2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  102ac5:	2d 00 00 00 40       	sub    $0x40000000,%eax
}
  102aca:	c9                   	leave  
  102acb:	c3                   	ret    

00102acc <pte2page>:
kva2page(void *kva) {
    return pa2page(PADDR(kva));
}

static inline struct Page *
pte2page(pte_t pte) {
  102acc:	55                   	push   %ebp
  102acd:	89 e5                	mov    %esp,%ebp
  102acf:	83 ec 18             	sub    $0x18,%esp
    if (!(pte & PTE_P)) {
  102ad2:	8b 45 08             	mov    0x8(%ebp),%eax
  102ad5:	83 e0 01             	and    $0x1,%eax
  102ad8:	85 c0                	test   %eax,%eax
  102ada:	75 1c                	jne    102af8 <pte2page+0x2c>
        panic("pte2page called with invalid pte");
  102adc:	c7 44 24 08 c4 69 10 	movl   $0x1069c4,0x8(%esp)
  102ae3:	00 
  102ae4:	c7 44 24 04 6c 00 00 	movl   $0x6c,0x4(%esp)
  102aeb:	00 
  102aec:	c7 04 24 8f 69 10 00 	movl   $0x10698f,(%esp)
  102af3:	e8 2e d9 ff ff       	call   100426 <__panic>
    }
    return pa2page(PTE_ADDR(pte));
  102af8:	8b 45 08             	mov    0x8(%ebp),%eax
  102afb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  102b00:	89 04 24             	mov    %eax,(%esp)
  102b03:	e8 21 ff ff ff       	call   102a29 <pa2page>
}
  102b08:	c9                   	leave  
  102b09:	c3                   	ret    

00102b0a <pde2page>:

static inline struct Page *
pde2page(pde_t pde) {
  102b0a:	55                   	push   %ebp
  102b0b:	89 e5                	mov    %esp,%ebp
  102b0d:	83 ec 18             	sub    $0x18,%esp
    return pa2page(PDE_ADDR(pde));
  102b10:	8b 45 08             	mov    0x8(%ebp),%eax
  102b13:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  102b18:	89 04 24             	mov    %eax,(%esp)
  102b1b:	e8 09 ff ff ff       	call   102a29 <pa2page>
}
  102b20:	c9                   	leave  
  102b21:	c3                   	ret    

00102b22 <page_ref>:

static inline int
page_ref(struct Page *page) {
  102b22:	55                   	push   %ebp
  102b23:	89 e5                	mov    %esp,%ebp
    return page->ref;
  102b25:	8b 45 08             	mov    0x8(%ebp),%eax
  102b28:	8b 00                	mov    (%eax),%eax
}
  102b2a:	5d                   	pop    %ebp
  102b2b:	c3                   	ret    

00102b2c <set_page_ref>:

static inline void
set_page_ref(struct Page *page, int val) {
  102b2c:	55                   	push   %ebp
  102b2d:	89 e5                	mov    %esp,%ebp
    page->ref = val;
  102b2f:	8b 45 08             	mov    0x8(%ebp),%eax
  102b32:	8b 55 0c             	mov    0xc(%ebp),%edx
  102b35:	89 10                	mov    %edx,(%eax)
}
  102b37:	90                   	nop
  102b38:	5d                   	pop    %ebp
  102b39:	c3                   	ret    

00102b3a <page_ref_inc>:

static inline int
page_ref_inc(struct Page *page) {
  102b3a:	55                   	push   %ebp
  102b3b:	89 e5                	mov    %esp,%ebp
    page->ref += 1;
  102b3d:	8b 45 08             	mov    0x8(%ebp),%eax
  102b40:	8b 00                	mov    (%eax),%eax
  102b42:	8d 50 01             	lea    0x1(%eax),%edx
  102b45:	8b 45 08             	mov    0x8(%ebp),%eax
  102b48:	89 10                	mov    %edx,(%eax)
    return page->ref;
  102b4a:	8b 45 08             	mov    0x8(%ebp),%eax
  102b4d:	8b 00                	mov    (%eax),%eax
}
  102b4f:	5d                   	pop    %ebp
  102b50:	c3                   	ret    

00102b51 <page_ref_dec>:

static inline int
page_ref_dec(struct Page *page) {
  102b51:	55                   	push   %ebp
  102b52:	89 e5                	mov    %esp,%ebp
    page->ref -= 1;
  102b54:	8b 45 08             	mov    0x8(%ebp),%eax
  102b57:	8b 00                	mov    (%eax),%eax
  102b59:	8d 50 ff             	lea    -0x1(%eax),%edx
  102b5c:	8b 45 08             	mov    0x8(%ebp),%eax
  102b5f:	89 10                	mov    %edx,(%eax)
    return page->ref;
  102b61:	8b 45 08             	mov    0x8(%ebp),%eax
  102b64:	8b 00                	mov    (%eax),%eax
}
  102b66:	5d                   	pop    %ebp
  102b67:	c3                   	ret    

00102b68 <__intr_save>:
__intr_save(void) {
  102b68:	55                   	push   %ebp
  102b69:	89 e5                	mov    %esp,%ebp
  102b6b:	83 ec 18             	sub    $0x18,%esp
    asm volatile ("pushfl; popl %0" : "=r" (eflags));
  102b6e:	9c                   	pushf  
  102b6f:	58                   	pop    %eax
  102b70:	89 45 f4             	mov    %eax,-0xc(%ebp)
    return eflags;
  102b73:	8b 45 f4             	mov    -0xc(%ebp),%eax
    if (read_eflags() & FL_IF) {
  102b76:	25 00 02 00 00       	and    $0x200,%eax
  102b7b:	85 c0                	test   %eax,%eax
  102b7d:	74 0c                	je     102b8b <__intr_save+0x23>
        intr_disable();
  102b7f:	e8 05 ee ff ff       	call   101989 <intr_disable>
        return 1;
  102b84:	b8 01 00 00 00       	mov    $0x1,%eax
  102b89:	eb 05                	jmp    102b90 <__intr_save+0x28>
    return 0;
  102b8b:	b8 00 00 00 00       	mov    $0x0,%eax
}
  102b90:	c9                   	leave  
  102b91:	c3                   	ret    

00102b92 <__intr_restore>:
__intr_restore(bool flag) {
  102b92:	55                   	push   %ebp
  102b93:	89 e5                	mov    %esp,%ebp
  102b95:	83 ec 08             	sub    $0x8,%esp
    if (flag) {
  102b98:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  102b9c:	74 05                	je     102ba3 <__intr_restore+0x11>
        intr_enable();
  102b9e:	e8 da ed ff ff       	call   10197d <intr_enable>
}
  102ba3:	90                   	nop
  102ba4:	c9                   	leave  
  102ba5:	c3                   	ret    

00102ba6 <lgdt>:
/* *
 * lgdt - load the global descriptor table register and reset the
 * data/code segement registers for kernel.
 * */
static inline void
lgdt(struct pseudodesc *pd) {
  102ba6:	55                   	push   %ebp
  102ba7:	89 e5                	mov    %esp,%ebp
    asm volatile ("lgdt (%0)" :: "r" (pd));
  102ba9:	8b 45 08             	mov    0x8(%ebp),%eax
  102bac:	0f 01 10             	lgdtl  (%eax)
    asm volatile ("movw %%ax, %%gs" :: "a" (USER_DS));
  102baf:	b8 23 00 00 00       	mov    $0x23,%eax
  102bb4:	8e e8                	mov    %eax,%gs
    asm volatile ("movw %%ax, %%fs" :: "a" (USER_DS));
  102bb6:	b8 23 00 00 00       	mov    $0x23,%eax
  102bbb:	8e e0                	mov    %eax,%fs
    asm volatile ("movw %%ax, %%es" :: "a" (KERNEL_DS));
  102bbd:	b8 10 00 00 00       	mov    $0x10,%eax
  102bc2:	8e c0                	mov    %eax,%es
    asm volatile ("movw %%ax, %%ds" :: "a" (KERNEL_DS));
  102bc4:	b8 10 00 00 00       	mov    $0x10,%eax
  102bc9:	8e d8                	mov    %eax,%ds
    asm volatile ("movw %%ax, %%ss" :: "a" (KERNEL_DS));
  102bcb:	b8 10 00 00 00       	mov    $0x10,%eax
  102bd0:	8e d0                	mov    %eax,%ss
    // reload cs
    asm volatile ("ljmp %0, $1f\n 1:\n" :: "i" (KERNEL_CS));
  102bd2:	ea d9 2b 10 00 08 00 	ljmp   $0x8,$0x102bd9
}
  102bd9:	90                   	nop
  102bda:	5d                   	pop    %ebp
  102bdb:	c3                   	ret    

00102bdc <load_esp0>:
 * load_esp0 - change the ESP0 in default task state segment,
 * so that we can use different kernel stack when we trap frame
 * user to kernel.
 * */
void
load_esp0(uintptr_t esp0) {
  102bdc:	f3 0f 1e fb          	endbr32 
  102be0:	55                   	push   %ebp
  102be1:	89 e5                	mov    %esp,%ebp
    ts.ts_esp0 = esp0;
  102be3:	8b 45 08             	mov    0x8(%ebp),%eax
  102be6:	a3 a4 ce 11 00       	mov    %eax,0x11cea4
}
  102beb:	90                   	nop
  102bec:	5d                   	pop    %ebp
  102bed:	c3                   	ret    

00102bee <gdt_init>:

/* gdt_init - initialize the default GDT and TSS */
static void
gdt_init(void) {
  102bee:	f3 0f 1e fb          	endbr32 
  102bf2:	55                   	push   %ebp
  102bf3:	89 e5                	mov    %esp,%ebp
  102bf5:	83 ec 14             	sub    $0x14,%esp
    // set boot kernel stack and default SS0
    load_esp0((uintptr_t)bootstacktop);
  102bf8:	b8 00 90 11 00       	mov    $0x119000,%eax
  102bfd:	89 04 24             	mov    %eax,(%esp)
  102c00:	e8 d7 ff ff ff       	call   102bdc <load_esp0>
    ts.ts_ss0 = KERNEL_DS;
  102c05:	66 c7 05 a8 ce 11 00 	movw   $0x10,0x11cea8
  102c0c:	10 00 

    // initialize the TSS filed of the gdt
    gdt[SEG_TSS] = SEGTSS(STS_T32A, (uintptr_t)&ts, sizeof(ts), DPL_KERNEL);
  102c0e:	66 c7 05 28 9a 11 00 	movw   $0x68,0x119a28
  102c15:	68 00 
  102c17:	b8 a0 ce 11 00       	mov    $0x11cea0,%eax
  102c1c:	0f b7 c0             	movzwl %ax,%eax
  102c1f:	66 a3 2a 9a 11 00    	mov    %ax,0x119a2a
  102c25:	b8 a0 ce 11 00       	mov    $0x11cea0,%eax
  102c2a:	c1 e8 10             	shr    $0x10,%eax
  102c2d:	a2 2c 9a 11 00       	mov    %al,0x119a2c
  102c32:	0f b6 05 2d 9a 11 00 	movzbl 0x119a2d,%eax
  102c39:	24 f0                	and    $0xf0,%al
  102c3b:	0c 09                	or     $0x9,%al
  102c3d:	a2 2d 9a 11 00       	mov    %al,0x119a2d
  102c42:	0f b6 05 2d 9a 11 00 	movzbl 0x119a2d,%eax
  102c49:	24 ef                	and    $0xef,%al
  102c4b:	a2 2d 9a 11 00       	mov    %al,0x119a2d
  102c50:	0f b6 05 2d 9a 11 00 	movzbl 0x119a2d,%eax
  102c57:	24 9f                	and    $0x9f,%al
  102c59:	a2 2d 9a 11 00       	mov    %al,0x119a2d
  102c5e:	0f b6 05 2d 9a 11 00 	movzbl 0x119a2d,%eax
  102c65:	0c 80                	or     $0x80,%al
  102c67:	a2 2d 9a 11 00       	mov    %al,0x119a2d
  102c6c:	0f b6 05 2e 9a 11 00 	movzbl 0x119a2e,%eax
  102c73:	24 f0                	and    $0xf0,%al
  102c75:	a2 2e 9a 11 00       	mov    %al,0x119a2e
  102c7a:	0f b6 05 2e 9a 11 00 	movzbl 0x119a2e,%eax
  102c81:	24 ef                	and    $0xef,%al
  102c83:	a2 2e 9a 11 00       	mov    %al,0x119a2e
  102c88:	0f b6 05 2e 9a 11 00 	movzbl 0x119a2e,%eax
  102c8f:	24 df                	and    $0xdf,%al
  102c91:	a2 2e 9a 11 00       	mov    %al,0x119a2e
  102c96:	0f b6 05 2e 9a 11 00 	movzbl 0x119a2e,%eax
  102c9d:	0c 40                	or     $0x40,%al
  102c9f:	a2 2e 9a 11 00       	mov    %al,0x119a2e
  102ca4:	0f b6 05 2e 9a 11 00 	movzbl 0x119a2e,%eax
  102cab:	24 7f                	and    $0x7f,%al
  102cad:	a2 2e 9a 11 00       	mov    %al,0x119a2e
  102cb2:	b8 a0 ce 11 00       	mov    $0x11cea0,%eax
  102cb7:	c1 e8 18             	shr    $0x18,%eax
  102cba:	a2 2f 9a 11 00       	mov    %al,0x119a2f

    // reload all segment registers
    lgdt(&gdt_pd);
  102cbf:	c7 04 24 30 9a 11 00 	movl   $0x119a30,(%esp)
  102cc6:	e8 db fe ff ff       	call   102ba6 <lgdt>
  102ccb:	66 c7 45 fe 28 00    	movw   $0x28,-0x2(%ebp)
    asm volatile ("ltr %0" :: "r" (sel) : "memory");
  102cd1:	0f b7 45 fe          	movzwl -0x2(%ebp),%eax
  102cd5:	0f 00 d8             	ltr    %ax
}
  102cd8:	90                   	nop

    // load the TSS
    ltr(GD_TSS);
}
  102cd9:	90                   	nop
  102cda:	c9                   	leave  
  102cdb:	c3                   	ret    

00102cdc <init_pmm_manager>:

//init_pmm_manager - initialize a pmm_manager instance
static void
init_pmm_manager(void) {
  102cdc:	f3 0f 1e fb          	endbr32 
  102ce0:	55                   	push   %ebp
  102ce1:	89 e5                	mov    %esp,%ebp
  102ce3:	83 ec 18             	sub    $0x18,%esp
    pmm_manager = &default_pmm_manager;
  102ce6:	c7 05 10 cf 11 00 80 	movl   $0x107380,0x11cf10
  102ced:	73 10 00 
    cprintf("memory management: %s\n", pmm_manager->name);
  102cf0:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102cf5:	8b 00                	mov    (%eax),%eax
  102cf7:	89 44 24 04          	mov    %eax,0x4(%esp)
  102cfb:	c7 04 24 f0 69 10 00 	movl   $0x1069f0,(%esp)
  102d02:	e8 b3 d5 ff ff       	call   1002ba <cprintf>
    pmm_manager->init();
  102d07:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102d0c:	8b 40 04             	mov    0x4(%eax),%eax
  102d0f:	ff d0                	call   *%eax
}
  102d11:	90                   	nop
  102d12:	c9                   	leave  
  102d13:	c3                   	ret    

00102d14 <init_memmap>:

//init_memmap - call pmm->init_memmap to build Page struct for free memory  
static void
init_memmap(struct Page *base, size_t n) {
  102d14:	f3 0f 1e fb          	endbr32 
  102d18:	55                   	push   %ebp
  102d19:	89 e5                	mov    %esp,%ebp
  102d1b:	83 ec 18             	sub    $0x18,%esp
    pmm_manager->init_memmap(base, n);
  102d1e:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102d23:	8b 40 08             	mov    0x8(%eax),%eax
  102d26:	8b 55 0c             	mov    0xc(%ebp),%edx
  102d29:	89 54 24 04          	mov    %edx,0x4(%esp)
  102d2d:	8b 55 08             	mov    0x8(%ebp),%edx
  102d30:	89 14 24             	mov    %edx,(%esp)
  102d33:	ff d0                	call   *%eax
}
  102d35:	90                   	nop
  102d36:	c9                   	leave  
  102d37:	c3                   	ret    

00102d38 <alloc_pages>:

//alloc_pages - call pmm->alloc_pages to allocate a continuous n*PAGESIZE memory 
struct Page *
alloc_pages(size_t n) {
  102d38:	f3 0f 1e fb          	endbr32 
  102d3c:	55                   	push   %ebp
  102d3d:	89 e5                	mov    %esp,%ebp
  102d3f:	83 ec 28             	sub    $0x28,%esp
    struct Page *page=NULL;
  102d42:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    bool intr_flag;
    local_intr_save(intr_flag);
  102d49:	e8 1a fe ff ff       	call   102b68 <__intr_save>
  102d4e:	89 45 f0             	mov    %eax,-0x10(%ebp)
    {
        page = pmm_manager->alloc_pages(n);
  102d51:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102d56:	8b 40 0c             	mov    0xc(%eax),%eax
  102d59:	8b 55 08             	mov    0x8(%ebp),%edx
  102d5c:	89 14 24             	mov    %edx,(%esp)
  102d5f:	ff d0                	call   *%eax
  102d61:	89 45 f4             	mov    %eax,-0xc(%ebp)
    }
    local_intr_restore(intr_flag);
  102d64:	8b 45 f0             	mov    -0x10(%ebp),%eax
  102d67:	89 04 24             	mov    %eax,(%esp)
  102d6a:	e8 23 fe ff ff       	call   102b92 <__intr_restore>
    return page;
  102d6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  102d72:	c9                   	leave  
  102d73:	c3                   	ret    

00102d74 <free_pages>:

//free_pages - call pmm->free_pages to free a continuous n*PAGESIZE memory 
void
free_pages(struct Page *base, size_t n) {
  102d74:	f3 0f 1e fb          	endbr32 
  102d78:	55                   	push   %ebp
  102d79:	89 e5                	mov    %esp,%ebp
  102d7b:	83 ec 28             	sub    $0x28,%esp
    bool intr_flag;
    local_intr_save(intr_flag);
  102d7e:	e8 e5 fd ff ff       	call   102b68 <__intr_save>
  102d83:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        pmm_manager->free_pages(base, n);
  102d86:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102d8b:	8b 40 10             	mov    0x10(%eax),%eax
  102d8e:	8b 55 0c             	mov    0xc(%ebp),%edx
  102d91:	89 54 24 04          	mov    %edx,0x4(%esp)
  102d95:	8b 55 08             	mov    0x8(%ebp),%edx
  102d98:	89 14 24             	mov    %edx,(%esp)
  102d9b:	ff d0                	call   *%eax
    }
    local_intr_restore(intr_flag);
  102d9d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  102da0:	89 04 24             	mov    %eax,(%esp)
  102da3:	e8 ea fd ff ff       	call   102b92 <__intr_restore>
}
  102da8:	90                   	nop
  102da9:	c9                   	leave  
  102daa:	c3                   	ret    

00102dab <nr_free_pages>:

//nr_free_pages - call pmm->nr_free_pages to get the size (nr*PAGESIZE) 
//of current free memory
size_t
nr_free_pages(void) {
  102dab:	f3 0f 1e fb          	endbr32 
  102daf:	55                   	push   %ebp
  102db0:	89 e5                	mov    %esp,%ebp
  102db2:	83 ec 28             	sub    $0x28,%esp
    size_t ret;
    bool intr_flag;
    local_intr_save(intr_flag);
  102db5:	e8 ae fd ff ff       	call   102b68 <__intr_save>
  102dba:	89 45 f4             	mov    %eax,-0xc(%ebp)
    {
        ret = pmm_manager->nr_free_pages();
  102dbd:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  102dc2:	8b 40 14             	mov    0x14(%eax),%eax
  102dc5:	ff d0                	call   *%eax
  102dc7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    }
    local_intr_restore(intr_flag);
  102dca:	8b 45 f4             	mov    -0xc(%ebp),%eax
  102dcd:	89 04 24             	mov    %eax,(%esp)
  102dd0:	e8 bd fd ff ff       	call   102b92 <__intr_restore>
    return ret;
  102dd5:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
  102dd8:	c9                   	leave  
  102dd9:	c3                   	ret    

00102dda <page_init>:

/* pmm_init - initialize the physical memory management */
static void
page_init(void) {
  102dda:	f3 0f 1e fb          	endbr32 
  102dde:	55                   	push   %ebp
  102ddf:	89 e5                	mov    %esp,%ebp
  102de1:	57                   	push   %edi
  102de2:	56                   	push   %esi
  102de3:	53                   	push   %ebx
  102de4:	81 ec 9c 00 00 00    	sub    $0x9c,%esp
    struct e820map *memmap = (struct e820map *)(0x8000 + KERNBASE);
  102dea:	c7 45 c4 00 80 00 c0 	movl   $0xc0008000,-0x3c(%ebp)
    uint64_t maxpa = 0;
  102df1:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
  102df8:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)

    cprintf("e820map:\n");
  102dff:	c7 04 24 07 6a 10 00 	movl   $0x106a07,(%esp)
  102e06:	e8 af d4 ff ff       	call   1002ba <cprintf>
    int i;
    for (i = 0; i < memmap->nr_map; i ++) {
  102e0b:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
  102e12:	e9 1a 01 00 00       	jmp    102f31 <page_init+0x157>
        uint64_t begin = memmap->map[i].addr, end = begin + memmap->map[i].size;
  102e17:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  102e1a:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102e1d:	89 d0                	mov    %edx,%eax
  102e1f:	c1 e0 02             	shl    $0x2,%eax
  102e22:	01 d0                	add    %edx,%eax
  102e24:	c1 e0 02             	shl    $0x2,%eax
  102e27:	01 c8                	add    %ecx,%eax
  102e29:	8b 50 08             	mov    0x8(%eax),%edx
  102e2c:	8b 40 04             	mov    0x4(%eax),%eax
  102e2f:	89 45 a0             	mov    %eax,-0x60(%ebp)
  102e32:	89 55 a4             	mov    %edx,-0x5c(%ebp)
  102e35:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  102e38:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102e3b:	89 d0                	mov    %edx,%eax
  102e3d:	c1 e0 02             	shl    $0x2,%eax
  102e40:	01 d0                	add    %edx,%eax
  102e42:	c1 e0 02             	shl    $0x2,%eax
  102e45:	01 c8                	add    %ecx,%eax
  102e47:	8b 48 0c             	mov    0xc(%eax),%ecx
  102e4a:	8b 58 10             	mov    0x10(%eax),%ebx
  102e4d:	8b 45 a0             	mov    -0x60(%ebp),%eax
  102e50:	8b 55 a4             	mov    -0x5c(%ebp),%edx
  102e53:	01 c8                	add    %ecx,%eax
  102e55:	11 da                	adc    %ebx,%edx
  102e57:	89 45 98             	mov    %eax,-0x68(%ebp)
  102e5a:	89 55 9c             	mov    %edx,-0x64(%ebp)
        cprintf("  memory: %08llx, [%08llx, %08llx], type = %d.\n",
  102e5d:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  102e60:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102e63:	89 d0                	mov    %edx,%eax
  102e65:	c1 e0 02             	shl    $0x2,%eax
  102e68:	01 d0                	add    %edx,%eax
  102e6a:	c1 e0 02             	shl    $0x2,%eax
  102e6d:	01 c8                	add    %ecx,%eax
  102e6f:	83 c0 14             	add    $0x14,%eax
  102e72:	8b 00                	mov    (%eax),%eax
  102e74:	89 45 84             	mov    %eax,-0x7c(%ebp)
  102e77:	8b 45 98             	mov    -0x68(%ebp),%eax
  102e7a:	8b 55 9c             	mov    -0x64(%ebp),%edx
  102e7d:	83 c0 ff             	add    $0xffffffff,%eax
  102e80:	83 d2 ff             	adc    $0xffffffff,%edx
  102e83:	89 85 78 ff ff ff    	mov    %eax,-0x88(%ebp)
  102e89:	89 95 7c ff ff ff    	mov    %edx,-0x84(%ebp)
  102e8f:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  102e92:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102e95:	89 d0                	mov    %edx,%eax
  102e97:	c1 e0 02             	shl    $0x2,%eax
  102e9a:	01 d0                	add    %edx,%eax
  102e9c:	c1 e0 02             	shl    $0x2,%eax
  102e9f:	01 c8                	add    %ecx,%eax
  102ea1:	8b 48 0c             	mov    0xc(%eax),%ecx
  102ea4:	8b 58 10             	mov    0x10(%eax),%ebx
  102ea7:	8b 55 84             	mov    -0x7c(%ebp),%edx
  102eaa:	89 54 24 1c          	mov    %edx,0x1c(%esp)
  102eae:	8b 85 78 ff ff ff    	mov    -0x88(%ebp),%eax
  102eb4:	8b 95 7c ff ff ff    	mov    -0x84(%ebp),%edx
  102eba:	89 44 24 14          	mov    %eax,0x14(%esp)
  102ebe:	89 54 24 18          	mov    %edx,0x18(%esp)
  102ec2:	8b 45 a0             	mov    -0x60(%ebp),%eax
  102ec5:	8b 55 a4             	mov    -0x5c(%ebp),%edx
  102ec8:	89 44 24 0c          	mov    %eax,0xc(%esp)
  102ecc:	89 54 24 10          	mov    %edx,0x10(%esp)
  102ed0:	89 4c 24 04          	mov    %ecx,0x4(%esp)
  102ed4:	89 5c 24 08          	mov    %ebx,0x8(%esp)
  102ed8:	c7 04 24 14 6a 10 00 	movl   $0x106a14,(%esp)
  102edf:	e8 d6 d3 ff ff       	call   1002ba <cprintf>
                memmap->map[i].size, begin, end - 1, memmap->map[i].type);
        if (memmap->map[i].type == E820_ARM) {
  102ee4:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  102ee7:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102eea:	89 d0                	mov    %edx,%eax
  102eec:	c1 e0 02             	shl    $0x2,%eax
  102eef:	01 d0                	add    %edx,%eax
  102ef1:	c1 e0 02             	shl    $0x2,%eax
  102ef4:	01 c8                	add    %ecx,%eax
  102ef6:	83 c0 14             	add    $0x14,%eax
  102ef9:	8b 00                	mov    (%eax),%eax
  102efb:	83 f8 01             	cmp    $0x1,%eax
  102efe:	75 2e                	jne    102f2e <page_init+0x154>
            if (maxpa < end && begin < KMEMSIZE) {
  102f00:	8b 45 e0             	mov    -0x20(%ebp),%eax
  102f03:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  102f06:	3b 45 98             	cmp    -0x68(%ebp),%eax
  102f09:	89 d0                	mov    %edx,%eax
  102f0b:	1b 45 9c             	sbb    -0x64(%ebp),%eax
  102f0e:	73 1e                	jae    102f2e <page_init+0x154>
  102f10:	ba ff ff ff 37       	mov    $0x37ffffff,%edx
  102f15:	b8 00 00 00 00       	mov    $0x0,%eax
  102f1a:	3b 55 a0             	cmp    -0x60(%ebp),%edx
  102f1d:	1b 45 a4             	sbb    -0x5c(%ebp),%eax
  102f20:	72 0c                	jb     102f2e <page_init+0x154>
                maxpa = end;
  102f22:	8b 45 98             	mov    -0x68(%ebp),%eax
  102f25:	8b 55 9c             	mov    -0x64(%ebp),%edx
  102f28:	89 45 e0             	mov    %eax,-0x20(%ebp)
  102f2b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
    for (i = 0; i < memmap->nr_map; i ++) {
  102f2e:	ff 45 dc             	incl   -0x24(%ebp)
  102f31:	8b 45 c4             	mov    -0x3c(%ebp),%eax
  102f34:	8b 00                	mov    (%eax),%eax
  102f36:	39 45 dc             	cmp    %eax,-0x24(%ebp)
  102f39:	0f 8c d8 fe ff ff    	jl     102e17 <page_init+0x3d>
            }
        }
    }
    if (maxpa > KMEMSIZE) {
  102f3f:	ba 00 00 00 38       	mov    $0x38000000,%edx
  102f44:	b8 00 00 00 00       	mov    $0x0,%eax
  102f49:	3b 55 e0             	cmp    -0x20(%ebp),%edx
  102f4c:	1b 45 e4             	sbb    -0x1c(%ebp),%eax
  102f4f:	73 0e                	jae    102f5f <page_init+0x185>
        maxpa = KMEMSIZE;
  102f51:	c7 45 e0 00 00 00 38 	movl   $0x38000000,-0x20(%ebp)
  102f58:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
    }

    extern char end[];

    npage = maxpa / PGSIZE;
  102f5f:	8b 45 e0             	mov    -0x20(%ebp),%eax
  102f62:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  102f65:	0f ac d0 0c          	shrd   $0xc,%edx,%eax
  102f69:	c1 ea 0c             	shr    $0xc,%edx
  102f6c:	a3 80 ce 11 00       	mov    %eax,0x11ce80
    pages = (struct Page *)ROUNDUP((void *)end, PGSIZE);
  102f71:	c7 45 c0 00 10 00 00 	movl   $0x1000,-0x40(%ebp)
  102f78:	b8 28 cf 11 00       	mov    $0x11cf28,%eax
  102f7d:	8d 50 ff             	lea    -0x1(%eax),%edx
  102f80:	8b 45 c0             	mov    -0x40(%ebp),%eax
  102f83:	01 d0                	add    %edx,%eax
  102f85:	89 45 bc             	mov    %eax,-0x44(%ebp)
  102f88:	8b 45 bc             	mov    -0x44(%ebp),%eax
  102f8b:	ba 00 00 00 00       	mov    $0x0,%edx
  102f90:	f7 75 c0             	divl   -0x40(%ebp)
  102f93:	8b 45 bc             	mov    -0x44(%ebp),%eax
  102f96:	29 d0                	sub    %edx,%eax
  102f98:	a3 18 cf 11 00       	mov    %eax,0x11cf18

    for (i = 0; i < npage; i ++) {
  102f9d:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
  102fa4:	eb 2f                	jmp    102fd5 <page_init+0x1fb>
        SetPageReserved(pages + i);
  102fa6:	8b 0d 18 cf 11 00    	mov    0x11cf18,%ecx
  102fac:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102faf:	89 d0                	mov    %edx,%eax
  102fb1:	c1 e0 02             	shl    $0x2,%eax
  102fb4:	01 d0                	add    %edx,%eax
  102fb6:	c1 e0 02             	shl    $0x2,%eax
  102fb9:	01 c8                	add    %ecx,%eax
  102fbb:	83 c0 04             	add    $0x4,%eax
  102fbe:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
  102fc5:	89 45 90             	mov    %eax,-0x70(%ebp)
 * Note that @nr may be almost arbitrarily large; this function is not
 * restricted to acting on a single-word quantity.
 * */
static inline void
set_bit(int nr, volatile void *addr) {
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  102fc8:	8b 45 90             	mov    -0x70(%ebp),%eax
  102fcb:	8b 55 94             	mov    -0x6c(%ebp),%edx
  102fce:	0f ab 10             	bts    %edx,(%eax)
}
  102fd1:	90                   	nop
    for (i = 0; i < npage; i ++) {
  102fd2:	ff 45 dc             	incl   -0x24(%ebp)
  102fd5:	8b 55 dc             	mov    -0x24(%ebp),%edx
  102fd8:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  102fdd:	39 c2                	cmp    %eax,%edx
  102fdf:	72 c5                	jb     102fa6 <page_init+0x1cc>
    }

    uintptr_t freemem = PADDR((uintptr_t)pages + sizeof(struct Page) * npage);
  102fe1:	8b 15 80 ce 11 00    	mov    0x11ce80,%edx
  102fe7:	89 d0                	mov    %edx,%eax
  102fe9:	c1 e0 02             	shl    $0x2,%eax
  102fec:	01 d0                	add    %edx,%eax
  102fee:	c1 e0 02             	shl    $0x2,%eax
  102ff1:	89 c2                	mov    %eax,%edx
  102ff3:	a1 18 cf 11 00       	mov    0x11cf18,%eax
  102ff8:	01 d0                	add    %edx,%eax
  102ffa:	89 45 b8             	mov    %eax,-0x48(%ebp)
  102ffd:	81 7d b8 ff ff ff bf 	cmpl   $0xbfffffff,-0x48(%ebp)
  103004:	77 23                	ja     103029 <page_init+0x24f>
  103006:	8b 45 b8             	mov    -0x48(%ebp),%eax
  103009:	89 44 24 0c          	mov    %eax,0xc(%esp)
  10300d:	c7 44 24 08 44 6a 10 	movl   $0x106a44,0x8(%esp)
  103014:	00 
  103015:	c7 44 24 04 dc 00 00 	movl   $0xdc,0x4(%esp)
  10301c:	00 
  10301d:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103024:	e8 fd d3 ff ff       	call   100426 <__panic>
  103029:	8b 45 b8             	mov    -0x48(%ebp),%eax
  10302c:	05 00 00 00 40       	add    $0x40000000,%eax
  103031:	89 45 b4             	mov    %eax,-0x4c(%ebp)

    for (i = 0; i < memmap->nr_map; i ++) {
  103034:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
  10303b:	e9 4b 01 00 00       	jmp    10318b <page_init+0x3b1>
        uint64_t begin = memmap->map[i].addr, end = begin + memmap->map[i].size;
  103040:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  103043:	8b 55 dc             	mov    -0x24(%ebp),%edx
  103046:	89 d0                	mov    %edx,%eax
  103048:	c1 e0 02             	shl    $0x2,%eax
  10304b:	01 d0                	add    %edx,%eax
  10304d:	c1 e0 02             	shl    $0x2,%eax
  103050:	01 c8                	add    %ecx,%eax
  103052:	8b 50 08             	mov    0x8(%eax),%edx
  103055:	8b 40 04             	mov    0x4(%eax),%eax
  103058:	89 45 d0             	mov    %eax,-0x30(%ebp)
  10305b:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  10305e:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  103061:	8b 55 dc             	mov    -0x24(%ebp),%edx
  103064:	89 d0                	mov    %edx,%eax
  103066:	c1 e0 02             	shl    $0x2,%eax
  103069:	01 d0                	add    %edx,%eax
  10306b:	c1 e0 02             	shl    $0x2,%eax
  10306e:	01 c8                	add    %ecx,%eax
  103070:	8b 48 0c             	mov    0xc(%eax),%ecx
  103073:	8b 58 10             	mov    0x10(%eax),%ebx
  103076:	8b 45 d0             	mov    -0x30(%ebp),%eax
  103079:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10307c:	01 c8                	add    %ecx,%eax
  10307e:	11 da                	adc    %ebx,%edx
  103080:	89 45 c8             	mov    %eax,-0x38(%ebp)
  103083:	89 55 cc             	mov    %edx,-0x34(%ebp)
        if (memmap->map[i].type == E820_ARM) {
  103086:	8b 4d c4             	mov    -0x3c(%ebp),%ecx
  103089:	8b 55 dc             	mov    -0x24(%ebp),%edx
  10308c:	89 d0                	mov    %edx,%eax
  10308e:	c1 e0 02             	shl    $0x2,%eax
  103091:	01 d0                	add    %edx,%eax
  103093:	c1 e0 02             	shl    $0x2,%eax
  103096:	01 c8                	add    %ecx,%eax
  103098:	83 c0 14             	add    $0x14,%eax
  10309b:	8b 00                	mov    (%eax),%eax
  10309d:	83 f8 01             	cmp    $0x1,%eax
  1030a0:	0f 85 e2 00 00 00    	jne    103188 <page_init+0x3ae>
            if (begin < freemem) {
  1030a6:	8b 45 b4             	mov    -0x4c(%ebp),%eax
  1030a9:	ba 00 00 00 00       	mov    $0x0,%edx
  1030ae:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
  1030b1:	39 45 d0             	cmp    %eax,-0x30(%ebp)
  1030b4:	19 d1                	sbb    %edx,%ecx
  1030b6:	73 0d                	jae    1030c5 <page_init+0x2eb>
                begin = freemem;
  1030b8:	8b 45 b4             	mov    -0x4c(%ebp),%eax
  1030bb:	89 45 d0             	mov    %eax,-0x30(%ebp)
  1030be:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
            }
            if (end > KMEMSIZE) {
  1030c5:	ba 00 00 00 38       	mov    $0x38000000,%edx
  1030ca:	b8 00 00 00 00       	mov    $0x0,%eax
  1030cf:	3b 55 c8             	cmp    -0x38(%ebp),%edx
  1030d2:	1b 45 cc             	sbb    -0x34(%ebp),%eax
  1030d5:	73 0e                	jae    1030e5 <page_init+0x30b>
                end = KMEMSIZE;
  1030d7:	c7 45 c8 00 00 00 38 	movl   $0x38000000,-0x38(%ebp)
  1030de:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%ebp)
            }
            if (begin < end) {
  1030e5:	8b 45 d0             	mov    -0x30(%ebp),%eax
  1030e8:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  1030eb:	3b 45 c8             	cmp    -0x38(%ebp),%eax
  1030ee:	89 d0                	mov    %edx,%eax
  1030f0:	1b 45 cc             	sbb    -0x34(%ebp),%eax
  1030f3:	0f 83 8f 00 00 00    	jae    103188 <page_init+0x3ae>
                begin = ROUNDUP(begin, PGSIZE);
  1030f9:	c7 45 b0 00 10 00 00 	movl   $0x1000,-0x50(%ebp)
  103100:	8b 55 d0             	mov    -0x30(%ebp),%edx
  103103:	8b 45 b0             	mov    -0x50(%ebp),%eax
  103106:	01 d0                	add    %edx,%eax
  103108:	48                   	dec    %eax
  103109:	89 45 ac             	mov    %eax,-0x54(%ebp)
  10310c:	8b 45 ac             	mov    -0x54(%ebp),%eax
  10310f:	ba 00 00 00 00       	mov    $0x0,%edx
  103114:	f7 75 b0             	divl   -0x50(%ebp)
  103117:	8b 45 ac             	mov    -0x54(%ebp),%eax
  10311a:	29 d0                	sub    %edx,%eax
  10311c:	ba 00 00 00 00       	mov    $0x0,%edx
  103121:	89 45 d0             	mov    %eax,-0x30(%ebp)
  103124:	89 55 d4             	mov    %edx,-0x2c(%ebp)
                end = ROUNDDOWN(end, PGSIZE);
  103127:	8b 45 c8             	mov    -0x38(%ebp),%eax
  10312a:	89 45 a8             	mov    %eax,-0x58(%ebp)
  10312d:	8b 45 a8             	mov    -0x58(%ebp),%eax
  103130:	ba 00 00 00 00       	mov    $0x0,%edx
  103135:	89 c3                	mov    %eax,%ebx
  103137:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  10313d:	89 de                	mov    %ebx,%esi
  10313f:	89 d0                	mov    %edx,%eax
  103141:	83 e0 00             	and    $0x0,%eax
  103144:	89 c7                	mov    %eax,%edi
  103146:	89 75 c8             	mov    %esi,-0x38(%ebp)
  103149:	89 7d cc             	mov    %edi,-0x34(%ebp)
                if (begin < end) {
  10314c:	8b 45 d0             	mov    -0x30(%ebp),%eax
  10314f:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  103152:	3b 45 c8             	cmp    -0x38(%ebp),%eax
  103155:	89 d0                	mov    %edx,%eax
  103157:	1b 45 cc             	sbb    -0x34(%ebp),%eax
  10315a:	73 2c                	jae    103188 <page_init+0x3ae>
                    init_memmap(pa2page(begin), (end - begin) / PGSIZE);
  10315c:	8b 45 c8             	mov    -0x38(%ebp),%eax
  10315f:	8b 55 cc             	mov    -0x34(%ebp),%edx
  103162:	2b 45 d0             	sub    -0x30(%ebp),%eax
  103165:	1b 55 d4             	sbb    -0x2c(%ebp),%edx
  103168:	0f ac d0 0c          	shrd   $0xc,%edx,%eax
  10316c:	c1 ea 0c             	shr    $0xc,%edx
  10316f:	89 c3                	mov    %eax,%ebx
  103171:	8b 45 d0             	mov    -0x30(%ebp),%eax
  103174:	89 04 24             	mov    %eax,(%esp)
  103177:	e8 ad f8 ff ff       	call   102a29 <pa2page>
  10317c:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  103180:	89 04 24             	mov    %eax,(%esp)
  103183:	e8 8c fb ff ff       	call   102d14 <init_memmap>
    for (i = 0; i < memmap->nr_map; i ++) {
  103188:	ff 45 dc             	incl   -0x24(%ebp)
  10318b:	8b 45 c4             	mov    -0x3c(%ebp),%eax
  10318e:	8b 00                	mov    (%eax),%eax
  103190:	39 45 dc             	cmp    %eax,-0x24(%ebp)
  103193:	0f 8c a7 fe ff ff    	jl     103040 <page_init+0x266>
                }
            }
        }
    }
}
  103199:	90                   	nop
  10319a:	90                   	nop
  10319b:	81 c4 9c 00 00 00    	add    $0x9c,%esp
  1031a1:	5b                   	pop    %ebx
  1031a2:	5e                   	pop    %esi
  1031a3:	5f                   	pop    %edi
  1031a4:	5d                   	pop    %ebp
  1031a5:	c3                   	ret    

001031a6 <boot_map_segment>:
//  la:   linear address of this memory need to map (after x86 segment map)
//  size: memory size
//  pa:   physical address of this memory
//  perm: permission of this memory  
static void
boot_map_segment(pde_t *pgdir, uintptr_t la, size_t size, uintptr_t pa, uint32_t perm) {
  1031a6:	f3 0f 1e fb          	endbr32 
  1031aa:	55                   	push   %ebp
  1031ab:	89 e5                	mov    %esp,%ebp
  1031ad:	83 ec 38             	sub    $0x38,%esp
    assert(PGOFF(la) == PGOFF(pa));
  1031b0:	8b 45 0c             	mov    0xc(%ebp),%eax
  1031b3:	33 45 14             	xor    0x14(%ebp),%eax
  1031b6:	25 ff 0f 00 00       	and    $0xfff,%eax
  1031bb:	85 c0                	test   %eax,%eax
  1031bd:	74 24                	je     1031e3 <boot_map_segment+0x3d>
  1031bf:	c7 44 24 0c 76 6a 10 	movl   $0x106a76,0xc(%esp)
  1031c6:	00 
  1031c7:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  1031ce:	00 
  1031cf:	c7 44 24 04 fa 00 00 	movl   $0xfa,0x4(%esp)
  1031d6:	00 
  1031d7:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1031de:	e8 43 d2 ff ff       	call   100426 <__panic>
    size_t n = ROUNDUP(size + PGOFF(la), PGSIZE) / PGSIZE;
  1031e3:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
  1031ea:	8b 45 0c             	mov    0xc(%ebp),%eax
  1031ed:	25 ff 0f 00 00       	and    $0xfff,%eax
  1031f2:	89 c2                	mov    %eax,%edx
  1031f4:	8b 45 10             	mov    0x10(%ebp),%eax
  1031f7:	01 c2                	add    %eax,%edx
  1031f9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1031fc:	01 d0                	add    %edx,%eax
  1031fe:	48                   	dec    %eax
  1031ff:	89 45 ec             	mov    %eax,-0x14(%ebp)
  103202:	8b 45 ec             	mov    -0x14(%ebp),%eax
  103205:	ba 00 00 00 00       	mov    $0x0,%edx
  10320a:	f7 75 f0             	divl   -0x10(%ebp)
  10320d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  103210:	29 d0                	sub    %edx,%eax
  103212:	c1 e8 0c             	shr    $0xc,%eax
  103215:	89 45 f4             	mov    %eax,-0xc(%ebp)
    la = ROUNDDOWN(la, PGSIZE);
  103218:	8b 45 0c             	mov    0xc(%ebp),%eax
  10321b:	89 45 e8             	mov    %eax,-0x18(%ebp)
  10321e:	8b 45 e8             	mov    -0x18(%ebp),%eax
  103221:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  103226:	89 45 0c             	mov    %eax,0xc(%ebp)
    pa = ROUNDDOWN(pa, PGSIZE);
  103229:	8b 45 14             	mov    0x14(%ebp),%eax
  10322c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  10322f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103232:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  103237:	89 45 14             	mov    %eax,0x14(%ebp)
    for (; n > 0; n --, la += PGSIZE, pa += PGSIZE) {
  10323a:	eb 68                	jmp    1032a4 <boot_map_segment+0xfe>
        pte_t *ptep = get_pte(pgdir, la, 1);
  10323c:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
  103243:	00 
  103244:	8b 45 0c             	mov    0xc(%ebp),%eax
  103247:	89 44 24 04          	mov    %eax,0x4(%esp)
  10324b:	8b 45 08             	mov    0x8(%ebp),%eax
  10324e:	89 04 24             	mov    %eax,(%esp)
  103251:	e8 8a 01 00 00       	call   1033e0 <get_pte>
  103256:	89 45 e0             	mov    %eax,-0x20(%ebp)
        assert(ptep != NULL);
  103259:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  10325d:	75 24                	jne    103283 <boot_map_segment+0xdd>
  10325f:	c7 44 24 0c a2 6a 10 	movl   $0x106aa2,0xc(%esp)
  103266:	00 
  103267:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10326e:	00 
  10326f:	c7 44 24 04 00 01 00 	movl   $0x100,0x4(%esp)
  103276:	00 
  103277:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10327e:	e8 a3 d1 ff ff       	call   100426 <__panic>
        *ptep = pa | PTE_P | perm;
  103283:	8b 45 14             	mov    0x14(%ebp),%eax
  103286:	0b 45 18             	or     0x18(%ebp),%eax
  103289:	83 c8 01             	or     $0x1,%eax
  10328c:	89 c2                	mov    %eax,%edx
  10328e:	8b 45 e0             	mov    -0x20(%ebp),%eax
  103291:	89 10                	mov    %edx,(%eax)
    for (; n > 0; n --, la += PGSIZE, pa += PGSIZE) {
  103293:	ff 4d f4             	decl   -0xc(%ebp)
  103296:	81 45 0c 00 10 00 00 	addl   $0x1000,0xc(%ebp)
  10329d:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  1032a4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1032a8:	75 92                	jne    10323c <boot_map_segment+0x96>
    }
}
  1032aa:	90                   	nop
  1032ab:	90                   	nop
  1032ac:	c9                   	leave  
  1032ad:	c3                   	ret    

001032ae <boot_alloc_page>:

//boot_alloc_page - allocate one page using pmm->alloc_pages(1) 
// return value: the kernel virtual address of this allocated page
//note: this function is used to get the memory for PDT(Page Directory Table)&PT(Page Table)
static void *
boot_alloc_page(void) {
  1032ae:	f3 0f 1e fb          	endbr32 
  1032b2:	55                   	push   %ebp
  1032b3:	89 e5                	mov    %esp,%ebp
  1032b5:	83 ec 28             	sub    $0x28,%esp
    struct Page *p = alloc_page();
  1032b8:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  1032bf:	e8 74 fa ff ff       	call   102d38 <alloc_pages>
  1032c4:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (p == NULL) {
  1032c7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1032cb:	75 1c                	jne    1032e9 <boot_alloc_page+0x3b>
        panic("boot_alloc_page failed.\n");
  1032cd:	c7 44 24 08 af 6a 10 	movl   $0x106aaf,0x8(%esp)
  1032d4:	00 
  1032d5:	c7 44 24 04 0c 01 00 	movl   $0x10c,0x4(%esp)
  1032dc:	00 
  1032dd:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1032e4:	e8 3d d1 ff ff       	call   100426 <__panic>
    }
    return page2kva(p);
  1032e9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1032ec:	89 04 24             	mov    %eax,(%esp)
  1032ef:	e8 84 f7 ff ff       	call   102a78 <page2kva>
}
  1032f4:	c9                   	leave  
  1032f5:	c3                   	ret    

001032f6 <pmm_init>:

//pmm_init - setup a pmm to manage physical memory, build PDT&PT to setup paging mechanism 
//         - check the correctness of pmm & paging mechanism, print PDT&PT
void
pmm_init(void) {
  1032f6:	f3 0f 1e fb          	endbr32 
  1032fa:	55                   	push   %ebp
  1032fb:	89 e5                	mov    %esp,%ebp
  1032fd:	83 ec 38             	sub    $0x38,%esp
    // We've already enabled paging
    boot_cr3 = PADDR(boot_pgdir);
  103300:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103305:	89 45 f4             	mov    %eax,-0xc(%ebp)
  103308:	81 7d f4 ff ff ff bf 	cmpl   $0xbfffffff,-0xc(%ebp)
  10330f:	77 23                	ja     103334 <pmm_init+0x3e>
  103311:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103314:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103318:	c7 44 24 08 44 6a 10 	movl   $0x106a44,0x8(%esp)
  10331f:	00 
  103320:	c7 44 24 04 16 01 00 	movl   $0x116,0x4(%esp)
  103327:	00 
  103328:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10332f:	e8 f2 d0 ff ff       	call   100426 <__panic>
  103334:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103337:	05 00 00 00 40       	add    $0x40000000,%eax
  10333c:	a3 14 cf 11 00       	mov    %eax,0x11cf14
    //We need to alloc/free the physical memory (granularity is 4KB or other size). 
    //So a framework of physical memory manager (struct pmm_manager)is defined in pmm.h
    //First we should init a physical memory manager(pmm) based on the framework.
    //Then pmm can alloc/free the physical memory. 
    //Now the first_fit/best_fit/worst_fit/buddy_system pmm are available.
    init_pmm_manager();
  103341:	e8 96 f9 ff ff       	call   102cdc <init_pmm_manager>

    // detect physical memory space, reserve already used memory,
    // then use pmm->init_memmap to create free page list
    page_init();
  103346:	e8 8f fa ff ff       	call   102dda <page_init>

    //use pmm->check to verify the correctness of the alloc/free function in a pmm
    check_alloc_page();
  10334b:	e8 5b 04 00 00       	call   1037ab <check_alloc_page>

    check_pgdir();
  103350:	e8 79 04 00 00       	call   1037ce <check_pgdir>

    static_assert(KERNBASE % PTSIZE == 0 && KERNTOP % PTSIZE == 0);

    // recursively insert boot_pgdir in itself
    // to form a virtual page table at virtual address VPT
    boot_pgdir[PDX(VPT)] = PADDR(boot_pgdir) | PTE_P | PTE_W;
  103355:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  10335a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  10335d:	81 7d f0 ff ff ff bf 	cmpl   $0xbfffffff,-0x10(%ebp)
  103364:	77 23                	ja     103389 <pmm_init+0x93>
  103366:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103369:	89 44 24 0c          	mov    %eax,0xc(%esp)
  10336d:	c7 44 24 08 44 6a 10 	movl   $0x106a44,0x8(%esp)
  103374:	00 
  103375:	c7 44 24 04 2c 01 00 	movl   $0x12c,0x4(%esp)
  10337c:	00 
  10337d:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103384:	e8 9d d0 ff ff       	call   100426 <__panic>
  103389:	8b 45 f0             	mov    -0x10(%ebp),%eax
  10338c:	8d 90 00 00 00 40    	lea    0x40000000(%eax),%edx
  103392:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103397:	05 ac 0f 00 00       	add    $0xfac,%eax
  10339c:	83 ca 03             	or     $0x3,%edx
  10339f:	89 10                	mov    %edx,(%eax)

    // map all physical memory to linear memory with base linear addr KERNBASE
    // linear_addr KERNBASE ~ KERNBASE + KMEMSIZE = phy_addr 0 ~ KMEMSIZE
    boot_map_segment(boot_pgdir, KERNBASE, KMEMSIZE, 0, PTE_W);
  1033a1:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  1033a6:	c7 44 24 10 02 00 00 	movl   $0x2,0x10(%esp)
  1033ad:	00 
  1033ae:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  1033b5:	00 
  1033b6:	c7 44 24 08 00 00 00 	movl   $0x38000000,0x8(%esp)
  1033bd:	38 
  1033be:	c7 44 24 04 00 00 00 	movl   $0xc0000000,0x4(%esp)
  1033c5:	c0 
  1033c6:	89 04 24             	mov    %eax,(%esp)
  1033c9:	e8 d8 fd ff ff       	call   1031a6 <boot_map_segment>

    // Since we are using bootloader's GDT,
    // we should reload gdt (second time, the last time) to get user segments and the TSS
    // map virtual_addr 0 ~ 4G = linear_addr 0 ~ 4G
    // then set kernel stack (ss:esp) in TSS, setup TSS in gdt, load TSS
    gdt_init();
  1033ce:	e8 1b f8 ff ff       	call   102bee <gdt_init>

    //now the basic virtual memory map(see memalyout.h) is established.
    //check the correctness of the basic virtual memory map.
    check_boot_pgdir();
  1033d3:	e8 96 0a 00 00       	call   103e6e <check_boot_pgdir>

    print_pgdir();
  1033d8:	e8 1b 0f 00 00       	call   1042f8 <print_pgdir>

}
  1033dd:	90                   	nop
  1033de:	c9                   	leave  
  1033df:	c3                   	ret    

001033e0 <get_pte>:
//  la:     the linear address need to map
//  create: a logical value to decide if alloc a page for PT
// return vaule: the kernel virtual address of this pte

pte_t *
get_pte(pde_t *pgdir, uintptr_t la, bool create) {
  1033e0:	f3 0f 1e fb          	endbr32 
  1033e4:	55                   	push   %ebp
  1033e5:	89 e5                	mov    %esp,%ebp
  1033e7:	83 ec 48             	sub    $0x48,%esp
                          // (7) set page directory entry's permission
    }
    return NULL;          // (8) return page table entry
#endif
    //找PDE
    pde_t *pdep = pgdir + PDX(la);
  1033ea:	8b 45 0c             	mov    0xc(%ebp),%eax
  1033ed:	c1 e8 16             	shr    $0x16,%eax
  1033f0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  1033f7:	8b 45 08             	mov    0x8(%ebp),%eax
  1033fa:	01 d0                	add    %edx,%eax
  1033fc:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //如果存在的话，返回对应的PTE即可
    if (*pdep & PTE_P) {
  1033ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103402:	8b 00                	mov    (%eax),%eax
  103404:	83 e0 01             	and    $0x1,%eax
  103407:	85 c0                	test   %eax,%eax
  103409:	74 68                	je     103473 <get_pte+0x93>
        pte_t *ptep = (pte_t *)KADDR(*pdep & ~0x0fff) + PTX(la);
  10340b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10340e:	8b 00                	mov    (%eax),%eax
  103410:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  103415:	89 45 d4             	mov    %eax,-0x2c(%ebp)
  103418:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  10341b:	c1 e8 0c             	shr    $0xc,%eax
  10341e:	89 45 d0             	mov    %eax,-0x30(%ebp)
  103421:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  103426:	39 45 d0             	cmp    %eax,-0x30(%ebp)
  103429:	72 23                	jb     10344e <get_pte+0x6e>
  10342b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  10342e:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103432:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  103439:	00 
  10343a:	c7 44 24 04 6f 01 00 	movl   $0x16f,0x4(%esp)
  103441:	00 
  103442:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103449:	e8 d8 cf ff ff       	call   100426 <__panic>
  10344e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  103451:	2d 00 00 00 40       	sub    $0x40000000,%eax
  103456:	89 c2                	mov    %eax,%edx
  103458:	8b 45 0c             	mov    0xc(%ebp),%eax
  10345b:	c1 e8 0c             	shr    $0xc,%eax
  10345e:	25 ff 03 00 00       	and    $0x3ff,%eax
  103463:	c1 e0 02             	shl    $0x2,%eax
  103466:	01 d0                	add    %edx,%eax
  103468:	89 45 cc             	mov    %eax,-0x34(%ebp)
        return ptep;
  10346b:	8b 45 cc             	mov    -0x34(%ebp),%eax
  10346e:	e9 10 01 00 00       	jmp    103583 <get_pte+0x1a3>
    }
    //如果不存在的话分配给它page
    struct Page *page;
    if (!create || ((page = alloc_page()) == NULL))
  103473:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  103477:	74 15                	je     10348e <get_pte+0xae>
  103479:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  103480:	e8 b3 f8 ff ff       	call   102d38 <alloc_pages>
  103485:	89 45 f0             	mov    %eax,-0x10(%ebp)
  103488:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  10348c:	75 0a                	jne    103498 <get_pte+0xb8>
     {
        return NULL;
  10348e:	b8 00 00 00 00       	mov    $0x0,%eax
  103493:	e9 eb 00 00 00       	jmp    103583 <get_pte+0x1a3>
    }
    //设置page reference
    set_page_ref(page, 1);
  103498:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  10349f:	00 
  1034a0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1034a3:	89 04 24             	mov    %eax,(%esp)
  1034a6:	e8 81 f6 ff ff       	call   102b2c <set_page_ref>
    //得到page的线性地址
    uintptr_t pa = page2pa(page) & ~0x0fff;
  1034ab:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1034ae:	89 04 24             	mov    %eax,(%esp)
  1034b1:	e8 5d f5 ff ff       	call   102a13 <page2pa>
  1034b6:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  1034bb:	89 45 ec             	mov    %eax,-0x14(%ebp)
    //用memset清除page
    memset((void *)KADDR(pa), 0, PGSIZE);
  1034be:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1034c1:	89 45 e8             	mov    %eax,-0x18(%ebp)
  1034c4:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1034c7:	c1 e8 0c             	shr    $0xc,%eax
  1034ca:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  1034cd:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  1034d2:	39 45 e4             	cmp    %eax,-0x1c(%ebp)
  1034d5:	72 23                	jb     1034fa <get_pte+0x11a>
  1034d7:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1034da:	89 44 24 0c          	mov    %eax,0xc(%esp)
  1034de:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  1034e5:	00 
  1034e6:	c7 44 24 04 7d 01 00 	movl   $0x17d,0x4(%esp)
  1034ed:	00 
  1034ee:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1034f5:	e8 2c cf ff ff       	call   100426 <__panic>
  1034fa:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1034fd:	2d 00 00 00 40       	sub    $0x40000000,%eax
  103502:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  103509:	00 
  10350a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  103511:	00 
  103512:	89 04 24             	mov    %eax,(%esp)
  103515:	e8 dd 24 00 00       	call   1059f7 <memset>
    //设置PDE权限
    *pdep = pa | PTE_P | PTE_W | PTE_U;
  10351a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10351d:	83 c8 07             	or     $0x7,%eax
  103520:	89 c2                	mov    %eax,%edx
  103522:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103525:	89 10                	mov    %edx,(%eax)
    //返回PTE
    pte_t *ptep = (pte_t *)KADDR(pa) + PTX(la);
  103527:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10352a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  10352d:	8b 45 e0             	mov    -0x20(%ebp),%eax
  103530:	c1 e8 0c             	shr    $0xc,%eax
  103533:	89 45 dc             	mov    %eax,-0x24(%ebp)
  103536:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  10353b:	39 45 dc             	cmp    %eax,-0x24(%ebp)
  10353e:	72 23                	jb     103563 <get_pte+0x183>
  103540:	8b 45 e0             	mov    -0x20(%ebp),%eax
  103543:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103547:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  10354e:	00 
  10354f:	c7 44 24 04 81 01 00 	movl   $0x181,0x4(%esp)
  103556:	00 
  103557:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10355e:	e8 c3 ce ff ff       	call   100426 <__panic>
  103563:	8b 45 e0             	mov    -0x20(%ebp),%eax
  103566:	2d 00 00 00 40       	sub    $0x40000000,%eax
  10356b:	89 c2                	mov    %eax,%edx
  10356d:	8b 45 0c             	mov    0xc(%ebp),%eax
  103570:	c1 e8 0c             	shr    $0xc,%eax
  103573:	25 ff 03 00 00       	and    $0x3ff,%eax
  103578:	c1 e0 02             	shl    $0x2,%eax
  10357b:	01 d0                	add    %edx,%eax
  10357d:	89 45 d8             	mov    %eax,-0x28(%ebp)

    return ptep;
  103580:	8b 45 d8             	mov    -0x28(%ebp),%eax
}
  103583:	c9                   	leave  
  103584:	c3                   	ret    

00103585 <get_page>:


//get_page - get related Page struct for linear address la using PDT pgdir
struct Page *
get_page(pde_t *pgdir, uintptr_t la, pte_t **ptep_store) {
  103585:	f3 0f 1e fb          	endbr32 
  103589:	55                   	push   %ebp
  10358a:	89 e5                	mov    %esp,%ebp
  10358c:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 0);
  10358f:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  103596:	00 
  103597:	8b 45 0c             	mov    0xc(%ebp),%eax
  10359a:	89 44 24 04          	mov    %eax,0x4(%esp)
  10359e:	8b 45 08             	mov    0x8(%ebp),%eax
  1035a1:	89 04 24             	mov    %eax,(%esp)
  1035a4:	e8 37 fe ff ff       	call   1033e0 <get_pte>
  1035a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep_store != NULL) {
  1035ac:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  1035b0:	74 08                	je     1035ba <get_page+0x35>
        *ptep_store = ptep;
  1035b2:	8b 45 10             	mov    0x10(%ebp),%eax
  1035b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
  1035b8:	89 10                	mov    %edx,(%eax)
    }
    if (ptep != NULL && *ptep & PTE_P) {
  1035ba:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1035be:	74 1b                	je     1035db <get_page+0x56>
  1035c0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1035c3:	8b 00                	mov    (%eax),%eax
  1035c5:	83 e0 01             	and    $0x1,%eax
  1035c8:	85 c0                	test   %eax,%eax
  1035ca:	74 0f                	je     1035db <get_page+0x56>
        return pte2page(*ptep);
  1035cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1035cf:	8b 00                	mov    (%eax),%eax
  1035d1:	89 04 24             	mov    %eax,(%esp)
  1035d4:	e8 f3 f4 ff ff       	call   102acc <pte2page>
  1035d9:	eb 05                	jmp    1035e0 <get_page+0x5b>
    }
    return NULL;
  1035db:	b8 00 00 00 00       	mov    $0x0,%eax
}
  1035e0:	c9                   	leave  
  1035e1:	c3                   	ret    

001035e2 <page_remove_pte>:

//page_remove_pte - free an Page sturct which is related linear address la
//                - and clean(invalidate) pte which is related linear address la
//note: PT is changed, so the TLB need to be invalidate 
static inline void
page_remove_pte(pde_t *pgdir, uintptr_t la, pte_t *ptep) {
  1035e2:	55                   	push   %ebp
  1035e3:	89 e5                	mov    %esp,%ebp
  1035e5:	83 ec 28             	sub    $0x28,%esp
                                  //(6) flush tlb
    }
#endif
    //检查page directory是否存在
    //练习二我们已经学到 PTE_P用于表示page dir是否存在
    if (*ptep & PTE_P) 
  1035e8:	8b 45 10             	mov    0x10(%ebp),%eax
  1035eb:	8b 00                	mov    (%eax),%eax
  1035ed:	83 e0 01             	and    $0x1,%eax
  1035f0:	85 c0                	test   %eax,%eax
  1035f2:	74 4d                	je     103641 <page_remove_pte+0x5f>
    {
        struct Page *page = pte2page(*ptep);
  1035f4:	8b 45 10             	mov    0x10(%ebp),%eax
  1035f7:	8b 00                	mov    (%eax),%eax
  1035f9:	89 04 24             	mov    %eax,(%esp)
  1035fc:	e8 cb f4 ff ff       	call   102acc <pte2page>
  103601:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //  (page_ref_dec(page)将page的ref减1,并返回减1之后的ref  
        if (page_ref_dec(page) == 0) //如果ref为0,则free即可
  103604:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103607:	89 04 24             	mov    %eax,(%esp)
  10360a:	e8 42 f5 ff ff       	call   102b51 <page_ref_dec>
  10360f:	85 c0                	test   %eax,%eax
  103611:	75 13                	jne    103626 <page_remove_pte+0x44>
        {
            free_page(page);
  103613:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  10361a:	00 
  10361b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10361e:	89 04 24             	mov    %eax,(%esp)
  103621:	e8 4e f7 ff ff       	call   102d74 <free_pages>
        }
        //清除第二个页表 PTE
        *ptep = 0; 
  103626:	8b 45 10             	mov    0x10(%ebp),%eax
  103629:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
        //刷新 tlb  
        tlb_invalidate(pgdir, la);
  10362f:	8b 45 0c             	mov    0xc(%ebp),%eax
  103632:	89 44 24 04          	mov    %eax,0x4(%esp)
  103636:	8b 45 08             	mov    0x8(%ebp),%eax
  103639:	89 04 24             	mov    %eax,(%esp)
  10363c:	e8 09 01 00 00       	call   10374a <tlb_invalidate>
    }
}
  103641:	90                   	nop
  103642:	c9                   	leave  
  103643:	c3                   	ret    

00103644 <page_remove>:

//page_remove - free an Page which is related linear address la and has an validated pte
void
page_remove(pde_t *pgdir, uintptr_t la) {
  103644:	f3 0f 1e fb          	endbr32 
  103648:	55                   	push   %ebp
  103649:	89 e5                	mov    %esp,%ebp
  10364b:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 0);
  10364e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  103655:	00 
  103656:	8b 45 0c             	mov    0xc(%ebp),%eax
  103659:	89 44 24 04          	mov    %eax,0x4(%esp)
  10365d:	8b 45 08             	mov    0x8(%ebp),%eax
  103660:	89 04 24             	mov    %eax,(%esp)
  103663:	e8 78 fd ff ff       	call   1033e0 <get_pte>
  103668:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep != NULL) {
  10366b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  10366f:	74 19                	je     10368a <page_remove+0x46>
        page_remove_pte(pgdir, la, ptep);
  103671:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103674:	89 44 24 08          	mov    %eax,0x8(%esp)
  103678:	8b 45 0c             	mov    0xc(%ebp),%eax
  10367b:	89 44 24 04          	mov    %eax,0x4(%esp)
  10367f:	8b 45 08             	mov    0x8(%ebp),%eax
  103682:	89 04 24             	mov    %eax,(%esp)
  103685:	e8 58 ff ff ff       	call   1035e2 <page_remove_pte>
    }
}
  10368a:	90                   	nop
  10368b:	c9                   	leave  
  10368c:	c3                   	ret    

0010368d <page_insert>:
//  la:    the linear address need to map
//  perm:  the permission of this Page which is setted in related pte
// return value: always 0
//note: PT is changed, so the TLB need to be invalidate 
int
page_insert(pde_t *pgdir, struct Page *page, uintptr_t la, uint32_t perm) {
  10368d:	f3 0f 1e fb          	endbr32 
  103691:	55                   	push   %ebp
  103692:	89 e5                	mov    %esp,%ebp
  103694:	83 ec 28             	sub    $0x28,%esp
    pte_t *ptep = get_pte(pgdir, la, 1);
  103697:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
  10369e:	00 
  10369f:	8b 45 10             	mov    0x10(%ebp),%eax
  1036a2:	89 44 24 04          	mov    %eax,0x4(%esp)
  1036a6:	8b 45 08             	mov    0x8(%ebp),%eax
  1036a9:	89 04 24             	mov    %eax,(%esp)
  1036ac:	e8 2f fd ff ff       	call   1033e0 <get_pte>
  1036b1:	89 45 f4             	mov    %eax,-0xc(%ebp)
    if (ptep == NULL) {
  1036b4:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1036b8:	75 0a                	jne    1036c4 <page_insert+0x37>
        return -E_NO_MEM;
  1036ba:	b8 fc ff ff ff       	mov    $0xfffffffc,%eax
  1036bf:	e9 84 00 00 00       	jmp    103748 <page_insert+0xbb>
    }
    page_ref_inc(page);
  1036c4:	8b 45 0c             	mov    0xc(%ebp),%eax
  1036c7:	89 04 24             	mov    %eax,(%esp)
  1036ca:	e8 6b f4 ff ff       	call   102b3a <page_ref_inc>
    if (*ptep & PTE_P) {
  1036cf:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1036d2:	8b 00                	mov    (%eax),%eax
  1036d4:	83 e0 01             	and    $0x1,%eax
  1036d7:	85 c0                	test   %eax,%eax
  1036d9:	74 3e                	je     103719 <page_insert+0x8c>
        struct Page *p = pte2page(*ptep);
  1036db:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1036de:	8b 00                	mov    (%eax),%eax
  1036e0:	89 04 24             	mov    %eax,(%esp)
  1036e3:	e8 e4 f3 ff ff       	call   102acc <pte2page>
  1036e8:	89 45 f0             	mov    %eax,-0x10(%ebp)
        if (p == page) {
  1036eb:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1036ee:	3b 45 0c             	cmp    0xc(%ebp),%eax
  1036f1:	75 0d                	jne    103700 <page_insert+0x73>
            page_ref_dec(page);
  1036f3:	8b 45 0c             	mov    0xc(%ebp),%eax
  1036f6:	89 04 24             	mov    %eax,(%esp)
  1036f9:	e8 53 f4 ff ff       	call   102b51 <page_ref_dec>
  1036fe:	eb 19                	jmp    103719 <page_insert+0x8c>
        }
        else {
            page_remove_pte(pgdir, la, ptep);
  103700:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103703:	89 44 24 08          	mov    %eax,0x8(%esp)
  103707:	8b 45 10             	mov    0x10(%ebp),%eax
  10370a:	89 44 24 04          	mov    %eax,0x4(%esp)
  10370e:	8b 45 08             	mov    0x8(%ebp),%eax
  103711:	89 04 24             	mov    %eax,(%esp)
  103714:	e8 c9 fe ff ff       	call   1035e2 <page_remove_pte>
        }
    }
    *ptep = page2pa(page) | PTE_P | perm;
  103719:	8b 45 0c             	mov    0xc(%ebp),%eax
  10371c:	89 04 24             	mov    %eax,(%esp)
  10371f:	e8 ef f2 ff ff       	call   102a13 <page2pa>
  103724:	0b 45 14             	or     0x14(%ebp),%eax
  103727:	83 c8 01             	or     $0x1,%eax
  10372a:	89 c2                	mov    %eax,%edx
  10372c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10372f:	89 10                	mov    %edx,(%eax)
    tlb_invalidate(pgdir, la);
  103731:	8b 45 10             	mov    0x10(%ebp),%eax
  103734:	89 44 24 04          	mov    %eax,0x4(%esp)
  103738:	8b 45 08             	mov    0x8(%ebp),%eax
  10373b:	89 04 24             	mov    %eax,(%esp)
  10373e:	e8 07 00 00 00       	call   10374a <tlb_invalidate>
    return 0;
  103743:	b8 00 00 00 00       	mov    $0x0,%eax
}
  103748:	c9                   	leave  
  103749:	c3                   	ret    

0010374a <tlb_invalidate>:

// invalidate a TLB entry, but only if the page tables being
// edited are the ones currently in use by the processor.
void
tlb_invalidate(pde_t *pgdir, uintptr_t la) {
  10374a:	f3 0f 1e fb          	endbr32 
  10374e:	55                   	push   %ebp
  10374f:	89 e5                	mov    %esp,%ebp
  103751:	83 ec 28             	sub    $0x28,%esp
}

static inline uintptr_t
rcr3(void) {
    uintptr_t cr3;
    asm volatile ("mov %%cr3, %0" : "=r" (cr3) :: "memory");
  103754:	0f 20 d8             	mov    %cr3,%eax
  103757:	89 45 f0             	mov    %eax,-0x10(%ebp)
    return cr3;
  10375a:	8b 55 f0             	mov    -0x10(%ebp),%edx
    if (rcr3() == PADDR(pgdir)) {
  10375d:	8b 45 08             	mov    0x8(%ebp),%eax
  103760:	89 45 f4             	mov    %eax,-0xc(%ebp)
  103763:	81 7d f4 ff ff ff bf 	cmpl   $0xbfffffff,-0xc(%ebp)
  10376a:	77 23                	ja     10378f <tlb_invalidate+0x45>
  10376c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10376f:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103773:	c7 44 24 08 44 6a 10 	movl   $0x106a44,0x8(%esp)
  10377a:	00 
  10377b:	c7 44 24 04 ed 01 00 	movl   $0x1ed,0x4(%esp)
  103782:	00 
  103783:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10378a:	e8 97 cc ff ff       	call   100426 <__panic>
  10378f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103792:	05 00 00 00 40       	add    $0x40000000,%eax
  103797:	39 d0                	cmp    %edx,%eax
  103799:	75 0d                	jne    1037a8 <tlb_invalidate+0x5e>
        invlpg((void *)la);
  10379b:	8b 45 0c             	mov    0xc(%ebp),%eax
  10379e:	89 45 ec             	mov    %eax,-0x14(%ebp)
}

static inline void
invlpg(void *addr) {
    asm volatile ("invlpg (%0)" :: "r" (addr) : "memory");
  1037a1:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1037a4:	0f 01 38             	invlpg (%eax)
}
  1037a7:	90                   	nop
    }
}
  1037a8:	90                   	nop
  1037a9:	c9                   	leave  
  1037aa:	c3                   	ret    

001037ab <check_alloc_page>:

static void
check_alloc_page(void) {
  1037ab:	f3 0f 1e fb          	endbr32 
  1037af:	55                   	push   %ebp
  1037b0:	89 e5                	mov    %esp,%ebp
  1037b2:	83 ec 18             	sub    $0x18,%esp
    pmm_manager->check();
  1037b5:	a1 10 cf 11 00       	mov    0x11cf10,%eax
  1037ba:	8b 40 18             	mov    0x18(%eax),%eax
  1037bd:	ff d0                	call   *%eax
    cprintf("check_alloc_page() succeeded!\n");
  1037bf:	c7 04 24 c8 6a 10 00 	movl   $0x106ac8,(%esp)
  1037c6:	e8 ef ca ff ff       	call   1002ba <cprintf>
}
  1037cb:	90                   	nop
  1037cc:	c9                   	leave  
  1037cd:	c3                   	ret    

001037ce <check_pgdir>:

static void
check_pgdir(void) {
  1037ce:	f3 0f 1e fb          	endbr32 
  1037d2:	55                   	push   %ebp
  1037d3:	89 e5                	mov    %esp,%ebp
  1037d5:	83 ec 38             	sub    $0x38,%esp
    assert(npage <= KMEMSIZE / PGSIZE);
  1037d8:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  1037dd:	3d 00 80 03 00       	cmp    $0x38000,%eax
  1037e2:	76 24                	jbe    103808 <check_pgdir+0x3a>
  1037e4:	c7 44 24 0c e7 6a 10 	movl   $0x106ae7,0xc(%esp)
  1037eb:	00 
  1037ec:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  1037f3:	00 
  1037f4:	c7 44 24 04 fa 01 00 	movl   $0x1fa,0x4(%esp)
  1037fb:	00 
  1037fc:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103803:	e8 1e cc ff ff       	call   100426 <__panic>
    assert(boot_pgdir != NULL && (uint32_t)PGOFF(boot_pgdir) == 0);
  103808:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  10380d:	85 c0                	test   %eax,%eax
  10380f:	74 0e                	je     10381f <check_pgdir+0x51>
  103811:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103816:	25 ff 0f 00 00       	and    $0xfff,%eax
  10381b:	85 c0                	test   %eax,%eax
  10381d:	74 24                	je     103843 <check_pgdir+0x75>
  10381f:	c7 44 24 0c 04 6b 10 	movl   $0x106b04,0xc(%esp)
  103826:	00 
  103827:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10382e:	00 
  10382f:	c7 44 24 04 fb 01 00 	movl   $0x1fb,0x4(%esp)
  103836:	00 
  103837:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10383e:	e8 e3 cb ff ff       	call   100426 <__panic>
    assert(get_page(boot_pgdir, 0x0, NULL) == NULL);
  103843:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103848:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  10384f:	00 
  103850:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  103857:	00 
  103858:	89 04 24             	mov    %eax,(%esp)
  10385b:	e8 25 fd ff ff       	call   103585 <get_page>
  103860:	85 c0                	test   %eax,%eax
  103862:	74 24                	je     103888 <check_pgdir+0xba>
  103864:	c7 44 24 0c 3c 6b 10 	movl   $0x106b3c,0xc(%esp)
  10386b:	00 
  10386c:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103873:	00 
  103874:	c7 44 24 04 fc 01 00 	movl   $0x1fc,0x4(%esp)
  10387b:	00 
  10387c:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103883:	e8 9e cb ff ff       	call   100426 <__panic>

    struct Page *p1, *p2;
    p1 = alloc_page();
  103888:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  10388f:	e8 a4 f4 ff ff       	call   102d38 <alloc_pages>
  103894:	89 45 f4             	mov    %eax,-0xc(%ebp)
    assert(page_insert(boot_pgdir, p1, 0x0, 0) == 0);
  103897:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  10389c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  1038a3:	00 
  1038a4:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1038ab:	00 
  1038ac:	8b 55 f4             	mov    -0xc(%ebp),%edx
  1038af:	89 54 24 04          	mov    %edx,0x4(%esp)
  1038b3:	89 04 24             	mov    %eax,(%esp)
  1038b6:	e8 d2 fd ff ff       	call   10368d <page_insert>
  1038bb:	85 c0                	test   %eax,%eax
  1038bd:	74 24                	je     1038e3 <check_pgdir+0x115>
  1038bf:	c7 44 24 0c 64 6b 10 	movl   $0x106b64,0xc(%esp)
  1038c6:	00 
  1038c7:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  1038ce:	00 
  1038cf:	c7 44 24 04 00 02 00 	movl   $0x200,0x4(%esp)
  1038d6:	00 
  1038d7:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1038de:	e8 43 cb ff ff       	call   100426 <__panic>

    pte_t *ptep;
    assert((ptep = get_pte(boot_pgdir, 0x0, 0)) != NULL);
  1038e3:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  1038e8:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1038ef:	00 
  1038f0:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1038f7:	00 
  1038f8:	89 04 24             	mov    %eax,(%esp)
  1038fb:	e8 e0 fa ff ff       	call   1033e0 <get_pte>
  103900:	89 45 f0             	mov    %eax,-0x10(%ebp)
  103903:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  103907:	75 24                	jne    10392d <check_pgdir+0x15f>
  103909:	c7 44 24 0c 90 6b 10 	movl   $0x106b90,0xc(%esp)
  103910:	00 
  103911:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103918:	00 
  103919:	c7 44 24 04 03 02 00 	movl   $0x203,0x4(%esp)
  103920:	00 
  103921:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103928:	e8 f9 ca ff ff       	call   100426 <__panic>
    assert(pte2page(*ptep) == p1);
  10392d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103930:	8b 00                	mov    (%eax),%eax
  103932:	89 04 24             	mov    %eax,(%esp)
  103935:	e8 92 f1 ff ff       	call   102acc <pte2page>
  10393a:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  10393d:	74 24                	je     103963 <check_pgdir+0x195>
  10393f:	c7 44 24 0c bd 6b 10 	movl   $0x106bbd,0xc(%esp)
  103946:	00 
  103947:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10394e:	00 
  10394f:	c7 44 24 04 04 02 00 	movl   $0x204,0x4(%esp)
  103956:	00 
  103957:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10395e:	e8 c3 ca ff ff       	call   100426 <__panic>
    assert(page_ref(p1) == 1);
  103963:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103966:	89 04 24             	mov    %eax,(%esp)
  103969:	e8 b4 f1 ff ff       	call   102b22 <page_ref>
  10396e:	83 f8 01             	cmp    $0x1,%eax
  103971:	74 24                	je     103997 <check_pgdir+0x1c9>
  103973:	c7 44 24 0c d3 6b 10 	movl   $0x106bd3,0xc(%esp)
  10397a:	00 
  10397b:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103982:	00 
  103983:	c7 44 24 04 05 02 00 	movl   $0x205,0x4(%esp)
  10398a:	00 
  10398b:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103992:	e8 8f ca ff ff       	call   100426 <__panic>

    ptep = &((pte_t *)KADDR(PDE_ADDR(boot_pgdir[0])))[1];
  103997:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  10399c:	8b 00                	mov    (%eax),%eax
  10399e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  1039a3:	89 45 ec             	mov    %eax,-0x14(%ebp)
  1039a6:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1039a9:	c1 e8 0c             	shr    $0xc,%eax
  1039ac:	89 45 e8             	mov    %eax,-0x18(%ebp)
  1039af:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  1039b4:	39 45 e8             	cmp    %eax,-0x18(%ebp)
  1039b7:	72 23                	jb     1039dc <check_pgdir+0x20e>
  1039b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1039bc:	89 44 24 0c          	mov    %eax,0xc(%esp)
  1039c0:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  1039c7:	00 
  1039c8:	c7 44 24 04 07 02 00 	movl   $0x207,0x4(%esp)
  1039cf:	00 
  1039d0:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1039d7:	e8 4a ca ff ff       	call   100426 <__panic>
  1039dc:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1039df:	2d 00 00 00 40       	sub    $0x40000000,%eax
  1039e4:	83 c0 04             	add    $0x4,%eax
  1039e7:	89 45 f0             	mov    %eax,-0x10(%ebp)
    assert(get_pte(boot_pgdir, PGSIZE, 0) == ptep);
  1039ea:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  1039ef:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  1039f6:	00 
  1039f7:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
  1039fe:	00 
  1039ff:	89 04 24             	mov    %eax,(%esp)
  103a02:	e8 d9 f9 ff ff       	call   1033e0 <get_pte>
  103a07:	39 45 f0             	cmp    %eax,-0x10(%ebp)
  103a0a:	74 24                	je     103a30 <check_pgdir+0x262>
  103a0c:	c7 44 24 0c e8 6b 10 	movl   $0x106be8,0xc(%esp)
  103a13:	00 
  103a14:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103a1b:	00 
  103a1c:	c7 44 24 04 08 02 00 	movl   $0x208,0x4(%esp)
  103a23:	00 
  103a24:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103a2b:	e8 f6 c9 ff ff       	call   100426 <__panic>

    p2 = alloc_page();
  103a30:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  103a37:	e8 fc f2 ff ff       	call   102d38 <alloc_pages>
  103a3c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    assert(page_insert(boot_pgdir, p2, PGSIZE, PTE_U | PTE_W) == 0);
  103a3f:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103a44:	c7 44 24 0c 06 00 00 	movl   $0x6,0xc(%esp)
  103a4b:	00 
  103a4c:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  103a53:	00 
  103a54:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  103a57:	89 54 24 04          	mov    %edx,0x4(%esp)
  103a5b:	89 04 24             	mov    %eax,(%esp)
  103a5e:	e8 2a fc ff ff       	call   10368d <page_insert>
  103a63:	85 c0                	test   %eax,%eax
  103a65:	74 24                	je     103a8b <check_pgdir+0x2bd>
  103a67:	c7 44 24 0c 10 6c 10 	movl   $0x106c10,0xc(%esp)
  103a6e:	00 
  103a6f:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103a76:	00 
  103a77:	c7 44 24 04 0b 02 00 	movl   $0x20b,0x4(%esp)
  103a7e:	00 
  103a7f:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103a86:	e8 9b c9 ff ff       	call   100426 <__panic>
    assert((ptep = get_pte(boot_pgdir, PGSIZE, 0)) != NULL);
  103a8b:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103a90:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  103a97:	00 
  103a98:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
  103a9f:	00 
  103aa0:	89 04 24             	mov    %eax,(%esp)
  103aa3:	e8 38 f9 ff ff       	call   1033e0 <get_pte>
  103aa8:	89 45 f0             	mov    %eax,-0x10(%ebp)
  103aab:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  103aaf:	75 24                	jne    103ad5 <check_pgdir+0x307>
  103ab1:	c7 44 24 0c 48 6c 10 	movl   $0x106c48,0xc(%esp)
  103ab8:	00 
  103ab9:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103ac0:	00 
  103ac1:	c7 44 24 04 0c 02 00 	movl   $0x20c,0x4(%esp)
  103ac8:	00 
  103ac9:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103ad0:	e8 51 c9 ff ff       	call   100426 <__panic>
    assert(*ptep & PTE_U);
  103ad5:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103ad8:	8b 00                	mov    (%eax),%eax
  103ada:	83 e0 04             	and    $0x4,%eax
  103add:	85 c0                	test   %eax,%eax
  103adf:	75 24                	jne    103b05 <check_pgdir+0x337>
  103ae1:	c7 44 24 0c 78 6c 10 	movl   $0x106c78,0xc(%esp)
  103ae8:	00 
  103ae9:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103af0:	00 
  103af1:	c7 44 24 04 0d 02 00 	movl   $0x20d,0x4(%esp)
  103af8:	00 
  103af9:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103b00:	e8 21 c9 ff ff       	call   100426 <__panic>
    assert(*ptep & PTE_W);
  103b05:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103b08:	8b 00                	mov    (%eax),%eax
  103b0a:	83 e0 02             	and    $0x2,%eax
  103b0d:	85 c0                	test   %eax,%eax
  103b0f:	75 24                	jne    103b35 <check_pgdir+0x367>
  103b11:	c7 44 24 0c 86 6c 10 	movl   $0x106c86,0xc(%esp)
  103b18:	00 
  103b19:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103b20:	00 
  103b21:	c7 44 24 04 0e 02 00 	movl   $0x20e,0x4(%esp)
  103b28:	00 
  103b29:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103b30:	e8 f1 c8 ff ff       	call   100426 <__panic>
    assert(boot_pgdir[0] & PTE_U);
  103b35:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103b3a:	8b 00                	mov    (%eax),%eax
  103b3c:	83 e0 04             	and    $0x4,%eax
  103b3f:	85 c0                	test   %eax,%eax
  103b41:	75 24                	jne    103b67 <check_pgdir+0x399>
  103b43:	c7 44 24 0c 94 6c 10 	movl   $0x106c94,0xc(%esp)
  103b4a:	00 
  103b4b:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103b52:	00 
  103b53:	c7 44 24 04 0f 02 00 	movl   $0x20f,0x4(%esp)
  103b5a:	00 
  103b5b:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103b62:	e8 bf c8 ff ff       	call   100426 <__panic>
    assert(page_ref(p2) == 1);
  103b67:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103b6a:	89 04 24             	mov    %eax,(%esp)
  103b6d:	e8 b0 ef ff ff       	call   102b22 <page_ref>
  103b72:	83 f8 01             	cmp    $0x1,%eax
  103b75:	74 24                	je     103b9b <check_pgdir+0x3cd>
  103b77:	c7 44 24 0c aa 6c 10 	movl   $0x106caa,0xc(%esp)
  103b7e:	00 
  103b7f:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103b86:	00 
  103b87:	c7 44 24 04 10 02 00 	movl   $0x210,0x4(%esp)
  103b8e:	00 
  103b8f:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103b96:	e8 8b c8 ff ff       	call   100426 <__panic>

    assert(page_insert(boot_pgdir, p1, PGSIZE, 0) == 0);
  103b9b:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103ba0:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
  103ba7:	00 
  103ba8:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
  103baf:	00 
  103bb0:	8b 55 f4             	mov    -0xc(%ebp),%edx
  103bb3:	89 54 24 04          	mov    %edx,0x4(%esp)
  103bb7:	89 04 24             	mov    %eax,(%esp)
  103bba:	e8 ce fa ff ff       	call   10368d <page_insert>
  103bbf:	85 c0                	test   %eax,%eax
  103bc1:	74 24                	je     103be7 <check_pgdir+0x419>
  103bc3:	c7 44 24 0c bc 6c 10 	movl   $0x106cbc,0xc(%esp)
  103bca:	00 
  103bcb:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103bd2:	00 
  103bd3:	c7 44 24 04 12 02 00 	movl   $0x212,0x4(%esp)
  103bda:	00 
  103bdb:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103be2:	e8 3f c8 ff ff       	call   100426 <__panic>
    assert(page_ref(p1) == 2);
  103be7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103bea:	89 04 24             	mov    %eax,(%esp)
  103bed:	e8 30 ef ff ff       	call   102b22 <page_ref>
  103bf2:	83 f8 02             	cmp    $0x2,%eax
  103bf5:	74 24                	je     103c1b <check_pgdir+0x44d>
  103bf7:	c7 44 24 0c e8 6c 10 	movl   $0x106ce8,0xc(%esp)
  103bfe:	00 
  103bff:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103c06:	00 
  103c07:	c7 44 24 04 13 02 00 	movl   $0x213,0x4(%esp)
  103c0e:	00 
  103c0f:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103c16:	e8 0b c8 ff ff       	call   100426 <__panic>
    assert(page_ref(p2) == 0);
  103c1b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103c1e:	89 04 24             	mov    %eax,(%esp)
  103c21:	e8 fc ee ff ff       	call   102b22 <page_ref>
  103c26:	85 c0                	test   %eax,%eax
  103c28:	74 24                	je     103c4e <check_pgdir+0x480>
  103c2a:	c7 44 24 0c fa 6c 10 	movl   $0x106cfa,0xc(%esp)
  103c31:	00 
  103c32:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103c39:	00 
  103c3a:	c7 44 24 04 14 02 00 	movl   $0x214,0x4(%esp)
  103c41:	00 
  103c42:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103c49:	e8 d8 c7 ff ff       	call   100426 <__panic>
    assert((ptep = get_pte(boot_pgdir, PGSIZE, 0)) != NULL);
  103c4e:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103c53:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  103c5a:	00 
  103c5b:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
  103c62:	00 
  103c63:	89 04 24             	mov    %eax,(%esp)
  103c66:	e8 75 f7 ff ff       	call   1033e0 <get_pte>
  103c6b:	89 45 f0             	mov    %eax,-0x10(%ebp)
  103c6e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  103c72:	75 24                	jne    103c98 <check_pgdir+0x4ca>
  103c74:	c7 44 24 0c 48 6c 10 	movl   $0x106c48,0xc(%esp)
  103c7b:	00 
  103c7c:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103c83:	00 
  103c84:	c7 44 24 04 15 02 00 	movl   $0x215,0x4(%esp)
  103c8b:	00 
  103c8c:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103c93:	e8 8e c7 ff ff       	call   100426 <__panic>
    assert(pte2page(*ptep) == p1);
  103c98:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103c9b:	8b 00                	mov    (%eax),%eax
  103c9d:	89 04 24             	mov    %eax,(%esp)
  103ca0:	e8 27 ee ff ff       	call   102acc <pte2page>
  103ca5:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  103ca8:	74 24                	je     103cce <check_pgdir+0x500>
  103caa:	c7 44 24 0c bd 6b 10 	movl   $0x106bbd,0xc(%esp)
  103cb1:	00 
  103cb2:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103cb9:	00 
  103cba:	c7 44 24 04 16 02 00 	movl   $0x216,0x4(%esp)
  103cc1:	00 
  103cc2:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103cc9:	e8 58 c7 ff ff       	call   100426 <__panic>
    assert((*ptep & PTE_U) == 0);
  103cce:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103cd1:	8b 00                	mov    (%eax),%eax
  103cd3:	83 e0 04             	and    $0x4,%eax
  103cd6:	85 c0                	test   %eax,%eax
  103cd8:	74 24                	je     103cfe <check_pgdir+0x530>
  103cda:	c7 44 24 0c 0c 6d 10 	movl   $0x106d0c,0xc(%esp)
  103ce1:	00 
  103ce2:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103ce9:	00 
  103cea:	c7 44 24 04 17 02 00 	movl   $0x217,0x4(%esp)
  103cf1:	00 
  103cf2:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103cf9:	e8 28 c7 ff ff       	call   100426 <__panic>

    page_remove(boot_pgdir, 0x0);
  103cfe:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103d03:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  103d0a:	00 
  103d0b:	89 04 24             	mov    %eax,(%esp)
  103d0e:	e8 31 f9 ff ff       	call   103644 <page_remove>
    assert(page_ref(p1) == 1);
  103d13:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103d16:	89 04 24             	mov    %eax,(%esp)
  103d19:	e8 04 ee ff ff       	call   102b22 <page_ref>
  103d1e:	83 f8 01             	cmp    $0x1,%eax
  103d21:	74 24                	je     103d47 <check_pgdir+0x579>
  103d23:	c7 44 24 0c d3 6b 10 	movl   $0x106bd3,0xc(%esp)
  103d2a:	00 
  103d2b:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103d32:	00 
  103d33:	c7 44 24 04 1a 02 00 	movl   $0x21a,0x4(%esp)
  103d3a:	00 
  103d3b:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103d42:	e8 df c6 ff ff       	call   100426 <__panic>
    assert(page_ref(p2) == 0);
  103d47:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103d4a:	89 04 24             	mov    %eax,(%esp)
  103d4d:	e8 d0 ed ff ff       	call   102b22 <page_ref>
  103d52:	85 c0                	test   %eax,%eax
  103d54:	74 24                	je     103d7a <check_pgdir+0x5ac>
  103d56:	c7 44 24 0c fa 6c 10 	movl   $0x106cfa,0xc(%esp)
  103d5d:	00 
  103d5e:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103d65:	00 
  103d66:	c7 44 24 04 1b 02 00 	movl   $0x21b,0x4(%esp)
  103d6d:	00 
  103d6e:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103d75:	e8 ac c6 ff ff       	call   100426 <__panic>

    page_remove(boot_pgdir, PGSIZE);
  103d7a:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103d7f:	c7 44 24 04 00 10 00 	movl   $0x1000,0x4(%esp)
  103d86:	00 
  103d87:	89 04 24             	mov    %eax,(%esp)
  103d8a:	e8 b5 f8 ff ff       	call   103644 <page_remove>
    assert(page_ref(p1) == 0);
  103d8f:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103d92:	89 04 24             	mov    %eax,(%esp)
  103d95:	e8 88 ed ff ff       	call   102b22 <page_ref>
  103d9a:	85 c0                	test   %eax,%eax
  103d9c:	74 24                	je     103dc2 <check_pgdir+0x5f4>
  103d9e:	c7 44 24 0c 21 6d 10 	movl   $0x106d21,0xc(%esp)
  103da5:	00 
  103da6:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103dad:	00 
  103dae:	c7 44 24 04 1e 02 00 	movl   $0x21e,0x4(%esp)
  103db5:	00 
  103db6:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103dbd:	e8 64 c6 ff ff       	call   100426 <__panic>
    assert(page_ref(p2) == 0);
  103dc2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103dc5:	89 04 24             	mov    %eax,(%esp)
  103dc8:	e8 55 ed ff ff       	call   102b22 <page_ref>
  103dcd:	85 c0                	test   %eax,%eax
  103dcf:	74 24                	je     103df5 <check_pgdir+0x627>
  103dd1:	c7 44 24 0c fa 6c 10 	movl   $0x106cfa,0xc(%esp)
  103dd8:	00 
  103dd9:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103de0:	00 
  103de1:	c7 44 24 04 1f 02 00 	movl   $0x21f,0x4(%esp)
  103de8:	00 
  103de9:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103df0:	e8 31 c6 ff ff       	call   100426 <__panic>

    assert(page_ref(pde2page(boot_pgdir[0])) == 1);
  103df5:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103dfa:	8b 00                	mov    (%eax),%eax
  103dfc:	89 04 24             	mov    %eax,(%esp)
  103dff:	e8 06 ed ff ff       	call   102b0a <pde2page>
  103e04:	89 04 24             	mov    %eax,(%esp)
  103e07:	e8 16 ed ff ff       	call   102b22 <page_ref>
  103e0c:	83 f8 01             	cmp    $0x1,%eax
  103e0f:	74 24                	je     103e35 <check_pgdir+0x667>
  103e11:	c7 44 24 0c 34 6d 10 	movl   $0x106d34,0xc(%esp)
  103e18:	00 
  103e19:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103e20:	00 
  103e21:	c7 44 24 04 21 02 00 	movl   $0x221,0x4(%esp)
  103e28:	00 
  103e29:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103e30:	e8 f1 c5 ff ff       	call   100426 <__panic>
    free_page(pde2page(boot_pgdir[0]));
  103e35:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103e3a:	8b 00                	mov    (%eax),%eax
  103e3c:	89 04 24             	mov    %eax,(%esp)
  103e3f:	e8 c6 ec ff ff       	call   102b0a <pde2page>
  103e44:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  103e4b:	00 
  103e4c:	89 04 24             	mov    %eax,(%esp)
  103e4f:	e8 20 ef ff ff       	call   102d74 <free_pages>
    boot_pgdir[0] = 0;
  103e54:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103e59:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

    cprintf("check_pgdir() succeeded!\n");
  103e5f:	c7 04 24 5b 6d 10 00 	movl   $0x106d5b,(%esp)
  103e66:	e8 4f c4 ff ff       	call   1002ba <cprintf>
}
  103e6b:	90                   	nop
  103e6c:	c9                   	leave  
  103e6d:	c3                   	ret    

00103e6e <check_boot_pgdir>:

static void
check_boot_pgdir(void) {
  103e6e:	f3 0f 1e fb          	endbr32 
  103e72:	55                   	push   %ebp
  103e73:	89 e5                	mov    %esp,%ebp
  103e75:	83 ec 38             	sub    $0x38,%esp
    pte_t *ptep;
    int i;
    for (i = 0; i < npage; i += PGSIZE) {
  103e78:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  103e7f:	e9 ca 00 00 00       	jmp    103f4e <check_boot_pgdir+0xe0>
        assert((ptep = get_pte(boot_pgdir, (uintptr_t)KADDR(i), 0)) != NULL);
  103e84:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103e87:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  103e8a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103e8d:	c1 e8 0c             	shr    $0xc,%eax
  103e90:	89 45 e0             	mov    %eax,-0x20(%ebp)
  103e93:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  103e98:	39 45 e0             	cmp    %eax,-0x20(%ebp)
  103e9b:	72 23                	jb     103ec0 <check_boot_pgdir+0x52>
  103e9d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103ea0:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103ea4:	c7 44 24 08 a0 69 10 	movl   $0x1069a0,0x8(%esp)
  103eab:	00 
  103eac:	c7 44 24 04 2d 02 00 	movl   $0x22d,0x4(%esp)
  103eb3:	00 
  103eb4:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103ebb:	e8 66 c5 ff ff       	call   100426 <__panic>
  103ec0:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  103ec3:	2d 00 00 00 40       	sub    $0x40000000,%eax
  103ec8:	89 c2                	mov    %eax,%edx
  103eca:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103ecf:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
  103ed6:	00 
  103ed7:	89 54 24 04          	mov    %edx,0x4(%esp)
  103edb:	89 04 24             	mov    %eax,(%esp)
  103ede:	e8 fd f4 ff ff       	call   1033e0 <get_pte>
  103ee3:	89 45 dc             	mov    %eax,-0x24(%ebp)
  103ee6:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  103eea:	75 24                	jne    103f10 <check_boot_pgdir+0xa2>
  103eec:	c7 44 24 0c 78 6d 10 	movl   $0x106d78,0xc(%esp)
  103ef3:	00 
  103ef4:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103efb:	00 
  103efc:	c7 44 24 04 2d 02 00 	movl   $0x22d,0x4(%esp)
  103f03:	00 
  103f04:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103f0b:	e8 16 c5 ff ff       	call   100426 <__panic>
        assert(PTE_ADDR(*ptep) == i);
  103f10:	8b 45 dc             	mov    -0x24(%ebp),%eax
  103f13:	8b 00                	mov    (%eax),%eax
  103f15:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  103f1a:	89 c2                	mov    %eax,%edx
  103f1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  103f1f:	39 c2                	cmp    %eax,%edx
  103f21:	74 24                	je     103f47 <check_boot_pgdir+0xd9>
  103f23:	c7 44 24 0c b5 6d 10 	movl   $0x106db5,0xc(%esp)
  103f2a:	00 
  103f2b:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103f32:	00 
  103f33:	c7 44 24 04 2e 02 00 	movl   $0x22e,0x4(%esp)
  103f3a:	00 
  103f3b:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103f42:	e8 df c4 ff ff       	call   100426 <__panic>
    for (i = 0; i < npage; i += PGSIZE) {
  103f47:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
  103f4e:	8b 55 f4             	mov    -0xc(%ebp),%edx
  103f51:	a1 80 ce 11 00       	mov    0x11ce80,%eax
  103f56:	39 c2                	cmp    %eax,%edx
  103f58:	0f 82 26 ff ff ff    	jb     103e84 <check_boot_pgdir+0x16>
    }

    assert(PDE_ADDR(boot_pgdir[PDX(VPT)]) == PADDR(boot_pgdir));
  103f5e:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103f63:	05 ac 0f 00 00       	add    $0xfac,%eax
  103f68:	8b 00                	mov    (%eax),%eax
  103f6a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  103f6f:	89 c2                	mov    %eax,%edx
  103f71:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103f76:	89 45 f0             	mov    %eax,-0x10(%ebp)
  103f79:	81 7d f0 ff ff ff bf 	cmpl   $0xbfffffff,-0x10(%ebp)
  103f80:	77 23                	ja     103fa5 <check_boot_pgdir+0x137>
  103f82:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103f85:	89 44 24 0c          	mov    %eax,0xc(%esp)
  103f89:	c7 44 24 08 44 6a 10 	movl   $0x106a44,0x8(%esp)
  103f90:	00 
  103f91:	c7 44 24 04 31 02 00 	movl   $0x231,0x4(%esp)
  103f98:	00 
  103f99:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103fa0:	e8 81 c4 ff ff       	call   100426 <__panic>
  103fa5:	8b 45 f0             	mov    -0x10(%ebp),%eax
  103fa8:	05 00 00 00 40       	add    $0x40000000,%eax
  103fad:	39 d0                	cmp    %edx,%eax
  103faf:	74 24                	je     103fd5 <check_boot_pgdir+0x167>
  103fb1:	c7 44 24 0c cc 6d 10 	movl   $0x106dcc,0xc(%esp)
  103fb8:	00 
  103fb9:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103fc0:	00 
  103fc1:	c7 44 24 04 31 02 00 	movl   $0x231,0x4(%esp)
  103fc8:	00 
  103fc9:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103fd0:	e8 51 c4 ff ff       	call   100426 <__panic>

    assert(boot_pgdir[0] == 0);
  103fd5:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  103fda:	8b 00                	mov    (%eax),%eax
  103fdc:	85 c0                	test   %eax,%eax
  103fde:	74 24                	je     104004 <check_boot_pgdir+0x196>
  103fe0:	c7 44 24 0c 00 6e 10 	movl   $0x106e00,0xc(%esp)
  103fe7:	00 
  103fe8:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  103fef:	00 
  103ff0:	c7 44 24 04 33 02 00 	movl   $0x233,0x4(%esp)
  103ff7:	00 
  103ff8:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  103fff:	e8 22 c4 ff ff       	call   100426 <__panic>

    struct Page *p;
    p = alloc_page();
  104004:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  10400b:	e8 28 ed ff ff       	call   102d38 <alloc_pages>
  104010:	89 45 ec             	mov    %eax,-0x14(%ebp)
    assert(page_insert(boot_pgdir, p, 0x100, PTE_W) == 0);
  104013:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  104018:	c7 44 24 0c 02 00 00 	movl   $0x2,0xc(%esp)
  10401f:	00 
  104020:	c7 44 24 08 00 01 00 	movl   $0x100,0x8(%esp)
  104027:	00 
  104028:	8b 55 ec             	mov    -0x14(%ebp),%edx
  10402b:	89 54 24 04          	mov    %edx,0x4(%esp)
  10402f:	89 04 24             	mov    %eax,(%esp)
  104032:	e8 56 f6 ff ff       	call   10368d <page_insert>
  104037:	85 c0                	test   %eax,%eax
  104039:	74 24                	je     10405f <check_boot_pgdir+0x1f1>
  10403b:	c7 44 24 0c 14 6e 10 	movl   $0x106e14,0xc(%esp)
  104042:	00 
  104043:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10404a:	00 
  10404b:	c7 44 24 04 37 02 00 	movl   $0x237,0x4(%esp)
  104052:	00 
  104053:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10405a:	e8 c7 c3 ff ff       	call   100426 <__panic>
    assert(page_ref(p) == 1);
  10405f:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104062:	89 04 24             	mov    %eax,(%esp)
  104065:	e8 b8 ea ff ff       	call   102b22 <page_ref>
  10406a:	83 f8 01             	cmp    $0x1,%eax
  10406d:	74 24                	je     104093 <check_boot_pgdir+0x225>
  10406f:	c7 44 24 0c 42 6e 10 	movl   $0x106e42,0xc(%esp)
  104076:	00 
  104077:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10407e:	00 
  10407f:	c7 44 24 04 38 02 00 	movl   $0x238,0x4(%esp)
  104086:	00 
  104087:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10408e:	e8 93 c3 ff ff       	call   100426 <__panic>
    assert(page_insert(boot_pgdir, p, 0x100 + PGSIZE, PTE_W) == 0);
  104093:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  104098:	c7 44 24 0c 02 00 00 	movl   $0x2,0xc(%esp)
  10409f:	00 
  1040a0:	c7 44 24 08 00 11 00 	movl   $0x1100,0x8(%esp)
  1040a7:	00 
  1040a8:	8b 55 ec             	mov    -0x14(%ebp),%edx
  1040ab:	89 54 24 04          	mov    %edx,0x4(%esp)
  1040af:	89 04 24             	mov    %eax,(%esp)
  1040b2:	e8 d6 f5 ff ff       	call   10368d <page_insert>
  1040b7:	85 c0                	test   %eax,%eax
  1040b9:	74 24                	je     1040df <check_boot_pgdir+0x271>
  1040bb:	c7 44 24 0c 54 6e 10 	movl   $0x106e54,0xc(%esp)
  1040c2:	00 
  1040c3:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  1040ca:	00 
  1040cb:	c7 44 24 04 39 02 00 	movl   $0x239,0x4(%esp)
  1040d2:	00 
  1040d3:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1040da:	e8 47 c3 ff ff       	call   100426 <__panic>
    assert(page_ref(p) == 2);
  1040df:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1040e2:	89 04 24             	mov    %eax,(%esp)
  1040e5:	e8 38 ea ff ff       	call   102b22 <page_ref>
  1040ea:	83 f8 02             	cmp    $0x2,%eax
  1040ed:	74 24                	je     104113 <check_boot_pgdir+0x2a5>
  1040ef:	c7 44 24 0c 8b 6e 10 	movl   $0x106e8b,0xc(%esp)
  1040f6:	00 
  1040f7:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  1040fe:	00 
  1040ff:	c7 44 24 04 3a 02 00 	movl   $0x23a,0x4(%esp)
  104106:	00 
  104107:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  10410e:	e8 13 c3 ff ff       	call   100426 <__panic>

    const char *str = "ucore: Hello world!!";
  104113:	c7 45 e8 9c 6e 10 00 	movl   $0x106e9c,-0x18(%ebp)
    strcpy((void *)0x100, str);
  10411a:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10411d:	89 44 24 04          	mov    %eax,0x4(%esp)
  104121:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
  104128:	e8 e6 15 00 00       	call   105713 <strcpy>
    assert(strcmp((void *)0x100, (void *)(0x100 + PGSIZE)) == 0);
  10412d:	c7 44 24 04 00 11 00 	movl   $0x1100,0x4(%esp)
  104134:	00 
  104135:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
  10413c:	e8 50 16 00 00       	call   105791 <strcmp>
  104141:	85 c0                	test   %eax,%eax
  104143:	74 24                	je     104169 <check_boot_pgdir+0x2fb>
  104145:	c7 44 24 0c b4 6e 10 	movl   $0x106eb4,0xc(%esp)
  10414c:	00 
  10414d:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  104154:	00 
  104155:	c7 44 24 04 3e 02 00 	movl   $0x23e,0x4(%esp)
  10415c:	00 
  10415d:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  104164:	e8 bd c2 ff ff       	call   100426 <__panic>

    *(char *)(page2kva(p) + 0x100) = '\0';
  104169:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10416c:	89 04 24             	mov    %eax,(%esp)
  10416f:	e8 04 e9 ff ff       	call   102a78 <page2kva>
  104174:	05 00 01 00 00       	add    $0x100,%eax
  104179:	c6 00 00             	movb   $0x0,(%eax)
    assert(strlen((const char *)0x100) == 0);
  10417c:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
  104183:	e8 2d 15 00 00       	call   1056b5 <strlen>
  104188:	85 c0                	test   %eax,%eax
  10418a:	74 24                	je     1041b0 <check_boot_pgdir+0x342>
  10418c:	c7 44 24 0c ec 6e 10 	movl   $0x106eec,0xc(%esp)
  104193:	00 
  104194:	c7 44 24 08 8d 6a 10 	movl   $0x106a8d,0x8(%esp)
  10419b:	00 
  10419c:	c7 44 24 04 41 02 00 	movl   $0x241,0x4(%esp)
  1041a3:	00 
  1041a4:	c7 04 24 68 6a 10 00 	movl   $0x106a68,(%esp)
  1041ab:	e8 76 c2 ff ff       	call   100426 <__panic>

    free_page(p);
  1041b0:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  1041b7:	00 
  1041b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1041bb:	89 04 24             	mov    %eax,(%esp)
  1041be:	e8 b1 eb ff ff       	call   102d74 <free_pages>
    free_page(pde2page(boot_pgdir[0]));
  1041c3:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  1041c8:	8b 00                	mov    (%eax),%eax
  1041ca:	89 04 24             	mov    %eax,(%esp)
  1041cd:	e8 38 e9 ff ff       	call   102b0a <pde2page>
  1041d2:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  1041d9:	00 
  1041da:	89 04 24             	mov    %eax,(%esp)
  1041dd:	e8 92 eb ff ff       	call   102d74 <free_pages>
    boot_pgdir[0] = 0;
  1041e2:	a1 e0 99 11 00       	mov    0x1199e0,%eax
  1041e7:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

    cprintf("check_boot_pgdir() succeeded!\n");
  1041ed:	c7 04 24 10 6f 10 00 	movl   $0x106f10,(%esp)
  1041f4:	e8 c1 c0 ff ff       	call   1002ba <cprintf>
}
  1041f9:	90                   	nop
  1041fa:	c9                   	leave  
  1041fb:	c3                   	ret    

001041fc <perm2str>:

//perm2str - use string 'u,r,w,-' to present the permission
static const char *
perm2str(int perm) {
  1041fc:	f3 0f 1e fb          	endbr32 
  104200:	55                   	push   %ebp
  104201:	89 e5                	mov    %esp,%ebp
    static char str[4];
    str[0] = (perm & PTE_U) ? 'u' : '-';
  104203:	8b 45 08             	mov    0x8(%ebp),%eax
  104206:	83 e0 04             	and    $0x4,%eax
  104209:	85 c0                	test   %eax,%eax
  10420b:	74 04                	je     104211 <perm2str+0x15>
  10420d:	b0 75                	mov    $0x75,%al
  10420f:	eb 02                	jmp    104213 <perm2str+0x17>
  104211:	b0 2d                	mov    $0x2d,%al
  104213:	a2 08 cf 11 00       	mov    %al,0x11cf08
    str[1] = 'r';
  104218:	c6 05 09 cf 11 00 72 	movb   $0x72,0x11cf09
    str[2] = (perm & PTE_W) ? 'w' : '-';
  10421f:	8b 45 08             	mov    0x8(%ebp),%eax
  104222:	83 e0 02             	and    $0x2,%eax
  104225:	85 c0                	test   %eax,%eax
  104227:	74 04                	je     10422d <perm2str+0x31>
  104229:	b0 77                	mov    $0x77,%al
  10422b:	eb 02                	jmp    10422f <perm2str+0x33>
  10422d:	b0 2d                	mov    $0x2d,%al
  10422f:	a2 0a cf 11 00       	mov    %al,0x11cf0a
    str[3] = '\0';
  104234:	c6 05 0b cf 11 00 00 	movb   $0x0,0x11cf0b
    return str;
  10423b:	b8 08 cf 11 00       	mov    $0x11cf08,%eax
}
  104240:	5d                   	pop    %ebp
  104241:	c3                   	ret    

00104242 <get_pgtable_items>:
//  table:       the beginning addr of table
//  left_store:  the pointer of the high side of table's next range
//  right_store: the pointer of the low side of table's next range
// return value: 0 - not a invalid item range, perm - a valid item range with perm permission 
static int
get_pgtable_items(size_t left, size_t right, size_t start, uintptr_t *table, size_t *left_store, size_t *right_store) {
  104242:	f3 0f 1e fb          	endbr32 
  104246:	55                   	push   %ebp
  104247:	89 e5                	mov    %esp,%ebp
  104249:	83 ec 10             	sub    $0x10,%esp
    if (start >= right) {
  10424c:	8b 45 10             	mov    0x10(%ebp),%eax
  10424f:	3b 45 0c             	cmp    0xc(%ebp),%eax
  104252:	72 0d                	jb     104261 <get_pgtable_items+0x1f>
        return 0;
  104254:	b8 00 00 00 00       	mov    $0x0,%eax
  104259:	e9 98 00 00 00       	jmp    1042f6 <get_pgtable_items+0xb4>
    }
    while (start < right && !(table[start] & PTE_P)) {
        start ++;
  10425e:	ff 45 10             	incl   0x10(%ebp)
    while (start < right && !(table[start] & PTE_P)) {
  104261:	8b 45 10             	mov    0x10(%ebp),%eax
  104264:	3b 45 0c             	cmp    0xc(%ebp),%eax
  104267:	73 18                	jae    104281 <get_pgtable_items+0x3f>
  104269:	8b 45 10             	mov    0x10(%ebp),%eax
  10426c:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  104273:	8b 45 14             	mov    0x14(%ebp),%eax
  104276:	01 d0                	add    %edx,%eax
  104278:	8b 00                	mov    (%eax),%eax
  10427a:	83 e0 01             	and    $0x1,%eax
  10427d:	85 c0                	test   %eax,%eax
  10427f:	74 dd                	je     10425e <get_pgtable_items+0x1c>
    }
    if (start < right) {
  104281:	8b 45 10             	mov    0x10(%ebp),%eax
  104284:	3b 45 0c             	cmp    0xc(%ebp),%eax
  104287:	73 68                	jae    1042f1 <get_pgtable_items+0xaf>
        if (left_store != NULL) {
  104289:	83 7d 18 00          	cmpl   $0x0,0x18(%ebp)
  10428d:	74 08                	je     104297 <get_pgtable_items+0x55>
            *left_store = start;
  10428f:	8b 45 18             	mov    0x18(%ebp),%eax
  104292:	8b 55 10             	mov    0x10(%ebp),%edx
  104295:	89 10                	mov    %edx,(%eax)
        }
        int perm = (table[start ++] & PTE_USER);
  104297:	8b 45 10             	mov    0x10(%ebp),%eax
  10429a:	8d 50 01             	lea    0x1(%eax),%edx
  10429d:	89 55 10             	mov    %edx,0x10(%ebp)
  1042a0:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  1042a7:	8b 45 14             	mov    0x14(%ebp),%eax
  1042aa:	01 d0                	add    %edx,%eax
  1042ac:	8b 00                	mov    (%eax),%eax
  1042ae:	83 e0 07             	and    $0x7,%eax
  1042b1:	89 45 fc             	mov    %eax,-0x4(%ebp)
        while (start < right && (table[start] & PTE_USER) == perm) {
  1042b4:	eb 03                	jmp    1042b9 <get_pgtable_items+0x77>
            start ++;
  1042b6:	ff 45 10             	incl   0x10(%ebp)
        while (start < right && (table[start] & PTE_USER) == perm) {
  1042b9:	8b 45 10             	mov    0x10(%ebp),%eax
  1042bc:	3b 45 0c             	cmp    0xc(%ebp),%eax
  1042bf:	73 1d                	jae    1042de <get_pgtable_items+0x9c>
  1042c1:	8b 45 10             	mov    0x10(%ebp),%eax
  1042c4:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
  1042cb:	8b 45 14             	mov    0x14(%ebp),%eax
  1042ce:	01 d0                	add    %edx,%eax
  1042d0:	8b 00                	mov    (%eax),%eax
  1042d2:	83 e0 07             	and    $0x7,%eax
  1042d5:	89 c2                	mov    %eax,%edx
  1042d7:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1042da:	39 c2                	cmp    %eax,%edx
  1042dc:	74 d8                	je     1042b6 <get_pgtable_items+0x74>
        }
        if (right_store != NULL) {
  1042de:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  1042e2:	74 08                	je     1042ec <get_pgtable_items+0xaa>
            *right_store = start;
  1042e4:	8b 45 1c             	mov    0x1c(%ebp),%eax
  1042e7:	8b 55 10             	mov    0x10(%ebp),%edx
  1042ea:	89 10                	mov    %edx,(%eax)
        }
        return perm;
  1042ec:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1042ef:	eb 05                	jmp    1042f6 <get_pgtable_items+0xb4>
    }
    return 0;
  1042f1:	b8 00 00 00 00       	mov    $0x0,%eax
}
  1042f6:	c9                   	leave  
  1042f7:	c3                   	ret    

001042f8 <print_pgdir>:

//print_pgdir - print the PDT&PT
void
print_pgdir(void) {
  1042f8:	f3 0f 1e fb          	endbr32 
  1042fc:	55                   	push   %ebp
  1042fd:	89 e5                	mov    %esp,%ebp
  1042ff:	57                   	push   %edi
  104300:	56                   	push   %esi
  104301:	53                   	push   %ebx
  104302:	83 ec 4c             	sub    $0x4c,%esp
    cprintf("-------------------- BEGIN --------------------\n");
  104305:	c7 04 24 30 6f 10 00 	movl   $0x106f30,(%esp)
  10430c:	e8 a9 bf ff ff       	call   1002ba <cprintf>
    size_t left, right = 0, perm;
  104311:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    while ((perm = get_pgtable_items(0, NPDEENTRY, right, vpd, &left, &right)) != 0) {
  104318:	e9 fa 00 00 00       	jmp    104417 <print_pgdir+0x11f>
        cprintf("PDE(%03x) %08x-%08x %08x %s\n", right - left,
  10431d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  104320:	89 04 24             	mov    %eax,(%esp)
  104323:	e8 d4 fe ff ff       	call   1041fc <perm2str>
                left * PTSIZE, right * PTSIZE, (right - left) * PTSIZE, perm2str(perm));
  104328:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  10432b:	8b 55 e0             	mov    -0x20(%ebp),%edx
  10432e:	29 d1                	sub    %edx,%ecx
  104330:	89 ca                	mov    %ecx,%edx
        cprintf("PDE(%03x) %08x-%08x %08x %s\n", right - left,
  104332:	89 d6                	mov    %edx,%esi
  104334:	c1 e6 16             	shl    $0x16,%esi
  104337:	8b 55 dc             	mov    -0x24(%ebp),%edx
  10433a:	89 d3                	mov    %edx,%ebx
  10433c:	c1 e3 16             	shl    $0x16,%ebx
  10433f:	8b 55 e0             	mov    -0x20(%ebp),%edx
  104342:	89 d1                	mov    %edx,%ecx
  104344:	c1 e1 16             	shl    $0x16,%ecx
  104347:	8b 7d dc             	mov    -0x24(%ebp),%edi
  10434a:	8b 55 e0             	mov    -0x20(%ebp),%edx
  10434d:	29 d7                	sub    %edx,%edi
  10434f:	89 fa                	mov    %edi,%edx
  104351:	89 44 24 14          	mov    %eax,0x14(%esp)
  104355:	89 74 24 10          	mov    %esi,0x10(%esp)
  104359:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  10435d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  104361:	89 54 24 04          	mov    %edx,0x4(%esp)
  104365:	c7 04 24 61 6f 10 00 	movl   $0x106f61,(%esp)
  10436c:	e8 49 bf ff ff       	call   1002ba <cprintf>
        size_t l, r = left * NPTEENTRY;
  104371:	8b 45 e0             	mov    -0x20(%ebp),%eax
  104374:	c1 e0 0a             	shl    $0xa,%eax
  104377:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        while ((perm = get_pgtable_items(left * NPTEENTRY, right * NPTEENTRY, r, vpt, &l, &r)) != 0) {
  10437a:	eb 54                	jmp    1043d0 <print_pgdir+0xd8>
            cprintf("  |-- PTE(%05x) %08x-%08x %08x %s\n", r - l,
  10437c:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  10437f:	89 04 24             	mov    %eax,(%esp)
  104382:	e8 75 fe ff ff       	call   1041fc <perm2str>
                    l * PGSIZE, r * PGSIZE, (r - l) * PGSIZE, perm2str(perm));
  104387:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
  10438a:	8b 55 d8             	mov    -0x28(%ebp),%edx
  10438d:	29 d1                	sub    %edx,%ecx
  10438f:	89 ca                	mov    %ecx,%edx
            cprintf("  |-- PTE(%05x) %08x-%08x %08x %s\n", r - l,
  104391:	89 d6                	mov    %edx,%esi
  104393:	c1 e6 0c             	shl    $0xc,%esi
  104396:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  104399:	89 d3                	mov    %edx,%ebx
  10439b:	c1 e3 0c             	shl    $0xc,%ebx
  10439e:	8b 55 d8             	mov    -0x28(%ebp),%edx
  1043a1:	89 d1                	mov    %edx,%ecx
  1043a3:	c1 e1 0c             	shl    $0xc,%ecx
  1043a6:	8b 7d d4             	mov    -0x2c(%ebp),%edi
  1043a9:	8b 55 d8             	mov    -0x28(%ebp),%edx
  1043ac:	29 d7                	sub    %edx,%edi
  1043ae:	89 fa                	mov    %edi,%edx
  1043b0:	89 44 24 14          	mov    %eax,0x14(%esp)
  1043b4:	89 74 24 10          	mov    %esi,0x10(%esp)
  1043b8:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  1043bc:	89 4c 24 08          	mov    %ecx,0x8(%esp)
  1043c0:	89 54 24 04          	mov    %edx,0x4(%esp)
  1043c4:	c7 04 24 80 6f 10 00 	movl   $0x106f80,(%esp)
  1043cb:	e8 ea be ff ff       	call   1002ba <cprintf>
        while ((perm = get_pgtable_items(left * NPTEENTRY, right * NPTEENTRY, r, vpt, &l, &r)) != 0) {
  1043d0:	be 00 00 c0 fa       	mov    $0xfac00000,%esi
  1043d5:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1043d8:	8b 55 dc             	mov    -0x24(%ebp),%edx
  1043db:	89 d3                	mov    %edx,%ebx
  1043dd:	c1 e3 0a             	shl    $0xa,%ebx
  1043e0:	8b 55 e0             	mov    -0x20(%ebp),%edx
  1043e3:	89 d1                	mov    %edx,%ecx
  1043e5:	c1 e1 0a             	shl    $0xa,%ecx
  1043e8:	8d 55 d4             	lea    -0x2c(%ebp),%edx
  1043eb:	89 54 24 14          	mov    %edx,0x14(%esp)
  1043ef:	8d 55 d8             	lea    -0x28(%ebp),%edx
  1043f2:	89 54 24 10          	mov    %edx,0x10(%esp)
  1043f6:	89 74 24 0c          	mov    %esi,0xc(%esp)
  1043fa:	89 44 24 08          	mov    %eax,0x8(%esp)
  1043fe:	89 5c 24 04          	mov    %ebx,0x4(%esp)
  104402:	89 0c 24             	mov    %ecx,(%esp)
  104405:	e8 38 fe ff ff       	call   104242 <get_pgtable_items>
  10440a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  10440d:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  104411:	0f 85 65 ff ff ff    	jne    10437c <print_pgdir+0x84>
    while ((perm = get_pgtable_items(0, NPDEENTRY, right, vpd, &left, &right)) != 0) {
  104417:	b9 00 b0 fe fa       	mov    $0xfafeb000,%ecx
  10441c:	8b 45 dc             	mov    -0x24(%ebp),%eax
  10441f:	8d 55 dc             	lea    -0x24(%ebp),%edx
  104422:	89 54 24 14          	mov    %edx,0x14(%esp)
  104426:	8d 55 e0             	lea    -0x20(%ebp),%edx
  104429:	89 54 24 10          	mov    %edx,0x10(%esp)
  10442d:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
  104431:	89 44 24 08          	mov    %eax,0x8(%esp)
  104435:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
  10443c:	00 
  10443d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
  104444:	e8 f9 fd ff ff       	call   104242 <get_pgtable_items>
  104449:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  10444c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  104450:	0f 85 c7 fe ff ff    	jne    10431d <print_pgdir+0x25>
        }
    }
    cprintf("--------------------- END ---------------------\n");
  104456:	c7 04 24 a4 6f 10 00 	movl   $0x106fa4,(%esp)
  10445d:	e8 58 be ff ff       	call   1002ba <cprintf>
}
  104462:	90                   	nop
  104463:	83 c4 4c             	add    $0x4c,%esp
  104466:	5b                   	pop    %ebx
  104467:	5e                   	pop    %esi
  104468:	5f                   	pop    %edi
  104469:	5d                   	pop    %ebp
  10446a:	c3                   	ret    

0010446b <page2ppn>:
page2ppn(struct Page *page) {
  10446b:	55                   	push   %ebp
  10446c:	89 e5                	mov    %esp,%ebp
    return page - pages;
  10446e:	a1 18 cf 11 00       	mov    0x11cf18,%eax
  104473:	8b 55 08             	mov    0x8(%ebp),%edx
  104476:	29 c2                	sub    %eax,%edx
  104478:	89 d0                	mov    %edx,%eax
  10447a:	c1 f8 02             	sar    $0x2,%eax
  10447d:	69 c0 cd cc cc cc    	imul   $0xcccccccd,%eax,%eax
}
  104483:	5d                   	pop    %ebp
  104484:	c3                   	ret    

00104485 <page2pa>:
page2pa(struct Page *page) {
  104485:	55                   	push   %ebp
  104486:	89 e5                	mov    %esp,%ebp
  104488:	83 ec 04             	sub    $0x4,%esp
    return page2ppn(page) << PGSHIFT;
  10448b:	8b 45 08             	mov    0x8(%ebp),%eax
  10448e:	89 04 24             	mov    %eax,(%esp)
  104491:	e8 d5 ff ff ff       	call   10446b <page2ppn>
  104496:	c1 e0 0c             	shl    $0xc,%eax
}
  104499:	c9                   	leave  
  10449a:	c3                   	ret    

0010449b <page_ref>:
page_ref(struct Page *page) {
  10449b:	55                   	push   %ebp
  10449c:	89 e5                	mov    %esp,%ebp
    return page->ref;
  10449e:	8b 45 08             	mov    0x8(%ebp),%eax
  1044a1:	8b 00                	mov    (%eax),%eax
}
  1044a3:	5d                   	pop    %ebp
  1044a4:	c3                   	ret    

001044a5 <set_page_ref>:
set_page_ref(struct Page *page, int val) {
  1044a5:	55                   	push   %ebp
  1044a6:	89 e5                	mov    %esp,%ebp
    page->ref = val;
  1044a8:	8b 45 08             	mov    0x8(%ebp),%eax
  1044ab:	8b 55 0c             	mov    0xc(%ebp),%edx
  1044ae:	89 10                	mov    %edx,(%eax)
}
  1044b0:	90                   	nop
  1044b1:	5d                   	pop    %ebp
  1044b2:	c3                   	ret    

001044b3 <default_init>:
#define free_list (free_area.free_list) //列表的头
#define nr_free (free_area.nr_free)   //空闲页的数目

//初始化free_area
static void
default_init(void) {
  1044b3:	f3 0f 1e fb          	endbr32 
  1044b7:	55                   	push   %ebp
  1044b8:	89 e5                	mov    %esp,%ebp
  1044ba:	83 ec 10             	sub    $0x10,%esp
  1044bd:	c7 45 fc 1c cf 11 00 	movl   $0x11cf1c,-0x4(%ebp)
 * list_init - initialize a new entry
 * @elm:        new entry to be initialized
 * */
static inline void
list_init(list_entry_t *elm) {
    elm->prev = elm->next = elm;
  1044c4:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1044c7:	8b 55 fc             	mov    -0x4(%ebp),%edx
  1044ca:	89 50 04             	mov    %edx,0x4(%eax)
  1044cd:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1044d0:	8b 50 04             	mov    0x4(%eax),%edx
  1044d3:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1044d6:	89 10                	mov    %edx,(%eax)
}
  1044d8:	90                   	nop
    list_init(&free_list);  //列表只有一个头指针
    nr_free = 0;   //空闲页数设为0
  1044d9:	c7 05 24 cf 11 00 00 	movl   $0x0,0x11cf24
  1044e0:	00 00 00 
}
  1044e3:	90                   	nop
  1044e4:	c9                   	leave  
  1044e5:	c3                   	ret    

001044e6 <default_init_memmap>:
//初始化n个空闲页链表
static void
default_init_memmap(struct Page *base, size_t n) {
  1044e6:	f3 0f 1e fb          	endbr32 
  1044ea:	55                   	push   %ebp
  1044eb:	89 e5                	mov    %esp,%ebp
  1044ed:	83 ec 48             	sub    $0x48,%esp
    assert(n > 0);
  1044f0:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  1044f4:	75 24                	jne    10451a <default_init_memmap+0x34>
  1044f6:	c7 44 24 0c d8 6f 10 	movl   $0x106fd8,0xc(%esp)
  1044fd:	00 
  1044fe:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104505:	00 
  104506:	c7 44 24 04 6e 00 00 	movl   $0x6e,0x4(%esp)
  10450d:	00 
  10450e:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104515:	e8 0c bf ff ff       	call   100426 <__panic>
    struct Page *p = base;  //让p为最开始的空闲页表
  10451a:	8b 45 08             	mov    0x8(%ebp),%eax
  10451d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    for (; p != base + n; p ++)   
  104520:	eb 7d                	jmp    10459f <default_init_memmap+0xb9>
    {
        assert(PageReserved(p));//检查是否为保留页
  104522:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104525:	83 c0 04             	add    $0x4,%eax
  104528:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  10452f:	89 45 ec             	mov    %eax,-0x14(%ebp)
 * @addr:   the address to count from
 * */
static inline bool
test_bit(int nr, volatile void *addr) {
    int oldbit;
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  104532:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104535:	8b 55 f0             	mov    -0x10(%ebp),%edx
  104538:	0f a3 10             	bt     %edx,(%eax)
  10453b:	19 c0                	sbb    %eax,%eax
  10453d:	89 45 e8             	mov    %eax,-0x18(%ebp)
    return oldbit != 0;
  104540:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  104544:	0f 95 c0             	setne  %al
  104547:	0f b6 c0             	movzbl %al,%eax
  10454a:	85 c0                	test   %eax,%eax
  10454c:	75 24                	jne    104572 <default_init_memmap+0x8c>
  10454e:	c7 44 24 0c 09 70 10 	movl   $0x107009,0xc(%esp)
  104555:	00 
  104556:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10455d:	00 
  10455e:	c7 44 24 04 72 00 00 	movl   $0x72,0x4(%esp)
  104565:	00 
  104566:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10456d:	e8 b4 be ff ff       	call   100426 <__panic>
        p->flags = p->property = 0;//设置标记，flag为0表示可以分配
  104572:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104575:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  10457c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10457f:	8b 50 08             	mov    0x8(%eax),%edx
  104582:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104585:	89 50 04             	mov    %edx,0x4(%eax)
                                   //property为0表示不是base
        set_page_ref(p, 0); //将p的ref设置为0(调用已经写过的函数)
  104588:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  10458f:	00 
  104590:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104593:	89 04 24             	mov    %eax,(%esp)
  104596:	e8 0a ff ff ff       	call   1044a5 <set_page_ref>
    for (; p != base + n; p ++)   
  10459b:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
  10459f:	8b 55 0c             	mov    0xc(%ebp),%edx
  1045a2:	89 d0                	mov    %edx,%eax
  1045a4:	c1 e0 02             	shl    $0x2,%eax
  1045a7:	01 d0                	add    %edx,%eax
  1045a9:	c1 e0 02             	shl    $0x2,%eax
  1045ac:	89 c2                	mov    %eax,%edx
  1045ae:	8b 45 08             	mov    0x8(%ebp),%eax
  1045b1:	01 d0                	add    %edx,%eax
  1045b3:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  1045b6:	0f 85 66 ff ff ff    	jne    104522 <default_init_memmap+0x3c>
    }
    base->property = n;  //第一个页表也就是base的property设置为n，因为有n个空闲块
  1045bc:	8b 45 08             	mov    0x8(%ebp),%eax
  1045bf:	8b 55 0c             	mov    0xc(%ebp),%edx
  1045c2:	89 50 08             	mov    %edx,0x8(%eax)
    
    SetPageProperty(base);  
  1045c5:	8b 45 08             	mov    0x8(%ebp),%eax
  1045c8:	83 c0 04             	add    $0x4,%eax
  1045cb:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
  1045d2:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  1045d5:	8b 45 cc             	mov    -0x34(%ebp),%eax
  1045d8:	8b 55 d0             	mov    -0x30(%ebp),%edx
  1045db:	0f ab 10             	bts    %edx,(%eax)
}
  1045de:	90                   	nop
    nr_free += n;   //将空闲页数目设置为n
  1045df:	8b 15 24 cf 11 00    	mov    0x11cf24,%edx
  1045e5:	8b 45 0c             	mov    0xc(%ebp),%eax
  1045e8:	01 d0                	add    %edx,%eax
  1045ea:	a3 24 cf 11 00       	mov    %eax,0x11cf24
  // list_add(&free_list, &(base->page_link));
  //之前是list_add,等价于list_add_after,这样的话就把第一个块浪费了，所以有小问题，虽然检测不会出问题
   //改为list_add_before()就更合适了
    list_add_before(&free_list, &(base->page_link));
  1045ef:	8b 45 08             	mov    0x8(%ebp),%eax
  1045f2:	83 c0 0c             	add    $0xc,%eax
  1045f5:	c7 45 e4 1c cf 11 00 	movl   $0x11cf1c,-0x1c(%ebp)
  1045fc:	89 45 e0             	mov    %eax,-0x20(%ebp)
 * Insert the new element @elm *before* the element @listelm which
 * is already in the list.
 * */
static inline void
list_add_before(list_entry_t *listelm, list_entry_t *elm) {
    __list_add(elm, listelm->prev, listelm);
  1045ff:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  104602:	8b 00                	mov    (%eax),%eax
  104604:	8b 55 e0             	mov    -0x20(%ebp),%edx
  104607:	89 55 dc             	mov    %edx,-0x24(%ebp)
  10460a:	89 45 d8             	mov    %eax,-0x28(%ebp)
  10460d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  104610:	89 45 d4             	mov    %eax,-0x2c(%ebp)
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 * */
static inline void
__list_add(list_entry_t *elm, list_entry_t *prev, list_entry_t *next) {
    prev->next = next->prev = elm;
  104613:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  104616:	8b 55 dc             	mov    -0x24(%ebp),%edx
  104619:	89 10                	mov    %edx,(%eax)
  10461b:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  10461e:	8b 10                	mov    (%eax),%edx
  104620:	8b 45 d8             	mov    -0x28(%ebp),%eax
  104623:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
  104626:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104629:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10462c:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
  10462f:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104632:	8b 55 d8             	mov    -0x28(%ebp),%edx
  104635:	89 10                	mov    %edx,(%eax)
}
  104637:	90                   	nop
}
  104638:	90                   	nop
}
  104639:	90                   	nop
  10463a:	c9                   	leave  
  10463b:	c3                   	ret    

0010463c <default_alloc_pages>:


//由于是first-fit函数，故把遇到的第一个可以用于分配的连续内存进行分配即可
static struct Page * default_alloc_pages(size_t n) {
  10463c:	f3 0f 1e fb          	endbr32 
  104640:	55                   	push   %ebp
  104641:	89 e5                	mov    %esp,%ebp
  104643:	83 ec 68             	sub    $0x68,%esp
    assert(n > 0);  //n的值应该大于0
  104646:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  10464a:	75 24                	jne    104670 <default_alloc_pages+0x34>
  10464c:	c7 44 24 0c d8 6f 10 	movl   $0x106fd8,0xc(%esp)
  104653:	00 
  104654:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10465b:	00 
  10465c:	c7 44 24 04 84 00 00 	movl   $0x84,0x4(%esp)
  104663:	00 
  104664:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10466b:	e8 b6 bd ff ff       	call   100426 <__panic>
    //如果n>nf_free，表示无法分配这么大内存，返回NULL即可
    if (n > nr_free)
  104670:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  104675:	39 45 08             	cmp    %eax,0x8(%ebp)
  104678:	76 0a                	jbe    104684 <default_alloc_pages+0x48>
     {
        return NULL;
  10467a:	b8 00 00 00 00       	mov    $0x0,%eax
  10467f:	e9 50 01 00 00       	jmp    1047d4 <default_alloc_pages+0x198>
    }
    //说明够分配，因此找到即可
    struct Page *page = NULL;
  104684:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    list_entry_t *le = &free_list;
  10468b:	c7 45 f0 1c cf 11 00 	movl   $0x11cf1c,-0x10(%ebp)
    // 查找n个或以上空闲页块，若找到，则判断是否大过n，大于n的话则将其拆分 
    //  并将拆分后的剩下的空闲页块加回到链表中
    while ((le = list_next(le)) != &free_list) 
  104692:	eb 1c                	jmp    1046b0 <default_alloc_pages+0x74>
    // 如果list_next(le)) == &free_list说明已经遍历完了整个双向链表
    {
        // 此处le2page就是将le的地址-page_link 在Page的偏移，从而找到 Page 的地址
        struct Page *p = le2page(le, page_link);
  104694:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104697:	83 e8 0c             	sub    $0xc,%eax
  10469a:	89 45 ec             	mov    %eax,-0x14(%ebp)
        //说明找到可以满足的连续空闲内存了，让page等于p即可，退出循环
        if (p->property >= n) 
  10469d:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1046a0:	8b 40 08             	mov    0x8(%eax),%eax
  1046a3:	39 45 08             	cmp    %eax,0x8(%ebp)
  1046a6:	77 08                	ja     1046b0 <default_alloc_pages+0x74>
        {
            page = p;
  1046a8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1046ab:	89 45 f4             	mov    %eax,-0xc(%ebp)
            break;
  1046ae:	eb 18                	jmp    1046c8 <default_alloc_pages+0x8c>
  1046b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1046b3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return listelm->next;
  1046b6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  1046b9:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) 
  1046bc:	89 45 f0             	mov    %eax,-0x10(%ebp)
  1046bf:	81 7d f0 1c cf 11 00 	cmpl   $0x11cf1c,-0x10(%ebp)
  1046c6:	75 cc                	jne    104694 <default_alloc_pages+0x58>
        }
    }
    if (page != NULL) 
  1046c8:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  1046cc:	0f 84 ff 00 00 00    	je     1047d1 <default_alloc_pages+0x195>
    {
        //如果property>n的话，我们需要把多出的内存加到链表里
        if (page->property > n) 
  1046d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1046d5:	8b 40 08             	mov    0x8(%eax),%eax
  1046d8:	39 45 08             	cmp    %eax,0x8(%ebp)
  1046db:	0f 83 9c 00 00 00    	jae    10477d <default_alloc_pages+0x141>
        {
            //创建一个新的Page，起始地址为page+n
            struct Page *p = page + n; 
  1046e1:	8b 55 08             	mov    0x8(%ebp),%edx
  1046e4:	89 d0                	mov    %edx,%eax
  1046e6:	c1 e0 02             	shl    $0x2,%eax
  1046e9:	01 d0                	add    %edx,%eax
  1046eb:	c1 e0 02             	shl    $0x2,%eax
  1046ee:	89 c2                	mov    %eax,%edx
  1046f0:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1046f3:	01 d0                	add    %edx,%eax
  1046f5:	89 45 e8             	mov    %eax,-0x18(%ebp)
            p->property = page->property - n;  
  1046f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1046fb:	8b 40 08             	mov    0x8(%eax),%eax
  1046fe:	2b 45 08             	sub    0x8(%ebp),%eax
  104701:	89 c2                	mov    %eax,%edx
  104703:	8b 45 e8             	mov    -0x18(%ebp),%eax
  104706:	89 50 08             	mov    %edx,0x8(%eax)
            //将其property设置为page->property-n
            SetPageProperty(p);   
  104709:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10470c:	83 c0 04             	add    $0x4,%eax
  10470f:	c7 45 c4 01 00 00 00 	movl   $0x1,-0x3c(%ebp)
  104716:	89 45 c0             	mov    %eax,-0x40(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  104719:	8b 45 c0             	mov    -0x40(%ebp),%eax
  10471c:	8b 55 c4             	mov    -0x3c(%ebp),%edx
  10471f:	0f ab 10             	bts    %edx,(%eax)
}
  104722:	90                   	nop
            // 将多出来的插入到被分配掉的页块后面
            list_add(&(page->page_link), &(p->page_link));
  104723:	8b 45 e8             	mov    -0x18(%ebp),%eax
  104726:	83 c0 0c             	add    $0xc,%eax
  104729:	8b 55 f4             	mov    -0xc(%ebp),%edx
  10472c:	83 c2 0c             	add    $0xc,%edx
  10472f:	89 55 e0             	mov    %edx,-0x20(%ebp)
  104732:	89 45 dc             	mov    %eax,-0x24(%ebp)
  104735:	8b 45 e0             	mov    -0x20(%ebp),%eax
  104738:	89 45 d8             	mov    %eax,-0x28(%ebp)
  10473b:	8b 45 dc             	mov    -0x24(%ebp),%eax
  10473e:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    __list_add(elm, listelm, listelm->next);
  104741:	8b 45 d8             	mov    -0x28(%ebp),%eax
  104744:	8b 40 04             	mov    0x4(%eax),%eax
  104747:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  10474a:	89 55 d0             	mov    %edx,-0x30(%ebp)
  10474d:	8b 55 d8             	mov    -0x28(%ebp),%edx
  104750:	89 55 cc             	mov    %edx,-0x34(%ebp)
  104753:	89 45 c8             	mov    %eax,-0x38(%ebp)
    prev->next = next->prev = elm;
  104756:	8b 45 c8             	mov    -0x38(%ebp),%eax
  104759:	8b 55 d0             	mov    -0x30(%ebp),%edx
  10475c:	89 10                	mov    %edx,(%eax)
  10475e:	8b 45 c8             	mov    -0x38(%ebp),%eax
  104761:	8b 10                	mov    (%eax),%edx
  104763:	8b 45 cc             	mov    -0x34(%ebp),%eax
  104766:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
  104769:	8b 45 d0             	mov    -0x30(%ebp),%eax
  10476c:	8b 55 c8             	mov    -0x38(%ebp),%edx
  10476f:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
  104772:	8b 45 d0             	mov    -0x30(%ebp),%eax
  104775:	8b 55 cc             	mov    -0x34(%ebp),%edx
  104778:	89 10                	mov    %edx,(%eax)
}
  10477a:	90                   	nop
}
  10477b:	90                   	nop
}
  10477c:	90                   	nop
        }
        // 在空闲页链表中删去刚才分配的空闲页
        list_del(&(page->page_link));
  10477d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104780:	83 c0 0c             	add    $0xc,%eax
  104783:	89 45 b4             	mov    %eax,-0x4c(%ebp)
    __list_del(listelm->prev, listelm->next);
  104786:	8b 45 b4             	mov    -0x4c(%ebp),%eax
  104789:	8b 40 04             	mov    0x4(%eax),%eax
  10478c:	8b 55 b4             	mov    -0x4c(%ebp),%edx
  10478f:	8b 12                	mov    (%edx),%edx
  104791:	89 55 b0             	mov    %edx,-0x50(%ebp)
  104794:	89 45 ac             	mov    %eax,-0x54(%ebp)
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 * */
static inline void
__list_del(list_entry_t *prev, list_entry_t *next) {
    prev->next = next;
  104797:	8b 45 b0             	mov    -0x50(%ebp),%eax
  10479a:	8b 55 ac             	mov    -0x54(%ebp),%edx
  10479d:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
  1047a0:	8b 45 ac             	mov    -0x54(%ebp),%eax
  1047a3:	8b 55 b0             	mov    -0x50(%ebp),%edx
  1047a6:	89 10                	mov    %edx,(%eax)
}
  1047a8:	90                   	nop
}
  1047a9:	90                   	nop
        //因为分配了n个内存，故nr_free-n即可
        nr_free -= n;
  1047aa:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  1047af:	2b 45 08             	sub    0x8(%ebp),%eax
  1047b2:	a3 24 cf 11 00       	mov    %eax,0x11cf24
        ClearPageProperty(page);
  1047b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1047ba:	83 c0 04             	add    $0x4,%eax
  1047bd:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%ebp)
  1047c4:	89 45 b8             	mov    %eax,-0x48(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  1047c7:	8b 45 b8             	mov    -0x48(%ebp),%eax
  1047ca:	8b 55 bc             	mov    -0x44(%ebp),%edx
  1047cd:	0f b3 10             	btr    %edx,(%eax)
}
  1047d0:	90                   	nop
    }
    //返回分配的内存
    return page;
  1047d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  1047d4:	c9                   	leave  
  1047d5:	c3                   	ret    

001047d6 <default_free_pages>:

//释放掉n个页块，释放后也要考虑释放的块是否和已有的空闲块是紧挨着的，也就是可以合并的
//如果可以合并，则合并，否则直接加入双向链表

static void default_free_pages(struct Page *base, size_t n) 
{
  1047d6:	f3 0f 1e fb          	endbr32 
  1047da:	55                   	push   %ebp
  1047db:	89 e5                	mov    %esp,%ebp
  1047dd:	81 ec 98 00 00 00    	sub    $0x98,%esp
    assert(n > 0);  //n必须大于0
  1047e3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  1047e7:	75 24                	jne    10480d <default_free_pages+0x37>
  1047e9:	c7 44 24 0c d8 6f 10 	movl   $0x106fd8,0xc(%esp)
  1047f0:	00 
  1047f1:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1047f8:	00 
  1047f9:	c7 44 24 04 b8 00 00 	movl   $0xb8,0x4(%esp)
  104800:	00 
  104801:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104808:	e8 19 bc ff ff       	call   100426 <__panic>
    struct Page *p = base;  
  10480d:	8b 45 08             	mov    0x8(%ebp),%eax
  104810:	89 45 f4             	mov    %eax,-0xc(%ebp)
    //首先将base-->base+n之间的内存的标记以及ref初始化
    for (; p != base + n; p ++)
  104813:	e9 9d 00 00 00       	jmp    1048b5 <default_free_pages+0xdf>
     {
        assert(!PageReserved(p) && !PageProperty(p));
  104818:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10481b:	83 c0 04             	add    $0x4,%eax
  10481e:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  104825:	89 45 e8             	mov    %eax,-0x18(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  104828:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10482b:	8b 55 ec             	mov    -0x14(%ebp),%edx
  10482e:	0f a3 10             	bt     %edx,(%eax)
  104831:	19 c0                	sbb    %eax,%eax
  104833:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    return oldbit != 0;
  104836:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  10483a:	0f 95 c0             	setne  %al
  10483d:	0f b6 c0             	movzbl %al,%eax
  104840:	85 c0                	test   %eax,%eax
  104842:	75 2c                	jne    104870 <default_free_pages+0x9a>
  104844:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104847:	83 c0 04             	add    $0x4,%eax
  10484a:	c7 45 e0 01 00 00 00 	movl   $0x1,-0x20(%ebp)
  104851:	89 45 dc             	mov    %eax,-0x24(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  104854:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104857:	8b 55 e0             	mov    -0x20(%ebp),%edx
  10485a:	0f a3 10             	bt     %edx,(%eax)
  10485d:	19 c0                	sbb    %eax,%eax
  10485f:	89 45 d8             	mov    %eax,-0x28(%ebp)
    return oldbit != 0;
  104862:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
  104866:	0f 95 c0             	setne  %al
  104869:	0f b6 c0             	movzbl %al,%eax
  10486c:	85 c0                	test   %eax,%eax
  10486e:	74 24                	je     104894 <default_free_pages+0xbe>
  104870:	c7 44 24 0c 1c 70 10 	movl   $0x10701c,0xc(%esp)
  104877:	00 
  104878:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10487f:	00 
  104880:	c7 44 24 04 bd 00 00 	movl   $0xbd,0x4(%esp)
  104887:	00 
  104888:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10488f:	e8 92 bb ff ff       	call   100426 <__panic>
        //将flags和ref设为0
        p->flags = 0; 
  104894:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104897:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
        set_page_ref(p, 0);
  10489e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
  1048a5:	00 
  1048a6:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1048a9:	89 04 24             	mov    %eax,(%esp)
  1048ac:	e8 f4 fb ff ff       	call   1044a5 <set_page_ref>
    for (; p != base + n; p ++)
  1048b1:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
  1048b5:	8b 55 0c             	mov    0xc(%ebp),%edx
  1048b8:	89 d0                	mov    %edx,%eax
  1048ba:	c1 e0 02             	shl    $0x2,%eax
  1048bd:	01 d0                	add    %edx,%eax
  1048bf:	c1 e0 02             	shl    $0x2,%eax
  1048c2:	89 c2                	mov    %eax,%edx
  1048c4:	8b 45 08             	mov    0x8(%ebp),%eax
  1048c7:	01 d0                	add    %edx,%eax
  1048c9:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  1048cc:	0f 85 46 ff ff ff    	jne    104818 <default_free_pages+0x42>
    }
    //释放完毕后先将这一块的property改为n
    base->property = n;
  1048d2:	8b 45 08             	mov    0x8(%ebp),%eax
  1048d5:	8b 55 0c             	mov    0xc(%ebp),%edx
  1048d8:	89 50 08             	mov    %edx,0x8(%eax)
    SetPageProperty(base);
  1048db:	8b 45 08             	mov    0x8(%ebp),%eax
  1048de:	83 c0 04             	add    $0x4,%eax
  1048e1:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
  1048e8:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btsl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  1048eb:	8b 45 cc             	mov    -0x34(%ebp),%eax
  1048ee:	8b 55 d0             	mov    -0x30(%ebp),%edx
  1048f1:	0f ab 10             	bts    %edx,(%eax)
}
  1048f4:	90                   	nop
  1048f5:	c7 45 d4 1c cf 11 00 	movl   $0x11cf1c,-0x2c(%ebp)
    return listelm->next;
  1048fc:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1048ff:	8b 40 04             	mov    0x4(%eax),%eax
    list_entry_t *le = list_next(&free_list);
  104902:	89 45 f0             	mov    %eax,-0x10(%ebp)

    // 检查能否将其合并到合适的页块中
    while (le != &free_list)
  104905:	e9 0e 01 00 00       	jmp    104a18 <default_free_pages+0x242>
    {
        p = le2page(le, page_link);
  10490a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  10490d:	83 e8 0c             	sub    $0xc,%eax
  104910:	89 45 f4             	mov    %eax,-0xc(%ebp)
  104913:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104916:	89 45 c8             	mov    %eax,-0x38(%ebp)
  104919:	8b 45 c8             	mov    -0x38(%ebp),%eax
  10491c:	8b 40 04             	mov    0x4(%eax),%eax
        le = list_next(le);
  10491f:	89 45 f0             	mov    %eax,-0x10(%ebp)
        //如果这个块在下一个空闲块前面，二者可以合并
        if (base + base->property == p) 
  104922:	8b 45 08             	mov    0x8(%ebp),%eax
  104925:	8b 50 08             	mov    0x8(%eax),%edx
  104928:	89 d0                	mov    %edx,%eax
  10492a:	c1 e0 02             	shl    $0x2,%eax
  10492d:	01 d0                	add    %edx,%eax
  10492f:	c1 e0 02             	shl    $0x2,%eax
  104932:	89 c2                	mov    %eax,%edx
  104934:	8b 45 08             	mov    0x8(%ebp),%eax
  104937:	01 d0                	add    %edx,%eax
  104939:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  10493c:	75 5d                	jne    10499b <default_free_pages+0x1c5>
        {
            //让base的property等于两个块的大小之和
            base->property += p->property;
  10493e:	8b 45 08             	mov    0x8(%ebp),%eax
  104941:	8b 50 08             	mov    0x8(%eax),%edx
  104944:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104947:	8b 40 08             	mov    0x8(%eax),%eax
  10494a:	01 c2                	add    %eax,%edx
  10494c:	8b 45 08             	mov    0x8(%ebp),%eax
  10494f:	89 50 08             	mov    %edx,0x8(%eax)
            //将另一个空闲块删除即可
            ClearPageProperty(p);
  104952:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104955:	83 c0 04             	add    $0x4,%eax
  104958:	c7 45 b8 01 00 00 00 	movl   $0x1,-0x48(%ebp)
  10495f:	89 45 b4             	mov    %eax,-0x4c(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  104962:	8b 45 b4             	mov    -0x4c(%ebp),%eax
  104965:	8b 55 b8             	mov    -0x48(%ebp),%edx
  104968:	0f b3 10             	btr    %edx,(%eax)
}
  10496b:	90                   	nop
            list_del(&(p->page_link));
  10496c:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10496f:	83 c0 0c             	add    $0xc,%eax
  104972:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    __list_del(listelm->prev, listelm->next);
  104975:	8b 45 c4             	mov    -0x3c(%ebp),%eax
  104978:	8b 40 04             	mov    0x4(%eax),%eax
  10497b:	8b 55 c4             	mov    -0x3c(%ebp),%edx
  10497e:	8b 12                	mov    (%edx),%edx
  104980:	89 55 c0             	mov    %edx,-0x40(%ebp)
  104983:	89 45 bc             	mov    %eax,-0x44(%ebp)
    prev->next = next;
  104986:	8b 45 c0             	mov    -0x40(%ebp),%eax
  104989:	8b 55 bc             	mov    -0x44(%ebp),%edx
  10498c:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
  10498f:	8b 45 bc             	mov    -0x44(%ebp),%eax
  104992:	8b 55 c0             	mov    -0x40(%ebp),%edx
  104995:	89 10                	mov    %edx,(%eax)
}
  104997:	90                   	nop
}
  104998:	90                   	nop
  104999:	eb 7d                	jmp    104a18 <default_free_pages+0x242>
        }
        //如果这个块在上一个空闲块的后面，二者可以合并
        else if (p + p->property == base) 
  10499b:	8b 45 f4             	mov    -0xc(%ebp),%eax
  10499e:	8b 50 08             	mov    0x8(%eax),%edx
  1049a1:	89 d0                	mov    %edx,%eax
  1049a3:	c1 e0 02             	shl    $0x2,%eax
  1049a6:	01 d0                	add    %edx,%eax
  1049a8:	c1 e0 02             	shl    $0x2,%eax
  1049ab:	89 c2                	mov    %eax,%edx
  1049ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1049b0:	01 d0                	add    %edx,%eax
  1049b2:	39 45 08             	cmp    %eax,0x8(%ebp)
  1049b5:	75 61                	jne    104a18 <default_free_pages+0x242>
        {
            //将p的property设置为二者之和
            p->property += base->property;
  1049b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1049ba:	8b 50 08             	mov    0x8(%eax),%edx
  1049bd:	8b 45 08             	mov    0x8(%ebp),%eax
  1049c0:	8b 40 08             	mov    0x8(%eax),%eax
  1049c3:	01 c2                	add    %eax,%edx
  1049c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1049c8:	89 50 08             	mov    %edx,0x8(%eax)
            //将后面的空闲块删除即可
            ClearPageProperty(base);
  1049cb:	8b 45 08             	mov    0x8(%ebp),%eax
  1049ce:	83 c0 04             	add    $0x4,%eax
  1049d1:	c7 45 a4 01 00 00 00 	movl   $0x1,-0x5c(%ebp)
  1049d8:	89 45 a0             	mov    %eax,-0x60(%ebp)
    asm volatile ("btrl %1, %0" :"=m" (*(volatile long *)addr) : "Ir" (nr));
  1049db:	8b 45 a0             	mov    -0x60(%ebp),%eax
  1049de:	8b 55 a4             	mov    -0x5c(%ebp),%edx
  1049e1:	0f b3 10             	btr    %edx,(%eax)
}
  1049e4:	90                   	nop
            base = p;
  1049e5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1049e8:	89 45 08             	mov    %eax,0x8(%ebp)
            //注意这里需要把p删除，因为之后再次去确认插入的位置
            list_del(&(p->page_link));
  1049eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1049ee:	83 c0 0c             	add    $0xc,%eax
  1049f1:	89 45 b0             	mov    %eax,-0x50(%ebp)
    __list_del(listelm->prev, listelm->next);
  1049f4:	8b 45 b0             	mov    -0x50(%ebp),%eax
  1049f7:	8b 40 04             	mov    0x4(%eax),%eax
  1049fa:	8b 55 b0             	mov    -0x50(%ebp),%edx
  1049fd:	8b 12                	mov    (%edx),%edx
  1049ff:	89 55 ac             	mov    %edx,-0x54(%ebp)
  104a02:	89 45 a8             	mov    %eax,-0x58(%ebp)
    prev->next = next;
  104a05:	8b 45 ac             	mov    -0x54(%ebp),%eax
  104a08:	8b 55 a8             	mov    -0x58(%ebp),%edx
  104a0b:	89 50 04             	mov    %edx,0x4(%eax)
    next->prev = prev;
  104a0e:	8b 45 a8             	mov    -0x58(%ebp),%eax
  104a11:	8b 55 ac             	mov    -0x54(%ebp),%edx
  104a14:	89 10                	mov    %edx,(%eax)
}
  104a16:	90                   	nop
}
  104a17:	90                   	nop
    while (le != &free_list)
  104a18:	81 7d f0 1c cf 11 00 	cmpl   $0x11cf1c,-0x10(%ebp)
  104a1f:	0f 85 e5 fe ff ff    	jne    10490a <default_free_pages+0x134>
        }
    }
    //整体上空闲空间增大了n
    nr_free += n;
  104a25:	8b 15 24 cf 11 00    	mov    0x11cf24,%edx
  104a2b:	8b 45 0c             	mov    0xc(%ebp),%eax
  104a2e:	01 d0                	add    %edx,%eax
  104a30:	a3 24 cf 11 00       	mov    %eax,0x11cf24
  104a35:	c7 45 9c 1c cf 11 00 	movl   $0x11cf1c,-0x64(%ebp)
    return listelm->next;
  104a3c:	8b 45 9c             	mov    -0x64(%ebp),%eax
  104a3f:	8b 40 04             	mov    0x4(%eax),%eax
    le = list_next(&free_list);
  104a42:	89 45 f0             	mov    %eax,-0x10(%ebp)
    // 将合并好的合适的页块添加回空闲页块链表
    //因为需要按照内存从小到大的顺序排列列表，故需要找到应该插入的位置
    while (le != &free_list) 
  104a45:	eb 34                	jmp    104a7b <default_free_pages+0x2a5>
    {
        p = le2page(le, page_link);
  104a47:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a4a:	83 e8 0c             	sub    $0xc,%eax
  104a4d:	89 45 f4             	mov    %eax,-0xc(%ebp)
        //找到正确的位置：
        if (base + base->property <= p)
  104a50:	8b 45 08             	mov    0x8(%ebp),%eax
  104a53:	8b 50 08             	mov    0x8(%eax),%edx
  104a56:	89 d0                	mov    %edx,%eax
  104a58:	c1 e0 02             	shl    $0x2,%eax
  104a5b:	01 d0                	add    %edx,%eax
  104a5d:	c1 e0 02             	shl    $0x2,%eax
  104a60:	89 c2                	mov    %eax,%edx
  104a62:	8b 45 08             	mov    0x8(%ebp),%eax
  104a65:	01 d0                	add    %edx,%eax
  104a67:	39 45 f4             	cmp    %eax,-0xc(%ebp)
  104a6a:	73 1a                	jae    104a86 <default_free_pages+0x2b0>
  104a6c:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a6f:	89 45 98             	mov    %eax,-0x68(%ebp)
  104a72:	8b 45 98             	mov    -0x68(%ebp),%eax
  104a75:	8b 40 04             	mov    0x4(%eax),%eax
        {
            break;
        }
        //否则链表项向后，继续查找
        le = list_next(le);
  104a78:	89 45 f0             	mov    %eax,-0x10(%ebp)
    while (le != &free_list) 
  104a7b:	81 7d f0 1c cf 11 00 	cmpl   $0x11cf1c,-0x10(%ebp)
  104a82:	75 c3                	jne    104a47 <default_free_pages+0x271>
  104a84:	eb 01                	jmp    104a87 <default_free_pages+0x2b1>
            break;
  104a86:	90                   	nop
    }
    //将base插入到刚才找到的正确位置即可
    list_add_before(le, &(base->page_link));
  104a87:	8b 45 08             	mov    0x8(%ebp),%eax
  104a8a:	8d 50 0c             	lea    0xc(%eax),%edx
  104a8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104a90:	89 45 94             	mov    %eax,-0x6c(%ebp)
  104a93:	89 55 90             	mov    %edx,-0x70(%ebp)
    __list_add(elm, listelm->prev, listelm);
  104a96:	8b 45 94             	mov    -0x6c(%ebp),%eax
  104a99:	8b 00                	mov    (%eax),%eax
  104a9b:	8b 55 90             	mov    -0x70(%ebp),%edx
  104a9e:	89 55 8c             	mov    %edx,-0x74(%ebp)
  104aa1:	89 45 88             	mov    %eax,-0x78(%ebp)
  104aa4:	8b 45 94             	mov    -0x6c(%ebp),%eax
  104aa7:	89 45 84             	mov    %eax,-0x7c(%ebp)
    prev->next = next->prev = elm;
  104aaa:	8b 45 84             	mov    -0x7c(%ebp),%eax
  104aad:	8b 55 8c             	mov    -0x74(%ebp),%edx
  104ab0:	89 10                	mov    %edx,(%eax)
  104ab2:	8b 45 84             	mov    -0x7c(%ebp),%eax
  104ab5:	8b 10                	mov    (%eax),%edx
  104ab7:	8b 45 88             	mov    -0x78(%ebp),%eax
  104aba:	89 50 04             	mov    %edx,0x4(%eax)
    elm->next = next;
  104abd:	8b 45 8c             	mov    -0x74(%ebp),%eax
  104ac0:	8b 55 84             	mov    -0x7c(%ebp),%edx
  104ac3:	89 50 04             	mov    %edx,0x4(%eax)
    elm->prev = prev;
  104ac6:	8b 45 8c             	mov    -0x74(%ebp),%eax
  104ac9:	8b 55 88             	mov    -0x78(%ebp),%edx
  104acc:	89 10                	mov    %edx,(%eax)
}
  104ace:	90                   	nop
}
  104acf:	90                   	nop
}
  104ad0:	90                   	nop
  104ad1:	c9                   	leave  
  104ad2:	c3                   	ret    

00104ad3 <default_nr_free_pages>:


static size_t
default_nr_free_pages(void) {
  104ad3:	f3 0f 1e fb          	endbr32 
  104ad7:	55                   	push   %ebp
  104ad8:	89 e5                	mov    %esp,%ebp
    return nr_free;
  104ada:	a1 24 cf 11 00       	mov    0x11cf24,%eax
}
  104adf:	5d                   	pop    %ebp
  104ae0:	c3                   	ret    

00104ae1 <basic_check>:

static void
basic_check(void) {
  104ae1:	f3 0f 1e fb          	endbr32 
  104ae5:	55                   	push   %ebp
  104ae6:	89 e5                	mov    %esp,%ebp
  104ae8:	83 ec 48             	sub    $0x48,%esp
    struct Page *p0, *p1, *p2;
    p0 = p1 = p2 = NULL;
  104aeb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  104af2:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104af5:	89 45 f0             	mov    %eax,-0x10(%ebp)
  104af8:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104afb:	89 45 ec             	mov    %eax,-0x14(%ebp)
    assert((p0 = alloc_page()) != NULL);
  104afe:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104b05:	e8 2e e2 ff ff       	call   102d38 <alloc_pages>
  104b0a:	89 45 ec             	mov    %eax,-0x14(%ebp)
  104b0d:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  104b11:	75 24                	jne    104b37 <basic_check+0x56>
  104b13:	c7 44 24 0c 41 70 10 	movl   $0x107041,0xc(%esp)
  104b1a:	00 
  104b1b:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104b22:	00 
  104b23:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
  104b2a:	00 
  104b2b:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104b32:	e8 ef b8 ff ff       	call   100426 <__panic>
    assert((p1 = alloc_page()) != NULL);
  104b37:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104b3e:	e8 f5 e1 ff ff       	call   102d38 <alloc_pages>
  104b43:	89 45 f0             	mov    %eax,-0x10(%ebp)
  104b46:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  104b4a:	75 24                	jne    104b70 <basic_check+0x8f>
  104b4c:	c7 44 24 0c 5d 70 10 	movl   $0x10705d,0xc(%esp)
  104b53:	00 
  104b54:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104b5b:	00 
  104b5c:	c7 44 24 04 00 01 00 	movl   $0x100,0x4(%esp)
  104b63:	00 
  104b64:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104b6b:	e8 b6 b8 ff ff       	call   100426 <__panic>
    assert((p2 = alloc_page()) != NULL);
  104b70:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104b77:	e8 bc e1 ff ff       	call   102d38 <alloc_pages>
  104b7c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  104b7f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  104b83:	75 24                	jne    104ba9 <basic_check+0xc8>
  104b85:	c7 44 24 0c 79 70 10 	movl   $0x107079,0xc(%esp)
  104b8c:	00 
  104b8d:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104b94:	00 
  104b95:	c7 44 24 04 01 01 00 	movl   $0x101,0x4(%esp)
  104b9c:	00 
  104b9d:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104ba4:	e8 7d b8 ff ff       	call   100426 <__panic>

    assert(p0 != p1 && p0 != p2 && p1 != p2);
  104ba9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104bac:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  104baf:	74 10                	je     104bc1 <basic_check+0xe0>
  104bb1:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104bb4:	3b 45 f4             	cmp    -0xc(%ebp),%eax
  104bb7:	74 08                	je     104bc1 <basic_check+0xe0>
  104bb9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104bbc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
  104bbf:	75 24                	jne    104be5 <basic_check+0x104>
  104bc1:	c7 44 24 0c 98 70 10 	movl   $0x107098,0xc(%esp)
  104bc8:	00 
  104bc9:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104bd0:	00 
  104bd1:	c7 44 24 04 03 01 00 	movl   $0x103,0x4(%esp)
  104bd8:	00 
  104bd9:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104be0:	e8 41 b8 ff ff       	call   100426 <__panic>
    assert(page_ref(p0) == 0 && page_ref(p1) == 0 && page_ref(p2) == 0);
  104be5:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104be8:	89 04 24             	mov    %eax,(%esp)
  104beb:	e8 ab f8 ff ff       	call   10449b <page_ref>
  104bf0:	85 c0                	test   %eax,%eax
  104bf2:	75 1e                	jne    104c12 <basic_check+0x131>
  104bf4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104bf7:	89 04 24             	mov    %eax,(%esp)
  104bfa:	e8 9c f8 ff ff       	call   10449b <page_ref>
  104bff:	85 c0                	test   %eax,%eax
  104c01:	75 0f                	jne    104c12 <basic_check+0x131>
  104c03:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104c06:	89 04 24             	mov    %eax,(%esp)
  104c09:	e8 8d f8 ff ff       	call   10449b <page_ref>
  104c0e:	85 c0                	test   %eax,%eax
  104c10:	74 24                	je     104c36 <basic_check+0x155>
  104c12:	c7 44 24 0c bc 70 10 	movl   $0x1070bc,0xc(%esp)
  104c19:	00 
  104c1a:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104c21:	00 
  104c22:	c7 44 24 04 04 01 00 	movl   $0x104,0x4(%esp)
  104c29:	00 
  104c2a:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104c31:	e8 f0 b7 ff ff       	call   100426 <__panic>

    assert(page2pa(p0) < npage * PGSIZE);
  104c36:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104c39:	89 04 24             	mov    %eax,(%esp)
  104c3c:	e8 44 f8 ff ff       	call   104485 <page2pa>
  104c41:	8b 15 80 ce 11 00    	mov    0x11ce80,%edx
  104c47:	c1 e2 0c             	shl    $0xc,%edx
  104c4a:	39 d0                	cmp    %edx,%eax
  104c4c:	72 24                	jb     104c72 <basic_check+0x191>
  104c4e:	c7 44 24 0c f8 70 10 	movl   $0x1070f8,0xc(%esp)
  104c55:	00 
  104c56:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104c5d:	00 
  104c5e:	c7 44 24 04 06 01 00 	movl   $0x106,0x4(%esp)
  104c65:	00 
  104c66:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104c6d:	e8 b4 b7 ff ff       	call   100426 <__panic>
    assert(page2pa(p1) < npage * PGSIZE);
  104c72:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104c75:	89 04 24             	mov    %eax,(%esp)
  104c78:	e8 08 f8 ff ff       	call   104485 <page2pa>
  104c7d:	8b 15 80 ce 11 00    	mov    0x11ce80,%edx
  104c83:	c1 e2 0c             	shl    $0xc,%edx
  104c86:	39 d0                	cmp    %edx,%eax
  104c88:	72 24                	jb     104cae <basic_check+0x1cd>
  104c8a:	c7 44 24 0c 15 71 10 	movl   $0x107115,0xc(%esp)
  104c91:	00 
  104c92:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104c99:	00 
  104c9a:	c7 44 24 04 07 01 00 	movl   $0x107,0x4(%esp)
  104ca1:	00 
  104ca2:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104ca9:	e8 78 b7 ff ff       	call   100426 <__panic>
    assert(page2pa(p2) < npage * PGSIZE);
  104cae:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104cb1:	89 04 24             	mov    %eax,(%esp)
  104cb4:	e8 cc f7 ff ff       	call   104485 <page2pa>
  104cb9:	8b 15 80 ce 11 00    	mov    0x11ce80,%edx
  104cbf:	c1 e2 0c             	shl    $0xc,%edx
  104cc2:	39 d0                	cmp    %edx,%eax
  104cc4:	72 24                	jb     104cea <basic_check+0x209>
  104cc6:	c7 44 24 0c 32 71 10 	movl   $0x107132,0xc(%esp)
  104ccd:	00 
  104cce:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104cd5:	00 
  104cd6:	c7 44 24 04 08 01 00 	movl   $0x108,0x4(%esp)
  104cdd:	00 
  104cde:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104ce5:	e8 3c b7 ff ff       	call   100426 <__panic>

    list_entry_t free_list_store = free_list;
  104cea:	a1 1c cf 11 00       	mov    0x11cf1c,%eax
  104cef:	8b 15 20 cf 11 00    	mov    0x11cf20,%edx
  104cf5:	89 45 d0             	mov    %eax,-0x30(%ebp)
  104cf8:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  104cfb:	c7 45 dc 1c cf 11 00 	movl   $0x11cf1c,-0x24(%ebp)
    elm->prev = elm->next = elm;
  104d02:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104d05:	8b 55 dc             	mov    -0x24(%ebp),%edx
  104d08:	89 50 04             	mov    %edx,0x4(%eax)
  104d0b:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104d0e:	8b 50 04             	mov    0x4(%eax),%edx
  104d11:	8b 45 dc             	mov    -0x24(%ebp),%eax
  104d14:	89 10                	mov    %edx,(%eax)
}
  104d16:	90                   	nop
  104d17:	c7 45 e0 1c cf 11 00 	movl   $0x11cf1c,-0x20(%ebp)
    return list->next == list;
  104d1e:	8b 45 e0             	mov    -0x20(%ebp),%eax
  104d21:	8b 40 04             	mov    0x4(%eax),%eax
  104d24:	39 45 e0             	cmp    %eax,-0x20(%ebp)
  104d27:	0f 94 c0             	sete   %al
  104d2a:	0f b6 c0             	movzbl %al,%eax
    list_init(&free_list);
    assert(list_empty(&free_list));
  104d2d:	85 c0                	test   %eax,%eax
  104d2f:	75 24                	jne    104d55 <basic_check+0x274>
  104d31:	c7 44 24 0c 4f 71 10 	movl   $0x10714f,0xc(%esp)
  104d38:	00 
  104d39:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104d40:	00 
  104d41:	c7 44 24 04 0c 01 00 	movl   $0x10c,0x4(%esp)
  104d48:	00 
  104d49:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104d50:	e8 d1 b6 ff ff       	call   100426 <__panic>

    unsigned int nr_free_store = nr_free;
  104d55:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  104d5a:	89 45 e8             	mov    %eax,-0x18(%ebp)
    nr_free = 0;
  104d5d:	c7 05 24 cf 11 00 00 	movl   $0x0,0x11cf24
  104d64:	00 00 00 

    assert(alloc_page() == NULL);
  104d67:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104d6e:	e8 c5 df ff ff       	call   102d38 <alloc_pages>
  104d73:	85 c0                	test   %eax,%eax
  104d75:	74 24                	je     104d9b <basic_check+0x2ba>
  104d77:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  104d7e:	00 
  104d7f:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104d86:	00 
  104d87:	c7 44 24 04 11 01 00 	movl   $0x111,0x4(%esp)
  104d8e:	00 
  104d8f:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104d96:	e8 8b b6 ff ff       	call   100426 <__panic>

    free_page(p0);
  104d9b:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  104da2:	00 
  104da3:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104da6:	89 04 24             	mov    %eax,(%esp)
  104da9:	e8 c6 df ff ff       	call   102d74 <free_pages>
    free_page(p1);
  104dae:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  104db5:	00 
  104db6:	8b 45 f0             	mov    -0x10(%ebp),%eax
  104db9:	89 04 24             	mov    %eax,(%esp)
  104dbc:	e8 b3 df ff ff       	call   102d74 <free_pages>
    free_page(p2);
  104dc1:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  104dc8:	00 
  104dc9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  104dcc:	89 04 24             	mov    %eax,(%esp)
  104dcf:	e8 a0 df ff ff       	call   102d74 <free_pages>
    assert(nr_free == 3);
  104dd4:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  104dd9:	83 f8 03             	cmp    $0x3,%eax
  104ddc:	74 24                	je     104e02 <basic_check+0x321>
  104dde:	c7 44 24 0c 7b 71 10 	movl   $0x10717b,0xc(%esp)
  104de5:	00 
  104de6:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104ded:	00 
  104dee:	c7 44 24 04 16 01 00 	movl   $0x116,0x4(%esp)
  104df5:	00 
  104df6:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104dfd:	e8 24 b6 ff ff       	call   100426 <__panic>

    assert((p0 = alloc_page()) != NULL);
  104e02:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104e09:	e8 2a df ff ff       	call   102d38 <alloc_pages>
  104e0e:	89 45 ec             	mov    %eax,-0x14(%ebp)
  104e11:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
  104e15:	75 24                	jne    104e3b <basic_check+0x35a>
  104e17:	c7 44 24 0c 41 70 10 	movl   $0x107041,0xc(%esp)
  104e1e:	00 
  104e1f:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104e26:	00 
  104e27:	c7 44 24 04 18 01 00 	movl   $0x118,0x4(%esp)
  104e2e:	00 
  104e2f:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104e36:	e8 eb b5 ff ff       	call   100426 <__panic>
    assert((p1 = alloc_page()) != NULL);
  104e3b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104e42:	e8 f1 de ff ff       	call   102d38 <alloc_pages>
  104e47:	89 45 f0             	mov    %eax,-0x10(%ebp)
  104e4a:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  104e4e:	75 24                	jne    104e74 <basic_check+0x393>
  104e50:	c7 44 24 0c 5d 70 10 	movl   $0x10705d,0xc(%esp)
  104e57:	00 
  104e58:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104e5f:	00 
  104e60:	c7 44 24 04 19 01 00 	movl   $0x119,0x4(%esp)
  104e67:	00 
  104e68:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104e6f:	e8 b2 b5 ff ff       	call   100426 <__panic>
    assert((p2 = alloc_page()) != NULL);
  104e74:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104e7b:	e8 b8 de ff ff       	call   102d38 <alloc_pages>
  104e80:	89 45 f4             	mov    %eax,-0xc(%ebp)
  104e83:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  104e87:	75 24                	jne    104ead <basic_check+0x3cc>
  104e89:	c7 44 24 0c 79 70 10 	movl   $0x107079,0xc(%esp)
  104e90:	00 
  104e91:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104e98:	00 
  104e99:	c7 44 24 04 1a 01 00 	movl   $0x11a,0x4(%esp)
  104ea0:	00 
  104ea1:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104ea8:	e8 79 b5 ff ff       	call   100426 <__panic>

    assert(alloc_page() == NULL);
  104ead:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104eb4:	e8 7f de ff ff       	call   102d38 <alloc_pages>
  104eb9:	85 c0                	test   %eax,%eax
  104ebb:	74 24                	je     104ee1 <basic_check+0x400>
  104ebd:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  104ec4:	00 
  104ec5:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104ecc:	00 
  104ecd:	c7 44 24 04 1c 01 00 	movl   $0x11c,0x4(%esp)
  104ed4:	00 
  104ed5:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104edc:	e8 45 b5 ff ff       	call   100426 <__panic>

    free_page(p0);
  104ee1:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  104ee8:	00 
  104ee9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  104eec:	89 04 24             	mov    %eax,(%esp)
  104eef:	e8 80 de ff ff       	call   102d74 <free_pages>
  104ef4:	c7 45 d8 1c cf 11 00 	movl   $0x11cf1c,-0x28(%ebp)
  104efb:	8b 45 d8             	mov    -0x28(%ebp),%eax
  104efe:	8b 40 04             	mov    0x4(%eax),%eax
  104f01:	39 45 d8             	cmp    %eax,-0x28(%ebp)
  104f04:	0f 94 c0             	sete   %al
  104f07:	0f b6 c0             	movzbl %al,%eax
    assert(!list_empty(&free_list));
  104f0a:	85 c0                	test   %eax,%eax
  104f0c:	74 24                	je     104f32 <basic_check+0x451>
  104f0e:	c7 44 24 0c 88 71 10 	movl   $0x107188,0xc(%esp)
  104f15:	00 
  104f16:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104f1d:	00 
  104f1e:	c7 44 24 04 1f 01 00 	movl   $0x11f,0x4(%esp)
  104f25:	00 
  104f26:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104f2d:	e8 f4 b4 ff ff       	call   100426 <__panic>

    struct Page *p;
    assert((p = alloc_page()) == p0);
  104f32:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104f39:	e8 fa dd ff ff       	call   102d38 <alloc_pages>
  104f3e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  104f41:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  104f44:	3b 45 ec             	cmp    -0x14(%ebp),%eax
  104f47:	74 24                	je     104f6d <basic_check+0x48c>
  104f49:	c7 44 24 0c a0 71 10 	movl   $0x1071a0,0xc(%esp)
  104f50:	00 
  104f51:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104f58:	00 
  104f59:	c7 44 24 04 22 01 00 	movl   $0x122,0x4(%esp)
  104f60:	00 
  104f61:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104f68:	e8 b9 b4 ff ff       	call   100426 <__panic>
    assert(alloc_page() == NULL);
  104f6d:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  104f74:	e8 bf dd ff ff       	call   102d38 <alloc_pages>
  104f79:	85 c0                	test   %eax,%eax
  104f7b:	74 24                	je     104fa1 <basic_check+0x4c0>
  104f7d:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  104f84:	00 
  104f85:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104f8c:	00 
  104f8d:	c7 44 24 04 23 01 00 	movl   $0x123,0x4(%esp)
  104f94:	00 
  104f95:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104f9c:	e8 85 b4 ff ff       	call   100426 <__panic>

    assert(nr_free == 0);
  104fa1:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  104fa6:	85 c0                	test   %eax,%eax
  104fa8:	74 24                	je     104fce <basic_check+0x4ed>
  104faa:	c7 44 24 0c b9 71 10 	movl   $0x1071b9,0xc(%esp)
  104fb1:	00 
  104fb2:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  104fb9:	00 
  104fba:	c7 44 24 04 25 01 00 	movl   $0x125,0x4(%esp)
  104fc1:	00 
  104fc2:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  104fc9:	e8 58 b4 ff ff       	call   100426 <__panic>
    free_list = free_list_store;
  104fce:	8b 45 d0             	mov    -0x30(%ebp),%eax
  104fd1:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  104fd4:	a3 1c cf 11 00       	mov    %eax,0x11cf1c
  104fd9:	89 15 20 cf 11 00    	mov    %edx,0x11cf20
    nr_free = nr_free_store;
  104fdf:	8b 45 e8             	mov    -0x18(%ebp),%eax
  104fe2:	a3 24 cf 11 00       	mov    %eax,0x11cf24

    free_page(p);
  104fe7:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  104fee:	00 
  104fef:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  104ff2:	89 04 24             	mov    %eax,(%esp)
  104ff5:	e8 7a dd ff ff       	call   102d74 <free_pages>
    free_page(p1);
  104ffa:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  105001:	00 
  105002:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105005:	89 04 24             	mov    %eax,(%esp)
  105008:	e8 67 dd ff ff       	call   102d74 <free_pages>
    free_page(p2);
  10500d:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  105014:	00 
  105015:	8b 45 f4             	mov    -0xc(%ebp),%eax
  105018:	89 04 24             	mov    %eax,(%esp)
  10501b:	e8 54 dd ff ff       	call   102d74 <free_pages>
}
  105020:	90                   	nop
  105021:	c9                   	leave  
  105022:	c3                   	ret    

00105023 <default_check>:

// LAB2: below code is used to check the first fit allocation algorithm (your EXERCISE 1) 
// NOTICE: You SHOULD NOT CHANGE basic_check, default_check functions!
static void
default_check(void) {
  105023:	f3 0f 1e fb          	endbr32 
  105027:	55                   	push   %ebp
  105028:	89 e5                	mov    %esp,%ebp
  10502a:	81 ec 98 00 00 00    	sub    $0x98,%esp
    int count = 0, total = 0;
  105030:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  105037:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    list_entry_t *le = &free_list;
  10503e:	c7 45 ec 1c cf 11 00 	movl   $0x11cf1c,-0x14(%ebp)
    while ((le = list_next(le)) != &free_list) {
  105045:	eb 6a                	jmp    1050b1 <default_check+0x8e>
        struct Page *p = le2page(le, page_link);
  105047:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10504a:	83 e8 0c             	sub    $0xc,%eax
  10504d:	89 45 d4             	mov    %eax,-0x2c(%ebp)
        assert(PageProperty(p));
  105050:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  105053:	83 c0 04             	add    $0x4,%eax
  105056:	c7 45 d0 01 00 00 00 	movl   $0x1,-0x30(%ebp)
  10505d:	89 45 cc             	mov    %eax,-0x34(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  105060:	8b 45 cc             	mov    -0x34(%ebp),%eax
  105063:	8b 55 d0             	mov    -0x30(%ebp),%edx
  105066:	0f a3 10             	bt     %edx,(%eax)
  105069:	19 c0                	sbb    %eax,%eax
  10506b:	89 45 c8             	mov    %eax,-0x38(%ebp)
    return oldbit != 0;
  10506e:	83 7d c8 00          	cmpl   $0x0,-0x38(%ebp)
  105072:	0f 95 c0             	setne  %al
  105075:	0f b6 c0             	movzbl %al,%eax
  105078:	85 c0                	test   %eax,%eax
  10507a:	75 24                	jne    1050a0 <default_check+0x7d>
  10507c:	c7 44 24 0c c6 71 10 	movl   $0x1071c6,0xc(%esp)
  105083:	00 
  105084:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10508b:	00 
  10508c:	c7 44 24 04 36 01 00 	movl   $0x136,0x4(%esp)
  105093:	00 
  105094:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10509b:	e8 86 b3 ff ff       	call   100426 <__panic>
        count ++, total += p->property;
  1050a0:	ff 45 f4             	incl   -0xc(%ebp)
  1050a3:	8b 45 d4             	mov    -0x2c(%ebp),%eax
  1050a6:	8b 50 08             	mov    0x8(%eax),%edx
  1050a9:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1050ac:	01 d0                	add    %edx,%eax
  1050ae:	89 45 f0             	mov    %eax,-0x10(%ebp)
  1050b1:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1050b4:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    return listelm->next;
  1050b7:	8b 45 c4             	mov    -0x3c(%ebp),%eax
  1050ba:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) {
  1050bd:	89 45 ec             	mov    %eax,-0x14(%ebp)
  1050c0:	81 7d ec 1c cf 11 00 	cmpl   $0x11cf1c,-0x14(%ebp)
  1050c7:	0f 85 7a ff ff ff    	jne    105047 <default_check+0x24>
    }
    assert(total == nr_free_pages());
  1050cd:	e8 d9 dc ff ff       	call   102dab <nr_free_pages>
  1050d2:	8b 55 f0             	mov    -0x10(%ebp),%edx
  1050d5:	39 d0                	cmp    %edx,%eax
  1050d7:	74 24                	je     1050fd <default_check+0xda>
  1050d9:	c7 44 24 0c d6 71 10 	movl   $0x1071d6,0xc(%esp)
  1050e0:	00 
  1050e1:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1050e8:	00 
  1050e9:	c7 44 24 04 39 01 00 	movl   $0x139,0x4(%esp)
  1050f0:	00 
  1050f1:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1050f8:	e8 29 b3 ff ff       	call   100426 <__panic>

    basic_check();
  1050fd:	e8 df f9 ff ff       	call   104ae1 <basic_check>

    struct Page *p0 = alloc_pages(5), *p1, *p2;
  105102:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  105109:	e8 2a dc ff ff       	call   102d38 <alloc_pages>
  10510e:	89 45 e8             	mov    %eax,-0x18(%ebp)
    assert(p0 != NULL);
  105111:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105115:	75 24                	jne    10513b <default_check+0x118>
  105117:	c7 44 24 0c ef 71 10 	movl   $0x1071ef,0xc(%esp)
  10511e:	00 
  10511f:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105126:	00 
  105127:	c7 44 24 04 3e 01 00 	movl   $0x13e,0x4(%esp)
  10512e:	00 
  10512f:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105136:	e8 eb b2 ff ff       	call   100426 <__panic>
    assert(!PageProperty(p0));
  10513b:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10513e:	83 c0 04             	add    $0x4,%eax
  105141:	c7 45 c0 01 00 00 00 	movl   $0x1,-0x40(%ebp)
  105148:	89 45 bc             	mov    %eax,-0x44(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  10514b:	8b 45 bc             	mov    -0x44(%ebp),%eax
  10514e:	8b 55 c0             	mov    -0x40(%ebp),%edx
  105151:	0f a3 10             	bt     %edx,(%eax)
  105154:	19 c0                	sbb    %eax,%eax
  105156:	89 45 b8             	mov    %eax,-0x48(%ebp)
    return oldbit != 0;
  105159:	83 7d b8 00          	cmpl   $0x0,-0x48(%ebp)
  10515d:	0f 95 c0             	setne  %al
  105160:	0f b6 c0             	movzbl %al,%eax
  105163:	85 c0                	test   %eax,%eax
  105165:	74 24                	je     10518b <default_check+0x168>
  105167:	c7 44 24 0c fa 71 10 	movl   $0x1071fa,0xc(%esp)
  10516e:	00 
  10516f:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105176:	00 
  105177:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
  10517e:	00 
  10517f:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105186:	e8 9b b2 ff ff       	call   100426 <__panic>

    list_entry_t free_list_store = free_list;
  10518b:	a1 1c cf 11 00       	mov    0x11cf1c,%eax
  105190:	8b 15 20 cf 11 00    	mov    0x11cf20,%edx
  105196:	89 45 80             	mov    %eax,-0x80(%ebp)
  105199:	89 55 84             	mov    %edx,-0x7c(%ebp)
  10519c:	c7 45 b0 1c cf 11 00 	movl   $0x11cf1c,-0x50(%ebp)
    elm->prev = elm->next = elm;
  1051a3:	8b 45 b0             	mov    -0x50(%ebp),%eax
  1051a6:	8b 55 b0             	mov    -0x50(%ebp),%edx
  1051a9:	89 50 04             	mov    %edx,0x4(%eax)
  1051ac:	8b 45 b0             	mov    -0x50(%ebp),%eax
  1051af:	8b 50 04             	mov    0x4(%eax),%edx
  1051b2:	8b 45 b0             	mov    -0x50(%ebp),%eax
  1051b5:	89 10                	mov    %edx,(%eax)
}
  1051b7:	90                   	nop
  1051b8:	c7 45 b4 1c cf 11 00 	movl   $0x11cf1c,-0x4c(%ebp)
    return list->next == list;
  1051bf:	8b 45 b4             	mov    -0x4c(%ebp),%eax
  1051c2:	8b 40 04             	mov    0x4(%eax),%eax
  1051c5:	39 45 b4             	cmp    %eax,-0x4c(%ebp)
  1051c8:	0f 94 c0             	sete   %al
  1051cb:	0f b6 c0             	movzbl %al,%eax
    list_init(&free_list);
    assert(list_empty(&free_list));
  1051ce:	85 c0                	test   %eax,%eax
  1051d0:	75 24                	jne    1051f6 <default_check+0x1d3>
  1051d2:	c7 44 24 0c 4f 71 10 	movl   $0x10714f,0xc(%esp)
  1051d9:	00 
  1051da:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1051e1:	00 
  1051e2:	c7 44 24 04 43 01 00 	movl   $0x143,0x4(%esp)
  1051e9:	00 
  1051ea:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1051f1:	e8 30 b2 ff ff       	call   100426 <__panic>
    assert(alloc_page() == NULL);
  1051f6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  1051fd:	e8 36 db ff ff       	call   102d38 <alloc_pages>
  105202:	85 c0                	test   %eax,%eax
  105204:	74 24                	je     10522a <default_check+0x207>
  105206:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  10520d:	00 
  10520e:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105215:	00 
  105216:	c7 44 24 04 44 01 00 	movl   $0x144,0x4(%esp)
  10521d:	00 
  10521e:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105225:	e8 fc b1 ff ff       	call   100426 <__panic>

    unsigned int nr_free_store = nr_free;
  10522a:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  10522f:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    nr_free = 0;
  105232:	c7 05 24 cf 11 00 00 	movl   $0x0,0x11cf24
  105239:	00 00 00 

    free_pages(p0 + 2, 3);
  10523c:	8b 45 e8             	mov    -0x18(%ebp),%eax
  10523f:	83 c0 28             	add    $0x28,%eax
  105242:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
  105249:	00 
  10524a:	89 04 24             	mov    %eax,(%esp)
  10524d:	e8 22 db ff ff       	call   102d74 <free_pages>
    assert(alloc_pages(4) == NULL);
  105252:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
  105259:	e8 da da ff ff       	call   102d38 <alloc_pages>
  10525e:	85 c0                	test   %eax,%eax
  105260:	74 24                	je     105286 <default_check+0x263>
  105262:	c7 44 24 0c 0c 72 10 	movl   $0x10720c,0xc(%esp)
  105269:	00 
  10526a:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105271:	00 
  105272:	c7 44 24 04 4a 01 00 	movl   $0x14a,0x4(%esp)
  105279:	00 
  10527a:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105281:	e8 a0 b1 ff ff       	call   100426 <__panic>
    assert(PageProperty(p0 + 2) && p0[2].property == 3);
  105286:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105289:	83 c0 28             	add    $0x28,%eax
  10528c:	83 c0 04             	add    $0x4,%eax
  10528f:	c7 45 ac 01 00 00 00 	movl   $0x1,-0x54(%ebp)
  105296:	89 45 a8             	mov    %eax,-0x58(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  105299:	8b 45 a8             	mov    -0x58(%ebp),%eax
  10529c:	8b 55 ac             	mov    -0x54(%ebp),%edx
  10529f:	0f a3 10             	bt     %edx,(%eax)
  1052a2:	19 c0                	sbb    %eax,%eax
  1052a4:	89 45 a4             	mov    %eax,-0x5c(%ebp)
    return oldbit != 0;
  1052a7:	83 7d a4 00          	cmpl   $0x0,-0x5c(%ebp)
  1052ab:	0f 95 c0             	setne  %al
  1052ae:	0f b6 c0             	movzbl %al,%eax
  1052b1:	85 c0                	test   %eax,%eax
  1052b3:	74 0e                	je     1052c3 <default_check+0x2a0>
  1052b5:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1052b8:	83 c0 28             	add    $0x28,%eax
  1052bb:	8b 40 08             	mov    0x8(%eax),%eax
  1052be:	83 f8 03             	cmp    $0x3,%eax
  1052c1:	74 24                	je     1052e7 <default_check+0x2c4>
  1052c3:	c7 44 24 0c 24 72 10 	movl   $0x107224,0xc(%esp)
  1052ca:	00 
  1052cb:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1052d2:	00 
  1052d3:	c7 44 24 04 4b 01 00 	movl   $0x14b,0x4(%esp)
  1052da:	00 
  1052db:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1052e2:	e8 3f b1 ff ff       	call   100426 <__panic>
    assert((p1 = alloc_pages(3)) != NULL);
  1052e7:	c7 04 24 03 00 00 00 	movl   $0x3,(%esp)
  1052ee:	e8 45 da ff ff       	call   102d38 <alloc_pages>
  1052f3:	89 45 e0             	mov    %eax,-0x20(%ebp)
  1052f6:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
  1052fa:	75 24                	jne    105320 <default_check+0x2fd>
  1052fc:	c7 44 24 0c 50 72 10 	movl   $0x107250,0xc(%esp)
  105303:	00 
  105304:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10530b:	00 
  10530c:	c7 44 24 04 4c 01 00 	movl   $0x14c,0x4(%esp)
  105313:	00 
  105314:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10531b:	e8 06 b1 ff ff       	call   100426 <__panic>
    assert(alloc_page() == NULL);
  105320:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  105327:	e8 0c da ff ff       	call   102d38 <alloc_pages>
  10532c:	85 c0                	test   %eax,%eax
  10532e:	74 24                	je     105354 <default_check+0x331>
  105330:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  105337:	00 
  105338:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10533f:	00 
  105340:	c7 44 24 04 4d 01 00 	movl   $0x14d,0x4(%esp)
  105347:	00 
  105348:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10534f:	e8 d2 b0 ff ff       	call   100426 <__panic>
    assert(p0 + 2 == p1);
  105354:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105357:	83 c0 28             	add    $0x28,%eax
  10535a:	39 45 e0             	cmp    %eax,-0x20(%ebp)
  10535d:	74 24                	je     105383 <default_check+0x360>
  10535f:	c7 44 24 0c 6e 72 10 	movl   $0x10726e,0xc(%esp)
  105366:	00 
  105367:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10536e:	00 
  10536f:	c7 44 24 04 4e 01 00 	movl   $0x14e,0x4(%esp)
  105376:	00 
  105377:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  10537e:	e8 a3 b0 ff ff       	call   100426 <__panic>

    p2 = p0 + 1;
  105383:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105386:	83 c0 14             	add    $0x14,%eax
  105389:	89 45 dc             	mov    %eax,-0x24(%ebp)
    free_page(p0);
  10538c:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  105393:	00 
  105394:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105397:	89 04 24             	mov    %eax,(%esp)
  10539a:	e8 d5 d9 ff ff       	call   102d74 <free_pages>
    free_pages(p1, 3);
  10539f:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
  1053a6:	00 
  1053a7:	8b 45 e0             	mov    -0x20(%ebp),%eax
  1053aa:	89 04 24             	mov    %eax,(%esp)
  1053ad:	e8 c2 d9 ff ff       	call   102d74 <free_pages>
    assert(PageProperty(p0) && p0->property == 1);
  1053b2:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1053b5:	83 c0 04             	add    $0x4,%eax
  1053b8:	c7 45 a0 01 00 00 00 	movl   $0x1,-0x60(%ebp)
  1053bf:	89 45 9c             	mov    %eax,-0x64(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  1053c2:	8b 45 9c             	mov    -0x64(%ebp),%eax
  1053c5:	8b 55 a0             	mov    -0x60(%ebp),%edx
  1053c8:	0f a3 10             	bt     %edx,(%eax)
  1053cb:	19 c0                	sbb    %eax,%eax
  1053cd:	89 45 98             	mov    %eax,-0x68(%ebp)
    return oldbit != 0;
  1053d0:	83 7d 98 00          	cmpl   $0x0,-0x68(%ebp)
  1053d4:	0f 95 c0             	setne  %al
  1053d7:	0f b6 c0             	movzbl %al,%eax
  1053da:	85 c0                	test   %eax,%eax
  1053dc:	74 0b                	je     1053e9 <default_check+0x3c6>
  1053de:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1053e1:	8b 40 08             	mov    0x8(%eax),%eax
  1053e4:	83 f8 01             	cmp    $0x1,%eax
  1053e7:	74 24                	je     10540d <default_check+0x3ea>
  1053e9:	c7 44 24 0c 7c 72 10 	movl   $0x10727c,0xc(%esp)
  1053f0:	00 
  1053f1:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1053f8:	00 
  1053f9:	c7 44 24 04 53 01 00 	movl   $0x153,0x4(%esp)
  105400:	00 
  105401:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105408:	e8 19 b0 ff ff       	call   100426 <__panic>
    assert(PageProperty(p1) && p1->property == 3);
  10540d:	8b 45 e0             	mov    -0x20(%ebp),%eax
  105410:	83 c0 04             	add    $0x4,%eax
  105413:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
  10541a:	89 45 90             	mov    %eax,-0x70(%ebp)
    asm volatile ("btl %2, %1; sbbl %0,%0" : "=r" (oldbit) : "m" (*(volatile long *)addr), "Ir" (nr));
  10541d:	8b 45 90             	mov    -0x70(%ebp),%eax
  105420:	8b 55 94             	mov    -0x6c(%ebp),%edx
  105423:	0f a3 10             	bt     %edx,(%eax)
  105426:	19 c0                	sbb    %eax,%eax
  105428:	89 45 8c             	mov    %eax,-0x74(%ebp)
    return oldbit != 0;
  10542b:	83 7d 8c 00          	cmpl   $0x0,-0x74(%ebp)
  10542f:	0f 95 c0             	setne  %al
  105432:	0f b6 c0             	movzbl %al,%eax
  105435:	85 c0                	test   %eax,%eax
  105437:	74 0b                	je     105444 <default_check+0x421>
  105439:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10543c:	8b 40 08             	mov    0x8(%eax),%eax
  10543f:	83 f8 03             	cmp    $0x3,%eax
  105442:	74 24                	je     105468 <default_check+0x445>
  105444:	c7 44 24 0c a4 72 10 	movl   $0x1072a4,0xc(%esp)
  10544b:	00 
  10544c:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105453:	00 
  105454:	c7 44 24 04 54 01 00 	movl   $0x154,0x4(%esp)
  10545b:	00 
  10545c:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105463:	e8 be af ff ff       	call   100426 <__panic>

    assert((p0 = alloc_page()) == p2 - 1);
  105468:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  10546f:	e8 c4 d8 ff ff       	call   102d38 <alloc_pages>
  105474:	89 45 e8             	mov    %eax,-0x18(%ebp)
  105477:	8b 45 dc             	mov    -0x24(%ebp),%eax
  10547a:	83 e8 14             	sub    $0x14,%eax
  10547d:	39 45 e8             	cmp    %eax,-0x18(%ebp)
  105480:	74 24                	je     1054a6 <default_check+0x483>
  105482:	c7 44 24 0c ca 72 10 	movl   $0x1072ca,0xc(%esp)
  105489:	00 
  10548a:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105491:	00 
  105492:	c7 44 24 04 56 01 00 	movl   $0x156,0x4(%esp)
  105499:	00 
  10549a:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1054a1:	e8 80 af ff ff       	call   100426 <__panic>
    free_page(p0);
  1054a6:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  1054ad:	00 
  1054ae:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1054b1:	89 04 24             	mov    %eax,(%esp)
  1054b4:	e8 bb d8 ff ff       	call   102d74 <free_pages>
    assert((p0 = alloc_pages(2)) == p2 + 1);
  1054b9:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
  1054c0:	e8 73 d8 ff ff       	call   102d38 <alloc_pages>
  1054c5:	89 45 e8             	mov    %eax,-0x18(%ebp)
  1054c8:	8b 45 dc             	mov    -0x24(%ebp),%eax
  1054cb:	83 c0 14             	add    $0x14,%eax
  1054ce:	39 45 e8             	cmp    %eax,-0x18(%ebp)
  1054d1:	74 24                	je     1054f7 <default_check+0x4d4>
  1054d3:	c7 44 24 0c e8 72 10 	movl   $0x1072e8,0xc(%esp)
  1054da:	00 
  1054db:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1054e2:	00 
  1054e3:	c7 44 24 04 58 01 00 	movl   $0x158,0x4(%esp)
  1054ea:	00 
  1054eb:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1054f2:	e8 2f af ff ff       	call   100426 <__panic>

    free_pages(p0, 2);
  1054f7:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
  1054fe:	00 
  1054ff:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105502:	89 04 24             	mov    %eax,(%esp)
  105505:	e8 6a d8 ff ff       	call   102d74 <free_pages>
    free_page(p2);
  10550a:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
  105511:	00 
  105512:	8b 45 dc             	mov    -0x24(%ebp),%eax
  105515:	89 04 24             	mov    %eax,(%esp)
  105518:	e8 57 d8 ff ff       	call   102d74 <free_pages>

    assert((p0 = alloc_pages(5)) != NULL);
  10551d:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
  105524:	e8 0f d8 ff ff       	call   102d38 <alloc_pages>
  105529:	89 45 e8             	mov    %eax,-0x18(%ebp)
  10552c:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105530:	75 24                	jne    105556 <default_check+0x533>
  105532:	c7 44 24 0c 08 73 10 	movl   $0x107308,0xc(%esp)
  105539:	00 
  10553a:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105541:	00 
  105542:	c7 44 24 04 5d 01 00 	movl   $0x15d,0x4(%esp)
  105549:	00 
  10554a:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105551:	e8 d0 ae ff ff       	call   100426 <__panic>
    assert(alloc_page() == NULL);
  105556:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
  10555d:	e8 d6 d7 ff ff       	call   102d38 <alloc_pages>
  105562:	85 c0                	test   %eax,%eax
  105564:	74 24                	je     10558a <default_check+0x567>
  105566:	c7 44 24 0c 66 71 10 	movl   $0x107166,0xc(%esp)
  10556d:	00 
  10556e:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105575:	00 
  105576:	c7 44 24 04 5e 01 00 	movl   $0x15e,0x4(%esp)
  10557d:	00 
  10557e:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105585:	e8 9c ae ff ff       	call   100426 <__panic>

    assert(nr_free == 0);
  10558a:	a1 24 cf 11 00       	mov    0x11cf24,%eax
  10558f:	85 c0                	test   %eax,%eax
  105591:	74 24                	je     1055b7 <default_check+0x594>
  105593:	c7 44 24 0c b9 71 10 	movl   $0x1071b9,0xc(%esp)
  10559a:	00 
  10559b:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  1055a2:	00 
  1055a3:	c7 44 24 04 60 01 00 	movl   $0x160,0x4(%esp)
  1055aa:	00 
  1055ab:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1055b2:	e8 6f ae ff ff       	call   100426 <__panic>
    nr_free = nr_free_store;
  1055b7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  1055ba:	a3 24 cf 11 00       	mov    %eax,0x11cf24

    free_list = free_list_store;
  1055bf:	8b 45 80             	mov    -0x80(%ebp),%eax
  1055c2:	8b 55 84             	mov    -0x7c(%ebp),%edx
  1055c5:	a3 1c cf 11 00       	mov    %eax,0x11cf1c
  1055ca:	89 15 20 cf 11 00    	mov    %edx,0x11cf20
    free_pages(p0, 5);
  1055d0:	c7 44 24 04 05 00 00 	movl   $0x5,0x4(%esp)
  1055d7:	00 
  1055d8:	8b 45 e8             	mov    -0x18(%ebp),%eax
  1055db:	89 04 24             	mov    %eax,(%esp)
  1055de:	e8 91 d7 ff ff       	call   102d74 <free_pages>

    le = &free_list;
  1055e3:	c7 45 ec 1c cf 11 00 	movl   $0x11cf1c,-0x14(%ebp)
    while ((le = list_next(le)) != &free_list) {
  1055ea:	eb 5a                	jmp    105646 <default_check+0x623>
        assert(le->next->prev == le && le->prev->next == le);
  1055ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1055ef:	8b 40 04             	mov    0x4(%eax),%eax
  1055f2:	8b 00                	mov    (%eax),%eax
  1055f4:	39 45 ec             	cmp    %eax,-0x14(%ebp)
  1055f7:	75 0d                	jne    105606 <default_check+0x5e3>
  1055f9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1055fc:	8b 00                	mov    (%eax),%eax
  1055fe:	8b 40 04             	mov    0x4(%eax),%eax
  105601:	39 45 ec             	cmp    %eax,-0x14(%ebp)
  105604:	74 24                	je     10562a <default_check+0x607>
  105606:	c7 44 24 0c 28 73 10 	movl   $0x107328,0xc(%esp)
  10560d:	00 
  10560e:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105615:	00 
  105616:	c7 44 24 04 68 01 00 	movl   $0x168,0x4(%esp)
  10561d:	00 
  10561e:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105625:	e8 fc ad ff ff       	call   100426 <__panic>
        struct Page *p = le2page(le, page_link);
  10562a:	8b 45 ec             	mov    -0x14(%ebp),%eax
  10562d:	83 e8 0c             	sub    $0xc,%eax
  105630:	89 45 d8             	mov    %eax,-0x28(%ebp)
        count --, total -= p->property;
  105633:	ff 4d f4             	decl   -0xc(%ebp)
  105636:	8b 55 f0             	mov    -0x10(%ebp),%edx
  105639:	8b 45 d8             	mov    -0x28(%ebp),%eax
  10563c:	8b 40 08             	mov    0x8(%eax),%eax
  10563f:	29 c2                	sub    %eax,%edx
  105641:	89 d0                	mov    %edx,%eax
  105643:	89 45 f0             	mov    %eax,-0x10(%ebp)
  105646:	8b 45 ec             	mov    -0x14(%ebp),%eax
  105649:	89 45 88             	mov    %eax,-0x78(%ebp)
    return listelm->next;
  10564c:	8b 45 88             	mov    -0x78(%ebp),%eax
  10564f:	8b 40 04             	mov    0x4(%eax),%eax
    while ((le = list_next(le)) != &free_list) {
  105652:	89 45 ec             	mov    %eax,-0x14(%ebp)
  105655:	81 7d ec 1c cf 11 00 	cmpl   $0x11cf1c,-0x14(%ebp)
  10565c:	75 8e                	jne    1055ec <default_check+0x5c9>
    }
    assert(count == 0);
  10565e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
  105662:	74 24                	je     105688 <default_check+0x665>
  105664:	c7 44 24 0c 55 73 10 	movl   $0x107355,0xc(%esp)
  10566b:	00 
  10566c:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  105673:	00 
  105674:	c7 44 24 04 6c 01 00 	movl   $0x16c,0x4(%esp)
  10567b:	00 
  10567c:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  105683:	e8 9e ad ff ff       	call   100426 <__panic>
    assert(total == 0);
  105688:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  10568c:	74 24                	je     1056b2 <default_check+0x68f>
  10568e:	c7 44 24 0c 60 73 10 	movl   $0x107360,0xc(%esp)
  105695:	00 
  105696:	c7 44 24 08 de 6f 10 	movl   $0x106fde,0x8(%esp)
  10569d:	00 
  10569e:	c7 44 24 04 6d 01 00 	movl   $0x16d,0x4(%esp)
  1056a5:	00 
  1056a6:	c7 04 24 f3 6f 10 00 	movl   $0x106ff3,(%esp)
  1056ad:	e8 74 ad ff ff       	call   100426 <__panic>
}
  1056b2:	90                   	nop
  1056b3:	c9                   	leave  
  1056b4:	c3                   	ret    

001056b5 <strlen>:
 * @s:      the input string
 *
 * The strlen() function returns the length of string @s.
 * */
size_t
strlen(const char *s) {
  1056b5:	f3 0f 1e fb          	endbr32 
  1056b9:	55                   	push   %ebp
  1056ba:	89 e5                	mov    %esp,%ebp
  1056bc:	83 ec 10             	sub    $0x10,%esp
    size_t cnt = 0;
  1056bf:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    while (*s ++ != '\0') {
  1056c6:	eb 03                	jmp    1056cb <strlen+0x16>
        cnt ++;
  1056c8:	ff 45 fc             	incl   -0x4(%ebp)
    while (*s ++ != '\0') {
  1056cb:	8b 45 08             	mov    0x8(%ebp),%eax
  1056ce:	8d 50 01             	lea    0x1(%eax),%edx
  1056d1:	89 55 08             	mov    %edx,0x8(%ebp)
  1056d4:	0f b6 00             	movzbl (%eax),%eax
  1056d7:	84 c0                	test   %al,%al
  1056d9:	75 ed                	jne    1056c8 <strlen+0x13>
    }
    return cnt;
  1056db:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  1056de:	c9                   	leave  
  1056df:	c3                   	ret    

001056e0 <strnlen>:
 * The return value is strlen(s), if that is less than @len, or
 * @len if there is no '\0' character among the first @len characters
 * pointed by @s.
 * */
size_t
strnlen(const char *s, size_t len) {
  1056e0:	f3 0f 1e fb          	endbr32 
  1056e4:	55                   	push   %ebp
  1056e5:	89 e5                	mov    %esp,%ebp
  1056e7:	83 ec 10             	sub    $0x10,%esp
    size_t cnt = 0;
  1056ea:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    while (cnt < len && *s ++ != '\0') {
  1056f1:	eb 03                	jmp    1056f6 <strnlen+0x16>
        cnt ++;
  1056f3:	ff 45 fc             	incl   -0x4(%ebp)
    while (cnt < len && *s ++ != '\0') {
  1056f6:	8b 45 fc             	mov    -0x4(%ebp),%eax
  1056f9:	3b 45 0c             	cmp    0xc(%ebp),%eax
  1056fc:	73 10                	jae    10570e <strnlen+0x2e>
  1056fe:	8b 45 08             	mov    0x8(%ebp),%eax
  105701:	8d 50 01             	lea    0x1(%eax),%edx
  105704:	89 55 08             	mov    %edx,0x8(%ebp)
  105707:	0f b6 00             	movzbl (%eax),%eax
  10570a:	84 c0                	test   %al,%al
  10570c:	75 e5                	jne    1056f3 <strnlen+0x13>
    }
    return cnt;
  10570e:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
  105711:	c9                   	leave  
  105712:	c3                   	ret    

00105713 <strcpy>:
 * To avoid overflows, the size of array pointed by @dst should be long enough to
 * contain the same string as @src (including the terminating null character), and
 * should not overlap in memory with @src.
 * */
char *
strcpy(char *dst, const char *src) {
  105713:	f3 0f 1e fb          	endbr32 
  105717:	55                   	push   %ebp
  105718:	89 e5                	mov    %esp,%ebp
  10571a:	57                   	push   %edi
  10571b:	56                   	push   %esi
  10571c:	83 ec 20             	sub    $0x20,%esp
  10571f:	8b 45 08             	mov    0x8(%ebp),%eax
  105722:	89 45 f4             	mov    %eax,-0xc(%ebp)
  105725:	8b 45 0c             	mov    0xc(%ebp),%eax
  105728:	89 45 f0             	mov    %eax,-0x10(%ebp)
#ifndef __HAVE_ARCH_STRCPY
#define __HAVE_ARCH_STRCPY
static inline char *
__strcpy(char *dst, const char *src) {
    int d0, d1, d2;
    asm volatile (
  10572b:	8b 55 f0             	mov    -0x10(%ebp),%edx
  10572e:	8b 45 f4             	mov    -0xc(%ebp),%eax
  105731:	89 d1                	mov    %edx,%ecx
  105733:	89 c2                	mov    %eax,%edx
  105735:	89 ce                	mov    %ecx,%esi
  105737:	89 d7                	mov    %edx,%edi
  105739:	ac                   	lods   %ds:(%esi),%al
  10573a:	aa                   	stos   %al,%es:(%edi)
  10573b:	84 c0                	test   %al,%al
  10573d:	75 fa                	jne    105739 <strcpy+0x26>
  10573f:	89 fa                	mov    %edi,%edx
  105741:	89 f1                	mov    %esi,%ecx
  105743:	89 4d ec             	mov    %ecx,-0x14(%ebp)
  105746:	89 55 e8             	mov    %edx,-0x18(%ebp)
  105749:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        "stosb;"
        "testb %%al, %%al;"
        "jne 1b;"
        : "=&S" (d0), "=&D" (d1), "=&a" (d2)
        : "0" (src), "1" (dst) : "memory");
    return dst;
  10574c:	8b 45 f4             	mov    -0xc(%ebp),%eax
    char *p = dst;
    while ((*p ++ = *src ++) != '\0')
        /* nothing */;
    return dst;
#endif /* __HAVE_ARCH_STRCPY */
}
  10574f:	83 c4 20             	add    $0x20,%esp
  105752:	5e                   	pop    %esi
  105753:	5f                   	pop    %edi
  105754:	5d                   	pop    %ebp
  105755:	c3                   	ret    

00105756 <strncpy>:
 * @len:    maximum number of characters to be copied from @src
 *
 * The return value is @dst
 * */
char *
strncpy(char *dst, const char *src, size_t len) {
  105756:	f3 0f 1e fb          	endbr32 
  10575a:	55                   	push   %ebp
  10575b:	89 e5                	mov    %esp,%ebp
  10575d:	83 ec 10             	sub    $0x10,%esp
    char *p = dst;
  105760:	8b 45 08             	mov    0x8(%ebp),%eax
  105763:	89 45 fc             	mov    %eax,-0x4(%ebp)
    while (len > 0) {
  105766:	eb 1e                	jmp    105786 <strncpy+0x30>
        if ((*p = *src) != '\0') {
  105768:	8b 45 0c             	mov    0xc(%ebp),%eax
  10576b:	0f b6 10             	movzbl (%eax),%edx
  10576e:	8b 45 fc             	mov    -0x4(%ebp),%eax
  105771:	88 10                	mov    %dl,(%eax)
  105773:	8b 45 fc             	mov    -0x4(%ebp),%eax
  105776:	0f b6 00             	movzbl (%eax),%eax
  105779:	84 c0                	test   %al,%al
  10577b:	74 03                	je     105780 <strncpy+0x2a>
            src ++;
  10577d:	ff 45 0c             	incl   0xc(%ebp)
        }
        p ++, len --;
  105780:	ff 45 fc             	incl   -0x4(%ebp)
  105783:	ff 4d 10             	decl   0x10(%ebp)
    while (len > 0) {
  105786:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  10578a:	75 dc                	jne    105768 <strncpy+0x12>
    }
    return dst;
  10578c:	8b 45 08             	mov    0x8(%ebp),%eax
}
  10578f:	c9                   	leave  
  105790:	c3                   	ret    

00105791 <strcmp>:
 * - A value greater than zero indicates that the first character that does
 *   not match has a greater value in @s1 than in @s2;
 * - And a value less than zero indicates the opposite.
 * */
int
strcmp(const char *s1, const char *s2) {
  105791:	f3 0f 1e fb          	endbr32 
  105795:	55                   	push   %ebp
  105796:	89 e5                	mov    %esp,%ebp
  105798:	57                   	push   %edi
  105799:	56                   	push   %esi
  10579a:	83 ec 20             	sub    $0x20,%esp
  10579d:	8b 45 08             	mov    0x8(%ebp),%eax
  1057a0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  1057a3:	8b 45 0c             	mov    0xc(%ebp),%eax
  1057a6:	89 45 f0             	mov    %eax,-0x10(%ebp)
    asm volatile (
  1057a9:	8b 55 f4             	mov    -0xc(%ebp),%edx
  1057ac:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1057af:	89 d1                	mov    %edx,%ecx
  1057b1:	89 c2                	mov    %eax,%edx
  1057b3:	89 ce                	mov    %ecx,%esi
  1057b5:	89 d7                	mov    %edx,%edi
  1057b7:	ac                   	lods   %ds:(%esi),%al
  1057b8:	ae                   	scas   %es:(%edi),%al
  1057b9:	75 08                	jne    1057c3 <strcmp+0x32>
  1057bb:	84 c0                	test   %al,%al
  1057bd:	75 f8                	jne    1057b7 <strcmp+0x26>
  1057bf:	31 c0                	xor    %eax,%eax
  1057c1:	eb 04                	jmp    1057c7 <strcmp+0x36>
  1057c3:	19 c0                	sbb    %eax,%eax
  1057c5:	0c 01                	or     $0x1,%al
  1057c7:	89 fa                	mov    %edi,%edx
  1057c9:	89 f1                	mov    %esi,%ecx
  1057cb:	89 45 ec             	mov    %eax,-0x14(%ebp)
  1057ce:	89 4d e8             	mov    %ecx,-0x18(%ebp)
  1057d1:	89 55 e4             	mov    %edx,-0x1c(%ebp)
    return ret;
  1057d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
    while (*s1 != '\0' && *s1 == *s2) {
        s1 ++, s2 ++;
    }
    return (int)((unsigned char)*s1 - (unsigned char)*s2);
#endif /* __HAVE_ARCH_STRCMP */
}
  1057d7:	83 c4 20             	add    $0x20,%esp
  1057da:	5e                   	pop    %esi
  1057db:	5f                   	pop    %edi
  1057dc:	5d                   	pop    %ebp
  1057dd:	c3                   	ret    

001057de <strncmp>:
 * they are equal to each other, it continues with the following pairs until
 * the characters differ, until a terminating null-character is reached, or
 * until @n characters match in both strings, whichever happens first.
 * */
int
strncmp(const char *s1, const char *s2, size_t n) {
  1057de:	f3 0f 1e fb          	endbr32 
  1057e2:	55                   	push   %ebp
  1057e3:	89 e5                	mov    %esp,%ebp
    while (n > 0 && *s1 != '\0' && *s1 == *s2) {
  1057e5:	eb 09                	jmp    1057f0 <strncmp+0x12>
        n --, s1 ++, s2 ++;
  1057e7:	ff 4d 10             	decl   0x10(%ebp)
  1057ea:	ff 45 08             	incl   0x8(%ebp)
  1057ed:	ff 45 0c             	incl   0xc(%ebp)
    while (n > 0 && *s1 != '\0' && *s1 == *s2) {
  1057f0:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  1057f4:	74 1a                	je     105810 <strncmp+0x32>
  1057f6:	8b 45 08             	mov    0x8(%ebp),%eax
  1057f9:	0f b6 00             	movzbl (%eax),%eax
  1057fc:	84 c0                	test   %al,%al
  1057fe:	74 10                	je     105810 <strncmp+0x32>
  105800:	8b 45 08             	mov    0x8(%ebp),%eax
  105803:	0f b6 10             	movzbl (%eax),%edx
  105806:	8b 45 0c             	mov    0xc(%ebp),%eax
  105809:	0f b6 00             	movzbl (%eax),%eax
  10580c:	38 c2                	cmp    %al,%dl
  10580e:	74 d7                	je     1057e7 <strncmp+0x9>
    }
    return (n == 0) ? 0 : (int)((unsigned char)*s1 - (unsigned char)*s2);
  105810:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  105814:	74 18                	je     10582e <strncmp+0x50>
  105816:	8b 45 08             	mov    0x8(%ebp),%eax
  105819:	0f b6 00             	movzbl (%eax),%eax
  10581c:	0f b6 d0             	movzbl %al,%edx
  10581f:	8b 45 0c             	mov    0xc(%ebp),%eax
  105822:	0f b6 00             	movzbl (%eax),%eax
  105825:	0f b6 c0             	movzbl %al,%eax
  105828:	29 c2                	sub    %eax,%edx
  10582a:	89 d0                	mov    %edx,%eax
  10582c:	eb 05                	jmp    105833 <strncmp+0x55>
  10582e:	b8 00 00 00 00       	mov    $0x0,%eax
}
  105833:	5d                   	pop    %ebp
  105834:	c3                   	ret    

00105835 <strchr>:
 *
 * The strchr() function returns a pointer to the first occurrence of
 * character in @s. If the value is not found, the function returns 'NULL'.
 * */
char *
strchr(const char *s, char c) {
  105835:	f3 0f 1e fb          	endbr32 
  105839:	55                   	push   %ebp
  10583a:	89 e5                	mov    %esp,%ebp
  10583c:	83 ec 04             	sub    $0x4,%esp
  10583f:	8b 45 0c             	mov    0xc(%ebp),%eax
  105842:	88 45 fc             	mov    %al,-0x4(%ebp)
    while (*s != '\0') {
  105845:	eb 13                	jmp    10585a <strchr+0x25>
        if (*s == c) {
  105847:	8b 45 08             	mov    0x8(%ebp),%eax
  10584a:	0f b6 00             	movzbl (%eax),%eax
  10584d:	38 45 fc             	cmp    %al,-0x4(%ebp)
  105850:	75 05                	jne    105857 <strchr+0x22>
            return (char *)s;
  105852:	8b 45 08             	mov    0x8(%ebp),%eax
  105855:	eb 12                	jmp    105869 <strchr+0x34>
        }
        s ++;
  105857:	ff 45 08             	incl   0x8(%ebp)
    while (*s != '\0') {
  10585a:	8b 45 08             	mov    0x8(%ebp),%eax
  10585d:	0f b6 00             	movzbl (%eax),%eax
  105860:	84 c0                	test   %al,%al
  105862:	75 e3                	jne    105847 <strchr+0x12>
    }
    return NULL;
  105864:	b8 00 00 00 00       	mov    $0x0,%eax
}
  105869:	c9                   	leave  
  10586a:	c3                   	ret    

0010586b <strfind>:
 * The strfind() function is like strchr() except that if @c is
 * not found in @s, then it returns a pointer to the null byte at the
 * end of @s, rather than 'NULL'.
 * */
char *
strfind(const char *s, char c) {
  10586b:	f3 0f 1e fb          	endbr32 
  10586f:	55                   	push   %ebp
  105870:	89 e5                	mov    %esp,%ebp
  105872:	83 ec 04             	sub    $0x4,%esp
  105875:	8b 45 0c             	mov    0xc(%ebp),%eax
  105878:	88 45 fc             	mov    %al,-0x4(%ebp)
    while (*s != '\0') {
  10587b:	eb 0e                	jmp    10588b <strfind+0x20>
        if (*s == c) {
  10587d:	8b 45 08             	mov    0x8(%ebp),%eax
  105880:	0f b6 00             	movzbl (%eax),%eax
  105883:	38 45 fc             	cmp    %al,-0x4(%ebp)
  105886:	74 0f                	je     105897 <strfind+0x2c>
            break;
        }
        s ++;
  105888:	ff 45 08             	incl   0x8(%ebp)
    while (*s != '\0') {
  10588b:	8b 45 08             	mov    0x8(%ebp),%eax
  10588e:	0f b6 00             	movzbl (%eax),%eax
  105891:	84 c0                	test   %al,%al
  105893:	75 e8                	jne    10587d <strfind+0x12>
  105895:	eb 01                	jmp    105898 <strfind+0x2d>
            break;
  105897:	90                   	nop
    }
    return (char *)s;
  105898:	8b 45 08             	mov    0x8(%ebp),%eax
}
  10589b:	c9                   	leave  
  10589c:	c3                   	ret    

0010589d <strtol>:
 * an optional "0x" or "0X" prefix.
 *
 * The strtol() function returns the converted integral number as a long int value.
 * */
long
strtol(const char *s, char **endptr, int base) {
  10589d:	f3 0f 1e fb          	endbr32 
  1058a1:	55                   	push   %ebp
  1058a2:	89 e5                	mov    %esp,%ebp
  1058a4:	83 ec 10             	sub    $0x10,%esp
    int neg = 0;
  1058a7:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
    long val = 0;
  1058ae:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)

    // gobble initial whitespace
    while (*s == ' ' || *s == '\t') {
  1058b5:	eb 03                	jmp    1058ba <strtol+0x1d>
        s ++;
  1058b7:	ff 45 08             	incl   0x8(%ebp)
    while (*s == ' ' || *s == '\t') {
  1058ba:	8b 45 08             	mov    0x8(%ebp),%eax
  1058bd:	0f b6 00             	movzbl (%eax),%eax
  1058c0:	3c 20                	cmp    $0x20,%al
  1058c2:	74 f3                	je     1058b7 <strtol+0x1a>
  1058c4:	8b 45 08             	mov    0x8(%ebp),%eax
  1058c7:	0f b6 00             	movzbl (%eax),%eax
  1058ca:	3c 09                	cmp    $0x9,%al
  1058cc:	74 e9                	je     1058b7 <strtol+0x1a>
    }

    // plus/minus sign
    if (*s == '+') {
  1058ce:	8b 45 08             	mov    0x8(%ebp),%eax
  1058d1:	0f b6 00             	movzbl (%eax),%eax
  1058d4:	3c 2b                	cmp    $0x2b,%al
  1058d6:	75 05                	jne    1058dd <strtol+0x40>
        s ++;
  1058d8:	ff 45 08             	incl   0x8(%ebp)
  1058db:	eb 14                	jmp    1058f1 <strtol+0x54>
    }
    else if (*s == '-') {
  1058dd:	8b 45 08             	mov    0x8(%ebp),%eax
  1058e0:	0f b6 00             	movzbl (%eax),%eax
  1058e3:	3c 2d                	cmp    $0x2d,%al
  1058e5:	75 0a                	jne    1058f1 <strtol+0x54>
        s ++, neg = 1;
  1058e7:	ff 45 08             	incl   0x8(%ebp)
  1058ea:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%ebp)
    }

    // hex or octal base prefix
    if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x')) {
  1058f1:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  1058f5:	74 06                	je     1058fd <strtol+0x60>
  1058f7:	83 7d 10 10          	cmpl   $0x10,0x10(%ebp)
  1058fb:	75 22                	jne    10591f <strtol+0x82>
  1058fd:	8b 45 08             	mov    0x8(%ebp),%eax
  105900:	0f b6 00             	movzbl (%eax),%eax
  105903:	3c 30                	cmp    $0x30,%al
  105905:	75 18                	jne    10591f <strtol+0x82>
  105907:	8b 45 08             	mov    0x8(%ebp),%eax
  10590a:	40                   	inc    %eax
  10590b:	0f b6 00             	movzbl (%eax),%eax
  10590e:	3c 78                	cmp    $0x78,%al
  105910:	75 0d                	jne    10591f <strtol+0x82>
        s += 2, base = 16;
  105912:	83 45 08 02          	addl   $0x2,0x8(%ebp)
  105916:	c7 45 10 10 00 00 00 	movl   $0x10,0x10(%ebp)
  10591d:	eb 29                	jmp    105948 <strtol+0xab>
    }
    else if (base == 0 && s[0] == '0') {
  10591f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  105923:	75 16                	jne    10593b <strtol+0x9e>
  105925:	8b 45 08             	mov    0x8(%ebp),%eax
  105928:	0f b6 00             	movzbl (%eax),%eax
  10592b:	3c 30                	cmp    $0x30,%al
  10592d:	75 0c                	jne    10593b <strtol+0x9e>
        s ++, base = 8;
  10592f:	ff 45 08             	incl   0x8(%ebp)
  105932:	c7 45 10 08 00 00 00 	movl   $0x8,0x10(%ebp)
  105939:	eb 0d                	jmp    105948 <strtol+0xab>
    }
    else if (base == 0) {
  10593b:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
  10593f:	75 07                	jne    105948 <strtol+0xab>
        base = 10;
  105941:	c7 45 10 0a 00 00 00 	movl   $0xa,0x10(%ebp)

    // digits
    while (1) {
        int dig;

        if (*s >= '0' && *s <= '9') {
  105948:	8b 45 08             	mov    0x8(%ebp),%eax
  10594b:	0f b6 00             	movzbl (%eax),%eax
  10594e:	3c 2f                	cmp    $0x2f,%al
  105950:	7e 1b                	jle    10596d <strtol+0xd0>
  105952:	8b 45 08             	mov    0x8(%ebp),%eax
  105955:	0f b6 00             	movzbl (%eax),%eax
  105958:	3c 39                	cmp    $0x39,%al
  10595a:	7f 11                	jg     10596d <strtol+0xd0>
            dig = *s - '0';
  10595c:	8b 45 08             	mov    0x8(%ebp),%eax
  10595f:	0f b6 00             	movzbl (%eax),%eax
  105962:	0f be c0             	movsbl %al,%eax
  105965:	83 e8 30             	sub    $0x30,%eax
  105968:	89 45 f4             	mov    %eax,-0xc(%ebp)
  10596b:	eb 48                	jmp    1059b5 <strtol+0x118>
        }
        else if (*s >= 'a' && *s <= 'z') {
  10596d:	8b 45 08             	mov    0x8(%ebp),%eax
  105970:	0f b6 00             	movzbl (%eax),%eax
  105973:	3c 60                	cmp    $0x60,%al
  105975:	7e 1b                	jle    105992 <strtol+0xf5>
  105977:	8b 45 08             	mov    0x8(%ebp),%eax
  10597a:	0f b6 00             	movzbl (%eax),%eax
  10597d:	3c 7a                	cmp    $0x7a,%al
  10597f:	7f 11                	jg     105992 <strtol+0xf5>
            dig = *s - 'a' + 10;
  105981:	8b 45 08             	mov    0x8(%ebp),%eax
  105984:	0f b6 00             	movzbl (%eax),%eax
  105987:	0f be c0             	movsbl %al,%eax
  10598a:	83 e8 57             	sub    $0x57,%eax
  10598d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  105990:	eb 23                	jmp    1059b5 <strtol+0x118>
        }
        else if (*s >= 'A' && *s <= 'Z') {
  105992:	8b 45 08             	mov    0x8(%ebp),%eax
  105995:	0f b6 00             	movzbl (%eax),%eax
  105998:	3c 40                	cmp    $0x40,%al
  10599a:	7e 3b                	jle    1059d7 <strtol+0x13a>
  10599c:	8b 45 08             	mov    0x8(%ebp),%eax
  10599f:	0f b6 00             	movzbl (%eax),%eax
  1059a2:	3c 5a                	cmp    $0x5a,%al
  1059a4:	7f 31                	jg     1059d7 <strtol+0x13a>
            dig = *s - 'A' + 10;
  1059a6:	8b 45 08             	mov    0x8(%ebp),%eax
  1059a9:	0f b6 00             	movzbl (%eax),%eax
  1059ac:	0f be c0             	movsbl %al,%eax
  1059af:	83 e8 37             	sub    $0x37,%eax
  1059b2:	89 45 f4             	mov    %eax,-0xc(%ebp)
        }
        else {
            break;
        }
        if (dig >= base) {
  1059b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1059b8:	3b 45 10             	cmp    0x10(%ebp),%eax
  1059bb:	7d 19                	jge    1059d6 <strtol+0x139>
            break;
        }
        s ++, val = (val * base) + dig;
  1059bd:	ff 45 08             	incl   0x8(%ebp)
  1059c0:	8b 45 f8             	mov    -0x8(%ebp),%eax
  1059c3:	0f af 45 10          	imul   0x10(%ebp),%eax
  1059c7:	89 c2                	mov    %eax,%edx
  1059c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
  1059cc:	01 d0                	add    %edx,%eax
  1059ce:	89 45 f8             	mov    %eax,-0x8(%ebp)
    while (1) {
  1059d1:	e9 72 ff ff ff       	jmp    105948 <strtol+0xab>
            break;
  1059d6:	90                   	nop
        // we don't properly detect overflow!
    }

    if (endptr) {
  1059d7:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  1059db:	74 08                	je     1059e5 <strtol+0x148>
        *endptr = (char *) s;
  1059dd:	8b 45 0c             	mov    0xc(%ebp),%eax
  1059e0:	8b 55 08             	mov    0x8(%ebp),%edx
  1059e3:	89 10                	mov    %edx,(%eax)
    }
    return (neg ? -val : val);
  1059e5:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
  1059e9:	74 07                	je     1059f2 <strtol+0x155>
  1059eb:	8b 45 f8             	mov    -0x8(%ebp),%eax
  1059ee:	f7 d8                	neg    %eax
  1059f0:	eb 03                	jmp    1059f5 <strtol+0x158>
  1059f2:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
  1059f5:	c9                   	leave  
  1059f6:	c3                   	ret    

001059f7 <memset>:
 * @n:      number of bytes to be set to the value
 *
 * The memset() function returns @s.
 * */
void *
memset(void *s, char c, size_t n) {
  1059f7:	f3 0f 1e fb          	endbr32 
  1059fb:	55                   	push   %ebp
  1059fc:	89 e5                	mov    %esp,%ebp
  1059fe:	57                   	push   %edi
  1059ff:	83 ec 24             	sub    $0x24,%esp
  105a02:	8b 45 0c             	mov    0xc(%ebp),%eax
  105a05:	88 45 d8             	mov    %al,-0x28(%ebp)
#ifdef __HAVE_ARCH_MEMSET
    return __memset(s, c, n);
  105a08:	0f be 55 d8          	movsbl -0x28(%ebp),%edx
  105a0c:	8b 45 08             	mov    0x8(%ebp),%eax
  105a0f:	89 45 f8             	mov    %eax,-0x8(%ebp)
  105a12:	88 55 f7             	mov    %dl,-0x9(%ebp)
  105a15:	8b 45 10             	mov    0x10(%ebp),%eax
  105a18:	89 45 f0             	mov    %eax,-0x10(%ebp)
#ifndef __HAVE_ARCH_MEMSET
#define __HAVE_ARCH_MEMSET
static inline void *
__memset(void *s, char c, size_t n) {
    int d0, d1;
    asm volatile (
  105a1b:	8b 4d f0             	mov    -0x10(%ebp),%ecx
  105a1e:	0f b6 45 f7          	movzbl -0x9(%ebp),%eax
  105a22:	8b 55 f8             	mov    -0x8(%ebp),%edx
  105a25:	89 d7                	mov    %edx,%edi
  105a27:	f3 aa                	rep stos %al,%es:(%edi)
  105a29:	89 fa                	mov    %edi,%edx
  105a2b:	89 4d ec             	mov    %ecx,-0x14(%ebp)
  105a2e:	89 55 e8             	mov    %edx,-0x18(%ebp)
        "rep; stosb;"
        : "=&c" (d0), "=&D" (d1)
        : "0" (n), "a" (c), "1" (s)
        : "memory");
    return s;
  105a31:	8b 45 f8             	mov    -0x8(%ebp),%eax
    while (n -- > 0) {
        *p ++ = c;
    }
    return s;
#endif /* __HAVE_ARCH_MEMSET */
}
  105a34:	83 c4 24             	add    $0x24,%esp
  105a37:	5f                   	pop    %edi
  105a38:	5d                   	pop    %ebp
  105a39:	c3                   	ret    

00105a3a <memmove>:
 * @n:      number of bytes to copy
 *
 * The memmove() function returns @dst.
 * */
void *
memmove(void *dst, const void *src, size_t n) {
  105a3a:	f3 0f 1e fb          	endbr32 
  105a3e:	55                   	push   %ebp
  105a3f:	89 e5                	mov    %esp,%ebp
  105a41:	57                   	push   %edi
  105a42:	56                   	push   %esi
  105a43:	53                   	push   %ebx
  105a44:	83 ec 30             	sub    $0x30,%esp
  105a47:	8b 45 08             	mov    0x8(%ebp),%eax
  105a4a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  105a4d:	8b 45 0c             	mov    0xc(%ebp),%eax
  105a50:	89 45 ec             	mov    %eax,-0x14(%ebp)
  105a53:	8b 45 10             	mov    0x10(%ebp),%eax
  105a56:	89 45 e8             	mov    %eax,-0x18(%ebp)

#ifndef __HAVE_ARCH_MEMMOVE
#define __HAVE_ARCH_MEMMOVE
static inline void *
__memmove(void *dst, const void *src, size_t n) {
    if (dst < src) {
  105a59:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105a5c:	3b 45 ec             	cmp    -0x14(%ebp),%eax
  105a5f:	73 42                	jae    105aa3 <memmove+0x69>
  105a61:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105a64:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  105a67:	8b 45 ec             	mov    -0x14(%ebp),%eax
  105a6a:	89 45 e0             	mov    %eax,-0x20(%ebp)
  105a6d:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105a70:	89 45 dc             	mov    %eax,-0x24(%ebp)
        "andl $3, %%ecx;"
        "jz 1f;"
        "rep; movsb;"
        "1:"
        : "=&c" (d0), "=&D" (d1), "=&S" (d2)
        : "0" (n / 4), "g" (n), "1" (dst), "2" (src)
  105a73:	8b 45 dc             	mov    -0x24(%ebp),%eax
  105a76:	c1 e8 02             	shr    $0x2,%eax
  105a79:	89 c1                	mov    %eax,%ecx
    asm volatile (
  105a7b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  105a7e:	8b 45 e0             	mov    -0x20(%ebp),%eax
  105a81:	89 d7                	mov    %edx,%edi
  105a83:	89 c6                	mov    %eax,%esi
  105a85:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  105a87:	8b 4d dc             	mov    -0x24(%ebp),%ecx
  105a8a:	83 e1 03             	and    $0x3,%ecx
  105a8d:	74 02                	je     105a91 <memmove+0x57>
  105a8f:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
  105a91:	89 f0                	mov    %esi,%eax
  105a93:	89 fa                	mov    %edi,%edx
  105a95:	89 4d d8             	mov    %ecx,-0x28(%ebp)
  105a98:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  105a9b:	89 45 d0             	mov    %eax,-0x30(%ebp)
        : "memory");
    return dst;
  105a9e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
        return __memcpy(dst, src, n);
  105aa1:	eb 36                	jmp    105ad9 <memmove+0x9f>
        : "0" (n), "1" (n - 1 + src), "2" (n - 1 + dst)
  105aa3:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105aa6:	8d 50 ff             	lea    -0x1(%eax),%edx
  105aa9:	8b 45 ec             	mov    -0x14(%ebp),%eax
  105aac:	01 c2                	add    %eax,%edx
  105aae:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105ab1:	8d 48 ff             	lea    -0x1(%eax),%ecx
  105ab4:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105ab7:	8d 1c 01             	lea    (%ecx,%eax,1),%ebx
    asm volatile (
  105aba:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105abd:	89 c1                	mov    %eax,%ecx
  105abf:	89 d8                	mov    %ebx,%eax
  105ac1:	89 d6                	mov    %edx,%esi
  105ac3:	89 c7                	mov    %eax,%edi
  105ac5:	fd                   	std    
  105ac6:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
  105ac8:	fc                   	cld    
  105ac9:	89 f8                	mov    %edi,%eax
  105acb:	89 f2                	mov    %esi,%edx
  105acd:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  105ad0:	89 55 c8             	mov    %edx,-0x38(%ebp)
  105ad3:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    return dst;
  105ad6:	8b 45 f0             	mov    -0x10(%ebp),%eax
            *d ++ = *s ++;
        }
    }
    return dst;
#endif /* __HAVE_ARCH_MEMMOVE */
}
  105ad9:	83 c4 30             	add    $0x30,%esp
  105adc:	5b                   	pop    %ebx
  105add:	5e                   	pop    %esi
  105ade:	5f                   	pop    %edi
  105adf:	5d                   	pop    %ebp
  105ae0:	c3                   	ret    

00105ae1 <memcpy>:
 * it always copies exactly @n bytes. To avoid overflows, the size of arrays pointed
 * by both @src and @dst, should be at least @n bytes, and should not overlap
 * (for overlapping memory area, memmove is a safer approach).
 * */
void *
memcpy(void *dst, const void *src, size_t n) {
  105ae1:	f3 0f 1e fb          	endbr32 
  105ae5:	55                   	push   %ebp
  105ae6:	89 e5                	mov    %esp,%ebp
  105ae8:	57                   	push   %edi
  105ae9:	56                   	push   %esi
  105aea:	83 ec 20             	sub    $0x20,%esp
  105aed:	8b 45 08             	mov    0x8(%ebp),%eax
  105af0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  105af3:	8b 45 0c             	mov    0xc(%ebp),%eax
  105af6:	89 45 f0             	mov    %eax,-0x10(%ebp)
  105af9:	8b 45 10             	mov    0x10(%ebp),%eax
  105afc:	89 45 ec             	mov    %eax,-0x14(%ebp)
        : "0" (n / 4), "g" (n), "1" (dst), "2" (src)
  105aff:	8b 45 ec             	mov    -0x14(%ebp),%eax
  105b02:	c1 e8 02             	shr    $0x2,%eax
  105b05:	89 c1                	mov    %eax,%ecx
    asm volatile (
  105b07:	8b 55 f4             	mov    -0xc(%ebp),%edx
  105b0a:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105b0d:	89 d7                	mov    %edx,%edi
  105b0f:	89 c6                	mov    %eax,%esi
  105b11:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  105b13:	8b 4d ec             	mov    -0x14(%ebp),%ecx
  105b16:	83 e1 03             	and    $0x3,%ecx
  105b19:	74 02                	je     105b1d <memcpy+0x3c>
  105b1b:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
  105b1d:	89 f0                	mov    %esi,%eax
  105b1f:	89 fa                	mov    %edi,%edx
  105b21:	89 4d e8             	mov    %ecx,-0x18(%ebp)
  105b24:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  105b27:	89 45 e0             	mov    %eax,-0x20(%ebp)
    return dst;
  105b2a:	8b 45 f4             	mov    -0xc(%ebp),%eax
    while (n -- > 0) {
        *d ++ = *s ++;
    }
    return dst;
#endif /* __HAVE_ARCH_MEMCPY */
}
  105b2d:	83 c4 20             	add    $0x20,%esp
  105b30:	5e                   	pop    %esi
  105b31:	5f                   	pop    %edi
  105b32:	5d                   	pop    %ebp
  105b33:	c3                   	ret    

00105b34 <memcmp>:
 *   match in both memory blocks has a greater value in @v1 than in @v2
 *   as if evaluated as unsigned char values;
 * - And a value less than zero indicates the opposite.
 * */
int
memcmp(const void *v1, const void *v2, size_t n) {
  105b34:	f3 0f 1e fb          	endbr32 
  105b38:	55                   	push   %ebp
  105b39:	89 e5                	mov    %esp,%ebp
  105b3b:	83 ec 10             	sub    $0x10,%esp
    const char *s1 = (const char *)v1;
  105b3e:	8b 45 08             	mov    0x8(%ebp),%eax
  105b41:	89 45 fc             	mov    %eax,-0x4(%ebp)
    const char *s2 = (const char *)v2;
  105b44:	8b 45 0c             	mov    0xc(%ebp),%eax
  105b47:	89 45 f8             	mov    %eax,-0x8(%ebp)
    while (n -- > 0) {
  105b4a:	eb 2e                	jmp    105b7a <memcmp+0x46>
        if (*s1 != *s2) {
  105b4c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  105b4f:	0f b6 10             	movzbl (%eax),%edx
  105b52:	8b 45 f8             	mov    -0x8(%ebp),%eax
  105b55:	0f b6 00             	movzbl (%eax),%eax
  105b58:	38 c2                	cmp    %al,%dl
  105b5a:	74 18                	je     105b74 <memcmp+0x40>
            return (int)((unsigned char)*s1 - (unsigned char)*s2);
  105b5c:	8b 45 fc             	mov    -0x4(%ebp),%eax
  105b5f:	0f b6 00             	movzbl (%eax),%eax
  105b62:	0f b6 d0             	movzbl %al,%edx
  105b65:	8b 45 f8             	mov    -0x8(%ebp),%eax
  105b68:	0f b6 00             	movzbl (%eax),%eax
  105b6b:	0f b6 c0             	movzbl %al,%eax
  105b6e:	29 c2                	sub    %eax,%edx
  105b70:	89 d0                	mov    %edx,%eax
  105b72:	eb 18                	jmp    105b8c <memcmp+0x58>
        }
        s1 ++, s2 ++;
  105b74:	ff 45 fc             	incl   -0x4(%ebp)
  105b77:	ff 45 f8             	incl   -0x8(%ebp)
    while (n -- > 0) {
  105b7a:	8b 45 10             	mov    0x10(%ebp),%eax
  105b7d:	8d 50 ff             	lea    -0x1(%eax),%edx
  105b80:	89 55 10             	mov    %edx,0x10(%ebp)
  105b83:	85 c0                	test   %eax,%eax
  105b85:	75 c5                	jne    105b4c <memcmp+0x18>
    }
    return 0;
  105b87:	b8 00 00 00 00       	mov    $0x0,%eax
}
  105b8c:	c9                   	leave  
  105b8d:	c3                   	ret    

00105b8e <printnum>:
 * @width:      maximum number of digits, if the actual width is less than @width, use @padc instead
 * @padc:       character that padded on the left if the actual width is less than @width
 * */
static void
printnum(void (*putch)(int, void*), void *putdat,
        unsigned long long num, unsigned base, int width, int padc) {
  105b8e:	f3 0f 1e fb          	endbr32 
  105b92:	55                   	push   %ebp
  105b93:	89 e5                	mov    %esp,%ebp
  105b95:	83 ec 58             	sub    $0x58,%esp
  105b98:	8b 45 10             	mov    0x10(%ebp),%eax
  105b9b:	89 45 d0             	mov    %eax,-0x30(%ebp)
  105b9e:	8b 45 14             	mov    0x14(%ebp),%eax
  105ba1:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    unsigned long long result = num;
  105ba4:	8b 45 d0             	mov    -0x30(%ebp),%eax
  105ba7:	8b 55 d4             	mov    -0x2c(%ebp),%edx
  105baa:	89 45 e8             	mov    %eax,-0x18(%ebp)
  105bad:	89 55 ec             	mov    %edx,-0x14(%ebp)
    unsigned mod = do_div(result, base);
  105bb0:	8b 45 18             	mov    0x18(%ebp),%eax
  105bb3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  105bb6:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105bb9:	8b 55 ec             	mov    -0x14(%ebp),%edx
  105bbc:	89 45 e0             	mov    %eax,-0x20(%ebp)
  105bbf:	89 55 f0             	mov    %edx,-0x10(%ebp)
  105bc2:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105bc5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  105bc8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
  105bcc:	74 1c                	je     105bea <printnum+0x5c>
  105bce:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105bd1:	ba 00 00 00 00       	mov    $0x0,%edx
  105bd6:	f7 75 e4             	divl   -0x1c(%ebp)
  105bd9:	89 55 f4             	mov    %edx,-0xc(%ebp)
  105bdc:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105bdf:	ba 00 00 00 00       	mov    $0x0,%edx
  105be4:	f7 75 e4             	divl   -0x1c(%ebp)
  105be7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  105bea:	8b 45 e0             	mov    -0x20(%ebp),%eax
  105bed:	8b 55 f4             	mov    -0xc(%ebp),%edx
  105bf0:	f7 75 e4             	divl   -0x1c(%ebp)
  105bf3:	89 45 e0             	mov    %eax,-0x20(%ebp)
  105bf6:	89 55 dc             	mov    %edx,-0x24(%ebp)
  105bf9:	8b 45 e0             	mov    -0x20(%ebp),%eax
  105bfc:	8b 55 f0             	mov    -0x10(%ebp),%edx
  105bff:	89 45 e8             	mov    %eax,-0x18(%ebp)
  105c02:	89 55 ec             	mov    %edx,-0x14(%ebp)
  105c05:	8b 45 dc             	mov    -0x24(%ebp),%eax
  105c08:	89 45 d8             	mov    %eax,-0x28(%ebp)

    // first recursively print all preceding (more significant) digits
    if (num >= base) {
  105c0b:	8b 45 18             	mov    0x18(%ebp),%eax
  105c0e:	ba 00 00 00 00       	mov    $0x0,%edx
  105c13:	8b 4d d4             	mov    -0x2c(%ebp),%ecx
  105c16:	39 45 d0             	cmp    %eax,-0x30(%ebp)
  105c19:	19 d1                	sbb    %edx,%ecx
  105c1b:	72 4c                	jb     105c69 <printnum+0xdb>
        printnum(putch, putdat, result, base, width - 1, padc);
  105c1d:	8b 45 1c             	mov    0x1c(%ebp),%eax
  105c20:	8d 50 ff             	lea    -0x1(%eax),%edx
  105c23:	8b 45 20             	mov    0x20(%ebp),%eax
  105c26:	89 44 24 18          	mov    %eax,0x18(%esp)
  105c2a:	89 54 24 14          	mov    %edx,0x14(%esp)
  105c2e:	8b 45 18             	mov    0x18(%ebp),%eax
  105c31:	89 44 24 10          	mov    %eax,0x10(%esp)
  105c35:	8b 45 e8             	mov    -0x18(%ebp),%eax
  105c38:	8b 55 ec             	mov    -0x14(%ebp),%edx
  105c3b:	89 44 24 08          	mov    %eax,0x8(%esp)
  105c3f:	89 54 24 0c          	mov    %edx,0xc(%esp)
  105c43:	8b 45 0c             	mov    0xc(%ebp),%eax
  105c46:	89 44 24 04          	mov    %eax,0x4(%esp)
  105c4a:	8b 45 08             	mov    0x8(%ebp),%eax
  105c4d:	89 04 24             	mov    %eax,(%esp)
  105c50:	e8 39 ff ff ff       	call   105b8e <printnum>
  105c55:	eb 1b                	jmp    105c72 <printnum+0xe4>
    } else {
        // print any needed pad characters before first digit
        while (-- width > 0)
            putch(padc, putdat);
  105c57:	8b 45 0c             	mov    0xc(%ebp),%eax
  105c5a:	89 44 24 04          	mov    %eax,0x4(%esp)
  105c5e:	8b 45 20             	mov    0x20(%ebp),%eax
  105c61:	89 04 24             	mov    %eax,(%esp)
  105c64:	8b 45 08             	mov    0x8(%ebp),%eax
  105c67:	ff d0                	call   *%eax
        while (-- width > 0)
  105c69:	ff 4d 1c             	decl   0x1c(%ebp)
  105c6c:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
  105c70:	7f e5                	jg     105c57 <printnum+0xc9>
    }
    // then print this (the least significant) digit
    putch("0123456789abcdef"[mod], putdat);
  105c72:	8b 45 d8             	mov    -0x28(%ebp),%eax
  105c75:	05 1c 74 10 00       	add    $0x10741c,%eax
  105c7a:	0f b6 00             	movzbl (%eax),%eax
  105c7d:	0f be c0             	movsbl %al,%eax
  105c80:	8b 55 0c             	mov    0xc(%ebp),%edx
  105c83:	89 54 24 04          	mov    %edx,0x4(%esp)
  105c87:	89 04 24             	mov    %eax,(%esp)
  105c8a:	8b 45 08             	mov    0x8(%ebp),%eax
  105c8d:	ff d0                	call   *%eax
}
  105c8f:	90                   	nop
  105c90:	c9                   	leave  
  105c91:	c3                   	ret    

00105c92 <getuint>:
 * getuint - get an unsigned int of various possible sizes from a varargs list
 * @ap:         a varargs list pointer
 * @lflag:      determines the size of the vararg that @ap points to
 * */
static unsigned long long
getuint(va_list *ap, int lflag) {
  105c92:	f3 0f 1e fb          	endbr32 
  105c96:	55                   	push   %ebp
  105c97:	89 e5                	mov    %esp,%ebp
    if (lflag >= 2) {
  105c99:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  105c9d:	7e 14                	jle    105cb3 <getuint+0x21>
        return va_arg(*ap, unsigned long long);
  105c9f:	8b 45 08             	mov    0x8(%ebp),%eax
  105ca2:	8b 00                	mov    (%eax),%eax
  105ca4:	8d 48 08             	lea    0x8(%eax),%ecx
  105ca7:	8b 55 08             	mov    0x8(%ebp),%edx
  105caa:	89 0a                	mov    %ecx,(%edx)
  105cac:	8b 50 04             	mov    0x4(%eax),%edx
  105caf:	8b 00                	mov    (%eax),%eax
  105cb1:	eb 30                	jmp    105ce3 <getuint+0x51>
    }
    else if (lflag) {
  105cb3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  105cb7:	74 16                	je     105ccf <getuint+0x3d>
        return va_arg(*ap, unsigned long);
  105cb9:	8b 45 08             	mov    0x8(%ebp),%eax
  105cbc:	8b 00                	mov    (%eax),%eax
  105cbe:	8d 48 04             	lea    0x4(%eax),%ecx
  105cc1:	8b 55 08             	mov    0x8(%ebp),%edx
  105cc4:	89 0a                	mov    %ecx,(%edx)
  105cc6:	8b 00                	mov    (%eax),%eax
  105cc8:	ba 00 00 00 00       	mov    $0x0,%edx
  105ccd:	eb 14                	jmp    105ce3 <getuint+0x51>
    }
    else {
        return va_arg(*ap, unsigned int);
  105ccf:	8b 45 08             	mov    0x8(%ebp),%eax
  105cd2:	8b 00                	mov    (%eax),%eax
  105cd4:	8d 48 04             	lea    0x4(%eax),%ecx
  105cd7:	8b 55 08             	mov    0x8(%ebp),%edx
  105cda:	89 0a                	mov    %ecx,(%edx)
  105cdc:	8b 00                	mov    (%eax),%eax
  105cde:	ba 00 00 00 00       	mov    $0x0,%edx
    }
}
  105ce3:	5d                   	pop    %ebp
  105ce4:	c3                   	ret    

00105ce5 <getint>:
 * getint - same as getuint but signed, we can't use getuint because of sign extension
 * @ap:         a varargs list pointer
 * @lflag:      determines the size of the vararg that @ap points to
 * */
static long long
getint(va_list *ap, int lflag) {
  105ce5:	f3 0f 1e fb          	endbr32 
  105ce9:	55                   	push   %ebp
  105cea:	89 e5                	mov    %esp,%ebp
    if (lflag >= 2) {
  105cec:	83 7d 0c 01          	cmpl   $0x1,0xc(%ebp)
  105cf0:	7e 14                	jle    105d06 <getint+0x21>
        return va_arg(*ap, long long);
  105cf2:	8b 45 08             	mov    0x8(%ebp),%eax
  105cf5:	8b 00                	mov    (%eax),%eax
  105cf7:	8d 48 08             	lea    0x8(%eax),%ecx
  105cfa:	8b 55 08             	mov    0x8(%ebp),%edx
  105cfd:	89 0a                	mov    %ecx,(%edx)
  105cff:	8b 50 04             	mov    0x4(%eax),%edx
  105d02:	8b 00                	mov    (%eax),%eax
  105d04:	eb 28                	jmp    105d2e <getint+0x49>
    }
    else if (lflag) {
  105d06:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
  105d0a:	74 12                	je     105d1e <getint+0x39>
        return va_arg(*ap, long);
  105d0c:	8b 45 08             	mov    0x8(%ebp),%eax
  105d0f:	8b 00                	mov    (%eax),%eax
  105d11:	8d 48 04             	lea    0x4(%eax),%ecx
  105d14:	8b 55 08             	mov    0x8(%ebp),%edx
  105d17:	89 0a                	mov    %ecx,(%edx)
  105d19:	8b 00                	mov    (%eax),%eax
  105d1b:	99                   	cltd   
  105d1c:	eb 10                	jmp    105d2e <getint+0x49>
    }
    else {
        return va_arg(*ap, int);
  105d1e:	8b 45 08             	mov    0x8(%ebp),%eax
  105d21:	8b 00                	mov    (%eax),%eax
  105d23:	8d 48 04             	lea    0x4(%eax),%ecx
  105d26:	8b 55 08             	mov    0x8(%ebp),%edx
  105d29:	89 0a                	mov    %ecx,(%edx)
  105d2b:	8b 00                	mov    (%eax),%eax
  105d2d:	99                   	cltd   
    }
}
  105d2e:	5d                   	pop    %ebp
  105d2f:	c3                   	ret    

00105d30 <printfmt>:
 * @putch:      specified putch function, print a single character
 * @putdat:     used by @putch function
 * @fmt:        the format string to use
 * */
void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...) {
  105d30:	f3 0f 1e fb          	endbr32 
  105d34:	55                   	push   %ebp
  105d35:	89 e5                	mov    %esp,%ebp
  105d37:	83 ec 28             	sub    $0x28,%esp
    va_list ap;

    va_start(ap, fmt);
  105d3a:	8d 45 14             	lea    0x14(%ebp),%eax
  105d3d:	89 45 f4             	mov    %eax,-0xc(%ebp)
    vprintfmt(putch, putdat, fmt, ap);
  105d40:	8b 45 f4             	mov    -0xc(%ebp),%eax
  105d43:	89 44 24 0c          	mov    %eax,0xc(%esp)
  105d47:	8b 45 10             	mov    0x10(%ebp),%eax
  105d4a:	89 44 24 08          	mov    %eax,0x8(%esp)
  105d4e:	8b 45 0c             	mov    0xc(%ebp),%eax
  105d51:	89 44 24 04          	mov    %eax,0x4(%esp)
  105d55:	8b 45 08             	mov    0x8(%ebp),%eax
  105d58:	89 04 24             	mov    %eax,(%esp)
  105d5b:	e8 03 00 00 00       	call   105d63 <vprintfmt>
    va_end(ap);
}
  105d60:	90                   	nop
  105d61:	c9                   	leave  
  105d62:	c3                   	ret    

00105d63 <vprintfmt>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want printfmt() instead.
 * */
void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap) {
  105d63:	f3 0f 1e fb          	endbr32 
  105d67:	55                   	push   %ebp
  105d68:	89 e5                	mov    %esp,%ebp
  105d6a:	56                   	push   %esi
  105d6b:	53                   	push   %ebx
  105d6c:	83 ec 40             	sub    $0x40,%esp
    register int ch, err;
    unsigned long long num;
    int base, width, precision, lflag, altflag;

    while (1) {
        while ((ch = *(unsigned char *)fmt ++) != '%') {
  105d6f:	eb 17                	jmp    105d88 <vprintfmt+0x25>
            if (ch == '\0') {
  105d71:	85 db                	test   %ebx,%ebx
  105d73:	0f 84 c0 03 00 00    	je     106139 <vprintfmt+0x3d6>
                return;
            }
            putch(ch, putdat);
  105d79:	8b 45 0c             	mov    0xc(%ebp),%eax
  105d7c:	89 44 24 04          	mov    %eax,0x4(%esp)
  105d80:	89 1c 24             	mov    %ebx,(%esp)
  105d83:	8b 45 08             	mov    0x8(%ebp),%eax
  105d86:	ff d0                	call   *%eax
        while ((ch = *(unsigned char *)fmt ++) != '%') {
  105d88:	8b 45 10             	mov    0x10(%ebp),%eax
  105d8b:	8d 50 01             	lea    0x1(%eax),%edx
  105d8e:	89 55 10             	mov    %edx,0x10(%ebp)
  105d91:	0f b6 00             	movzbl (%eax),%eax
  105d94:	0f b6 d8             	movzbl %al,%ebx
  105d97:	83 fb 25             	cmp    $0x25,%ebx
  105d9a:	75 d5                	jne    105d71 <vprintfmt+0xe>
        }

        // Process a %-escape sequence
        char padc = ' ';
  105d9c:	c6 45 db 20          	movb   $0x20,-0x25(%ebp)
        width = precision = -1;
  105da0:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  105da7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105daa:	89 45 e8             	mov    %eax,-0x18(%ebp)
        lflag = altflag = 0;
  105dad:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
  105db4:	8b 45 dc             	mov    -0x24(%ebp),%eax
  105db7:	89 45 e0             	mov    %eax,-0x20(%ebp)

    reswitch:
        switch (ch = *(unsigned char *)fmt ++) {
  105dba:	8b 45 10             	mov    0x10(%ebp),%eax
  105dbd:	8d 50 01             	lea    0x1(%eax),%edx
  105dc0:	89 55 10             	mov    %edx,0x10(%ebp)
  105dc3:	0f b6 00             	movzbl (%eax),%eax
  105dc6:	0f b6 d8             	movzbl %al,%ebx
  105dc9:	8d 43 dd             	lea    -0x23(%ebx),%eax
  105dcc:	83 f8 55             	cmp    $0x55,%eax
  105dcf:	0f 87 38 03 00 00    	ja     10610d <vprintfmt+0x3aa>
  105dd5:	8b 04 85 40 74 10 00 	mov    0x107440(,%eax,4),%eax
  105ddc:	3e ff e0             	notrack jmp *%eax

        // flag to pad on the right
        case '-':
            padc = '-';
  105ddf:	c6 45 db 2d          	movb   $0x2d,-0x25(%ebp)
            goto reswitch;
  105de3:	eb d5                	jmp    105dba <vprintfmt+0x57>

        // flag to pad with 0's instead of spaces
        case '0':
            padc = '0';
  105de5:	c6 45 db 30          	movb   $0x30,-0x25(%ebp)
            goto reswitch;
  105de9:	eb cf                	jmp    105dba <vprintfmt+0x57>

        // width field
        case '1' ... '9':
            for (precision = 0; ; ++ fmt) {
  105deb:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
                precision = precision * 10 + ch - '0';
  105df2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  105df5:	89 d0                	mov    %edx,%eax
  105df7:	c1 e0 02             	shl    $0x2,%eax
  105dfa:	01 d0                	add    %edx,%eax
  105dfc:	01 c0                	add    %eax,%eax
  105dfe:	01 d8                	add    %ebx,%eax
  105e00:	83 e8 30             	sub    $0x30,%eax
  105e03:	89 45 e4             	mov    %eax,-0x1c(%ebp)
                ch = *fmt;
  105e06:	8b 45 10             	mov    0x10(%ebp),%eax
  105e09:	0f b6 00             	movzbl (%eax),%eax
  105e0c:	0f be d8             	movsbl %al,%ebx
                if (ch < '0' || ch > '9') {
  105e0f:	83 fb 2f             	cmp    $0x2f,%ebx
  105e12:	7e 38                	jle    105e4c <vprintfmt+0xe9>
  105e14:	83 fb 39             	cmp    $0x39,%ebx
  105e17:	7f 33                	jg     105e4c <vprintfmt+0xe9>
            for (precision = 0; ; ++ fmt) {
  105e19:	ff 45 10             	incl   0x10(%ebp)
                precision = precision * 10 + ch - '0';
  105e1c:	eb d4                	jmp    105df2 <vprintfmt+0x8f>
                }
            }
            goto process_precision;

        case '*':
            precision = va_arg(ap, int);
  105e1e:	8b 45 14             	mov    0x14(%ebp),%eax
  105e21:	8d 50 04             	lea    0x4(%eax),%edx
  105e24:	89 55 14             	mov    %edx,0x14(%ebp)
  105e27:	8b 00                	mov    (%eax),%eax
  105e29:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            goto process_precision;
  105e2c:	eb 1f                	jmp    105e4d <vprintfmt+0xea>

        case '.':
            if (width < 0)
  105e2e:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105e32:	79 86                	jns    105dba <vprintfmt+0x57>
                width = 0;
  105e34:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
            goto reswitch;
  105e3b:	e9 7a ff ff ff       	jmp    105dba <vprintfmt+0x57>

        case '#':
            altflag = 1;
  105e40:	c7 45 dc 01 00 00 00 	movl   $0x1,-0x24(%ebp)
            goto reswitch;
  105e47:	e9 6e ff ff ff       	jmp    105dba <vprintfmt+0x57>
            goto process_precision;
  105e4c:	90                   	nop

        process_precision:
            if (width < 0)
  105e4d:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105e51:	0f 89 63 ff ff ff    	jns    105dba <vprintfmt+0x57>
                width = precision, precision = -1;
  105e57:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105e5a:	89 45 e8             	mov    %eax,-0x18(%ebp)
  105e5d:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
            goto reswitch;
  105e64:	e9 51 ff ff ff       	jmp    105dba <vprintfmt+0x57>

        // long flag (doubled for long long)
        case 'l':
            lflag ++;
  105e69:	ff 45 e0             	incl   -0x20(%ebp)
            goto reswitch;
  105e6c:	e9 49 ff ff ff       	jmp    105dba <vprintfmt+0x57>

        // character
        case 'c':
            putch(va_arg(ap, int), putdat);
  105e71:	8b 45 14             	mov    0x14(%ebp),%eax
  105e74:	8d 50 04             	lea    0x4(%eax),%edx
  105e77:	89 55 14             	mov    %edx,0x14(%ebp)
  105e7a:	8b 00                	mov    (%eax),%eax
  105e7c:	8b 55 0c             	mov    0xc(%ebp),%edx
  105e7f:	89 54 24 04          	mov    %edx,0x4(%esp)
  105e83:	89 04 24             	mov    %eax,(%esp)
  105e86:	8b 45 08             	mov    0x8(%ebp),%eax
  105e89:	ff d0                	call   *%eax
            break;
  105e8b:	e9 a4 02 00 00       	jmp    106134 <vprintfmt+0x3d1>

        // error message
        case 'e':
            err = va_arg(ap, int);
  105e90:	8b 45 14             	mov    0x14(%ebp),%eax
  105e93:	8d 50 04             	lea    0x4(%eax),%edx
  105e96:	89 55 14             	mov    %edx,0x14(%ebp)
  105e99:	8b 18                	mov    (%eax),%ebx
            if (err < 0) {
  105e9b:	85 db                	test   %ebx,%ebx
  105e9d:	79 02                	jns    105ea1 <vprintfmt+0x13e>
                err = -err;
  105e9f:	f7 db                	neg    %ebx
            }
            if (err > MAXERROR || (p = error_string[err]) == NULL) {
  105ea1:	83 fb 06             	cmp    $0x6,%ebx
  105ea4:	7f 0b                	jg     105eb1 <vprintfmt+0x14e>
  105ea6:	8b 34 9d 00 74 10 00 	mov    0x107400(,%ebx,4),%esi
  105ead:	85 f6                	test   %esi,%esi
  105eaf:	75 23                	jne    105ed4 <vprintfmt+0x171>
                printfmt(putch, putdat, "error %d", err);
  105eb1:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
  105eb5:	c7 44 24 08 2d 74 10 	movl   $0x10742d,0x8(%esp)
  105ebc:	00 
  105ebd:	8b 45 0c             	mov    0xc(%ebp),%eax
  105ec0:	89 44 24 04          	mov    %eax,0x4(%esp)
  105ec4:	8b 45 08             	mov    0x8(%ebp),%eax
  105ec7:	89 04 24             	mov    %eax,(%esp)
  105eca:	e8 61 fe ff ff       	call   105d30 <printfmt>
            }
            else {
                printfmt(putch, putdat, "%s", p);
            }
            break;
  105ecf:	e9 60 02 00 00       	jmp    106134 <vprintfmt+0x3d1>
                printfmt(putch, putdat, "%s", p);
  105ed4:	89 74 24 0c          	mov    %esi,0xc(%esp)
  105ed8:	c7 44 24 08 36 74 10 	movl   $0x107436,0x8(%esp)
  105edf:	00 
  105ee0:	8b 45 0c             	mov    0xc(%ebp),%eax
  105ee3:	89 44 24 04          	mov    %eax,0x4(%esp)
  105ee7:	8b 45 08             	mov    0x8(%ebp),%eax
  105eea:	89 04 24             	mov    %eax,(%esp)
  105eed:	e8 3e fe ff ff       	call   105d30 <printfmt>
            break;
  105ef2:	e9 3d 02 00 00       	jmp    106134 <vprintfmt+0x3d1>

        // string
        case 's':
            if ((p = va_arg(ap, char *)) == NULL) {
  105ef7:	8b 45 14             	mov    0x14(%ebp),%eax
  105efa:	8d 50 04             	lea    0x4(%eax),%edx
  105efd:	89 55 14             	mov    %edx,0x14(%ebp)
  105f00:	8b 30                	mov    (%eax),%esi
  105f02:	85 f6                	test   %esi,%esi
  105f04:	75 05                	jne    105f0b <vprintfmt+0x1a8>
                p = "(null)";
  105f06:	be 39 74 10 00       	mov    $0x107439,%esi
            }
            if (width > 0 && padc != '-') {
  105f0b:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105f0f:	7e 76                	jle    105f87 <vprintfmt+0x224>
  105f11:	80 7d db 2d          	cmpb   $0x2d,-0x25(%ebp)
  105f15:	74 70                	je     105f87 <vprintfmt+0x224>
                for (width -= strnlen(p, precision); width > 0; width --) {
  105f17:	8b 45 e4             	mov    -0x1c(%ebp),%eax
  105f1a:	89 44 24 04          	mov    %eax,0x4(%esp)
  105f1e:	89 34 24             	mov    %esi,(%esp)
  105f21:	e8 ba f7 ff ff       	call   1056e0 <strnlen>
  105f26:	8b 55 e8             	mov    -0x18(%ebp),%edx
  105f29:	29 c2                	sub    %eax,%edx
  105f2b:	89 d0                	mov    %edx,%eax
  105f2d:	89 45 e8             	mov    %eax,-0x18(%ebp)
  105f30:	eb 16                	jmp    105f48 <vprintfmt+0x1e5>
                    putch(padc, putdat);
  105f32:	0f be 45 db          	movsbl -0x25(%ebp),%eax
  105f36:	8b 55 0c             	mov    0xc(%ebp),%edx
  105f39:	89 54 24 04          	mov    %edx,0x4(%esp)
  105f3d:	89 04 24             	mov    %eax,(%esp)
  105f40:	8b 45 08             	mov    0x8(%ebp),%eax
  105f43:	ff d0                	call   *%eax
                for (width -= strnlen(p, precision); width > 0; width --) {
  105f45:	ff 4d e8             	decl   -0x18(%ebp)
  105f48:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105f4c:	7f e4                	jg     105f32 <vprintfmt+0x1cf>
                }
            }
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
  105f4e:	eb 37                	jmp    105f87 <vprintfmt+0x224>
                if (altflag && (ch < ' ' || ch > '~')) {
  105f50:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
  105f54:	74 1f                	je     105f75 <vprintfmt+0x212>
  105f56:	83 fb 1f             	cmp    $0x1f,%ebx
  105f59:	7e 05                	jle    105f60 <vprintfmt+0x1fd>
  105f5b:	83 fb 7e             	cmp    $0x7e,%ebx
  105f5e:	7e 15                	jle    105f75 <vprintfmt+0x212>
                    putch('?', putdat);
  105f60:	8b 45 0c             	mov    0xc(%ebp),%eax
  105f63:	89 44 24 04          	mov    %eax,0x4(%esp)
  105f67:	c7 04 24 3f 00 00 00 	movl   $0x3f,(%esp)
  105f6e:	8b 45 08             	mov    0x8(%ebp),%eax
  105f71:	ff d0                	call   *%eax
  105f73:	eb 0f                	jmp    105f84 <vprintfmt+0x221>
                }
                else {
                    putch(ch, putdat);
  105f75:	8b 45 0c             	mov    0xc(%ebp),%eax
  105f78:	89 44 24 04          	mov    %eax,0x4(%esp)
  105f7c:	89 1c 24             	mov    %ebx,(%esp)
  105f7f:	8b 45 08             	mov    0x8(%ebp),%eax
  105f82:	ff d0                	call   *%eax
            for (; (ch = *p ++) != '\0' && (precision < 0 || -- precision >= 0); width --) {
  105f84:	ff 4d e8             	decl   -0x18(%ebp)
  105f87:	89 f0                	mov    %esi,%eax
  105f89:	8d 70 01             	lea    0x1(%eax),%esi
  105f8c:	0f b6 00             	movzbl (%eax),%eax
  105f8f:	0f be d8             	movsbl %al,%ebx
  105f92:	85 db                	test   %ebx,%ebx
  105f94:	74 27                	je     105fbd <vprintfmt+0x25a>
  105f96:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  105f9a:	78 b4                	js     105f50 <vprintfmt+0x1ed>
  105f9c:	ff 4d e4             	decl   -0x1c(%ebp)
  105f9f:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
  105fa3:	79 ab                	jns    105f50 <vprintfmt+0x1ed>
                }
            }
            for (; width > 0; width --) {
  105fa5:	eb 16                	jmp    105fbd <vprintfmt+0x25a>
                putch(' ', putdat);
  105fa7:	8b 45 0c             	mov    0xc(%ebp),%eax
  105faa:	89 44 24 04          	mov    %eax,0x4(%esp)
  105fae:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
  105fb5:	8b 45 08             	mov    0x8(%ebp),%eax
  105fb8:	ff d0                	call   *%eax
            for (; width > 0; width --) {
  105fba:	ff 4d e8             	decl   -0x18(%ebp)
  105fbd:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
  105fc1:	7f e4                	jg     105fa7 <vprintfmt+0x244>
            }
            break;
  105fc3:	e9 6c 01 00 00       	jmp    106134 <vprintfmt+0x3d1>

        // (signed) decimal
        case 'd':
            num = getint(&ap, lflag);
  105fc8:	8b 45 e0             	mov    -0x20(%ebp),%eax
  105fcb:	89 44 24 04          	mov    %eax,0x4(%esp)
  105fcf:	8d 45 14             	lea    0x14(%ebp),%eax
  105fd2:	89 04 24             	mov    %eax,(%esp)
  105fd5:	e8 0b fd ff ff       	call   105ce5 <getint>
  105fda:	89 45 f0             	mov    %eax,-0x10(%ebp)
  105fdd:	89 55 f4             	mov    %edx,-0xc(%ebp)
            if ((long long)num < 0) {
  105fe0:	8b 45 f0             	mov    -0x10(%ebp),%eax
  105fe3:	8b 55 f4             	mov    -0xc(%ebp),%edx
  105fe6:	85 d2                	test   %edx,%edx
  105fe8:	79 26                	jns    106010 <vprintfmt+0x2ad>
                putch('-', putdat);
  105fea:	8b 45 0c             	mov    0xc(%ebp),%eax
  105fed:	89 44 24 04          	mov    %eax,0x4(%esp)
  105ff1:	c7 04 24 2d 00 00 00 	movl   $0x2d,(%esp)
  105ff8:	8b 45 08             	mov    0x8(%ebp),%eax
  105ffb:	ff d0                	call   *%eax
                num = -(long long)num;
  105ffd:	8b 45 f0             	mov    -0x10(%ebp),%eax
  106000:	8b 55 f4             	mov    -0xc(%ebp),%edx
  106003:	f7 d8                	neg    %eax
  106005:	83 d2 00             	adc    $0x0,%edx
  106008:	f7 da                	neg    %edx
  10600a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  10600d:	89 55 f4             	mov    %edx,-0xc(%ebp)
            }
            base = 10;
  106010:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
            goto number;
  106017:	e9 a8 00 00 00       	jmp    1060c4 <vprintfmt+0x361>

        // unsigned decimal
        case 'u':
            num = getuint(&ap, lflag);
  10601c:	8b 45 e0             	mov    -0x20(%ebp),%eax
  10601f:	89 44 24 04          	mov    %eax,0x4(%esp)
  106023:	8d 45 14             	lea    0x14(%ebp),%eax
  106026:	89 04 24             	mov    %eax,(%esp)
  106029:	e8 64 fc ff ff       	call   105c92 <getuint>
  10602e:	89 45 f0             	mov    %eax,-0x10(%ebp)
  106031:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 10;
  106034:	c7 45 ec 0a 00 00 00 	movl   $0xa,-0x14(%ebp)
            goto number;
  10603b:	e9 84 00 00 00       	jmp    1060c4 <vprintfmt+0x361>

        // (unsigned) octal
        case 'o':
            num = getuint(&ap, lflag);
  106040:	8b 45 e0             	mov    -0x20(%ebp),%eax
  106043:	89 44 24 04          	mov    %eax,0x4(%esp)
  106047:	8d 45 14             	lea    0x14(%ebp),%eax
  10604a:	89 04 24             	mov    %eax,(%esp)
  10604d:	e8 40 fc ff ff       	call   105c92 <getuint>
  106052:	89 45 f0             	mov    %eax,-0x10(%ebp)
  106055:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 8;
  106058:	c7 45 ec 08 00 00 00 	movl   $0x8,-0x14(%ebp)
            goto number;
  10605f:	eb 63                	jmp    1060c4 <vprintfmt+0x361>

        // pointer
        case 'p':
            putch('0', putdat);
  106061:	8b 45 0c             	mov    0xc(%ebp),%eax
  106064:	89 44 24 04          	mov    %eax,0x4(%esp)
  106068:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
  10606f:	8b 45 08             	mov    0x8(%ebp),%eax
  106072:	ff d0                	call   *%eax
            putch('x', putdat);
  106074:	8b 45 0c             	mov    0xc(%ebp),%eax
  106077:	89 44 24 04          	mov    %eax,0x4(%esp)
  10607b:	c7 04 24 78 00 00 00 	movl   $0x78,(%esp)
  106082:	8b 45 08             	mov    0x8(%ebp),%eax
  106085:	ff d0                	call   *%eax
            num = (unsigned long long)(uintptr_t)va_arg(ap, void *);
  106087:	8b 45 14             	mov    0x14(%ebp),%eax
  10608a:	8d 50 04             	lea    0x4(%eax),%edx
  10608d:	89 55 14             	mov    %edx,0x14(%ebp)
  106090:	8b 00                	mov    (%eax),%eax
  106092:	89 45 f0             	mov    %eax,-0x10(%ebp)
  106095:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
            base = 16;
  10609c:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
            goto number;
  1060a3:	eb 1f                	jmp    1060c4 <vprintfmt+0x361>

        // (unsigned) hexadecimal
        case 'x':
            num = getuint(&ap, lflag);
  1060a5:	8b 45 e0             	mov    -0x20(%ebp),%eax
  1060a8:	89 44 24 04          	mov    %eax,0x4(%esp)
  1060ac:	8d 45 14             	lea    0x14(%ebp),%eax
  1060af:	89 04 24             	mov    %eax,(%esp)
  1060b2:	e8 db fb ff ff       	call   105c92 <getuint>
  1060b7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  1060ba:	89 55 f4             	mov    %edx,-0xc(%ebp)
            base = 16;
  1060bd:	c7 45 ec 10 00 00 00 	movl   $0x10,-0x14(%ebp)
        number:
            printnum(putch, putdat, num, base, width, padc);
  1060c4:	0f be 55 db          	movsbl -0x25(%ebp),%edx
  1060c8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  1060cb:	89 54 24 18          	mov    %edx,0x18(%esp)
  1060cf:	8b 55 e8             	mov    -0x18(%ebp),%edx
  1060d2:	89 54 24 14          	mov    %edx,0x14(%esp)
  1060d6:	89 44 24 10          	mov    %eax,0x10(%esp)
  1060da:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1060dd:	8b 55 f4             	mov    -0xc(%ebp),%edx
  1060e0:	89 44 24 08          	mov    %eax,0x8(%esp)
  1060e4:	89 54 24 0c          	mov    %edx,0xc(%esp)
  1060e8:	8b 45 0c             	mov    0xc(%ebp),%eax
  1060eb:	89 44 24 04          	mov    %eax,0x4(%esp)
  1060ef:	8b 45 08             	mov    0x8(%ebp),%eax
  1060f2:	89 04 24             	mov    %eax,(%esp)
  1060f5:	e8 94 fa ff ff       	call   105b8e <printnum>
            break;
  1060fa:	eb 38                	jmp    106134 <vprintfmt+0x3d1>

        // escaped '%' character
        case '%':
            putch(ch, putdat);
  1060fc:	8b 45 0c             	mov    0xc(%ebp),%eax
  1060ff:	89 44 24 04          	mov    %eax,0x4(%esp)
  106103:	89 1c 24             	mov    %ebx,(%esp)
  106106:	8b 45 08             	mov    0x8(%ebp),%eax
  106109:	ff d0                	call   *%eax
            break;
  10610b:	eb 27                	jmp    106134 <vprintfmt+0x3d1>

        // unrecognized escape sequence - just print it literally
        default:
            putch('%', putdat);
  10610d:	8b 45 0c             	mov    0xc(%ebp),%eax
  106110:	89 44 24 04          	mov    %eax,0x4(%esp)
  106114:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
  10611b:	8b 45 08             	mov    0x8(%ebp),%eax
  10611e:	ff d0                	call   *%eax
            for (fmt --; fmt[-1] != '%'; fmt --)
  106120:	ff 4d 10             	decl   0x10(%ebp)
  106123:	eb 03                	jmp    106128 <vprintfmt+0x3c5>
  106125:	ff 4d 10             	decl   0x10(%ebp)
  106128:	8b 45 10             	mov    0x10(%ebp),%eax
  10612b:	48                   	dec    %eax
  10612c:	0f b6 00             	movzbl (%eax),%eax
  10612f:	3c 25                	cmp    $0x25,%al
  106131:	75 f2                	jne    106125 <vprintfmt+0x3c2>
                /* do nothing */;
            break;
  106133:	90                   	nop
    while (1) {
  106134:	e9 36 fc ff ff       	jmp    105d6f <vprintfmt+0xc>
                return;
  106139:	90                   	nop
        }
    }
}
  10613a:	83 c4 40             	add    $0x40,%esp
  10613d:	5b                   	pop    %ebx
  10613e:	5e                   	pop    %esi
  10613f:	5d                   	pop    %ebp
  106140:	c3                   	ret    

00106141 <sprintputch>:
 * sprintputch - 'print' a single character in a buffer
 * @ch:         the character will be printed
 * @b:          the buffer to place the character @ch
 * */
static void
sprintputch(int ch, struct sprintbuf *b) {
  106141:	f3 0f 1e fb          	endbr32 
  106145:	55                   	push   %ebp
  106146:	89 e5                	mov    %esp,%ebp
    b->cnt ++;
  106148:	8b 45 0c             	mov    0xc(%ebp),%eax
  10614b:	8b 40 08             	mov    0x8(%eax),%eax
  10614e:	8d 50 01             	lea    0x1(%eax),%edx
  106151:	8b 45 0c             	mov    0xc(%ebp),%eax
  106154:	89 50 08             	mov    %edx,0x8(%eax)
    if (b->buf < b->ebuf) {
  106157:	8b 45 0c             	mov    0xc(%ebp),%eax
  10615a:	8b 10                	mov    (%eax),%edx
  10615c:	8b 45 0c             	mov    0xc(%ebp),%eax
  10615f:	8b 40 04             	mov    0x4(%eax),%eax
  106162:	39 c2                	cmp    %eax,%edx
  106164:	73 12                	jae    106178 <sprintputch+0x37>
        *b->buf ++ = ch;
  106166:	8b 45 0c             	mov    0xc(%ebp),%eax
  106169:	8b 00                	mov    (%eax),%eax
  10616b:	8d 48 01             	lea    0x1(%eax),%ecx
  10616e:	8b 55 0c             	mov    0xc(%ebp),%edx
  106171:	89 0a                	mov    %ecx,(%edx)
  106173:	8b 55 08             	mov    0x8(%ebp),%edx
  106176:	88 10                	mov    %dl,(%eax)
    }
}
  106178:	90                   	nop
  106179:	5d                   	pop    %ebp
  10617a:	c3                   	ret    

0010617b <snprintf>:
 * @str:        the buffer to place the result into
 * @size:       the size of buffer, including the trailing null space
 * @fmt:        the format string to use
 * */
int
snprintf(char *str, size_t size, const char *fmt, ...) {
  10617b:	f3 0f 1e fb          	endbr32 
  10617f:	55                   	push   %ebp
  106180:	89 e5                	mov    %esp,%ebp
  106182:	83 ec 28             	sub    $0x28,%esp
    va_list ap;
    int cnt;
    va_start(ap, fmt);
  106185:	8d 45 14             	lea    0x14(%ebp),%eax
  106188:	89 45 f0             	mov    %eax,-0x10(%ebp)
    cnt = vsnprintf(str, size, fmt, ap);
  10618b:	8b 45 f0             	mov    -0x10(%ebp),%eax
  10618e:	89 44 24 0c          	mov    %eax,0xc(%esp)
  106192:	8b 45 10             	mov    0x10(%ebp),%eax
  106195:	89 44 24 08          	mov    %eax,0x8(%esp)
  106199:	8b 45 0c             	mov    0xc(%ebp),%eax
  10619c:	89 44 24 04          	mov    %eax,0x4(%esp)
  1061a0:	8b 45 08             	mov    0x8(%ebp),%eax
  1061a3:	89 04 24             	mov    %eax,(%esp)
  1061a6:	e8 08 00 00 00       	call   1061b3 <vsnprintf>
  1061ab:	89 45 f4             	mov    %eax,-0xc(%ebp)
    va_end(ap);
    return cnt;
  1061ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  1061b1:	c9                   	leave  
  1061b2:	c3                   	ret    

001061b3 <vsnprintf>:
 *
 * Call this function if you are already dealing with a va_list.
 * Or you probably want snprintf() instead.
 * */
int
vsnprintf(char *str, size_t size, const char *fmt, va_list ap) {
  1061b3:	f3 0f 1e fb          	endbr32 
  1061b7:	55                   	push   %ebp
  1061b8:	89 e5                	mov    %esp,%ebp
  1061ba:	83 ec 28             	sub    $0x28,%esp
    struct sprintbuf b = {str, str + size - 1, 0};
  1061bd:	8b 45 08             	mov    0x8(%ebp),%eax
  1061c0:	89 45 ec             	mov    %eax,-0x14(%ebp)
  1061c3:	8b 45 0c             	mov    0xc(%ebp),%eax
  1061c6:	8d 50 ff             	lea    -0x1(%eax),%edx
  1061c9:	8b 45 08             	mov    0x8(%ebp),%eax
  1061cc:	01 d0                	add    %edx,%eax
  1061ce:	89 45 f0             	mov    %eax,-0x10(%ebp)
  1061d1:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if (str == NULL || b.buf > b.ebuf) {
  1061d8:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
  1061dc:	74 0a                	je     1061e8 <vsnprintf+0x35>
  1061de:	8b 55 ec             	mov    -0x14(%ebp),%edx
  1061e1:	8b 45 f0             	mov    -0x10(%ebp),%eax
  1061e4:	39 c2                	cmp    %eax,%edx
  1061e6:	76 07                	jbe    1061ef <vsnprintf+0x3c>
        return -E_INVAL;
  1061e8:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax
  1061ed:	eb 2a                	jmp    106219 <vsnprintf+0x66>
    }
    // print the string to the buffer
    vprintfmt((void*)sprintputch, &b, fmt, ap);
  1061ef:	8b 45 14             	mov    0x14(%ebp),%eax
  1061f2:	89 44 24 0c          	mov    %eax,0xc(%esp)
  1061f6:	8b 45 10             	mov    0x10(%ebp),%eax
  1061f9:	89 44 24 08          	mov    %eax,0x8(%esp)
  1061fd:	8d 45 ec             	lea    -0x14(%ebp),%eax
  106200:	89 44 24 04          	mov    %eax,0x4(%esp)
  106204:	c7 04 24 41 61 10 00 	movl   $0x106141,(%esp)
  10620b:	e8 53 fb ff ff       	call   105d63 <vprintfmt>
    // null terminate the buffer
    *b.buf = '\0';
  106210:	8b 45 ec             	mov    -0x14(%ebp),%eax
  106213:	c6 00 00             	movb   $0x0,(%eax)
    return b.cnt;
  106216:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
  106219:	c9                   	leave  
  10621a:	c3                   	ret    
